All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.9551548855205985
MST len: 3.1948529154199092
Steiner Ratio: 0.924973688540586
Solution Steiner tree:
	Terminals:
	  #0: (0.0156746367,0.2746502162,0.9714187407,0.2212781451,0.4164950971) [ #9 ]
	  #1: (0.3028504617,0.4612696383,0.4511669648,0.2853909383,0.0763532385) [ #3 ]
	  #2: (0.2561370005,0.3634193076,0.2718471113,0.4159667661,0.2562429226) [ #3 ]
	  #4: (0.8217415124,0.9652387276,0.3917416899,0.3736224269,0.9697014317) [ #7 ]
	  #6: (0.0796446726,0.2805664764,0.4484554052,0.4982478328,0.8940018629) [ #7 ]
	  #8: (0.2747455962,0.0464677648,0.9927552245,0.080030445,0.1476887968) [ #9 ]
	Steiner points:
	  #3: (0.26216158456806843,0.39370814422060996,0.3988865265491277,0.35594666276586223,0.24601534990271123) [ #5 #1 #2 ]
	  #5: (0.22483081780391398,0.3656678492434465,0.5482524224283174,0.33844612232749643,0.4252825929896355) [ #7 #3 #9 ]
	  #7: (0.25713815611843116,0.42357034922666587,0.4820056218531967,0.4109044794112828,0.7066706909089439) [ #4 #5 #6 ]
	  #9: (0.11838482162443244,0.2414606172515875,0.8882354590356284,0.21339101513093883,0.3569132559988968) [ #0 #5 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.005183051
	Number of best updates: 2

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 93

	Init times:
		Bottleneck Steiner distances: 0.000018788
		Smallest sphere distances: 0.000000712
		Sorting: 0.000001044
		Total init time: 0.000021506

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000011061, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003412, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000023917, bsd pruned: 0, ss pruned: 30

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 75
	Total number iterations 750 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 10
	Total time: 0.005074943
	Avarage time pr. topology: 0.000067665

Data for the geometric median iteration:
	Total time: 0.004875172
	Total steps: 26372
	Average number of steps pr. geometric median: 87.90666666666667
	Average time pr. step: 0.00000018486167146974063
	Average time pr. geometric median: 0.000016250573333333332

