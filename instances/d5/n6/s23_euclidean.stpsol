All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.1090392416238775
MST len: 3.396675534687256
Steiner Ratio: 0.9153182898613652
Solution Steiner tree:
	Terminals:
	  #0: (0.6899727819,0.193846746,0.6994581058,0.1578644305,0.7714722398) [ #3 ]
	  #1: (0.7275817463,0.4842158102,0.9432684327,0.3444002356,0.4630026335) [ #5 ]
	  #2: (0.5578971945,0.2818813474,0.2621717841,0.5012496633,0.921708364) [ #7 ]
	  #4: (0.4597480886,0.253197706,0.9255737224,0.0517316135,0.6330953071) [ #5 ]
	  #6: (0.6140612409,0.9471684238,0.6104576749,0.9758801474,0.721258364) [ #9 ]
	  #8: (0.0351164541,0.1380414735,0.5359635696,0.9014610839,0.1763991002) [ #9 ]
	Steiner points:
	  #3: (0.6376108728054704,0.24972182991684155,0.7120698630193976,0.21692370429443836,0.7259279410021796) [ #7 #0 #5 ]
	  #5: (0.5970185298075869,0.3019432081774442,0.8346340241778962,0.18878266526648052,0.6372848901369657) [ #1 #4 #3 ]
	  #7: (0.541226892990472,0.3307548496202413,0.46861534161646096,0.49915555881664303,0.7688556728271293) [ #2 #3 #9 ]
	  #9: (0.4487884307038134,0.4484749076318056,0.5202793962138843,0.71150993371831,0.6261348252544962) [ #6 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.007738892
	Number of best updates: 10

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000005928
		Smallest sphere distances: 0.000000781
		Sorting: 0.000001179
		Total init time: 0.000008668

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000818, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003599, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031113, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 1201 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 11.438095238095238
	Total time: 0.007622304
	Avarage time pr. topology: 0.000072593

Data for the geometric median iteration:
	Total time: 0.007286924
	Total steps: 36514
	Average number of steps pr. geometric median: 86.93809523809524
	Average time pr. step: 0.00000019956520786547625
	Average time pr. geometric median: 0.000017349819047619047

