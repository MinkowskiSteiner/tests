All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 6.5654073602451986
MST len: 7.772700369499999
Steiner Ratio: 0.8446752155798767
Solution Steiner tree:
	Terminals:
	  #0: (0.9082862325,0.9183224421,0.4284378902,0.8492767708,0.0796413781) [ #7 ]
	  #1: (0.1370705227,0.0844243104,0.5482809849,0.0280388566,0.8680997448) [ #5 ]
	  #2: (0.4696959581,0.4854901943,0.4042461926,0.7061174278,0.9462171723) [ #9 ]
	  #4: (0.9854360809,0.0812585769,0.4243695994,0.1816147301,0.3554427341) [ #5 ]
	  #6: (0.2806415555,0.9451450268,0.2765807278,0.0144947227,0.0034960154) [ #7 ]
	  #8: (0.1055795798,0.3157934068,0.1158501856,0.8452832088,0.8998082606) [ #9 ]
	Steiner points:
	  #3: (0.46969596111694245,0.42892459860471777,0.4042461926,0.4452056168921814,0.6972140745666708) [ #5 #7 #9 ]
	  #5: (0.46969596111694245,0.08442431040000005,0.4243695994,0.1816147301,0.6972140745666708) [ #4 #3 #1 ]
	  #7: (0.46969596111694245,0.9183224421,0.4042461926,0.4452056168921814,0.0796413781) [ #6 #3 #0 ]
	  #9: (0.46969318164187557,0.42892459860471777,0.40424399232986813,0.7061174278,0.8998082606) [ #2 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.002359578
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.00002864
		Smallest sphere distances: 0.000002103
		Sorting: 0.000004363
		Total init time: 0.000036222

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001389, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000008577, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000082313, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 211 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0095238095238095
	Total time: 0.002109471
	Avarage time pr. topology: 0.00002009

Data for the geometric median iteration:
	Total time: 0.00178138
	Total steps: 2959
	Average number of steps pr. geometric median: 7.045238095238095
	Average time pr. step: 0.0000006020209530246705
	Average time pr. geometric median: 0.000004241380952380952

