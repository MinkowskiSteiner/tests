All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.036729514375458
MST len: 3.308761185858716
Steiner Ratio: 0.9177844346561209
Solution Steiner tree:
	Terminals:
	  #0: (0.540312229,0.1304018107,0.0569058615,0.9637256646,0.384026217) [ #9 ]
	  #1: (0.5076044628,0.0156422849,0.215467854,0.2348691641,0.1913313205) [ #7 ]
	  #2: (0.8656925191,0.3785816228,0.6610816175,0.5074669032,0.561359823) [ #5 ]
	  #4: (0.9218689981,0.5204600438,0.6477700405,0.0916402913,0.5212438672) [ #5 ]
	  #6: (0.4307993098,0.5479775013,0.0151233026,0.0380932228,0.0248203492) [ #7 ]
	  #8: (0.8943212232,0.6286429756,0.1040961771,0.975851736,0.6714702508) [ #9 ]
	Steiner points:
	  #3: (0.6983480480865202,0.3140828910055023,0.32338996857689417,0.5093019945669992,0.41048228371312717) [ #7 #5 #9 ]
	  #5: (0.8416182952134027,0.3918595894898792,0.588735050843352,0.42982958356848916,0.5226253920877748) [ #2 #4 #3 ]
	  #7: (0.543930991695097,0.20675927377563919,0.20373504444071255,0.26937997532821567,0.21696124328287736) [ #1 #3 #6 ]
	  #9: (0.6930292917344681,0.33146594197999724,0.16514753926258416,0.8042889981786383,0.47155008207065374) [ #0 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.008250234
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000005694
		Smallest sphere distances: 0.000000705
		Sorting: 0.000001356
		Total init time: 0.00000861

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000091, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003627, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000030381, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 1638 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 15.6
	Total time: 0.008149644
	Avarage time pr. topology: 0.000077615

Data for the geometric median iteration:
	Total time: 0.00773233
	Total steps: 41032
	Average number of steps pr. geometric median: 97.6952380952381
	Average time pr. step: 0.00000018844633456814194
	Average time pr. geometric median: 0.000018410309523809525

