All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.874929954520956
MST len: 6.1006376825
Steiner Ratio: 0.7990853101315866
Solution Steiner tree:
	Terminals:
	  #0: (0.1874291036,0.8574096602,0.9521014504,0.4074837125,0.1925271313) [ #9 ]
	  #1: (0.1171411286,0.3212535252,0.8584552788,0.7502715582,0.0250844127) [ #5 ]
	  #2: (0.1765204129,0.443329524,0.5713405118,0.4465203399,0.0654066005) [ #3 ]
	  #4: (0.5121703011,0.1360900286,0.5095511137,0.9792704061,0.1979680449) [ #7 ]
	  #6: (0.3823489777,0.5831195775,0.1180090462,0.7377023845,0.5829005281) [ #7 ]
	  #8: (0.7497371406,0.6546126439,0.7923043043,0.0619196091,0.0727788904) [ #9 ]
	Steiner points:
	  #3: (0.37486368592095654,0.443329524,0.6626830896963356,0.4465203399,0.06786616617544623) [ #5 #2 #9 ]
	  #5: (0.37486368592095654,0.3212535252,0.6626830896963356,0.7502715582,0.06786616617544623) [ #1 #3 #7 ]
	  #7: (0.3823489777,0.3212535252,0.5095511137,0.7502715581999999,0.1979680449) [ #4 #5 #6 ]
	  #9: (0.37486368592095654,0.6546126439,0.7923043043,0.4074837125,0.0727788904) [ #0 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.00221667
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000021322
		Smallest sphere distances: 0.000002163
		Sorting: 0.000004374
		Total init time: 0.0000288

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001333, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000008308, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00007774, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 212 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.019047619047619
	Total time: 0.001982265
	Avarage time pr. topology: 0.000018878

Data for the geometric median iteration:
	Total time: 0.00166242
	Total steps: 2913
	Average number of steps pr. geometric median: 6.935714285714286
	Average time pr. step: 0.0000005706900102986611
	Average time pr. geometric median: 0.000003958142857142857

