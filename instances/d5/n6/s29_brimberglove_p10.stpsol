All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.275843037696383
MST len: 5.1578151977
Steiner Ratio: 0.8290027606268424
Solution Steiner tree:
	Terminals:
	  #0: (0.3313394461,0.5059928463,0.5876262708,0.6013480707,0.5097069594) [ #3 ]
	  #1: (0.7873155781,0.22562532,0.6172168532,0.5542016586,0.1448612135) [ #5 ]
	  #2: (0.5434454384,0.7647884776,0.324058877,0.6973411928,0.589627737) [ #7 ]
	  #4: (0.7454492528,0.1616497483,0.1027180125,0.9668541835,0.2171490557) [ #5 ]
	  #6: (0.3799464867,0.4450082292,0.5312397017,0.8309675659,0.9260082347) [ #7 ]
	  #8: (0.8759893737,0.4619202984,0.7565934233,0.2626699606,0.5662877856) [ #9 ]
	Steiner points:
	  #3: (0.5434454383999999,0.49130199699999993,0.5876262708,0.6013480707,0.5097069594) [ #7 #0 #9 ]
	  #5: (0.7873155608154365,0.22562532,0.6172168531999999,0.5542016586,0.2171490557) [ #1 #4 #9 ]
	  #7: (0.5434454383999999,0.49130199699999993,0.5312397017,0.601348070703617,0.589627737) [ #6 #3 #2 ]
	  #9: (0.7873155608154365,0.4619202984,0.6172168531999999,0.5542016586,0.5097069594) [ #3 #5 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.002098012
	Number of best updates: 7

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000009938
		Smallest sphere distances: 0.000002204
		Sorting: 0.000004386
		Total init time: 0.000017449

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000136, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000008538, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000078937, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 210 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.001859339
	Avarage time pr. topology: 0.000017707

Data for the geometric median iteration:
	Total time: 0.001537187
	Total steps: 2652
	Average number of steps pr. geometric median: 6.314285714285714
	Average time pr. step: 0.0000005796331070889894
	Average time pr. geometric median: 0.0000036599690476190474

