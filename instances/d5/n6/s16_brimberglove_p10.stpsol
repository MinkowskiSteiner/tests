All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.005837460421686
MST len: 5.887361588299999
Steiner Ratio: 0.8502683902360995
Solution Steiner tree:
	Terminals:
	  #0: (0.2154374701,0.0895364271,0.3215230151,0.5994994997,0.9209823329) [ #5 ]
	  #1: (0.0983172874,0.3055314134,0.1783176238,0.3670024752,0.0997784725) [ #7 ]
	  #2: (0.1180784167,0.5551794663,0.3833639991,0.2886057418,0.8238008194) [ #3 ]
	  #4: (0.5895236719,0.2716148031,0.0401816108,0.8858513329,0.9203709387) [ #5 ]
	  #6: (0.3094491373,0.8584360545,0.6783925484,0.0389199243,0.0333043621) [ #7 ]
	  #8: (0.0083380486,0.2498201687,0.6923724262,0.3582976681,0.8231695224) [ #9 ]
	Steiner points:
	  #3: (0.1180784167,0.40982869652168574,0.3833639991000001,0.2886057418,0.8238008194) [ #2 #5 #9 ]
	  #5: (0.2154374701,0.2716148031,0.3215230151,0.5994994997,0.9203709387) [ #4 #0 #3 ]
	  #7: (0.0983172874,0.40982869652168574,0.39485676561123917,0.3118363839,0.0997784725) [ #1 #6 #9 ]
	  #9: (0.0983172874,0.40982869652168574,0.39485676561123917,0.3118363839,0.8231695223999997) [ #3 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.002337555
	Number of best updates: 8

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000010523
		Smallest sphere distances: 0.000002135
		Sorting: 0.000004544
		Total init time: 0.000018604

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001754, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000008789, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000084605, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 210 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.002087066
	Avarage time pr. topology: 0.000019876

Data for the geometric median iteration:
	Total time: 0.001749281
	Total steps: 2893
	Average number of steps pr. geometric median: 6.8880952380952385
	Average time pr. step: 0.0000006046598686484619
	Average time pr. geometric median: 0.000004164954761904763

Data for the geometric median step function:
	Total number of fixed points encountered: 7322

