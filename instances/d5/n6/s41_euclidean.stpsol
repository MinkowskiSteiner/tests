All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.7470975031016858
MST len: 3.091712578967041
Steiner Ratio: 0.8885358625475809
Solution Steiner tree:
	Terminals:
	  #0: (0.022327458,0.7685739201,0.5612500564,0.4878506383,0.4318964698) [ #5 ]
	  #1: (0.6702460561,0.9116797088,0.38213495,0.5965716008,0.2660512897) [ #7 ]
	  #2: (0.3927549945,0.8972157686,0.7339704534,0.1277633953,0.3233618859) [ #3 ]
	  #4: (0.0549102435,0.9953047172,0.408344438,0.1785172881,0.7420079246) [ #5 ]
	  #6: (0.7889988608,0.387681659,0.2726833854,0.3569481244,0.0666660061) [ #7 ]
	  #8: (0.8467889367,0.5935844293,0.9065222973,0.5735588286,0.6033617564) [ #9 ]
	Steiner points:
	  #3: (0.38050910797531623,0.842363542063315,0.6411827204691913,0.27883050505663176,0.3888281851009885) [ #2 #5 #9 ]
	  #5: (0.14929370732298924,0.8492112726713548,0.5499212436229491,0.34186962791362663,0.4943896002555453) [ #4 #0 #3 ]
	  #7: (0.6621848490041035,0.7555283994857284,0.4620050070473808,0.48708492537028336,0.28259188361107196) [ #1 #6 #9 ]
	  #9: (0.6094891114252861,0.7524735714064368,0.5987824659847515,0.4389352588908191,0.37420109725827805) [ #3 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.007500157
	Number of best updates: 7

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000006135
		Smallest sphere distances: 0.000000763
		Sorting: 0.000001365
		Total init time: 0.000009135

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000988, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003312, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031343, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 1278 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 12.17142857142857
	Total time: 0.007397536
	Avarage time pr. topology: 0.000070452

Data for the geometric median iteration:
	Total time: 0.007030912
	Total steps: 35245
	Average number of steps pr. geometric median: 83.91666666666667
	Average time pr. step: 0.0000001994867924528302
	Average time pr. geometric median: 0.00001674026666666667

