All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.0465436386454465
MST len: 8.3209195963
Steiner Ratio: 0.8468467405668455
Solution Steiner tree:
	Terminals:
	  #0: (0.3430800132,0.2595131855,0.6990013098,0.2817635985,0.6604524426) [ #3 ]
	  #1: (0.2335532183,0.158582164,0.7489994023,0.9178017638,0.3488313283) [ #17 ]
	  #2: (0.3601613163,0.4198029756,0.654078258,0.1943656375,0.3097259371) [ #11 ]
	  #4: (0.5068725648,0.1146662767,0.5706952487,0.6701787769,0.9271462466) [ #7 ]
	  #6: (0.6525253559,0.0558690829,0.5223290522,0.6627442654,0.7797560393) [ #7 ]
	  #8: (0.0087156049,0.4163471956,0.8781052776,0.6627938634,0.6107128331) [ #13 ]
	  #10: (0.9164578756,0.1339819669,0.1912108968,0.2600808285,0.2142811954) [ #15 ]
	  #12: (0.1878312143,0.1696664277,0.7253791097,0.7585264629,0.8398452051) [ #13 ]
	  #14: (0.8649749825,0.1982214736,0.79443441,0.0561858788,0.458302302) [ #15 ]
	  #16: (0.29734262,0.6824838476,0.0102189095,0.7238869563,0.0950620273) [ #17 ]
	Steiner points:
	  #3: (0.3430800132,0.25951748804876446,0.6387764996526166,0.2817635985,0.3488313283) [ #11 #5 #0 ]
	  #5: (0.31595556717703926,0.25951748804876446,0.6387764996526166,0.6701787769,0.3488313283) [ #9 #3 #17 ]
	  #7: (0.5068725648,0.1146662767,0.5706952487,0.6701787769,0.7797560393) [ #6 #4 #9 ]
	  #9: (0.31595556717703926,0.25951748804876446,0.6387764996526166,0.6701787769,0.6107128331) [ #5 #13 #7 ]
	  #11: (0.3601613163,0.25951748804876446,0.6387764996526166,0.1943656471280482,0.3097259371) [ #3 #2 #15 ]
	  #13: (0.1878312143,0.2595174880487645,0.7253791097,0.6701787769,0.6107128331) [ #8 #12 #9 ]
	  #15: (0.8649749825,0.1982214736,0.6387764996526166,0.1943656471280482,0.3097259371) [ #10 #11 #14 ]
	  #17: (0.29734262,0.25951748804876446,0.6387764996526166,0.7238869563,0.3488274561468499) [ #1 #5 #16 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 51.876424133
	Number of best updates: 28

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1768218

	Init times:
		Bottleneck Steiner distances: 0.000036816
		Smallest sphere distances: 0.000005998
		Sorting: 0.000033645
		Total init time: 0.000077659

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001539, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000008495, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000080449, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.00091494, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.012147658, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.186639202, bsd pruned: 0, ss pruned: 0
		Level 7 - Time: 2.729544794, bsd pruned: 0, ss pruned: 405405

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 1621620
	Total number iterations 3320116 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.047406914073581
	Total time: 46.346061118
	Avarage time pr. topology: 0.000028579

Data for the geometric median iteration:
	Total time: 36.818965831
	Total steps: 62981867
	Average number of steps pr. geometric median: 4.854857102773769
	Average time pr. step: 0.0000005845962907228521
	Average time pr. geometric median: 0.0000028381314542710374

