All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.727500329035626
MST len: 5.27605881970902
Steiner Ratio: 0.8960287386061312
Solution Steiner tree:
	Terminals:
	  #0: (0.2561370005,0.3634193076,0.2718471113,0.4159667661,0.2562429226) [ #13 ]
	  #1: (0.7391616813,0.1305225478,0.2126854878,0.0110087921,0.546489314) [ #3 ]
	  #2: (0.0796446726,0.2805664764,0.4484554052,0.4982478328,0.8940018629) [ #7 ]
	  #4: (0.3028504617,0.4612696383,0.4511669648,0.2853909383,0.0763532385) [ #9 ]
	  #6: (0.4773686391,0.6483680232,0.016169196,0.4701238631,0.7283984682) [ #11 ]
	  #8: (0.0156746367,0.2746502162,0.9714187407,0.2212781451,0.4164950971) [ #17 ]
	  #10: (0.9029668076,0.6279880356,0.3226665842,0.8743855478,0.8492661807) [ #15 ]
	  #12: (0.1638579933,0.5497685357,0.0089649442,0.6123133984,0.048016368) [ #13 ]
	  #14: (0.8217415124,0.9652387276,0.3917416899,0.3736224269,0.9697014317) [ #15 ]
	  #16: (0.2747455962,0.0464677648,0.9927552245,0.080030445,0.1476887968) [ #17 ]
	Steiner points:
	  #3: (0.40728993608202047,0.35219411635884496,0.26374705497692663,0.33195104796803865,0.5155734025508923) [ #5 #1 #7 ]
	  #5: (0.2793654207561877,0.3691809646758606,0.3018013896746202,0.38639302715842777,0.2721736124020782) [ #9 #13 #3 ]
	  #7: (0.3733375465469643,0.4173919512308274,0.2566860433364029,0.40797851457614065,0.6560533675047202) [ #2 #11 #3 ]
	  #9: (0.28027628474818134,0.4076766728636445,0.46194078815515827,0.3003145640093361,0.15065079928282915) [ #4 #5 #17 ]
	  #11: (0.5046510664635311,0.6121396375649082,0.12233946987302421,0.47562922896476223,0.7355856083273757) [ #15 #7 #6 ]
	  #13: (0.2561370005,0.3634193076,0.2718471113,0.4159667661,0.2562429226) [ #5 #12 #0 ]
	  #15: (0.7377859626573435,0.7336581749366129,0.27582921368748975,0.5707480399746759,0.8494674602179729) [ #10 #11 #14 ]
	  #17: (0.1669599846806481,0.2190208513492235,0.8712351852171029,0.1861413920452292,0.26136950196803527) [ #8 #9 #16 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 168.902180983
	Number of best updates: 23

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1497948

	Init times:
		Bottleneck Steiner distances: 0.000016379
		Smallest sphere distances: 0.000001496
		Sorting: 0.000004354
		Total init time: 0.00002324

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000747, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003467, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031991, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000360838, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004733314, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.071991423, bsd pruned: 0, ss pruned: 0
		Level 7 - Time: 0.925473822, bsd pruned: 270270, ss pruned: 405405

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 1351350
	Total number iterations 26408726 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 19.542476782476783
	Total time: 167.098806767
	Avarage time pr. topology: 0.000123653

Data for the geometric median iteration:
	Total time: 154.979325165
	Total steps: 842933141
	Average number of steps pr. geometric median: 77.97139351389352
	Average time pr. step: 0.0000001838571977145694
	Average time pr. geometric median: 0.000014335601913364416

