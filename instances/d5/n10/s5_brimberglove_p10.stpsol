All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 8.133675668981526
MST len: 10.0147890084
Steiner Ratio: 0.8121664532482241
Solution Steiner tree:
	Terminals:
	  #0: (0.3028504617,0.4612696383,0.4511669648,0.2853909383,0.0763532385) [ #13 ]
	  #1: (0.0796446726,0.2805664764,0.4484554052,0.4982478328,0.8940018629) [ #7 ]
	  #2: (0.2561370005,0.3634193076,0.2718471113,0.4159667661,0.2562429226) [ #3 ]
	  #4: (0.7391616813,0.1305225478,0.2126854878,0.0110087921,0.546489314) [ #9 ]
	  #6: (0.4773686391,0.6483680232,0.016169196,0.4701238631,0.7283984682) [ #11 ]
	  #8: (0.0156746367,0.2746502162,0.9714187407,0.2212781451,0.4164950971) [ #17 ]
	  #10: (0.9029668076,0.6279880356,0.3226665842,0.8743855478,0.8492661807) [ #15 ]
	  #12: (0.1638579933,0.5497685357,0.0089649442,0.6123133984,0.048016368) [ #13 ]
	  #14: (0.8217415124,0.9652387276,0.3917416899,0.3736224269,0.9697014317) [ #15 ]
	  #16: (0.2747455962,0.0464677648,0.9927552245,0.080030445,0.1476887968) [ #17 ]
	Steiner points:
	  #3: (0.27427230306421063,0.3634193076,0.44845540519955485,0.2853909383742029,0.2562429226) [ #2 #13 #5 ]
	  #5: (0.2802978762829687,0.27465021620000424,0.44845540519955485,0.2212792893845489,0.546489314) [ #9 #3 #7 ]
	  #7: (0.28029787628296876,0.2805664764,0.4484554052,0.4982478328,0.7686877056999973) [ #1 #11 #5 ]
	  #9: (0.2802978762829687,0.274650216199993,0.44845540519955485,0.22127690383705742,0.546489314) [ #4 #5 #17 ]
	  #11: (0.4773686391,0.6483680231999701,0.3456916194237599,0.4982478328,0.7686877056999973) [ #15 #7 #6 ]
	  #13: (0.27427230306421063,0.46126963830000006,0.4484554051995548,0.2853909383742029,0.0763532385) [ #3 #12 #0 ]
	  #15: (0.8217415124,0.6483680231999701,0.3456916194237599,0.4982478328,0.8492661807) [ #10 #11 #14 ]
	  #17: (0.2747455962,0.27464847531933295,0.9714187407,0.221276547734609,0.4164950971) [ #8 #9 #16 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 37.80454329
	Number of best updates: 22

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1269258

	Init times:
		Bottleneck Steiner distances: 0.000037484
		Smallest sphere distances: 0.00000674
		Sorting: 0.00002794
		Total init time: 0.000073673

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001838, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000008629, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000080841, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000908637, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.012154268, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.18707757, bsd pruned: 0, ss pruned: 0
		Level 7 - Time: 2.31818983, bsd pruned: 405405, ss pruned: 498960

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 1122660
	Total number iterations 2297127 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.046146651702207
	Total time: 33.326709661
	Avarage time pr. topology: 0.000029685

Data for the geometric median iteration:
	Total time: 26.73870292
	Total steps: 45997716
	Average number of steps pr. geometric median: 5.121510074287852
	Average time pr. step: 0.0000005813050134924091
	Average time pr. geometric median: 0.000002977159482835409

