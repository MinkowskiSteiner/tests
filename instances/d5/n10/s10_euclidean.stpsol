All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.6402620242280905
MST len: 6.410209964762572
Steiner Ratio: 0.8798872510000537
Solution Steiner tree:
	Terminals:
	  #0: (0.2144171056,0.0119858943,0.8683905582,0.3317870611,0.5361120061) [ #7 ]
	  #1: (0.9637281978,0.2458365472,0.6618976293,0.3858842744,0.2711707318) [ #13 ]
	  #2: (0.5658107323,0.6109299178,0.5057680786,0.1796468744,0.8166862739) [ #5 ]
	  #4: (0.1834716542,0.584652962,0.4221560766,0.0253341845,0.3162595943) [ #15 ]
	  #6: (0.2287187526,0.0080249533,0.6942072132,0.3210569827,0.8889881558) [ #7 ]
	  #8: (0.0530768098,0.2689884604,0.0923382296,0.8809632025,0.5625731631) [ #9 ]
	  #10: (0.6635139085,0.9814409283,0.9619104471,0.1394470134,0.2234422426) [ #11 ]
	  #12: (0.9781572241,0.4471604677,0.3548297497,0.954948144,0.4252187523) [ #13 ]
	  #14: (0.5565968144,0.8975977934,0.1470419239,0.0623648931,0.0772446674) [ #15 ]
	  #16: (0.0612761933,0.083659018,0.9767909204,0.9780582851,0.8738890034) [ #17 ]
	Steiner points:
	  #3: (0.5609801454284283,0.413039243777773,0.5328448075100786,0.4432437734419747,0.5380859678817962) [ #5 #13 #9 ]
	  #5: (0.5408830813012864,0.5715589056043827,0.5305628460983101,0.26607346436114554,0.5805395974416768) [ #2 #11 #3 ]
	  #7: (0.2237721028099722,0.05853500484364237,0.7487300539695126,0.424468603493519,0.7176535119767181) [ #6 #0 #17 ]
	  #9: (0.3065291700018824,0.25175147921609053,0.5271858049021748,0.5818098453444825,0.6217251913811591) [ #3 #8 #17 ]
	  #11: (0.4937193344304354,0.7027503005289284,0.553063708711777,0.1627966091905341,0.38572311702431045) [ #10 #5 #15 ]
	  #13: (0.8136050158732875,0.3576338025305299,0.5394123260091346,0.5435552058147468,0.409706245743222) [ #3 #12 #1 ]
	  #15: (0.400659751040365,0.7014062557896175,0.4275478243291991,0.09598352472972814,0.30057047476903126) [ #14 #11 #4 ]
	  #17: (0.22603679392114767,0.12326704405589355,0.7122554924195716,0.5555275007514222,0.7103078300563224) [ #7 #9 #16 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 240.60192577
	Number of best updates: 31

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 2038488

	Init times:
		Bottleneck Steiner distances: 0.000015928
		Smallest sphere distances: 0.000001459
		Sorting: 0.000013097
		Total init time: 0.000040375

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000823, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003451, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032362, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000366321, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004699107, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.071550278, bsd pruned: 0, ss pruned: 0
		Level 7 - Time: 1.170686614, bsd pruned: 135135, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 1891890
	Total number iterations 33997201 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 17.96996706996707
	Total time: 238.456062971
	Avarage time pr. topology: 0.000126041

Data for the geometric median iteration:
	Total time: 222.503315722
	Total steps: 1213633471
	Average number of steps pr. geometric median: 80.1865773776488
	Average time pr. step: 0.00000018333650236151077
	Average time pr. geometric median: 0.000014701126632758775

