All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 8.720905860181936
MST len: 10.9500849667
Steiner Ratio: 0.7964235790592348
Solution Steiner tree:
	Terminals:
	  #0: (0.2144171056,0.0119858943,0.8683905582,0.3317870611,0.5361120061) [ #7 ]
	  #1: (0.9637281978,0.2458365472,0.6618976293,0.3858842744,0.2711707318) [ #3 ]
	  #2: (0.5658107323,0.6109299178,0.5057680786,0.1796468744,0.8166862739) [ #5 ]
	  #4: (0.1834716542,0.584652962,0.4221560766,0.0253341845,0.3162595943) [ #11 ]
	  #6: (0.2287187526,0.0080249533,0.6942072132,0.3210569827,0.8889881558) [ #7 ]
	  #8: (0.0530768098,0.2689884604,0.0923382296,0.8809632025,0.5625731631) [ #17 ]
	  #10: (0.6635139085,0.9814409283,0.9619104471,0.1394470134,0.2234422426) [ #15 ]
	  #12: (0.9781572241,0.4471604677,0.3548297497,0.954948144,0.4252187523) [ #13 ]
	  #14: (0.5565968144,0.8975977934,0.1470419239,0.0623648931,0.0772446674) [ #15 ]
	  #16: (0.0612761933,0.083659018,0.9767909204,0.9780582851,0.8738890034) [ #17 ]
	Steiner points:
	  #3: (0.5658107322996471,0.2458365472,0.6618976293,0.3858842744,0.42521875229999995) [ #13 #1 #9 ]
	  #5: (0.5658107322996471,0.584652962,0.5057680786000001,0.1796468744,0.4252187523) [ #11 #13 #2 ]
	  #7: (0.22871875259999996,0.0119858943,0.6942072163648022,0.38588427438537615,0.5625694604287165) [ #0 #9 #6 ]
	  #9: (0.22871875259999996,0.18147494973391493,0.6942072163648022,0.3858842744,0.5625694604287165) [ #7 #3 #17 ]
	  #11: (0.5658107322996471,0.584652962,0.5057680786000001,0.1137529733481645,0.3162595943) [ #15 #5 #4 ]
	  #13: (0.5658107322996471,0.4471604677,0.5057680786000001,0.3858842744,0.4252187523) [ #5 #12 #3 ]
	  #15: (0.5658107322996471,0.8975977934,0.5057680786000001,0.1137529733481645,0.2234422426) [ #10 #11 #14 ]
	  #17: (0.0612761933,0.18147494973391493,0.6942072163648021,0.8809632025,0.5625763001314049) [ #8 #9 #16 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 36.62903613
	Number of best updates: 38

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1269258

	Init times:
		Bottleneck Steiner distances: 0.000038957
		Smallest sphere distances: 0.000005955
		Sorting: 0.000032989
		Total init time: 0.000079171

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001502, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000008485, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000078251, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000879904, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.011683686, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.18026396, bsd pruned: 0, ss pruned: 0
		Level 7 - Time: 2.288837801, bsd pruned: 405405, ss pruned: 498960

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 1122660
	Total number iterations 2292708 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.042210464432687
	Total time: 32.223129603
	Avarage time pr. topology: 0.000028701

Data for the geometric median iteration:
	Total time: 25.734955763
	Total steps: 46651882
	Average number of steps pr. geometric median: 5.194346685550389
	Average time pr. step: 0.0000005516381046106564
	Average time pr. geometric median: 0.000002865399560307662

