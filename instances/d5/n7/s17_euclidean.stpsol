All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.9069471479676796
MST len: 3.3516770850449964
Steiner Ratio: 0.8673112218770483
Solution Steiner tree:
	Terminals:
	  #0: (0.22754956,0.1078885724,0.4860870128,0.6620165676,0.9043156765) [ #11 ]
	  #1: (0.3574414162,0.5424212746,0.2320200839,0.4801497965,0.4595757073) [ #7 ]
	  #2: (0.1680129702,0.6031280205,0.3763264294,0.7351060159,0.8217135835) [ #3 ]
	  #4: (0.7641613385,0.5371329414,0.440277009,0.726414976,0.6913943327) [ #5 ]
	  #6: (0.0627080598,0.4779169375,0.2268656857,0.9706272744,0.2301676097) [ #7 ]
	  #8: (0.6258278161,0.215128048,0.2804932088,0.3381360212,0.8132569412) [ #9 ]
	  #10: (0.5717940003,0.0018524737,0.1227083798,0.9171544332,0.8540148287) [ #11 ]
	Steiner points:
	  #3: (0.3374911572610208,0.5029398090542521,0.32502280782240134,0.6635738163030562,0.6603167055080276) [ #5 #2 #7 ]
	  #5: (0.504508422590792,0.4123188220856901,0.3522736592802913,0.6489752872110663,0.7209567314650009) [ #3 #4 #9 ]
	  #7: (0.30686933892979973,0.5152416624727868,0.27396729151119514,0.6332346367315164,0.5194413570457658) [ #1 #3 #6 ]
	  #9: (0.5042925828614653,0.2713742339275142,0.3277820897458159,0.5957092876887546,0.7865382473705201) [ #8 #5 #11 ]
	  #11: (0.4403427503716499,0.16904580778728143,0.3296342608551016,0.6814693396171532,0.8338254639999145) [ #0 #9 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.071561055
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000020506
		Smallest sphere distances: 0.000000988
		Sorting: 0.000001912
		Total init time: 0.000024373

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000818, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003548, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000030539, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000342421, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 11240 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 11.894179894179894
	Total time: 0.070712287
	Avarage time pr. topology: 0.000074827

Data for the geometric median iteration:
	Total time: 0.067268947
	Total steps: 369917
	Average number of steps pr. geometric median: 78.28931216931217
	Average time pr. step: 0.0000001818487579646245
	Average time pr. geometric median: 0.00001423681417989418

