All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.070477559974862
MST len: 5.8856137218
Steiner Ratio: 0.8615036255597412
Solution Steiner tree:
	Terminals:
	  #0: (0.214533581,0.4014023423,0.4076549855,0.7457033986,0.781220815) [ #5 ]
	  #1: (0.153481192,0.2006002349,0.4885317048,0.9929940151,0.7874475535) [ #9 ]
	  #2: (0.1478928184,0.0819520862,0.2319531549,0.3117739127,0.1775910422) [ #7 ]
	  #4: (0.3903584468,0.6938135543,0.3702549456,0.5785871486,0.6617850073) [ #11 ]
	  #6: (0.5034501541,0.1039613784,0.3216621426,0.2525900748,0.6675356089) [ #7 ]
	  #8: (0.383365222,0.0224110372,0.9863731647,0.8460247199,0.0179714379) [ #9 ]
	  #10: (0.1488122675,0.7401193766,0.5311698176,0.3798184727,0.6551358368) [ #11 ]
	Steiner points:
	  #3: (0.2145335809999875,0.141203835668956,0.40765596949508826,0.5785871486,0.6617850073) [ #7 #5 #9 ]
	  #5: (0.2145335809999875,0.4014023423,0.40765596949508826,0.5785871486,0.6617850073) [ #3 #0 #11 ]
	  #7: (0.21453358099998754,0.1039613784,0.3216621426,0.31177391270000004,0.6617850073) [ #3 #6 #2 ]
	  #9: (0.2145335809999875,0.141203835668956,0.4885317048,0.8460247199,0.6617850073) [ #8 #3 #1 ]
	  #11: (0.21453358099998748,0.6938135542999999,0.4076559694950882,0.578584115676981,0.6617849058432581) [ #4 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.017092338
	Number of best updates: 10

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 858

	Init times:
		Bottleneck Steiner distances: 0.000018281
		Smallest sphere distances: 0.000003801
		Sorting: 0.000008771
		Total init time: 0.000032373

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000159, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000008947, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000077949, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000721129, bsd pruned: 0, ss pruned: 210

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 735
	Total number iterations 1478 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0108843537414964
	Total time: 0.015291482
	Avarage time pr. topology: 0.000020804

Data for the geometric median iteration:
	Total time: 0.012562412
	Total steps: 22555
	Average number of steps pr. geometric median: 6.137414965986395
	Average time pr. step: 0.0000005569679450232765
	Average time pr. geometric median: 0.0000034183434013605444

Data for the geometric median step function:
	Total number of fixed points encountered: 64048

