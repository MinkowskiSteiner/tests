All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 6.334621221233334
MST len: 7.569056921
Steiner Ratio: 0.8369102369488357
Solution Steiner tree:
	Terminals:
	  #0: (0.6449911807,0.8039444624,0.8545768367,0.7337866364,0.9254236528) [ #9 ]
	  #1: (0.4219620016,0.6998054984,0.0663843365,0.5874823525,0.6429663057) [ #5 ]
	  #2: (0.5966266629,0.5564203358,0.8479080023,0.1846236713,0.3974697401) [ #3 ]
	  #4: (0.7009763693,0.8096763491,0.0887954552,0.1214791905,0.3483067552) [ #5 ]
	  #6: (0.8897629529,0.8504806137,0.3177298374,0.1029680931,0.0449004877) [ #11 ]
	  #8: (0.3821752264,0.3227100961,0.9883655114,0.8425220697,0.2753141943) [ #9 ]
	  #10: (0.9906032789,0.295718284,0.2713367107,0.069655987,0.949639379) [ #11 ]
	Steiner points:
	  #3: (0.5966266629,0.6088200189666667,0.8479080023,0.1846236713,0.44361103303333405) [ #9 #2 #7 ]
	  #5: (0.7009763693,0.6998054984,0.0887954552,0.15740514523333332,0.44361103303333393) [ #7 #1 #4 ]
	  #7: (0.7009763693,0.6998054983999932,0.30226546183333336,0.15740514523333332,0.44361103303333393) [ #3 #5 #11 ]
	  #9: (0.5966266629,0.6088200189666667,0.8479080023,0.7337866364,0.44361103303333405) [ #8 #3 #0 ]
	  #11: (0.8897629529,0.6998054983999932,0.30226546183333336,0.1029680931,0.44361103303333393) [ #6 #7 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.022180722
	Number of best updates: 15

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000016033
		Smallest sphere distances: 0.000003942
		Sorting: 0.000009095
		Total init time: 0.000030575

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001543, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00000857, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000078677, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000904195, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 1917 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0285714285714285
	Total time: 0.020018105
	Avarage time pr. topology: 0.000021183

Data for the geometric median iteration:
	Total time: 0.01648211
	Total steps: 29907
	Average number of steps pr. geometric median: 6.32952380952381
	Average time pr. step: 0.000000551112114220751
	Average time pr. geometric median: 0.0000034882772486772488

Data for the geometric median step function:
	Total number of fixed points encountered: 85369

