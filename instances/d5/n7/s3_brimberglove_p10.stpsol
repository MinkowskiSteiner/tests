All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 6.182604604851791
MST len: 7.8364058588
Steiner Ratio: 0.7889592137330342
Solution Steiner tree:
	Terminals:
	  #0: (0.1744425461,0.3665206686,0.0588192782,0.5624328081,0.1339392146) [ #9 ]
	  #1: (0.7704661483,0.768258802,0.0937586353,0.1635579272,0.2121971949) [ #5 ]
	  #2: (0.5613801752,0.2249833128,0.3930917794,0.4439383934,0.2850412509) [ #3 ]
	  #4: (0.878686387,0.568443856,0.5212840333,0.2068786268,0.8687753225) [ #5 ]
	  #6: (0.8460845993,0.0099969395,0.257080875,0.0407533725,0.4439474528) [ #7 ]
	  #8: (0.0040116059,0.4605808875,0.1763232952,0.6261662299,0.9448735988) [ #9 ]
	  #10: (0.1447810513,0.5635544996,0.8646787037,0.8954018363,0.2308046744) [ #11 ]
	Steiner points:
	  #3: (0.5613801752,0.3665206686,0.3930917794,0.4439383934,0.2850412509) [ #7 #2 #11 ]
	  #5: (0.7704661483,0.568443856,0.39309177939999995,0.20687862679999997,0.3022748813777778) [ #4 #1 #7 ]
	  #7: (0.7704661483,0.3665206686,0.3930917794,0.20687862679999997,0.3022748813777778) [ #3 #6 #5 ]
	  #9: (0.1744425461,0.3665206686,0.17632329520000004,0.5624328081,0.26696239206666666) [ #0 #8 #11 ]
	  #11: (0.1744425461,0.36652367505179057,0.3930917794,0.5624328081,0.26696239206666666) [ #3 #9 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.019690957
	Number of best updates: 10

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 963

	Init times:
		Bottleneck Steiner distances: 0.000015796
		Smallest sphere distances: 0.000004251
		Sorting: 0.000008661
		Total init time: 0.00003033

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001737, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000008698, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000079947, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000807802, bsd pruned: 105, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 840
	Total number iterations 1701 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.025
	Total time: 0.017709362
	Avarage time pr. topology: 0.000021082

Data for the geometric median iteration:
	Total time: 0.014562732
	Total steps: 26551
	Average number of steps pr. geometric median: 6.321666666666666
	Average time pr. step: 0.0000005484814884561787
	Average time pr. geometric median: 0.0000034673171428571427

Data for the geometric median step function:
	Total number of fixed points encountered: 76476

