All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.479354163524702
MST len: 3.8284936786233636
Steiner Ratio: 0.9088049910992138
Solution Steiner tree:
	Terminals:
	  #0: (0.5953307052,0.7999637326,0.8187845688,0.6298717496,0.5035535896) [ #11 ]
	  #1: (0.2370779334,0.3946801244,0.8697485588,0.2358092783,0.2918461092) [ #3 ]
	  #2: (0.859679718,0.8993852078,0.1680767476,0.4144257491,0.4769978577) [ #9 ]
	  #4: (0.229967075,0.8697586934,0.5547460311,0.5776126504,0.9940509931) [ #5 ]
	  #6: (0.2899153434,0.1948941383,0.2638538039,0.6694181327,0.2983180547) [ #7 ]
	  #8: (0.6015800124,0.5553159199,0.0093155527,0.05122513,0.3029992102) [ #9 ]
	  #10: (0.7460086135,0.8155644125,0.995645286,0.5080417518,0.6990755753) [ #11 ]
	Steiner points:
	  #3: (0.39818811537512283,0.5491666857655438,0.6123623953840047,0.4311899731134115,0.43857175385260627) [ #5 #7 #1 ]
	  #5: (0.4523583806543062,0.7177777536937568,0.6946091146809431,0.5346118131056975,0.5910051151827324) [ #4 #3 #11 ]
	  #7: (0.4392951750143363,0.4889700445516473,0.40182924022882466,0.4495970420935919,0.3884001073709299) [ #6 #3 #9 ]
	  #9: (0.6284445283556195,0.6402723518651658,0.1871488823700616,0.2943733011780719,0.38493575118739953) [ #2 #7 #8 ]
	  #11: (0.5893374598119745,0.7878632409823111,0.8191849060087654,0.598793688881164,0.5422829882891431) [ #0 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.074972773
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 858

	Init times:
		Bottleneck Steiner distances: 0.000008147
		Smallest sphere distances: 0.000000962
		Sorting: 0.000001756
		Total init time: 0.000011881

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000813, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003347, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000030449, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000278332, bsd pruned: 0, ss pruned: 210

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 735
	Total number iterations 12164 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 16.54965986394558
	Total time: 0.07424317
	Avarage time pr. topology: 0.000101011

Data for the geometric median iteration:
	Total time: 0.070598831
	Total steps: 389660
	Average number of steps pr. geometric median: 106.02993197278911
	Average time pr. step: 0.00000018118059590412154
	Average time pr. geometric median: 0.000019210566258503402

