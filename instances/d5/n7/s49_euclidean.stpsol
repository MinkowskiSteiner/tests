All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.8097931680829062
MST len: 4.275314899170987
Steiner Ratio: 0.8911140484228779
Solution Steiner tree:
	Terminals:
	  #0: (0.4070928839,0.1923376211,0.4982240295,0.9403857789,0.7748197265) [ #11 ]
	  #1: (0.4855085041,0.3490163122,0.8709676535,0.6129136903,0.1817772687) [ #5 ]
	  #2: (0.2952385462,0.6634417692,0.9268148322,0.4391896513,0.6174133409) [ #7 ]
	  #4: (0.9780446412,0.8501515877,0.8986921589,0.4174514107,0.3731905494) [ #9 ]
	  #6: (0.0876867548,0.9821973629,0.9948574575,0.3964075797,0.1451486364) [ #7 ]
	  #8: (0.9290067493,0.8974420614,0.6268771643,0.9956121147,0.5463564538) [ #9 ]
	  #10: (0.0418115836,0.220037731,0.3087208249,0.1629512739,0.7427612197) [ #11 ]
	Steiner points:
	  #3: (0.3976683033275558,0.5366580934795787,0.795283189225655,0.5379418419032378,0.49844533561593396) [ #5 #7 #11 ]
	  #5: (0.5104899612716846,0.5307014138577574,0.8178089315558852,0.5768691119958238,0.3935413475598332) [ #1 #3 #9 ]
	  #7: (0.30211247379663797,0.6608860411999717,0.8988878405072037,0.46095592238759847,0.5407840516905267) [ #3 #6 #2 ]
	  #9: (0.8156382342033099,0.7606614471374299,0.8006150617728186,0.6220319796383785,0.425712143365249) [ #4 #5 #8 ]
	  #11: (0.313806484666018,0.343080733607821,0.5760794421259657,0.5819591196379534,0.6514202971359576) [ #0 #3 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.081165567
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.00002067
		Smallest sphere distances: 0.000000989
		Sorting: 0.000001882
		Total init time: 0.000024716

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000079, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003245, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031038, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000337308, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 12008 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 12.706878306878307
	Total time: 0.080341359
	Avarage time pr. topology: 0.000085017

Data for the geometric median iteration:
	Total time: 0.076673226
	Total steps: 423724
	Average number of steps pr. geometric median: 89.67703703703704
	Average time pr. step: 0.0000001809508689618714
	Average time pr. geometric median: 0.00001622713777777778

