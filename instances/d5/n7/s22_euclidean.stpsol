All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.068311780619617
MST len: 3.292965924281535
Steiner Ratio: 0.9317775680563918
Solution Steiner tree:
	Terminals:
	  #0: (0.4631831755,0.5185416162,0.7034413422,0.9385241437,0.7902598408) [ #7 ]
	  #1: (0.8650269224,0.5663939652,0.6338536677,0.0191505631,0.5234706907) [ #5 ]
	  #2: (0.563644016,0.4461416623,0.7561149335,0.1936676377,0.5023851071) [ #9 ]
	  #4: (0.8290278501,0.414524392,0.4607495118,0.1862228323,0.3292264125) [ #11 ]
	  #6: (0.9351547686,0.8035510656,0.3566538055,0.5690084359,0.8227016287) [ #7 ]
	  #8: (0.0234475681,0.1250572825,0.8931799414,0.0834255573,0.9698017333) [ #9 ]
	  #10: (0.6730409189,0.520684405,0.2885274982,0.0973462291,0.2666000273) [ #11 ]
	Steiner points:
	  #3: (0.6904949222464171,0.5042292226209346,0.6397801841109747,0.23123154697230486,0.5216806829348846) [ #5 #7 #9 ]
	  #5: (0.7758114655739723,0.5063589327844578,0.5838430308855314,0.1463794883804459,0.4698895078307443) [ #1 #3 #11 ]
	  #7: (0.7239651840754342,0.6246340789589636,0.5468674281825265,0.5549092117089025,0.7118388734102732) [ #6 #3 #0 ]
	  #9: (0.5636441699213656,0.4461397731408936,0.7561052663365393,0.19366900510771556,0.5023997402358769) [ #8 #3 #2 ]
	  #11: (0.7961845264222425,0.4475470382498832,0.45937661623682924,0.1656828023540638,0.3471264868384203) [ #4 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.053785429
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 858

	Init times:
		Bottleneck Steiner distances: 0.000008546
		Smallest sphere distances: 0.000001129
		Sorting: 0.000002057
		Total init time: 0.000012681

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000821, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003243, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000030631, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.00028962, bsd pruned: 0, ss pruned: 210

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 735
	Total number iterations 9120 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 12.408163265306122
	Total time: 0.053049875
	Avarage time pr. topology: 0.000072176

Data for the geometric median iteration:
	Total time: 0.050277059
	Total steps: 274135
	Average number of steps pr. geometric median: 74.59455782312925
	Average time pr. step: 0.0000001834025534864209
	Average time pr. geometric median: 0.000013680832380952379

