All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.137003631147889
MST len: 3.236309634511064
Steiner Ratio: 0.969315048750526
Solution Steiner tree:
	Terminals:
	  #0: (0.2319139071,0.0643970538,0.576686004,0.5689670525,0.5896822263) [ #11 ]
	  #1: (0.4875804249,0.2520150534,0.5079065824,0.3625599515,0.8518746006) [ #3 ]
	  #2: (0.2945063893,0.4759735886,0.6125465755,0.2276748625,0.680792569) [ #9 ]
	  #4: (0.9126800242,0.2932653806,0.3414342214,0.5614015812,0.7468854965) [ #7 ]
	  #6: (0.8609460652,0.737937074,0.6182698098,0.6337565941,0.1025872855) [ #7 ]
	  #8: (0.0897512576,0.6359386517,0.9450358706,0.5259626366,0.9567871433) [ #9 ]
	  #10: (0.1689700215,0.0285875835,0.7728105293,0.3646502115,0.237839454) [ #11 ]
	Steiner points:
	  #3: (0.44433875705867637,0.29544475636965106,0.5460485612796415,0.39239109380579895,0.7512849153973628) [ #5 #1 #9 ]
	  #5: (0.45809998065679564,0.2775312226612521,0.5414756833421116,0.4278135960418463,0.7093030311168622) [ #7 #3 #11 ]
	  #7: (0.7398034572589272,0.3669058338965369,0.4628218423376196,0.5262136563335937,0.618448597736278) [ #6 #4 #5 ]
	  #9: (0.30973602915339954,0.44726013518043517,0.6346560735624621,0.3069520226867287,0.7324439650129672) [ #2 #3 #8 ]
	  #11: (0.2571718196306283,0.09079363088519103,0.593336418831379,0.5262958871020988,0.5678513800799679) [ #0 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.088500044
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 963

	Init times:
		Bottleneck Steiner distances: 0.000021772
		Smallest sphere distances: 0.000001016
		Sorting: 0.000001783
		Total init time: 0.000025649

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000759, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003182, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000030617, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000308519, bsd pruned: 105, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 840
	Total number iterations 13477 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 16.044047619047618
	Total time: 0.087716681
	Avarage time pr. topology: 0.000104424

Data for the geometric median iteration:
	Total time: 0.083649775
	Total steps: 464190
	Average number of steps pr. geometric median: 110.52142857142857
	Average time pr. step: 0.00000018020589629246643
	Average time pr. geometric median: 0.000019916613095238095

