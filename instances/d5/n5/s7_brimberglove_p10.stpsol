All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.550609660788889
MST len: 5.1384405495
Steiner Ratio: 0.8856013058731778
Solution Steiner tree:
	Terminals:
	  #0: (0.5267799439,0.0893740547,0.7644367087,0.815491903,0.8889724807) [ #3 ]
	  #1: (0.163898673,0.2155044015,0.7876827134,0.788697773,0.0666457862) [ #3 ]
	  #2: (0.4325436914,0.0533801331,0.3409319503,0.5098388086,0.016024685) [ #5 ]
	  #4: (0.514818562,0.9959482527,0.0319323023,0.6015652793,0.0553448466) [ #5 ]
	  #6: (0.4869041394,0.8679774124,0.5925911942,0.2147098403,0.0102265389) [ #7 ]
	Steiner points:
	  #3: (0.468947145311111,0.21550440150000003,0.7644367087,0.7886977729999999,0.0666457862) [ #0 #1 #7 ]
	  #5: (0.4689471453111109,0.5219314756440991,0.34093195029999995,0.5098388086,0.03905924530349764) [ #2 #4 #7 ]
	  #7: (0.468947145311111,0.5219314756440991,0.5925911942,0.5098388085999999,0.03905924530349764) [ #3 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.00027926
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000006948
		Smallest sphere distances: 0.000001544
		Sorting: 0.000002335
		Total init time: 0.000011848

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001559, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000008759, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 31 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.066666666666667
	Total time: 0.000234566
	Avarage time pr. topology: 0.000015637

Data for the geometric median iteration:
	Total time: 0.000197005
	Total steps: 345
	Average number of steps pr. geometric median: 7.666666666666667
	Average time pr. step: 0.0000005710289855072463
	Average time pr. geometric median: 0.000004377888888888889

Data for the geometric median step function:
	Total number of fixed points encountered: 743

