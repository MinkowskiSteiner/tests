All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.719578054701328
MST len: 5.640063157699999
Steiner Ratio: 0.8367952490492957
Solution Steiner tree:
	Terminals:
	  #0: (0.5150421176,0.9928987739,0.9662594152,0.9828661391,0.3598284723) [ #3 ]
	  #1: (0.3443029827,0.6699542467,0.4050064401,0.8388915681,0.8634974965) [ #7 ]
	  #2: (0.1034733854,0.3869860398,0.9775588964,0.7308916956,0.3246047955) [ #3 ]
	  #4: (0.7832298362,0.3857721921,0.3929051535,0.0476035024,0.2348172144) [ #5 ]
	  #6: (0.3549731771,0.6731490375,0.5153574038,0.2768868838,0.9831143995) [ #7 ]
	Steiner points:
	  #3: (0.4906119338666667,0.5752268951666667,0.9662594152,0.7308916956,0.359828472300664) [ #5 #2 #0 ]
	  #5: (0.4906119338666667,0.5752268951666667,0.4674236128925926,0.5516357913111112,0.35982847230066395) [ #4 #3 #7 ]
	  #7: (0.3549731771,0.6699542467,0.4674236128925926,0.5516357913111112,0.8634974965) [ #1 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000295012
	Number of best updates: 2

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 15

	Init times:
		Bottleneck Steiner distances: 0.000020889
		Smallest sphere distances: 0.00000161
		Sorting: 0.000002471
		Total init time: 0.000039557

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001483, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000008016, bsd pruned: 0, ss pruned: 3

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 12
	Total number iterations 24 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000227911
	Avarage time pr. topology: 0.000018992

Data for the geometric median iteration:
	Total time: 0.000167984
	Total steps: 264
	Average number of steps pr. geometric median: 7.333333333333333
	Average time pr. step: 0.0000006363030303030303
	Average time pr. geometric median: 0.000004666222222222222

Data for the geometric median step function:
	Total number of fixed points encountered: 631

