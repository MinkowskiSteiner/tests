All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.9570584921732914
MST len: 3.264653635702313
Steiner Ratio: 0.9057801599026754
Solution Steiner tree:
	Terminals:
	  #0: (0.7351341866,0.9038881473,0.5736667344,0.0998372487,0.0471116267) [ #7 ]
	  #1: (0.1321992586,0.3741906497,0.6797411119,0.8863195306,0.801372399) [ #5 ]
	  #2: (0.946517847,0.9674219824,0.2784534135,0.4652112594,0.4872422654) [ #3 ]
	  #4: (0.2213446718,0.2145956132,0.2002614784,0.9167352235,0.3484674186) [ #5 ]
	  #6: (0.9290727782,0.9129959489,0.9238626137,0.2359872904,0.785772445) [ #7 ]
	Steiner points:
	  #3: (0.7942694867129679,0.8509670223378228,0.46463824405455767,0.4204680158314139,0.4670263566766708) [ #5 #2 #7 ]
	  #5: (0.3023935844413866,0.40938189834418964,0.4485589379792342,0.8029292553945033,0.5561804898545054) [ #1 #3 #4 ]
	  #7: (0.8059306435171638,0.8720776914034946,0.5653687325597405,0.32560856506814345,0.4395659448813309) [ #0 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000578216
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 15

	Init times:
		Bottleneck Steiner distances: 0.000005372
		Smallest sphere distances: 0.000000556
		Sorting: 0.00000103
		Total init time: 0.000007805

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000968, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00000368, bsd pruned: 0, ss pruned: 3

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 12
	Total number iterations 117 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 9.75
	Total time: 0.000553745
	Avarage time pr. topology: 0.000046145

Data for the geometric median iteration:
	Total time: 0.00052814
	Total steps: 2729
	Average number of steps pr. geometric median: 75.80555555555556
	Average time pr. step: 0.00000019352876511542688
	Average time pr. geometric median: 0.000014670555555555554

