All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.7486679383
MST len: 5.300567565
Steiner Ratio: 0.8958791450288777
Solution Steiner tree:
	Terminals:
	  #0: (0.540312229,0.1304018107,0.0569058615,0.9637256646,0.384026217) [ #3 ]
	  #1: (0.8943212232,0.6286429756,0.1040961771,0.975851736,0.6714702508) [ #3 ]
	  #2: (0.5076044628,0.0156422849,0.215467854,0.2348691641,0.1913313205) [ #5 ]
	  #4: (0.9218689981,0.5204600438,0.6477700405,0.0916402913,0.5212438672) [ #7 ]
	  #6: (0.4307993098,0.5479775013,0.0151233026,0.0380932228,0.0248203492) [ #7 ]
	Steiner points:
	  #3: (0.6565952299666666,0.2695937075666667,0.1040961771,0.9637256646,0.36553380156666676) [ #0 #5 #1 ]
	  #5: (0.6565952299666666,0.2695937075666667,0.21546785399970364,0.2348691641,0.36553380156666676) [ #2 #3 #7 ]
	  #7: (0.6565952299666666,0.5204600438000001,0.21546785399970364,0.0916402913,0.3655338015666667) [ #4 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000323808
	Number of best updates: 7

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000007587
		Smallest sphere distances: 0.000001577
		Sorting: 0.000002406
		Total init time: 0.000013082

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001887, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000010572, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000270735
	Avarage time pr. topology: 0.000018049

Data for the geometric median iteration:
	Total time: 0.000229299
	Total steps: 346
	Average number of steps pr. geometric median: 7.688888888888889
	Average time pr. step: 0.0000006627138728323699
	Average time pr. geometric median: 0.000005095533333333333

