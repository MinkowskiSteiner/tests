All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.035316646101948
MST len: 3.8432305008000003
Steiner Ratio: 0.7897826178966163
Solution Steiner tree:
	Terminals:
	  #0: (0.0917933952,0.0898954501,0.3307895457,0.6858023101,0.9032684844) [ #7 ]
	  #1: (0.0947479369,0.6750120226,0.2615602367,0.272972847,0.9914577939) [ #3 ]
	  #2: (0.206965448,0.6995153961,0.2034307295,0.3588790108,0.884531313) [ #3 ]
	  #4: (0.05407485,0.9231773037,0.0740991244,0.9287021542,0.8842824203) [ #5 ]
	  #6: (0.4632373589,0.7227528005,0.6292115332,0.7207948913,0.7304651908) [ #7 ]
	Steiner points:
	  #3: (0.09474793690000001,0.6750120213335824,0.22338294691111102,0.3588790108,0.9046567005999999) [ #2 #1 #5 ]
	  #5: (0.09474793690000001,0.6750120212322347,0.22338294691111102,0.6970571249407407,0.9046567005999999) [ #4 #3 #7 ]
	  #7: (0.09474793690000001,0.6750120212322347,0.3307895457,0.6970571249407407,0.9032684844) [ #0 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000280964
	Number of best updates: 1

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000005767
		Smallest sphere distances: 0.00000145
		Sorting: 0.000002237
		Total init time: 0.000010163

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001311, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000008525, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.00024321
	Avarage time pr. topology: 0.000016214

Data for the geometric median iteration:
	Total time: 0.00020637
	Total steps: 366
	Average number of steps pr. geometric median: 8.133333333333333
	Average time pr. step: 0.0000005638524590163934
	Average time pr. geometric median: 0.000004586

Data for the geometric median step function:
	Total number of fixed points encountered: 691

