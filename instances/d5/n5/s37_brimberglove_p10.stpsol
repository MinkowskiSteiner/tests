All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.749941806811111
MST len: 5.3228154165
Steiner Ratio: 0.8923739478334982
Solution Steiner tree:
	Terminals:
	  #0: (0.9574638638,0.1660335614,0.7214625924,0.59228968,0.9251535474) [ #5 ]
	  #1: (0.6100173786,0.6899730524,0.429892563,0.1510021729,0.9164035199) [ #7 ]
	  #2: (0.3121092172,0.450471332,0.8071258347,0.3703712553,0.6420550997) [ #3 ]
	  #4: (0.6424573714,0.3284930686,0.8837608927,0.2698044499,0.5980566296) [ #5 ]
	  #6: (0.2394123931,0.7611420498,0.1745217923,0.8194240331,0.0320747178) [ #7 ]
	Steiner points:
	  #3: (0.48648238343333366,0.450471332,0.7526834931294412,0.3703712553,0.7836936331111111) [ #2 #5 #7 ]
	  #5: (0.6424573714,0.3284930686,0.7526834931294412,0.3703712553,0.7836936331111111) [ #4 #3 #0 ]
	  #7: (0.48648238343333366,0.6899730524000001,0.429892563,0.3703712553,0.7836936331111111) [ #1 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000299627
	Number of best updates: 2

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000008012
		Smallest sphere distances: 0.000001601
		Sorting: 0.000002373
		Total init time: 0.000013017

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001572, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000008404, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000245912
	Avarage time pr. topology: 0.000016394

Data for the geometric median iteration:
	Total time: 0.000209312
	Total steps: 350
	Average number of steps pr. geometric median: 7.777777777777778
	Average time pr. step: 0.0000005980342857142857
	Average time pr. geometric median: 0.000004651377777777778

