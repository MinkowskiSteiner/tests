All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.179268737433356
MST len: 4.8283280616
Steiner Ratio: 0.8655726545740224
Solution Steiner tree:
	Terminals:
	  #0: (0.0595591958,0.9910994321,0.8735950118,0.5318192572,0.054165287) [ #3 ]
	  #1: (0.845191426,0.7414528996,0.4705074068,0.3762617672,0.7236746008) [ #7 ]
	  #2: (0.4132981074,0.5756729662,0.8249852354,0.3957488953,0.5658004533) [ #3 ]
	  #4: (0.354725022,0.9420667789,0.3395639064,0.3297338986,0.9487606892) [ #5 ]
	  #6: (0.8256802293,0.8959328252,0.2042499758,0.0233809524,0.3771066863) [ #7 ]
	Steiner points:
	  #3: (0.4132981074,0.8083241926999768,0.8249852353999999,0.36075247766666674,0.5658004533) [ #5 #2 #0 ]
	  #5: (0.4132981074,0.8083241926999768,0.38175492980000003,0.36075247766666674,0.6081519626338686) [ #4 #3 #7 ]
	  #7: (0.8256802293,0.8083241926999768,0.38175492980000003,0.3607524776666667,0.6081519626338686) [ #1 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000338348
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000008193
		Smallest sphere distances: 0.000002079
		Sorting: 0.000002212
		Total init time: 0.000013918

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001984, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000009969, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000277087
	Avarage time pr. topology: 0.000018472

Data for the geometric median iteration:
	Total time: 0.000237793
	Total steps: 369
	Average number of steps pr. geometric median: 8.2
	Average time pr. step: 0.0000006444254742547425
	Average time pr. geometric median: 0.000005284288888888888

