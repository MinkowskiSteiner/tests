All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.743970583248771
MST len: 1.8154724047618576
Steiner Ratio: 0.9606153079906132
Solution Steiner tree:
	Terminals:
	  #0: (0.0234475681,0.1250572825,0.8931799414,0.0834255573,0.9698017333) [ #3 ]
	  #1: (0.8290278501,0.414524392,0.4607495118,0.1862228323,0.3292264125) [ #5 ]
	  #2: (0.563644016,0.4461416623,0.7561149335,0.1936676377,0.5023851071) [ #3 ]
	  #4: (0.6730409189,0.520684405,0.2885274982,0.0973462291,0.2666000273) [ #5 ]
	  #6: (0.8650269224,0.5663939652,0.6338536677,0.0191505631,0.5234706907) [ #7 ]
	Steiner points:
	  #3: (0.5636444870058547,0.44613989706793983,0.756111882816309,0.19366419146301755,0.5023883220641834) [ #2 #0 #7 ]
	  #5: (0.7917219188078991,0.4468109201847636,0.4619272871288984,0.15764064307694867,0.3441524714572941) [ #1 #4 #7 ]
	  #7: (0.7636904273283139,0.49409427454465044,0.6038241388260596,0.11170848665406469,0.45493561621504564) [ #3 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000581244
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000005514
		Smallest sphere distances: 0.000000529
		Sorting: 0.000000899
		Total init time: 0.000007736

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000928, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003506, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 152 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 10.133333333333333
	Total time: 0.000556516
	Avarage time pr. topology: 0.000037101

Data for the geometric median iteration:
	Total time: 0.000522704
	Total steps: 2686
	Average number of steps pr. geometric median: 59.68888888888889
	Average time pr. step: 0.00000019460312732688012
	Average time pr. geometric median: 0.000011615644444444445

