All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.938228360448199
MST len: 5.619756499138713
Steiner Ratio: 0.8787263934309315
Solution Steiner tree:
	Terminals:
	  #0: (0.1321992586,0.3741906497,0.6797411119,0.8863195306,0.801372399) [ #11 ]
	  #1: (0.7333814817,0.3375514235,0.1884241831,0.0118348948,0.8027626829) [ #7 ]
	  #2: (0.4671709633,0.9850149108,0.9476090371,0.391033577,0.2210022007) [ #3 ]
	  #4: (0.7351341866,0.9038881473,0.5736667344,0.0998372487,0.0471116267) [ #5 ]
	  #6: (0.946517847,0.9674219824,0.2784534135,0.4652112594,0.4872422654) [ #7 ]
	  #8: (0.0357322353,0.1799209691,0.9172431221,0.0559421322,0.0346130878) [ #9 ]
	  #10: (0.2213446718,0.2145956132,0.2002614784,0.9167352235,0.3484674186) [ #11 ]
	  #12: (0.9290727782,0.9129959489,0.9238626137,0.2359872904,0.785772445) [ #13 ]
	Steiner points:
	  #3: (0.5057394919150732,0.8246146340896274,0.7913933432634651,0.33256498932651,0.2543535458495101) [ #9 #2 #5 ]
	  #5: (0.66574913376426,0.8531831690547647,0.6702883409354663,0.24246590056907166,0.2725788843827847) [ #4 #3 #13 ]
	  #7: (0.8347634843041519,0.8062882106607906,0.4487835772105288,0.2878766278010768,0.5437271590139189) [ #1 #6 #13 ]
	  #9: (0.2792556394362067,0.49704205416365566,0.743871222563892,0.3972754641067715,0.2777477750312192) [ #8 #3 #11 ]
	  #11: (0.20249484868507361,0.3570299175476694,0.5423429177782412,0.7585218745771884,0.5071456873217323) [ #10 #9 #0 ]
	  #13: (0.8004764172180371,0.8414448974264562,0.6092570989566395,0.26376019915344845,0.5062335974478216) [ #5 #7 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 1.027572139
	Number of best updates: 12

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 10518

	Init times:
		Bottleneck Steiner distances: 0.000023412
		Smallest sphere distances: 0.000001137
		Sorting: 0.000002216
		Total init time: 0.000027845

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000716, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003434, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031195, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000350575, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004319419, bsd pruned: 0, ss pruned: 945

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 9450
	Total number iterations 148532 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 15.717671957671957
	Total time: 1.018327907
	Avarage time pr. topology: 0.000107759

Data for the geometric median iteration:
	Total time: 0.965728557
	Total steps: 5329890
	Average number of steps pr. geometric median: 94.0015873015873
	Average time pr. step: 0.0000001811910859323551
	Average time pr. geometric median: 0.000017032249682539684

