All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.926905288081608
MST len: 4.20020788556859
Steiner Ratio: 0.934931173662614
Solution Steiner tree:
	Terminals:
	  #0: (0.6424573714,0.3284930686,0.8837608927,0.2698044499,0.5980566296) [ #11 ]
	  #1: (0.6100173786,0.6899730524,0.429892563,0.1510021729,0.9164035199) [ #7 ]
	  #2: (0.3121092172,0.450471332,0.8071258347,0.3703712553,0.6420550997) [ #5 ]
	  #4: (0.1465295028,0.1651677811,0.6398503048,0.8193347011,0.9970282237) [ #9 ]
	  #6: (0.7902449909,0.9976388258,0.2066641954,0.6740058836,0.2674432752) [ #13 ]
	  #8: (0.1806596616,0.0587470937,0.758170273,0.3551814539,0.8781711272) [ #9 ]
	  #10: (0.9574638638,0.1660335614,0.7214625924,0.59228968,0.9251535474) [ #11 ]
	  #12: (0.2394123931,0.7611420498,0.1745217923,0.8194240331,0.0320747178) [ #13 ]
	Steiner points:
	  #3: (0.5065352560961406,0.4304632834856496,0.7299837593871005,0.35379259836902544,0.6799861255105467) [ #7 #11 #5 ]
	  #5: (0.35433845967146155,0.4059060874239802,0.7756092817663028,0.3777854229194308,0.682029459068584) [ #9 #2 #3 ]
	  #7: (0.5621119162335684,0.609191498093016,0.5277066299402785,0.33290764194886485,0.6945363499575299) [ #3 #1 #13 ]
	  #9: (0.21781374426959177,0.16952712536545342,0.7378789377618453,0.4579914342399387,0.8530035675941371) [ #8 #5 #4 ]
	  #11: (0.6280199635251028,0.34835495832499747,0.8040093305123686,0.34195331451643374,0.6699673451937632) [ #10 #3 #0 ]
	  #13: (0.5929267992395796,0.8503425599384249,0.2671761685636615,0.6396447611975303,0.2961325224951606) [ #6 #7 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.740303658
	Number of best updates: 8

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 7683

	Init times:
		Bottleneck Steiner distances: 0.000010258
		Smallest sphere distances: 0.000001136
		Sorting: 0.000002698
		Total init time: 0.000014894

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000715, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003417, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031529, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000351154, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.003322781, bsd pruned: 0, ss pruned: 3780

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 6615
	Total number iterations 107986 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 16.324414210128495
	Total time: 0.732607955
	Avarage time pr. topology: 0.000110749

Data for the geometric median iteration:
	Total time: 0.694485053
	Total steps: 3831644
	Average number of steps pr. geometric median: 96.53927941546989
	Average time pr. step: 0.00000018124988986450724
	Average time pr. geometric median: 0.00001749773376165281

