All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.701049169771361
MST len: 9.1400912932
Steiner Ratio: 0.8425571389533877
Solution Steiner tree:
	Terminals:
	  #0: (0.7515285484,0.101288803,0.0099073653,0.073051563,0.7007883027) [ #11 ]
	  #1: (0.0983172874,0.3055314134,0.1783176238,0.3670024752,0.0997784725) [ #13 ]
	  #2: (0.0083380486,0.2498201687,0.6923724262,0.3582976681,0.8231695224) [ #5 ]
	  #4: (0.1180784167,0.5551794663,0.3833639991,0.2886057418,0.8238008194) [ #5 ]
	  #6: (0.2154374701,0.0895364271,0.3215230151,0.5994994997,0.9209823329) [ #9 ]
	  #8: (0.9308896982,0.0813896121,0.9506084719,0.623262124,0.4396872802) [ #9 ]
	  #10: (0.5895236719,0.2716148031,0.0401816108,0.8858513329,0.9203709387) [ #11 ]
	  #12: (0.3094491373,0.8584360545,0.6783925484,0.0389199243,0.0333043621) [ #13 ]
	Steiner points:
	  #3: (0.24677469425800255,0.3055314134,0.3215230151,0.3670024752,0.7007883026991245) [ #7 #5 #13 ]
	  #5: (0.1180784167,0.3055314134,0.3833639991,0.3582976681,0.8231695224) [ #2 #4 #3 ]
	  #7: (0.24677469425800255,0.1580641363671973,0.3215230151,0.3670024752,0.7007883026991245) [ #9 #3 #11 ]
	  #9: (0.24677469425800255,0.0895364271,0.3215242149829973,0.5994994997,0.7007883026991245) [ #6 #7 #8 ]
	  #11: (0.5895236719,0.1580641363671973,0.0401816108,0.3670024752,0.7007883026991245) [ #10 #7 #0 ]
	  #13: (0.24677469425800255,0.3055335225557768,0.3215230150999999,0.3670012236691645,0.0997784725) [ #1 #3 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.251815049
	Number of best updates: 13

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 10518

	Init times:
		Bottleneck Steiner distances: 0.000030331
		Smallest sphere distances: 0.000003681
		Sorting: 0.000024239
		Total init time: 0.000059126

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001231, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000008513, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000078446, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000898912, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.010978131, bsd pruned: 0, ss pruned: 945

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 9450
	Total number iterations 19183 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.02994708994709
	Total time: 0.226992971
	Avarage time pr. topology: 0.00002402

Data for the geometric median iteration:
	Total time: 0.185037188
	Total steps: 335974
	Average number of steps pr. geometric median: 5.9254673721340385
	Average time pr. step: 0.0000005507485341127587
	Average time pr. geometric median: 0.000003263442469135802

Data for the geometric median step function:
	Total number of fixed points encountered: 1025272

