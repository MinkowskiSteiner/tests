All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 6.653999492203308
MST len: 8.154761088099999
Steiner Ratio: 0.8159649829488312
Solution Steiner tree:
	Terminals:
	  #0: (0.2899153434,0.1948941383,0.2638538039,0.6694181327,0.2983180547) [ #9 ]
	  #1: (0.5953307052,0.7999637326,0.8187845688,0.6298717496,0.5035535896) [ #13 ]
	  #2: (0.2370779334,0.3946801244,0.8697485588,0.2358092783,0.2918461092) [ #3 ]
	  #4: (0.859679718,0.8993852078,0.1680767476,0.4144257491,0.4769978577) [ #11 ]
	  #6: (0.229967075,0.8697586934,0.5547460311,0.5776126504,0.9940509931) [ #7 ]
	  #8: (0.1621277403,0.0097564538,0.2769615898,0.9809123091,0.6396282034) [ #9 ]
	  #10: (0.6015800124,0.5553159199,0.0093155527,0.05122513,0.3029992102) [ #11 ]
	  #12: (0.7460086135,0.8155644125,0.995645286,0.5080417518,0.6990755753) [ #13 ]
	Steiner points:
	  #3: (0.24731947570000284,0.39468012439999994,0.5547476493636251,0.5776039704868718,0.50775059460635) [ #7 #9 #2 ]
	  #5: (0.6455566746333324,0.8104974255854047,0.5547476493636251,0.5776039704868718,0.50775059460635) [ #11 #7 #13 ]
	  #7: (0.24731947570000284,0.8104974255854047,0.5547476493636251,0.5776039704868718,0.50775059460635) [ #3 #6 #5 ]
	  #9: (0.2899153434,0.1948941383,0.2769615898,0.6694181327,0.50775059460635) [ #3 #8 #0 ]
	  #11: (0.6455566746333324,0.8104974255854047,0.1680767476,0.41442574909999996,0.4769978577) [ #4 #5 #10 ]
	  #13: (0.6455566746333324,0.8104974255854047,0.8187845688,0.5776039704868718,0.50775059460635) [ #1 #5 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.205838813
	Number of best updates: 11

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 8628

	Init times:
		Bottleneck Steiner distances: 0.000023365
		Smallest sphere distances: 0.00000442
		Sorting: 0.000012601
		Total init time: 0.000042578

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001733, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000008594, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000080174, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000909227, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.009442192, bsd pruned: 0, ss pruned: 2835

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 7560
	Total number iterations 15269 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.019708994708995
	Total time: 0.184964438
	Avarage time pr. topology: 0.000024466

Data for the geometric median iteration:
	Total time: 0.151421362
	Total steps: 259807
	Average number of steps pr. geometric median: 5.7276675485008814
	Average time pr. step: 0.0000005828224874618467
	Average time pr. geometric median: 0.0000033382134479717812

