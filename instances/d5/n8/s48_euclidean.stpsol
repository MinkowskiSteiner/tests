All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.4474035230062734
MST len: 3.747865414936416
Steiner Ratio: 0.9198311949162561
Solution Steiner tree:
	Terminals:
	  #0: (0.4162855211,0.0445806133,0.6728030828,0.7459352001,0.9519015979) [ #5 ]
	  #1: (0.5749283333,0.3027468763,0.1342774458,0.6345026189,0.0329879546) [ #11 ]
	  #2: (0.471497888,0.0372633706,0.2766019526,0.4778683178,0.3804945626) [ #3 ]
	  #4: (0.4328879981,0.4879451569,0.8024439862,0.3533975968,0.9678474162) [ #5 ]
	  #6: (0.0184599767,0.1722892593,0.6367555897,0.2241443257,0.7155450055) [ #9 ]
	  #8: (0.0874567135,0.0527966507,0.6832414389,0.2217341593,0.6872992696) [ #9 ]
	  #10: (0.4224220134,0.736733945,0.2257205198,0.9471432748,0.0564415637) [ #11 ]
	  #12: (0.6838657743,0.3087545369,0.0063704299,0.343231192,0.8108547613) [ #13 ]
	Steiner points:
	  #3: (0.47150054630520166,0.037275842140192306,0.2765998318978523,0.4778697059331815,0.3804969405495118) [ #11 #2 #13 ]
	  #5: (0.36995476553411333,0.2122198963057231,0.6474578210486398,0.47553874412566005,0.8538159264417108) [ #0 #7 #4 ]
	  #7: (0.32066758847564564,0.17121345042655234,0.5686555036165475,0.40175687256975595,0.7585272737408426) [ #9 #5 #13 ]
	  #9: (0.08956185619378394,0.11297785790374772,0.6514561770806634,0.24465537239047766,0.706808559119084) [ #6 #8 #7 ]
	  #11: (0.5675818050587788,0.3063317904787449,0.14116728003102127,0.6381560640386913,0.044378166231314076) [ #10 #3 #1 ]
	  #13: (0.4741595004852139,0.1486284065147367,0.30629546260390356,0.41944452173661084,0.6117634212444106) [ #3 #7 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.771772702
	Number of best updates: 12

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 9573

	Init times:
		Bottleneck Steiner distances: 0.000022971
		Smallest sphere distances: 0.000001086
		Sorting: 0.000002389
		Total init time: 0.000027412

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000999, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003437, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031161, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000347725, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.003976477, bsd pruned: 0, ss pruned: 1890

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 8505
	Total number iterations 132474 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 15.576014109347442
	Total time: 0.763095792
	Avarage time pr. topology: 0.000089723

Data for the geometric median iteration:
	Total time: 0.71595181
	Total steps: 3914552
	Average number of steps pr. geometric median: 76.71079757005683
	Average time pr. step: 0.00000018289495451842253
	Average time pr. geometric median: 0.00001403001783264746

