All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 6.717011736805525
MST len: 7.9892846162
Steiner Ratio: 0.8407525904366122
Solution Steiner tree:
	Terminals:
	  #0: (0.6140612409,0.9471684238,0.6104576749,0.9758801474,0.721258364) [ #3 ]
	  #1: (0.5578971945,0.2818813474,0.2621717841,0.5012496633,0.921708364) [ #9 ]
	  #2: (0.5803137503,0.0846754555,0.4055949428,0.8424855344,0.5859251188) [ #13 ]
	  #4: (0.7275817463,0.4842158102,0.9432684327,0.3444002356,0.4630026335) [ #7 ]
	  #6: (0.6899727819,0.193846746,0.6994581058,0.1578644305,0.7714722398) [ #11 ]
	  #8: (0.5835098287,0.7793133598,0.1173111168,0.526778261,0.1237135954) [ #9 ]
	  #10: (0.4597480886,0.253197706,0.9255737224,0.0517316135,0.6330953071) [ #11 ]
	  #12: (0.0351164541,0.1380414735,0.5359635696,0.9014610839,0.1763991002) [ #13 ]
	Steiner points:
	  #3: (0.5578971945,0.2818813474,0.5856263064717558,0.8621440509,0.4630026333472537) [ #5 #0 #13 ]
	  #5: (0.5578971945,0.2818813474,0.5856263064717558,0.526778261,0.46300263334723785) [ #9 #7 #3 ]
	  #7: (0.6132312176804396,0.2818813474,0.7748299780000001,0.3444002356,0.4630026335) [ #5 #4 #11 ]
	  #9: (0.5578971945,0.2818813474,0.2621717841,0.526778261,0.46300263334723785) [ #5 #8 #1 ]
	  #11: (0.6132312176804396,0.253197706,0.7748299780000001,0.1578644305,0.6330953071) [ #6 #7 #10 ]
	  #13: (0.5578971945,0.1380414735,0.5359635696,0.8621440509,0.4630026333472537) [ #2 #3 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.21007935
	Number of best updates: 13

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 8628

	Init times:
		Bottleneck Steiner distances: 0.000018544
		Smallest sphere distances: 0.000004014
		Sorting: 0.000011724
		Total init time: 0.000035292

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001602, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000008839, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000080171, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000901624, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.00941162, bsd pruned: 0, ss pruned: 2835

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 7560
	Total number iterations 15286 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.021957671957672
	Total time: 0.189205582
	Avarage time pr. topology: 0.000025027

Data for the geometric median iteration:
	Total time: 0.155604778
	Total steps: 267433
	Average number of steps pr. geometric median: 5.895789241622575
	Average time pr. step: 0.0000005818458380229814
	Average time pr. geometric median: 0.0000034304404320987652

