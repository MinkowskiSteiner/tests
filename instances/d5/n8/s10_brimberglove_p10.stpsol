All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.7010051082308575
MST len: 9.5016061427
Steiner Ratio: 0.8104950881538562
Solution Steiner tree:
	Terminals:
	  #0: (0.1834716542,0.584652962,0.4221560766,0.0253341845,0.3162595943) [ #7 ]
	  #1: (0.2144171056,0.0119858943,0.8683905582,0.3317870611,0.5361120061) [ #5 ]
	  #2: (0.5658107323,0.6109299178,0.5057680786,0.1796468744,0.8166862739) [ #3 ]
	  #4: (0.0530768098,0.2689884604,0.0923382296,0.8809632025,0.5625731631) [ #13 ]
	  #6: (0.6635139085,0.9814409283,0.9619104471,0.1394470134,0.2234422426) [ #11 ]
	  #8: (0.9637281978,0.2458365472,0.6618976293,0.3858842744,0.2711707318) [ #9 ]
	  #10: (0.5565968144,0.8975977934,0.1470419239,0.0623648931,0.0772446674) [ #11 ]
	  #12: (0.0612761933,0.083659018,0.9767909204,0.9780582851,0.8738890034) [ #13 ]
	Steiner points:
	  #3: (0.27469694946946127,0.584652962,0.5057680786,0.3858842743999323,0.5361120129878374) [ #7 #9 #2 ]
	  #5: (0.21441710560000002,0.2458365472,0.6618976293,0.3858842744,0.5361120129878374) [ #9 #1 #13 ]
	  #7: (0.27469694946946127,0.584652962,0.5057680786,0.1137529733481645,0.3162595943) [ #11 #3 #0 ]
	  #9: (0.21441711335691213,0.2458365472,0.6618976293,0.3858842744,0.5361120129878374) [ #5 #8 #3 ]
	  #11: (0.5565968144,0.8975977934,0.5057680786,0.1137529733481645,0.2234422426) [ #6 #7 #10 ]
	  #13: (0.061276193300000004,0.24583654720000003,0.6618976293,0.8809632025,0.5625731631) [ #4 #5 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.20623848
	Number of best updates: 10

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 8628

	Init times:
		Bottleneck Steiner distances: 0.000018722
		Smallest sphere distances: 0.00000379
		Sorting: 0.000020779
		Total init time: 0.000044268

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001257, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000008606, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000078301, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000883924, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.00923451, bsd pruned: 945, ss pruned: 1890

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 7560
	Total number iterations 15377 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.033994708994709
	Total time: 0.185142796
	Avarage time pr. topology: 0.000024489

Data for the geometric median iteration:
	Total time: 0.151496439
	Total steps: 276356
	Average number of steps pr. geometric median: 6.092504409171076
	Average time pr. step: 0.0000005481930517159027
	Average time pr. geometric median: 0.0000033398685846560846

Data for the geometric median step function:
	Total number of fixed points encountered: 811618

