All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.41491915558193
MST len: 6.4266928318
Steiner Ratio: 0.8425669776511334
Solution Steiner tree:
	Terminals:
	  #0: (0.1287793886,0.8487633443,0.6536876334,0.510914339,0.4453349446) [ #3 ]
	  #1: (0.022327458,0.7685739201,0.5612500564,0.4878506383,0.4318964698) [ #3 ]
	  #2: (0.6702460561,0.9116797088,0.38213495,0.5965716008,0.2660512897) [ #7 ]
	  #4: (0.3927549945,0.8972157686,0.7339704534,0.1277633953,0.3233618859) [ #5 ]
	  #6: (0.7889988608,0.387681659,0.2726833854,0.3569481244,0.0666660061) [ #9 ]
	  #8: (0.9197389232,0.533241797,0.2139088648,0.4809889791,0.0210924349) [ #9 ]
	  #10: (0.0549102435,0.9953047172,0.408344438,0.1785172881,0.7420079246) [ #11 ]
	  #12: (0.8467889367,0.5935844293,0.9065222973,0.5735588286,0.6033617564) [ #13 ]
	Steiner points:
	  #3: (0.1287793886,0.7685739201,0.5719065682666666,0.4878506382999999,0.4453349446) [ #0 #1 #11 ]
	  #5: (0.3927549945,0.7685739201,0.5719065682666666,0.4446296642847375,0.32336188590000003) [ #11 #4 #7 ]
	  #7: (0.6702460561,0.7685739201,0.38213495,0.4446296642847375,0.2660512897000002) [ #5 #2 #13 ]
	  #9: (0.7889988608,0.533241797,0.2726833854,0.4809889766307517,0.0666660061) [ #6 #8 #13 ]
	  #11: (0.1287793886,0.7685739201,0.5719065682666666,0.4446296642847375,0.4453349446) [ #10 #5 #3 ]
	  #13: (0.7889988608,0.5935844293,0.38213495,0.4809889766307517,0.2660512897000002) [ #7 #9 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.263174042
	Number of best updates: 26

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 11043

	Init times:
		Bottleneck Steiner distances: 0.000026797
		Smallest sphere distances: 0.000004036
		Sorting: 0.000011416
		Total init time: 0.000043322

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001654, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000008463, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000079303, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000897508, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.011783573, bsd pruned: 0, ss pruned: 420

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 9975
	Total number iterations 20320 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0370927318295737
	Total time: 0.237697439
	Avarage time pr. topology: 0.000023829

Data for the geometric median iteration:
	Total time: 0.193562234
	Total steps: 335450
	Average number of steps pr. geometric median: 5.60484544695071
	Average time pr. step: 0.0000005770226084364286
	Average time pr. geometric median: 0.0000032341225396825398

