All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 6.461368444602814
MST len: 7.9688970579000005
Steiner Ratio: 0.8108234298493426
Solution Steiner tree:
	Terminals:
	  #0: (0.9164578756,0.1339819669,0.1912108968,0.2600808285,0.2142811954) [ #9 ]
	  #1: (0.2335532183,0.158582164,0.7489994023,0.9178017638,0.3488313283) [ #5 ]
	  #2: (0.3601613163,0.4198029756,0.654078258,0.1943656375,0.3097259371) [ #3 ]
	  #4: (0.0087156049,0.4163471956,0.8781052776,0.6627938634,0.6107128331) [ #11 ]
	  #6: (0.3430800132,0.2595131855,0.6990013098,0.2817635985,0.6604524426) [ #7 ]
	  #8: (0.8649749825,0.1982214736,0.79443441,0.0561858788,0.458302302) [ #9 ]
	  #10: (0.29734262,0.6824838476,0.0102189095,0.7238869563,0.0950620273) [ #11 ]
	  #12: (0.5068725648,0.1146662767,0.5706952487,0.6701787769,0.9271462466) [ #13 ]
	Steiner points:
	  #3: (0.36016131630000003,0.1982214736,0.6540782441485946,0.2600808285,0.45830230199999994) [ #2 #9 #7 ]
	  #5: (0.2335532183,0.17036974133488234,0.7489994023,0.6831582276999995,0.34883132854701593) [ #11 #1 #13 ]
	  #7: (0.36016131630000003,0.1982214736,0.658642425643995,0.2817635985,0.5416029678980107) [ #6 #3 #13 ]
	  #9: (0.8649749825,0.1982214736,0.6540782441485946,0.2600808285,0.458302302) [ #3 #8 #0 ]
	  #11: (0.2335532183,0.4163471956,0.7489994023,0.6831582276999995,0.34883132854701593) [ #10 #5 #4 ]
	  #13: (0.36016131630000003,0.17036974133488234,0.658642425643995,0.6701787768999975,0.5416029678980107) [ #5 #7 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.359534883
	Number of best updates: 12

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 11463

	Init times:
		Bottleneck Steiner distances: 0.000077701
		Smallest sphere distances: 0.000023288
		Sorting: 0.000030661
		Total init time: 0.000135454

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002764, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000011996, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000107348, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001204903, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.016102845, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 10395
	Total number iterations 21111 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0308802308802307
	Total time: 0.32316477
	Avarage time pr. topology: 0.000031088

Data for the geometric median iteration:
	Total time: 0.260513969
	Total steps: 347140
	Average number of steps pr. geometric median: 5.565816899150232
	Average time pr. step: 0.0000007504579391599931
	Average time pr. geometric median: 0.000004176911479878147

Data for the geometric median step function:
	Total number of fixed points encountered: 1109838

