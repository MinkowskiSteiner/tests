All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.43900043475973
MST len: 4.957936474000163
Steiner Ratio: 0.8953322532545994
Solution Steiner tree:
	Terminals:
	  #0: (0.1447218867,0.6269470568,0.4722914819,0.0229840363,0.4052342835) [ #3 ]
	  #1: (0.3870529828,0.7761541152,0.6623876005,0.1703382573,0.3191112491) [ #7 ]
	  #2: (0.2226562315,0.5511224272,0.0462225136,0.6177704891,0.4252223039) [ #13 ]
	  #4: (0.6132100036,0.1134380932,0.7647564228,0.4129372162,0.5164472929) [ #9 ]
	  #6: (0.5296626759,0.9438327159,0.87826215,0.7782872272,0.4166978609) [ #15 ]
	  #8: (0.8056346727,0.6451566176,0.2473998085,0.3098868776,0.9831179767) [ #11 ]
	  #10: (0.8498845724,0.2185718884,0.16160391,0.0972843804,0.5284587664) [ #11 ]
	  #12: (0.3110007436,0.691104038,0.1034285073,0.9779993249,0.8022396713) [ #13 ]
	  #14: (0.1354387687,0.9625167521,0.799727213,0.4030091993,0.0851281491) [ #15 ]
	Steiner points:
	  #3: (0.30490103508709093,0.6503599756600449,0.5164355673123843,0.16299566907879048,0.39738697034777576) [ #7 #0 #5 ]
	  #5: (0.4157959349442542,0.5156389447225169,0.38365211504383495,0.33427396121126957,0.4903008591749287) [ #9 #13 #3 ]
	  #7: (0.38317664586843503,0.7750406401961897,0.6604938990424596,0.174888073445284,0.31965885087895685) [ #1 #3 #15 ]
	  #9: (0.5754117871531332,0.38859849755822556,0.4244605125538767,0.3135513303048452,0.559588956180044) [ #11 #5 #4 ]
	  #11: (0.7107944509711243,0.3951484889961278,0.3046420690854258,0.24713675030404997,0.6460801474232857) [ #10 #8 #9 ]
	  #13: (0.2678563056084182,0.5655542308137637,0.11105240553894825,0.6227275041594164,0.4909929976153547) [ #12 #5 #2 ]
	  #15: (0.30798342716880367,0.8927104509113973,0.7679649941751335,0.40455727836988825,0.23942636484432273) [ #6 #7 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 12.151021696
	Number of best updates: 22

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 115413

	Init times:
		Bottleneck Steiner distances: 0.000022504
		Smallest sphere distances: 0.000001258
		Sorting: 0.000003161
		Total init time: 0.000028042

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000886, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003484, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031888, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000353131, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004738869, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.05884165, bsd pruned: 0, ss pruned: 31185

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 103950
	Total number iterations 1746423 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 16.80060606060606
	Total time: 12.03168413
	Avarage time pr. topology: 0.000115744

Data for the geometric median iteration:
	Total time: 11.321173969
	Total steps: 62247240
	Average number of steps pr. geometric median: 85.54557823129252
	Average time pr. step: 0.00000018187431232292387
	Average time pr. geometric median: 0.000015558543213083214

