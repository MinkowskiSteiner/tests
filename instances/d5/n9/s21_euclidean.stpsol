All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.701663456503365
MST len: 5.184772974683557
Steiner Ratio: 0.9068214711542548
Solution Steiner tree:
	Terminals:
	  #0: (0.8250681748,0.3494697229,0.1410814236,0.1587108039,0.0477595204) [ #7 ]
	  #1: (0.6112220658,0.052807556,0.6665685525,0.8844101391,0.2439990897) [ #5 ]
	  #2: (0.5058995711,0.1543681692,0.3336426291,0.698289798,0.0904165954) [ #3 ]
	  #4: (0.4821770696,0.3179936159,0.5119722684,0.8435701522,0.9867132548) [ #9 ]
	  #6: (0.1642170884,0.3632087118,0.3869168281,0.0459364807,0.2802418788) [ #15 ]
	  #8: (0.2314980189,0.7699328697,0.1005670764,0.8980665714,0.6543430084) [ #13 ]
	  #10: (0.3445661666,0.0622836598,0.0175517202,0.7314829946,0.1082201405) [ #11 ]
	  #12: (0.9582093637,0.8222869392,0.1893208209,0.7204574974,0.7427683257) [ #13 ]
	  #14: (0.1041107807,0.2645267724,0.9979975852,0.0011071404,0.3400666059) [ #15 ]
	Steiner points:
	  #3: (0.5058981747984459,0.15436885688543447,0.3336407399697687,0.698287550275889,0.09042563795816914) [ #2 #11 #5 ]
	  #5: (0.5546251297977552,0.16380359781522993,0.4873388840689971,0.8028229352422889,0.2772853338281787) [ #1 #9 #3 ]
	  #7: (0.5118605711697835,0.2767335567126237,0.26012850473548765,0.31087150984633005,0.13162130207937597) [ #0 #11 #15 ]
	  #9: (0.5247071042958066,0.4224858162941819,0.3910152169640441,0.8248504590606947,0.704718886467414) [ #13 #5 #4 ]
	  #11: (0.4707622356334581,0.15735459852230438,0.24830028479483574,0.6307192975510332,0.10240725817407048) [ #7 #10 #3 ]
	  #13: (0.5412936250351212,0.6143865937163269,0.26128593521161875,0.8216015316391292,0.6991317779241863) [ #8 #9 #12 ]
	  #15: (0.1908239765451847,0.3489050640029862,0.41705929744655046,0.06627935367617575,0.27117541217162744) [ #6 #7 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 10.997680138
	Number of best updates: 17

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 94623

	Init times:
		Bottleneck Steiner distances: 0.000021998
		Smallest sphere distances: 0.000001362
		Sorting: 0.000003496
		Total init time: 0.000028233

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000937, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00000355, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032019, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000354765, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004734947, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.050431058, bsd pruned: 0, ss pruned: 51975

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 83160
	Total number iterations 1579771 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 18.996765271765273
	Total time: 10.890628913
	Avarage time pr. topology: 0.000130959

Data for the geometric median iteration:
	Total time: 10.251112316
	Total steps: 56362517
	Average number of steps pr. geometric median: 96.82284924070639
	Average time pr. step: 0.00000018187818539047858
	Average time pr. geometric median: 0.000017609964124235556

