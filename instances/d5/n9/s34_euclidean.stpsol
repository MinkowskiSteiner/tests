All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.728567913919134
MST len: 3.918408750045443
Steiner Ratio: 0.9515515485401804
Solution Steiner tree:
	Terminals:
	  #0: (0.2319139071,0.0643970538,0.576686004,0.5689670525,0.5896822263) [ #5 ]
	  #1: (0.4875804249,0.2520150534,0.5079065824,0.3625599515,0.8518746006) [ #3 ]
	  #2: (0.2945063893,0.4759735886,0.6125465755,0.2276748625,0.680792569) [ #9 ]
	  #4: (0.1689700215,0.0285875835,0.7728105293,0.3646502115,0.237839454) [ #5 ]
	  #6: (0.9126800242,0.2932653806,0.3414342214,0.5614015812,0.7468854965) [ #11 ]
	  #8: (0.206476939,0.6585222355,0.9598705587,0.8190235145,0.886197098) [ #15 ]
	  #10: (0.8561092638,0.1213370185,0.3546023389,0.3640158462,0.48389697) [ #11 ]
	  #12: (0.8609460652,0.737937074,0.6182698098,0.6337565941,0.1025872855) [ #13 ]
	  #14: (0.0897512576,0.6359386517,0.9450358706,0.5259626366,0.9567871433) [ #15 ]
	Steiner points:
	  #3: (0.438472316276363,0.29197220615599084,0.5503136938714779,0.38462442340175695,0.7392481321318761) [ #9 #1 #7 ]
	  #5: (0.26510520236682666,0.09528490819963235,0.5963072508268645,0.5157910850669315,0.5582491473113127) [ #4 #0 #7 ]
	  #7: (0.45677487348200585,0.2568999718116303,0.5433299834274103,0.42597927205228975,0.6697157939046414) [ #13 #3 #5 ]
	  #9: (0.3161047034655816,0.44097667511818317,0.6344307232283273,0.31869854187287644,0.7294626520905749) [ #3 #2 #15 ]
	  #11: (0.8107944454371737,0.2527061018012657,0.4018973673638481,0.4647156169213463,0.5761078762760022) [ #10 #13 #6 ]
	  #13: (0.732964977346049,0.30904101244176796,0.45995521569116593,0.47485442703752245,0.5441901735163304) [ #7 #11 #12 ]
	  #15: (0.14055278534217777,0.6176784713455966,0.9115659107202679,0.562265687991943,0.9154251603479069) [ #8 #9 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 9.160544864
	Number of best updates: 23

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 84228

	Init times:
		Bottleneck Steiner distances: 0.000021293
		Smallest sphere distances: 0.00000129
		Sorting: 0.000003613
		Total init time: 0.000027356

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000765, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003589, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032157, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000355352, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004736748, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.045824341, bsd pruned: 10395, ss pruned: 51975

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 72765
	Total number iterations 1323236 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 18.185061499347213
	Total time: 9.060500963
	Avarage time pr. topology: 0.000124516

Data for the geometric median iteration:
	Total time: 8.523852766
	Total steps: 46558413
	Average number of steps pr. geometric median: 91.40660835762877
	Average time pr. step: 0.00000018307867937852605
	Average time pr. geometric median: 0.000016734601144584818

