All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.546112382855498
MST len: 5.094663783014521
Steiner Ratio: 0.8923282431339474
Solution Steiner tree:
	Terminals:
	  #0: (0.8449453608,0.1197466637,0.0576706543,0.2857596349,0.6257388632) [ #7 ]
	  #1: (0.0583464685,0.4434102766,0.4754556192,0.1466605273,0.2289445811) [ #11 ]
	  #2: (0.8119810777,0.9184383005,0.2771394566,0.1947866791,0.8602793272) [ #5 ]
	  #4: (0.3481050377,0.7246325052,0.5291235137,0.6861238385,0.9269996448) [ #15 ]
	  #6: (0.8194229132,0.5062902763,0.0883140588,0.7855343049,0.3411775247) [ #7 ]
	  #8: (0.35034057,0.4464217548,0.0208598026,0.0373252058,0.1292244392) [ #9 ]
	  #10: (0.0076622083,0.4441131705,0.5998500383,0.0285220109,0.4814383762) [ #11 ]
	  #12: (0.8166331438,0.958641605,0.1473828816,0.0937726004,0.1534282836) [ #13 ]
	  #14: (0.607333408,0.320081121,0.7510317898,0.6239873639,0.9691653433) [ #15 ]
	Steiner points:
	  #3: (0.7001284274820953,0.5803540530074426,0.19691778786265743,0.35267012782809104,0.5072269858498782) [ #7 #5 #13 ]
	  #5: (0.6836495971020753,0.6987343937765836,0.31567434158891,0.3646977388224977,0.7270406319039721) [ #2 #3 #15 ]
	  #7: (0.7608652878978203,0.4598331141513178,0.13996899147568703,0.44071960548038536,0.4941917789575296) [ #6 #3 #0 ]
	  #9: (0.3667756391118678,0.5046832049864793,0.15620682680131046,0.10375841994119264,0.20447026417630218) [ #8 #11 #13 ]
	  #11: (0.06905380154254107,0.4462660869526107,0.4692386160658848,0.13670261059229913,0.2449031293729623) [ #10 #9 #1 ]
	  #13: (0.6211033475894995,0.6563520290432455,0.170242931012383,0.20131484185846568,0.3127455733339338) [ #3 #9 #12 ]
	  #15: (0.5063243334852369,0.6077454910349662,0.5337682378778961,0.5852424743312137,0.8862413442435387) [ #4 #5 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 10.583639756
	Number of best updates: 14

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 105018

	Init times:
		Bottleneck Steiner distances: 0.000022286
		Smallest sphere distances: 0.000001236
		Sorting: 0.000003145
		Total init time: 0.000027647

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000948, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003543, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031887, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000355705, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004699909, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.054806583, bsd pruned: 0, ss pruned: 41580

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 93555
	Total number iterations 1537359 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 16.4326759660093
	Total time: 10.470104532
	Avarage time pr. topology: 0.000111912

Data for the geometric median iteration:
	Total time: 9.842263748
	Total steps: 53953283
	Average number of steps pr. geometric median: 82.38588912557167
	Average time pr. step: 0.00000018242196212601189
	Average time pr. geometric median: 0.000015028995545782848

