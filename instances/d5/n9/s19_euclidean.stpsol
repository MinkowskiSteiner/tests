All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.537151199032921
MST len: 5.042946707376836
Steiner Ratio: 0.8997023887632928
Solution Steiner tree:
	Terminals:
	  #0: (0.6696780285,0.3323929949,0.5761897869,0.273373468,0.0765914861) [ #5 ]
	  #1: (0.9196610394,0.6276440698,0.3077861812,0.506227026,0.5700069734) [ #7 ]
	  #2: (0.3123343211,0.2170182561,0.784748353,0.4766765621,0.2870572444) [ #3 ]
	  #4: (0.0970141232,0.4512588286,0.1844230174,0.3255779349,0.5106765579) [ #15 ]
	  #6: (0.5252522843,0.8965011402,0.6247255405,0.1610254427,0.8566360394) [ #9 ]
	  #8: (0.6874572605,0.9015435013,0.9686559904,0.617568895,0.5169671907) [ #11 ]
	  #10: (0.4063816287,0.9952559634,0.8430695528,0.9825714156,0.2686294314) [ #11 ]
	  #12: (0.6450726547,0.0391567499,0.586565987,0.9423629041,0.7965240673) [ #13 ]
	  #14: (0.2902333854,0.3277869785,0.2285638117,0.0594177293,0.2219586108) [ #15 ]
	Steiner points:
	  #3: (0.470255256796211,0.31367774429779555,0.6093004614285401,0.42772399069535866,0.32959996077247455) [ #5 #13 #2 ]
	  #5: (0.48685482493632154,0.32847919170858486,0.5333682445683201,0.33638704696329613,0.25468586888018213) [ #3 #0 #15 ]
	  #7: (0.7038839975452678,0.5871586261003847,0.5380178316389176,0.5066479596759362,0.5701591675747963) [ #9 #13 #1 ]
	  #9: (0.6430506385700252,0.756359827944994,0.6733941468418458,0.46050559174670724,0.6209157415901717) [ #11 #7 #6 ]
	  #11: (0.6492573664856484,0.8880190313730241,0.9063805649668031,0.6319293531415948,0.5067185817647248) [ #10 #8 #9 ]
	  #13: (0.6044980160448161,0.3856780969397957,0.5737795975822757,0.5540257689581175,0.5188077477560279) [ #3 #7 #12 ]
	  #15: (0.2909886503237602,0.3591567093287236,0.29425889812355877,0.19649269429854188,0.3031545046892528) [ #4 #5 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 14.64874053
	Number of best updates: 19

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 125808

	Init times:
		Bottleneck Steiner distances: 0.000022815
		Smallest sphere distances: 0.000001253
		Sorting: 0.000003307
		Total init time: 0.000028347

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000945, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003771, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031647, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000352834, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004678787, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.06310929, bsd pruned: 0, ss pruned: 20790

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 114345
	Total number iterations 2107133 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 18.427854300581572
	Total time: 14.523398526
	Avarage time pr. topology: 0.000127013

Data for the geometric median iteration:
	Total time: 13.670548477
	Total steps: 74990547
	Average number of steps pr. geometric median: 93.68958227919266
	Average time pr. step: 0.00000018229695640171822
	Average time pr. geometric median: 0.000017079325696045177

