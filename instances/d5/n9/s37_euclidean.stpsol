All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.279157889430675
MST len: 4.6029864598358365
Steiner Ratio: 0.9296481592481786
Solution Steiner tree:
	Terminals:
	  #0: (0.6424573714,0.3284930686,0.8837608927,0.2698044499,0.5980566296) [ #13 ]
	  #1: (0.6100173786,0.6899730524,0.429892563,0.1510021729,0.9164035199) [ #5 ]
	  #2: (0.3121092172,0.450471332,0.8071258347,0.3703712553,0.6420550997) [ #7 ]
	  #4: (0.8047208254,0.9861151008,0.7179146072,0.6118466596,0.3564863561) [ #9 ]
	  #6: (0.1465295028,0.1651677811,0.6398503048,0.8193347011,0.9970282237) [ #11 ]
	  #8: (0.7902449909,0.9976388258,0.2066641954,0.6740058836,0.2674432752) [ #15 ]
	  #10: (0.1806596616,0.0587470937,0.758170273,0.3551814539,0.8781711272) [ #11 ]
	  #12: (0.9574638638,0.1660335614,0.7214625924,0.59228968,0.9251535474) [ #13 ]
	  #14: (0.2394123931,0.7611420498,0.1745217923,0.8194240331,0.0320747178) [ #15 ]
	Steiner points:
	  #3: (0.5350386473300938,0.4444386997540435,0.7517745456984032,0.3520547933422047,0.6733643858582581) [ #5 #13 #7 ]
	  #5: (0.6026221051762944,0.6245809082831577,0.609586895631791,0.3408401760489439,0.6847981560567985) [ #1 #9 #3 ]
	  #7: (0.34959433055092815,0.4140926435200154,0.7864917116113994,0.3772823036883137,0.6752870397777425) [ #2 #11 #3 ]
	  #9: (0.7279049073814535,0.896644669334602,0.5675971014108582,0.5745549268198824,0.39523825414559055) [ #5 #4 #15 ]
	  #11: (0.21569372569320128,0.16990412159431656,0.7403726465792695,0.45828026640186553,0.8524664040720421) [ #10 #7 #6 ]
	  #13: (0.6335914119922468,0.3580569516225325,0.8110049919413801,0.3400696483937648,0.6659754846632671) [ #12 #3 #0 ]
	  #15: (0.6884850897631427,0.9332843413054186,0.3022186056849186,0.6686025576094947,0.2669539856239858) [ #8 #9 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 10.661842065
	Number of best updates: 19

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 105018

	Init times:
		Bottleneck Steiner distances: 0.000012839
		Smallest sphere distances: 0.000001297
		Sorting: 0.000003296
		Total init time: 0.000018408

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000694, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003447, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031905, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000354388, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004747417, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.054546351, bsd pruned: 0, ss pruned: 41580

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 93555
	Total number iterations 1605941 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 17.16574207685319
	Total time: 10.548720231
	Avarage time pr. topology: 0.000112753

Data for the geometric median iteration:
	Total time: 9.896082584
	Total steps: 54285005
	Average number of steps pr. geometric median: 82.89242386067782
	Average time pr. step: 0.0000001822986400019674
	Average time pr. geometric median: 0.000015111176136268199

