All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.282110477955134
MST len: 8.953003003
Steiner Ratio: 0.8133707176815447
Solution Steiner tree:
	Terminals:
	  #0: (0.4325436914,0.0533801331,0.3409319503,0.5098388086,0.016024685) [ #3 ]
	  #1: (0.163898673,0.2155044015,0.7876827134,0.788697773,0.0666457862) [ #13 ]
	  #2: (0.7888301731,0.5093233755,0.9799026092,0.5532668813,0.3248152781) [ #5 ]
	  #4: (0.1815535189,0.5228272334,0.9403845905,0.7741447132,0.7375370742) [ #7 ]
	  #6: (0.5267799439,0.0893740547,0.7644367087,0.815491903,0.8889724807) [ #11 ]
	  #8: (0.4869041394,0.8679774124,0.5925911942,0.2147098403,0.0102265389) [ #15 ]
	  #10: (0.9506111294,0.2889632747,0.7334853265,0.9825434321,0.8905285545) [ #11 ]
	  #12: (0.2613700387,0.3636968142,0.7594234276,0.035923094,0.0724071777) [ #13 ]
	  #14: (0.514818562,0.9959482527,0.0319323023,0.6015652793,0.0553448466) [ #15 ]
	Steiner points:
	  #3: (0.4325436914,0.3636968142,0.5925911941999998,0.5532668814180322,0.06664571497947587) [ #0 #13 #9 ]
	  #5: (0.5137306080945323,0.5093233755,0.5925911942238509,0.5532668814237363,0.3248152781) [ #9 #2 #7 ]
	  #7: (0.5137306080945323,0.5228272334,0.7644366496650356,0.7741447132,0.7375370742) [ #4 #11 #5 ]
	  #9: (0.49620894693327,0.5093233755,0.5925911941999998,0.5532668814237363,0.06664571497947587) [ #3 #5 #15 ]
	  #11: (0.5267799439,0.2889632747,0.7644366496650356,0.815491903,0.8889724807) [ #7 #10 #6 ]
	  #13: (0.2613700387,0.3636968142,0.7594234276,0.5532668814180322,0.0666457862) [ #12 #3 #1 ]
	  #15: (0.49620894693327,0.8679774124,0.5925869167447505,0.5532668814237363,0.055344846600000004) [ #8 #9 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 2.718208379
	Number of best updates: 22

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 105018

	Init times:
		Bottleneck Steiner distances: 0.000036697
		Smallest sphere distances: 0.000005934
		Sorting: 0.000017216
		Total init time: 0.000061787

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001456, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000008658, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000079453, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000924368, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.012196699, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.140825139, bsd pruned: 31185, ss pruned: 10395

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 93555
	Total number iterations 189349 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.023932446154668
	Total time: 2.411785805
	Avarage time pr. topology: 0.000025778

Data for the geometric median iteration:
	Total time: 1.921812732
	Total steps: 3388429
	Average number of steps pr. geometric median: 5.174082472495171
	Average time pr. step: 0.000000567169249230248
	Average time pr. geometric median: 0.0000029345804713804718

Data for the geometric median step function:
	Total number of fixed points encountered: 10944644

