All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 8.994671341334474
MST len: 10.936694770399999
Steiner Ratio: 0.8224304993569371
Solution Steiner tree:
	Terminals:
	  #0: (0.6756664485,0.1440341534,0.1769533321,0.3554075599,0.0303536835) [ #3 ]
	  #1: (0.4671709633,0.9850149108,0.9476090371,0.391033577,0.2210022007) [ #5 ]
	  #2: (0.7333814817,0.3375514235,0.1884241831,0.0118348948,0.8027626829) [ #3 ]
	  #4: (0.1321992586,0.3741906497,0.6797411119,0.8863195306,0.801372399) [ #13 ]
	  #6: (0.7351341866,0.9038881473,0.5736667344,0.0998372487,0.0471116267) [ #7 ]
	  #8: (0.946517847,0.9674219824,0.2784534135,0.4652112594,0.4872422654) [ #9 ]
	  #10: (0.0357322353,0.1799209691,0.9172431221,0.0559421322,0.0346130878) [ #11 ]
	  #12: (0.2213446718,0.2145956132,0.2002614784,0.9167352235,0.3484674186) [ #13 ]
	  #14: (0.9290727782,0.9129959489,0.9238626137,0.2359872904,0.785772445) [ #15 ]
	Steiner points:
	  #3: (0.7333814817,0.3375514235,0.1884241831,0.3554075598395523,0.4872422654) [ #0 #2 #9 ]
	  #5: (0.4671709633,0.9069240811659457,0.8749838672867053,0.3554075598761895,0.48724226539999943) [ #11 #1 #15 ]
	  #7: (0.7351341866,0.9038881473,0.5736667344,0.3554075598761895,0.4872422654) [ #9 #6 #15 ]
	  #9: (0.7351341866,0.9038881473,0.2784534135,0.3554075598761895,0.4872422654) [ #8 #7 #3 ]
	  #11: (0.16191439633333335,0.32099230420000013,0.8749838672867053,0.3554075598761895,0.48724226539999943) [ #5 #10 #13 ]
	  #13: (0.16191439633333335,0.32099230420000013,0.6797411119,0.8863195306,0.48724226539999943) [ #12 #11 #4 ]
	  #15: (0.7351341866,0.9069240811659457,0.8749838672867053,0.35540664877878975,0.48724454298714276) [ #5 #7 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 3.262229956
	Number of best updates: 22

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 125808

	Init times:
		Bottleneck Steiner distances: 0.000027926
		Smallest sphere distances: 0.00000513
		Sorting: 0.000017558
		Total init time: 0.00005192

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000136, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000008694, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000079525, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000890097, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.012035147, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.160828849, bsd pruned: 10395, ss pruned: 10395

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 114345
	Total number iterations 231893 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.02801171892081
	Total time: 2.915302468
	Avarage time pr. topology: 0.000025494

Data for the geometric median iteration:
	Total time: 2.327219013
	Total steps: 4182069
	Average number of steps pr. geometric median: 5.224875845655067
	Average time pr. step: 0.0000005564755179792586
	Average time pr. geometric median: 0.0000029075154925882203

Data for the geometric median step function:
	Total number of fixed points encountered: 13670486

