All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.498273384298977
MST len: 3.8571892846070206
Steiner Ratio: 0.9069488495831982
Solution Steiner tree:
	Terminals:
	  #0: (0.4541832616,0.7798275243,0.608648137,0.7615917403,0.6558818466) [ #5 ]
	  #1: (0.4132981074,0.5756729662,0.8249852354,0.3957488953,0.5658004533) [ #7 ]
	  #2: (0.357595246,0.5872719691,0.5518146714,0.5618452218,0.610652922) [ #3 ]
	  #4: (0.9289213582,0.9165702439,0.5527197004,0.268485264,0.2463041419) [ #15 ]
	  #6: (0.5014803892,0.6817833715,0.8219771086,0.3264656241,0.0775322663) [ #11 ]
	  #8: (0.845191426,0.7414528996,0.4705074068,0.3762617672,0.7236746008) [ #9 ]
	  #10: (0.0595591958,0.9910994321,0.8735950118,0.5318192572,0.054165287) [ #11 ]
	  #12: (0.354725022,0.9420667789,0.3395639064,0.3297338986,0.9487606892) [ #13 ]
	  #14: (0.8256802293,0.8959328252,0.2042499758,0.0233809524,0.3771066863) [ #15 ]
	Steiner points:
	  #3: (0.39436221048651715,0.6324426280016368,0.5915046468895456,0.5471361327382943,0.6093784129659762) [ #7 #5 #2 ]
	  #5: (0.4458133604489896,0.7101769888610475,0.5678403726798101,0.5796291785175085,0.6518179531582846) [ #3 #13 #0 ]
	  #7: (0.40378854858891716,0.6135287888949024,0.767586211472772,0.43268695918944977,0.5171269899716225) [ #11 #3 #1 ]
	  #9: (0.7840987288639688,0.7720683444421128,0.46249942286119433,0.36388812110125,0.673349425703296) [ #13 #8 #15 ]
	  #11: (0.3822323529735089,0.7273564600632562,0.8176566876914342,0.3987472117316357,0.1945738410289509) [ #10 #6 #7 ]
	  #13: (0.5386715163288444,0.7880533170211977,0.4760564619428538,0.44496290806969585,0.7318891951913272) [ #5 #9 #12 ]
	  #15: (0.8545144792313599,0.8687847680566967,0.41510404831088515,0.21693728381657937,0.40994617829327346) [ #4 #9 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 9.093976811
	Number of best updates: 14

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 105018

	Init times:
		Bottleneck Steiner distances: 0.000021756
		Smallest sphere distances: 0.000001282
		Sorting: 0.000003104
		Total init time: 0.000027263

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000891, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00000342, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000033108, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000352043, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004671677, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.054515233, bsd pruned: 10395, ss pruned: 31185

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 93555
	Total number iterations 1374876 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 14.695911495911496
	Total time: 8.981191088
	Avarage time pr. topology: 0.000095998

Data for the geometric median iteration:
	Total time: 8.420555581
	Total steps: 46094225
	Average number of steps pr. geometric median: 70.38522030585523
	Average time pr. step: 0.00000018268135717652266
	Average time pr. geometric median: 0.000012858067570642174

