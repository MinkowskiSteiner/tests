All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.0895484716759904
MST len: 8.681739343500002
Steiner Ratio: 0.8166046216284896
Solution Steiner tree:
	Terminals:
	  #0: (0.5318192572,0.054165287,0.845191426,0.7414528996,0.4705074068,0.3762617672,0.7236746008,0.4541832616,0.7798275243) [ #3 ]
	  #1: (0.9165702439,0.5527197004,0.268485264,0.2463041419,0.5014803892,0.6817833715,0.8219771086,0.3264656241,0.0775322663) [ #7 ]
	  #2: (0.608648137,0.7615917403,0.6558818466,0.357595246,0.5872719691,0.5518146714,0.5618452218,0.610652922,0.9289213582) [ #5 ]
	  #4: (0.9487606892,0.4132981074,0.5756729662,0.8249852354,0.3957488953,0.5658004533,0.0595591958,0.9910994321,0.8735950118) [ #5 ]
	  #6: (0.8256802293,0.8959328252,0.2042499758,0.0233809524,0.3771066863,0.354725022,0.9420667789,0.3395639064,0.3297338986) [ #7 ]
	Steiner points:
	  #3: (0.7220189877333334,0.5527197008863619,0.5756729662,0.37414303266666665,0.4845094237333333,0.5288321078888895,0.7236746008,0.4541832616,0.7798275242999999) [ #0 #5 #7 ]
	  #5: (0.7220189877333334,0.5527197008863619,0.5756729662,0.37414303266666665,0.4845094237333333,0.5288321078888895,0.5618452218,0.610652922,0.8735950118) [ #2 #3 #4 ]
	  #7: (0.8256802293,0.5527223191315465,0.268485264,0.2463041419,0.48450942373333333,0.5288321078888895,0.8219771086,0.3395639064,0.32973389859999996) [ #1 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000429475
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 15

	Init times:
		Bottleneck Steiner distances: 0.000008678
		Smallest sphere distances: 0.00000213
		Sorting: 0.000003306
		Total init time: 0.000015186

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002479, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000011884, bsd pruned: 0, ss pruned: 3

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 12
	Total number iterations 24 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000372004
	Avarage time pr. topology: 0.000031

Data for the geometric median iteration:
	Total time: 0.000325118
	Total steps: 310
	Average number of steps pr. geometric median: 8.61111111111111
	Average time pr. step: 0.000001048767741935484
	Average time pr. geometric median: 0.000009031055555555555

