All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.476590024125695
MST len: 9.3430813109
Steiner Ratio: 0.8002274383937145
Solution Steiner tree:
	Terminals:
	  #0: (0.7699328697,0.1005670764,0.8980665714,0.6543430084,0.3445661666,0.0622836598,0.0175517202,0.7314829946,0.1082201405) [ #3 ]
	  #1: (0.5119722684,0.8435701522,0.9867132548,0.8250681748,0.3494697229,0.1410814236,0.1587108039,0.0477595204,0.2314980189) [ #5 ]
	  #2: (0.5058995711,0.1543681692,0.3336426291,0.698289798,0.0904165954,0.6112220658,0.052807556,0.6665685525,0.8844101391) [ #7 ]
	  #4: (0.0011071404,0.3400666059,0.9582093637,0.8222869392,0.1893208209,0.7204574974,0.7427683257,0.4821770696,0.3179936159) [ #5 ]
	  #6: (0.2439990897,0.1642170884,0.3632087118,0.3869168281,0.0459364807,0.2802418788,0.1041107807,0.2645267724,0.9979975852) [ #7 ]
	Steiner points:
	  #3: (0.5058995710999983,0.15765114226666666,0.8980665714,0.6543430092648728,0.19418798392839504,0.3964117162691359,0.0699086308995798,0.47223173806666663,0.3179936159) [ #5 #0 #7 ]
	  #5: (0.5058995710999983,0.3400666059,0.9582093637,0.8222869392,0.19418798392839504,0.3964117162691359,0.15871080389999997,0.47223173806666663,0.3179936159) [ #4 #3 #1 ]
	  #7: (0.5058955748767756,0.15765114226666666,0.3632087118,0.6543430092648728,0.0904165954,0.3964117162691359,0.0699086308995798,0.47223173806666663,0.8844101391) [ #2 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000593505
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000013297
		Smallest sphere distances: 0.000002323
		Sorting: 0.000003545
		Total init time: 0.000021151

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002752, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00001325, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000507146
	Avarage time pr. topology: 0.000033809

Data for the geometric median iteration:
	Total time: 0.00043812
	Total steps: 394
	Average number of steps pr. geometric median: 8.755555555555556
	Average time pr. step: 0.000001111979695431472
	Average time pr. geometric median: 0.000009736000000000001

