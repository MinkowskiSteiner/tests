All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.09003223399271
MST len: 8.5718235385
Steiner Ratio: 0.8271323134626041
Solution Steiner tree:
	Terminals:
	  #0: (0.3630522044,0.078085281,0.7284141638,0.8289281227,0.5918537013,0.5327139341,0.6194605108,0.9981613504,0.3531363068) [ #3 ]
	  #1: (0.3812597861,0.3492478469,0.8728369269,0.9995752056,0.5993793447,0.9228681605,0.4677818946,0.0902298559,0.4658759178) [ #5 ]
	  #2: (0.4463474226,0.2294683951,0.6895571163,0.567347118,0.6107281813,0.0388049632,0.4401840444,0.6103033869,0.6381843079) [ #7 ]
	  #4: (0.3998939984,0.2654632643,0.541314984,0.3868324786,0.2293901449,0.7843163962,0.1882775664,0.1697030753,0.9607357951) [ #5 ]
	  #6: (0.5137684199,0.8042997703,0.7905323886,0.4063076491,0.8204223732,0.1528823809,0.9984735842,0.4864156537,0.460190471) [ #7 ]
	Steiner points:
	  #3: (0.39989399840000006,0.32131965269999996,0.6895571163000285,0.6061036935913581,0.6107281812999413,0.5327139341,0.44018404440000003,0.5690074758336062,0.6381843078999975) [ #5 #0 #7 ]
	  #5: (0.3998939984,0.32131965269999996,0.6895571163000285,0.6061036935913581,0.6107281812999413,0.7843163962,0.44018404440000003,0.1697030753,0.6381843078999975) [ #4 #3 #1 ]
	  #7: (0.4463474226,0.32131965269999996,0.6895575014886399,0.567347118,0.6107289812167798,0.15288238090000003,0.4401861740974567,0.5690074758336062,0.6381836289099861) [ #2 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000528485
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000021074
		Smallest sphere distances: 0.00000213
		Sorting: 0.000003552
		Total init time: 0.000027776

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002073, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000013286, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000456884
	Avarage time pr. topology: 0.000030458

Data for the geometric median iteration:
	Total time: 0.000399324
	Total steps: 386
	Average number of steps pr. geometric median: 8.577777777777778
	Average time pr. step: 0.000001034518134715026
	Average time pr. geometric median: 0.000008873866666666666

