All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.8879570151820273
MST len: 4.258403193446798
Steiner Ratio: 0.9130081954581367
Solution Steiner tree:
	Terminals:
	  #0: (0.6704191042,0.7412129779,0.229606642,0.9381766622,0.9481784259,0.9291220382,0.1416073912,0.3070574362,0.8136533512) [ #3 ]
	  #1: (0.206965448,0.6995153961,0.2034307295,0.3588790108,0.884531313,0.4632373589,0.7227528005,0.6292115332,0.7207948913) [ #5 ]
	  #2: (0.6048447502,0.0298102363,0.4428648839,0.3256396411,0.7602754271,0.4969397343,0.2488169443,0.8343745516,0.425641888) [ #7 ]
	  #4: (0.6858023101,0.9032684844,0.0947479369,0.6750120226,0.2615602367,0.272972847,0.9914577939,0.2373887199,0.3081078475) [ #5 ]
	  #6: (0.7304651908,0.05407485,0.9231773037,0.0740991244,0.9287021542,0.8842824203,0.0917933952,0.0898954501,0.3307895457) [ #7 ]
	Steiner points:
	  #3: (0.5417099546197476,0.5354662004823872,0.3178349726214728,0.546805096169483,0.8144405013158275,0.6393933075925294,0.40696776797041645,0.4731128667841046,0.606769268454796) [ #5 #0 #7 ]
	  #5: (0.43302892297877954,0.6614726521047045,0.2355044769856095,0.4936418822143321,0.7504658630783214,0.5088207465737052,0.6290296577387093,0.49582961883200044,0.6023783071821817) [ #4 #3 #1 ]
	  #7: (0.6087040892593472,0.21026014731466225,0.498002892185007,0.35079022632540807,0.813620919099928,0.6258290047003002,0.27148835429007334,0.5562956592046718,0.4690397102382807) [ #2 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.001727708
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000004592
		Smallest sphere distances: 0.00000103
		Sorting: 0.000001138
		Total init time: 0.000007642

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000847, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000004196, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 218 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 14.533333333333333
	Total time: 0.001700677
	Avarage time pr. topology: 0.000113378

Data for the geometric median iteration:
	Total time: 0.001649461
	Total steps: 6103
	Average number of steps pr. geometric median: 135.62222222222223
	Average time pr. step: 0.0000002702705226937572
	Average time pr. geometric median: 0.0000366546888888889

