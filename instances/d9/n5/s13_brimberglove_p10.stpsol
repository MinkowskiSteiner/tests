All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 8.4995810034
MST len: 10.468730800500001
Steiner Ratio: 0.8119017639649352
Solution Steiner tree:
	Terminals:
	  #0: (0.0944045461,0.485553364,0.1763587325,0.0226054806,0.0416181572,0.251262738,0.0757720694,0.205355405,0.3670426623) [ #7 ]
	  #1: (0.4365024196,0.0231408114,0.3533674392,0.8654058324,0.0406219666,0.7204131906,0.3779684163,0.1168798023,0.6027370345) [ #5 ]
	  #2: (0.5560647936,0.0749040051,0.0531665883,0.1637372478,0.1157799243,0.8294556531,0.9116950463,0.792852993,0.8622477059) [ #3 ]
	  #4: (0.4139833741,0.5670465732,0.4317533082,0.8557877559,0.1996897558,0.7731688911,0.7598976808,0.3297242696,0.3815315046) [ #5 ]
	  #6: (0.6326431831,0.3414155828,0.904109925,0.1300345134,0.6083626135,0.3345068657,0.1558290944,0.7948272283,0.928200935) [ #7 ]
	Steiner points:
	  #3: (0.42899607110000004,0.0749040051,0.3794960622,0.1637372478,0.1157799243,0.7379984240999999,0.5052781711333334,0.4888769210123004,0.6326602106) [ #5 #2 #7 ]
	  #5: (0.42899607110000004,0.0749040051,0.3794960622,0.8557877559000001,0.1157799243,0.7379984240999999,0.5052781711333334,0.3297242696,0.6027370345) [ #1 #3 #4 ]
	  #7: (0.42899607110000004,0.3414155828,0.37949606220000004,0.13003451340000002,0.1157799243,0.3345068657,0.1558290944,0.4888769210123004,0.6326602106) [ #0 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000686465
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000010558
		Smallest sphere distances: 0.000002861
		Sorting: 0.000004558
		Total init time: 0.000019096

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002593, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000018041, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.00061348
	Avarage time pr. topology: 0.000040898

Data for the geometric median iteration:
	Total time: 0.000537314
	Total steps: 390
	Average number of steps pr. geometric median: 8.666666666666666
	Average time pr. step: 0.000001377728205128205
	Average time pr. geometric median: 0.00001194031111111111

Data for the geometric median step function:
	Total number of fixed points encountered: 1803

