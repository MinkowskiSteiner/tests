All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 8.492941616716845
MST len: 10.7310298244
Steiner Ratio: 0.7914377050193043
Solution Steiner tree:
	Terminals:
	  #0: (0.776706667,0.031953645,0.6816179667,0.8094309405,0.8175539276,0.1217957135,0.9245497658,0.6410978039,0.8101899306) [ #3 ]
	  #1: (0.0544752507,0.9522628411,0.3267083561,0.696483258,0.4432977626,0.4120336987,0.1106646979,0.7293048565,0.4997239469) [ #5 ]
	  #2: (0.7856002826,0.4401777473,0.1151188249,0.8235438768,0.6883942167,0.1299254853,0.3111650372,0.5165184259,0.6420080073) [ #3 ]
	  #4: (0.7771808131,0.8949633971,0.8192397947,0.6017836037,0.3779945804,0.8946192055,0.1970067798,0.1855946668,0.9467040281) [ #5 ]
	  #6: (0.4910349215,0.0853253426,0.4141814399,0.2860070939,0.0876902482,0.3650377357,0.4803538809,0.6394705305,0.1302257493) [ #7 ]
	Steiner points:
	  #3: (0.7767066669999986,0.4401777473,0.4541733147168435,0.8094309405000001,0.6883942167,0.1299254853,0.3111650372,0.5165184259,0.6420080073) [ #2 #0 #7 ]
	  #5: (0.5851205002876543,0.8949633971000001,0.4541733147168435,0.6017836037,0.4432977626,0.4120336987,0.19700677980000003,0.5575024607666667,0.4997239469000011) [ #1 #4 #7 ]
	  #7: (0.5851205002876543,0.4401777473,0.4541733147168435,0.6017836037,0.4432977625999999,0.36503773570000003,0.31116503720000005,0.5575024607666667,0.4997239469000011) [ #3 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000710437
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000009552
		Smallest sphere distances: 0.000002892
		Sorting: 0.00000592
		Total init time: 0.000019422

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002573, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000018053, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000635327
	Avarage time pr. topology: 0.000042355

Data for the geometric median iteration:
	Total time: 0.000558692
	Total steps: 404
	Average number of steps pr. geometric median: 8.977777777777778
	Average time pr. step: 0.0000013829009900990098
	Average time pr. geometric median: 0.000012415377777777777

Data for the geometric median step function:
	Total number of fixed points encountered: 1911

