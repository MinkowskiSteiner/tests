All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 8.004586400141404
MST len: 10.1501713904
Steiner Ratio: 0.7886158856107707
Solution Steiner tree:
	Terminals:
	  #0: (0.1213370185,0.3546023389,0.3640158462,0.48389697,0.206476939,0.6585222355,0.9598705587,0.8190235145,0.886197098) [ #7 ]
	  #1: (0.576686004,0.5689670525,0.5896822263,0.8609460652,0.737937074,0.6182698098,0.6337565941,0.1025872855,0.8561092638) [ #3 ]
	  #2: (0.1689700215,0.0285875835,0.7728105293,0.3646502115,0.237839454,0.4875804249,0.2520150534,0.5079065824,0.3625599515) [ #5 ]
	  #4: (0.5259626366,0.9567871433,0.9126800242,0.2932653806,0.3414342214,0.5614015812,0.7468854965,0.2319139071,0.0643970538) [ #5 ]
	  #6: (0.8518746006,0.2945063893,0.4759735886,0.6125465755,0.2276748625,0.680792569,0.0897512576,0.6359386517,0.9450358706) [ #7 ]
	Steiner points:
	  #3: (0.3807272135333334,0.4466590218999355,0.5896822263,0.5267801718333334,0.2723710431333333,0.6182698098,0.5006810994112854,0.4159090239666667,0.8341318234569884) [ #1 #5 #7 ]
	  #5: (0.38072721353333333,0.4466590218999355,0.7728105293,0.3646502115,0.2723710431333333,0.5614015812,0.5006810994112854,0.4159090239666667,0.3625599515) [ #2 #3 #4 ]
	  #7: (0.3807272135333334,0.3546023389,0.4759735886,0.5267801718333334,0.2276748625,0.6585222355,0.5006810994112854,0.6359386517,0.8341318234569884) [ #0 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000585398
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000012067
		Smallest sphere distances: 0.000002938
		Sorting: 0.000003787
		Total init time: 0.000021115

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002849, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000015516, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000512346
	Avarage time pr. topology: 0.000034156

Data for the geometric median iteration:
	Total time: 0.000445628
	Total steps: 328
	Average number of steps pr. geometric median: 7.288888888888889
	Average time pr. step: 0.000001358621951219512
	Average time pr. geometric median: 0.000009902844444444443

