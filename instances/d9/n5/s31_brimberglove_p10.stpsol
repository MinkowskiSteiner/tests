All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.341593867824949
MST len: 9.5701592174
Steiner Ratio: 0.7671339317403224
Solution Steiner tree:
	Terminals:
	  #0: (0.1869343599,0.44229848,0.9763836213,0.62982698,0.0372657241,0.2388074055,0.1162355529,0.442412433,0.306818193) [ #3 ]
	  #1: (0.5921190123,0.7871168474,0.8626960571,0.4022940902,0.433104444,0.3224785879,0.1665347033,0.6197118892,0.4428926201) [ #3 ]
	  #2: (0.5949672445,0.2624237846,0.4864085733,0.4051467089,0.068010787,0.7012902786,0.2066657628,0.0859796964,0.6465744323) [ #5 ]
	  #4: (0.4809464605,0.2813506742,0.1364504407,0.0017597149,0.873469687,0.9235672881,0.864455772,0.2757637767,0.3566717321) [ #7 ]
	  #6: (0.9411502084,0.4651792224,0.1887814688,0.3742742359,0.7257872944,0.9398560226,0.0504484792,0.7854848689,0.4252954514) [ #7 ]
	Steiner points:
	  #3: (0.5743506043750526,0.44229848,0.8626960571,0.40229409019999945,0.33649708700000003,0.3224785879,0.1665347033,0.442412433,0.4428926201) [ #0 #5 #1 ]
	  #5: (0.5743506043750526,0.3426268569333333,0.4864085733,0.40229409019999945,0.33649708700000003,0.7012902786,0.16653879961514606,0.4424124329975997,0.4428926201) [ #2 #3 #7 ]
	  #7: (0.5743506043750526,0.3426268569333333,0.1887814688,0.3742742359,0.7257872944,0.9235672881,0.16653879961514606,0.4424124329975997,0.4252954514) [ #4 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.00063258
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000010187
		Smallest sphere distances: 0.000002446
		Sorting: 0.000003947
		Total init time: 0.000017778

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002666, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000014709, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000553575
	Avarage time pr. topology: 0.000036905

Data for the geometric median iteration:
	Total time: 0.000489456
	Total steps: 377
	Average number of steps pr. geometric median: 8.377777777777778
	Average time pr. step: 0.000001298291777188329
	Average time pr. geometric median: 0.0000108768

