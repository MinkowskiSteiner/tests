All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.529261814234081
MST len: 9.3296374331
Steiner Ratio: 0.8070261967010115
Solution Steiner tree:
	Terminals:
	  #0: (0.7510317898,0.6239873639,0.9691653433,0.0583464685,0.4434102766,0.4754556192,0.1466605273,0.2289445811,0.8166331438) [ #3 ]
	  #1: (0.6861238385,0.9269996448,0.8449453608,0.1197466637,0.0576706543,0.2857596349,0.6257388632,0.607333408,0.320081121) [ #3 ]
	  #2: (0.958641605,0.1473828816,0.0937726004,0.1534282836,0.0076622083,0.4441131705,0.5998500383,0.0285220109,0.4814383762) [ #7 ]
	  #4: (0.8602793272,0.35034057,0.4464217548,0.0208598026,0.0373252058,0.1292244392,0.3481050377,0.7246325052,0.5291235137) [ #5 ]
	  #6: (0.8194229132,0.5062902763,0.0883140588,0.7855343049,0.3411775247,0.8119810777,0.9184383005,0.2771394566,0.1947866791) [ #7 ]
	Steiner points:
	  #3: (0.7510317898,0.6239873639,0.8449453608,0.11974666369999999,0.06421304923408191,0.3391502602397384,0.554982955366667,0.32736636573333333,0.4973333513739485) [ #0 #5 #1 ]
	  #5: (0.8602793272,0.35034057,0.4464217548,0.11974666369999999,0.06421304923408191,0.33915026023973843,0.554982955366667,0.32736636573333333,0.4973333513739485) [ #4 #3 #7 ]
	  #7: (0.8602793272000001,0.35034057,0.09377260040000011,0.1534282836,0.06421304923408191,0.4441131705,0.5998500383,0.2771394566,0.48143837619999996) [ #2 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000517438
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000010848
		Smallest sphere distances: 0.000002225
		Sorting: 0.000003983
		Total init time: 0.000018496

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002216, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000014583, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000450732
	Avarage time pr. topology: 0.000030048

Data for the geometric median iteration:
	Total time: 0.000389952
	Total steps: 358
	Average number of steps pr. geometric median: 7.955555555555556
	Average time pr. step: 0.0000010892513966480448
	Average time pr. geometric median: 0.0000086656

