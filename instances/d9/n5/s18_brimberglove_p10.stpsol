All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 8.46018245092994
MST len: 9.3275416033
Steiner Ratio: 0.9070109585935059
Solution Steiner tree:
	Terminals:
	  #0: (0.3375514235,0.1884241831,0.0118348948,0.8027626829,0.6756664485,0.1440341534,0.1769533321,0.3554075599,0.0303536835) [ #3 ]
	  #1: (0.9167352235,0.3484674186,0.7351341866,0.9038881473,0.5736667344,0.0998372487,0.0471116267,0.0357322353,0.1799209691) [ #3 ]
	  #2: (0.4872422654,0.1321992586,0.3741906497,0.6797411119,0.8863195306,0.801372399,0.2213446718,0.2145956132,0.2002614784) [ #5 ]
	  #4: (0.9172431221,0.0559421322,0.0346130878,0.4671709633,0.9850149108,0.9476090371,0.391033577,0.2210022007,0.7333814817) [ #7 ]
	  #6: (0.9290727782,0.9129959489,0.9238626137,0.2359872904,0.785772445,0.946517847,0.9674219824,0.2784534135,0.4652112594) [ #7 ]
	Steiner points:
	  #3: (0.6305758843,0.1884241831,0.26099812906666664,0.6088843957016983,0.6756664485,0.1440341534,0.2779076402000001,0.21673134051710286,0.2817979023) [ #0 #5 #1 ]
	  #5: (0.6305758843,0.1884241831,0.26099812906666664,0.6088843957016983,0.87453039706627,0.801372399,0.2779076402000001,0.21673134051710286,0.2817979023) [ #2 #3 #7 ]
	  #7: (0.9172431221,0.18842418309999998,0.26099812906666664,0.4671709633,0.87453039706627,0.9465178469999973,0.391033577,0.22100220070000004,0.4652112594) [ #4 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000598355
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 15

	Init times:
		Bottleneck Steiner distances: 0.000010154
		Smallest sphere distances: 0.000002958
		Sorting: 0.000004535
		Total init time: 0.000018714

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002536, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000015055, bsd pruned: 0, ss pruned: 3

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 12
	Total number iterations 24 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000529244
	Avarage time pr. topology: 0.000044103

Data for the geometric median iteration:
	Total time: 0.00046862
	Total steps: 339
	Average number of steps pr. geometric median: 9.416666666666666
	Average time pr. step: 0.0000013823598820058996
	Average time pr. geometric median: 0.00001301722222222222

Data for the geometric median step function:
	Total number of fixed points encountered: 1509

