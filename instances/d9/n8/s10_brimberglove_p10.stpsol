All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 11.769011985136157
MST len: 14.453021017100003
Steiner Ratio: 0.8142942552433655
Solution Steiner tree:
	Terminals:
	  #0: (0.8191577288,0.5812235915,0.7397374505,0.7828859262,0.8270601387,0.4016350794,0.1687702002,0.09823087,0.379792303) [ #11 ]
	  #1: (0.5658107323,0.6109299178,0.5057680786,0.1796468744,0.8166862739,0.1834716542,0.584652962,0.4221560766,0.0253341845) [ #3 ]
	  #2: (0.2458365472,0.6618976293,0.3858842744,0.2711707318,0.9781572241,0.4471604677,0.3548297497,0.954948144,0.4252187523) [ #5 ]
	  #4: (0.1240179041,0.0938713258,0.4331079281,0.1360037984,0.962261884,0.7648949897,0.6721158049,0.5188586984,0.6624927827) [ #9 ]
	  #6: (0.8809632025,0.5625731631,0.6635139085,0.9814409283,0.9619104471,0.1394470134,0.2234422426,0.2144171056,0.0119858943) [ #7 ]
	  #8: (0.2287187526,0.0080249533,0.6942072132,0.3210569827,0.8889881558,0.2567803758,0.9845708911,0.8704290836,0.2186908225) [ #9 ]
	  #10: (0.8683905582,0.3317870611,0.5361120061,0.5565968144,0.8975977934,0.1470419239,0.0623648931,0.0772446674,0.9637281978) [ #11 ]
	  #12: (0.3162595943,0.0612761933,0.083659018,0.9767909204,0.9780582851,0.8738890034,0.0530768098,0.2689884604,0.0923382296) [ #13 ]
	Steiner points:
	  #3: (0.5658107323,0.5625731631,0.5057680786,0.3210569967397648,0.9622618818432029,0.4471631530868504,0.3548297497,0.4221560766,0.1297760349022058) [ #1 #5 #13 ]
	  #5: (0.2534642940334508,0.5625731631,0.5057680786,0.32105699673976484,0.9622618818432029,0.4471631531256205,0.3548297497,0.6360488268,0.4252187523) [ #2 #9 #3 ]
	  #7: (0.8355686719333333,0.5625731631,0.5361120061000001,0.7074562222666666,0.9619104471,0.3167706942333335,0.16877027881439044,0.21441710560000002,0.1297760349022058) [ #6 #11 #13 ]
	  #9: (0.2534642940334508,0.0938713258,0.5057680786,0.32105699673976484,0.9622618818432029,0.4471631531256205,0.6721158049,0.6360488268,0.4252187523) [ #8 #5 #4 ]
	  #11: (0.8355686719333333,0.5625731630999999,0.5361120061000001,0.7074562222666666,0.8975977934,0.3167706942333335,0.1687702002,0.09823087,0.379792303) [ #10 #7 #0 ]
	  #13: (0.5658107323,0.5625693385368192,0.5057680786,0.7074562222666666,0.9622618818432029,0.4471631530868504,0.16877027881439044,0.2689884604000001,0.1297760349022058) [ #3 #7 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.504429137
	Number of best updates: 15

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 11463

	Init times:
		Bottleneck Steiner distances: 0.000027352
		Smallest sphere distances: 0.000007393
		Sorting: 0.000032296
		Total init time: 0.000069491

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002304, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000013297, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000119437, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001341788, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.018074311, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 10395
	Total number iterations 21124 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0321308321308322
	Total time: 0.464183418
	Avarage time pr. topology: 0.000044654

Data for the geometric median iteration:
	Total time: 0.391581367
	Total steps: 397620
	Average number of steps pr. geometric median: 6.375180375180375
	Average time pr. step: 0.000000984813055178311
	Average time pr. geometric median: 0.000006278360862594196

Data for the geometric median step function:
	Total number of fixed points encountered: 2411383

