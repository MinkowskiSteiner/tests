All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 11.85473856669879
MST len: 14.618717736499999
Steiner Ratio: 0.8109287545172236
Solution Steiner tree:
	Terminals:
	  #0: (0.1034285073,0.9779993249,0.8022396713,0.6132100036,0.1134380932,0.7647564228,0.4129372162,0.5164472929,0.8498845724) [ #5 ]
	  #1: (0.6177704891,0.4252223039,0.3870529828,0.7761541152,0.6623876005,0.1703382573,0.3191112491,0.3110007436,0.691104038) [ #7 ]
	  #2: (0.7507032281,0.0882518473,0.9628520696,0.6005878,0.3068237362,0.1244559796,0.6978721804,0.8352825026,0.2691778663) [ #3 ]
	  #4: (0.1354387687,0.9625167521,0.799727213,0.4030091993,0.0851281491,0.8056346727,0.6451566176,0.2473998085,0.3098868776) [ #11 ]
	  #6: (0.8889893428,0.2456402682,0.9563567112,0.9352118568,0.8634107573,0.3815790147,0.3222648391,0.6395648721,0.0439666151) [ #13 ]
	  #8: (0.4926030964,0.9586761212,0.3549673587,0.183707134,0.062104628,0.3329666836,0.9859468052,0.6753146316,0.4464047767) [ #9 ]
	  #10: (0.2185718884,0.16160391,0.0972843804,0.5284587664,0.1447218867,0.6269470568,0.4722914819,0.0229840363,0.4052342835) [ #11 ]
	  #12: (0.9831179767,0.5296626759,0.9438327159,0.87826215,0.7782872272,0.4166978609,0.2226562315,0.5511224272,0.0462225136) [ #13 ]
	Steiner points:
	  #3: (0.7507032281,0.4252223039,0.8809025088987711,0.6005878848154171,0.3068237362,0.3815790147,0.6039404125333334,0.5280056710000001,0.2691778663) [ #2 #7 #9 ]
	  #5: (0.1631498085831974,0.9586761212,0.8022396713,0.6132100036,0.1134380932,0.7647564228,0.6039404125333333,0.5164472929,0.34166934623107764) [ #11 #9 #0 ]
	  #7: (0.7507032281,0.4252223039,0.8809025088987711,0.7761541152,0.6623876005,0.3815790147,0.31911124950158753,0.5280056710000001,0.2691778663) [ #3 #1 #13 ]
	  #9: (0.4926030964,0.9586761212,0.8022396713,0.6132100036,0.11343809320000002,0.3815790147,0.6039404125333333,0.5280056710000001,0.34166934623107764) [ #5 #8 #3 ]
	  #11: (0.1631498085831974,0.9586761212,0.799727213,0.5284587664,0.1134380932,0.7647564228000001,0.6039404125333334,0.2473998085,0.34166934623107764) [ #10 #5 #4 ]
	  #13: (0.8889893428,0.42522230390000004,0.9438327158999998,0.8782621499999999,0.7782872272,0.3815833011452825,0.31911124950158753,0.5511224272,0.0462225136) [ #6 #7 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.347270711
	Number of best updates: 13

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 7683

	Init times:
		Bottleneck Steiner distances: 0.000037047
		Smallest sphere distances: 0.000008961
		Sorting: 0.000023661
		Total init time: 0.000072689

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002635, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000014053, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000126066, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001423261, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.013445246, bsd pruned: 0, ss pruned: 3780

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 6615
	Total number iterations 13379 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0225245653817083
	Total time: 0.317031023
	Avarage time pr. topology: 0.000047926

Data for the geometric median iteration:
	Total time: 0.268305611
	Total steps: 257275
	Average number of steps pr. geometric median: 6.482111363063744
	Average time pr. step: 0.000001042874787678554
	Average time pr. geometric median: 0.0000067600305114638445

Data for the geometric median step function:
	Total number of fixed points encountered: 1528519

