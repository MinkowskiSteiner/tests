All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 13.105102378977344
MST len: 14.8525785241
Steiner Ratio: 0.882345268043042
Solution Steiner tree:
	Terminals:
	  #0: (0.206965448,0.6995153961,0.2034307295,0.3588790108,0.884531313,0.4632373589,0.7227528005,0.6292115332,0.7207948913) [ #3 ]
	  #1: (0.1330993646,0.9261679467,0.5155373381,0.4638889103,0.6119702568,0.4188058225,0.5586368472,0.2869822789,0.6803660592) [ #3 ]
	  #2: (0.6048447502,0.0298102363,0.4428648839,0.3256396411,0.7602754271,0.4969397343,0.2488169443,0.8343745516,0.425641888) [ #5 ]
	  #4: (0.7304651908,0.05407485,0.9231773037,0.0740991244,0.9287021542,0.8842824203,0.0917933952,0.0898954501,0.3307895457) [ #7 ]
	  #6: (0.8316096942,0.2784400723,0.9177547795,0.1397175417,0.9488591766,0.658967757,0.3693241837,0.8870358383,0.6071461824) [ #9 ]
	  #8: (0.2984462214,0.0286432291,0.9142036186,0.1120995721,0.6334879797,0.9440138549,0.5549644565,0.9591276208,0.7042892816) [ #9 ]
	  #10: (0.6704191042,0.7412129779,0.229606642,0.9381766622,0.9481784259,0.9291220382,0.1416073912,0.3070574362,0.8136533512) [ #13 ]
	  #12: (0.6858023101,0.9032684844,0.0947479369,0.6750120226,0.2615602367,0.272972847,0.9914577939,0.2373887199,0.3081078475) [ #13 ]
	Steiner points:
	  #3: (0.206965448,0.7412129779,0.37177880326666674,0.4638889103,0.6119702572114686,0.4632373589,0.5586549745861094,0.2869822789,0.6803660592) [ #1 #0 #11 ]
	  #5: (0.6048447502,0.2784400723211185,0.37177880326666674,0.3256396411,0.7602754271,0.4969397343,0.5586549745861094,0.2869822789,0.425641888) [ #11 #2 #7 ]
	  #7: (0.7056524793000001,0.2784400723211185,0.9142036186,0.1397175417,0.7602754271,0.658967757,0.5586549745861094,0.2869822789,0.425641888) [ #5 #4 #9 ]
	  #9: (0.7056524793000001,0.2784400723,0.9142036186,0.1397175417,0.6334879801357434,0.658967757,0.5549644565,0.8870358383,0.6071461824) [ #6 #8 #7 ]
	  #11: (0.6048447502,0.7412129779,0.37177880326666674,0.4638889103,0.7602754271,0.4969397343,0.5586549745861094,0.2869822789,0.6803660592) [ #3 #5 #13 ]
	  #13: (0.6704191041999998,0.7412179232906588,0.229606642,0.6750120226,0.7602754271,0.4969397343,0.5586549745861094,0.2869822789,0.6803660592) [ #10 #11 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.401008803
	Number of best updates: 17

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 8628

	Init times:
		Bottleneck Steiner distances: 0.000033958
		Smallest sphere distances: 0.000007972
		Sorting: 0.000021526
		Total init time: 0.000066249

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002275, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000013297, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000118542, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001348439, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.014032963, bsd pruned: 945, ss pruned: 1890

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 7560
	Total number iterations 15475 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.046957671957672
	Total time: 0.369467975
	Avarage time pr. topology: 0.000048871

Data for the geometric median iteration:
	Total time: 0.316493378
	Total steps: 322272
	Average number of steps pr. geometric median: 7.104761904761904
	Average time pr. step: 0.0000009820691155297388
	Average time pr. geometric median: 0.0000069773672398589055

Data for the geometric median step function:
	Total number of fixed points encountered: 1962709

