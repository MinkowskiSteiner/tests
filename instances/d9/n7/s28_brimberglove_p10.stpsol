All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 11.338104244961217
MST len: 14.200201479700002
Steiner Ratio: 0.7984467164898811
Solution Steiner tree:
	Terminals:
	  #0: (0.3412099454,0.230164813,0.4956663146,0.7013661609,0.408563846,0.7201248397,0.8276713341,0.0367873907,0.8458900362) [ #7 ]
	  #1: (0.4957790014,0.912344499,0.7575103081,0.3416690372,0.7732275016,0.2325874237,0.3037002093,0.7334666446,0.3451992978) [ #9 ]
	  #2: (0.517155105,0.0449436945,0.4524295369,0.442212973,0.1277577337,0.1569511728,0.0837527169,0.7310733133,0.0224596341) [ #3 ]
	  #4: (0.1184722181,0.0927696466,0.1879583286,0.4004020325,0.9834894496,0.1869345928,0.5786215498,0.6316369533,0.7030659619) [ #5 ]
	  #6: (0.257528213,0.0420970586,0.0819942584,0.2799878476,0.3833070041,0.3121590714,0.7756541622,0.0846731649,0.7207229173) [ #7 ]
	  #8: (0.890719803,0.9989762641,0.1782195168,0.6481475037,0.5161313692,0.2231632118,0.1005770402,0.9583443422,0.3509209456) [ #11 ]
	  #10: (0.860883003,0.4750771162,0.9620311726,0.9602391431,0.1126118736,0.8147720088,0.3593030024,0.8427590313,0.2819298144) [ #11 ]
	Steiner points:
	  #3: (0.517155105,0.23958404281111348,0.4524295369,0.442212973,0.46394219048640434,0.23258742369999993,0.23599248626723998,0.7683018856864473,0.3471065137333334) [ #2 #9 #5 ]
	  #5: (0.3412099454,0.23958404281111348,0.4524295369,0.442212973,0.46394219048640434,0.2325874236999999,0.5786215498,0.6316369533,0.7030659619) [ #4 #3 #7 ]
	  #7: (0.3412099454,0.230164813,0.4956663146,0.442212973,0.3833070041,0.3121590714,0.7756541622,0.08467316490000001,0.7207229173000002) [ #6 #0 #5 ]
	  #9: (0.517155105,0.912344499,0.4524295369,0.442212973,0.46394219049502133,0.23258742369999993,0.23599248626723998,0.7683018856864473,0.3471065137333334) [ #1 #3 #11 ]
	  #11: (0.860883003,0.912344499,0.4524295369,0.6481475037,0.4639421904950213,0.2325874237,0.23599248626723998,0.8427590313,0.3471065137333334) [ #8 #9 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.038292127
	Number of best updates: 8

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 963

	Init times:
		Bottleneck Steiner distances: 0.000019278
		Smallest sphere distances: 0.000005808
		Sorting: 0.000024146
		Total init time: 0.000050744

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002293, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00001274, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000115731, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001250899, bsd pruned: 0, ss pruned: 105

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 840
	Total number iterations 1706 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.030952380952381
	Total time: 0.035407513
	Avarage time pr. topology: 0.000042151

Data for the geometric median iteration:
	Total time: 0.03059577
	Total steps: 31404
	Average number of steps pr. geometric median: 7.477142857142857
	Average time pr. step: 0.0000009742634696217043
	Average time pr. geometric median: 0.000007284707142857143

