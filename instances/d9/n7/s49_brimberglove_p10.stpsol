All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 12.063460341219733
MST len: 15.6436604559
Steiner Ratio: 0.7711405124923946
Solution Steiner tree:
	Terminals:
	  #0: (0.5746342924,0.030925551,0.4392039592,0.4733264509,0.4483769622,0.812394509,0.958834955,0.7973932744,0.6833621621) [ #11 ]
	  #1: (0.6174133409,0.9290067493,0.8974420614,0.6268771643,0.9956121147,0.5463564538,0.9780446412,0.8501515877,0.8986921589) [ #7 ]
	  #2: (0.4174514107,0.3731905494,0.4855085041,0.3490163122,0.8709676535,0.6129136903,0.1817772687,0.4070928839,0.1923376211) [ #3 ]
	  #4: (0.6916461264,0.8085904055,0.6644335085,0.1308357772,0.4260037464,0.5934402573,0.0282778386,0.0528809102,0.589052372) [ #9 ]
	  #6: (0.5717486453,0.9791705431,0.0904550455,0.7640862664,0.4773945722,0.0308408244,0.5389059924,0.565081327,0.0130381868) [ #7 ]
	  #8: (0.4982240295,0.9403857789,0.7748197265,0.0876867548,0.9821973629,0.9948574575,0.3964075797,0.1451486364,0.7376186767) [ #9 ]
	  #10: (0.0418115836,0.220037731,0.3087208249,0.1629512739,0.7427612197,0.2952385462,0.6634417692,0.9268148322,0.4391896513) [ #11 ]
	Steiner points:
	  #3: (0.4174514107001018,0.3731905494,0.48550850410000007,0.3490113487811333,0.8709676535,0.5934402573,0.5389059924,0.565081327,0.589052372) [ #5 #2 #11 ]
	  #5: (0.4174514107001018,0.8525221966332602,0.6644335085,0.3490113487811333,0.8709676535,0.5934402573,0.5389059924,0.565081327,0.589052372) [ #7 #3 #9 ]
	  #7: (0.5717486453,0.9290067493,0.6644335085,0.6268771643,0.8709676535,0.5463564538,0.5389059924,0.565081327,0.589052372) [ #1 #6 #5 ]
	  #9: (0.49822402950000005,0.8525221966332602,0.6644339295883978,0.1308357772,0.8709676535000002,0.5934417885792547,0.39640757970000007,0.1451486364,0.5890529387333144) [ #4 #5 #8 ]
	  #11: (0.4174514107001018,0.22003773100000001,0.43920395919999994,0.3490113487811333,0.7427612196999999,0.5934402573,0.6634417692,0.7973932744,0.589052372) [ #0 #3 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.04093294
	Number of best updates: 10

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000022182
		Smallest sphere distances: 0.000006064
		Sorting: 0.000011322
		Total init time: 0.000041447

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002276, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000012465, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000115513, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001329468, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 1904 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0148148148148146
	Total time: 0.03785774
	Avarage time pr. topology: 0.000040061

Data for the geometric median iteration:
	Total time: 0.032448527
	Total steps: 32773
	Average number of steps pr. geometric median: 6.936084656084656
	Average time pr. step: 0.0000009900993805876788
	Average time pr. geometric median: 0.000006867413121693121

