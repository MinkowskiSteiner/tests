All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 11.844542197918345
MST len: 14.599656939599999
Steiner Ratio: 0.8112890766488696
Solution Steiner tree:
	Terminals:
	  #0: (0.9184461929,0.7984392977,0.6373761276,0.9660496954,0.0332565117,0.740849513,0.3530357351,0.0108154076,0.4717412081) [ #9 ]
	  #1: (0.5805552269,0.5467372041,0.0709253289,0.6269347969,0.8786796065,0.8517323736,0.2151423344,0.4985385591,0.9875125955) [ #7 ]
	  #2: (0.3319424024,0.7808070447,0.5882075376,0.619858953,0.1357802214,0.2613565746,0.1352163563,0.4126671052,0.2444709736) [ #5 ]
	  #4: (0.3549731771,0.6731490375,0.5153574038,0.2768868838,0.9831143995,0.7832298362,0.3857721921,0.3929051535,0.0476035024) [ #5 ]
	  #6: (0.6776405306,0.3551183903,0.1416954548,0.0826469702,0.1940099584,0.0051929508,0.5976890882,0.1869087318,0.9714523661) [ #7 ]
	  #8: (0.8388915681,0.8634974965,0.5150421176,0.9928987739,0.9662594152,0.9828661391,0.3598284723,0.0994729628,0.0463795699) [ #9 ]
	  #10: (0.2348172144,0.1034733854,0.3869860398,0.9775588964,0.7308916956,0.3246047955,0.3443029827,0.6699542467,0.4050064401) [ #11 ]
	Steiner points:
	  #3: (0.5805552269,0.6731490374998954,0.5153574037999931,0.6198589531571137,0.7383115060720623,0.6568944956154693,0.3411867902990224,0.4126671052,0.32902264779809504) [ #5 #7 #11 ]
	  #5: (0.3549731771,0.6731490375,0.5153574038,0.619858953,0.7383115060720623,0.6568944956154693,0.3411867902990224,0.4126671052,0.2444709736) [ #2 #4 #3 ]
	  #7: (0.5805552269,0.5467372041,0.1416954548,0.6269347969,0.7383115060720623,0.6568944956154693,0.3411867902990224,0.4126671052,0.9714523661) [ #1 #6 #3 ]
	  #9: (0.8388915680999998,0.7984392976999999,0.5150421176,0.9660496954,0.7358382359147082,0.740849513,0.3530357351,0.09947296279999998,0.329022647798095) [ #0 #8 #11 ]
	  #11: (0.5805552269000002,0.6731490374998954,0.5150421175994794,0.9660496954,0.7358382359147082,0.6568944956154693,0.3443029827,0.4126671052,0.329022647798095) [ #3 #9 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.042660341
	Number of best updates: 11

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000024057
		Smallest sphere distances: 0.000005785
		Sorting: 0.000015726
		Total init time: 0.000047038

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002359, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000013345, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000118293, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001335034, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 1904 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0148148148148146
	Total time: 0.039475254
	Avarage time pr. topology: 0.000041772

Data for the geometric median iteration:
	Total time: 0.033909873
	Total steps: 34485
	Average number of steps pr. geometric median: 7.298412698412698
	Average time pr. step: 0.0000009833224010439322
	Average time pr. geometric median: 0.000007176692698412699

Data for the geometric median step function:
	Total number of fixed points encountered: 209785

