All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.873303130954926
MST len: 5.470320817529657
Steiner Ratio: 0.8908623997587881
Solution Steiner tree:
	Terminals:
	  #0: (0.1360618449,0.4750343694,0.4330457674,0.714648994,0.1368193762,0.5865269595,0.9152492289,0.625351081,0.5795209741) [ #9 ]
	  #1: (0.6617850073,0.153481192,0.2006002349,0.4885317048,0.9929940151,0.7874475535,0.1478928184,0.0819520862,0.2319531549) [ #11 ]
	  #2: (0.1488122675,0.7401193766,0.5311698176,0.3798184727,0.6551358368,0.3903584468,0.6938135543,0.3702549456,0.5785871486) [ #3 ]
	  #4: (0.3216621426,0.2525900748,0.6675356089,0.214533581,0.4014023423,0.4076549855,0.7457033986,0.781220815,0.0627908218) [ #5 ]
	  #6: (0.1640398778,0.1254002872,0.3288874209,0.2680012562,0.4470624302,0.5814774956,0.9355368656,0.6615960112,0.9828798384) [ #7 ]
	  #8: (0.7026967819,0.7732438994,0.6614730608,0.9346499368,0.085017812,0.839064103,0.3180151583,0.1074288493,0.8254372672) [ #9 ]
	  #10: (0.3117739127,0.1775910422,0.383365222,0.0224110372,0.9863731647,0.8460247199,0.0179714379,0.5034501541,0.1039613784) [ #11 ]
	Steiner points:
	  #3: (0.22468758649751147,0.49053314634346207,0.4967227455712486,0.39264556027971054,0.5185393254807369,0.501537306772486,0.6998652347834189,0.4994358020789989,0.5250642035280546) [ #5 #2 #7 ]
	  #5: (0.2985114285927075,0.3541263986160689,0.5201715615786634,0.30265439594848437,0.56236356770293,0.5265315086807302,0.604676058556589,0.5628284776168632,0.3060153159128487) [ #4 #3 #11 ]
	  #7: (0.2198424680538005,0.4253499913062242,0.45630195594133227,0.46815989274589564,0.394246065833203,0.5568561564797103,0.775581756607644,0.5391425220411746,0.6523562603125297) [ #6 #3 #9 ]
	  #9: (0.23738573620144626,0.48366438170258697,0.46887931313099,0.6210061783078354,0.2538990532056365,0.599747183569566,0.7839737644044908,0.5281478846246986,0.6408793101742866) [ #8 #7 #0 ]
	  #11: (0.41947167886359105,0.2140029687889704,0.35954360254789397,0.2401846955572094,0.8826515932761815,0.7477097619782394,0.20560235177304473,0.3845892204851303,0.1949818302373167) [ #1 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.143736653
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 963

	Init times:
		Bottleneck Steiner distances: 0.000021116
		Smallest sphere distances: 0.000001539
		Sorting: 0.000002347
		Total init time: 0.000026281

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000702, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003582, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032486, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000345155, bsd pruned: 0, ss pruned: 105

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 840
	Total number iterations 15784 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 18.79047619047619
	Total time: 0.142824274
	Avarage time pr. topology: 0.000170028

Data for the geometric median iteration:
	Total time: 0.137564578
	Total steps: 559881
	Average number of steps pr. geometric median: 133.305
	Average time pr. step: 0.0000002457032440822246
	Average time pr. geometric median: 0.00003275347095238095

