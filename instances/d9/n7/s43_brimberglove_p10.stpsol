All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 9.695748725124295
MST len: 12.0994565336
Steiner Ratio: 0.801337539268756
Solution Steiner tree:
	Terminals:
	  #0: (0.2578007133,0.9034257293,0.0485690669,0.5226562896,0.1556680115,0.8831310002,0.3024737403,0.0332014114,0.4304443828) [ #11 ]
	  #1: (0.8226312044,0.8875873521,0.8883300344,0.8879682691,0.2797363551,0.1298833849,0.3797864329,0.0226750942,0.2685777122) [ #9 ]
	  #2: (0.2147388822,0.6533952498,0.3377528034,0.6451832655,0.4760264542,0.2253401551,0.5335132994,0.3639947229,0.5050765101) [ #3 ]
	  #4: (0.3488384743,0.5206193517,0.4255304404,0.9535663561,0.1305656718,0.298230481,0.8778110425,0.8500182223,0.4839443832) [ #5 ]
	  #6: (0.9724066616,0.5704645065,0.2454576512,0.2830388156,0.8282652203,0.1488833801,0.3316078826,0.3509215095,0.304551392) [ #7 ]
	  #8: (0.6099463206,0.8727000411,0.9242446869,0.7194525505,0.1857139022,0.0945956191,0.7204462847,0.7615132685,0.3106321545) [ #9 ]
	  #10: (0.3921490034,0.2415533509,0.4918181642,0.7429387396,0.1386943274,0.9690520419,0.497944257,0.1569527281,0.6047278813) [ #11 ]
	Steiner points:
	  #3: (0.37851105548559677,0.6533952498,0.4255304404,0.7386489490462433,0.35461764912359683,0.2253401551,0.5335132994,0.3509215095,0.5050765101) [ #2 #5 #7 ]
	  #5: (0.37851105548559677,0.5206193517,0.4255304404,0.7386489490462433,0.15489658747466314,0.298230481,0.5335132994000001,0.3509215095,0.4839443832) [ #4 #3 #11 ]
	  #7: (0.7517362431333331,0.5704645065081411,0.4255304404,0.7386489490462433,0.35461764912359683,0.1488833801,0.49333971680192834,0.3509215095002982,0.304551392) [ #3 #6 #9 ]
	  #9: (0.751736243133333,0.8727000410999999,0.8883300344,0.7386489490462433,0.2797363551,0.1298833849,0.49333971680192834,0.3509215095002982,0.304551392) [ #1 #7 #8 ]
	  #11: (0.37851105548559677,0.5206193517,0.4255304404,0.7386489490462433,0.15489658747466314,0.8831310002,0.497944257,0.1569527281,0.4839443832) [ #0 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.040325809
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000036303
		Smallest sphere distances: 0.000005075
		Sorting: 0.000012977
		Total init time: 0.000056243

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002489, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000012752, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000116817, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001334292, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 1905 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.015873015873016
	Total time: 0.037237009
	Avarage time pr. topology: 0.000039404

Data for the geometric median iteration:
	Total time: 0.031818535
	Total steps: 32122
	Average number of steps pr. geometric median: 6.798306878306878
	Average time pr. step: 0.0000009905527364423138
	Average time pr. geometric median: 0.000006734081481481482

