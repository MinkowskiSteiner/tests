All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.901179603513391
MST len: 5.478819738897375
Steiner Ratio: 0.8945685087459667
Solution Steiner tree:
	Terminals:
	  #0: (0.0963502145,0.410173062,0.3672353534,0.6733629432,0.4161350468,0.1185037499,0.2078800305,0.3029427488,0.689293613) [ #7 ]
	  #1: (0.6533997663,0.55006754,0.2691321919,0.6820132009,0.3835517202,0.9017543196,0.6433020787,0.9163497486,0.5770127282) [ #9 ]
	  #2: (0.1237754617,0.5839534754,0.96214796,0.738404279,0.5816928249,0.128975798,0.6731079657,0.4309688389,0.6485643655) [ #11 ]
	  #4: (0.0059619849,0.751268397,0.5345170878,0.886807702,0.5707898632,0.9158954317,0.2810107266,0.2728543464,0.6146288172) [ #5 ]
	  #6: (0.0033729677,0.4314580166,0.517266345,0.9254637039,0.0848577824,0.0673338846,0.1945958953,0.7668709838,0.4508856048) [ #7 ]
	  #8: (0.9977393495,0.1668278385,0.9347036872,0.8492760145,0.5195885676,0.7242932826,0.7551018837,0.5634760026,0.6785582023) [ #9 ]
	  #10: (0.3527607286,0.7895895954,0.9058258696,0.043887435,0.9542649202,0.248271084,0.8679820145,0.8387081427,0.9220907362) [ #11 ]
	Steiner points:
	  #3: (0.1836187213670277,0.5416003558599063,0.6375223608632709,0.7316279344300566,0.4840072006745097,0.35308060844035,0.4533201383721302,0.48517644749387934,0.6401571893932579) [ #7 #5 #11 ]
	  #5: (0.2576743645012732,0.567519773314181,0.5904031692693735,0.7753348861305316,0.4974565582455319,0.5940402737177799,0.45499640756661475,0.48747906474274805,0.6290558563025956) [ #3 #4 #9 ]
	  #7: (0.10768677269484071,0.462883069318025,0.49911091136136604,0.7503408169843389,0.3677405428252183,0.19288521497815064,0.2945994481713961,0.47200140475385977,0.6186752611515233) [ #3 #6 #0 ]
	  #9: (0.5931811700767384,0.46031617135360337,0.5553130270374947,0.7585966112457727,0.4599241853190239,0.7431479708552522,0.6015779284715471,0.6687970393747711,0.621797005161035) [ #8 #5 #1 ]
	  #11: (0.16953265009996346,0.5954522556379346,0.8554563799290668,0.6534304958783984,0.5960746673137487,0.21222709846665683,0.6286805772883899,0.4963238707563391,0.6786192919928002) [ #2 #3 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.144326195
	Number of best updates: 8

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000024768
		Smallest sphere distances: 0.000001408
		Sorting: 0.000002265
		Total init time: 0.000029629

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000074, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003435, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032419, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000364586, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 16571 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 17.535449735449735
	Total time: 0.143382624
	Avarage time pr. topology: 0.000151727

Data for the geometric median iteration:
	Total time: 0.137811142
	Total steps: 558780
	Average number of steps pr. geometric median: 118.26031746031747
	Average time pr. step: 0.0000002466286230716919
	Average time pr. geometric median: 0.000029166379259259257

