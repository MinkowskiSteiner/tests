All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 10.452958180252425
MST len: 13.1517361445
Steiner Ratio: 0.7947968287535793
Solution Steiner tree:
	Terminals:
	  #0: (0.9571484462,0.5881108905,0.2816415891,0.9608924622,0.4010537497,0.5862380008,0.0521304696,0.9748592134,0.3832746434) [ #9 ]
	  #1: (0.9900835226,0.4085043023,0.6311877783,0.5804423944,0.4470295741,0.1396388189,0.7115480889,0.3690200771,0.6052383057) [ #3 ]
	  #2: (0.4828030474,0.7509467996,0.2591224784,0.303527225,0.3381346959,0.3982325915,0.3818814775,0.10839433,0.5903588718) [ #5 ]
	  #4: (0.5788986746,0.2402925525,0.4337119527,0.3327284527,0.7230956004,0.1846587524,0.5918509311,0.0266228248,0.5227934483) [ #5 ]
	  #6: (0.789870093,0.4786224181,0.0951295472,0.1731447364,0.2480481846,0.4273886245,0.9502985026,0.8603497673,0.402739647) [ #7 ]
	  #8: (0.0385252712,0.5084510411,0.1311056945,0.9219905035,0.4655994868,0.719216585,0.2036320922,0.4264919485,0.1202703342) [ #9 ]
	  #10: (0.7694257664,0.3322590768,0.7771537661,0.6123015828,0.975351023,0.6286001725,0.3799427857,0.0309723057,0.7538297781) [ #11 ]
	Steiner points:
	  #3: (0.837835817152426,0.4617197461777778,0.47560148207777775,0.557279582777778,0.44702957410000005,0.4273886245,0.46143681656979424,0.3690200771,0.6052383057) [ #7 #1 #11 ]
	  #5: (0.5788986746,0.4617197461777778,0.4337119527,0.3327284527,0.6231367237333334,0.3982325915,0.4342721396131961,0.10839433,0.5903588718) [ #2 #4 #11 ]
	  #7: (0.837835817152426,0.4786224181000001,0.21073939735555555,0.557279582777778,0.40556837705556587,0.4273886245,0.46143681656979424,0.7120313937222221,0.402739647) [ #6 #3 #9 ]
	  #9: (0.837835817152426,0.5084510411,0.21073939735555555,0.9219905035000001,0.4055683770555659,0.5862380008,0.2036320922,0.7120313937222221,0.402739647) [ #8 #7 #0 ]
	  #11: (0.7694257664,0.4617197461777778,0.47560148207777775,0.557279582777778,0.6231367237333334,0.4273886245,0.4342721396131961,0.10839433000000001,0.6052383057) [ #3 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.041266227
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000022993
		Smallest sphere distances: 0.000005142
		Sorting: 0.000012238
		Total init time: 0.000042209

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002453, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000012748, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00011638, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001331198, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 1905 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.015873015873016
	Total time: 0.038164424
	Avarage time pr. topology: 0.000040385

Data for the geometric median iteration:
	Total time: 0.032756512
	Total steps: 33428
	Average number of steps pr. geometric median: 7.0747089947089945
	Average time pr. step: 0.000000979912408759124
	Average time pr. geometric median: 0.0000069325951322751324

