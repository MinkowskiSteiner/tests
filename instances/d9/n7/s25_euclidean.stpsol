All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.01310341659458
MST len: 5.673237713459493
Steiner Ratio: 0.8836406422211475
Solution Steiner tree:
	Terminals:
	  #0: (0.7898902324,0.7133313109,0.2344475841,0.283649136,0.6526875024,0.0401867987,0.325981314,0.631247768,0.3657580602) [ #5 ]
	  #1: (0.6641981568,0.1102642064,0.0404164344,0.3145068122,0.9001544388,0.7537477458,0.5489543963,0.1838035747,0.4064352477) [ #3 ]
	  #2: (0.5821549858,0.7011187615,0.5075186931,0.9913254278,0.7521004219,0.7428126711,0.9414201057,0.9901046399,0.0505868262) [ #11 ]
	  #4: (0.7723662428,0.4866999432,0.7424799096,0.5370173396,0.9999918654,0.1990076612,0.6581573387,0.8989526019,0.6503086554) [ #7 ]
	  #6: (0.6721237603,0.1427232857,0.6547720249,0.7227105865,0.915089529,0.1414719681,0.4651904956,0.4521068681,0.141463833) [ #7 ]
	  #8: (0.3181859326,0.6278227673,0.2114482025,0.4217923961,0.2099777531,0.912566964,0.9293110892,0.2013031804,0.6646673855) [ #9 ]
	  #10: (0.939356192,0.805739215,0.042332178,0.9785602656,0.3255712615,0.9922046186,0.9965749998,0.8456901428,0.1036064635) [ #11 ]
	Steiner points:
	  #3: (0.6490617424483833,0.4130471627890856,0.24398093180413175,0.47459499327491617,0.7089013293502398,0.584446925377372,0.6365546379080423,0.4421534730842743,0.3931299299014012) [ #5 #9 #1 ]
	  #5: (0.7145936327238481,0.4705105008924629,0.3869948902745924,0.4678473138943217,0.7715766006775286,0.2871667820743976,0.5182375968938123,0.572419717480501,0.3837012046336913) [ #7 #3 #0 ]
	  #7: (0.7176544802016025,0.3936113658500999,0.5360697655259329,0.5467644002033345,0.8603372312003079,0.2306798327910816,0.5379118636615258,0.6191914638430659,0.38644200970689885) [ #6 #4 #5 ]
	  #9: (0.5868121008992141,0.5415327578314848,0.2450844630393873,0.5724083058087714,0.5467692103975669,0.7285486053831614,0.7835089399901368,0.4851314842154967,0.3933438220471877) [ #8 #3 #11 ]
	  #11: (0.7025460644926219,0.6885084913644485,0.27448006908974903,0.8624735773566546,0.5489251776253954,0.8216580419112255,0.9128279342900596,0.7917964780634716,0.17019755213669135) [ #2 #9 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.137894747
	Number of best updates: 7

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.00002083
		Smallest sphere distances: 0.000001343
		Sorting: 0.000002417
		Total init time: 0.000025806

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000718, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003449, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032202, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000364993, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 16558 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 17.521693121693122
	Total time: 0.136956972
	Avarage time pr. topology: 0.000144928

Data for the geometric median iteration:
	Total time: 0.13137206
	Total steps: 534505
	Average number of steps pr. geometric median: 113.12275132275133
	Average time pr. step: 0.00000024578265872162096
	Average time pr. geometric median: 0.000027803610582010585

