All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 10.547947500361209
MST len: 13.2744037547
Steiner Ratio: 0.7946080061506756
Solution Steiner tree:
	Terminals:
	  #0: (0.958641605,0.1473828816,0.0937726004,0.1534282836,0.0076622083,0.4441131705,0.5998500383,0.0285220109,0.4814383762) [ #11 ]
	  #1: (0.7510317898,0.6239873639,0.9691653433,0.0583464685,0.4434102766,0.4754556192,0.1466605273,0.2289445811,0.8166331438) [ #7 ]
	  #2: (0.6861238385,0.9269996448,0.8449453608,0.1197466637,0.0576706543,0.2857596349,0.6257388632,0.607333408,0.320081121) [ #3 ]
	  #4: (0.729074478,0.3766270487,0.206070881,0.2581979913,0.0627508867,0.1330705258,0.1031433517,0.1824975504,0.1907411801) [ #9 ]
	  #6: (0.3889029866,0.808236414,0.7980745881,0.7089841076,0.5592682034,0.4220619516,0.6781494509,0.6176146719,0.8654722287) [ #7 ]
	  #8: (0.8602793272,0.35034057,0.4464217548,0.0208598026,0.0373252058,0.1292244392,0.3481050377,0.7246325052,0.5291235137) [ #9 ]
	  #10: (0.8194229132,0.5062902763,0.0883140588,0.7855343049,0.3411775247,0.8119810777,0.9184383005,0.2771394566,0.1947866791) [ #11 ]
	Steiner points:
	  #3: (0.7510317897999957,0.6239873639,0.7980745881,0.11974666370472194,0.0627508867,0.4441131705,0.5998500385065477,0.2628617037501166,0.3200811210000001) [ #7 #5 #2 ]
	  #5: (0.7510317897999957,0.3678648891333334,0.2861878389333333,0.14416202583333335,0.0627508867,0.4441131705,0.5998500385065477,0.2628617037501105,0.3200811210000001) [ #3 #9 #11 ]
	  #7: (0.7510317898,0.6239873639,0.7980745881,0.11974666370472194,0.4434102766,0.4754556192,0.6781494509,0.2628617037501166,0.8166331438) [ #1 #6 #3 ]
	  #9: (0.7510317897999956,0.3678648891333334,0.2861878389333333,0.14416202583333335,0.06274778335801294,0.1330705258,0.3481050377,0.2628617037501105,0.3200811210000001) [ #4 #5 #8 ]
	  #11: (0.8194229132,0.3678648891333334,0.0937726004,0.1534282836,0.0627508867,0.4441145737993465,0.599851253716412,0.2628617037501105,0.3200811210000001) [ #0 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.041259491
	Number of best updates: 9

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000022876
		Smallest sphere distances: 0.000005516
		Sorting: 0.000024374
		Total init time: 0.000054655

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002399, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000012578, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000115951, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001349765, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 1909 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.02010582010582
	Total time: 0.038118582
	Avarage time pr. topology: 0.000040337

Data for the geometric median iteration:
	Total time: 0.032738229
	Total steps: 33075
	Average number of steps pr. geometric median: 7
	Average time pr. step: 0.0000009898179591836734
	Average time pr. geometric median: 0.000006928725714285714

