All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 10.949750824881969
MST len: 13.1097943353
Steiner Ratio: 0.8352343709464759
Solution Steiner tree:
	Terminals:
	  #0: (0.1360618449,0.4750343694,0.4330457674,0.714648994,0.1368193762,0.5865269595,0.9152492289,0.625351081,0.5795209741) [ #9 ]
	  #1: (0.6617850073,0.153481192,0.2006002349,0.4885317048,0.9929940151,0.7874475535,0.1478928184,0.0819520862,0.2319531549) [ #11 ]
	  #2: (0.1488122675,0.7401193766,0.5311698176,0.3798184727,0.6551358368,0.3903584468,0.6938135543,0.3702549456,0.5785871486) [ #3 ]
	  #4: (0.3216621426,0.2525900748,0.6675356089,0.214533581,0.4014023423,0.4076549855,0.7457033986,0.781220815,0.0627908218) [ #7 ]
	  #6: (0.1640398778,0.1254002872,0.3288874209,0.2680012562,0.4470624302,0.5814774956,0.9355368656,0.6615960112,0.9828798384) [ #7 ]
	  #8: (0.7026967819,0.7732438994,0.6614730608,0.9346499368,0.085017812,0.839064103,0.3180151583,0.1074288493,0.8254372672) [ #9 ]
	  #10: (0.3117739127,0.1775910422,0.383365222,0.0224110372,0.9863731647,0.8460247199,0.0179714379,0.5034501541,0.1039613784) [ #11 ]
	Steiner points:
	  #3: (0.3216621426,0.5581398727022008,0.433045767400003,0.3798184727,0.4014023423113932,0.5865269595,0.693812120748355,0.5089592642127552,0.5785871486) [ #2 #5 #9 ]
	  #5: (0.3216621426,0.21019347893333334,0.43304576740000295,0.3798184727,0.4014023423113934,0.5865269595,0.693812120748355,0.5089592642127552,0.4258746049752441) [ #7 #3 #11 ]
	  #7: (0.3216597375118086,0.21019347893333334,0.433045767400003,0.2680012562,0.4014030390127155,0.5814774956,0.7457033985999999,0.6615960112,0.4258746049752441) [ #4 #5 #6 ]
	  #9: (0.3216621426,0.5581398727022008,0.43304751014864834,0.714648994,0.1368193762,0.5865288861908021,0.693812120748355,0.5089592642127552,0.5795209741) [ #8 #3 #0 ]
	  #11: (0.3216621426,0.1775910422,0.38336522200000006,0.37981847269999996,0.9863731647,0.7874475535,0.1478928184,0.5034501541,0.2319531549) [ #1 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.038409956
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 963

	Init times:
		Bottleneck Steiner distances: 0.00002477
		Smallest sphere distances: 0.000005589
		Sorting: 0.000015977
		Total init time: 0.000047695

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002333, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000013126, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000131249, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001231544, bsd pruned: 0, ss pruned: 105

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 840
	Total number iterations 1707 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.032142857142857
	Total time: 0.035449463
	Avarage time pr. topology: 0.000042201

Data for the geometric median iteration:
	Total time: 0.030519815
	Total steps: 30809
	Average number of steps pr. geometric median: 7.33547619047619
	Average time pr. step: 0.0000009906136193969294
	Average time pr. geometric median: 0.0000072666226190476185

Data for the geometric median step function:
	Total number of fixed points encountered: 186593

