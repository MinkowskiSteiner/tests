All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 11.109259644919701
MST len: 14.359938283699998
Steiner Ratio: 0.7736286483577614
Solution Steiner tree:
	Terminals:
	  #0: (0.0544752507,0.9522628411,0.3267083561,0.696483258,0.4432977626,0.4120336987,0.1106646979,0.7293048565,0.4997239469) [ #3 ]
	  #1: (0.3197871825,0.7856299336,0.5977471213,0.2664912102,0.5623366006,0.6297007662,0.9481091769,0.3717675406,0.4472546938) [ #9 ]
	  #2: (0.7856002826,0.4401777473,0.1151188249,0.8235438768,0.6883942167,0.1299254853,0.3111650372,0.5165184259,0.6420080073) [ #7 ]
	  #4: (0.4757024336,0.2096587369,0.1391944774,0.6059281829,0.9868395505,0.0341578741,0.4251679771,0.5886231538,0.4121524544) [ #5 ]
	  #6: (0.776706667,0.031953645,0.6816179667,0.8094309405,0.8175539276,0.1217957135,0.9245497658,0.6410978039,0.8101899306) [ #7 ]
	  #8: (0.7771808131,0.8949633971,0.8192397947,0.6017836037,0.3779945804,0.8946192055,0.1970067798,0.1855946668,0.9467040281) [ #9 ]
	  #10: (0.4910349215,0.0853253426,0.4141814399,0.2860070939,0.0876902482,0.3650377357,0.4803538809,0.6394705305,0.1302257493) [ #11 ]
	Steiner points:
	  #3: (0.48820936940617,0.7856299336,0.3267083561,0.49300503966671466,0.5623259456610248,0.4120336987,0.4251679770999998,0.603044124753555,0.4121524574068082) [ #0 #9 #11 ]
	  #5: (0.4891512201041134,0.32188714155833653,0.1391944774,0.6059281829,0.6883942167,0.1299254853,0.4251679770999998,0.584830921880863,0.4121524544) [ #4 #7 #11 ]
	  #7: (0.776706667,0.32188714155833653,0.1391944774,0.8094309405,0.6883942167,0.1299254853,0.4251679770999998,0.584830921880863,0.6420080073) [ #5 #6 #2 ]
	  #9: (0.48820936940617,0.7856299336,0.5977471213,0.49300503966671466,0.5623259456610248,0.6297007662,0.4251679770999998,0.3717675406,0.4121524574068082) [ #8 #3 #1 ]
	  #11: (0.4891512201041134,0.32188714155833653,0.3267083561,0.4930050396667148,0.5623259456610248,0.3650377357,0.4251683981318202,0.603044124753555,0.41215030498942484) [ #3 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.041133169
	Number of best updates: 13

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000023652
		Smallest sphere distances: 0.000006014
		Sorting: 0.000016105
		Total init time: 0.000047474

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002021, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00001283, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000129772, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001352236, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 1918 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0296296296296297
	Total time: 0.037904873
	Avarage time pr. topology: 0.00004011

Data for the geometric median iteration:
	Total time: 0.03236881
	Total steps: 32900
	Average number of steps pr. geometric median: 6.962962962962963
	Average time pr. step: 0.0000009838544072948328
	Average time pr. geometric median: 0.000006850541798941798

Data for the geometric median step function:
	Total number of fixed points encountered: 190535

