All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 8.685008418298136
MST len: 10.9763286303
Steiner Ratio: 0.7912489422304005
Solution Steiner tree:
	Terminals:
	  #0: (0.6861238385,0.9269996448,0.8449453608,0.1197466637,0.0576706543,0.2857596349,0.6257388632,0.607333408,0.320081121) [ #5 ]
	  #1: (0.958641605,0.1473828816,0.0937726004,0.1534282836,0.0076622083,0.4441131705,0.5998500383,0.0285220109,0.4814383762) [ #3 ]
	  #2: (0.729074478,0.3766270487,0.206070881,0.2581979913,0.0627508867,0.1330705258,0.1031433517,0.1824975504,0.1907411801) [ #9 ]
	  #4: (0.7510317898,0.6239873639,0.9691653433,0.0583464685,0.4434102766,0.4754556192,0.1466605273,0.2289445811,0.8166331438) [ #5 ]
	  #6: (0.8602793272,0.35034057,0.4464217548,0.0208598026,0.0373252058,0.1292244392,0.3481050377,0.7246325052,0.5291235137) [ #7 ]
	  #8: (0.8194229132,0.5062902763,0.0883140588,0.7855343049,0.3411775247,0.8119810777,0.9184383005,0.2771394566,0.1947866791) [ #9 ]
	Steiner points:
	  #3: (0.8026431610566988,0.37662704869999997,0.16681860693333334,0.1534282836,0.0627508867,0.44411317049999993,0.5245646464000001,0.21404485246666669,0.4814383762) [ #1 #7 #9 ]
	  #5: (0.7510317898,0.6239873639,0.8449453608000002,0.10923878993333333,0.0627508867,0.4754556192,0.5245646464000001,0.3800645516074742,0.4973334196531366) [ #4 #7 #0 ]
	  #7: (0.8026431610566988,0.37662704869999997,0.4464217548,0.10923878993333333,0.0627508867,0.44411317049999993,0.5245646464000001,0.3800645516074742,0.4973334196531366) [ #3 #5 #6 ]
	  #9: (0.8026431610566988,0.37662803794437216,0.16681860693333334,0.2581979913,0.06275301091046287,0.44411317049999993,0.5245646464000001,0.21404485246666669,0.1947866791) [ #2 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.004128379
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000013281
		Smallest sphere distances: 0.000015674
		Sorting: 0.000006728
		Total init time: 0.000036873

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002147, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000012744, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000122438, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 212 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.019047619047619
	Total time: 0.003783738
	Avarage time pr. topology: 0.000036035

Data for the geometric median iteration:
	Total time: 0.003287498
	Total steps: 3241
	Average number of steps pr. geometric median: 7.716666666666667
	Average time pr. step: 0.0000010143468065411909
	Average time pr. geometric median: 0.00000782737619047619

