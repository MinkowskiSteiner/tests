All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 8.954561029778505
MST len: 10.4728672471
Steiner Ratio: 0.8550247815141624
Solution Steiner tree:
	Terminals:
	  #0: (0.6610816175,0.5074669032,0.561359823,0.6648593674,0.4017881259,0.1900027982,0.7689555445,0.3776398615,0.861473049) [ #7 ]
	  #1: (0.0842270214,0.5216057671,0.066293479,0.7319970623,0.6132460584,0.5875373467,0.2723092908,0.7436478691,0.6444432082) [ #3 ]
	  #2: (0.2765600068,0.3932821464,0.0769409025,0.5114291709,0.5846134669,0.5077402124,0.0594066722,0.5997367695,0.5458334356) [ #9 ]
	  #4: (0.0916402913,0.5212438672,0.540312229,0.1304018107,0.0569058615,0.9637256646,0.384026217,0.8656925191,0.3785816228) [ #5 ]
	  #6: (0.1913313205,0.4307993098,0.5479775013,0.0151233026,0.0380932228,0.0248203492,0.9218689981,0.5204600438,0.6477700405) [ #7 ]
	  #8: (0.8943212232,0.6286429756,0.1040961771,0.975851736,0.6714702508,0.5076044628,0.0156422849,0.215467854,0.2348691641) [ #9 ]
	Steiner points:
	  #3: (0.2765600068000022,0.5009430142111111,0.08599266070000004,0.6662366926000001,0.6132460491643495,0.5273850828555555,0.2723092908,0.605669071322222,0.6413237453555556) [ #1 #5 #9 ]
	  #5: (0.2765600068000022,0.5009430142111111,0.5182831955259045,0.3857004875889174,0.25683932413703703,0.5273850828555555,0.384026217,0.605669071322222,0.6413237453555556) [ #4 #3 #7 ]
	  #7: (0.2765600068000022,0.5009430142111111,0.5182831955259045,0.3857004875889174,0.25683932413703703,0.1900027982,0.7689555444999999,0.5204600438,0.6413237453555556) [ #6 #5 #0 ]
	  #9: (0.27656471990808895,0.5009430142111111,0.08599266070000006,0.6662366926,0.6132460491643495,0.5077402124000001,0.0594066722,0.5997367695000001,0.5458334356) [ #2 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.003706487
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 108

	Init times:
		Bottleneck Steiner distances: 0.00001328
		Smallest sphere distances: 0.00000436
		Sorting: 0.000006538
		Total init time: 0.00002574

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002555, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000013174, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000103594, bsd pruned: 15, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 90
	Total number iterations 182 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.022222222222222
	Total time: 0.00340642
	Avarage time pr. topology: 0.000037849

Data for the geometric median iteration:
	Total time: 0.002991159
	Total steps: 3089
	Average number of steps pr. geometric median: 8.580555555555556
	Average time pr. step: 0.000000968325995467789
	Average time pr. geometric median: 0.000008308775

