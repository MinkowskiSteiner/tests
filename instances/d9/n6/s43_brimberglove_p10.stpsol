All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 9.25939513718792
MST len: 10.929584656700001
Steiner Ratio: 0.8471863687438268
Solution Steiner tree:
	Terminals:
	  #0: (0.3488384743,0.5206193517,0.4255304404,0.9535663561,0.1305656718,0.298230481,0.8778110425,0.8500182223,0.4839443832) [ #7 ]
	  #1: (0.2578007133,0.9034257293,0.0485690669,0.5226562896,0.1556680115,0.8831310002,0.3024737403,0.0332014114,0.4304443828) [ #9 ]
	  #2: (0.8226312044,0.8875873521,0.8883300344,0.8879682691,0.2797363551,0.1298833849,0.3797864329,0.0226750942,0.2685777122) [ #5 ]
	  #4: (0.9724066616,0.5704645065,0.2454576512,0.2830388156,0.8282652203,0.1488833801,0.3316078826,0.3509215095,0.304551392) [ #5 ]
	  #6: (0.6099463206,0.8727000411,0.9242446869,0.7194525505,0.1857139022,0.0945956191,0.7204462847,0.7615132685,0.3106321545) [ #7 ]
	  #8: (0.3921490034,0.2415533509,0.4918181642,0.7429387396,0.1386943274,0.9690520419,0.497944257,0.1569527281,0.6047278813) [ #9 ]
	Steiner points:
	  #3: (0.49360113072943657,0.732371220774074,0.5673532751901196,0.6863251179451326,0.17777747954814732,0.23035219369999999,0.3637269261570671,0.24150603710180757,0.4009722815109066) [ #5 #7 #9 ]
	  #5: (0.8226312044,0.732371220774074,0.5673532751901196,0.6863251179451326,0.2797363551,0.1488833801,0.3637269261570671,0.24150603710180757,0.304551392) [ #2 #3 #4 ]
	  #7: (0.49360113072943657,0.732371220774074,0.5673532751901196,0.7194525505,0.17777747954814732,0.23035219369999999,0.7204462847000002,0.7615132685000001,0.4009722815109066) [ #6 #3 #0 ]
	  #9: (0.39214900340000003,0.7323712207740739,0.4918181642,0.6863251179451326,0.1556680115,0.8831310001999999,0.3637269261570671,0.1569527281,0.4304443828) [ #1 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.004228164
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000013207
		Smallest sphere distances: 0.000003174
		Sorting: 0.000006713
		Total init time: 0.00002419

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002245, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000013245, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000125386, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 211 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0095238095238095
	Total time: 0.00389246
	Avarage time pr. topology: 0.000037071

Data for the geometric median iteration:
	Total time: 0.003374414
	Total steps: 3241
	Average number of steps pr. geometric median: 7.716666666666667
	Average time pr. step: 0.0000010411644554149953
	Average time pr. geometric median: 0.000008034319047619048

