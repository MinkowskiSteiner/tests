All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.810747524874603
MST len: 4.190160724495838
Steiner Ratio: 0.909451397078119
Solution Steiner tree:
	Terminals:
	  #0: (0.1354387687,0.9625167521,0.799727213,0.4030091993,0.0851281491,0.8056346727,0.6451566176,0.2473998085,0.3098868776) [ #9 ]
	  #1: (0.9831179767,0.5296626759,0.9438327159,0.87826215,0.7782872272,0.4166978609,0.2226562315,0.5511224272,0.0462225136) [ #5 ]
	  #2: (0.6177704891,0.4252223039,0.3870529828,0.7761541152,0.6623876005,0.1703382573,0.3191112491,0.3110007436,0.691104038) [ #3 ]
	  #4: (0.8889893428,0.2456402682,0.9563567112,0.9352118568,0.8634107573,0.3815790147,0.3222648391,0.6395648721,0.0439666151) [ #5 ]
	  #6: (0.2185718884,0.16160391,0.0972843804,0.5284587664,0.1447218867,0.6269470568,0.4722914819,0.0229840363,0.4052342835) [ #7 ]
	  #8: (0.1034285073,0.9779993249,0.8022396713,0.6132100036,0.1134380932,0.7647564228,0.4129372162,0.5164472929,0.8498845724) [ #9 ]
	Steiner points:
	  #3: (0.5906286952469886,0.4403093197678585,0.5016703787438501,0.7473262113404726,0.5814104191279579,0.32463843117035723,0.3461993545141311,0.33776484680829666,0.5183564218280807) [ #5 #2 #7 ]
	  #5: (0.9017141314159909,0.4112060746958719,0.8972440775722854,0.8847257783090536,0.7878131945953156,0.3926419953355352,0.27490895646820834,0.559997199671739,0.10019947075775211) [ #1 #3 #4 ]
	  #7: (0.3454959308999792,0.484373995580452,0.4445170882630718,0.6104749685725932,0.3112950072970847,0.5446456370878017,0.4378669025523276,0.2416111483214626,0.49079714240784855) [ #6 #3 #9 ]
	  #9: (0.17941642749070286,0.8429401705499977,0.7078436883358807,0.5285056070538616,0.15378153419348403,0.7236250166797162,0.5122340739985887,0.3371908801000739,0.5403682273505901) [ #0 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.006785963
	Number of best updates: 2

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 93

	Init times:
		Bottleneck Steiner distances: 0.000005885
		Smallest sphere distances: 0.000001142
		Sorting: 0.000001802
		Total init time: 0.000009665

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000717, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003755, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000026548, bsd pruned: 0, ss pruned: 30

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 75
	Total number iterations 970 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 12.933333333333334
	Total time: 0.00668786
	Avarage time pr. topology: 0.000089171

Data for the geometric median iteration:
	Total time: 0.006411985
	Total steps: 24720
	Average number of steps pr. geometric median: 82.4
	Average time pr. step: 0.00000025938450647249194
	Average time pr. geometric median: 0.000021373283333333338

