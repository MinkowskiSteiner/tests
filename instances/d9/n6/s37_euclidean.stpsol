All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.467198665055147
MST len: 5.131470686782574
Steiner Ratio: 0.8705493878317514
Solution Steiner tree:
	Terminals:
	  #0: (0.6398503048,0.8193347011,0.9970282237,0.1806596616,0.0587470937,0.758170273,0.3551814539,0.8781711272,0.7902449909) [ #3 ]
	  #1: (0.5980566296,0.3121092172,0.450471332,0.8071258347,0.3703712553,0.6420550997,0.9574638638,0.1660335614,0.7214625924) [ #7 ]
	  #2: (0.3599697064,0.5693105229,0.5225199175,0.0814322983,0.1616002024,0.4476734644,0.691449677,0.8515732548,0.8775660274) [ #3 ]
	  #4: (0.9976388258,0.2066641954,0.6740058836,0.2674432752,0.8047208254,0.9861151008,0.7179146072,0.6118466596,0.3564863561) [ #5 ]
	  #6: (0.59228968,0.9251535474,0.6100173786,0.6899730524,0.429892563,0.1510021729,0.9164035199,0.1465295028,0.1651677811) [ #7 ]
	  #8: (0.2394123931,0.7611420498,0.1745217923,0.8194240331,0.0320747178,0.6424573714,0.3284930686,0.8837608927,0.2698044499) [ #9 ]
	Steiner points:
	  #3: (0.4807071743698633,0.6456679798603502,0.6318875125130758,0.25689678916319497,0.1664864080333304,0.5912506249824719,0.5696038517978242,0.7981880020211647,0.7274162618803945) [ #0 #2 #9 ]
	  #5: (0.6351052793552668,0.5033443843957641,0.5477188530580831,0.5152881969219407,0.4069382467578132,0.6477191518418753,0.7274959210654701,0.5102874088376166,0.4986453423013888) [ #4 #7 #9 ]
	  #7: (0.6157644090444052,0.5228074601693086,0.5293701983190561,0.6374668712791994,0.39995134968253343,0.5537944479472586,0.833284951259991,0.33689827780831677,0.5052888267451391) [ #6 #5 #1 ]
	  #9: (0.4959312237922581,0.612015073698376,0.5155571465210773,0.45964477005408527,0.23447320079277695,0.6224009799833967,0.5862078457618473,0.702960472341891,0.5553268763856557) [ #3 #5 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.01338904
	Number of best updates: 8

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000005722
		Smallest sphere distances: 0.000001263
		Sorting: 0.000001596
		Total init time: 0.000009421

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000712, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003663, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000045408, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 1765 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 16.80952380952381
	Total time: 0.013263574
	Avarage time pr. topology: 0.000126319

Data for the geometric median iteration:
	Total time: 0.012775372
	Total steps: 49857
	Average number of steps pr. geometric median: 118.70714285714286
	Average time pr. step: 0.0000002562402872214534
	Average time pr. geometric median: 0.000030417552380952384

