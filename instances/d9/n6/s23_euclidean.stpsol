All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.388070052155337
MST len: 4.88479080829317
Steiner Ratio: 0.8983127884832809
Solution Steiner tree:
	Terminals:
	  #0: (0.9014610839,0.1763991002,0.6140612409,0.9471684238,0.6104576749,0.9758801474,0.721258364,0.4597480886,0.253197706) [ #9 ]
	  #1: (0.9255737224,0.0517316135,0.6330953071,0.5835098287,0.7793133598,0.1173111168,0.526778261,0.1237135954,0.5803137503) [ #5 ]
	  #2: (0.7275817463,0.4842158102,0.9432684327,0.3444002356,0.4630026335,0.5578971945,0.2818813474,0.2621717841,0.5012496633) [ #3 ]
	  #4: (0.5512441045,0.0618778663,0.8283642204,0.0872076736,0.9633389502,0.0047633201,0.7012689145,0.9105073739,0.615220995) [ #5 ]
	  #6: (0.0846754555,0.4055949428,0.8424855344,0.5859251188,0.3273033064,0.5324583159,0.7797718648,0.0267614122,0.6903227464) [ #7 ]
	  #8: (0.921708364,0.6899727819,0.193846746,0.6994581058,0.1578644305,0.7714722398,0.0351164541,0.1380414735,0.5359635696) [ #9 ]
	Steiner points:
	  #3: (0.6923873622066464,0.39531045880437377,0.7779003847449323,0.4956811935338027,0.48557837435977336,0.5526721355560518,0.4188568110385601,0.25405084259524585,0.5158566138754398) [ #2 #7 #9 ]
	  #5: (0.7367961401918769,0.1668210374846662,0.7234807361781523,0.46092020728699623,0.7103288688092352,0.2360683619306614,0.5487277666710186,0.31019974247331433,0.576931118995072) [ #4 #1 #7 ]
	  #7: (0.6186330054927037,0.3385809557881114,0.7730860204872385,0.4994629424886512,0.5206421324785585,0.46924164453795003,0.5024358626587769,0.23653211383544495,0.5558246094773808) [ #3 #5 #6 ]
	  #9: (0.7968921587909455,0.40706504359925033,0.6043830000142795,0.6553183608960955,0.44270344611551976,0.7086066721491779,0.40789942258925455,0.27945719209793823,0.45440438321910603) [ #0 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.017128948
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000018639
		Smallest sphere distances: 0.000001519
		Sorting: 0.000001856
		Total init time: 0.000022996

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000762, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003789, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000033154, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 2246 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 21.39047619047619
	Total time: 0.017004051
	Avarage time pr. topology: 0.000161943

Data for the geometric median iteration:
	Total time: 0.016389556
	Total steps: 65547
	Average number of steps pr. geometric median: 156.06428571428572
	Average time pr. step: 0.0000002500428089767648
	Average time pr. geometric median: 0.00003902275238095239

