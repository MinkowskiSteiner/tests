All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 9.102583149983626
MST len: 11.0220724668
Steiner Ratio: 0.8258504176416787
Solution Steiner tree:
	Terminals:
	  #0: (0.8251469377,0.5661086378,0.4243843828,0.5732345397,0.7447643731,0.6941410954,0.7210808409,0.0502029169,0.8113621514) [ #3 ]
	  #1: (0.1333542867,0.6234359283,0.2597159544,0.6775168025,0.4244183676,0.8089938242,0.7790788094,0.2599427934,0.6323595734) [ #9 ]
	  #2: (0.4496756841,0.6990365287,0.6652369316,0.7215187111,0.8656997224,0.5431189162,0.5266172777,0.9129774263,0.7480876026) [ #5 ]
	  #4: (0.9132866975,0.7154636065,0.5759032646,0.8231241507,0.8488178932,0.1993391929,0.0828401046,0.5263346953,0.6237575606) [ #5 ]
	  #6: (0.1754854299,0.8682969659,0.9466707064,0.6167910903,0.3179726495,0.6457072346,0.2820280214,0.0394913601,0.5114069565) [ #7 ]
	  #8: (0.1786557348,0.2697567126,0.1478463012,0.3054385443,0.1172210561,0.1922058562,0.6652606896,0.7645411136,0.9098374536) [ #9 ]
	Steiner points:
	  #3: (0.4496756841,0.6990365286999127,0.5597693743111112,0.6572748984333331,0.7447643731,0.5431189162,0.5409331787407406,0.2599427934,0.7480876026) [ #7 #5 #0 ]
	  #5: (0.4496756841,0.6990365287,0.5759032646,0.7215187111,0.8488178931999999,0.5431189162,0.5266172777,0.5263346953,0.7480876026) [ #2 #4 #3 ]
	  #7: (0.1754854299,0.6990365286999127,0.5597693743111112,0.6572748984333331,0.3220192650351862,0.6457072346,0.5409331787407406,0.2599427934,0.7248522001333333) [ #6 #3 #9 ]
	  #9: (0.1754854299,0.6234359283,0.2597159544,0.6572748984333331,0.3220192650351862,0.6457072346,0.6652606896,0.25994664315029337,0.7248522001333333) [ #1 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.00453736
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000012475
		Smallest sphere distances: 0.000003184
		Sorting: 0.000006775
		Total init time: 0.00002339

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002186, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000013032, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000122754, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 216 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.057142857142857
	Total time: 0.004175915
	Avarage time pr. topology: 0.00003977

Data for the geometric median iteration:
	Total time: 0.003635741
	Total steps: 3489
	Average number of steps pr. geometric median: 8.307142857142857
	Average time pr. step: 0.0000010420581828604185
	Average time pr. geometric median: 0.00000865652619047619

