All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 8.963177955128913
MST len: 11.2815182552
Steiner Ratio: 0.794501037216113
Solution Steiner tree:
	Terminals:
	  #0: (0.6782248629,0.940597109,0.1648225715,0.3753514985,0.6495995315,0.774436301,0.8460808666,0.2345622611,0.808130308) [ #5 ]
	  #1: (0.7090024225,0.6096137295,0.470729368,0.5849627301,0.033694007,0.4157471649,0.461066215,0.6987889817,0.7545507828) [ #3 ]
	  #2: (0.5244897048,0.4379004908,0.0703035561,0.1812310276,0.1675375119,0.7022328906,0.1086744881,0.7979400231,0.6606961906) [ #9 ]
	  #4: (0.7263199448,0.4611567121,0.784020462,0.387016135,0.8923050812,0.582232727,0.5161556138,0.8994785263,0.355014119) [ #5 ]
	  #6: (0.7532587111,0.6467614917,0.3806424362,0.9537834986,0.171251196,0.818542927,0.0240870542,0.352482224,0.9860804393) [ #7 ]
	  #8: (0.4311483691,0.7982122655,0.1291394788,0.0071734451,0.7727813925,0.162069249,0.0411185827,0.809808453,0.6971266361) [ #9 ]
	Steiner points:
	  #3: (0.6782248623908463,0.6467614747129742,0.38064184211677154,0.3753514985,0.1687754066,0.5822327273810363,0.461066215,0.6987893637846697,0.6728396772559347) [ #7 #5 #1 ]
	  #5: (0.6782248623908463,0.6467614747129742,0.38064184211677154,0.3753514985,0.6495995315,0.5822327273810363,0.5161556138,0.6987893637846697,0.6728396772559347) [ #4 #3 #0 ]
	  #7: (0.6782248623908462,0.6467614746402628,0.3806418421167716,0.3753514985,0.1687754066,0.5822327273861857,0.08615585296666665,0.6987893637846697,0.6728396772559347) [ #6 #3 #9 ]
	  #9: (0.5244897048000001,0.6467614746402628,0.1291394788,0.18123102759999998,0.16877540659999998,0.5822327273861857,0.08615585296666665,0.7979400231,0.6728396772559347) [ #2 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.004218468
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.00001814
		Smallest sphere distances: 0.000005053
		Sorting: 0.000006554
		Total init time: 0.00003191

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000003513, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000013371, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000121332, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 210 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.003857794
	Avarage time pr. topology: 0.00003674

Data for the geometric median iteration:
	Total time: 0.00336209
	Total steps: 3325
	Average number of steps pr. geometric median: 7.916666666666667
	Average time pr. step: 0.000001011154887218045
	Average time pr. geometric median: 0.00000800497619047619

