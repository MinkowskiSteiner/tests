All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.036766556058592
MST len: 4.543987142289232
Steiner Ratio: 0.8883754354165919
Solution Steiner tree:
	Terminals:
	  #0: (0.8175258314,0.6490781958,0.3927978894,0.4641002638,0.5902284042,0.8579771118,0.6528817325,0.9645026401,0.5837644062) [ #9 ]
	  #1: (0.5921190123,0.7871168474,0.8626960571,0.4022940902,0.433104444,0.3224785879,0.1665347033,0.6197118892,0.4428926201) [ #5 ]
	  #2: (0.5949672445,0.2624237846,0.4864085733,0.4051467089,0.068010787,0.7012902786,0.2066657628,0.0859796964,0.6465744323) [ #3 ]
	  #4: (0.1869343599,0.44229848,0.9763836213,0.62982698,0.0372657241,0.2388074055,0.1162355529,0.442412433,0.306818193) [ #5 ]
	  #6: (0.4809464605,0.2813506742,0.1364504407,0.0017597149,0.873469687,0.9235672881,0.864455772,0.2757637767,0.3566717321) [ #7 ]
	  #8: (0.9411502084,0.4651792224,0.1887814688,0.3742742359,0.7257872944,0.9398560226,0.0504484792,0.7854848689,0.4252954514) [ #9 ]
	Steiner points:
	  #3: (0.5824518096160339,0.44197130068041063,0.5550045256023608,0.394717267932085,0.3135845502984242,0.6240833852020078,0.27863648478190817,0.3864700430887539,0.5148799549208294) [ #5 #2 #7 ]
	  #5: (0.4897344222739422,0.589935661875135,0.7900336948790748,0.455540444166366,0.2971265757978468,0.40050359730303636,0.19083345963678502,0.5001068950447559,0.43308271350875543) [ #1 #3 #4 ]
	  #7: (0.6748299454873292,0.4529426493512011,0.35803566381054847,0.31595052345807734,0.5751574205293991,0.799565026114138,0.447893623286232,0.5558465945700787,0.4766359584745719) [ #6 #3 #9 ]
	  #9: (0.7793789780735394,0.5098844252413992,0.32609290297419014,0.3709963318369513,0.6162384205570726,0.8500304910283766,0.4068113680007528,0.7245451853049745,0.4935091580906864) [ #0 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.00863851
	Number of best updates: 2

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 108

	Init times:
		Bottleneck Steiner distances: 0.000006651
		Smallest sphere distances: 0.000001092
		Sorting: 0.000001767
		Total init time: 0.000010387

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000814, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003784, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000029653, bsd pruned: 0, ss pruned: 15

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 90
	Total number iterations 953 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 10.588888888888889
	Total time: 0.008531167
	Avarage time pr. topology: 0.00009479

Data for the geometric median iteration:
	Total time: 0.008247112
	Total steps: 32768
	Average number of steps pr. geometric median: 91.02222222222223
	Average time pr. step: 0.000000251681884765625
	Average time pr. geometric median: 0.000022908644444444442

