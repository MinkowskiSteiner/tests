All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.140186506466647
MST len: 4.699178575356753
Steiner Ratio: 0.8810447273015862
Solution Steiner tree:
	Terminals:
	  #0: (0.1333542867,0.6234359283,0.2597159544,0.6775168025,0.4244183676,0.8089938242,0.7790788094,0.2599427934,0.6323595734) [ #9 ]
	  #1: (0.8251469377,0.5661086378,0.4243843828,0.5732345397,0.7447643731,0.6941410954,0.7210808409,0.0502029169,0.8113621514) [ #3 ]
	  #2: (0.4496756841,0.6990365287,0.6652369316,0.7215187111,0.8656997224,0.5431189162,0.5266172777,0.9129774263,0.7480876026) [ #5 ]
	  #4: (0.9132866975,0.7154636065,0.5759032646,0.8231241507,0.8488178932,0.1993391929,0.0828401046,0.5263346953,0.6237575606) [ #5 ]
	  #6: (0.1754854299,0.8682969659,0.9466707064,0.6167910903,0.3179726495,0.6457072346,0.2820280214,0.0394913601,0.5114069565) [ #7 ]
	  #8: (0.1786557348,0.2697567126,0.1478463012,0.3054385443,0.1172210561,0.1922058562,0.6652606896,0.7645411136,0.9098374536) [ #9 ]
	Steiner points:
	  #3: (0.5427376100269291,0.6436783465435708,0.5191162923358209,0.6376783304748008,0.6372989665118627,0.5925833617077227,0.5529388442638976,0.3215802634223567,0.7127545066123401) [ #5 #1 #7 ]
	  #5: (0.6045300565822392,0.6813090754610186,0.5846642008155977,0.7142028495640146,0.7710069219081868,0.47517862785933346,0.4240609803611349,0.5804888379272704,0.7024525978291629) [ #2 #3 #4 ]
	  #7: (0.36767718358314433,0.6567580007291663,0.5223075249005724,0.6196946315864504,0.5002885658602425,0.6193100213477066,0.555118785237251,0.2859667588626058,0.6720651218219694) [ #6 #3 #9 ]
	  #9: (0.24692929311334524,0.5911831496966603,0.3651327388506426,0.6009823174688971,0.4179304216363042,0.6390818457843772,0.6610480419965643,0.3397031331034398,0.6878685897951541) [ #0 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.01246191
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000006586
		Smallest sphere distances: 0.000001251
		Sorting: 0.000001858
		Total init time: 0.000010633

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000752, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003669, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032341, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 1282 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 12.209523809523809
	Total time: 0.012328981
	Avarage time pr. topology: 0.000117418

Data for the geometric median iteration:
	Total time: 0.011959617
	Total steps: 48304
	Average number of steps pr. geometric median: 115.00952380952381
	Average time pr. step: 0.0000002475906136137794
	Average time pr. geometric median: 0.000028475278571428573

