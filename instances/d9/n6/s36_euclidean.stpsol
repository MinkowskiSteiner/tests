All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.92089081651371
MST len: 4.402090773581582
Steiner Ratio: 0.8906883156622498
Solution Steiner tree:
	Terminals:
	  #0: (0.3812597861,0.3492478469,0.8728369269,0.9995752056,0.5993793447,0.9228681605,0.4677818946,0.0902298559,0.4658759178) [ #9 ]
	  #1: (0.5137684199,0.8042997703,0.7905323886,0.4063076491,0.8204223732,0.1528823809,0.9984735842,0.4864156537,0.460190471) [ #5 ]
	  #2: (0.4463474226,0.2294683951,0.6895571163,0.567347118,0.6107281813,0.0388049632,0.4401840444,0.6103033869,0.6381843079) [ #3 ]
	  #4: (0.7723428918,0.9966349341,0.8395519605,0.2325333623,0.3965289325,0.1050152244,0.7738483463,0.7833614111,0.3344053693) [ #5 ]
	  #6: (0.3630522044,0.078085281,0.7284141638,0.8289281227,0.5918537013,0.5327139341,0.6194605108,0.9981613504,0.3531363068) [ #7 ]
	  #8: (0.3998939984,0.2654632643,0.541314984,0.3868324786,0.2293901449,0.7843163962,0.1882775664,0.1697030753,0.9607357951) [ #9 ]
	Steiner points:
	  #3: (0.45739490657916426,0.33691635655597973,0.7218288759987993,0.5826470926350901,0.6007974914187657,0.22136153269454878,0.5489170262151072,0.6149237565157677,0.5569507268146301) [ #5 #2 #7 ]
	  #5: (0.5686153033265339,0.7423019542042684,0.7869125314477308,0.4029001720465786,0.6551161993826652,0.15675317550992116,0.8303767965020211,0.5960925919696242,0.4502625985960951) [ #1 #3 #4 ]
	  #7: (0.42024201116434007,0.26416590831961256,0.7240905227927316,0.673501294015551,0.5682193396183786,0.41937380837616184,0.5301958271522877,0.6272882893721929,0.5267816238659564) [ #6 #3 #9 ]
	  #9: (0.40144266898519887,0.29336977121438423,0.7243845186694566,0.7054194868426304,0.48583478423911364,0.6902023721779937,0.41523659061844165,0.31966516394556216,0.6251758384329892) [ #0 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.010552157
	Number of best updates: 2

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.00000644
		Smallest sphere distances: 0.000001283
		Sorting: 0.000001974
		Total init time: 0.000010615

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000728, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003625, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032877, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 1262 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 12.019047619047619
	Total time: 0.010443604
	Avarage time pr. topology: 0.000099462

Data for the geometric median iteration:
	Total time: 0.010090667
	Total steps: 40158
	Average number of steps pr. geometric median: 95.61428571428571
	Average time pr. step: 0.0000002512741421385527
	Average time pr. geometric median: 0.00002402539761904762

