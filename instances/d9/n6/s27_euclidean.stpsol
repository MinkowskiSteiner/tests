All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.736965366234405
MST len: 4.310900117310972
Steiner Ratio: 0.8668642892532216
Solution Steiner tree:
	Terminals:
	  #0: (0.2581046555,0.7259597377,0.3388851464,0.3019310251,0.3627231598,0.2349354551,0.3703337798,0.8618260598,0.5896025284) [ #7 ]
	  #1: (0.6598329719,0.1353963139,0.1522402149,0.1232835241,0.6977873504,0.43929728,0.7240747631,0.4406859998,0.7210137708) [ #5 ]
	  #2: (0.6315492818,0.4236526631,0.6559597434,0.3525630521,0.6817573186,0.3819194806,0.691448199,0.9836883438,0.7446426403) [ #3 ]
	  #4: (0.9263836541,0.3540221231,0.6064687002,0.515986182,0.524526092,0.9271068843,0.9155044336,0.1841309705,0.8451116587) [ #9 ]
	  #6: (0.7012481791,0.9164614044,0.3820900793,0.0400117864,0.5762943759,0.5174863937,0.1922520013,0.6995779,0.2152737436) [ #7 ]
	  #8: (0.1705039685,0.3206381841,0.3995182511,0.6596048789,0.9180047749,0.9557145545,0.6343384281,0.3725255692,0.5868318391) [ #9 ]
	Steiner points:
	  #3: (0.5715933235680405,0.4665044386767282,0.4811333877353464,0.3029310719469937,0.6248463520944967,0.4383516479763782,0.60426598637037,0.775876663613217,0.6616660906340756) [ #5 #7 #2 ]
	  #5: (0.598498983205507,0.31436419519870923,0.3664078815772358,0.306193688492486,0.674681325908065,0.5604054892641288,0.6893984295622441,0.5330406333268252,0.6965554079134043) [ #1 #3 #9 ]
	  #7: (0.47353412373763437,0.676403493507472,0.4001651560237823,0.24178201524518012,0.5065920904954125,0.37356900445577645,0.41355498807699004,0.7933471091960448,0.5291116639284148) [ #6 #3 #0 ]
	  #9: (0.5739382234416294,0.3258016646810742,0.43443374775575483,0.4461912544893694,0.6976764536572023,0.74989468887228,0.7320600435744465,0.4062698789823416,0.7063271694776283) [ #4 #5 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.016543328
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000005784
		Smallest sphere distances: 0.000000973
		Sorting: 0.000002547
		Total init time: 0.000010144

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000684, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003531, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032266, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 2108 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 20.076190476190476
	Total time: 0.016435629
	Avarage time pr. topology: 0.000156529

Data for the geometric median iteration:
	Total time: 0.01585239
	Total steps: 63063
	Average number of steps pr. geometric median: 150.15
	Average time pr. step: 0.0000002513738642310071
	Average time pr. geometric median: 0.00003774378571428572

