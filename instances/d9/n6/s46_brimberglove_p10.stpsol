All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 8.692560368856563
MST len: 10.977881854899998
Steiner Ratio: 0.7918249152022548
Solution Steiner tree:
	Terminals:
	  #0: (0.3222904919,0.8439696402,0.4087874943,0.4381406775,0.6892528486,0.3085957548,0.7187822329,0.6343978749,0.5851764826) [ #5 ]
	  #1: (0.2962878152,0.4418524259,0.3354310847,0.3243266718,0.3099521703,0.2437173171,0.2426491134,0.7383900605,0.0929940874) [ #3 ]
	  #2: (0.4696959581,0.4854901943,0.4042461926,0.7061174278,0.9462171723,0.1370705227,0.0844243104,0.5482809849,0.0280388566) [ #7 ]
	  #4: (0.4243695994,0.1816147301,0.3554427341,0.7549710999,0.6513106882,0.8409329284,0.1592172925,0.3574281155,0.7871501002) [ #5 ]
	  #6: (0.8452832088,0.8998082606,0.2806415555,0.9451450268,0.2765807278,0.0144947227,0.0034960154,0.9854360809,0.0812585769) [ #9 ]
	  #8: (0.8680997448,0.9082862325,0.9183224421,0.4284378902,0.8492767708,0.0796413781,0.1055795798,0.3157934068,0.1158501856) [ #9 ]
	Steiner points:
	  #3: (0.4696959580728936,0.4854934533005815,0.36304464691571936,0.5167326381629629,0.6892528486000001,0.2437173171,0.16649937251111113,0.5482809849,0.0929940874) [ #7 #1 #5 ]
	  #5: (0.4243695994,0.4854934533005815,0.40878749429880745,0.5167326381629629,0.6892528486,0.3085957548,0.16649937251111113,0.5482809849,0.5851764826) [ #0 #4 #3 ]
	  #7: (0.4696959581,0.4854934533005815,0.36304464691571936,0.7061174278,0.6892528486000001,0.1370705227,0.0844243104,0.5482809849,0.0927889885929943) [ #2 #3 #9 ]
	  #9: (0.8452832088,0.8998082605999999,0.3630446469157194,0.7061174278,0.6892528486000001,0.0796413781,0.0844243104,0.5482809849,0.0927889885929943) [ #6 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.004259871
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000012091
		Smallest sphere distances: 0.000015705
		Sorting: 0.000006732
		Total init time: 0.00003564

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002148, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000012738, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000120398, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 218 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0761904761904764
	Total time: 0.003926398
	Avarage time pr. topology: 0.000037394

Data for the geometric median iteration:
	Total time: 0.003403176
	Total steps: 3423
	Average number of steps pr. geometric median: 8.15
	Average time pr. step: 0.0000009942085889570553
	Average time pr. geometric median: 0.000008102800000000001

