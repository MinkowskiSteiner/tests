All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.551688822798715
MST len: 5.262336136078263
Steiner Ratio: 0.8649559254857567
Solution Steiner tree:
	Terminals:
	  #0: (0.5613801752,0.2249833128,0.3930917794,0.4439383934,0.2850412509,0.1447810513,0.5635544996,0.8646787037,0.8954018363) [ #3 ]
	  #1: (0.5212840333,0.2068786268,0.8687753225,0.7704661483,0.768258802,0.0937586353,0.1635579272,0.2121971949,0.3787998862) [ #5 ]
	  #2: (0.0407533725,0.4439474528,0.1744425461,0.3665206686,0.0588192782,0.5624328081,0.1339392146,0.878686387,0.568443856) [ #7 ]
	  #4: (0.6091953011,0.0289642629,0.8836533655,0.8662761761,0.0697176354,0.3276008178,0.0407187217,0.436238304,0.386420096) [ #5 ]
	  #6: (0.3083389789,0.7757516945,0.2434785898,0.2037408148,0.0065563684,0.2474901957,0.6643217023,0.1828796636,0.873656426) [ #7 ]
	  #8: (0.2308046744,0.0040116059,0.4605808875,0.1763232952,0.6261662299,0.9448735988,0.8460845993,0.0099969395,0.257080875) [ #9 ]
	Steiner points:
	  #3: (0.38340308261324296,0.32848724548076613,0.3972412896595955,0.41230853608357176,0.2442490717069169,0.3309578126518209,0.4608565871545808,0.5901786826263248,0.6948766198526286) [ #7 #0 #9 ]
	  #5: (0.5079328056541501,0.14900374137746816,0.7595321820363633,0.6983693010601101,0.4057771368904323,0.28354640077293647,0.22095844023692232,0.3489538745486732,0.4303622543326039) [ #1 #4 #9 ]
	  #7: (0.3073757319310053,0.4241211275761437,0.330392008966126,0.369206835013812,0.17040682202992308,0.36005436337335583,0.4339630519791302,0.5761248644060547,0.7010833969612862) [ #6 #3 #2 ]
	  #9: (0.4050857700963098,0.2032337621721023,0.5479144170190497,0.48189597002553203,0.3724953596214933,0.4186741747610022,0.43484350144670586,0.3969594001182413,0.5172598764499013) [ #3 #5 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.011674012
	Number of best updates: 12

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000006315
		Smallest sphere distances: 0.000014029
		Sorting: 0.000001644
		Total init time: 0.000023125

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000726, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003756, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031676, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 1468 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 13.980952380952381
	Total time: 0.011536335
	Avarage time pr. topology: 0.000109869

Data for the geometric median iteration:
	Total time: 0.011114527
	Total steps: 44683
	Average number of steps pr. geometric median: 106.38809523809523
	Average time pr. step: 0.0000002487417362307813
	Average time pr. geometric median: 0.000026463159523809527

