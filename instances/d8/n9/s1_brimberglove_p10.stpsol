All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 12.20718508285356
MST len: 15.469931071800001
Steiner Ratio: 0.7890911101152821
Solution Steiner tree:
	Terminals:
	  #0: (0.1566790893,0.4009443942,0.1297904468,0.108808802,0.998924518,0.2182569053,0.5129323944,0.8391122347) [ #15 ]
	  #1: (0.5267449792,0.7699138363,0.4002286221,0.891529452,0.283314746,0.3524583473,0.80772452,0.919026474) [ #13 ]
	  #2: (0.2777747108,0.5539699558,0.4773970519,0.6288709248,0.3647844728,0.5134009102,0.9522297252,0.916195068) [ #9 ]
	  #4: (0.2666657494,0.5397603407,0.3752069764,0.7602487364,0.5125353641,0.6677237608,0.5316064342,0.0392803434) [ #5 ]
	  #6: (0.6126398326,0.2960316177,0.6375522677,0.5242871901,0.493582987,0.9727750239,0.2925167844,0.7713576978) [ #7 ]
	  #8: (0.0697552762,0.9493270754,0.5259953502,0.0860558479,0.192213846,0.663226927,0.8902326025,0.3488929352) [ #11 ]
	  #10: (0.0641713208,0.0200230489,0.4577017373,0.0630958383,0.2382799542,0.9706341317,0.9022080735,0.8509197868) [ #11 ]
	  #12: (0.635711728,0.7172969294,0.1416025554,0.6069688763,0.0163005716,0.2428867706,0.1372315768,0.8041767542) [ #13 ]
	  #14: (0.8401877172,0.3943829268,0.7830992238,0.7984400335,0.9116473579,0.1975513693,0.3352227557,0.7682295948) [ #15 ]
	Steiner points:
	  #3: (0.5267449792,0.40094442929264723,0.4002286603421306,0.6288798873020796,0.5125353049442101,0.31593448840000316,0.5129322626980422,0.8050209488609374) [ #13 #7 #15 ]
	  #5: (0.2777747108,0.5397603407,0.4002286603421306,0.6288798873020796,0.5125353641,0.4081231347779899,0.5316064342,0.7703149968000003) [ #4 #7 #9 ]
	  #7: (0.5267449792,0.40094442929264723,0.4002286603421306,0.6288798873020796,0.5125353049442101,0.4081231347779899,0.5129322626980422,0.7713576978) [ #6 #3 #5 ]
	  #9: (0.2777747108,0.5539699558,0.4773970519,0.6288709248,0.3647844727999999,0.5134009102,0.8902326025,0.7703149968000003) [ #5 #2 #11 ]
	  #11: (0.0697552762,0.5539699558,0.5259953377470477,0.0860558479,0.2382799542,0.663226927,0.8902326025,0.7703149968000003) [ #8 #10 #9 ]
	  #13: (0.5267458105439747,0.7172969294,0.4002266680134719,0.6288798873020796,0.283314746,0.31593448840000316,0.5129322626980422,0.8050209488609374) [ #1 #3 #12 ]
	  #15: (0.5267449792,0.400944310604602,0.4002286603421306,0.6288798873020796,0.9116473579,0.2182569053,0.5129296164244677,0.8050209488609374) [ #0 #3 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 5.450815618
	Number of best updates: 18

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 125808

	Init times:
		Bottleneck Steiner distances: 0.000031108
		Smallest sphere distances: 0.000006656
		Sorting: 0.000025023
		Total init time: 0.000064072

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001966, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000011479, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000108292, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001268925, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.016572512, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.224645254, bsd pruned: 0, ss pruned: 20790

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 114345
	Total number iterations 233786 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.044566880930517
	Total time: 4.973602895
	Avarage time pr. topology: 0.000043495

Data for the geometric median iteration:
	Total time: 4.138631882
	Total steps: 4709773
	Average number of steps pr. geometric median: 5.884163840007996
	Average time pr. step: 0.000000878732771621902
	Average time pr. geometric median: 0.0000051706075998075996

Data for the geometric median step function:
	Total number of fixed points encountered: 27357231

