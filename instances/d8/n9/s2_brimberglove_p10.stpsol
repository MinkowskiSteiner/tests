All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 11.320325294090967
MST len: 14.965100792699998
Steiner Ratio: 0.7564483160456255
Solution Steiner tree:
	Terminals:
	  #0: (0.7009763693,0.8096763491,0.0887954552,0.1214791905,0.3483067552,0.4219620016,0.6998054984,0.0663843365) [ #15 ]
	  #1: (0.4146271927,0.4975586461,0.7110460278,0.5703758302,0.1227877969,0.9803139563,0.3136068197,0.3909832534) [ #7 ]
	  #2: (0.9769541635,0.7642294633,0.9275514842,0.5735808264,0.3206497986,0.7754594864,0.7582044978,0.7181195387) [ #13 ]
	  #4: (0.5874823525,0.6429663057,0.9906032789,0.295718284,0.2713367107,0.069655987,0.949639379,0.3821752264) [ #9 ]
	  #6: (0.6652224388,0.6086851114,0.0358493757,0.7681905319,0.6535855991,0.6808405568,0.5721349938,0.5081624358) [ #11 ]
	  #8: (0.3974697401,0.8897629529,0.8504806137,0.3177298374,0.1029680931,0.0449004877,0.6449911807,0.8039444624) [ #9 ]
	  #10: (0.2681954565,0.2598712068,0.0389492735,0.5395321672,0.3295271938,0.9885886526,0.921707394,0.6522372899) [ #11 ]
	  #12: (0.8545768367,0.7337866364,0.9254236528,0.202883592,0.1557486375,0.6252291508,0.2692679284,0.74323099) [ #13 ]
	  #14: (0.3227100961,0.9883655114,0.8425220697,0.2753141943,0.5966266629,0.5564203358,0.8479080023,0.1846236713) [ #15 ]
	Steiner points:
	  #3: (0.549839009908658,0.754081854337022,0.815848739174405,0.5703737772923767,0.3206497986,0.6808405568000001,0.6998054985006751,0.5081570146243474) [ #5 #7 #13 ]
	  #5: (0.549839009908658,0.8096763491,0.815848739174405,0.295718284,0.3206497986,0.4667814462435636,0.6998054985006751,0.5081570146243474) [ #9 #3 #15 ]
	  #7: (0.549839009908658,0.6086851114000001,0.7110460278,0.5703737772923767,0.3206497986,0.6808405568000001,0.6998054985006751,0.5081570146243474) [ #3 #11 #1 ]
	  #9: (0.549839009908658,0.8096763491,0.8504806137,0.295718284,0.27133671070000004,0.069655987,0.6998054985006751,0.5081570146243474) [ #4 #8 #5 ]
	  #11: (0.549839009908658,0.6086851114000001,0.0389492735,0.5395321672,0.3295271938,0.6808405568,0.6998054985006751,0.5081624358) [ #10 #6 #7 ]
	  #13: (0.8545768367,0.7540818543370219,0.9254236528,0.5703737772923768,0.32064728244636004,0.6808405568000001,0.6998054985006751,0.7181195387) [ #2 #3 #12 ]
	  #15: (0.549839009908658,0.8096790756386315,0.8158487391744049,0.2753141943,0.3483067552,0.4667814462435636,0.6998077582807215,0.1846236713) [ #0 #5 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 5.991637827
	Number of best updates: 31

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 146598

	Init times:
		Bottleneck Steiner distances: 0.000060786
		Smallest sphere distances: 0.000009342
		Sorting: 0.000031258
		Total init time: 0.00010461

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002331, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000011992, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000115558, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001230548, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.016311437, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.25245845, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 135135
	Total number iterations 274796 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0334924334924334
	Total time: 5.456501853
	Avarage time pr. topology: 0.000040378

Data for the geometric median iteration:
	Total time: 4.488171807
	Total steps: 5153868
	Average number of steps pr. geometric median: 5.448380191237334
	Average time pr. step: 0.0000008708356145326189
	Average time pr. geometric median: 0.000004744643512043511

Data for the geometric median step function:
	Total number of fixed points encountered: 30224511

