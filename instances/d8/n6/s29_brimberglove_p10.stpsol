All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.447547640658079
MST len: 8.8823597291
Steiner Ratio: 0.8384649876607393
Solution Steiner tree:
	Terminals:
	  #0: (0.22562532,0.6172168532,0.5542016586,0.1448612135,0.5434454384,0.7647884776,0.324058877,0.6973411928) [ #5 ]
	  #1: (0.8759893737,0.4619202984,0.7565934233,0.2626699606,0.5662877856,0.3799464867,0.4450082292,0.5312397017) [ #7 ]
	  #2: (0.589627737,0.3313394461,0.5059928463,0.5876262708,0.6013480707,0.5097069594,0.2666209295,0.477337444) [ #3 ]
	  #4: (0.1110238685,0.8146039973,0.3957781528,0.2137418809,0.7814581803,0.6129272085,0.001057459,0.0070835003) [ #5 ]
	  #6: (0.9716272578,0.0232143523,0.740007405,0.5379150429,0.4031608391,0.1850156338,0.0691547445,0.2341284045) [ #9 ]
	  #8: (0.8309675659,0.9260082347,0.7454492528,0.1616497483,0.1027180125,0.9668541835,0.2171490557,0.7873155781) [ #9 ]
	Steiner points:
	  #3: (0.589627737,0.461920285355588,0.5542016585999999,0.2626699606136668,0.5662870870720308,0.5097069594,0.2666209295,0.5312397016999999) [ #2 #7 #5 ]
	  #5: (0.22562532000000002,0.6172168532,0.5542016586,0.2137418809,0.5434454384,0.6129272085,0.2666209295,0.5312397016999999) [ #4 #0 #3 ]
	  #7: (0.8759893737,0.461920285355588,0.7418213537527341,0.2626699606136668,0.5662870870720308,0.5097069594,0.2666209295,0.5312397016999999) [ #1 #3 #9 ]
	  #9: (0.8759893736999999,0.461920285355588,0.7418213537527341,0.2626699606136668,0.4031608391,0.5097069594,0.21714905569999998,0.5312397016999999) [ #6 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.004097163
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000011159
		Smallest sphere distances: 0.000004361
		Sorting: 0.000018517
		Total init time: 0.000035413

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002258, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.0000126, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000112232, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 210 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.003763915
	Avarage time pr. topology: 0.000035846

Data for the geometric median iteration:
	Total time: 0.003297851
	Total steps: 3428
	Average number of steps pr. geometric median: 8.161904761904761
	Average time pr. step: 0.0000009620335472578763
	Average time pr. geometric median: 0.00000785202619047619

