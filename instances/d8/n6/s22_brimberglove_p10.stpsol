All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 8.526361799285185
MST len: 10.6812019844
Steiner Ratio: 0.7982586427761614
Solution Steiner tree:
	Terminals:
	  #0: (0.8650269224,0.5663939652,0.6338536677,0.0191505631,0.5234706907,0.8290278501,0.414524392,0.4607495118) [ #5 ]
	  #1: (0.5664524332,0.0139149264,0.1049435228,0.8549799313,0.1112611555,0.3715435501,0.4186239473,0.5574028178) [ #7 ]
	  #2: (0.3566538055,0.5690084359,0.8227016287,0.8801244962,0.3980362855,0.2372260202,0.3408740076,0.5842591182) [ #3 ]
	  #4: (0.9698017333,0.4631831755,0.5185416162,0.7034413422,0.9385241437,0.7902598408,0.9351547686,0.8035510656) [ #5 ]
	  #6: (0.4461416623,0.7561149335,0.1936676377,0.5023851071,0.0234475681,0.1250572825,0.8931799414,0.0834255573) [ #7 ]
	  #8: (0.1862228323,0.3292264125,0.6730409189,0.520684405,0.2885274982,0.0973462291,0.2666000273,0.563644016) [ #9 ]
	Steiner points:
	  #3: (0.35950205538300983,0.4631831755,0.5961013930333332,0.7034413422,0.3980362855,0.23722602019999997,0.5084320789592592,0.5842591182) [ #2 #5 #9 ]
	  #5: (0.8650269224,0.4631831755,0.5961013930333332,0.7034413422,0.5234706907,0.7902598408,0.5084320789592592,0.5842591182) [ #4 #0 #3 ]
	  #7: (0.4461416623,0.4185309211666667,0.1936676377,0.6651936838417163,0.1112611555,0.22536971696666666,0.4278213950741542,0.5574028178) [ #1 #6 #9 ]
	  #9: (0.35950205538300983,0.4185309211666667,0.5961013930333331,0.6651936838417163,0.2885274982,0.22536971696666666,0.4278213950741542,0.5636440160000001) [ #3 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.003756974
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000012926
		Smallest sphere distances: 0.00000416
		Sorting: 0.000006251
		Total init time: 0.000024895

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000234, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000012023, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000111886, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 211 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0095238095238095
	Total time: 0.003437084
	Avarage time pr. topology: 0.000032734

Data for the geometric median iteration:
	Total time: 0.002984129
	Total steps: 3305
	Average number of steps pr. geometric median: 7.869047619047619
	Average time pr. step: 0.0000009029134644478063
	Average time pr. geometric median: 0.000007105069047619047

