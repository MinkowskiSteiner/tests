All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.275071441129649
MST len: 9.1056115153
Steiner Ratio: 0.7989657178878622
Solution Steiner tree:
	Terminals:
	  #0: (0.5846134669,0.5077402124,0.0594066722,0.5997367695,0.5458334356,0.0842270214,0.5216057671,0.066293479) [ #9 ]
	  #1: (0.1900027982,0.7689555445,0.3776398615,0.861473049,0.2765600068,0.3932821464,0.0769409025,0.5114291709) [ #5 ]
	  #2: (0.5204600438,0.6477700405,0.0916402913,0.5212438672,0.540312229,0.1304018107,0.0569058615,0.9637256646) [ #3 ]
	  #4: (0.384026217,0.8656925191,0.3785816228,0.6610816175,0.5074669032,0.561359823,0.6648593674,0.4017881259) [ #5 ]
	  #6: (0.2348691641,0.1913313205,0.4307993098,0.5479775013,0.0151233026,0.0380932228,0.0248203492,0.9218689981) [ #7 ]
	  #8: (0.8943212232,0.6286429756,0.1040961771,0.975851736,0.6714702508,0.5076044628,0.0156422849,0.215467854) [ #9 ]
	Steiner points:
	  #3: (0.5204600438000001,0.6477700405,0.0916402913,0.6379064203777778,0.5403122289999999,0.1304018107,0.07563400155186953,0.5773331369960943) [ #2 #7 #9 ]
	  #5: (0.384026217,0.7689555445,0.3776398615,0.6610816175,0.3689300579333333,0.3932821464,0.0769409025,0.5114291709) [ #4 #1 #7 ]
	  #7: (0.384026217,0.6477700405,0.3776398615,0.6379064203777778,0.3689300579333333,0.1304018107,0.07563400155186953,0.5773331369960943) [ #3 #5 #6 ]
	  #9: (0.5846134669,0.6286429756,0.0916402913,0.6379064203777778,0.5458334355999999,0.1304018107,0.07563400155186953,0.215467854) [ #0 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.003793064
	Number of best updates: 10

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000027491
		Smallest sphere distances: 0.000004148
		Sorting: 0.000006365
		Total init time: 0.000039567

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002414, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000011782, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000110408, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 213 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0285714285714285
	Total time: 0.003461994
	Avarage time pr. topology: 0.000032971

Data for the geometric median iteration:
	Total time: 0.002981711
	Total steps: 3105
	Average number of steps pr. geometric median: 7.392857142857143
	Average time pr. step: 0.0000009602933977455718
	Average time pr. geometric median: 0.000007099311904761906

