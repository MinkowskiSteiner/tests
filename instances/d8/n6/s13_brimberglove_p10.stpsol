All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 9.101742931128392
MST len: 12.1005637644
Steiner Ratio: 0.752175114179872
Solution Steiner tree:
	Terminals:
	  #0: (0.792852993,0.8622477059,0.4365024196,0.0231408114,0.3533674392,0.8654058324,0.0406219666,0.7204131906) [ #9 ]
	  #1: (0.6326431831,0.3414155828,0.904109925,0.1300345134,0.6083626135,0.3345068657,0.1558290944,0.7948272283) [ #3 ]
	  #2: (0.7731688911,0.7598976808,0.3297242696,0.3815315046,0.0944045461,0.485553364,0.1763587325,0.0226054806) [ #5 ]
	  #4: (0.0416181572,0.251262738,0.0757720694,0.205355405,0.3670426623,0.9052277226,0.1170504508,0.1598956548) [ #7 ]
	  #6: (0.3779684163,0.1168798023,0.6027370345,0.4139833741,0.5670465732,0.4317533082,0.8557877559,0.1996897558) [ #7 ]
	  #8: (0.928200935,0.5560647936,0.0749040051,0.0531665883,0.1637372478,0.1157799243,0.8294556531,0.9116950463) [ #9 ]
	Steiner points:
	  #3: (0.6326494854757777,0.5665566308908028,0.3159696147666667,0.1300345134,0.36704266230000004,0.48555041928838927,0.1558290944,0.7841738089313328) [ #1 #5 #9 ]
	  #5: (0.6326494854757777,0.5665566308908028,0.3159696147666667,0.27489806136474126,0.36704266230000004,0.48555041928838927,0.1558290944,0.17316035513333333) [ #3 #2 #7 ]
	  #7: (0.3779684163,0.251262738,0.31596961476666663,0.27489806136474126,0.36704571407092174,0.48555041928838916,0.1558290944,0.17316035513333333) [ #4 #5 #6 ]
	  #9: (0.792852993,0.5665566308908028,0.3159696147666667,0.0531665883,0.35336743919999997,0.4855504192883894,0.1558290944,0.7841738089313328) [ #0 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.004022816
	Number of best updates: 8

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000015954
		Smallest sphere distances: 0.000003831
		Sorting: 0.000019474
		Total init time: 0.000041051

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.0000021, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000012203, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000111048, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 212 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.019047619047619
	Total time: 0.003696326
	Avarage time pr. topology: 0.000035203

Data for the geometric median iteration:
	Total time: 0.003242138
	Total steps: 3551
	Average number of steps pr. geometric median: 8.454761904761904
	Average time pr. step: 0.0000009130211208110391
	Average time pr. geometric median: 0.000007719376190476189

Data for the geometric median step function:
	Total number of fixed points encountered: 17282

