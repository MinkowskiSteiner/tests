All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.720555645112929
MST len: 9.9410461792
Steiner Ratio: 0.7766341193814106
Solution Steiner tree:
	Terminals:
	  #0: (0.0099969395,0.257080875,0.0407533725,0.4439474528,0.1744425461,0.3665206686,0.0588192782,0.5624328081) [ #3 ]
	  #1: (0.0937586353,0.1635579272,0.2121971949,0.3787998862,0.3083389789,0.7757516945,0.2434785898,0.2037408148) [ #3 ]
	  #2: (0.5613801752,0.2249833128,0.3930917794,0.4439383934,0.2850412509,0.1447810513,0.5635544996,0.8646787037) [ #9 ]
	  #4: (0.0065563684,0.2474901957,0.6643217023,0.1828796636,0.873656426,0.6091953011,0.0289642629,0.8836533655) [ #7 ]
	  #6: (0.1339392146,0.878686387,0.568443856,0.5212840333,0.2068786268,0.8687753225,0.7704661483,0.768258802) [ #7 ]
	  #8: (0.8954018363,0.2308046744,0.0040116059,0.4605808875,0.1763232952,0.6261662299,0.9448735988,0.8460845993) [ #9 ]
	Steiner points:
	  #3: (0.0937586353,0.23692682573333318,0.21219719490000002,0.4242337758111111,0.28504119546872964,0.5646813343441167,0.24347858980000292,0.5624328081000001) [ #0 #5 #1 ]
	  #5: (0.0937586353,0.23692682573333318,0.2633983882392264,0.4242337758111111,0.28504119546872964,0.5646813343441167,0.24347858980000292,0.8296553971957035) [ #3 #7 #9 ]
	  #7: (0.09375863530000002,0.24749019569999997,0.568443856,0.4242337758111111,0.28504119546872964,0.6091953011,0.2434785898000029,0.8296553971957035) [ #4 #5 #6 ]
	  #9: (0.5613801752,0.2308046744,0.2633983882392264,0.4439383934,0.2850379054005752,0.5646813343441167,0.5635544996,0.8460845993000001) [ #2 #5 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.003921774
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000010639
		Smallest sphere distances: 0.00000294
		Sorting: 0.000006176
		Total init time: 0.000021154

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002006, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000011952, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000113104, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 212 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.019047619047619
	Total time: 0.003597367
	Avarage time pr. topology: 0.00003426

Data for the geometric median iteration:
	Total time: 0.003128788
	Total steps: 3273
	Average number of steps pr. geometric median: 7.792857142857143
	Average time pr. step: 0.000000955938893981057
	Average time pr. geometric median: 0.000007449495238095237

Data for the geometric median step function:
	Total number of fixed points encountered: 15288

