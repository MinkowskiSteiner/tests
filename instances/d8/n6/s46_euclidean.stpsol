All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.179301907327551
MST len: 4.442850546323663
Steiner Ratio: 0.9406802825689936
Solution Steiner tree:
	Terminals:
	  #0: (0.0280388566,0.8680997448,0.9082862325,0.9183224421,0.4284378902,0.8492767708,0.0796413781,0.1055795798) [ #7 ]
	  #1: (0.3099521703,0.2437173171,0.2426491134,0.7383900605,0.0929940874,0.3222904919,0.8439696402,0.4087874943) [ #5 ]
	  #2: (0.8409329284,0.1592172925,0.3574281155,0.7871501002,0.2962878152,0.4418524259,0.3354310847,0.3243266718) [ #3 ]
	  #4: (0.0034960154,0.9854360809,0.0812585769,0.4243695994,0.1816147301,0.3554427341,0.7549710999,0.6513106882) [ #5 ]
	  #6: (0.3157934068,0.1158501856,0.8452832088,0.8998082606,0.2806415555,0.9451450268,0.2765807278,0.0144947227) [ #7 ]
	  #8: (0.4696959581,0.4854901943,0.4042461926,0.7061174278,0.9462171723,0.1370705227,0.0844243104,0.5482809849) [ #9 ]
	Steiner points:
	  #3: (0.546605350645141,0.3046687581341168,0.4012366233059401,0.7632453279831696,0.35981282809182175,0.43250930159319523,0.39608939713209557,0.35380553598023) [ #2 #5 #9 ]
	  #5: (0.3239093580144524,0.3759395853065095,0.25889459248401686,0.6956816268243163,0.17677697415330013,0.3563583137115831,0.712698857286742,0.4323979052578437) [ #1 #4 #3 ]
	  #7: (0.2902187342061695,0.388233787683498,0.74772838506215,0.8666482485636271,0.37295156877917074,0.7694293003066107,0.23585547273913682,0.13859480857618173) [ #0 #6 #9 ]
	  #9: (0.48315907879160946,0.34939142173058335,0.4714004312255408,0.7752268460600281,0.4529776264141657,0.45467691840538954,0.31574622775082434,0.34053511558299854) [ #3 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.007508146
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000005681
		Smallest sphere distances: 0.000000734
		Sorting: 0.00000168
		Total init time: 0.000009119

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000859, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000016266, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031827, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 743 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 7.076190476190476
	Total time: 0.007391104
	Avarage time pr. topology: 0.000070391

Data for the geometric median iteration:
	Total time: 0.007172158
	Total steps: 29544
	Average number of steps pr. geometric median: 70.34285714285714
	Average time pr. step: 0.00000024276191443271053
	Average time pr. geometric median: 0.000017076566666666665

