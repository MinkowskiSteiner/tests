All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.3359323768373095
MST len: 9.6338250936
Steiner Ratio: 0.7614765999551684
Solution Steiner tree:
	Terminals:
	  #0: (0.5918537013,0.5327139341,0.6194605108,0.9981613504,0.3531363068,0.7723428918,0.9966349341,0.8395519605) [ #3 ]
	  #1: (0.1882775664,0.1697030753,0.9607357951,0.4463474226,0.2294683951,0.6895571163,0.567347118,0.6107281813) [ #5 ]
	  #2: (0.4658759178,0.5137684199,0.8042997703,0.7905323886,0.4063076491,0.8204223732,0.1528823809,0.9984735842) [ #3 ]
	  #4: (0.0388049632,0.4401840444,0.6103033869,0.6381843079,0.3630522044,0.078085281,0.7284141638,0.8289281227) [ #7 ]
	  #6: (0.4864156537,0.460190471,0.3998939984,0.2654632643,0.541314984,0.3868324786,0.2293901449,0.7843163962) [ #7 ]
	  #8: (0.3812597861,0.3492478469,0.8728369269,0.9995752056,0.5993793447,0.9228681605,0.4677818946,0.0902298559) [ #9 ]
	Steiner points:
	  #3: (0.4658759178,0.5137684198999998,0.8042997702999999,0.7905323886000001,0.40630764909993267,0.7723428918,0.5341587102,0.8395519605) [ #2 #0 #9 ]
	  #5: (0.1882775664,0.4401840443999994,0.8271454891665848,0.6381858927686539,0.3630576573067115,0.6895571163,0.567347118,0.6107281814064099) [ #7 #1 #9 ]
	  #7: (0.1882775664,0.4401840443999994,0.6103033869,0.6381858927686539,0.3630576573067116,0.3868324786,0.567347118,0.7843163961999999) [ #6 #5 #4 ]
	  #9: (0.3812597861,0.4401840443999995,0.8271454891665848,0.7905323886000001,0.40630764909993267,0.7723428918,0.5341587102,0.6107281814064099) [ #3 #5 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.003978344
	Number of best updates: 7

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000028302
		Smallest sphere distances: 0.000003873
		Sorting: 0.000008144
		Total init time: 0.000042066

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002118, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00001219, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000122884, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 211 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0095238095238095
	Total time: 0.003632987
	Avarage time pr. topology: 0.000034599

Data for the geometric median iteration:
	Total time: 0.003163372
	Total steps: 3336
	Average number of steps pr. geometric median: 7.942857142857143
	Average time pr. step: 0.0000009482529976019184
	Average time pr. geometric median: 0.000007531838095238095

