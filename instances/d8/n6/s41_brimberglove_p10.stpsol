All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 8.413837458964327
MST len: 10.191128796999998
Steiner Ratio: 0.8256040745399216
Solution Steiner tree:
	Terminals:
	  #0: (0.6458053345,0.3277779153,0.6146768642,0.5523276313,0.9013367439,0.2180386205,0.3413264916,0.2890184029) [ #5 ]
	  #1: (0.387681659,0.2726833854,0.3569481244,0.0666660061,0.3927549945,0.8972157686,0.7339704534,0.1277633953) [ #3 ]
	  #2: (0.4878506383,0.4318964698,0.8467889367,0.5935844293,0.9065222973,0.5735588286,0.6033617564,0.7889988608) [ #7 ]
	  #4: (0.6536876334,0.510914339,0.4453349446,0.9197389232,0.533241797,0.2139088648,0.4809889791,0.0210924349) [ #9 ]
	  #6: (0.3233618859,0.0549102435,0.9953047172,0.408344438,0.1785172881,0.7420079246,0.1287793886,0.8487633443) [ #7 ]
	  #8: (0.6702460561,0.9116797088,0.38213495,0.5965716008,0.2660512897,0.022327458,0.7685739201,0.5612500564) [ #9 ]
	Steiner points:
	  #3: (0.43302105416666664,0.3062343943772778,0.4976338242,0.5935844293,0.5704874416333336,0.6297085272666666,0.48098897910000044,0.2890184029) [ #7 #5 #1 ]
	  #5: (0.6458053345000001,0.32777791529999994,0.4976338242,0.5935844293,0.5704874416333336,0.2180386205,0.48098897910000044,0.2890184029) [ #0 #3 #9 ]
	  #7: (0.43302105416666664,0.3062343943772778,0.8467889367000001,0.5935844293,0.5704874416333336,0.6297085272666666,0.48098897910000044,0.7889988608000001) [ #6 #3 #2 ]
	  #9: (0.6536876334,0.510914339,0.4453349446,0.5965716008,0.533241797,0.2139088648,0.4809933672309949,0.2890184029) [ #4 #5 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.003716653
	Number of best updates: 7

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000012338
		Smallest sphere distances: 0.000002822
		Sorting: 0.000018609
		Total init time: 0.000034784

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001846, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000011859, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000111413, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 211 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0095238095238095
	Total time: 0.00336103
	Avarage time pr. topology: 0.000032009

Data for the geometric median iteration:
	Total time: 0.002881422
	Total steps: 3052
	Average number of steps pr. geometric median: 7.266666666666667
	Average time pr. step: 0.0000009441094364351244
	Average time pr. geometric median: 0.000006860528571428571

