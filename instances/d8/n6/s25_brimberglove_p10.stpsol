All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 9.594010367902163
MST len: 12.330836128500001
Steiner Ratio: 0.7780502690914632
Solution Steiner tree:
	Terminals:
	  #0: (0.0401867987,0.325981314,0.631247768,0.3657580602,0.3181859326,0.6278227673,0.2114482025,0.4217923961) [ #5 ]
	  #1: (0.1036064635,0.5821549858,0.7011187615,0.5075186931,0.9913254278,0.7521004219,0.7428126711,0.9414201057) [ #3 ]
	  #2: (0.6581573387,0.8989526019,0.6503086554,0.7898902324,0.7133313109,0.2344475841,0.283649136,0.6526875024) [ #7 ]
	  #4: (0.2099777531,0.912566964,0.9293110892,0.2013031804,0.6646673855,0.6721237603,0.1427232857,0.6547720249) [ #5 ]
	  #6: (0.9901046399,0.0505868262,0.7723662428,0.4866999432,0.7424799096,0.5370173396,0.9999918654,0.1990076612) [ #9 ]
	  #8: (0.939356192,0.805739215,0.042332178,0.9785602656,0.3255712615,0.9922046186,0.9965749998,0.8456901428) [ #9 ]
	Steiner points:
	  #3: (0.2099777531,0.7203604891086419,0.631247768,0.5075186931000091,0.7230475104666668,0.7521004200354836,0.5224300458,0.6547720248999979) [ #7 #1 #5 ]
	  #5: (0.2099777531,0.7203604891086419,0.631247768,0.3657580602,0.6646673854999999,0.6721237603,0.21144820250000002,0.6547720249) [ #0 #4 #3 ]
	  #7: (0.6581573387,0.7203604891086419,0.631247768,0.5075186931000091,0.7230475104666668,0.6887464325999597,0.5224300458,0.6547720248999979) [ #2 #3 #9 ]
	  #9: (0.9393561919999999,0.7203604891086419,0.6312477679999999,0.5075186931000091,0.7230475104666668,0.6887464325999597,0.9965749997999995,0.6547720248999979) [ #6 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.004072423
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000012581
		Smallest sphere distances: 0.000015462
		Sorting: 0.000006158
		Total init time: 0.000035311

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001762, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000013231, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000111866, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 210 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.003742418
	Avarage time pr. topology: 0.000035642

Data for the geometric median iteration:
	Total time: 0.003277376
	Total steps: 3433
	Average number of steps pr. geometric median: 8.173809523809524
	Average time pr. step: 0.000000954668220215555
	Average time pr. geometric median: 0.000007803276190476191

