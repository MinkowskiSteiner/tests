All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.892543743429357
MST len: 9.5662071127
Steiner Ratio: 0.8250442051323867
Solution Steiner tree:
	Terminals:
	  #0: (0.2474670286,0.2316154909,0.3833849972,0.502905194,0.9416959309,0.238100005,0.1387001626,0.3471150195) [ #9 ]
	  #1: (0.7955457819,0.2153102822,0.3426214272,0.296161194,0.2554381654,0.71008044,0.8547150078,0.6357949691) [ #5 ]
	  #2: (0.6638084062,0.3077898539,0.4603446049,0.1393605741,0.3061093089,0.3350736878,0.6921740634,0.5593727015) [ #3 ]
	  #4: (0.9016035897,0.843498724,0.51194808,0.7903852178,0.1405945132,0.2406681847,0.1869459665,0.8044029194) [ #7 ]
	  #6: (0.8804080379,0.3659706252,0.346876925,0.9765705247,0.2916780218,0.0321567464,0.8889940641,0.0872238032) [ #7 ]
	  #8: (0.4054190886,0.6635035848,0.7047985609,0.1648330605,0.8887816281,0.2970957897,0.7287201051,0.3965607492) [ #9 ]
	Steiner points:
	  #3: (0.6638084062,0.36597062517564644,0.51194808,0.4609025352600593,0.30610930889999993,0.32241438843333337,0.6921744596952181,0.5593727015) [ #5 #2 #9 ]
	  #5: (0.7955457819,0.36597062517564644,0.51194808,0.4609025352600593,0.2554381654,0.32241438843333337,0.6921744596952181,0.6357949691) [ #1 #3 #7 ]
	  #7: (0.8804080379,0.36597426841029324,0.5119468206166412,0.7903852178,0.2554381654,0.2406681847,0.692174459695218,0.6357949691) [ #4 #5 #6 ]
	  #9: (0.4054190886,0.36597062517564644,0.51194808,0.4609025352600593,0.8887816281,0.2970957897,0.6921744596952178,0.39656074919999995) [ #0 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.003865707
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000024581
		Smallest sphere distances: 0.000003007
		Sorting: 0.000006127
		Total init time: 0.000034663

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001812, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000012037, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000127258, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 210 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.00350347
	Avarage time pr. topology: 0.000033366

Data for the geometric median iteration:
	Total time: 0.003018931
	Total steps: 3171
	Average number of steps pr. geometric median: 7.55
	Average time pr. step: 0.000000952043834752444
	Average time pr. geometric median: 0.000007187930952380952

