All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.215868989454661
MST len: 8.7562019057
Steiner Ratio: 0.8240866379242999
Solution Steiner tree:
	Terminals:
	  #0: (0.1298833849,0.3797864329,0.0226750942,0.2685777122,0.3488384743,0.5206193517,0.4255304404,0.9535663561) [ #3 ]
	  #1: (0.7615132685,0.3106321545,0.2578007133,0.9034257293,0.0485690669,0.5226562896,0.1556680115,0.8831310002) [ #7 ]
	  #2: (0.3921490034,0.2415533509,0.4918181642,0.7429387396,0.1386943274,0.9690520419,0.497944257,0.1569527281) [ #5 ]
	  #4: (0.3024737403,0.0332014114,0.4304443828,0.8226312044,0.8875873521,0.8883300344,0.8879682691,0.2797363551) [ #5 ]
	  #6: (0.6047278813,0.6099463206,0.8727000411,0.9242446869,0.7194525505,0.1857139022,0.0945956191,0.7204462847) [ #7 ]
	Steiner points:
	  #3: (0.37558675653333357,0.3797864328860361,0.4623977665,0.7695028945333333,0.38832532905463374,0.5206193517,0.4255304404,0.7922661932556012) [ #0 #5 #7 ]
	  #5: (0.3755867565333336,0.2415533509,0.4623977665,0.7695028945333333,0.38832532905463374,0.8883300344,0.497944257,0.2797363551) [ #2 #3 #4 ]
	  #7: (0.6047278813,0.3797864328860361,0.4623977664999999,0.9034257292999999,0.3883253290546337,0.5206193516999735,0.1556680115,0.7922661932556012) [ #1 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000551956
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000022962
		Smallest sphere distances: 0.000004787
		Sorting: 0.000008157
		Total init time: 0.000040518

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000004305, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000013417, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000448534
	Avarage time pr. topology: 0.000029902

Data for the geometric median iteration:
	Total time: 0.000393705
	Total steps: 409
	Average number of steps pr. geometric median: 9.088888888888889
	Average time pr. step: 0.00000096260391198044
	Average time pr. geometric median: 0.000008748999999999999

