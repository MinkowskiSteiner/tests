All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.6802629753358005
MST len: 9.2150966795
Steiner Ratio: 0.8334435592435393
Solution Steiner tree:
	Terminals:
	  #0: (0.0034960154,0.9854360809,0.0812585769,0.4243695994,0.1816147301,0.3554427341,0.7549710999,0.6513106882) [ #7 ]
	  #1: (0.0280388566,0.8680997448,0.9082862325,0.9183224421,0.4284378902,0.8492767708,0.0796413781,0.1055795798) [ #5 ]
	  #2: (0.8409329284,0.1592172925,0.3574281155,0.7871501002,0.2962878152,0.4418524259,0.3354310847,0.3243266718) [ #3 ]
	  #4: (0.3157934068,0.1158501856,0.8452832088,0.8998082606,0.2806415555,0.9451450268,0.2765807278,0.0144947227) [ #5 ]
	  #6: (0.4696959581,0.4854901943,0.4042461926,0.7061174278,0.9462171723,0.1370705227,0.0844243104,0.5482809849) [ #7 ]
	Steiner points:
	  #3: (0.25511258075802656,0.5132010333938272,0.3574281155,0.7871501002000001,0.3791724453,0.4418524259,0.33543108446748016,0.3243266718) [ #5 #2 #7 ]
	  #5: (0.25511258075802656,0.5132010333938272,0.8452832088,0.8998082606,0.3791724453,0.8492767708000001,0.2765807278,0.10557957979999999) [ #1 #3 #4 ]
	  #7: (0.25511258075802656,0.5132010333938273,0.3574281155,0.7061174278,0.3791724453,0.3554427341,0.33543108446748016,0.5482809849) [ #0 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000466483
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.00002291
		Smallest sphere distances: 0.00000208
		Sorting: 0.000003369
		Total init time: 0.000029429

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002721, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000012909, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000394801
	Avarage time pr. topology: 0.00002632

Data for the geometric median iteration:
	Total time: 0.000342744
	Total steps: 370
	Average number of steps pr. geometric median: 8.222222222222221
	Average time pr. step: 0.0000009263351351351352
	Average time pr. geometric median: 0.000007616533333333333

