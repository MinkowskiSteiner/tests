All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.323920155736159
MST len: 3.586507237740001
Steiner Ratio: 0.9267847338377857
Solution Steiner tree:
	Terminals:
	  #0: (0.1217957135,0.9245497658,0.6410978039,0.8101899306,0.0544752507,0.9522628411,0.3267083561,0.696483258) [ #5 ]
	  #1: (0.6420080073,0.4910349215,0.0853253426,0.4141814399,0.2860070939,0.0876902482,0.3650377357,0.4803538809) [ #7 ]
	  #2: (0.6394705305,0.1302257493,0.7771808131,0.8949633971,0.8192397947,0.6017836037,0.3779945804,0.8946192055) [ #3 ]
	  #4: (0.1970067798,0.1855946668,0.9467040281,0.776706667,0.031953645,0.6816179667,0.8094309405,0.8175539276) [ #5 ]
	  #6: (0.7856002826,0.4401777473,0.1151188249,0.8235438768,0.6883942167,0.1299254853,0.3111650372,0.5165184259) [ #7 ]
	Steiner points:
	  #3: (0.5369108209236609,0.3164210863984655,0.6082134899479451,0.8073967667404539,0.5353000930278988,0.5233564146638682,0.42701425802137194,0.7554991268278586) [ #2 #5 #7 ]
	  #5: (0.30154942891421926,0.42962585140386134,0.7416141824213517,0.7968342021130991,0.22230057450816368,0.6954749405498526,0.5412466218182422,0.7626930823951604) [ #4 #3 #0 ]
	  #7: (0.6915158611072236,0.4288753329408681,0.2103414728986827,0.7012722167139621,0.5392991372102085,0.20054040828510836,0.3512130200447948,0.5563600646809403) [ #1 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.001646955
	Number of best updates: 2

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.00000469
		Smallest sphere distances: 0.000000551
		Sorting: 0.000001321
		Total init time: 0.000007935

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000822, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003718, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 280 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 18.666666666666668
	Total time: 0.001620518
	Avarage time pr. topology: 0.000108034

Data for the geometric median iteration:
	Total time: 0.001557691
	Total steps: 6031
	Average number of steps pr. geometric median: 134.0222222222222
	Average time pr. step: 0.00000025828071629912117
	Average time pr. geometric median: 0.000034615355555555545

