All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 6.278585830101278
MST len: 7.7504147247
Steiner Ratio: 0.8100967565118662
Solution Steiner tree:
	Terminals:
	  #0: (0.0937586353,0.1635579272,0.2121971949,0.3787998862,0.3083389789,0.7757516945,0.2434785898,0.2037408148) [ #3 ]
	  #1: (0.0099969395,0.257080875,0.0407533725,0.4439474528,0.1744425461,0.3665206686,0.0588192782,0.5624328081) [ #3 ]
	  #2: (0.5613801752,0.2249833128,0.3930917794,0.4439383934,0.2850412509,0.1447810513,0.5635544996,0.8646787037) [ #5 ]
	  #4: (0.1339392146,0.878686387,0.568443856,0.5212840333,0.2068786268,0.8687753225,0.7704661483,0.768258802) [ #5 ]
	  #6: (0.8954018363,0.2308046744,0.0040116059,0.4605808875,0.1763232952,0.6261662299,0.9448735988,0.8460845993) [ #7 ]
	Steiner points:
	  #3: (0.09375863530000002,0.257080875,0.2121971949,0.4439474528,0.22981965951111105,0.5594415673098323,0.2434785898,0.5624328081) [ #0 #1 #7 ]
	  #5: (0.539151607277773,0.24832214146666662,0.3930917794,0.4494929665964586,0.22981965951111105,0.5594415673098323,0.7042704636444445,0.768258802) [ #2 #4 #7 ]
	  #7: (0.539151607277773,0.24832214146666662,0.21219719489999997,0.4494929665964586,0.22981965951111105,0.5594415673098323,0.7042704636444445,0.768258802) [ #3 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000532323
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000007351
		Smallest sphere distances: 0.000002038
		Sorting: 0.000003057
		Total init time: 0.000013389

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001853, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000012772, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000478208
	Avarage time pr. topology: 0.00003188

Data for the geometric median iteration:
	Total time: 0.000398445
	Total steps: 382
	Average number of steps pr. geometric median: 8.488888888888889
	Average time pr. step: 0.0000010430497382198953
	Average time pr. geometric median: 0.000008854333333333333

Data for the geometric median step function:
	Total number of fixed points encountered: 1633

