All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 6.243575969136739
MST len: 7.6392548886
Steiner Ratio: 0.8173016950192326
Solution Steiner tree:
	Terminals:
	  #0: (0.1450036579,0.1299871505,0.3764338714,0.8339692707,0.916499746,0.3131860557,0.29486724,0.5147261031) [ #3 ]
	  #1: (0.4884398666,0.7715497821,0.6546407694,0.9558374691,0.6343455583,0.344454169,0.1362124086,0.2091968196) [ #5 ]
	  #2: (0.5748512617,0.1829102199,0.3235021184,0.6120159149,0.7084588719,0.4244470016,0.6334648415,0.41691941) [ #7 ]
	  #4: (0.3503611406,0.4857762337,0.7851141266,0.5901703474,0.1827713406,0.9935369142,0.1363650975,0.5229908035) [ #5 ]
	  #6: (0.5982263575,0.1752538104,0.4766825421,0.1399146664,0.4673976025,0.8627957766,0.6898134,0.18037494) [ #7 ]
	Steiner points:
	  #3: (0.4424136246,0.1803580834,0.3764338714,0.6120159148999941,0.6281051154833683,0.42444700160000326,0.2948672399999999,0.41691940999995736) [ #5 #0 #7 ]
	  #5: (0.4424136246,0.4857762337,0.6546407694,0.6120159148999941,0.6281051154833553,0.42444700160000326,0.1363650975,0.41691940999995736) [ #4 #3 #1 ]
	  #7: (0.5748512617,0.1803580834,0.3764338714,0.6120141139835253,0.6281051154833683,0.4244486737614962,0.6334648415,0.4169185076578779) [ #2 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000496032
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.00000935
		Smallest sphere distances: 0.000002158
		Sorting: 0.000003365
		Total init time: 0.00001591

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001924, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000013368, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000422714
	Avarage time pr. topology: 0.00002818

Data for the geometric median iteration:
	Total time: 0.000369133
	Total steps: 391
	Average number of steps pr. geometric median: 8.688888888888888
	Average time pr. step: 0.0000009440741687979539
	Average time pr. geometric median: 0.000008202955555555555

