All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.516492342000001
MST len: 7.0303185253
Steiner Ratio: 0.7846717502411599
Solution Steiner tree:
	Terminals:
	  #0: (0.0063632978,0.7409414774,0.3425149626,0.8681538691,0.4625301303,0.7613690005,0.7293978635,0.7704790662) [ #3 ]
	  #1: (0.3740666413,0.1966241674,0.3118738645,0.6745135252,0.4598806493,0.1991169612,0.3833771825,0.5127728258) [ #5 ]
	  #2: (0.3079489355,0.6126976412,0.4672263043,0.5413947988,0.3004468834,0.263256482,0.8872430967,0.7088636578) [ #3 ]
	  #4: (0.9474799428,0.1736593936,0.3251002693,0.2867596859,0.331827752,0.4798882434,0.6288429693,0.6801386246) [ #5 ]
	  #6: (0.0528921765,0.2627286242,0.2802385941,0.3090906377,0.2058983465,0.1701670499,0.6832347255,0.1237438932) [ #7 ]
	Steiner points:
	  #3: (0.3079489355,0.6126976412,0.3425149626,0.5413947988,0.3004468834019075,0.263256482,0.7293978635,0.7088636578) [ #2 #0 #7 ]
	  #5: (0.3740666413,0.19662416740000002,0.3118738648095127,0.46396007843333326,0.331827752,0.23222667130000005,0.6288429693,0.5127728258) [ #1 #4 #7 ]
	  #7: (0.30794893549999997,0.26272862420000004,0.3118738648095127,0.46396007843333326,0.3004468834019075,0.23222667130000005,0.6832347255,0.5127728258) [ #3 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000446902
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000008922
		Smallest sphere distances: 0.00000223
		Sorting: 0.000003468
		Total init time: 0.000015806

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002343, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000013153, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000386596
	Avarage time pr. topology: 0.000025773

Data for the geometric median iteration:
	Total time: 0.000333015
	Total steps: 358
	Average number of steps pr. geometric median: 7.955555555555556
	Average time pr. step: 0.0000009302094972067038
	Average time pr. geometric median: 0.000007400333333333333

