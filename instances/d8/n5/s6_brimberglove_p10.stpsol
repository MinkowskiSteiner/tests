All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 6.908322542544445
MST len: 8.4690824226
Steiner Ratio: 0.8157108642737234
Solution Steiner tree:
	Terminals:
	  #0: (0.7647564228,0.4129372162,0.5164472929,0.8498845724,0.2185718884,0.16160391,0.0972843804,0.5284587664) [ #3 ]
	  #1: (0.3098868776,0.9831179767,0.5296626759,0.9438327159,0.87826215,0.7782872272,0.4166978609,0.2226562315) [ #3 ]
	  #2: (0.5511224272,0.0462225136,0.6177704891,0.4252223039,0.3870529828,0.7761541152,0.6623876005,0.1703382573) [ #5 ]
	  #4: (0.3191112491,0.3110007436,0.691104038,0.1034285073,0.9779993249,0.8022396713,0.6132100036,0.1134380932) [ #5 ]
	  #6: (0.1354387687,0.9625167521,0.799727213,0.4030091993,0.0851281491,0.8056346727,0.6451566176,0.2473998085) [ #7 ]
	Steiner points:
	  #3: (0.3610031681222222,0.4129372162000391,0.5296626759,0.8498845724,0.4177327809222222,0.7782872271999999,0.4166978609,0.2226562315) [ #0 #1 #7 ]
	  #5: (0.3610031681222223,0.3110007436,0.6911040143177093,0.4252223039,0.4177327809222222,0.7874030423666829,0.6132100035999999,0.1703382573) [ #2 #4 #7 ]
	  #7: (0.3610031681222223,0.4129372162000391,0.6911040143177092,0.4252223039,0.4177327809222222,0.7874030423666829,0.6132100035999998,0.22265623150000002) [ #3 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.00046401
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000007474
		Smallest sphere distances: 0.000002007
		Sorting: 0.000003074
		Total init time: 0.000013401

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001755, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000012542, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000410168
	Avarage time pr. topology: 0.000027344

Data for the geometric median iteration:
	Total time: 0.00034487
	Total steps: 345
	Average number of steps pr. geometric median: 7.666666666666667
	Average time pr. step: 0.0000009996231884057972
	Average time pr. geometric median: 0.000007663777777777778

Data for the geometric median step function:
	Total number of fixed points encountered: 1264

