All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.858654414306341
MST len: 6.9399338388
Steiner Ratio: 0.8441945630016805
Solution Steiner tree:
	Terminals:
	  #0: (0.9716272578,0.0232143523,0.740007405,0.5379150429,0.4031608391,0.1850156338,0.0691547445,0.2341284045) [ #3 ]
	  #1: (0.589627737,0.3313394461,0.5059928463,0.5876262708,0.6013480707,0.5097069594,0.2666209295,0.477337444) [ #3 ]
	  #2: (0.8759893737,0.4619202984,0.7565934233,0.2626699606,0.5662877856,0.3799464867,0.4450082292,0.5312397017) [ #5 ]
	  #4: (0.22562532,0.6172168532,0.5542016586,0.1448612135,0.5434454384,0.7647884776,0.324058877,0.6973411928) [ #7 ]
	  #6: (0.8309675659,0.9260082347,0.7454492528,0.1616497483,0.1027180125,0.9668541835,0.2171490557,0.7873155781) [ #7 ]
	Steiner points:
	  #3: (0.6592013565213896,0.47038096179769745,0.6836008289676654,0.5379150429,0.5586736698666668,0.5097069594,0.2666209295,0.5312397017) [ #0 #5 #1 ]
	  #5: (0.6592013565213896,0.47038096179769745,0.6836008289676654,0.26266996060000003,0.5586736698666668,0.5097069594,0.2884222699043414,0.5312397017) [ #2 #3 #7 ]
	  #7: (0.6592013565213897,0.6172168532,0.6836008289676654,0.16164974829999998,0.5434454384,0.7647884776000001,0.2884222699043414,0.6973411928) [ #4 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000680997
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000010562
		Smallest sphere distances: 0.000002738
		Sorting: 0.000018402
		Total init time: 0.000033211

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000357, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000028472, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000582554
	Avarage time pr. topology: 0.000038836

Data for the geometric median iteration:
	Total time: 0.000519698
	Total steps: 378
	Average number of steps pr. geometric median: 8.4
	Average time pr. step: 0.0000013748624338624339
	Average time pr. geometric median: 0.000011548844444444444

