All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.614414562659269
MST len: 5.294040149520041
Steiner Ratio: 0.8716243988209296
Solution Steiner tree:
	Terminals:
	  #0: (0.4733264509,0.4483769622,0.812394509,0.958834955,0.7973932744,0.6833621621,0.5717486453,0.9791705431) [ #9 ]
	  #1: (0.1817772687,0.4070928839,0.1923376211,0.4982240295,0.9403857789,0.7748197265,0.0876867548,0.9821973629) [ #5 ]
	  #2: (0.8501515877,0.8986921589,0.4174514107,0.3731905494,0.4855085041,0.3490163122,0.8709676535,0.6129136903) [ #7 ]
	  #4: (0.4260037464,0.5934402573,0.0282778386,0.0528809102,0.589052372,0.5746342924,0.030925551,0.4392039592) [ #5 ]
	  #6: (0.9948574575,0.3964075797,0.1451486364,0.7376186767,0.6916461264,0.8085904055,0.6644335085,0.1308357772) [ #7 ]
	  #8: (0.4391896513,0.6174133409,0.9290067493,0.8974420614,0.6268771643,0.9956121147,0.5463564538,0.9780446412) [ #9 ]
	  #10: (0.0418115836,0.220037731,0.3087208249,0.1629512739,0.7427612197,0.2952385462,0.6634417692,0.9268148322) [ #11 ]
	Steiner points:
	  #3: (0.5115207560956464,0.5132299860868716,0.45440816027124675,0.5760013310202844,0.7014895176042508,0.6127376211137655,0.5566575605964236,0.7466738455885245) [ #7 #9 #11 ]
	  #5: (0.2934839169559682,0.4484618074377985,0.22681227986042707,0.3556741218768141,0.7719536075508832,0.6185248047888188,0.26230659175982823,0.7884736531113843) [ #1 #4 #11 ]
	  #7: (0.7335370075820069,0.6085215719509023,0.3696356739076657,0.5495018049638087,0.6303231827492328,0.574903501178011,0.6822803196572581,0.5586857733176516) [ #6 #3 #2 ]
	  #9: (0.4711598141479196,0.4984460536350411,0.784555015751463,0.8849994923179441,0.742164205959685,0.7463558700995709,0.5633988598435201,0.9428913087041785) [ #8 #3 #0 ]
	  #11: (0.30684772310894187,0.4237436977268298,0.3050985972720954,0.38067723432649625,0.7470003690005222,0.5565681983269404,0.41866650100777403,0.8027313033268298) [ #3 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.10076175
	Number of best updates: 8

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 963

	Init times:
		Bottleneck Steiner distances: 0.000021194
		Smallest sphere distances: 0.000000961
		Sorting: 0.000002117
		Total init time: 0.000025598

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000894, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003562, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031916, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000339789, bsd pruned: 0, ss pruned: 105

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 840
	Total number iterations 12323 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 14.670238095238096
	Total time: 0.099891183
	Avarage time pr. topology: 0.000118918

Data for the geometric median iteration:
	Total time: 0.095855262
	Total steps: 408034
	Average number of steps pr. geometric median: 97.15095238095238
	Average time pr. step: 0.000000234919790997809
	Average time pr. geometric median: 0.000022822681428571426

