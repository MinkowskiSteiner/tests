All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.3116402243792855
MST len: 6.150778491371538
Steiner Ratio: 0.8635720229285746
Solution Steiner tree:
	Terminals:
	  #0: (0.1036064635,0.5821549858,0.7011187615,0.5075186931,0.9913254278,0.7521004219,0.7428126711,0.9414201057) [ #5 ]
	  #1: (0.7227105865,0.915089529,0.1414719681,0.4651904956,0.4521068681,0.141463833,0.6641981568,0.1102642064) [ #9 ]
	  #2: (0.6581573387,0.8989526019,0.6503086554,0.7898902324,0.7133313109,0.2344475841,0.283649136,0.6526875024) [ #3 ]
	  #4: (0.0401867987,0.325981314,0.631247768,0.3657580602,0.3181859326,0.6278227673,0.2114482025,0.4217923961) [ #7 ]
	  #6: (0.2099777531,0.912566964,0.9293110892,0.2013031804,0.6646673855,0.6721237603,0.1427232857,0.6547720249) [ #7 ]
	  #8: (0.9901046399,0.0505868262,0.7723662428,0.4866999432,0.7424799096,0.5370173396,0.9999918654,0.1990076612) [ #11 ]
	  #10: (0.939356192,0.805739215,0.042332178,0.9785602656,0.3255712615,0.9922046186,0.9965749998,0.8456901428) [ #11 ]
	Steiner points:
	  #3: (0.5665118910367608,0.7710987933405857,0.5795334006948597,0.6283383220587427,0.6590440539629376,0.4060107922065773,0.46091587588263366,0.5781565120466093) [ #9 #2 #5 ]
	  #5: (0.29365879663594713,0.6844718813846202,0.6905279833847666,0.47134618214909674,0.7002201530114509,0.585144206237284,0.4362795100372101,0.6687381638007721) [ #0 #3 #7 ]
	  #7: (0.21871610167822728,0.6767132685006432,0.7462566614765862,0.3734414720928275,0.61294759655424,0.6184304785349671,0.3076140782310264,0.6148816505308864) [ #6 #4 #5 ]
	  #9: (0.709097687540349,0.7223074965716908,0.4181118995691429,0.6060106420921273,0.5723213117365014,0.4187698738245499,0.651111880500777,0.4302085676273277) [ #1 #3 #11 ]
	  #11: (0.7978361852274078,0.6152285779075445,0.41960023252489814,0.6462753675368922,0.5620567159371499,0.5350847587553604,0.7711590993520101,0.45744856330835915) [ #8 #9 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.141884628
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000008219
		Smallest sphere distances: 0.000001037
		Sorting: 0.000002196
		Total init time: 0.000012745

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000823, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003535, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031857, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000366138, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 16677 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 17.64761904761905
	Total time: 0.141002852
	Avarage time pr. topology: 0.000149209

Data for the geometric median iteration:
	Total time: 0.135626008
	Total steps: 579402
	Average number of steps pr. geometric median: 122.62476190476191
	Average time pr. step: 0.0000002340792886458797
	Average time pr. geometric median: 0.000028703917037037035

