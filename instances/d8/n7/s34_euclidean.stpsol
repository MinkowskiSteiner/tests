All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.64559556931383
MST len: 5.506601905564661
Steiner Ratio: 0.8436410782880914
Solution Steiner tree:
	Terminals:
	  #0: (0.206476939,0.6585222355,0.9598705587,0.8190235145,0.886197098,0.6406631272,0.9087747722,0.5221357497) [ #9 ]
	  #1: (0.6182698098,0.6337565941,0.1025872855,0.8561092638,0.1213370185,0.3546023389,0.3640158462,0.48389697) [ #7 ]
	  #2: (0.5856989974,0.4347374083,0.4789228926,0.4983790216,0.7280027888,0.8203571145,0.0597806024,0.4748882849) [ #11 ]
	  #4: (0.3625599515,0.8518746006,0.2945063893,0.4759735886,0.6125465755,0.2276748625,0.680792569,0.0897512576) [ #5 ]
	  #6: (0.7468854965,0.2319139071,0.0643970538,0.576686004,0.5689670525,0.5896822263,0.8609460652,0.737937074) [ #7 ]
	  #8: (0.6359386517,0.9450358706,0.5259626366,0.9567871433,0.9126800242,0.2932653806,0.3414342214,0.5614015812) [ #9 ]
	  #10: (0.1689700215,0.0285875835,0.7728105293,0.3646502115,0.237839454,0.4875804249,0.2520150534,0.5079065824) [ #11 ]
	Steiner points:
	  #3: (0.5157343128047405,0.5302873617427785,0.37300953276358656,0.6316641687463761,0.5340144174574616,0.4900203471105162,0.4610622555984028,0.4719440367012327) [ #5 #7 #11 ]
	  #5: (0.4621634869551821,0.6908399591187356,0.43760582060594666,0.660865968962256,0.6494983954524213,0.4074747130363185,0.5463812348288716,0.3918766243136348) [ #3 #4 #9 ]
	  #7: (0.5835026822405261,0.49817086374974257,0.2511858179025251,0.6739596322279174,0.44374541243374266,0.4770713635912237,0.513876560793959,0.5250346983937565) [ #6 #3 #1 ]
	  #9: (0.4623466722389624,0.7537040039287766,0.557189420318514,0.7699142031748967,0.7640755346927038,0.41928832814670086,0.557310915750873,0.4616117921354125) [ #8 #5 #0 ]
	  #11: (0.47034147359199313,0.3943967095582986,0.4919228913557138,0.5299261704833546,0.543142102322943,0.6074008243463969,0.2754553552462723,0.48029146448101684) [ #2 #3 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.144835819
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000021072
		Smallest sphere distances: 0.000000924
		Sorting: 0.000002137
		Total init time: 0.000025714

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000922, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00000347, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031447, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.00035464, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 17313 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 18.32063492063492
	Total time: 0.143943548
	Avarage time pr. topology: 0.000152321

Data for the geometric median iteration:
	Total time: 0.138349034
	Total steps: 590164
	Average number of steps pr. geometric median: 124.90243386243387
	Average time pr. step: 0.00000023442472600836378
	Average time pr. geometric median: 0.00002928021883597884

