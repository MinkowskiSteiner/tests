All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.050561052549325
MST len: 4.609411786723152
Steiner Ratio: 0.8787587744311479
Solution Steiner tree:
	Terminals:
	  #0: (0.2613725733,0.9178440775,0.1227969854,0.1065639989,0.6592969767,0.5933043922,0.4828257661,0.3829715771) [ #3 ]
	  #1: (0.9910994321,0.8735950118,0.5318192572,0.054165287,0.845191426,0.7414528996,0.4705074068,0.3762617672) [ #11 ]
	  #2: (0.3297338986,0.9487606892,0.4132981074,0.5756729662,0.8249852354,0.3957488953,0.5658004533,0.0595591958) [ #5 ]
	  #4: (0.7236746008,0.4541832616,0.7798275243,0.608648137,0.7615917403,0.6558818466,0.357595246,0.5872719691) [ #9 ]
	  #6: (0.5014803892,0.6817833715,0.8219771086,0.3264656241,0.0775322663,0.3877775615,0.3860248203,0.0686316979) [ #7 ]
	  #8: (0.5518146714,0.5618452218,0.610652922,0.9289213582,0.9165702439,0.5527197004,0.268485264,0.2463041419) [ #9 ]
	  #10: (0.8256802293,0.8959328252,0.2042499758,0.0233809524,0.3771066863,0.354725022,0.9420667789,0.3395639064) [ #11 ]
	Steiner points:
	  #3: (0.5088672142281628,0.8722420340473686,0.3304226450885439,0.2301307210177361,0.6563566165470915,0.5340027202218737,0.5463175363302326,0.3003116416513339) [ #0 #5 #11 ]
	  #5: (0.4629209189164051,0.8328470814131425,0.45631743444356065,0.42606572278945576,0.6759608355257503,0.47999047330826305,0.506926015918934,0.20788735827118843) [ #7 #3 #2 ]
	  #7: (0.5096590570573637,0.7308330766694509,0.5797596226114328,0.48459230678803444,0.5989323637869476,0.49044937679669504,0.44011123130371854,0.22836279623464498) [ #5 #6 #9 ]
	  #9: (0.5973686844645723,0.5778762136894362,0.6588097002405054,0.6805801878219326,0.7643187108446708,0.5685093981124787,0.35258559334928774,0.3572180159475792) [ #4 #7 #8 ]
	  #11: (0.7038325831349865,0.8781656370993733,0.35089725369875985,0.13749659686738053,0.637603508524395,0.5434650646895282,0.6207275182163143,0.3285080946929232) [ #1 #3 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.106756379
	Number of best updates: 12

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 963

	Init times:
		Bottleneck Steiner distances: 0.000008924
		Smallest sphere distances: 0.000000951
		Sorting: 0.000002266
		Total init time: 0.000013513

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001257, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003549, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032118, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000322284, bsd pruned: 0, ss pruned: 105

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 840
	Total number iterations 14567 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 17.341666666666665
	Total time: 0.105931724
	Avarage time pr. topology: 0.000126109

Data for the geometric median iteration:
	Total time: 0.101207918
	Total steps: 429769
	Average number of steps pr. geometric median: 102.32595238095239
	Average time pr. step: 0.0000002354937606016255
	Average time pr. geometric median: 0.000024097123333333333

