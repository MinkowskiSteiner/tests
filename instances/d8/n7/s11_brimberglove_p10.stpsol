All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 9.223808504689021
MST len: 11.426128172200002
Steiner Ratio: 0.8072558232919819
Solution Steiner tree:
	Terminals:
	  #0: (0.7182401273,0.4365231644,0.9778233603,0.0999500035,0.4717727618,0.3465953233,0.6167704093,0.4288659084) [ #5 ]
	  #1: (0.8329159067,0.4041060449,0.0752406661,0.2481696737,0.3817098766,0.0352495974,0.3687719635,0.5168204054) [ #7 ]
	  #2: (0.9263453185,0.5264996991,0.3097808907,0.5022105041,0.7537675108,0.4059770589,0.4484664097,0.2208949445) [ #3 ]
	  #4: (0.4592865964,0.7046087811,0.729890453,0.0711031715,0.4371118506,0.8601203439,0.1809798303,0.3634571686) [ #9 ]
	  #6: (0.70731932,0.1956754286,0.1610363345,0.9483695021,0.9867749587,0.3349739184,0.9654330178,0.4460615546) [ #7 ]
	  #8: (0.3866200426,0.490760721,0.8656676732,0.1403875533,0.8967377799,0.3141340824,0.3612824978,0.7296536866) [ #9 ]
	  #10: (0.9570931466,0.3607239967,0.5789050197,0.7321704266,0.2410501816,0.7910995301,0.1739375839,0.0170635157) [ #11 ]
	Steiner points:
	  #3: (0.8329159067,0.41125677516666653,0.3097808906999999,0.5022105041000001,0.4717727618,0.4059770589,0.3687719635,0.24799261764900055) [ #7 #2 #11 ]
	  #5: (0.7182401273,0.4365231644,0.7622062776666667,0.1403875533,0.4717727618,0.4059770589,0.36128249780000116,0.3634571686) [ #0 #9 #11 ]
	  #7: (0.8329159067,0.4041060449,0.1610363345,0.5022105041000001,0.4717727618,0.3349739184,0.3687719635,0.44606155459999997) [ #3 #6 #1 ]
	  #9: (0.4592865964,0.490760721,0.7622062776666667,0.1403875533,0.4717727618,0.4059770589,0.3612824978,0.3634571686) [ #8 #5 #4 ]
	  #11: (0.8329159067000002,0.41125677516666653,0.5789050197,0.5022105041000001,0.47177100153983814,0.40597999712885874,0.36128249780000116,0.24799261764900055) [ #3 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.036121589
	Number of best updates: 9

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000020703
		Smallest sphere distances: 0.000005445
		Sorting: 0.000013669
		Total init time: 0.000041215

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002084, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000011944, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000119465, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.00123868, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 1910 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0211640211640214
	Total time: 0.033233266
	Avarage time pr. topology: 0.000035167

Data for the geometric median iteration:
	Total time: 0.028325463
	Total steps: 32970
	Average number of steps pr. geometric median: 6.977777777777778
	Average time pr. step: 0.0000008591283894449499
	Average time pr. geometric median: 0.000005994806984126984

Data for the geometric median step function:
	Total number of fixed points encountered: 166143

