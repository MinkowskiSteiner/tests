All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.855001646839977
MST len: 4.332767106650915
Steiner Ratio: 0.8897320238889472
Solution Steiner tree:
	Terminals:
	  #0: (0.9474799428,0.1736593936,0.3251002693,0.2867596859,0.331827752,0.4798882434,0.6288429693,0.6801386246) [ #7 ]
	  #1: (0.3079489355,0.6126976412,0.4672263043,0.5413947988,0.3004468834,0.263256482,0.8872430967,0.7088636578) [ #5 ]
	  #2: (0.3740666413,0.1966241674,0.3118738645,0.6745135252,0.4598806493,0.1991169612,0.3833771825,0.5127728258) [ #11 ]
	  #4: (0.0063632978,0.7409414774,0.3425149626,0.8681538691,0.4625301303,0.7613690005,0.7293978635,0.7704790662) [ #9 ]
	  #6: (0.6787575826,0.1165880943,0.0680224551,0.0105853342,0.5964763377,0.6968654244,0.6907239592,0.6028396355) [ #7 ]
	  #8: (0.4618455854,0.663615777,0.8218634635,0.6677439318,0.8337828269,0.505098189,0.791487825,0.7812627697) [ #9 ]
	  #10: (0.0528921765,0.2627286242,0.2802385941,0.3090906377,0.2058983465,0.1701670499,0.6832347255,0.1237438932) [ #11 ]
	Steiner points:
	  #3: (0.415216888434052,0.34001576646780446,0.3487382872445388,0.47682312940454646,0.3929056679297599,0.33589976622252177,0.6477322748953408,0.5597910514060165) [ #7 #5 #11 ]
	  #5: (0.3246880793658008,0.553591475751706,0.4567296602431161,0.5655988240939318,0.39206389069760417,0.352424657364961,0.7970000905028893,0.68000451917776) [ #9 #3 #1 ]
	  #7: (0.7400741308203739,0.19988949824646055,0.260709934595623,0.25900667191392557,0.41959861015238764,0.5030345332651402,0.6505248842197261,0.6287178825948596) [ #3 #6 #0 ]
	  #9: (0.28054795970897495,0.6293754717109802,0.523380964430607,0.6682612085440763,0.5250976776991766,0.4950017262049628,0.7785813738926021,0.7291677554144596) [ #4 #5 #8 ]
	  #11: (0.34489574371766046,0.2839139901411144,0.32656321320197357,0.5104463430977527,0.3835450357886892,0.26771709415081824,0.5726760712376613,0.47592643488089975) [ #2 #3 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.122152151
	Number of best updates: 8

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000007926
		Smallest sphere distances: 0.000000953
		Sorting: 0.000002123
		Total init time: 0.000012407

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000836, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003481, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032002, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000357214, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 16424 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 17.37989417989418
	Total time: 0.121266313
	Avarage time pr. topology: 0.000128324

Data for the geometric median iteration:
	Total time: 0.115959884
	Total steps: 489784
	Average number of steps pr. geometric median: 103.65798941798941
	Average time pr. step: 0.00000023675719092497918
	Average time pr. geometric median: 0.00002454177439153439

