All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.2678653392382655
MST len: 4.537795628604858
Steiner Ratio: 0.9405151065717823
Solution Steiner tree:
	Terminals:
	  #0: (0.206965448,0.6995153961,0.2034307295,0.3588790108,0.884531313,0.4632373589,0.7227528005,0.6292115332) [ #3 ]
	  #1: (0.7602754271,0.4969397343,0.2488169443,0.8343745516,0.425641888,0.1330993646,0.9261679467,0.5155373381) [ #5 ]
	  #2: (0.4638889103,0.6119702568,0.4188058225,0.5586368472,0.2869822789,0.6803660592,0.8316096942,0.2784400723) [ #9 ]
	  #4: (0.9291220382,0.1416073912,0.3070574362,0.8136533512,0.6048447502,0.0298102363,0.4428648839,0.3256396411) [ #7 ]
	  #6: (0.9914577939,0.2373887199,0.3081078475,0.6704191042,0.7412129779,0.229606642,0.9381766622,0.9481784259) [ #7 ]
	  #8: (0.0898954501,0.3307895457,0.6858023101,0.9032684844,0.0947479369,0.6750120226,0.2615602367,0.272972847) [ #11 ]
	  #10: (0.7207948913,0.7304651908,0.05407485,0.9231773037,0.0740991244,0.9287021542,0.8842824203,0.0917933952) [ #11 ]
	Steiner points:
	  #3: (0.4970701823993574,0.5757928265646141,0.3068459954147324,0.5929759691846992,0.510073691789581,0.4556148847935541,0.8041197292061698,0.4563302802134755) [ #0 #9 #5 ]
	  #5: (0.7465284874841717,0.4522165523441848,0.2722596826161454,0.7667247332280073,0.4921108050625209,0.19840897725829154,0.8556124216149159,0.5287954298095052) [ #3 #1 #7 ]
	  #7: (0.8409679027111192,0.3369212012604889,0.2878238139484252,0.7552072614624089,0.572415081060943,0.16904317232622063,0.7851126964877413,0.5792105923754617) [ #4 #6 #5 ]
	  #9: (0.4659183096896844,0.6036952202662983,0.39994598576819873,0.5903529484224455,0.29455954033544507,0.6673692771017226,0.8113717620306974,0.2888306338669143) [ #2 #3 #11 ]
	  #11: (0.4597374698190852,0.5895173339566934,0.37931867006469283,0.686224757695983,0.23201945364263943,0.711868493011085,0.7524246140045797,0.25397428698826385) [ #8 #9 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.097372249
	Number of best updates: 11

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 963

	Init times:
		Bottleneck Steiner distances: 0.000008176
		Smallest sphere distances: 0.000000965
		Sorting: 0.000002215
		Total init time: 0.000012697

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000825, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003487, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032117, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.00032346, bsd pruned: 0, ss pruned: 105

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 840
	Total number iterations 11830 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 14.083333333333334
	Total time: 0.096541691
	Avarage time pr. topology: 0.00011493

Data for the geometric median iteration:
	Total time: 0.092672469
	Total steps: 394239
	Average number of steps pr. geometric median: 93.86642857142857
	Average time pr. step: 0.00000023506672094846018
	Average time pr. geometric median: 0.00002206487357142857

