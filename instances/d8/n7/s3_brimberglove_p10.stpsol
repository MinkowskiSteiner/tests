All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 8.734410660586686
MST len: 11.596852804400001
Steiner Ratio: 0.7531707789955507
Solution Steiner tree:
	Terminals:
	  #0: (0.0099969395,0.257080875,0.0407533725,0.4439474528,0.1744425461,0.3665206686,0.0588192782,0.5624328081) [ #3 ]
	  #1: (0.0937586353,0.1635579272,0.2121971949,0.3787998862,0.3083389789,0.7757516945,0.2434785898,0.2037408148) [ #3 ]
	  #2: (0.5613801752,0.2249833128,0.3930917794,0.4439383934,0.2850412509,0.1447810513,0.5635544996,0.8646787037) [ #5 ]
	  #4: (0.8954018363,0.2308046744,0.0040116059,0.4605808875,0.1763232952,0.6261662299,0.9448735988,0.8460845993) [ #7 ]
	  #6: (0.8662761761,0.0697176354,0.3276008178,0.0407187217,0.436238304,0.386420096,0.6031515303,0.5701775186) [ #7 ]
	  #8: (0.0065563684,0.2474901957,0.6643217023,0.1828796636,0.873656426,0.6091953011,0.0289642629,0.8836533655) [ #9 ]
	  #10: (0.1339392146,0.878686387,0.568443856,0.5212840333,0.2068786268,0.8687753225,0.7704661483,0.768258802) [ #11 ]
	Steiner points:
	  #3: (0.09375863529999999,0.257080875,0.2121971949,0.37879988620040683,0.3083389788994946,0.6261662298999997,0.2434785898,0.5624328081) [ #1 #0 #9 ]
	  #5: (0.5613801752,0.23080467439999994,0.3930917794,0.4439383934,0.2850412509,0.6261662299,0.5635544996,0.7654650776388968) [ #2 #7 #11 ]
	  #7: (0.8662761761,0.23080467439999997,0.3276008178,0.4605808875,0.2850412509,0.6261662299,0.6031515303,0.7654650776388968) [ #4 #6 #5 ]
	  #9: (0.09375863529999999,0.257080875,0.4515424715999442,0.37879988620040683,0.3083389788994946,0.6261662298999997,0.2434785898,0.7663963190925912) [ #3 #8 #11 ]
	  #11: (0.1339392146,0.257080875,0.4515424715999442,0.4439383934,0.2850412509,0.6261699317644825,0.5635544996,0.7663963190925912) [ #5 #9 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.03650969
	Number of best updates: 16

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000014702
		Smallest sphere distances: 0.000004139
		Sorting: 0.000010489
		Total init time: 0.000030376

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001927, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000011473, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000109572, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001249648, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 1926 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.038095238095238
	Total time: 0.033545609
	Avarage time pr. topology: 0.000035497

Data for the geometric median iteration:
	Total time: 0.028551448
	Total steps: 32584
	Average number of steps pr. geometric median: 6.896084656084656
	Average time pr. step: 0.0000008762413454456175
	Average time pr. geometric median: 0.000006042634497354497

Data for the geometric median step function:
	Total number of fixed points encountered: 166280

