All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.717458454542411
MST len: 5.3272427584281035
Steiner Ratio: 0.8855347256475281
Solution Steiner tree:
	Terminals:
	  #0: (0.1368193762,0.5865269595,0.9152492289,0.625351081,0.5795209741,0.7026967819,0.7732438994,0.6614730608) [ #5 ]
	  #1: (0.0179714379,0.5034501541,0.1039613784,0.3216621426,0.2525900748,0.6675356089,0.214533581,0.4014023423) [ #7 ]
	  #2: (0.4076549855,0.7457033986,0.781220815,0.0627908218,0.1360618449,0.4750343694,0.4330457674,0.714648994) [ #3 ]
	  #4: (0.1488122675,0.7401193766,0.5311698176,0.3798184727,0.6551358368,0.3903584468,0.6938135543,0.3702549456) [ #5 ]
	  #6: (0.9346499368,0.085017812,0.839064103,0.3180151583,0.1074288493,0.8254372672,0.1640398778,0.1254002872) [ #11 ]
	  #8: (0.0819520862,0.2319531549,0.3117739127,0.1775910422,0.383365222,0.0224110372,0.9863731647,0.8460247199) [ #9 ]
	  #10: (0.5785871486,0.6617850073,0.153481192,0.2006002349,0.4885317048,0.9929940151,0.7874475535,0.1478928184) [ #11 ]
	Steiner points:
	  #3: (0.285536036594669,0.5951635250999613,0.5378836754320862,0.24345513897092308,0.33605135625765825,0.52572364629489,0.545427705742426,0.5336674295144243) [ #7 #2 #9 ]
	  #5: (0.17719542546605307,0.6317449412193665,0.5994889364073825,0.38204790965456475,0.5354253544796667,0.465769734711721,0.6935966719777478,0.5105682399369923) [ #9 #0 #4 ]
	  #7: (0.3113334329159573,0.5365396655434758,0.3946609351578532,0.2645412431931268,0.3157405125506893,0.6545982581793559,0.4590014994544927,0.40947770367436953) [ #1 #3 #11 ]
	  #9: (0.20790064781664425,0.5756291213123963,0.5458212264861808,0.3082107153428239,0.44425212899401273,0.4413872797001064,0.6688160842680805,0.5547939353704393) [ #8 #5 #3 ]
	  #11: (0.4819913196569494,0.49710602479218796,0.4023394308577482,0.2562445613263257,0.32791523548700197,0.7713832756515926,0.498362281469608,0.294790780624096) [ #6 #7 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.147656474
	Number of best updates: 10

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000008825
		Smallest sphere distances: 0.000000976
		Sorting: 0.000001976
		Total init time: 0.000012936

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000796, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003419, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031696, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000366716, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 17521 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 18.54074074074074
	Total time: 0.146776235
	Avarage time pr. topology: 0.000155318

Data for the geometric median iteration:
	Total time: 0.141146784
	Total steps: 599618
	Average number of steps pr. geometric median: 126.90328042328042
	Average time pr. step: 0.000000235394507836656
	Average time pr. geometric median: 0.000029872335238095237

