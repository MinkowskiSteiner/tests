All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 10.1180186478377
MST len: 12.4831965456
Steiner Ratio: 0.8105310695764087
Solution Steiner tree:
	Terminals:
	  #0: (0.0763532385,0.8217415124,0.9652387276,0.3917416899,0.3736224269,0.9697014317,0.4773686391,0.6483680232) [ #7 ]
	  #1: (0.4982478328,0.8940018629,0.0156746367,0.2746502162,0.9714187407,0.2212781451,0.4164950971,0.2561370005) [ #9 ]
	  #2: (0.3634193076,0.2718471113,0.4159667661,0.2562429226,0.3028504617,0.4612696383,0.4511669648,0.2853909383) [ #3 ]
	  #4: (0.016169196,0.4701238631,0.7283984682,0.1638579933,0.5497685357,0.0089649442,0.6123133984,0.048016368) [ #11 ]
	  #6: (0.0110087921,0.546489314,0.4689284104,0.3138592538,0.0077589517,0.9200953757,0.5992501921,0.0841121902) [ #7 ]
	  #8: (0.9029668076,0.6279880356,0.3226665842,0.8743855478,0.8492661807,0.7391616813,0.1305225478,0.2126854878) [ #9 ]
	  #10: (0.2747455962,0.0464677648,0.9927552245,0.080030445,0.1476887968,0.0796446726,0.2805664764,0.4484554052) [ #11 ]
	Steiner points:
	  #3: (0.3634193076,0.4701238631,0.4159667661,0.2562429226,0.3028504617,0.39390599047440095,0.4164952967282063,0.2853909383) [ #7 #5 #2 ]
	  #5: (0.3634193076,0.4701238631,0.4159667661,0.2562429226,0.41574195606918785,0.39390599047440067,0.4164952967282063,0.24165316293333333) [ #3 #9 #11 ]
	  #7: (0.0763532385,0.4701238631,0.46892841039999994,0.3138592538,0.3028504617,0.9200953757,0.4164952967282063,0.2853909383) [ #3 #6 #0 ]
	  #9: (0.4982478328,0.6279880356,0.3226665842,0.2562429226,0.8492661807,0.39390599047440067,0.41649437131301315,0.24165316293333333) [ #8 #5 #1 ]
	  #11: (0.2747455962,0.47012063088514033,0.7283984681999999,0.1638579933,0.41574195606918785,0.0796446726,0.4164952967282063,0.24165316293333333) [ #4 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.036393218
	Number of best updates: 9

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.00002139
		Smallest sphere distances: 0.000005459
		Sorting: 0.000013379
		Total init time: 0.000041817

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000229, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00001205, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000109677, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001224764, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 1916 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0275132275132277
	Total time: 0.033479462
	Avarage time pr. topology: 0.000035428

Data for the geometric median iteration:
	Total time: 0.028480479
	Total steps: 32758
	Average number of steps pr. geometric median: 6.932910052910053
	Average time pr. step: 0.0000008694205690213077
	Average time pr. geometric median: 0.0000060276146031746025

Data for the geometric median step function:
	Total number of fixed points encountered: 170809

