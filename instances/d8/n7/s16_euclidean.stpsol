All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.94905041182662
MST len: 6.912056422599277
Steiner Ratio: 0.860677351008874
Solution Steiner tree:
	Terminals:
	  #0: (0.7737779938,0.7413405407,0.9948667465,0.1571419924,0.0299462825,0.8186675654,0.4665911298,0.888382337) [ #9 ]
	  #1: (0.2154374701,0.0895364271,0.3215230151,0.5994994997,0.9209823329,0.0083380486,0.2498201687,0.6923724262) [ #7 ]
	  #2: (0.3582976681,0.8231695224,0.1180784167,0.5551794663,0.3833639991,0.2886057418,0.8238008194,0.3094491373) [ #11 ]
	  #4: (0.0997784725,0.5895236719,0.2716148031,0.0401816108,0.8858513329,0.9203709387,0.7515285484,0.101288803) [ #5 ]
	  #6: (0.4970601138,0.5055110541,0.9216866991,0.5953774013,0.811042468,0.1000043224,0.9623798765,0.9108209405) [ #7 ]
	  #8: (0.0099073653,0.073051563,0.7007883027,0.9308896982,0.0813896121,0.9506084719,0.623262124,0.4396872802) [ #9 ]
	  #10: (0.8584360545,0.6783925484,0.0389199243,0.0333043621,0.0983172874,0.3055314134,0.1783176238,0.3670024752) [ #11 ]
	Steiner points:
	  #3: (0.3683282738240056,0.4903622275018885,0.5200718114605443,0.45584622532956903,0.4826947476635092,0.4907514596473108,0.6202361744764211,0.5473565200159334) [ #7 #5 #9 ]
	  #5: (0.36220234818180397,0.5958790257499883,0.3464544580408271,0.3637206479500546,0.4994893803576395,0.5085089507085614,0.6470614759313664,0.4001812727952764) [ #4 #3 #11 ]
	  #7: (0.36624878085069734,0.3925138425854812,0.584571799642051,0.532466310790391,0.688554894649063,0.25575506332190895,0.6238715561455946,0.6885128963043399) [ #6 #3 #1 ]
	  #9: (0.3755678331611311,0.45352404006524427,0.6554908638343847,0.49511818222041726,0.30515410742405097,0.6551116868068197,0.5894174752852922,0.5944178849596492) [ #8 #3 #0 ]
	  #11: (0.43135213110649717,0.6834639146421264,0.22655967683528416,0.380718824418547,0.40376980000630114,0.40628697789301,0.6395071005054495,0.3651841408715236) [ #2 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.17341163
	Number of best updates: 10

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000021375
		Smallest sphere distances: 0.000000923
		Sorting: 0.000003143
		Total init time: 0.000026761

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000913, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003572, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031895, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.00035506, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 19420 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 20.55026455026455
	Total time: 0.172510816
	Avarage time pr. topology: 0.000182551

Data for the geometric median iteration:
	Total time: 0.16628175
	Total steps: 711564
	Average number of steps pr. geometric median: 150.59555555555556
	Average time pr. step: 0.00000023368488287771726
	Average time pr. geometric median: 0.00003519190476190476

