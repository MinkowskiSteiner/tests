All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 11.272891752053523
MST len: 14.836856432999998
Steiner Ratio: 0.7597897710313124
Solution Steiner tree:
	Terminals:
	  #0: (0.7674754284,0.5535528709,0.1830364662,0.1208428671,0.4189587028,0.2236584333,0.8412560578,0.7969271195) [ #13 ]
	  #1: (0.7731688911,0.7598976808,0.3297242696,0.3815315046,0.0944045461,0.485553364,0.1763587325,0.0226054806) [ #5 ]
	  #2: (0.6326431831,0.3414155828,0.904109925,0.1300345134,0.6083626135,0.3345068657,0.1558290944,0.7948272283) [ #3 ]
	  #4: (0.3779684163,0.1168798023,0.6027370345,0.4139833741,0.5670465732,0.4317533082,0.8557877559,0.1996897558) [ #11 ]
	  #6: (0.792852993,0.8622477059,0.4365024196,0.0231408114,0.3533674392,0.8654058324,0.0406219666,0.7204131906) [ #7 ]
	  #8: (0.3405382355,0.4439930918,0.2109104931,0.9075848092,0.8757464,0.0666982485,0.1072745645,0.6489152911) [ #9 ]
	  #10: (0.0416181572,0.251262738,0.0757720694,0.205355405,0.3670426623,0.9052277226,0.1170504508,0.1598956548) [ #11 ]
	  #12: (0.928200935,0.5560647936,0.0749040051,0.0531665883,0.1637372478,0.1157799243,0.8294556531,0.9116950463) [ #13 ]
	Steiner points:
	  #3: (0.6326480460538692,0.5426417189188454,0.3297218571092263,0.1208428671,0.3670379695183088,0.3345068657,0.1763587326020742,0.7204131906000001) [ #7 #2 #13 ]
	  #5: (0.6326480460538686,0.5305128620644157,0.3297242696,0.3815315046,0.3670406323961682,0.4855533626978776,0.17636861605958393,0.18642505546666666) [ #9 #1 #11 ]
	  #7: (0.6326480460538692,0.5426417189188454,0.3297218571092263,0.1208428671,0.3670379695183088,0.3345068657,0.1763587326020742,0.7204131906000001) [ #3 #6 #9 ]
	  #9: (0.6326480460538686,0.5305128620644157,0.3297215290168612,0.3815315046,0.3670406323961682,0.3345068657,0.17636861605958393,0.6489152911) [ #5 #8 #7 ]
	  #11: (0.3779684163,0.251262738,0.3297242696,0.3815315046,0.3670448108297904,0.4855533626978776,0.17636861605958393,0.18642505546666666) [ #4 #5 #10 ]
	  #13: (0.7674754284,0.5535528709,0.1830364662,0.12083873671984742,0.3670379695183088,0.2236584333,0.8294556531,0.7969271194999998) [ #0 #3 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.309882868
	Number of best updates: 10

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 7683

	Init times:
		Bottleneck Steiner distances: 0.000030211
		Smallest sphere distances: 0.00000788
		Sorting: 0.000016998
		Total init time: 0.000058364

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002793, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000012139, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000110141, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001243239, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.011746026, bsd pruned: 0, ss pruned: 3780

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 6615
	Total number iterations 13525 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0445956160241874
	Total time: 0.283444448
	Avarage time pr. topology: 0.000042848

Data for the geometric median iteration:
	Total time: 0.241624164
	Total steps: 276533
	Average number of steps pr. geometric median: 6.96732174351222
	Average time pr. step: 0.0000008737624948921106
	Average time pr. geometric median: 0.000006087784429327287

Data for the geometric median step function:
	Total number of fixed points encountered: 1460374

