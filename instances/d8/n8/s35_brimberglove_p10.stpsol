All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 10.40871712745437
MST len: 13.331744891000001
Steiner Ratio: 0.7807467974039235
Solution Steiner tree:
	Terminals:
	  #0: (0.3753514985,0.6495995315,0.774436301,0.8460808666,0.2345622611,0.808130308,0.2618280315,0.6956284767) [ #3 ]
	  #1: (0.5069192892,0.0163788139,0.4488871873,0.153680781,0.3970212501,0.4026706854,0.324931977,0.215564177) [ #7 ]
	  #2: (0.809808453,0.6971266361,0.7090024225,0.6096137295,0.470729368,0.5849627301,0.033694007,0.4157471649) [ #3 ]
	  #4: (0.8923050812,0.582232727,0.5161556138,0.8994785263,0.355014119,0.6782248629,0.940597109,0.1648225715) [ #9 ]
	  #6: (0.6606961906,0.4311483691,0.7982122655,0.1291394788,0.0071734451,0.7727813925,0.162069249,0.0411185827) [ #13 ]
	  #8: (0.818542927,0.0240870542,0.352482224,0.9860804393,0.7263199448,0.4611567121,0.784020462,0.387016135) [ #11 ]
	  #10: (0.461066215,0.6987889817,0.7545507828,0.7532587111,0.6467614917,0.3806424362,0.9537834986,0.171251196) [ #11 ]
	  #12: (0.5244897048,0.4379004908,0.0703035561,0.1812310276,0.1675375119,0.7022328906,0.1086744881,0.7979400231) [ #13 ]
	Steiner points:
	  #3: (0.5424262013822011,0.6495995315,0.7090024224999999,0.6096137295,0.355014119,0.6674863374583858,0.2618280315,0.4157471649) [ #2 #0 #5 ]
	  #5: (0.5424262013822011,0.4505705763380856,0.5161556138,0.6096137295,0.355014119,0.6674863374583858,0.2618280315,0.4096894352924554) [ #7 #9 #3 ]
	  #7: (0.5424262013822011,0.4333990755926329,0.5161556138,0.153680781,0.355014119,0.6674863374583858,0.2618280315,0.4096894352924554) [ #1 #5 #13 ]
	  #9: (0.6993840229999999,0.4505705763380856,0.5161556138,0.8994785263,0.355014119,0.6674863374583858,0.8406081408666666,0.31509448866666667) [ #11 #5 #4 ]
	  #11: (0.6993840229999999,0.4505705763380856,0.5161556138,0.8994785263,0.6467614917,0.46115671210000003,0.8406081408666666,0.31509448866666667) [ #8 #9 #10 ]
	  #13: (0.5424262013822011,0.4333990755926329,0.5161556138,0.153680781,0.16753751190000002,0.7022328906000002,0.16206924900000003,0.40968943529245533) [ #6 #7 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.397920484
	Number of best updates: 13

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 10518

	Init times:
		Bottleneck Steiner distances: 0.000023883
		Smallest sphere distances: 0.00000514
		Sorting: 0.000016211
		Total init time: 0.000046418

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000182, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000011501, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000107355, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001215799, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.015126369, bsd pruned: 0, ss pruned: 945

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 9450
	Total number iterations 19149 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0263492063492063
	Total time: 0.365147168
	Avarage time pr. topology: 0.000038639

Data for the geometric median iteration:
	Total time: 0.306908923
	Total steps: 353826
	Average number of steps pr. geometric median: 6.240317460317461
	Average time pr. step: 0.0000008674007082577312
	Average time pr. geometric median: 0.000005412855784832452

