All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 10.450938884065332
MST len: 12.3783705067
Steiner Ratio: 0.8442903594143176
Solution Steiner tree:
	Terminals:
	  #0: (0.5982263575,0.1752538104,0.4766825421,0.1399146664,0.4673976025,0.8627957766,0.6898134,0.18037494) [ #11 ]
	  #1: (0.3503611406,0.4857762337,0.7851141266,0.5901703474,0.1827713406,0.9935369142,0.1363650975,0.5229908035) [ #3 ]
	  #2: (0.5748512617,0.1829102199,0.3235021184,0.6120159149,0.7084588719,0.4244470016,0.6334648415,0.41691941) [ #7 ]
	  #4: (0.5273643888,0.459714527,0.8212127349,0.2358232607,0.8841615286,0.4546775759,0.6527426707,0.2345226692) [ #13 ]
	  #6: (0.1450036579,0.1299871505,0.3764338714,0.8339692707,0.916499746,0.3131860557,0.29486724,0.5147261031) [ #9 ]
	  #8: (0.0910452647,0.0226498251,0.4103666397,0.0075450102,0.3358358808,0.7052338797,0.5222711137,0.8242757473) [ #9 ]
	  #10: (0.9404538097,0.4378567969,0.8246930166,0.1232251502,0.4313937111,0.9610581146,0.6462159537,0.576397369) [ #11 ]
	  #12: (0.4884398666,0.7715497821,0.6546407694,0.9558374691,0.6343455583,0.344454169,0.1362124086,0.2091968196) [ #13 ]
	Steiner points:
	  #3: (0.5273643888,0.459714527,0.6770128442364856,0.612015914775292,0.4553963053666667,0.8955498892666667,0.5222711139308653,0.4169193739726752) [ #11 #5 #1 ]
	  #5: (0.5273643888,0.459714527,0.6770128442364856,0.612015914775292,0.7084588719,0.42444700160053483,0.5222711139308653,0.4169193739726752) [ #3 #7 #13 ]
	  #7: (0.5273643888,0.18291021989999998,0.3877447941660661,0.6120159149,0.7084588719,0.4244470016,0.5222711139308653,0.4169193739726752) [ #9 #5 #2 ]
	  #9: (0.14500365790000003,0.1299871505,0.3877447941660661,0.6120159149,0.7084588719000001,0.4244470016,0.5222710595980552,0.5147261031) [ #6 #7 #8 ]
	  #11: (0.5982263575,0.4378567969,0.6770128442364856,0.1399146664,0.4553963053666667,0.8955498892666667,0.5222711139308653,0.4169193739726752) [ #10 #3 #0 ]
	  #13: (0.5273640918317289,0.4597169060960389,0.6770128442364856,0.612015914775292,0.7084588719,0.42444700160053483,0.5222711139308653,0.23452266919999998) [ #4 #5 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.428265747
	Number of best updates: 13

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 11463

	Init times:
		Bottleneck Steiner distances: 0.000031645
		Smallest sphere distances: 0.00000533
		Sorting: 0.000016581
		Total init time: 0.000054938

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001836, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000011571, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000107086, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001216607, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.016265445, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 10395
	Total number iterations 21152 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0348244348244346
	Total time: 0.393128501
	Avarage time pr. topology: 0.000037818

Data for the geometric median iteration:
	Total time: 0.329085919
	Total steps: 379724
	Average number of steps pr. geometric median: 6.088247554914221
	Average time pr. step: 0.0000008666450342880619
	Average time pr. geometric median: 0.0000052763495109828445

