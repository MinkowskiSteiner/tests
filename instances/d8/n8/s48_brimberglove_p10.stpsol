All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 11.127615780989803
MST len: 14.0863231093
Steiner Ratio: 0.7899588625539326
Solution Steiner tree:
	Terminals:
	  #0: (0.7162293935,0.6441561727,0.4240332141,0.9419499133,0.5912994475,0.4804747782,0.9604098899,0.7635887069) [ #9 ]
	  #1: (0.6838657743,0.3087545369,0.0063704299,0.343231192,0.8108547613,0.5749283333,0.3027468763,0.1342774458) [ #5 ]
	  #2: (0.9519015979,0.4328879981,0.4879451569,0.8024439862,0.3533975968,0.9678474162,0.471497888,0.0372633706) [ #13 ]
	  #4: (0.1172303674,0.1845542151,0.4791337119,0.5335158885,0.2291348289,0.1519367942,0.2794510882,0.1810364263) [ #11 ]
	  #6: (0.1722892593,0.6367555897,0.2241443257,0.7155450055,0.4162855211,0.0445806133,0.6728030828,0.7459352001) [ #7 ]
	  #8: (0.5848247928,0.767396245,0.9834804125,0.9382223896,0.7352436607,0.4549783,0.9754857602,0.0118456134) [ #9 ]
	  #10: (0.2766019526,0.4778683178,0.3804945626,0.0874567135,0.0527966507,0.6832414389,0.2217341593,0.6872992696) [ #11 ]
	  #12: (0.6345026189,0.0329879546,0.4224220134,0.736733945,0.2257205198,0.9471432748,0.0564415637,0.0184599767) [ #13 ]
	Steiner points:
	  #3: (0.5609237393802519,0.323725692295001,0.39287214806978127,0.7910115332333378,0.35339581374937123,0.5749283333,0.3027468763,0.1342774458) [ #7 #5 #13 ]
	  #5: (0.5609237393802519,0.3237256899249356,0.39287214806978127,0.5335158885,0.35339581374937123,0.5749283333,0.3027468763,0.1342774458) [ #11 #3 #1 ]
	  #7: (0.5609237393802518,0.6367555897,0.39287214806978127,0.7910115332333378,0.4162855211,0.4549783,0.6728030828,0.14369337664733442) [ #3 #6 #9 ]
	  #9: (0.5848247928,0.6441561727,0.4240332141,0.9382223896,0.5912994475,0.4549783,0.9604098899,0.14369337664733442) [ #7 #8 #0 ]
	  #11: (0.2766019526,0.3237256899249356,0.3928721480697813,0.5335158885,0.22913482889999998,0.5749283332999999,0.2794510882,0.1810364263) [ #4 #5 #10 ]
	  #13: (0.6345026189,0.323725692295001,0.4224220134,0.7910115332333378,0.3533927412632848,0.9471432748,0.3027468763000001,0.0372633706) [ #2 #3 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.326392862
	Number of best updates: 8

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 8628

	Init times:
		Bottleneck Steiner distances: 0.000025015
		Smallest sphere distances: 0.00000563
		Sorting: 0.000025594
		Total init time: 0.000057426

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001862, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000011656, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000106877, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001207388, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.012747587, bsd pruned: 0, ss pruned: 2835

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 7560
	Total number iterations 15434 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0415343915343915
	Total time: 0.298616661
	Avarage time pr. topology: 0.000039499

Data for the geometric median iteration:
	Total time: 0.251720366
	Total steps: 290447
	Average number of steps pr. geometric median: 6.403152557319224
	Average time pr. step: 0.0000008666654019494089
	Average time pr. geometric median: 0.000005549390784832451

