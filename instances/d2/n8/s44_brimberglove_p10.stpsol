All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.5171970198011264
MST len: 2.5339234387
Steiner Ratio: 0.9933990038359427
Solution Steiner tree:
	Terminals:
	  #0: (0.7497371406,0.6546126439) [ #9 ]
	  #1: (0.5829005281,0.1765204129) [ #13 ]
	  #2: (0.3823489777,0.5831195775) [ #3 ]
	  #4: (0.4074837125,0.1925271313) [ #11 ]
	  #6: (0.1180090462,0.7377023845) [ #7 ]
	  #8: (0.8574096602,0.9521014504) [ #9 ]
	  #10: (0.0727788904,0.1874291036) [ #11 ]
	  #12: (0.7923043043,0.0619196091) [ #13 ]
	Steiner points:
	  #3: (0.4622503600011263,0.5831195775) [ #5 #7 #2 ]
	  #5: (0.4622503600011263,0.18742910359999998) [ #11 #3 #13 ]
	  #7: (0.4622503600011263,0.7377023740613508) [ #3 #6 #9 ]
	  #9: (0.7497371405999999,0.7377023740613508) [ #7 #8 #0 ]
	  #11: (0.4074837125,0.1874291036) [ #4 #5 #10 ]
	  #13: (0.5829005281,0.1765204129) [ #1 #5 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.054833983
	Number of best updates: 14

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 5253

	Init times:
		Bottleneck Steiner distances: 0.000013619
		Smallest sphere distances: 0.000002178
		Sorting: 0.000016133
		Total init time: 0.000033017

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001302, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000006014, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000054384, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000563595, bsd pruned: 105, ss pruned: 0
		Level 5 - Time: 0.004745976, bsd pruned: 4740, ss pruned: 210

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 4290
	Total number iterations 8645 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.015151515151515
	Total time: 0.045230178
	Avarage time pr. topology: 0.000010543

Data for the geometric median iteration:
	Total time: 0.034255617
	Total steps: 132315
	Average number of steps pr. geometric median: 5.14044289044289
	Average time pr. step: 0.00000025889443373767146
	Average time pr. geometric median: 0.0000013308320512820513

