All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.2652187148441834
MST len: 2.1926433392
Steiner Ratio: 1.0330994897102888
Solution Steiner tree:
	Terminals:
	  #0: (0.0851281491,0.8056346727) [ #13 ]
	  #1: (0.3098868776,0.9831179767) [ #3 ]
	  #2: (0.5296626759,0.9438327159) [ #5 ]
	  #4: (0.799727213,0.4030091993) [ #7 ]
	  #6: (0.6451566176,0.2473998085) [ #7 ]
	  #8: (0.4166978609,0.2226562315) [ #9 ]
	  #10: (0.87826215,0.7782872272) [ #11 ]
	  #12: (0.1354387687,0.9625167521) [ #13 ]
	Steiner points:
	  #3: (0.3098868776,0.9277305694558485) [ #1 #5 #13 ]
	  #5: (0.5296626759,0.9277305694558486) [ #3 #2 #11 ]
	  #7: (0.6451566176000315,0.4030091993) [ #4 #9 #6 ]
	  #9: (0.6451566142407449,0.4030091993) [ #8 #7 #11 ]
	  #11: (0.6451566142407449,0.7782872272) [ #5 #9 #10 ]
	  #13: (0.1354387687,0.9277305694558485) [ #0 #3 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.038125039
	Number of best updates: 11

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 3858

	Init times:
		Bottleneck Steiner distances: 0.000013806
		Smallest sphere distances: 0.000002151
		Sorting: 0.00001594
		Total init time: 0.000033172

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001451, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000005846, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000053027, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000609084, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004815459, bsd pruned: 1890, ss pruned: 5715

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 2790
	Total number iterations 5611 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.011111111111111
	Total time: 0.028907026
	Avarage time pr. topology: 0.00001036

Data for the geometric median iteration:
	Total time: 0.021760391
	Total steps: 83687
	Average number of steps pr. geometric median: 4.999223416965353
	Average time pr. step: 0.0000002600211621876755
	Average time pr. geometric median: 0.0000012999038829151733

