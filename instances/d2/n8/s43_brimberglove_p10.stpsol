All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.237504570593014
MST len: 2.5001318574
Steiner Ratio: 0.8949546256811816
Solution Steiner tree:
	Terminals:
	  #0: (0.0945956191,0.7204462847) [ #13 ]
	  #1: (0.6047278813,0.6099463206) [ #5 ]
	  #2: (0.4918181642,0.7429387396) [ #3 ]
	  #4: (0.3921490034,0.2415533509) [ #7 ]
	  #6: (0.7194525505,0.1857139022) [ #11 ]
	  #8: (0.8727000411,0.9242446869) [ #9 ]
	  #10: (0.497944257,0.1569527281) [ #11 ]
	  #12: (0.1386943274,0.9690520419) [ #13 ]
	Steiner points:
	  #3: (0.4918181641999999,0.7429387396) [ #5 #2 #9 ]
	  #5: (0.5012334253930134,0.6099463206) [ #7 #1 #3 ]
	  #7: (0.5012334253930134,0.2415533509) [ #4 #5 #11 ]
	  #9: (0.4918181642,0.8359360554922393) [ #3 #8 #13 ]
	  #11: (0.5012334253930136,0.1857139022) [ #6 #7 #10 ]
	  #13: (0.1386943274,0.8359360554922393) [ #0 #9 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.043238647
	Number of best updates: 14

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 4113

	Init times:
		Bottleneck Steiner distances: 0.000013433
		Smallest sphere distances: 0.000002294
		Sorting: 0.00000737
		Total init time: 0.00002423

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001177, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000006371, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000057858, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000646797, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.00504104, bsd pruned: 1890, ss pruned: 5460

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 3045
	Total number iterations 6159 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0226600985221674
	Total time: 0.033478231
	Avarage time pr. topology: 0.000010994

Data for the geometric median iteration:
	Total time: 0.025223564
	Total steps: 92312
	Average number of steps pr. geometric median: 5.052654625068418
	Average time pr. step: 0.00000027324252534881704
	Average time pr. geometric median: 0.0000013806001094690748

