All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.1625687159997125
MST len: 1.1954887977646664
Steiner Ratio: 0.9724630780091723
Solution Steiner tree:
	Terminals:
	  #0: (0.3235021184,0.6120159149) [ #11 ]
	  #1: (0.7084588719,0.4244470016) [ #5 ]
	  #2: (0.6334648415,0.41691941) [ #3 ]
	  #4: (0.5748512617,0.1829102199) [ #13 ]
	  #6: (0.5982263575,0.1752538104) [ #7 ]
	  #8: (0.6898134,0.18037494) [ #9 ]
	  #10: (0.4673976025,0.8627957766) [ #11 ]
	  #12: (0.4766825421,0.1399146664) [ #13 ]
	Steiner points:
	  #3: (0.6334648415,0.41691941) [ #5 #11 #2 ]
	  #5: (0.6334664209012169,0.41691804706288826) [ #9 #3 #1 ]
	  #7: (0.5982203981160552,0.17530007360675262) [ #9 #6 #13 ]
	  #9: (0.6434328147541913,0.2043211912537583) [ #5 #7 #8 ]
	  #11: (0.409363709114582,0.636057571245339) [ #10 #3 #0 ]
	  #13: (0.5748513725125942,0.18290796216604707) [ #4 #7 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.099756523
	Number of best updates: 16

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 5268

	Init times:
		Bottleneck Steiner distances: 0.00000974
		Smallest sphere distances: 0.000001001
		Sorting: 0.000010972
		Total init time: 0.000022831

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000748, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003387, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000030416, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000288991, bsd pruned: 0, ss pruned: 210
		Level 5 - Time: 0.002257753, bsd pruned: 735, ss pruned: 2940

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 4410
	Total number iterations 25326 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 5.742857142857143
	Total time: 0.094415462
	Avarage time pr. topology: 0.000021409

Data for the geometric median iteration:
	Total time: 0.084541331
	Total steps: 769133
	Average number of steps pr. geometric median: 29.067762660619803
	Average time pr. step: 0.00000010991770083977674
	Average time pr. geometric median: 0.00000319506164021164

