All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.46907410977792
MST len: 2.5862450070999996
Steiner Ratio: 0.9546945873262545
Solution Steiner tree:
	Terminals:
	  #0: (0.8132569412,0.7641613385) [ #3 ]
	  #1: (0.6620165676,0.9043156765) [ #3 ]
	  #2: (0.2804932088,0.3381360212) [ #5 ]
	  #4: (0.1078885724,0.4860870128) [ #11 ]
	  #6: (0.6258278161,0.215128048) [ #13 ]
	  #8: (0.8540148287,0.22754956) [ #9 ]
	  #10: (0.1227083798,0.9171544332) [ #11 ]
	  #12: (0.5717940003,0.0018524737) [ #13 ]
	Steiner points:
	  #3: (0.6620165676,0.7641613385) [ #1 #0 #7 ]
	  #5: (0.28049320880000006,0.33813712556874614) [ #7 #11 #2 ]
	  #7: (0.6258278160663737,0.33813712556874614) [ #5 #9 #3 ]
	  #9: (0.6258278160663737,0.22754956) [ #7 #8 #13 ]
	  #11: (0.12270837980000002,0.4860870128) [ #10 #5 #4 ]
	  #13: (0.6258245183220801,0.21512804799999996) [ #6 #9 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.042289137
	Number of best updates: 22

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 4218

	Init times:
		Bottleneck Steiner distances: 0.000013559
		Smallest sphere distances: 0.000002162
		Sorting: 0.000015562
		Total init time: 0.000032499

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001235, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000005956, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000053964, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000498947, bsd pruned: 105, ss pruned: 105
		Level 5 - Time: 0.003977151, bsd pruned: 1470, ss pruned: 3255

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 3360
	Total number iterations 6740 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.005952380952381
	Total time: 0.034227884
	Avarage time pr. topology: 0.000010186

Data for the geometric median iteration:
	Total time: 0.025724929
	Total steps: 98841
	Average number of steps pr. geometric median: 4.902827380952381
	Average time pr. step: 0.0000002602657702775164
	Average time pr. geometric median: 0.00000127603814484127

