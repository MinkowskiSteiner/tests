All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.0376149870000004
MST len: 2.2515002888
Steiner Ratio: 0.9050032092538635
Solution Steiner tree:
	Terminals:
	  #0: (0.7480876026,0.1786557348) [ #7 ]
	  #1: (0.1922058562,0.6652606896) [ #3 ]
	  #2: (0.4496756841,0.6990365287) [ #3 ]
	  #4: (0.6652369316,0.7215187111) [ #5 ]
	  #6: (0.3054385443,0.1172210561) [ #9 ]
	  #8: (0.2697567126,0.1478463012) [ #9 ]
	  #10: (0.5266172777,0.9129774263) [ #11 ]
	  #12: (0.8656997224,0.5431189162) [ #13 ]
	Steiner points:
	  #3: (0.44967568410000003,0.6652606896) [ #2 #1 #11 ]
	  #5: (0.6652369316,0.7215187110999112) [ #11 #4 #13 ]
	  #7: (0.7480876026000001,0.1786557348) [ #0 #9 #13 ]
	  #9: (0.3054385443,0.1478463012) [ #8 #7 #6 ]
	  #11: (0.5266172777,0.7215187110999112) [ #10 #5 #3 ]
	  #13: (0.7480876026,0.5431189162) [ #5 #7 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.038097608
	Number of best updates: 11

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 3738

	Init times:
		Bottleneck Steiner distances: 0.000022348
		Smallest sphere distances: 0.000002303
		Sorting: 0.000006997
		Total init time: 0.000033156

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001284, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000005898, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000053779, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000498739, bsd pruned: 0, ss pruned: 210
		Level 5 - Time: 0.004059474, bsd pruned: 735, ss pruned: 4470

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 2880
	Total number iterations 5803 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0149305555555554
	Total time: 0.03018228
	Avarage time pr. topology: 0.000010479

Data for the geometric median iteration:
	Total time: 0.022796879
	Total steps: 87465
	Average number of steps pr. geometric median: 5.061631944444445
	Average time pr. step: 0.00000026064001600640255
	Average time pr. geometric median: 0.0000013192638310185185

