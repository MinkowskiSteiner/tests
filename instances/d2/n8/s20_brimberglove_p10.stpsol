All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.0915896772
MST len: 2.3122289722000002
Steiner Ratio: 0.904577229308709
Solution Steiner tree:
	Terminals:
	  #0: (0.5785871486,0.6617850073) [ #11 ]
	  #1: (0.6551358368,0.3903584468) [ #5 ]
	  #2: (0.5311698176,0.3798184727) [ #7 ]
	  #4: (0.6938135543,0.3702549456) [ #5 ]
	  #6: (0.153481192,0.2006002349) [ #9 ]
	  #8: (0.7874475535,0.1478928184) [ #9 ]
	  #10: (0.4885317048,0.9929940151) [ #13 ]
	  #12: (0.1488122675,0.7401193766) [ #13 ]
	Steiner points:
	  #3: (0.548568661271747,0.3903584468) [ #11 #5 #7 ]
	  #5: (0.6551358367999999,0.3903584468) [ #4 #1 #3 ]
	  #7: (0.5311698176,0.3798184727) [ #3 #2 #9 ]
	  #9: (0.5311698176,0.2006002349) [ #6 #8 #7 ]
	  #11: (0.548568661271747,0.6617850073) [ #0 #3 #13 ]
	  #13: (0.4885317048,0.7401193766) [ #10 #11 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.086966189
	Number of best updates: 12

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 7683

	Init times:
		Bottleneck Steiner distances: 0.000013375
		Smallest sphere distances: 0.000002229
		Sorting: 0.000007145
		Total init time: 0.000024144

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001352, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000005914, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00005526, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000619639, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.005861649, bsd pruned: 945, ss pruned: 2835

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 6615
	Total number iterations 13479 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.037641723356009
	Total time: 0.074712274
	Avarage time pr. topology: 0.000011294

Data for the geometric median iteration:
	Total time: 0.05748084
	Total steps: 219736
	Average number of steps pr. geometric median: 5.5363063744016126
	Average time pr. step: 0.00000026159045399934467
	Average time pr. geometric median: 0.0000014482448979591838

