All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.8034521974388367
MST len: 1.8678264873404946
Steiner Ratio: 0.9655351873752913
Solution Steiner tree:
	Terminals:
	  #0: (0.7011187615,0.5075186931) [ #5 ]
	  #1: (0.7428126711,0.9414201057) [ #3 ]
	  #2: (0.3255712615,0.9922046186) [ #9 ]
	  #4: (0.939356192,0.805739215) [ #7 ]
	  #6: (0.9913254278,0.7521004219) [ #11 ]
	  #8: (0.1036064635,0.5821549858) [ #13 ]
	  #10: (0.9965749998,0.8456901428) [ #11 ]
	  #12: (0.042332178,0.9785602656) [ #13 ]
	Steiner points:
	  #3: (0.7428126710999992,0.9414201056999986) [ #1 #9 #5 ]
	  #5: (0.8406824402362805,0.7982278224419028) [ #0 #3 #7 ]
	  #7: (0.939356192,0.805739215) [ #11 #4 #5 ]
	  #9: (0.32557183129352923,0.9922008133806675) [ #3 #2 #13 ]
	  #11: (0.9671844083578846,0.8034024639754773) [ #10 #7 #6 ]
	  #13: (0.13814280617268251,0.9088684192173033) [ #8 #9 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.070996469
	Number of best updates: 12

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 2958

	Init times:
		Bottleneck Steiner distances: 0.000010072
		Smallest sphere distances: 0.000000931
		Sorting: 0.000002281
		Total init time: 0.00001432

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000906, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003477, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000029373, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000307335, bsd pruned: 0, ss pruned: 105
		Level 5 - Time: 0.002192453, bsd pruned: 2520, ss pruned: 4725

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 1995
	Total number iterations 16149 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 8.094736842105263
	Total time: 0.066000012
	Avarage time pr. topology: 0.000033082

Data for the geometric median iteration:
	Total time: 0.059924955
	Total steps: 548197
	Average number of steps pr. geometric median: 45.79757727652464
	Average time pr. step: 0.00000010931281090556862
	Average time pr. geometric median: 0.000005006261904761904

