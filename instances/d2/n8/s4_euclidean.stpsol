All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.5253094122602482
MST len: 1.5356599839461522
Steiner Ratio: 0.9932598545289262
Solution Steiner tree:
	Terminals:
	  #0: (0.9271462466,0.29734262) [ #13 ]
	  #1: (0.4198029756,0.654078258) [ #9 ]
	  #2: (0.5068725648,0.1146662767) [ #3 ]
	  #4: (0.2142811954,0.3601613163) [ #5 ]
	  #6: (0.1943656375,0.3097259371) [ #7 ]
	  #8: (0.5706952487,0.6701787769) [ #9 ]
	  #10: (0.1912108968,0.2600808285) [ #11 ]
	  #12: (0.9164578756,0.1339819669) [ #13 ]
	Steiner points:
	  #3: (0.5068733210988323,0.11467149539454415) [ #2 #11 #13 ]
	  #5: (0.21428143572191374,0.3601611871469598) [ #9 #7 #4 ]
	  #7: (0.1943688257760076,0.3097254517606168) [ #6 #5 #11 ]
	  #9: (0.4198149699050071,0.6540579496494952) [ #1 #8 #5 ]
	  #11: (0.19770994288009122,0.26449580164104386) [ #3 #7 #10 ]
	  #13: (0.8816099056003276,0.17646734388698823) [ #0 #3 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 2.876613167
	Number of best updates: 13

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 2958

	Init times:
		Bottleneck Steiner distances: 0.000249935
		Smallest sphere distances: 0.00001933
		Sorting: 0.000079491
		Total init time: 0.000358277

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000009479, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000066879, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000636937, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.006813901, bsd pruned: 0, ss pruned: 105
		Level 5 - Time: 0.047027088, bsd pruned: 0, ss pruned: 7245

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 1995
	Total number iterations 14599 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 7.317794486215539
	Total time: 2.746647057
	Avarage time pr. topology: 0.001376765

Data for the geometric median iteration:
	Total time: 2.609117778
	Total steps: 590891
	Average number of steps pr. geometric median: 49.364327485380116
	Average time pr. step: 0.000004415565270075191
	Average time pr. geometric median: 0.00021797141002506264

