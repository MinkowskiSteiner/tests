All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.5776798743023797
MST len: 1.6715555136963915
Steiner Ratio: 0.9438393528513928
Solution Steiner tree:
	Terminals:
	  #0: (0.2520150534,0.5079065824) [ #11 ]
	  #1: (0.237839454,0.4875804249) [ #3 ]
	  #2: (0.2945063893,0.4759735886) [ #5 ]
	  #4: (0.6125465755,0.2276748625) [ #7 ]
	  #6: (0.7728105293,0.3646502115) [ #9 ]
	  #8: (0.680792569,0.0897512576) [ #9 ]
	  #10: (0.3625599515,0.8518746006) [ #11 ]
	  #12: (0.1689700215,0.0285875835) [ #13 ]
	Steiner points:
	  #3: (0.25259639294410363,0.49719900379106396) [ #1 #11 #5 ]
	  #5: (0.2945056846105545,0.47597296191673977) [ #3 #2 #13 ]
	  #7: (0.612546575430749,0.22767486215443122) [ #9 #4 #13 ]
	  #9: (0.6400989311668204,0.22140538168305243) [ #8 #7 #6 ]
	  #11: (0.25201505342392905,0.5079065823969116) [ #10 #3 #0 ]
	  #13: (0.370205273462386,0.26972851893292465) [ #5 #7 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.179801286
	Number of best updates: 20

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 9153

	Init times:
		Bottleneck Steiner distances: 0.000009011
		Smallest sphere distances: 0.000001114
		Sorting: 0.000002152
		Total init time: 0.000013326

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000729, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003357, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000029796, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000337604, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.003729361, bsd pruned: 0, ss pruned: 2310

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 8085
	Total number iterations 47440 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 5.867656153370439
	Total time: 0.171714429
	Avarage time pr. topology: 0.000021238

Data for the geometric median iteration:
	Total time: 0.153393186
	Total steps: 1402163
	Average number of steps pr. geometric median: 28.904617604617606
	Average time pr. step: 0.00000010939754222583251
	Average time pr. geometric median: 0.0000031620941249226963

