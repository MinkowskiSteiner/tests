All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.6438860695697892
MST len: 1.6602411662259458
Steiner Ratio: 0.990148963301919
Solution Steiner tree:
	Terminals:
	  #0: (0.3832746434,0.7694257664) [ #5 ]
	  #1: (0.6123015828,0.975351023) [ #7 ]
	  #2: (0.4010537497,0.5862380008) [ #9 ]
	  #4: (0.3322590768,0.7771537661) [ #5 ]
	  #6: (0.2816415891,0.9608924622) [ #11 ]
	  #8: (0.6286001725,0.3799427857) [ #13 ]
	  #10: (0.0521304696,0.9748592134) [ #11 ]
	  #12: (0.9571484462,0.5881108905) [ #13 ]
	Steiner points:
	  #3: (0.3832748715750435,0.7694257654431947) [ #5 #9 #7 ]
	  #5: (0.3832746434,0.7694257664) [ #0 #4 #3 ]
	  #7: (0.39479603049172685,0.8786137955412446) [ #11 #1 #3 ]
	  #9: (0.40105482352142835,0.5862386395402563) [ #3 #2 #13 ]
	  #11: (0.2816415891,0.9608924622) [ #10 #7 #6 ]
	  #13: (0.6321547436329161,0.4300426506721382) [ #8 #9 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.186242592
	Number of best updates: 9

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 7683

	Init times:
		Bottleneck Steiner distances: 0.000009153
		Smallest sphere distances: 0.000001078
		Sorting: 0.000011008
		Total init time: 0.00002235

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000767, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003386, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000030407, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000340559, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.003266629, bsd pruned: 945, ss pruned: 2835

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 6615
	Total number iterations 58225 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 8.80196523053666
	Total time: 0.178971396
	Avarage time pr. topology: 0.000027055

Data for the geometric median iteration:
	Total time: 0.157144666
	Total steps: 1408113
	Average number of steps pr. geometric median: 35.477777777777774
	Average time pr. step: 0.00000011159947106517729
	Average time pr. geometric median: 0.0000039593012345679

