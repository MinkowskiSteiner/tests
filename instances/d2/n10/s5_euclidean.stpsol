All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.0492569328494965
MST len: 2.054180272030831
Steiner Ratio: 0.997603258463549
Solution Steiner tree:
	Terminals:
	  #0: (0.3634193076,0.2718471113) [ #7 ]
	  #1: (0.4164950971,0.2561370005) [ #3 ]
	  #2: (0.4159667661,0.2562429226) [ #11 ]
	  #4: (0.2805664764,0.4484554052) [ #15 ]
	  #6: (0.2747455962,0.0464677648) [ #9 ]
	  #8: (0.1476887968,0.0796446726) [ #13 ]
	  #10: (0.9714187407,0.2212781451) [ #17 ]
	  #12: (0.0156746367,0.2746502162) [ #13 ]
	  #14: (0.4982478328,0.8940018629) [ #15 ]
	  #16: (0.9927552245,0.080030445) [ #17 ]
	Steiner points:
	  #3: (0.41650058445628024,0.25613744387504633) [ #1 #11 #5 ]
	  #5: (0.36343276823789716,0.2718626954243747) [ #15 #3 #7 ]
	  #7: (0.36341932110679376,0.27184710315788635) [ #5 #0 #9 ]
	  #9: (0.2747364606728616,0.04648573641878219) [ #6 #13 #7 ]
	  #11: (0.41650058445628046,0.2561374438750478) [ #2 #3 #17 ]
	  #13: (0.14768879680908087,0.07964467261284186) [ #9 #12 #8 ]
	  #15: (0.28059389807079255,0.4484551884947179) [ #14 #5 #4 ]
	  #17: (0.9469328842036999,0.1890220716319627) [ #10 #11 #16 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 14.018901776
	Number of best updates: 15

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 728718

	Init times:
		Bottleneck Steiner distances: 0.000023285
		Smallest sphere distances: 0.000001285
		Sorting: 0.000003632
		Total init time: 0.0000293

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000735, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003593, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031944, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000355142, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004580154, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.070175343, bsd pruned: 0, ss pruned: 0
		Level 7 - Time: 0.64848533, bsd pruned: 0, ss pruned: 1444905

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 582120
	Total number iterations 3520115 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 6.047060743489315
	Total time: 12.73705478
	Avarage time pr. topology: 0.00002188

Data for the geometric median iteration:
	Total time: 10.966148953
	Total steps: 98259398
	Average number of steps pr. geometric median: 21.099472187865047
	Average time pr. step: 0.00000011160407224355271
	Average time pr. geometric median: 0.000002354787018355322

