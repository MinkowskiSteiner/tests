All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.639249751875582
MST len: 2.7151950971000005
Steiner Ratio: 0.972029507085685
Solution Steiner tree:
	Terminals:
	  #0: (0.5658107323,0.6109299178) [ #7 ]
	  #1: (0.8166862739,0.1834716542) [ #13 ]
	  #2: (0.584652962,0.4221560766) [ #3 ]
	  #4: (0.5057680786,0.1796468744) [ #5 ]
	  #6: (0.8809632025,0.5625731631) [ #15 ]
	  #8: (0.2689884604,0.0923382296) [ #11 ]
	  #10: (0.0612761933,0.083659018) [ #17 ]
	  #12: (0.8738890034,0.0530768098) [ #13 ]
	  #14: (0.9767909204,0.9780582851) [ #15 ]
	  #16: (0.0253341845,0.3162595943) [ #17 ]
	Steiner points:
	  #3: (0.5846531338312553,0.42215607660000004) [ #2 #9 #7 ]
	  #5: (0.5846531338312554,0.18340416131307102) [ #13 #4 #9 ]
	  #7: (0.5846531338312553,0.6109299178) [ #0 #15 #3 ]
	  #9: (0.5846531338312554,0.18340416131307102) [ #3 #11 #5 ]
	  #11: (0.2689884604,0.18340415948682437) [ #9 #8 #17 ]
	  #13: (0.8166862739,0.18340416131307105) [ #5 #12 #1 ]
	  #15: (0.8809632025,0.6109299178) [ #14 #7 #6 ]
	  #17: (0.061276193300000004,0.18340415948682437) [ #10 #11 #16 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 3.312300344
	Number of best updates: 22

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 264507

	Init times:
		Bottleneck Steiner distances: 0.000029
		Smallest sphere distances: 0.000003659
		Sorting: 0.000013704
		Total init time: 0.000047603

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001303, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000005821, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000054407, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000614536, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.008132992, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.107430283, bsd pruned: 20790, ss pruned: 19845
		Level 7 - Time: 0.661781344, bsd pruned: 797183, ss pruned: 461773

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 158544
	Total number iterations 322852 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0363558381269553
	Total time: 2.134552281
	Avarage time pr. topology: 0.000013462

Data for the geometric median iteration:
	Total time: 1.6055693340000001
	Total steps: 6239653
	Average number of steps pr. geometric median: 4.919496322787365
	Average time pr. step: 0.00000025731708702390983
	Average time pr. geometric median: 0.0000012658704634044808

