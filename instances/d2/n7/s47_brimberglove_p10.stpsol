All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.292543727335711
MST len: 2.3810399558
Steiner Ratio: 0.962832951102429
Solution Steiner tree:
	Terminals:
	  #0: (0.8249852354,0.3957488953) [ #9 ]
	  #1: (0.3771066863,0.354725022) [ #3 ]
	  #2: (0.4132981074,0.5756729662) [ #5 ]
	  #4: (0.8256802293,0.8959328252) [ #7 ]
	  #6: (0.3297338986,0.9487606892) [ #7 ]
	  #8: (0.9420667789,0.3395639064) [ #9 ]
	  #10: (0.2042499758,0.0233809524) [ #11 ]
	Steiner points:
	  #3: (0.37710668629999994,0.3957463234660719) [ #9 #1 #11 ]
	  #5: (0.3771066868497448,0.5756729662) [ #2 #7 #11 ]
	  #7: (0.3771066868497448,0.8959328252) [ #6 #5 #4 ]
	  #9: (0.8249852354,0.39574683146674416) [ #8 #3 #0 ]
	  #11: (0.37710404903103334,0.3957463234660719) [ #3 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.005230908
	Number of best updates: 7

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 558

	Init times:
		Bottleneck Steiner distances: 0.00001037
		Smallest sphere distances: 0.000001849
		Sorting: 0.000017387
		Total init time: 0.000030925

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001243, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000005804, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000049129, bsd pruned: 0, ss pruned: 15
		Level 4 - Time: 0.000343358, bsd pruned: 270, ss pruned: 90

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 450
	Total number iterations 904 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.008888888888889
	Total time: 0.004371675
	Avarage time pr. topology: 0.000009714

Data for the geometric median iteration:
	Total time: 0.003361618
	Total steps: 12446
	Average number of steps pr. geometric median: 5.531555555555555
	Average time pr. step: 0.0000002700962558251647
	Average time pr. geometric median: 0.0000014940524444444444

