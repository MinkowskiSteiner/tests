All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.986320403036308
MST len: 2.366949531
Steiner Ratio: 0.8391900110337874
Solution Steiner tree:
	Terminals:
	  #0: (0.3921490034,0.2415533509) [ #5 ]
	  #1: (0.6047278813,0.6099463206) [ #3 ]
	  #2: (0.4918181642,0.7429387396) [ #7 ]
	  #4: (0.7194525505,0.1857139022) [ #9 ]
	  #6: (0.8727000411,0.9242446869) [ #11 ]
	  #8: (0.497944257,0.1569527281) [ #9 ]
	  #10: (0.1386943274,0.9690520419) [ #11 ]
	Steiner points:
	  #3: (0.49794425690759925,0.6099463206) [ #7 #1 #5 ]
	  #5: (0.49794214557129163,0.2415533509) [ #9 #0 #3 ]
	  #7: (0.49794425690759925,0.7429387396) [ #3 #2 #11 ]
	  #9: (0.497944257,0.1857139022) [ #8 #5 #4 ]
	  #11: (0.49794425690759936,0.9242446869) [ #6 #7 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.007078309
	Number of best updates: 8

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 753

	Init times:
		Bottleneck Steiner distances: 0.000011088
		Smallest sphere distances: 0.000001834
		Sorting: 0.000004755
		Total init time: 0.000019239

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001214, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000005808, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000056038, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000455273, bsd pruned: 210, ss pruned: 105

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 630
	Total number iterations 1271 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0174603174603174
	Total time: 0.006011081
	Avarage time pr. topology: 0.000009541

Data for the geometric median iteration:
	Total time: 0.004610981
	Total steps: 17203
	Average number of steps pr. geometric median: 5.461269841269841
	Average time pr. step: 0.00000026803354066151254
	Average time pr. geometric median: 0.0000014638034920634921

