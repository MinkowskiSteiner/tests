All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.8137427937671589
MST len: 2.1032338141
Steiner Ratio: 0.8623590879948275
Solution Steiner tree:
	Terminals:
	  #0: (0.6606961906,0.4311483691) [ #9 ]
	  #1: (0.5244897048,0.4379004908) [ #3 ]
	  #2: (0.0703035561,0.1812310276) [ #5 ]
	  #4: (0.1675375119,0.7022328906) [ #7 ]
	  #6: (0.0071734451,0.7727813925) [ #11 ]
	  #8: (0.7982122655,0.1291394788) [ #9 ]
	  #10: (0.1086744881,0.7979400231) [ #11 ]
	Steiner points:
	  #3: (0.5244897048,0.43790049583356694) [ #9 #1 #5 ]
	  #5: (0.16753751190001245,0.43790049583356694) [ #2 #7 #3 ]
	  #7: (0.16753751190001245,0.7022328906) [ #4 #5 #11 ]
	  #9: (0.6606961906,0.43114836909999993) [ #8 #3 #0 ]
	  #11: (0.1086744881,0.7727813925) [ #6 #7 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.003406948
	Number of best updates: 13

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 387

	Init times:
		Bottleneck Steiner distances: 0.000011296
		Smallest sphere distances: 0.000001724
		Sorting: 0.000017289
		Total init time: 0.000031562

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001321, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000005654, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00004785, bsd pruned: 0, ss pruned: 15
		Level 4 - Time: 0.00033591, bsd pruned: 90, ss pruned: 441

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 279
	Total number iterations 564 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.021505376344086
	Total time: 0.002628407
	Avarage time pr. topology: 0.00000942

Data for the geometric median iteration:
	Total time: 0.002004832
	Total steps: 7523
	Average number of steps pr. geometric median: 5.392831541218638
	Average time pr. step: 0.0000002664936860295095
	Average time pr. geometric median: 0.0000014371555555555556

