All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.4770980986698927
MST len: 1.5157181692543613
Steiner Ratio: 0.974520282617271
Solution Steiner tree:
	Terminals:
	  #0: (0.2219586108,0.6696780285) [ #7 ]
	  #1: (0.2902333854,0.3277869785) [ #5 ]
	  #2: (0.3323929949,0.5761897869) [ #3 ]
	  #4: (0.273373468,0.0765914861) [ #11 ]
	  #6: (0.586565987,0.9423629041) [ #7 ]
	  #8: (0.6450726547,0.0391567499) [ #9 ]
	  #10: (0.2285638117,0.0594177293) [ #11 ]
	Steiner points:
	  #3: (0.33239292662592845,0.5761897826501348) [ #7 #5 #2 ]
	  #5: (0.29023388949387097,0.32778699330967564) [ #9 #3 #1 ]
	  #7: (0.3105201504038789,0.6488348860662859) [ #3 #6 #0 ]
	  #9: (0.3349594124383776,0.13401498660702418) [ #8 #5 #11 ]
	  #11: (0.2733730927889208,0.07659208694087721) [ #4 #9 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.17752857
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 339

	Init times:
		Bottleneck Steiner distances: 0.000181005
		Smallest sphere distances: 0.000015256
		Sorting: 0.000064476
		Total init time: 0.000270298

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000009734, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000068724, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000654724, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.003843262, bsd pruned: 315, ss pruned: 414

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 216
	Total number iterations 1483 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 6.8657407407407405
	Total time: 0.164987561
	Avarage time pr. topology: 0.000763831

Data for the geometric median iteration:
	Total time: 0.152932258
	Total steps: 34228
	Average number of steps pr. geometric median: 31.692592592592593
	Average time pr. step: 0.000004468045401425733
	Average time pr. geometric median: 0.0001416039425925926

