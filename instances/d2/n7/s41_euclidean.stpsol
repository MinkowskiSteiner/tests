All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.4457522937543716
MST len: 1.4819879134431198
Steiner Ratio: 0.9755493149707534
Solution Steiner tree:
	Terminals:
	  #0: (0.7685739201,0.5612500564) [ #5 ]
	  #1: (0.38213495,0.5965716008) [ #3 ]
	  #2: (0.4878506383,0.4318964698) [ #9 ]
	  #4: (0.8467889367,0.5935844293) [ #7 ]
	  #6: (0.9065222973,0.5735588286) [ #7 ]
	  #8: (0.2660512897,0.022327458) [ #9 ]
	  #10: (0.6702460561,0.9116797088) [ #11 ]
	Steiner points:
	  #3: (0.49006700731751923,0.5310009644998686) [ #1 #9 #11 ]
	  #5: (0.7685739201,0.5612500564000006) [ #7 #0 #11 ]
	  #7: (0.8467890940065299,0.593579842095574) [ #6 #5 #4 ]
	  #9: (0.4878506377800095,0.4318964699379634) [ #8 #3 #2 ]
	  #11: (0.6632876958946057,0.625491214872122) [ #3 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.46272414
	Number of best updates: 13

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 963

	Init times:
		Bottleneck Steiner distances: 0.00017338
		Smallest sphere distances: 0.000014836
		Sorting: 0.000052309
		Total init time: 0.000249334

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000009631, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000067449, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000641338, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.006769444, bsd pruned: 105, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 840
	Total number iterations 3553 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 4.229761904761904
	Total time: 0.442944491
	Avarage time pr. topology: 0.000527314

Data for the geometric median iteration:
	Total time: 0.412299132
	Total steps: 93822
	Average number of steps pr. geometric median: 22.338571428571427
	Average time pr. step: 0.00000439448244548187
	Average time pr. geometric median: 0.00009816646

