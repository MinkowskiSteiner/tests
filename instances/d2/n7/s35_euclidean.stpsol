All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.5450735953966213
MST len: 1.6510125203300259
Steiner Ratio: 0.935833966351613
Solution Steiner tree:
	Terminals:
	  #0: (0.1675375119,0.7022328906) [ #5 ]
	  #1: (0.6606961906,0.4311483691) [ #9 ]
	  #2: (0.5244897048,0.4379004908) [ #3 ]
	  #4: (0.1086744881,0.7979400231) [ #7 ]
	  #6: (0.0071734451,0.7727813925) [ #7 ]
	  #8: (0.7982122655,0.1291394788) [ #9 ]
	  #10: (0.0703035561,0.1812310276) [ #11 ]
	Steiner points:
	  #3: (0.5244897048005854,0.437900490806264) [ #9 #2 #11 ]
	  #5: (0.1675364437245825,0.702232252705136) [ #7 #0 #11 ]
	  #7: (0.10352551438161323,0.785488751853483) [ #5 #6 #4 ]
	  #9: (0.6576093884083152,0.4267003860649037) [ #8 #3 #1 ]
	  #11: (0.27444573330026967,0.46359459406908643) [ #3 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.377032884
	Number of best updates: 8

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 744

	Init times:
		Bottleneck Steiner distances: 0.000159399
		Smallest sphere distances: 0.000014984
		Sorting: 0.000052112
		Total init time: 0.000236141

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000957, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000068074, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000503014, bsd pruned: 0, ss pruned: 30
		Level 4 - Time: 0.005325123, bsd pruned: 0, ss pruned: 24

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 651
	Total number iterations 2502 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 3.8433179723502304
	Total time: 0.361678751
	Avarage time pr. topology: 0.000555574

Data for the geometric median iteration:
	Total time: 0.339673455
	Total steps: 76916
	Average number of steps pr. geometric median: 23.63010752688172
	Average time pr. step: 0.000004416161201830569
	Average time pr. geometric median: 0.00010435436405529954

