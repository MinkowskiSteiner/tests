All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.7887679721413507
MST len: 1.9337053831000002
Steiner Ratio: 0.9250467976014555
Solution Steiner tree:
	Terminals:
	  #0: (0.2624237846,0.4864085733) [ #9 ]
	  #1: (0.433104444,0.3224785879) [ #7 ]
	  #2: (0.4428926201,0.5949672445) [ #3 ]
	  #4: (0.5921190123,0.7871168474) [ #11 ]
	  #6: (0.4051467089,0.068010787) [ #7 ]
	  #8: (0.1665347033,0.6197118892) [ #9 ]
	  #10: (0.8626960571,0.4022940902) [ #11 ]
	Steiner points:
	  #3: (0.44288349182850645,0.5152697939081091) [ #5 #9 #2 ]
	  #5: (0.44288348838944713,0.5152697939081091) [ #3 #7 #11 ]
	  #7: (0.44288261710229115,0.3224785879) [ #6 #5 #1 ]
	  #9: (0.2624237846,0.5152697939081091) [ #8 #3 #0 ]
	  #11: (0.5921190123,0.5152697939081091) [ #4 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.008069996
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 858

	Init times:
		Bottleneck Steiner distances: 0.000010583
		Smallest sphere distances: 0.000001771
		Sorting: 0.000016967
		Total init time: 0.000030548

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001201, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000005772, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000054248, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.00050846, bsd pruned: 0, ss pruned: 210

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 735
	Total number iterations 1482 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.016326530612245
	Total time: 0.006907764
	Avarage time pr. topology: 0.000009398

Data for the geometric median iteration:
	Total time: 0.00522641
	Total steps: 19973
	Average number of steps pr. geometric median: 5.434829931972789
	Average time pr. step: 0.0000002616737595754268
	Average time pr. geometric median: 0.000001422152380952381

