All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.7983588483000001
MST len: 1.9628535824999997
Steiner Ratio: 0.9161961260551641
Solution Steiner tree:
	Terminals:
	  #0: (0.3336426291,0.698289798) [ #5 ]
	  #1: (0.5058995711,0.1543681692) [ #9 ]
	  #2: (0.1642170884,0.3632087118) [ #3 ]
	  #4: (0.0904165954,0.6112220658) [ #11 ]
	  #6: (0.3869168281,0.0459364807) [ #7 ]
	  #8: (0.8844101391,0.2439990897) [ #9 ]
	  #10: (0.052807556,0.6665685525) [ #11 ]
	Steiner points:
	  #3: (0.21677224408548773,0.3632087118) [ #7 #5 #2 ]
	  #5: (0.21677224408548773,0.6281991162336662) [ #3 #0 #11 ]
	  #7: (0.3869168281,0.18424514269999998) [ #3 #6 #9 ]
	  #9: (0.5058995711,0.18424514269999998) [ #8 #7 #1 ]
	  #11: (0.09041659540000001,0.6281991162336662) [ #4 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.002156648
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 252

	Init times:
		Bottleneck Steiner distances: 0.00001066
		Smallest sphere distances: 0.000001729
		Sorting: 0.000004544
		Total init time: 0.000018354

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001437, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000006044, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000049028, bsd pruned: 0, ss pruned: 15
		Level 4 - Time: 0.000293263, bsd pruned: 90, ss pruned: 576

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 144
	Total number iterations 290 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.013888888888889
	Total time: 0.001493306
	Avarage time pr. topology: 0.00001037

Data for the geometric median iteration:
	Total time: 0.001141718
	Total steps: 4132
	Average number of steps pr. geometric median: 5.738888888888889
	Average time pr. step: 0.000000276311229428848
	Average time pr. geometric median: 0.0000015857194444444443

