All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.9114504890445096
MST len: 2.1616398464
Steiner Ratio: 0.8842594626611107
Solution Steiner tree:
	Terminals:
	  #0: (0.6797411119,0.8863195306) [ #5 ]
	  #1: (0.9238626137,0.2359872904) [ #9 ]
	  #2: (0.4652112594,0.4872422654) [ #7 ]
	  #4: (0.9290727782,0.9129959489) [ #11 ]
	  #6: (0.1321992586,0.3741906497) [ #7 ]
	  #8: (0.9674219824,0.2784534135) [ #9 ]
	  #10: (0.785772445,0.946517847) [ #11 ]
	Steiner points:
	  #3: (0.7890863716443839,0.4872422654) [ #7 #5 #9 ]
	  #5: (0.7890863716443839,0.8863195305999999) [ #3 #0 #11 ]
	  #7: (0.4652112594,0.4872422654) [ #3 #6 #2 ]
	  #9: (0.9238626137,0.2784534135) [ #8 #3 #1 ]
	  #11: (0.7890863716445097,0.9129959489) [ #4 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.005849638
	Number of best updates: 8

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 633

	Init times:
		Bottleneck Steiner distances: 0.000010697
		Smallest sphere distances: 0.000001729
		Sorting: 0.000017265
		Total init time: 0.000031075

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001245, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000006301, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000057855, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000434608, bsd pruned: 0, ss pruned: 435

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 510
	Total number iterations 1021 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0019607843137255
	Total time: 0.004813929
	Avarage time pr. topology: 0.000009439

Data for the geometric median iteration:
	Total time: 0.003618234
	Total steps: 12987
	Average number of steps pr. geometric median: 5.092941176470588
	Average time pr. step: 0.00000027860429660429664
	Average time pr. geometric median: 0.0000014189152941176472

