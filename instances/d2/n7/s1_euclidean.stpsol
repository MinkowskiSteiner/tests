All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.329705830551866
MST len: 1.423431978975737
Steiner Ratio: 0.9341548104804321
Solution Steiner tree:
	Terminals:
	  #0: (0.3647844728,0.5134009102) [ #7 ]
	  #1: (0.8401877172,0.3943829268) [ #9 ]
	  #2: (0.4773970519,0.6288709248) [ #3 ]
	  #4: (0.3352227557,0.7682295948) [ #5 ]
	  #6: (0.2777747108,0.5539699558) [ #7 ]
	  #8: (0.9116473579,0.1975513693) [ #9 ]
	  #10: (0.7830992238,0.7984400335) [ #11 ]
	Steiner points:
	  #3: (0.4773970519000031,0.6288709248001823) [ #2 #5 #11 ]
	  #5: (0.4025140642418124,0.6334223586471197) [ #4 #3 #7 ]
	  #7: (0.3468227572492052,0.5496037761225713) [ #6 #0 #5 ]
	  #9: (0.8401877171980617,0.39438292679906767) [ #1 #8 #11 ]
	  #11: (0.6936536689019067,0.6342375373196826) [ #3 #9 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.531838641
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 648

	Init times:
		Bottleneck Steiner distances: 0.000189368
		Smallest sphere distances: 0.000016058
		Sorting: 0.000047407
		Total init time: 0.000263177

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000010517, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000081221, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000596715, bsd pruned: 0, ss pruned: 15
		Level 4 - Time: 0.004752736, bsd pruned: 0, ss pruned: 270

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 540
	Total number iterations 3805 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 7.046296296296297
	Total time: 0.516842267
	Avarage time pr. topology: 0.000957115

Data for the geometric median iteration:
	Total time: 0.486216419
	Total steps: 109926
	Average number of steps pr. geometric median: 40.71333333333333
	Average time pr. step: 0.000004423124820333679
	Average time pr. geometric median: 0.00018008015518518517

