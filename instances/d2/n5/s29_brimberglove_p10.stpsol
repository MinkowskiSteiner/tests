All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.2456126336
MST len: 1.4079109675
Steiner Ratio: 0.884724007663503
Solution Steiner tree:
	Terminals:
	  #0: (0.8309675659,0.9260082347) [ #3 ]
	  #1: (0.5662877856,0.3799464867) [ #5 ]
	  #2: (0.8759893737,0.4619202984) [ #3 ]
	  #4: (0.4450082292,0.5312397017) [ #5 ]
	  #6: (0.7565934233,0.2626699606) [ #7 ]
	Steiner points:
	  #3: (0.8309675659,0.4619202984) [ #2 #0 #7 ]
	  #5: (0.5662877856,0.395503519133398) [ #1 #4 #7 ]
	  #7: (0.7565934233,0.395503519133398) [ #3 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.002107855
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000113803
		Smallest sphere distances: 0.000009053
		Sorting: 0.000015732
		Total init time: 0.000148431

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000011619, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000077024, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.001611067
	Avarage time pr. topology: 0.000107404

Data for the geometric median iteration:
	Total time: 0.001326522
	Total steps: 255
	Average number of steps pr. geometric median: 5.666666666666667
	Average time pr. step: 0.000005202047058823529
	Average time pr. geometric median: 0.00002947826666666667

