All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 0.8571270593130337
MST len: 0.8868734276606003
Steiner Ratio: 0.9664592855983613
Solution Steiner tree:
	Terminals:
	  #0: (0.5785871486,0.6617850073) [ #7 ]
	  #1: (0.6551358368,0.3903584468) [ #5 ]
	  #2: (0.5311698176,0.3798184727) [ #3 ]
	  #4: (0.6938135543,0.3702549456) [ #5 ]
	  #6: (0.1488122675,0.7401193766) [ #7 ]
	Steiner points:
	  #3: (0.5721190542577388,0.41697517118116384) [ #5 #2 #7 ]
	  #5: (0.6551358342908821,0.3903584407782795) [ #1 #3 #4 ]
	  #7: (0.5292049014080059,0.6171733021041453) [ #0 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.014126826
	Number of best updates: 2

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 12

	Init times:
		Bottleneck Steiner distances: 0.000131693
		Smallest sphere distances: 0.000020351
		Sorting: 0.000016112
		Total init time: 0.000178153

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000010419, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.0000724, bsd pruned: 0, ss pruned: 6

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 9
	Total number iterations 100 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 11.11111111111111
	Total time: 0.01364885
	Avarage time pr. topology: 0.001516538

Data for the geometric median iteration:
	Total time: 0.013123268
	Total steps: 2866
	Average number of steps pr. geometric median: 106.14814814814815
	Average time pr. step: 0.000004578949057920447
	Average time pr. geometric median: 0.000486046962962963

