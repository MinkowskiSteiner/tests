All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.4151092043949345
MST len: 1.5753123683999999
Steiner Ratio: 0.8983038746989721
Solution Steiner tree:
	Terminals:
	  #0: (0.6047278813,0.6099463206) [ #3 ]
	  #1: (0.3921490034,0.2415533509) [ #5 ]
	  #2: (0.4918181642,0.7429387396) [ #7 ]
	  #4: (0.497944257,0.1569527281) [ #5 ]
	  #6: (0.1386943274,0.9690520419) [ #7 ]
	Steiner points:
	  #3: (0.46063708110506557,0.6099463206000001) [ #5 #0 #7 ]
	  #5: (0.46063708110506557,0.2415533509) [ #4 #3 #1 ]
	  #7: (0.46063708110506557,0.7429387396) [ #2 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.001715742
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 12

	Init times:
		Bottleneck Steiner distances: 0.00010016
		Smallest sphere distances: 0.000009344
		Sorting: 0.000017163
		Total init time: 0.000136767

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000011353, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000066015, bsd pruned: 3, ss pruned: 3

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 9
	Total number iterations 18 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.001298745
	Avarage time pr. topology: 0.000144305

Data for the geometric median iteration:
	Total time: 0.001130167
	Total steps: 215
	Average number of steps pr. geometric median: 7.962962962962963
	Average time pr. step: 0.000005256590697674419
	Average time pr. geometric median: 0.00004185803703703704

