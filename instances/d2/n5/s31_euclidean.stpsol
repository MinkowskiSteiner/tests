All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.18448269713534
MST len: 1.2303611514985668
Steiner Ratio: 0.9627113922547478
Solution Steiner tree:
	Terminals:
	  #0: (0.1665347033,0.6197118892) [ #3 ]
	  #1: (0.433104444,0.3224785879) [ #5 ]
	  #2: (0.4428926201,0.5949672445) [ #3 ]
	  #4: (0.8626960571,0.4022940902) [ #5 ]
	  #6: (0.5921190123,0.7871168474) [ #7 ]
	Steiner points:
	  #3: (0.44289262009999986,0.594967244499998) [ #2 #0 #7 ]
	  #5: (0.5198578278666822,0.44040757136664466) [ #1 #4 #7 ]
	  #7: (0.45174556380873765,0.5940284666495984) [ #3 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.005633591
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.00010126
		Smallest sphere distances: 0.000009941
		Sorting: 0.000023055
		Total init time: 0.000143642

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000010414, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000070751, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 41 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.7333333333333334
	Total time: 0.005173495
	Avarage time pr. topology: 0.000344899

Data for the geometric median iteration:
	Total time: 0.004907991
	Total steps: 1085
	Average number of steps pr. geometric median: 24.11111111111111
	Average time pr. step: 0.0000045234940092165894
	Average time pr. geometric median: 0.00010906646666666666

