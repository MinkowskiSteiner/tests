All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.1743039862
MST len: 2.2745046379000002
Steiner Ratio: 0.9559461651427921
Solution Steiner tree:
	Terminals:
	  #0: (0.3297338986,0.9487606892) [ #3 ]
	  #1: (0.8256802293,0.8959328252) [ #3 ]
	  #2: (0.3771066863,0.354725022) [ #7 ]
	  #4: (0.9420667789,0.3395639064) [ #5 ]
	  #6: (0.2042499758,0.0233809524) [ #7 ]
	Steiner points:
	  #3: (0.5496357882395333,0.8959328252) [ #0 #5 #1 ]
	  #5: (0.5496357882395333,0.3395639064000025) [ #4 #3 #7 ]
	  #7: (0.37710668630000005,0.33956390640000256) [ #2 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.001762201
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 15

	Init times:
		Bottleneck Steiner distances: 0.000135078
		Smallest sphere distances: 0.000008794
		Sorting: 0.000015778
		Total init time: 0.000169272

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000011582, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000062392, bsd pruned: 3, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 12
	Total number iterations 24 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.001286992
	Avarage time pr. topology: 0.000107249

Data for the geometric median iteration:
	Total time: 0.001064993
	Total steps: 204
	Average number of steps pr. geometric median: 5.666666666666667
	Average time pr. step: 0.000005220553921568627
	Average time pr. geometric median: 0.00002958313888888889

