All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 0.7823642570000001
MST len: 0.8245238665000001
Steiner Ratio: 0.9488679331030621
Solution Steiner tree:
	Terminals:
	  #0: (0.3323929949,0.5761897869) [ #5 ]
	  #1: (0.273373468,0.0765914861) [ #7 ]
	  #2: (0.2902333854,0.3277869785) [ #3 ]
	  #4: (0.2219586108,0.6696780285) [ #5 ]
	  #6: (0.2285638117,0.0594177293) [ #7 ]
	Steiner points:
	  #3: (0.28433940830851673,0.3277869785) [ #2 #5 #7 ]
	  #5: (0.28433940830851673,0.5761897869000001) [ #4 #3 #0 ]
	  #7: (0.273373468,0.07659148610000002) [ #1 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000534852
	Number of best updates: 1

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 4

	Init times:
		Bottleneck Steiner distances: 0.00010422
		Smallest sphere distances: 0.000008816
		Sorting: 0.000014429
		Total init time: 0.000137028

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000008459, bsd pruned: 0, ss pruned: 1
		Level 2 - Time: 0.000026746, bsd pruned: 4, ss pruned: 4

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 2
	Total number iterations 4 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000230658
	Avarage time pr. topology: 0.000115329

Data for the geometric median iteration:
	Total time: 0.000193272
	Total steps: 34
	Average number of steps pr. geometric median: 5.666666666666667
	Average time pr. step: 0.000005684470588235294
	Average time pr. geometric median: 0.000032212

