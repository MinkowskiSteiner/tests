All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.3640998492
MST len: 1.4613059151
Steiner Ratio: 0.9334800024446982
Solution Steiner tree:
	Terminals:
	  #0: (0.6883942167,0.1299254853) [ #7 ]
	  #1: (0.6420080073,0.4910349215) [ #3 ]
	  #2: (0.3111650372,0.5165184259) [ #5 ]
	  #4: (0.1151188249,0.8235438768) [ #5 ]
	  #6: (0.7856002826,0.4401777473) [ #7 ]
	Steiner points:
	  #3: (0.6420080072999998,0.4910349215) [ #1 #5 #7 ]
	  #5: (0.3111650372,0.5165184259000001) [ #2 #3 #4 ]
	  #7: (0.6883942167,0.4401777472999999) [ #0 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.001934167
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 15

	Init times:
		Bottleneck Steiner distances: 0.000100799
		Smallest sphere distances: 0.000009441
		Sorting: 0.000023534
		Total init time: 0.000142562

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000011551, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000062228, bsd pruned: 3, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 12
	Total number iterations 24 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.001487797
	Avarage time pr. topology: 0.000123983

Data for the geometric median iteration:
	Total time: 0.001265052
	Total steps: 246
	Average number of steps pr. geometric median: 6.833333333333333
	Average time pr. step: 0.0000051424878048780484
	Average time pr. geometric median: 0.000035140333333333326

