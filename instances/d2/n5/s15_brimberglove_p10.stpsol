All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.6443077456027149
MST len: 1.7911250303
Steiner Ratio: 0.9180306889727881
Solution Steiner tree:
	Terminals:
	  #0: (0.0476035024,0.2348172144) [ #7 ]
	  #1: (0.3857721921,0.3929051535) [ #3 ]
	  #2: (0.3549731771,0.6731490375) [ #5 ]
	  #4: (0.9831143995,0.7832298362) [ #5 ]
	  #6: (0.5153574038,0.2768868838) [ #7 ]
	Steiner points:
	  #3: (0.38577219209728514,0.3929051535) [ #1 #5 #7 ]
	  #5: (0.3857721920972852,0.6731490374999999) [ #2 #3 #4 ]
	  #7: (0.38577219209728514,0.2768868838) [ #0 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.002356939
	Number of best updates: 2

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000155507
		Smallest sphere distances: 0.000009359
		Sorting: 0.000022838
		Total init time: 0.000197677

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00001208, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000087895, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.001799649
	Avarage time pr. topology: 0.000119976

Data for the geometric median iteration:
	Total time: 0.001499243
	Total steps: 260
	Average number of steps pr. geometric median: 5.777777777777778
	Average time pr. step: 0.00000576631923076923
	Average time pr. geometric median: 0.00003331651111111111

