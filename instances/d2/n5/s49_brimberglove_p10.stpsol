All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.6890653123
MST len: 2.0089078681
Steiner Ratio: 0.8407878425492438
Solution Steiner tree:
	Terminals:
	  #0: (0.6634417692,0.9268148322) [ #3 ]
	  #1: (0.3087208249,0.1629512739) [ #7 ]
	  #2: (0.4391896513,0.6174133409) [ #3 ]
	  #4: (0.7427612197,0.2952385462) [ #5 ]
	  #6: (0.0418115836,0.220037731) [ #7 ]
	Steiner points:
	  #3: (0.45340095868791663,0.6174133409) [ #5 #2 #0 ]
	  #5: (0.45340095868791663,0.2952385462) [ #4 #3 #7 ]
	  #7: (0.3087208249,0.220037731) [ #1 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.001919944
	Number of best updates: 2

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 15

	Init times:
		Bottleneck Steiner distances: 0.000168058
		Smallest sphere distances: 0.000026332
		Sorting: 0.00001638
		Total init time: 0.000221629

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000011797, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00006446, bsd pruned: 3, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 12
	Total number iterations 24 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.001403669
	Avarage time pr. topology: 0.000116972

Data for the geometric median iteration:
	Total time: 0.00117293
	Total steps: 215
	Average number of steps pr. geometric median: 5.972222222222222
	Average time pr. step: 0.000005455488372093023
	Average time pr. geometric median: 0.00003258138888888889

