All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.6656218684629596
MST len: 1.8066422274
Steiner Ratio: 0.9219433948801321
Solution Steiner tree:
	Terminals:
	  #0: (0.3582976681,0.8231695224) [ #3 ]
	  #1: (0.2498201687,0.6923724262) [ #3 ]
	  #2: (0.3215230151,0.5994994997) [ #5 ]
	  #4: (0.9209823329,0.0083380486) [ #7 ]
	  #6: (0.2154374701,0.0895364271) [ #7 ]
	Steiner points:
	  #3: (0.3582910475629599,0.6923724262) [ #0 #5 #1 ]
	  #5: (0.3582910475629599,0.5994994997) [ #2 #3 #7 ]
	  #7: (0.3582910475629599,0.0895364271) [ #4 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.002038841
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 15

	Init times:
		Bottleneck Steiner distances: 0.000107091
		Smallest sphere distances: 0.000020006
		Sorting: 0.000015628
		Total init time: 0.000152851

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000011741, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000063203, bsd pruned: 0, ss pruned: 3

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 12
	Total number iterations 25 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0833333333333335
	Total time: 0.001586064
	Avarage time pr. topology: 0.000132172

Data for the geometric median iteration:
	Total time: 0.001357864
	Total steps: 262
	Average number of steps pr. geometric median: 7.277777777777778
	Average time pr. step: 0.000005182687022900764
	Average time pr. geometric median: 0.000037718444444444444

