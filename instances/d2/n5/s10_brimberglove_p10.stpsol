All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.315253490587914
MST len: 1.4607997456000004
Steiner Ratio: 0.9003653611999327
Solution Steiner tree:
	Terminals:
	  #0: (0.0253341845,0.3162595943) [ #3 ]
	  #1: (0.584652962,0.4221560766) [ #7 ]
	  #2: (0.5057680786,0.1796468744) [ #5 ]
	  #4: (0.8166862739,0.1834716542) [ #5 ]
	  #6: (0.5658107323,0.6109299178) [ #7 ]
	Steiner points:
	  #3: (0.5520772576243413,0.3162595943) [ #0 #5 #7 ]
	  #5: (0.5520772576243413,0.18347165420000086) [ #2 #3 #4 ]
	  #7: (0.5520772579120871,0.4221560766) [ #1 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.001487351
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 12

	Init times:
		Bottleneck Steiner distances: 0.00010057
		Smallest sphere distances: 0.000009027
		Sorting: 0.000023829
		Total init time: 0.00014279

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000011623, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000053976, bsd pruned: 3, ss pruned: 3

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 9
	Total number iterations 18 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.001076559
	Avarage time pr. topology: 0.000119617

Data for the geometric median iteration:
	Total time: 0.000890057
	Total steps: 170
	Average number of steps pr. geometric median: 6.296296296296297
	Average time pr. step: 0.000005235629411764706
	Average time pr. geometric median: 0.00003296507407407408

