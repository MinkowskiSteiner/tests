All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.2470719066377525
MST len: 1.2847801716717417
Steiner Ratio: 0.9706500256888899
Solution Steiner tree:
	Terminals:
	  #0: (0.8844101391,0.2439990897) [ #7 ]
	  #1: (0.0904165954,0.6112220658) [ #5 ]
	  #2: (0.3336426291,0.698289798) [ #3 ]
	  #4: (0.052807556,0.6665685525) [ #5 ]
	  #6: (0.5058995711,0.1543681692) [ #7 ]
	Steiner points:
	  #3: (0.29356133084949576,0.6208716272005899) [ #5 #2 #7 ]
	  #5: (0.09057283797755375,0.611536113243904) [ #1 #3 #4 ]
	  #7: (0.5443987752920296,0.22862057431442181) [ #0 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.004198845
	Number of best updates: 2

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 12

	Init times:
		Bottleneck Steiner distances: 0.000136541
		Smallest sphere distances: 0.00000865
		Sorting: 0.000016363
		Total init time: 0.000171901

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000010705, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000049572, bsd pruned: 0, ss pruned: 6

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 9
	Total number iterations 36 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 4
	Total time: 0.003775791
	Avarage time pr. topology: 0.000419532

Data for the geometric median iteration:
	Total time: 0.003558971
	Total steps: 773
	Average number of steps pr. geometric median: 28.62962962962963
	Average time pr. step: 0.000004604102199223804
	Average time pr. geometric median: 0.00013181374074074074

