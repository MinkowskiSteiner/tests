All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.491568017369175
MST len: 1.5619745730036871
Steiner Ratio: 0.9549246467571365
Solution Steiner tree:
	Terminals:
	  #0: (0.8650269224,0.5663939652) [ #3 ]
	  #1: (0.6730409189,0.520684405) [ #3 ]
	  #2: (0.414524392,0.4607495118) [ #5 ]
	  #4: (0.1862228323,0.3292264125) [ #9 ]
	  #6: (0.5234706907,0.8290278501) [ #7 ]
	  #8: (0.6338536677,0.0191505631) [ #9 ]
	Steiner points:
	  #3: (0.6730409345617634,0.5206845978112614) [ #7 #0 #1 ]
	  #5: (0.4145270589699922,0.4607474755131397) [ #7 #2 #9 ]
	  #7: (0.5568509214516941,0.5692403588701557) [ #3 #5 #6 ]
	  #9: (0.37177434850603264,0.35516039303110114) [ #4 #5 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.059290439
	Number of best updates: 9

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000175058
		Smallest sphere distances: 0.000012526
		Sorting: 0.000025379
		Total init time: 0.000223274

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000010328, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000068858, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00068603, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 378 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 3.6
	Total time: 0.056803103
	Avarage time pr. topology: 0.000540981

Data for the geometric median iteration:
	Total time: 0.053962849
	Total steps: 12119
	Average number of steps pr. geometric median: 28.854761904761904
	Average time pr. step: 0.0000044527476689495835
	Average time pr. geometric median: 0.00012848297380952382

