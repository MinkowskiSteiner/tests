All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.8978024172000003
MST len: 2.1375001926
Steiner Ratio: 0.8878606999756863
Solution Steiner tree:
	Terminals:
	  #0: (0.3921490034,0.2415533509) [ #7 ]
	  #1: (0.6047278813,0.6099463206) [ #3 ]
	  #2: (0.4918181642,0.7429387396) [ #3 ]
	  #4: (0.8727000411,0.9242446869) [ #9 ]
	  #6: (0.497944257,0.1569527281) [ #7 ]
	  #8: (0.1386943274,0.9690520419) [ #9 ]
	Steiner points:
	  #3: (0.49237345892773476,0.7034926109531858) [ #1 #5 #2 ]
	  #5: (0.49237345892773476,0.7034926109531858) [ #7 #3 #9 ]
	  #7: (0.49237345892773476,0.2415533509) [ #6 #5 #0 ]
	  #9: (0.4923734589277349,0.9242446868999998) [ #4 #5 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.012447814
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 93

	Init times:
		Bottleneck Steiner distances: 0.000131688
		Smallest sphere distances: 0.000012891
		Sorting: 0.000033727
		Total init time: 0.000188603

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000011134, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000074457, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000561189, bsd pruned: 15, ss pruned: 15

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 75
	Total number iterations 150 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.01034587
	Avarage time pr. topology: 0.000137944

Data for the geometric median iteration:
	Total time: 0.008591087
	Total steps: 1664
	Average number of steps pr. geometric median: 5.546666666666667
	Average time pr. step: 0.000005162912860576923
	Average time pr. geometric median: 0.00002863695666666667

