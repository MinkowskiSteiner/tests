All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.464012963826166
MST len: 1.5370799027925912
Steiner Ratio: 0.9524637991599032
Solution Steiner tree:
	Terminals:
	  #0: (0.6899727819,0.193846746) [ #9 ]
	  #1: (0.4630026335,0.5578971945) [ #7 ]
	  #2: (0.7275817463,0.4842158102) [ #3 ]
	  #4: (0.5012496633,0.921708364) [ #5 ]
	  #6: (0.2818813474,0.2621717841) [ #7 ]
	  #8: (0.9432684327,0.3444002356) [ #9 ]
	Steiner points:
	  #3: (0.7275804182895701,0.48421449263173083) [ #2 #5 #9 ]
	  #5: (0.5170638262705048,0.592661118675001) [ #3 #4 #7 ]
	  #7: (0.46300402560807363,0.5578958326752177) [ #1 #5 #6 ]
	  #9: (0.7939735224678491,0.3527546966491157) [ #0 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.069480211
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 93

	Init times:
		Bottleneck Steiner distances: 0.000144332
		Smallest sphere distances: 0.000012574
		Sorting: 0.000021574
		Total init time: 0.000187827

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000011086, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000068911, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000514507, bsd pruned: 0, ss pruned: 30

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 75
	Total number iterations 383 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 5.1066666666666665
	Total time: 0.067434962
	Avarage time pr. topology: 0.000899132

Data for the geometric median iteration:
	Total time: 0.06467889
	Total steps: 14519
	Average number of steps pr. geometric median: 48.39666666666667
	Average time pr. step: 0.000004454775811006268
	Average time pr. geometric median: 0.00021559630000000002

