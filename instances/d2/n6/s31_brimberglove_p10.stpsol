All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.4531177962033026
MST len: 1.6512798471
Steiner Ratio: 0.8799948711027314
Solution Steiner tree:
	Terminals:
	  #0: (0.2624237846,0.4864085733) [ #5 ]
	  #1: (0.433104444,0.3224785879) [ #7 ]
	  #2: (0.4428926201,0.5949672445) [ #3 ]
	  #4: (0.1665347033,0.6197118892) [ #5 ]
	  #6: (0.8626960571,0.4022940902) [ #7 ]
	  #8: (0.5921190123,0.7871168474) [ #9 ]
	Steiner points:
	  #3: (0.44289232139669754,0.5949672445000002) [ #2 #7 #9 ]
	  #5: (0.26242378460000004,0.6197118892) [ #4 #0 #9 ]
	  #7: (0.44289232139669754,0.40229409020000007) [ #6 #3 #1 ]
	  #9: (0.44289232139669754,0.6197118892) [ #3 #5 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.012334691
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 93

	Init times:
		Bottleneck Steiner distances: 0.000138756
		Smallest sphere distances: 0.0000128
		Sorting: 0.000034592
		Total init time: 0.000196197

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000010782, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000073536, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000533631, bsd pruned: 0, ss pruned: 30

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 75
	Total number iterations 150 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.010271226
	Avarage time pr. topology: 0.000136949

Data for the geometric median iteration:
	Total time: 0.0085231
	Total steps: 1653
	Average number of steps pr. geometric median: 5.51
	Average time pr. step: 0.000005156140350877193
	Average time pr. geometric median: 0.00002841033333333333

