All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.4040785934283209
MST len: 1.415732136573826
Steiner Ratio: 0.991768539510795
Solution Steiner tree:
	Terminals:
	  #0: (0.0995092923,0.7305007729) [ #7 ]
	  #1: (0.0529571777,0.5661688692) [ #3 ]
	  #2: (0.2158478607,0.460222048) [ #5 ]
	  #4: (0.746623855,0.6549854328) [ #9 ]
	  #6: (0.0288339364,0.6024998616) [ #7 ]
	  #8: (0.9994984996,0.2633149048) [ #9 ]
	Steiner points:
	  #3: (0.05295884218166281,0.5661704868602434) [ #5 #7 #1 ]
	  #5: (0.21585141617806372,0.46024151698018717) [ #2 #3 #9 ]
	  #7: (0.03074435963351811,0.6025522959510786) [ #6 #3 #0 ]
	  #9: (0.7196964715529067,0.5704287748926459) [ #4 #5 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.02823849
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 63

	Init times:
		Bottleneck Steiner distances: 0.000172225
		Smallest sphere distances: 0.000025733
		Sorting: 0.000022573
		Total init time: 0.000230977

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000010996, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000069439, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000351814, bsd pruned: 0, ss pruned: 60

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 45
	Total number iterations 132 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.933333333333333
	Total time: 0.026500037
	Avarage time pr. topology: 0.000588889

Data for the geometric median iteration:
	Total time: 0.025405901
	Total steps: 5669
	Average number of steps pr. geometric median: 31.494444444444444
	Average time pr. step: 0.000004481548950432175
	Average time pr. geometric median: 0.00014114389444444444

