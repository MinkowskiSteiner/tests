All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.7683480955000002
MST len: 1.9189409853
Steiner Ratio: 0.9215229176125723
Solution Steiner tree:
	Terminals:
	  #0: (0.9542649202,0.248271084) [ #7 ]
	  #1: (0.55006754,0.2691321919) [ #3 ]
	  #2: (0.9220907362,0.6533997663) [ #5 ]
	  #4: (0.8679820145,0.8387081427) [ #9 ]
	  #6: (0.9058258696,0.043887435) [ #7 ]
	  #8: (0.3527607286,0.7895895954) [ #9 ]
	Steiner points:
	  #3: (0.9040544956333278,0.2691321919) [ #5 #7 #1 ]
	  #5: (0.9040544956333278,0.6533997663) [ #2 #3 #9 ]
	  #7: (0.9058258696,0.248271084) [ #6 #3 #0 ]
	  #9: (0.8679820144999999,0.7895895954) [ #4 #5 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.013167876
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 93

	Init times:
		Bottleneck Steiner distances: 0.000131374
		Smallest sphere distances: 0.000012576
		Sorting: 0.000035902
		Total init time: 0.000189427

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00001086, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000073059, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00055887, bsd pruned: 30, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 75
	Total number iterations 150 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.011068842
	Avarage time pr. topology: 0.000147584

Data for the geometric median iteration:
	Total time: 0.009310704
	Total steps: 1803
	Average number of steps pr. geometric median: 6.01
	Average time pr. step: 0.000005164006655574043
	Average time pr. geometric median: 0.00003103568

