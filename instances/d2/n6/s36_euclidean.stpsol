All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.2993672257789806
MST len: 1.3523523083267173
Steiner Ratio: 0.9608200598161467
Solution Steiner tree:
	Terminals:
	  #0: (0.3812597861,0.3492478469) [ #5 ]
	  #1: (0.8042997703,0.7905323886) [ #9 ]
	  #2: (0.4658759178,0.5137684199) [ #3 ]
	  #4: (0.4677818946,0.0902298559) [ #5 ]
	  #6: (0.5993793447,0.9228681605) [ #7 ]
	  #8: (0.8728369269,0.9995752056) [ #9 ]
	Steiner points:
	  #3: (0.46587591785319593,0.5137684198712026) [ #5 #2 #7 ]
	  #5: (0.38127348194898425,0.3492467997122456) [ #3 #4 #0 ]
	  #7: (0.6500505667286142,0.8359382843623505) [ #6 #3 #9 ]
	  #9: (0.7781621348283523,0.8358690291378074) [ #1 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.017613817
	Number of best updates: 2

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 54

	Init times:
		Bottleneck Steiner distances: 0.000153744
		Smallest sphere distances: 0.000011633
		Sorting: 0.00002558
		Total init time: 0.000200799

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000009763, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000068057, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000390959, bsd pruned: 15, ss pruned: 54

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 36
	Total number iterations 103 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.861111111111111
	Total time: 0.015983451
	Avarage time pr. topology: 0.000443984

Data for the geometric median iteration:
	Total time: 0.015159969
	Total steps: 3383
	Average number of steps pr. geometric median: 23.493055555555557
	Average time pr. step: 0.000004481220514336388
	Average time pr. geometric median: 0.00010527756250000002

