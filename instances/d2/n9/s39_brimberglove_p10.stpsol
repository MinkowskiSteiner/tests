All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.1339019353455493
MST len: 2.2954086628000003
Steiner Ratio: 0.9296392271790763
Solution Steiner tree:
	Terminals:
	  #0: (0.6123015828,0.975351023) [ #7 ]
	  #1: (0.3832746434,0.7694257664) [ #3 ]
	  #2: (0.4010537497,0.5862380008) [ #11 ]
	  #4: (0.3322590768,0.7771537661) [ #5 ]
	  #6: (0.2816415891,0.9608924622) [ #7 ]
	  #8: (0.0309723057,0.7538297781) [ #13 ]
	  #10: (0.6286001725,0.3799427857) [ #15 ]
	  #12: (0.0521304696,0.9748592134) [ #13 ]
	  #14: (0.9571484462,0.5881108905) [ #15 ]
	Steiner points:
	  #3: (0.3832746434,0.7694257664254923) [ #1 #5 #7 ]
	  #5: (0.38327464340000006,0.7694257664254923) [ #3 #4 #9 ]
	  #7: (0.38327464339999995,0.9608924622) [ #0 #6 #3 ]
	  #9: (0.3832746434000001,0.7694316026982665) [ #11 #13 #5 ]
	  #11: (0.40105374970000085,0.5862380008) [ #2 #9 #15 ]
	  #13: (0.0521304696,0.7694316026982665) [ #12 #9 #8 ]
	  #15: (0.6286001725,0.5862380008000001) [ #10 #11 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.948425891
	Number of best updates: 17

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 77613

	Init times:
		Bottleneck Steiner distances: 0.000026734
		Smallest sphere distances: 0.000002787
		Sorting: 0.000009742
		Total init time: 0.00004075

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001186, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000005943, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000054481, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000605499, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.006928616, bsd pruned: 945, ss pruned: 945
		Level 6 - Time: 0.070937522, bsd pruned: 17010, ss pruned: 25515

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 68040
	Total number iterations 138187 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0309670781893003
	Total time: 0.808290521
	Avarage time pr. topology: 0.000011879

Data for the geometric median iteration:
	Total time: 0.608814022
	Total steps: 2370230
	Average number of steps pr. geometric median: 4.976547409087091
	Average time pr. step: 0.00000025685862637803083
	Average time pr. geometric median: 0.0000012782691316032586

