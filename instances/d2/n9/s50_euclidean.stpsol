All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.9801867982361745
MST len: 2.0246399054584208
Steiner Ratio: 0.9780439439613924
Solution Steiner tree:
	Terminals:
	  #0: (0.2348691641,0.1913313205) [ #9 ]
	  #1: (0.5204600438,0.6477700405) [ #5 ]
	  #2: (0.4307993098,0.5479775013) [ #3 ]
	  #4: (0.6714702508,0.5076044628) [ #15 ]
	  #6: (0.0248203492,0.9218689981) [ #13 ]
	  #8: (0.0156422849,0.215467854) [ #11 ]
	  #10: (0.0151233026,0.0380932228) [ #11 ]
	  #12: (0.1040961771,0.975851736) [ #13 ]
	  #14: (0.8943212232,0.6286429756) [ #15 ]
	Steiner points:
	  #3: (0.4307993097999966,0.5479775013000012) [ #7 #9 #2 ]
	  #5: (0.520447546704098,0.647695152198325) [ #7 #1 #15 ]
	  #7: (0.4476574825126931,0.624613564355115) [ #5 #3 #13 ]
	  #9: (0.234869099262235,0.19133141145673757) [ #3 #11 #0 ]
	  #11: (0.06061398701182789,0.1613928571556704) [ #8 #10 #9 ]
	  #13: (0.09724947283462727,0.9447926251912038) [ #6 #7 #12 ]
	  #15: (0.6744507199141515,0.5334456901848814) [ #4 #5 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.70317445
	Number of best updates: 12

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 40233

	Init times:
		Bottleneck Steiner distances: 0.000010939
		Smallest sphere distances: 0.000001237
		Sorting: 0.000012062
		Total init time: 0.00002551

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000697, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003409, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000030888, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000353073, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004301203, bsd pruned: 0, ss pruned: 945
		Level 6 - Time: 0.035005427, bsd pruned: 18900, ss pruned: 74235

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 29715
	Total number iterations 154827 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 5.210398788490661
	Total time: 0.630940458
	Avarage time pr. topology: 0.000021233

Data for the geometric median iteration:
	Total time: 0.560663921
	Total steps: 5111274
	Average number of steps pr. geometric median: 24.57284199899041
	Average time pr. step: 0.00000010969161915405043
	Average time pr. geometric median: 0.000002695434826085911

