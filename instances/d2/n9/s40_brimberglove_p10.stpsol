All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.2609899604000003
MST len: 2.3919899688
Steiner Ratio: 0.9452338805309795
Solution Steiner tree:
	Terminals:
	  #0: (0.3411775247,0.8119810777) [ #9 ]
	  #1: (0.1292244392,0.3481050377) [ #11 ]
	  #2: (0.35034057,0.4464217548) [ #3 ]
	  #4: (0.7246325052,0.5291235137) [ #7 ]
	  #6: (0.8194229132,0.5062902763) [ #15 ]
	  #8: (0.0883140588,0.7855343049) [ #13 ]
	  #10: (0.0208598026,0.0373252058) [ #11 ]
	  #12: (0.1947866791,0.8602793272) [ #13 ]
	  #14: (0.9184383005,0.2771394566) [ #15 ]
	Steiner points:
	  #3: (0.35034057,0.4464217548) [ #11 #2 #5 ]
	  #5: (0.35034056999999996,0.5291235136963285) [ #7 #9 #3 ]
	  #7: (0.7246325052,0.5291235136963285) [ #4 #5 #15 ]
	  #9: (0.3411775247,0.8104493109104896) [ #5 #13 #0 ]
	  #11: (0.1292244392,0.3481050377) [ #3 #10 #1 ]
	  #13: (0.1947866791,0.8104493109104896) [ #8 #9 #12 ]
	  #15: (0.8194229132,0.5062902763) [ #6 #7 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.418755968
	Number of best updates: 14

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 36033

	Init times:
		Bottleneck Steiner distances: 0.000016526
		Smallest sphere distances: 0.000002704
		Sorting: 0.00001887
		Total init time: 0.000039457

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001149, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000005955, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000053653, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000604035, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.006898929, bsd pruned: 0, ss pruned: 1890
		Level 6 - Time: 0.054563949, bsd pruned: 34020, ss pruned: 50085

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 26460
	Total number iterations 53592 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0253968253968253
	Total time: 0.318204001
	Avarage time pr. topology: 0.000012025

Data for the geometric median iteration:
	Total time: 0.240174209
	Total steps: 934033
	Average number of steps pr. geometric median: 5.042830147932189
	Average time pr. step: 0.00000025713674891572355
	Average time pr. geometric median: 0.0000012966969495734803

