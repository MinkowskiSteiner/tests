All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.8574193928532572
MST len: 1.9541781473301876
Steiner Ratio: 0.950486216106181
Solution Steiner tree:
	Terminals:
	  #0: (0.5785871486,0.6617850073) [ #9 ]
	  #1: (0.6551358368,0.3903584468) [ #5 ]
	  #2: (0.5311698176,0.3798184727) [ #7 ]
	  #4: (0.6938135543,0.3702549456) [ #13 ]
	  #6: (0.153481192,0.2006002349) [ #11 ]
	  #8: (0.1488122675,0.7401193766) [ #15 ]
	  #10: (0.0819520862,0.2319531549) [ #11 ]
	  #12: (0.7874475535,0.1478928184) [ #13 ]
	  #14: (0.4885317048,0.9929940151) [ #15 ]
	Steiner points:
	  #3: (0.5934127884162455,0.42107900181725455) [ #9 #5 #7 ]
	  #5: (0.6551358368,0.3903584468) [ #1 #13 #3 ]
	  #7: (0.5311698175999998,0.37981847270000046) [ #2 #11 #3 ]
	  #9: (0.5785748708093336,0.6617796235964043) [ #0 #3 #15 ]
	  #11: (0.153481192,0.20060023490000004) [ #7 #10 #6 ]
	  #13: (0.6938135535697094,0.3702549449264455) [ #12 #5 #4 ]
	  #15: (0.43564845017532355,0.810691229284146) [ #8 #9 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 1.271912341
	Number of best updates: 15

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 54933

	Init times:
		Bottleneck Steiner distances: 0.000011456
		Smallest sphere distances: 0.000001138
		Sorting: 0.000003207
		Total init time: 0.000017239

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000735, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003463, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000030179, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000337094, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004526275, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.040545488, bsd pruned: 31185, ss pruned: 60480

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 43470
	Total number iterations 315825 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 7.265355417529331
	Total time: 1.188504118
	Avarage time pr. topology: 0.00002734

Data for the geometric median iteration:
	Total time: 1.050357893
	Total steps: 9535949
	Average number of steps pr. geometric median: 31.338358145190444
	Average time pr. step: 0.00000011014718021247806
	Average time pr. geometric median: 0.0000034518317821814717

