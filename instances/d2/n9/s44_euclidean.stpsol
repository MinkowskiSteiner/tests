All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.104982717289099
MST len: 2.1323317182704016
Steiner Ratio: 0.9871741339553461
Solution Steiner tree:
	Terminals:
	  #0: (0.7497371406,0.6546126439) [ #11 ]
	  #1: (0.3823489777,0.5831195775) [ #9 ]
	  #2: (0.443329524,0.5713405118) [ #3 ]
	  #4: (0.5829005281,0.1765204129) [ #15 ]
	  #6: (0.4074837125,0.1925271313) [ #13 ]
	  #8: (0.1180090462,0.7377023845) [ #9 ]
	  #10: (0.8574096602,0.9521014504) [ #11 ]
	  #12: (0.0727788904,0.1874291036) [ #13 ]
	  #14: (0.7923043043,0.0619196091) [ #15 ]
	Steiner points:
	  #3: (0.44332952369674783,0.5713405110465313) [ #9 #5 #2 ]
	  #5: (0.5067143697031794,0.5303122421063869) [ #7 #11 #3 ]
	  #7: (0.4916873047158221,0.23558413256833158) [ #13 #5 #15 ]
	  #9: (0.3823489777,0.5831195775) [ #3 #8 #1 ]
	  #11: (0.7497371406,0.6546126439) [ #5 #10 #0 ]
	  #13: (0.4074829490314146,0.1925301988402631) [ #6 #7 #12 ]
	  #15: (0.5829005365258886,0.17652042703034254) [ #4 #7 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 1.62910961
	Number of best updates: 14

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 61233

	Init times:
		Bottleneck Steiner distances: 0.000010404
		Smallest sphere distances: 0.000001226
		Sorting: 0.000003025
		Total init time: 0.000015893

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000673, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003241, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000030449, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000339808, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004238972, bsd pruned: 0, ss pruned: 945
		Level 6 - Time: 0.038495924, bsd pruned: 28350, ss pruned: 43785

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 50715
	Total number iterations 410860 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 8.101350685201616
	Total time: 1.548579087
	Avarage time pr. topology: 0.000030534

Data for the geometric median iteration:
	Total time: 1.370424273
	Total steps: 12426782
	Average number of steps pr. geometric median: 35.00452669680709
	Average time pr. step: 0.0000001102798997359091
	Average time pr. geometric median: 0.000003860295694426839

