All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.304883843884303
MST len: 5.873913044123009
Steiner Ratio: 0.9031260429011571
Solution Steiner tree:
	Terminals:
	  #0: (0.5567509437,0.309231437,0.9220288228,0.7756007373,0.7636273577,0.4406828454,0.3494424649) [ #9 ]
	  #1: (0.2825672064,0.2314274824,0.4842886475,0.3893124645,0.9920990551,0.5659609305,0.9402686734) [ #11 ]
	  #2: (0.2188497932,0.4543959207,0.5186540231,0.5738417281,0.5552310401,0.7285462551,0.6288739409) [ #5 ]
	  #4: (0.106745258,0.7606715722,0.0816722829,0.5509562085,0.5646518891,0.743270507,0.9817601498) [ #5 ]
	  #6: (0.033469948,0.3299642076,0.6906357043,0.4224866822,0.2062651386,0.2501284509,0.6365585242) [ #7 ]
	  #8: (0.7961281304,0.5837158191,0.2747664383,0.8295980784,0.9136800267,0.9654021426,0.2520847601) [ #15 ]
	  #10: (0.3188583978,0.1692291001,0.9783164058,0.1149865278,0.7529449192,0.2530828441,0.9445846062) [ #11 ]
	  #12: (0.1199451648,0.215530593,0.8886432843,0.9835675461,0.5171862247,0.9135672198,0.3485602547) [ #13 ]
	  #14: (0.8636223808,0.3016556312,0.0249239351,0.3649927091,0.7653809822,0.3178602631,0.1357283928) [ #15 ]
	Steiner points:
	  #3: (0.23967798516791933,0.3944529166733462,0.5937003121853385,0.5815718913298183,0.5715824678850081,0.6562562636201134,0.604884411030498) [ #13 #5 #7 ]
	  #5: (0.2188497931861752,0.45439592068759516,0.5186540229447183,0.5738417281545992,0.5552310404000029,0.7285462539535462,0.6288739416298635) [ #4 #2 #3 ]
	  #7: (0.20260681289814889,0.33426155381455214,0.6401896828534767,0.4704755978814777,0.5570422994726466,0.5045932080010809,0.6918017955955414) [ #6 #11 #3 ]
	  #9: (0.48036836596871335,0.3503760644593864,0.6937423579227566,0.7309424558890808,0.7062124667883821,0.6043021291670007,0.38139187833004357) [ #13 #0 #15 ]
	  #11: (0.2606140387914433,0.255707635203506,0.6708918924314593,0.3511286189947487,0.7614881293360587,0.462358673851122,0.8444383067963904) [ #10 #7 #1 ]
	  #13: (0.32464463433515617,0.3405773512001611,0.6947766174030804,0.7251654715521725,0.6215076720836753,0.6820726106727693,0.4558804774980243) [ #3 #9 #12 ]
	  #15: (0.6998280779619661,0.4302085248013046,0.3627831912942466,0.6790243032034827,0.8026172116817553,0.6754910962117927,0.26951274223238975) [ #8 #9 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 18.320835996
	Number of best updates: 15

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 146598

	Init times:
		Bottleneck Steiner distances: 0.000022371
		Smallest sphere distances: 0.000001585
		Sorting: 0.000003774
		Total init time: 0.00002989

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000789, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003691, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031961, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000360702, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004808287, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.073812953, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 135135
	Total number iterations 2103424 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 15.565353165353166
	Total time: 18.175769597
	Avarage time pr. topology: 0.0001345

Data for the geometric median iteration:
	Total time: 17.277729898
	Total steps: 79840046
	Average number of steps pr. geometric median: 84.4024187452759
	Average time pr. step: 0.00000021640430790833964
	Average time pr. geometric median: 0.000018265047014361302

