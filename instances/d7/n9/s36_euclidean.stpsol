All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.112261044490506
MST len: 5.816482382199245
Steiner Ratio: 0.8789265931821718
Solution Steiner tree:
	Terminals:
	  #0: (0.1528823809,0.9984735842,0.4864156537,0.460190471,0.3998939984,0.2654632643,0.541314984) [ #9 ]
	  #1: (0.3812597861,0.3492478469,0.8728369269,0.9995752056,0.5993793447,0.9228681605,0.4677818946) [ #11 ]
	  #2: (0.5041084446,0.5189005372,0.4179864002,0.7335768397,0.2084576535,0.9853335181,0.3443050209) [ #15 ]
	  #4: (0.0902298559,0.4658759178,0.5137684199,0.8042997703,0.7905323886,0.4063076491,0.8204223732) [ #7 ]
	  #6: (0.6381843079,0.3630522044,0.078085281,0.7284141638,0.8289281227,0.5918537013,0.5327139341) [ #13 ]
	  #8: (0.2294683951,0.6895571163,0.567347118,0.6107281813,0.0388049632,0.4401840444,0.6103033869) [ #9 ]
	  #10: (0.3965289325,0.1050152244,0.7738483463,0.7833614111,0.3344053693,0.5581647426,0.9716389775) [ #11 ]
	  #12: (0.6194605108,0.9981613504,0.3531363068,0.7723428918,0.9966349341,0.8395519605,0.2325333623) [ #13 ]
	  #14: (0.3868324786,0.2293901449,0.7843163962,0.1882775664,0.1697030753,0.9607357951,0.4463474226) [ #15 ]
	Steiner points:
	  #3: (0.3504111551572797,0.4960348211894886,0.5785599858214783,0.6536535326223816,0.3156948388289342,0.6921514556039167,0.5498541855558378) [ #9 #5 #15 ]
	  #5: (0.3531903120157849,0.4175441902348422,0.6079841686376312,0.7572036309832321,0.48103610667465985,0.6698186658472427,0.6239368136084634) [ #11 #7 #3 ]
	  #7: (0.33089493872363984,0.4633871647704216,0.48730937988960044,0.7685243371010749,0.6591810175958301,0.5896418272560143,0.6449304965712) [ #5 #4 #13 ]
	  #9: (0.2425787801041792,0.6999102562065641,0.5561506459899891,0.5946436591161979,0.15900075014387607,0.4649483590415853,0.5857175420646374) [ #8 #3 #0 ]
	  #11: (0.3658576867932512,0.3515265493909527,0.6861966614283681,0.8074250784406979,0.4784649998151015,0.6986336990777412,0.6535416210787863) [ #5 #10 #1 ]
	  #13: (0.5229427848241021,0.5293475807721792,0.28271867098903036,0.7519426291299218,0.8016903650755727,0.6417208479619408,0.5119945183984482) [ #6 #7 #12 ]
	  #15: (0.40720872567031025,0.4539797527490629,0.5644809057274511,0.5932818421691705,0.2536821430627279,0.8374565324675634,0.46372262041684176) [ #2 #3 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 17.819006439
	Number of best updates: 22

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 146598

	Init times:
		Bottleneck Steiner distances: 0.000013445
		Smallest sphere distances: 0.000001402
		Sorting: 0.000003885
		Total init time: 0.000038218

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000905, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003614, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032183, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.00037326, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004827182, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.073842049, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 135135
	Total number iterations 2118654 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 15.678055278055279
	Total time: 17.673656067
	Avarage time pr. topology: 0.000130785

Data for the geometric median iteration:
	Total time: 16.766314296
	Total steps: 77434361
	Average number of steps pr. geometric median: 81.85926348783492
	Average time pr. step: 0.00000021652292444177334
	Average time pr. geometric median: 0.000017724407123035698

