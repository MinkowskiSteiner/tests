All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.559490011030511
MST len: 6.12072123096413
Steiner Ratio: 0.9083063582287647
Solution Steiner tree:
	Terminals:
	  #0: (0.7980745881,0.7089841076,0.5592682034,0.4220619516,0.6781494509,0.6176146719,0.8654722287) [ #7 ]
	  #1: (0.0627508867,0.1330705258,0.1031433517,0.1824975504,0.1907411801,0.3889029866,0.808236414) [ #5 ]
	  #2: (0.5998500383,0.0285220109,0.4814383762,0.729074478,0.3766270487,0.206070881,0.2581979913) [ #15 ]
	  #4: (0.1197466637,0.0576706543,0.2857596349,0.6257388632,0.607333408,0.320081121,0.7510317898) [ #5 ]
	  #6: (0.6239873639,0.9691653433,0.0583464685,0.4434102766,0.4754556192,0.1466605273,0.2289445811) [ #11 ]
	  #8: (0.8194229132,0.5062902763,0.0883140588,0.7855343049,0.3411775247,0.8119810777,0.9184383005) [ #9 ]
	  #10: (0.8166331438,0.958641605,0.1473828816,0.0937726004,0.1534282836,0.0076622083,0.4441131705) [ #11 ]
	  #12: (0.1292244392,0.3481050377,0.7246325052,0.5291235137,0.6861238385,0.9269996448,0.8449453608) [ #13 ]
	  #14: (0.2771394566,0.1947866791,0.8602793272,0.35034057,0.4464217548,0.0208598026,0.0373252058) [ #15 ]
	Steiner points:
	  #3: (0.2982909445674563,0.1942315786772031,0.40115628294661404,0.5428098521093754,0.4960203251113654,0.4132171376875448,0.6405150937157683) [ #13 #5 #15 ]
	  #5: (0.19179331444333086,0.1305853202916078,0.3105067027397613,0.5219750115047641,0.4947482898440548,0.3724136413902284,0.7097172185421297) [ #3 #4 #1 ]
	  #7: (0.6911508229694181,0.6399756796124141,0.3895607678772327,0.4862490995496834,0.5438796510565973,0.5587224645822745,0.7545554931409578) [ #9 #11 #0 ]
	  #9: (0.6337014128315226,0.5435623158275361,0.35482486543179376,0.5528508878849809,0.5085971954067448,0.6138074775147266,0.7783252431602077) [ #8 #7 #13 ]
	  #11: (0.6964240937374875,0.9030725418744319,0.14912240047574932,0.343387931900287,0.388861351967506,0.182300735234961,0.395860243110814) [ #10 #7 #6 ]
	  #13: (0.3766532753272865,0.3519452707397379,0.4595502170092751,0.5431665181309646,0.5442650732252009,0.6018961332866646,0.7358812077646223) [ #3 #9 #12 ]
	  #15: (0.4481120924007082,0.10951105343777945,0.5415019914420327,0.5965706421393199,0.42417189944286104,0.22235097984661129,0.3143914900199383) [ #2 #3 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 16.947598192
	Number of best updates: 18

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 125808

	Init times:
		Bottleneck Steiner distances: 0.000022647
		Smallest sphere distances: 0.000001336
		Sorting: 0.000003743
		Total init time: 0.000038819

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000758, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003649, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032287, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000364417, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004824629, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.065316038, bsd pruned: 0, ss pruned: 20790

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 114345
	Total number iterations 2087400 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 18.255280073461893
	Total time: 16.814727366
	Avarage time pr. topology: 0.000147052

Data for the geometric median iteration:
	Total time: 15.923122997
	Total steps: 73432209
	Average number of steps pr. geometric median: 91.74266974007234
	Average time pr. step: 0.00000021684112753573845
	Average time pr. geometric median: 0.00001989358394957616

