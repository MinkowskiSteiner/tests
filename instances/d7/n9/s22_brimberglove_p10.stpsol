All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 10.122456016750181
MST len: 11.8431307769
Steiner Ratio: 0.8547111576690523
Solution Steiner tree:
	Terminals:
	  #0: (0.8801244962,0.3980362855,0.2372260202,0.3408740076,0.5842591182,0.5664524332,0.0139149264) [ #9 ]
	  #1: (0.2666000273,0.563644016,0.4461416623,0.7561149335,0.1936676377,0.5023851071,0.0234475681) [ #7 ]
	  #2: (0.4161510414,0.7530732251,0.4105919424,0.3546751846,0.5433330655,0.3457467106,0.1582262498) [ #13 ]
	  #4: (0.4607495118,0.1862228323,0.3292264125,0.6730409189,0.520684405,0.2885274982,0.0973462291) [ #11 ]
	  #6: (0.1250572825,0.8931799414,0.0834255573,0.9698017333,0.4631831755,0.5185416162,0.7034413422) [ #15 ]
	  #8: (0.8650269224,0.5663939652,0.6338536677,0.0191505631,0.5234706907,0.8290278501,0.414524392) [ #9 ]
	  #10: (0.612291585,0.0597879244,0.1511060517,0.7373488675,0.9529678663,0.234531609,0.7071506002) [ #11 ]
	  #12: (0.1049435228,0.8549799313,0.1112611555,0.3715435501,0.4186239473,0.5574028178,0.1276584832) [ #13 ]
	  #14: (0.9385241437,0.7902598408,0.9351547686,0.8035510656,0.3566538055,0.5690084359,0.8227016287) [ #15 ]
	Steiner points:
	  #3: (0.46074951181007534,0.7239300615351647,0.32922641250000567,0.3602979730201361,0.520684405,0.5185416162,0.1582262498) [ #13 #9 #5 ]
	  #5: (0.46074951181007534,0.7239300615351646,0.32922641250000567,0.7308349561843405,0.520684405,0.5185416162,0.1582262498) [ #7 #11 #3 ]
	  #7: (0.4607495118100754,0.7239300615351646,0.32922641250000567,0.7308349561843405,0.4276733855013451,0.5185416162,0.1582262498) [ #5 #1 #15 ]
	  #9: (0.8650269224,0.5663939652,0.32922641250000567,0.3602979730201361,0.5234706907,0.5664524332,0.1582262498) [ #3 #8 #0 ]
	  #11: (0.46074951181007534,0.18622283230000003,0.32922641250000567,0.7373488675,0.520684405,0.2885274982,0.1582262498) [ #10 #5 #4 ]
	  #13: (0.46074951179124785,0.7530732251,0.32922641250000567,0.3602979730201361,0.520684405,0.5185416162,0.1582262498) [ #12 #3 #2 ]
	  #15: (0.4607495118100753,0.7902598408000001,0.32922641250000567,0.8035510656,0.42767338550134504,0.5185446962622339,0.7034413422) [ #6 #7 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 5.399886845
	Number of best updates: 16

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 146598

	Init times:
		Bottleneck Steiner distances: 0.000030236
		Smallest sphere distances: 0.000006897
		Sorting: 0.000031219
		Total init time: 0.000070794

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002164, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000010848, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000099099, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001126467, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.015005843, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.232988937, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 135135
	Total number iterations 276078 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.042979242979243
	Total time: 4.914788871
	Avarage time pr. topology: 0.000036369

Data for the geometric median iteration:
	Total time: 4.030337291
	Total steps: 5262682
	Average number of steps pr. geometric median: 5.563412249126535
	Average time pr. step: 0.000000765833331939874
	Average time pr. geometric median: 0.000004260646539703683

