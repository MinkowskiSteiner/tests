All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 10.276922720799494
MST len: 12.6126965465
Steiner Ratio: 0.8148077362291983
Solution Steiner tree:
	Terminals:
	  #0: (0.1199451648,0.215530593,0.8886432843,0.9835675461,0.5171862247,0.9135672198,0.3485602547) [ #9 ]
	  #1: (0.2825672064,0.2314274824,0.4842886475,0.3893124645,0.9920990551,0.5659609305,0.9402686734) [ #11 ]
	  #2: (0.2188497932,0.4543959207,0.5186540231,0.5738417281,0.5552310401,0.7285462551,0.6288739409) [ #3 ]
	  #4: (0.8636223808,0.3016556312,0.0249239351,0.3649927091,0.7653809822,0.3178602631,0.1357283928) [ #13 ]
	  #6: (0.5567509437,0.309231437,0.9220288228,0.7756007373,0.7636273577,0.4406828454,0.3494424649) [ #7 ]
	  #8: (0.106745258,0.7606715722,0.0816722829,0.5509562085,0.5646518891,0.743270507,0.9817601498) [ #9 ]
	  #10: (0.3188583978,0.1692291001,0.9783164058,0.1149865278,0.7529449192,0.2530828441,0.9445846062) [ #11 ]
	  #12: (0.7961281304,0.5837158191,0.2747664383,0.8295980784,0.9136800267,0.9654021426,0.2520847601) [ #13 ]
	  #14: (0.033469948,0.3299642076,0.6906357043,0.4224866822,0.2062651386,0.2501284509,0.6365585242) [ #15 ]
	Steiner points:
	  #3: (0.2188497931998081,0.34418688038984185,0.5186540231,0.5509562085,0.5646518891,0.7432704198227827,0.6288739409) [ #2 #9 #15 ]
	  #5: (0.3188583978,0.309231437,0.5186540218787621,0.4301734234628133,0.7529449192,0.4406828454,0.6314354686666657) [ #11 #7 #15 ]
	  #7: (0.5567509437,0.309231437,0.5186540218787621,0.4301734234628133,0.7636273577,0.4406828454,0.3494424649) [ #6 #5 #13 ]
	  #9: (0.10674526002611696,0.34418688038984185,0.5186540231,0.5509562085,0.5646518891,0.743270507,0.6288739409) [ #8 #3 #0 ]
	  #11: (0.3188583978,0.2314274824,0.5186540218787621,0.3893124645,0.7529449192,0.4406828454,0.9402686734) [ #5 #10 #1 ]
	  #13: (0.7961281304,0.309231437,0.2747664383,0.4301734234628133,0.7636273577,0.4406828454,0.2520847601) [ #12 #7 #4 ]
	  #15: (0.2188497931998081,0.3299642076,0.5186566466815917,0.4301734234628133,0.5646518891,0.4406828454,0.6314354686666657) [ #3 #5 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 4.9032619870000005
	Number of best updates: 23

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 136203

	Init times:
		Bottleneck Steiner distances: 0.000036764
		Smallest sphere distances: 0.000005968
		Sorting: 0.000021679
		Total init time: 0.000066139

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001748, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000010474, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000097857, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001118618, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.015116299, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.218195754, bsd pruned: 0, ss pruned: 10395

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 124740
	Total number iterations 253577 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0328443161776497
	Total time: 4.448309558
	Avarage time pr. topology: 0.000035659

Data for the geometric median iteration:
	Total time: 3.636101973
	Total steps: 4753714
	Average number of steps pr. geometric median: 5.444139810806478
	Average time pr. step: 0.0000007648970832069409
	Average time pr. geometric median: 0.000004164206661856662

