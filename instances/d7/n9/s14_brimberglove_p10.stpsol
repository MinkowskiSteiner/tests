All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 10.770820308251018
MST len: 13.6294940703
Steiner Ratio: 0.7902582629036604
Solution Steiner tree:
	Terminals:
	  #0: (0.6981957097,0.2743996956,0.1229939042,0.1415459719,0.6331634999,0.8501201872,0.1499063168) [ #9 ]
	  #1: (0.7037021051,0.7905814442,0.5198903161,0.7032006042,0.0538963489,0.7357381767,0.1634226517) [ #3 ]
	  #2: (0.9994984996,0.2633149048,0.2158478607,0.460222048,0.0529571777,0.5661688692,0.0288339364) [ #3 ]
	  #4: (0.839495378,0.391475477,0.4961270823,0.6485379798,0.1537148399,0.2108905191,0.9520467748) [ #13 ]
	  #6: (0.7790316733,0.429230639,0.9945288911,0.6185270509,0.820706116,0.4906559729,0.2670650302) [ #11 ]
	  #8: (0.6024998616,0.746623855,0.6549854328,0.0995092923,0.7305007729,0.581988618,0.1856662101) [ #9 ]
	  #10: (0.609109687,0.1922258954,0.9640689543,0.312811792,0.9828073401,0.4839592699,0.0160123957) [ #11 ]
	  #12: (0.9744209559,0.7015464919,0.2191118045,0.6726166656,0.9759461875,0.3421057087,0.8141626375) [ #13 ]
	  #14: (0.1068535266,0.3019070454,0.1922565881,0.7093533882,0.0485309004,0.8472420214,0.8088626805) [ #15 ]
	Steiner points:
	  #3: (0.7037021051,0.391475477,0.410679877141861,0.6485379798000002,0.0538963489,0.5661688692000003,0.1634226517) [ #1 #2 #5 ]
	  #5: (0.7037021051,0.391475477,0.41067987714186105,0.6485379798000002,0.11866108726842305,0.5661688692000003,0.2670650302) [ #3 #7 #15 ]
	  #7: (0.6024998616,0.3396461001553923,0.41067987714186105,0.6485379798000002,0.6331634999,0.5661688692000003,0.2670650302) [ #5 #9 #11 ]
	  #9: (0.6024998616,0.3396461001553923,0.41067987714186105,0.1415459719,0.6331634999,0.581988618,0.1856662101) [ #0 #8 #7 ]
	  #11: (0.609109687,0.3396461001553923,0.9640689543,0.6185270509,0.820706116,0.4906559729,0.2670650302) [ #10 #6 #7 ]
	  #13: (0.8394953779999998,0.391475477,0.33787211412790735,0.6485379801265864,0.1537148399,0.34210570870000007,0.8141626375) [ #4 #12 #15 ]
	  #15: (0.7037021050999999,0.3914727436718056,0.33787211412790735,0.6485398358048023,0.11866108726842305,0.5661688692000003,0.8088626805) [ #5 #13 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 4.653103927
	Number of best updates: 16

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 125808

	Init times:
		Bottleneck Steiner distances: 0.000044689
		Smallest sphere distances: 0.000017093
		Sorting: 0.000026452
		Total init time: 0.000091922

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002267, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000011126, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000107651, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.00114018, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.015221644, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.206153002, bsd pruned: 0, ss pruned: 20790

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 114345
	Total number iterations 234921 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.054492981765709
	Total time: 4.218408499
	Avarage time pr. topology: 0.000036891

Data for the geometric median iteration:
	Total time: 3.47193945
	Total steps: 4545154
	Average number of steps pr. geometric median: 5.6784967797954815
	Average time pr. step: 0.0000007638771865595753
	Average time pr. geometric median: 0.0000043376741440377804

Data for the geometric median step function:
	Total number of fixed points encountered: 21951178

