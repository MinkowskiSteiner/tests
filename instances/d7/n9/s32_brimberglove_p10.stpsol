All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 9.607308340129489
MST len: 12.106059815700002
Steiner Ratio: 0.7935949835362656
Solution Steiner tree:
	Terminals:
	  #0: (0.883700354,0.5241935018,0.4808989346,0.7519973194,0.4708642077,0.0976900245,0.0699699684) [ #7 ]
	  #1: (0.5114069565,0.8251469377,0.5661086378,0.4243843828,0.5732345397,0.7447643731,0.6941410954) [ #5 ]
	  #2: (0.6775168025,0.4244183676,0.8089938242,0.7790788094,0.2599427934,0.6323595734,0.1754854299) [ #13 ]
	  #4: (0.4496756841,0.6990365287,0.6652369316,0.7215187111,0.8656997224,0.5431189162,0.5266172777) [ #5 ]
	  #6: (0.8682969659,0.9466707064,0.6167910903,0.3179726495,0.6457072346,0.2820280214,0.0394913601) [ #15 ]
	  #8: (0.8488178932,0.1993391929,0.0828401046,0.5263346953,0.6237575606,0.8918339288,0.3054135047) [ #11 ]
	  #10: (0.7210808409,0.0502029169,0.8113621514,0.9132866975,0.7154636065,0.5759032646,0.8231241507) [ #11 ]
	  #12: (0.1922058562,0.6652606896,0.7645411136,0.9098374536,0.1333542867,0.6234359283,0.2597159544) [ #13 ]
	  #14: (0.9129774263,0.7480876026,0.1786557348,0.2697567126,0.1478463012,0.3054385443,0.1172210561) [ #15 ]
	Steiner points:
	  #3: (0.7210808409,0.6652606895774891,0.5661071840167055,0.5267980346289367,0.47086420770000004,0.5431189162,0.20356227139999777) [ #13 #7 #9 ]
	  #5: (0.4496756841144049,0.6990365287,0.5661071840167055,0.5263346952999959,0.5732339621618247,0.5431189162,0.5266172777) [ #9 #1 #4 ]
	  #7: (0.8831904526170036,0.6652606895774891,0.5661071840167055,0.5267980346289367,0.47086420770000004,0.28983152903333337,0.0699699684) [ #3 #0 #15 ]
	  #9: (0.7210808409,0.6652606895774891,0.5661071840167055,0.5263346952999959,0.5732339621618247,0.5431189162,0.3054135047) [ #11 #5 #3 ]
	  #11: (0.7210808409,0.1993391929,0.5661071840167055,0.5263346953,0.6237575606,0.5759032646,0.3054135047) [ #10 #8 #9 ]
	  #13: (0.7210808409,0.6652606895765438,0.7645411136,0.7790788094,0.2599427934,0.6234359283,0.20356227139999777) [ #12 #3 #2 ]
	  #15: (0.8831904526170036,0.7480876026,0.5661071840167056,0.31797264950000004,0.47086420770000004,0.28983152903333337,0.0699699684) [ #6 #7 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 4.287390623
	Number of best updates: 17

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 115413

	Init times:
		Bottleneck Steiner distances: 0.000038331
		Smallest sphere distances: 0.000006008
		Sorting: 0.000031223
		Total init time: 0.000077372

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000004078, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000010633, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000098489, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.00112393, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.015052644, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.190147512, bsd pruned: 10395, ss pruned: 20790

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 103950
	Total number iterations 212951 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0485906685906685
	Total time: 3.890519876
	Avarage time pr. topology: 0.000037426

Data for the geometric median iteration:
	Total time: 3.208588402
	Total steps: 4202662
	Average number of steps pr. geometric median: 5.775664124235552
	Average time pr. step: 0.0000007634657276745073
	Average time pr. geometric median: 0.000004409521613413042

