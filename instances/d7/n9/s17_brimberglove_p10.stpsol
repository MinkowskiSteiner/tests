All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 10.885171252622323
MST len: 13.5793718342
Steiner Ratio: 0.8015960815807206
Solution Steiner tree:
	Terminals:
	  #0: (0.4860870128,0.6620165676,0.9043156765,0.6258278161,0.215128048,0.2804932088,0.3381360212) [ #5 ]
	  #1: (0.6031280205,0.3763264294,0.7351060159,0.8217135835,0.0627080598,0.4779169375,0.2268656857) [ #3 ]
	  #2: (0.9706272744,0.2301676097,0.3574414162,0.5424212746,0.2320200839,0.4801497965,0.4595757073) [ #15 ]
	  #4: (0.0860349122,0.7076993565,0.5674642797,0.572121925,0.3697159241,0.4717799562,0.1979497407) [ #7 ]
	  #6: (0.584843972,0.752273165,0.5360857619,0.3981009128,0.516434503,0.0732187028,0.8383779218) [ #11 ]
	  #8: (0.8132569412,0.7641613385,0.5371329414,0.440277009,0.726414976,0.6913943327,0.1680129702) [ #9 ]
	  #10: (0.2036475247,0.219413845,0.8945567677,0.1742747986,0.4495814547,0.2519981839,0.7166960732) [ #11 ]
	  #12: (0.2428494786,0.7646130355,0.0063908915,0.8459774991,0.1409394644,0.7414969079,0.6676910821) [ #13 ]
	  #14: (0.5717940003,0.0018524737,0.1227083798,0.9171544332,0.8540148287,0.22754956,0.1078885724) [ #15 ]
	Steiner points:
	  #3: (0.6031280205,0.37632642939999994,0.5360856572074931,0.6258278161,0.2320200838999999,0.4717799562,0.22686568569999996) [ #5 #1 #15 ]
	  #5: (0.4860870128,0.6620165676,0.5360856572074931,0.6258278161,0.23202008389999984,0.4717799562,0.22686568569999996) [ #7 #3 #0 ]
	  #7: (0.4860870128,0.6620165676,0.5360856572074931,0.572121925,0.3697159241,0.4717799562,0.1979497407) [ #5 #4 #9 ]
	  #9: (0.4860870128,0.6620165676,0.5360856572074931,0.440277009,0.3912694901337062,0.4717799562,0.1979497407) [ #7 #8 #13 ]
	  #11: (0.40500783473333335,0.6962153902333332,0.5360857619,0.3981009128,0.516434503,0.2519981839,0.7166960732000001) [ #10 #6 #13 ]
	  #13: (0.40500783473333335,0.6962153902333332,0.5360854533697921,0.44027700899999994,0.3912694901337062,0.4717799562,0.6676910821) [ #9 #11 #12 ]
	  #15: (0.6031280205,0.23016760969999997,0.35744141620000003,0.6258278161,0.2320248293070998,0.4717799562,0.22686568569999993) [ #2 #3 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 5.635107361
	Number of best updates: 24

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 146598

	Init times:
		Bottleneck Steiner distances: 0.000092338
		Smallest sphere distances: 0.000015603
		Sorting: 0.000083611
		Total init time: 0.00019797

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000003263, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000012465, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000102443, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001165771, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.015679863, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.241269495, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 135135
	Total number iterations 276671 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0473674473674475
	Total time: 5.127939895
	Avarage time pr. topology: 0.000037946

Data for the geometric median iteration:
	Total time: 4.226982927
	Total steps: 5392069
	Average number of steps pr. geometric median: 5.700192928764357
	Average time pr. step: 0.000000783925971088278
	Average time pr. geometric median: 0.000004468529277072134

Data for the geometric median step function:
	Total number of fixed points encountered: 26705233

