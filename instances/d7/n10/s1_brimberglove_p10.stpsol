All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 11.967338441111362
MST len: 14.5633327154
Steiner Ratio: 0.821744491798673
Solution Steiner tree:
	Terminals:
	  #0: (0.9493270754,0.5259953502,0.0860558479,0.192213846,0.663226927,0.8902326025,0.3488929352) [ #9 ]
	  #1: (0.7682295948,0.2777747108,0.5539699558,0.4773970519,0.6288709248,0.3647844728,0.5134009102) [ #5 ]
	  #2: (0.8509197868,0.2666657494,0.5397603407,0.3752069764,0.7602487364,0.5125353641,0.6677237608) [ #3 ]
	  #4: (0.2428867706,0.1372315768,0.8041767542,0.1566790893,0.4009443942,0.1297904468,0.108808802) [ #13 ]
	  #6: (0.5242871901,0.493582987,0.9727750239,0.2925167844,0.7713576978,0.5267449792,0.7699138363) [ #7 ]
	  #8: (0.4002286221,0.891529452,0.283314746,0.3524583473,0.80772452,0.919026474,0.0697552762) [ #15 ]
	  #10: (0.998924518,0.2182569053,0.5129323944,0.8391122347,0.6126398326,0.2960316177,0.6375522677) [ #17 ]
	  #12: (0.0641713208,0.0200230489,0.4577017373,0.0630958383,0.2382799542,0.9706341317,0.9022080735) [ #13 ]
	  #14: (0.9522297252,0.916195068,0.635711728,0.7172969294,0.1416025554,0.6069688763,0.0163005716) [ #15 ]
	  #16: (0.8401877172,0.3943829268,0.7830992238,0.7984400335,0.9116473579,0.1975513693,0.3352227557) [ #17 ]
	Steiner points:
	  #3: (0.8509197868,0.2777747107999998,0.6175253060449829,0.3752069764,0.7602487363999999,0.5125353641,0.5133996717986503) [ #11 #2 #7 ]
	  #5: (0.7682295948,0.2777747107999999,0.6175253060449829,0.4774023478593489,0.6288709248,0.3647844728,0.5133996717986503) [ #13 #11 #1 ]
	  #7: (0.8509197868,0.49358298700000003,0.6175253060449829,0.3752069764,0.7713576978,0.5267449792,0.5133996717986503) [ #3 #6 #9 ]
	  #9: (0.8509197868,0.5259953502,0.4007804066666675,0.3524586908441774,0.7713576978,0.6593388581005597,0.3488929352) [ #0 #15 #7 ]
	  #11: (0.8509197868,0.2777747107999999,0.6175253060449829,0.4774023478593489,0.6288709248,0.3647844728,0.5133996717986503) [ #3 #5 #17 ]
	  #13: (0.2428867706,0.1372315768,0.6175253060449829,0.15667908929999996,0.40094439419999994,0.3647844728,0.5133996717986503) [ #5 #12 #4 ]
	  #15: (0.8509197868,0.891529452,0.4007804066666675,0.3524586908441774,0.7713576978,0.6593388581005597,0.0697552762) [ #14 #9 #8 ]
	  #17: (0.8509197868,0.2777747107999999,0.6175253060449829,0.7984400335,0.6288709248,0.29603161769999997,0.5133996717986503) [ #10 #11 #16 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 78.963464515
	Number of best updates: 26

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 2038488

	Init times:
		Bottleneck Steiner distances: 0.000038023
		Smallest sphere distances: 0.000009096
		Sorting: 0.000039976
		Total init time: 0.000098642

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002397, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000011229, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00009939, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.00113087, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.014992578, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.231696516, bsd pruned: 0, ss pruned: 0
		Level 7 - Time: 3.8522825320000003, bsd pruned: 0, ss pruned: 135135

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 1891890
	Total number iterations 3846027 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0329020186163045
	Total time: 71.213695957
	Avarage time pr. topology: 0.00003764

Data for the geometric median iteration:
	Total time: 57.267910386
	Total steps: 74421401
	Average number of steps pr. geometric median: 4.91713319749034
	Average time pr. step: 0.0000007695086308036582
	Average time pr. geometric median: 0.0000037837764342800053

