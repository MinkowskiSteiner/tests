All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.824279574302684
MST len: 5.412827770187233
Steiner Ratio: 0.8912678879002665
Solution Steiner tree:
	Terminals:
	  #0: (0.1610254427,0.8566360394,0.0970141232,0.4512588286,0.1844230174,0.3255779349,0.5106765579) [ #13 ]
	  #1: (0.3077861812,0.506227026,0.5700069734,0.1043102486,0.1936842861,0.4715504742,0.0729662385) [ #7 ]
	  #2: (0.2170182561,0.784748353,0.4766765621,0.2870572444,0.5252522843,0.8965011402,0.6247255405) [ #5 ]
	  #4: (0.7965240673,0.6874572605,0.9015435013,0.9686559904,0.617568895,0.5169671907,0.3123343211) [ #11 ]
	  #6: (0.2902333854,0.3277869785,0.2285638117,0.0594177293,0.2219586108,0.6696780285,0.3323929949) [ #7 ]
	  #8: (0.8112531811,0.9885176653,0.3853005596,0.0282714367,0.7732660178,0.8619771217,0.3153286811) [ #9 ]
	  #10: (0.4063816287,0.9952559634,0.8430695528,0.9825714156,0.2686294314,0.9196610394,0.6276440698) [ #11 ]
	  #12: (0.5761897869,0.273373468,0.0765914861,0.6450726547,0.0391567499,0.586565987,0.9423629041) [ #13 ]
	Steiner points:
	  #3: (0.3214283779978391,0.6131581161914309,0.31846000346958764,0.30365198806972105,0.29200582669835345,0.6231435559308642,0.47533884586087816) [ #5 #7 #13 ]
	  #5: (0.35053839244440066,0.7616993140674226,0.4552787021979484,0.32929678814088287,0.47135991451668685,0.7933831229007636,0.530864852163187) [ #3 #9 #2 ]
	  #7: (0.3048596910866821,0.4659769820137755,0.34215862586344725,0.15120176025468274,0.23817479417176615,0.6056242183991842,0.31593425825382243) [ #6 #1 #3 ]
	  #9: (0.4587620510044612,0.8127208635330948,0.5020814911788932,0.3709435005897959,0.5143532674316657,0.7972581626691374,0.48924482767523747) [ #5 #8 #11 ]
	  #11: (0.5419507272194742,0.8443461242851988,0.7548064543760213,0.7879379592464764,0.45178438446878527,0.758459353011303,0.48817617257154117) [ #4 #9 #10 ]
	  #13: (0.31471030985975235,0.63122835741144,0.20959004130768363,0.4064556408529024,0.21640559555930983,0.5252455653294966,0.5645965024080616) [ #0 #3 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 1.504556697
	Number of best updates: 12

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 11463

	Init times:
		Bottleneck Steiner distances: 0.000022565
		Smallest sphere distances: 0.000001259
		Sorting: 0.000002749
		Total init time: 0.000028078

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000769, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003727, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032508, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000358944, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.00481955, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 10395
	Total number iterations 198048 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 19.05223665223665
	Total time: 1.494273519
	Avarage time pr. topology: 0.000143749

Data for the geometric median iteration:
	Total time: 1.421044338
	Total steps: 6564290
	Average number of steps pr. geometric median: 105.24755491422158
	Average time pr. step: 0.0000002164810418186887
	Average time pr. geometric median: 0.000022784100336700337

