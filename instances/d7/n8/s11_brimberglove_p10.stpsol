All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 9.01686945407697
MST len: 11.103846062099999
Steiner Ratio: 0.8120492128266832
Solution Steiner tree:
	Terminals:
	  #0: (0.7910995301,0.1739375839,0.0170635157,0.4592865964,0.7046087811,0.729890453,0.0711031715) [ #11 ]
	  #1: (0.1403875533,0.8967377799,0.3141340824,0.3612824978,0.7296536866,0.7182401273,0.4365231644) [ #3 ]
	  #2: (0.3687719635,0.5168204054,0.9570931466,0.3607239967,0.5789050197,0.7321704266,0.2410501816) [ #9 ]
	  #4: (0.9263453185,0.5264996991,0.3097808907,0.5022105041,0.7537675108,0.4059770589,0.4484664097) [ #5 ]
	  #6: (0.4371118506,0.8601203439,0.1809798303,0.3634571686,0.3866200426,0.490760721,0.8656676732) [ #13 ]
	  #8: (0.1956754286,0.1610363345,0.9483695021,0.9867749587,0.3349739184,0.9654330178,0.4460615546) [ #9 ]
	  #10: (0.9778233603,0.0999500035,0.4717727618,0.3465953233,0.6167704093,0.4288659084,0.70731932) [ #11 ]
	  #12: (0.2208949445,0.8329159067,0.4041060449,0.0752406661,0.2481696737,0.3817098766,0.0352495974) [ #13 ]
	Steiner points:
	  #3: (0.31107311853335023,0.5264996991,0.3141340824,0.3607239967,0.5789036441651291,0.7182401221473044,0.43652281696469925) [ #1 #9 #7 ]
	  #5: (0.8533408068333335,0.5264996991,0.3097808907,0.4217228385695346,0.6753293238332562,0.490760721,0.43652281696469897) [ #11 #7 #4 ]
	  #7: (0.3650395485666667,0.5264996991,0.3097808907,0.3607239967,0.5789036441651291,0.490760721,0.43652281696469897) [ #3 #5 #13 ]
	  #9: (0.31107311853335023,0.5264996991,0.9483695021,0.3607239967,0.5789036441651291,0.7182401221473044,0.43652281696469925) [ #8 #3 #2 ]
	  #11: (0.8533408068333335,0.1739375839,0.3097808907,0.4217228385695346,0.6753293238332562,0.490760721,0.43652281696469897) [ #10 #5 #0 ]
	  #13: (0.3650395485666667,0.8329159067,0.3097808906999999,0.3607239967,0.3866200426,0.4907573931338948,0.43652281696469897) [ #6 #7 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.379669022
	Number of best updates: 16

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 11463

	Init times:
		Bottleneck Steiner distances: 0.000022713
		Smallest sphere distances: 0.000006444
		Sorting: 0.000021132
		Total init time: 0.000053375

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001929, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000011196, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000099688, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001141741, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.01515186, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 10395
	Total number iterations 21135 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.033189033189033
	Total time: 0.346452895
	Avarage time pr. topology: 0.000033328

Data for the geometric median iteration:
	Total time: 0.288526238
	Total steps: 381756
	Average number of steps pr. geometric median: 6.1208273208273205
	Average time pr. step: 0.0000007557870419849328
	Average time pr. geometric median: 0.000004626041975308642

Data for the geometric median step function:
	Total number of fixed points encountered: 1767761

