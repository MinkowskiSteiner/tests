All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 10.275722835162869
MST len: 13.3778525472
Steiner Ratio: 0.7681145235311768
Solution Steiner tree:
	Terminals:
	  #0: (0.7761541152,0.6623876005,0.1703382573,0.3191112491,0.3110007436,0.691104038,0.1034285073) [ #3 ]
	  #1: (0.8634107573,0.3815790147,0.3222648391,0.6395648721,0.0439666151,0.4926030964,0.9586761212) [ #9 ]
	  #2: (0.4166978609,0.2226562315,0.5511224272,0.0462225136,0.6177704891,0.4252223039,0.3870529828) [ #7 ]
	  #4: (0.2473998085,0.3098868776,0.9831179767,0.5296626759,0.9438327159,0.87826215,0.7782872272) [ #13 ]
	  #6: (0.9779993249,0.8022396713,0.6132100036,0.1134380932,0.7647564228,0.4129372162,0.5164472929) [ #11 ]
	  #8: (0.4722914819,0.0229840363,0.4052342835,0.8889893428,0.2456402682,0.9563567112,0.9352118568) [ #9 ]
	  #10: (0.8498845724,0.2185718884,0.16160391,0.0972843804,0.5284587664,0.1447218867,0.6269470568) [ #11 ]
	  #12: (0.1354387687,0.9625167521,0.799727213,0.4030091993,0.0851281491,0.8056346727,0.6451566176) [ #13 ]
	Steiner points:
	  #3: (0.4722914819,0.3815790147,0.5750254969529158,0.3191112491,0.28403388383333344,0.6911040379999999,0.5164472929) [ #7 #5 #0 ]
	  #5: (0.4722914819,0.3815790147,0.5750254969529158,0.48744485036675095,0.28403388383333344,0.7547366303398856,0.7339103573333337) [ #9 #3 #13 ]
	  #7: (0.4722914819,0.3815790147,0.5511224272001615,0.10805352226666667,0.6177704891,0.4252223039,0.5164472929) [ #3 #11 #2 ]
	  #9: (0.4722914819,0.3815790147,0.4052342835,0.6395648721,0.2456402682,0.7547366303398856,0.9352118567999999) [ #5 #8 #1 ]
	  #11: (0.8498845724,0.38157901470000005,0.5511224272001615,0.10805352226666667,0.6177704891,0.4129372162,0.5164473455903682) [ #6 #7 #10 ]
	  #13: (0.24739980849999998,0.38157901470000005,0.799727213,0.48744485036675095,0.28403388383333344,0.8056346727,0.7339103573333337) [ #4 #5 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.370590085
	Number of best updates: 14

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 11463

	Init times:
		Bottleneck Steiner distances: 0.000032711
		Smallest sphere distances: 0.000005335
		Sorting: 0.000019451
		Total init time: 0.000060145

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001599, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000010625, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000100011, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001130991, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.015199579, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 10395
	Total number iterations 21135 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.033189033189033
	Total time: 0.337288423
	Avarage time pr. topology: 0.000032447

Data for the geometric median iteration:
	Total time: 0.279228092
	Total steps: 366986
	Average number of steps pr. geometric median: 5.884014750681417
	Average time pr. step: 0.0000007608685126953071
	Average time pr. geometric median: 0.000004476961552028218

Data for the geometric median step function:
	Total number of fixed points encountered: 1732538

