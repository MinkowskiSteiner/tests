All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.758606034756225
MST len: 5.367230028436954
Steiner Ratio: 0.8866037061098399
Solution Steiner tree:
	Terminals:
	  #0: (0.6132460584,0.5875373467,0.2723092908,0.7436478691,0.6444432082,0.236034955,0.1276740856) [ #13 ]
	  #1: (0.215467854,0.2348691641,0.1913313205,0.4307993098,0.5479775013,0.0151233026,0.0380932228) [ #3 ]
	  #2: (0.5074669032,0.561359823,0.6648593674,0.4017881259,0.1900027982,0.7689555445,0.3776398615) [ #11 ]
	  #4: (0.861473049,0.2765600068,0.3932821464,0.0769409025,0.5114291709,0.5846134669,0.5077402124) [ #5 ]
	  #6: (0.0594066722,0.5997367695,0.5458334356,0.0842270214,0.5216057671,0.066293479,0.7319970623) [ #9 ]
	  #8: (0.1304018107,0.0569058615,0.9637256646,0.384026217,0.8656925191,0.3785816228,0.6610816175) [ #9 ]
	  #10: (0.0248203492,0.9218689981,0.5204600438,0.6477700405,0.0916402913,0.5212438672,0.540312229) [ #11 ]
	  #12: (0.8943212232,0.6286429756,0.1040961771,0.975851736,0.6714702508,0.5076044628,0.0156422849) [ #13 ]
	Steiner points:
	  #3: (0.4145569046802441,0.4211437148901786,0.3266023252698612,0.508873418398855,0.5684922148759844,0.2110635552092383,0.2040321023747862) [ #1 #7 #13 ]
	  #5: (0.4934194531312469,0.44425809839712166,0.5044036169711649,0.32172984786286307,0.43473017672319425,0.48409708981822774,0.4355430486501494) [ #4 #7 #11 ]
	  #7: (0.3981313073154892,0.42434846022506395,0.493301334561477,0.35495361162488553,0.5146957488877895,0.35500107896546373,0.4178900705033944) [ #5 #3 #9 ]
	  #9: (0.22957477042017851,0.39487618618846293,0.6183892419808189,0.2763068042215863,0.5978484259925656,0.2694182726847143,0.5730243920541431) [ #8 #6 #7 ]
	  #11: (0.4272174635619558,0.5812137746435768,0.5917583872528297,0.415285301955406,0.2514363146762883,0.6405365790539134,0.4213919056833297) [ #2 #5 #10 ]
	  #13: (0.6132461143766059,0.587536566577903,0.2723088864108812,0.7436475099181763,0.6444428872885493,0.23603596765006823,0.12767404700943535) [ #0 #3 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 1.070561495
	Number of best updates: 10

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 8628

	Init times:
		Bottleneck Steiner distances: 0.00002028
		Smallest sphere distances: 0.000001758
		Sorting: 0.000002959
		Total init time: 0.000026916

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000842, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003607, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032291, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000360402, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.003736468, bsd pruned: 0, ss pruned: 2835

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 7560
	Total number iterations 135809 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 17.96415343915344
	Total time: 1.06190271
	Avarage time pr. topology: 0.000140463

Data for the geometric median iteration:
	Total time: 1.011617513
	Total steps: 4661551
	Average number of steps pr. geometric median: 102.76787918871253
	Average time pr. step: 0.0000002170130741892559
	Average time pr. geometric median: 0.000022301973390652558

