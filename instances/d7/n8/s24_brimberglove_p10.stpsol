All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 9.591658293495364
MST len: 11.833771270500002
Steiner Ratio: 0.810532675868603
Solution Steiner tree:
	Terminals:
	  #0: (0.908120057,0.6663319537,0.22706335,0.1750125909,0.1655713786,0.0504909251,0.4797119067) [ #9 ]
	  #1: (0.2656748631,0.393328767,0.6713674793,0.0235927571,0.7121990182,0.0244992781,0.4822485077) [ #7 ]
	  #2: (0.2668925339,0.4992394254,0.8234275756,0.3046993158,0.100103484,0.3428378414,0.1916555726) [ #3 ]
	  #4: (0.0877142293,0.8987790253,0.2479340957,0.6666313995,0.8988103708,0.2810884273,0.6454682083) [ #13 ]
	  #6: (0.7579178944,0.3188702512,0.3531317987,0.4586557506,0.9458669317,0.9701537522,0.1303727818) [ #11 ]
	  #8: (0.2529672176,0.239600985,0.0523553547,0.3122986417,0.0491195042,0.5618327174,0.5143123132) [ #9 ]
	  #10: (0.652541518,0.4553778225,0.7105784624,0.7402557478,0.3541568473,0.9585125581,0.4068871468) [ #11 ]
	  #12: (0.0593314241,0.8095185197,0.5094773627,0.2020136715,0.8590005528,0.1044992358,0.7127510368) [ #13 ]
	Steiner points:
	  #3: (0.25296721783553167,0.4992394254,0.5155179862337196,0.3046993158,0.1655713786002501,0.28108842730000005,0.48224850778172945) [ #9 #5 #2 ]
	  #5: (0.2656748631,0.4992394254,0.5155179862337196,0.3046993158,0.8348360642525561,0.2810884273,0.48224850778172945) [ #7 #3 #13 ]
	  #7: (0.2656748631,0.393328767,0.5155179862337196,0.3046993158,0.8348360642525561,0.2810884273,0.48224850778172945) [ #11 #5 #1 ]
	  #9: (0.2529672176,0.4992394254,0.22706335,0.3122986417,0.1655713786002501,0.28108842730000005,0.5143123132) [ #3 #8 #0 ]
	  #11: (0.652541518,0.393328767,0.5155179862337196,0.45865575059999997,0.8348360642525561,0.9585125581,0.4068871468) [ #6 #7 #10 ]
	  #13: (0.0877142293,0.8095185197,0.5094773627,0.3046993158,0.8590005528,0.28108573280389093,0.6454682083) [ #4 #5 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.293153401
	Number of best updates: 9

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 8628

	Init times:
		Bottleneck Steiner distances: 0.000024648
		Smallest sphere distances: 0.000006274
		Sorting: 0.00001515
		Total init time: 0.000047947

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002352, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000011176, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000100094, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001176162, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.012083803, bsd pruned: 0, ss pruned: 2835

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 7560
	Total number iterations 15322 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.026719576719577
	Total time: 0.266602977
	Avarage time pr. topology: 0.000035264

Data for the geometric median iteration:
	Total time: 0.223072032
	Total steps: 285218
	Average number of steps pr. geometric median: 6.287874779541446
	Average time pr. step: 0.000000782110638178516
	Average time pr. geometric median: 0.000004917813756613756

