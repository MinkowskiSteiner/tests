All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.299340685543759
MST len: 6.157773234808232
Steiner Ratio: 0.8605936729836062
Solution Steiner tree:
	Terminals:
	  #0: (0.7761541152,0.6623876005,0.1703382573,0.3191112491,0.3110007436,0.691104038,0.1034285073) [ #3 ]
	  #1: (0.8634107573,0.3815790147,0.3222648391,0.6395648721,0.0439666151,0.4926030964,0.9586761212) [ #9 ]
	  #2: (0.4166978609,0.2226562315,0.5511224272,0.0462225136,0.6177704891,0.4252223039,0.3870529828) [ #11 ]
	  #4: (0.2473998085,0.3098868776,0.9831179767,0.5296626759,0.9438327159,0.87826215,0.7782872272) [ #13 ]
	  #6: (0.9779993249,0.8022396713,0.6132100036,0.1134380932,0.7647564228,0.4129372162,0.5164472929) [ #7 ]
	  #8: (0.4722914819,0.0229840363,0.4052342835,0.8889893428,0.2456402682,0.9563567112,0.9352118568) [ #9 ]
	  #10: (0.8498845724,0.2185718884,0.16160391,0.0972843804,0.5284587664,0.1447218867,0.6269470568) [ #11 ]
	  #12: (0.1354387687,0.9625167521,0.799727213,0.4030091993,0.0851281491,0.8056346727,0.6451566176) [ #13 ]
	Steiner points:
	  #3: (0.6881265784691183,0.5270761700113155,0.41774872427145776,0.2926608924145868,0.4595157115858177,0.5649378239583424,0.45659967403404733) [ #7 #5 #0 ]
	  #5: (0.5537432338809818,0.48174201032201147,0.5296082026882798,0.4616789101031308,0.3780376148133111,0.6737266860633171,0.6586873712838553) [ #3 #9 #13 ]
	  #7: (0.7230318943801076,0.5048086332907442,0.44546142211663586,0.2047933590364534,0.5457730389295382,0.47278190450183355,0.4765711263725924) [ #3 #6 #11 ]
	  #9: (0.6439140089351421,0.3242962384942431,0.42192185432791224,0.6388466062901244,0.22251374990765962,0.6831260616359411,0.8400290284562474) [ #8 #5 #1 ]
	  #11: (0.6669334175216091,0.36721359969136513,0.41362739094694356,0.138838701956412,0.5616224438052724,0.38994052220705294,0.48433170728511404) [ #10 #7 #2 ]
	  #13: (0.39144338762270486,0.5592819790328318,0.6876044475870163,0.4619965834562545,0.42671893042653436,0.7473598245802802,0.6803795312794796) [ #4 #5 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 1.428052768
	Number of best updates: 19

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 11463

	Init times:
		Bottleneck Steiner distances: 0.000022785
		Smallest sphere distances: 0.000001112
		Sorting: 0.000002825
		Total init time: 0.00002817

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000912, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003701, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032406, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000359378, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004808416, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 10395
	Total number iterations 171442 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 16.492736892736893
	Total time: 1.417764829
	Avarage time pr. topology: 0.000136389

Data for the geometric median iteration:
	Total time: 1.353923178
	Total steps: 6225564
	Average number of steps pr. geometric median: 99.81664261664261
	Average time pr. step: 0.00000021747799524669574
	Average time pr. geometric median: 0.00002170792332852333

