All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.083950316410993
MST len: 5.650624623646848
Steiner Ratio: 0.8997147492572017
Solution Steiner tree:
	Terminals:
	  #0: (0.1197466637,0.0576706543,0.2857596349,0.6257388632,0.607333408,0.320081121,0.7510317898) [ #3 ]
	  #1: (0.0627508867,0.1330705258,0.1031433517,0.1824975504,0.1907411801,0.3889029866,0.808236414) [ #3 ]
	  #2: (0.5998500383,0.0285220109,0.4814383762,0.729074478,0.3766270487,0.206070881,0.2581979913) [ #13 ]
	  #4: (0.6239873639,0.9691653433,0.0583464685,0.4434102766,0.4754556192,0.1466605273,0.2289445811) [ #9 ]
	  #6: (0.8194229132,0.5062902763,0.0883140588,0.7855343049,0.3411775247,0.8119810777,0.9184383005) [ #11 ]
	  #8: (0.8166331438,0.958641605,0.1473828816,0.0937726004,0.1534282836,0.0076622083,0.4441131705) [ #9 ]
	  #10: (0.1292244392,0.3481050377,0.7246325052,0.5291235137,0.6861238385,0.9269996448,0.8449453608) [ #11 ]
	  #12: (0.2771394566,0.1947866791,0.8602793272,0.35034057,0.4464217548,0.0208598026,0.0373252058) [ #13 ]
	Steiner points:
	  #3: (0.20561295397804552,0.16801397193895845,0.2771794393712206,0.5054338116011672,0.4670764699364711,0.3896303544823518,0.7179736026039568) [ #7 #0 #1 ]
	  #5: (0.45044701302837303,0.3340611808430942,0.3765653114590777,0.52079328004003,0.42413377935554764,0.2735082125327404,0.4295938308510885) [ #9 #7 #13 ]
	  #7: (0.31583921810712995,0.2578618544065077,0.3315735837811572,0.5338359187337867,0.4651502894344987,0.43890536483697623,0.663590071886985) [ #5 #3 #11 ]
	  #9: (0.6433358738305089,0.8510643073337363,0.1396352367154126,0.3651542732078317,0.3811186561265085,0.13299690643340828,0.32218492385027253) [ #4 #8 #5 ]
	  #11: (0.37554240239689224,0.33198754425634497,0.3769611767746192,0.5853777632096347,0.49334346413095936,0.6366194187311844,0.7613864681205506) [ #6 #7 #10 ]
	  #13: (0.4802162232587698,0.1786393518577524,0.5133873259101229,0.5759198767029011,0.4083829699619253,0.1967094981769888,0.2822512292218056) [ #2 #5 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 1.135412753
	Number of best updates: 15

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 10518

	Init times:
		Bottleneck Steiner distances: 0.000022348
		Smallest sphere distances: 0.000001265
		Sorting: 0.000002776
		Total init time: 0.000027794

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000797, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003779, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032356, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000358112, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004460702, bsd pruned: 0, ss pruned: 945

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 9450
	Total number iterations 144606 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 15.302222222222222
	Total time: 1.125710927
	Avarage time pr. topology: 0.000119122

Data for the geometric median iteration:
	Total time: 1.072016904
	Total steps: 4952283
	Average number of steps pr. geometric median: 87.34185185185186
	Average time pr. step: 0.00000021646923328089288
	Average time pr. geometric median: 0.000018906823703703707

