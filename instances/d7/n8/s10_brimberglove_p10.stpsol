All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 11.010901542237146
MST len: 14.0336721125
Steiner Ratio: 0.7846058717895776
Solution Steiner tree:
	Terminals:
	  #0: (0.3548297497,0.954948144,0.4252187523,0.2287187526,0.0080249533,0.6942072132,0.3210569827) [ #3 ]
	  #1: (0.5658107323,0.6109299178,0.5057680786,0.1796468744,0.8166862739,0.1834716542,0.584652962) [ #7 ]
	  #2: (0.9637281978,0.2458365472,0.6618976293,0.3858842744,0.2711707318,0.9781572241,0.4471604677) [ #5 ]
	  #4: (0.8738890034,0.0530768098,0.2689884604,0.0923382296,0.8809632025,0.5625731631,0.6635139085) [ #5 ]
	  #6: (0.3317870611,0.5361120061,0.5565968144,0.8975977934,0.1470419239,0.0623648931,0.0772446674) [ #9 ]
	  #8: (0.8889881558,0.2567803758,0.9845708911,0.8704290836,0.2186908225,0.1240179041,0.0938713258) [ #9 ]
	  #10: (0.9814409283,0.9619104471,0.1394470134,0.2234422426,0.2144171056,0.0119858943,0.8683905582) [ #11 ]
	  #12: (0.4221560766,0.0253341845,0.3162595943,0.0612761933,0.083659018,0.9767909204,0.9780582851) [ #13 ]
	Steiner points:
	  #3: (0.5658107323,0.6109299177945999,0.4252187523,0.17964687439999966,0.1470419239,0.6942072132,0.5846529620000003) [ #7 #0 #13 ]
	  #5: (0.8738890034000001,0.2458365472,0.38889903296666667,0.11388538490000163,0.2711707318,0.7884017822666665,0.6635139085) [ #4 #2 #13 ]
	  #7: (0.5658107323,0.6109299177945999,0.5057680786,0.1796468744,0.1470419239,0.1834716542,0.5846529620000003) [ #3 #11 #1 ]
	  #9: (0.7043541309666667,0.5361120060999999,0.5565968144,0.8704290836,0.1470419239,0.1240179041,0.0938713258) [ #8 #6 #11 ]
	  #11: (0.7043541309666667,0.6109299177945999,0.5057680786,0.2234422426,0.14704193193174658,0.1240179041,0.5846529620000003) [ #7 #9 #10 ]
	  #13: (0.5658107323,0.2458365472,0.38889903296666667,0.11388538490000163,0.1470419239,0.7884017822666665,0.6635139084999999) [ #3 #5 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.322617121
	Number of best updates: 23

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 9573

	Init times:
		Bottleneck Steiner distances: 0.000025746
		Smallest sphere distances: 0.00000472
		Sorting: 0.000020456
		Total init time: 0.000053515

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000157, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000010771, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000100247, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001144308, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.012976981, bsd pruned: 0, ss pruned: 1890

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 8505
	Total number iterations 17319 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.036331569664903
	Total time: 0.293913849
	Avarage time pr. topology: 0.000034557

Data for the geometric median iteration:
	Total time: 0.246027925
	Total steps: 323832
	Average number of steps pr. geometric median: 6.345914168136391
	Average time pr. step: 0.0000007597393864719978
	Average time pr. geometric median: 0.0000048212409367038996

Data for the geometric median step function:
	Total number of fixed points encountered: 1510198

