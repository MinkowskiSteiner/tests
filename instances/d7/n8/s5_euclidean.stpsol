All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.969463227619716
MST len: 5.534187719447396
Steiner Ratio: 0.8979571130478261
Solution Steiner tree:
	Terminals:
	  #0: (0.3226665842,0.8743855478,0.8492661807,0.7391616813,0.1305225478,0.2126854878,0.0110087921) [ #7 ]
	  #1: (0.4484554052,0.4982478328,0.8940018629,0.0156746367,0.2746502162,0.9714187407,0.2212781451) [ #5 ]
	  #2: (0.4164950971,0.2561370005,0.3634193076,0.2718471113,0.4159667661,0.2562429226,0.3028504617) [ #13 ]
	  #4: (0.4612696383,0.4511669648,0.2853909383,0.0763532385,0.8217415124,0.9652387276,0.3917416899) [ #9 ]
	  #6: (0.3736224269,0.9697014317,0.4773686391,0.6483680232,0.016169196,0.4701238631,0.7283984682) [ #11 ]
	  #8: (0.546489314,0.4689284104,0.3138592538,0.0077589517,0.9200953757,0.5992501921,0.0841121902) [ #9 ]
	  #10: (0.1638579933,0.5497685357,0.0089649442,0.6123133984,0.048016368,0.9029668076,0.6279880356) [ #11 ]
	  #12: (0.2747455962,0.0464677648,0.9927552245,0.080030445,0.1476887968,0.0796446726,0.2805664764) [ #13 ]
	Steiner points:
	  #3: (0.39140353289826857,0.45024390578098766,0.5615880262639593,0.28482346128957664,0.3408710108622475,0.4621679973345527,0.2955084810952005) [ #5 #7 #13 ]
	  #5: (0.4365374523007125,0.46660317276976343,0.5898757722991493,0.14369425689479248,0.4588818353103223,0.6977284548335476,0.2618801906040203) [ #9 #3 #1 ]
	  #7: (0.3477251027561499,0.6787564642990778,0.5724082013749259,0.5121647145161873,0.1915425695183279,0.43628830486111614,0.3338356484824925) [ #3 #11 #0 ]
	  #9: (0.4830607707158497,0.461348546712792,0.37833243866071287,0.07180552368016441,0.7553213609384822,0.7692890706555362,0.2530560546966048) [ #4 #8 #5 ]
	  #11: (0.3213990132936653,0.7776252332330508,0.416715130704371,0.5911606075913446,0.08685846845272384,0.5459365478348964,0.5634984156640476) [ #6 #7 #10 ]
	  #13: (0.3860250632455421,0.31371868473680065,0.5372203144974564,0.2514497601175198,0.3460423505279691,0.3235812559127275,0.29655206919565763) [ #2 #3 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 1.12342527
	Number of best updates: 24

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 10518

	Init times:
		Bottleneck Steiner distances: 0.000010023
		Smallest sphere distances: 0.000001142
		Sorting: 0.000002821
		Total init time: 0.000015908

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000841, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003716, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032298, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000358318, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004463318, bsd pruned: 0, ss pruned: 945

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 9450
	Total number iterations 137142 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 14.512380952380953
	Total time: 1.11365417
	Avarage time pr. topology: 0.000117846

Data for the geometric median iteration:
	Total time: 1.062398334
	Total steps: 4918859
	Average number of steps pr. geometric median: 86.75236331569666
	Average time pr. step: 0.0000002159847098686911
	Average time pr. geometric median: 0.000018737184021164023

