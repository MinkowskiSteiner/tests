All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.151840590311647
MST len: 4.429556909667222
Steiner Ratio: 0.937303815027305
Solution Steiner tree:
	Terminals:
	  #0: (0.691448199,0.9836883438,0.7446426403,0.9263836541,0.3540221231,0.6064687002,0.515986182) [ #3 ]
	  #1: (0.6596048789,0.9180047749,0.9557145545,0.6343384281,0.3725255692,0.5868318391,0.7012481791) [ #9 ]
	  #2: (0.2152737436,0.6315492818,0.4236526631,0.6559597434,0.3525630521,0.6817573186,0.3819194806) [ #5 ]
	  #4: (0.6598329719,0.1353963139,0.1522402149,0.1232835241,0.6977873504,0.43929728,0.7240747631) [ #11 ]
	  #6: (0.4406859998,0.7210137708,0.2581046555,0.7259597377,0.3388851464,0.3019310251,0.3627231598) [ #7 ]
	  #8: (0.524526092,0.9271068843,0.9155044336,0.1841309705,0.8451116587,0.8712189877,0.8184693986) [ #9 ]
	  #10: (0.9164614044,0.3820900793,0.0400117864,0.5762943759,0.5174863937,0.1922520013,0.6995779) [ #11 ]
	  #12: (0.2349354551,0.3703337798,0.8618260598,0.5896025284,0.1705039685,0.3206381841,0.3995182511) [ #13 ]
	Steiner points:
	  #3: (0.6077872508436793,0.8809576220777758,0.7898900829866131,0.7575830598840935,0.3573720273321812,0.5762585974533891,0.567584300771194) [ #0 #9 #13 ]
	  #5: (0.3575854005311214,0.6410951291255242,0.4800661344678138,0.6690476775644769,0.3360778234981372,0.509413428608301,0.4215984617260073) [ #7 #2 #13 ]
	  #7: (0.4573126629794662,0.6519706577918101,0.3065481187323734,0.6771259097715844,0.36324150147185175,0.36211257298398425,0.4175333313580953) [ #6 #5 #11 ]
	  #9: (0.6481825372444311,0.913967175945352,0.9344793068552404,0.6313635972700754,0.38919267691403525,0.5966891091858384,0.6899775729786506) [ #3 #8 #1 ]
	  #11: (0.7490502357222517,0.3824318462579916,0.1281303693382058,0.4874133412475917,0.5269871488077541,0.29170520737469885,0.6416688691390603) [ #10 #7 #4 ]
	  #13: (0.3887752533927755,0.6411547482523215,0.6226475451366186,0.6730624198921308,0.30851002409425005,0.4873540552936033,0.4494367114744735) [ #3 #5 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 1.191867304
	Number of best updates: 20

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 11463

	Init times:
		Bottleneck Steiner distances: 0.000022528
		Smallest sphere distances: 0.000001123
		Sorting: 0.000002796
		Total init time: 0.000028303

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000776, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00000371, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032451, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000358242, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004794785, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 10395
	Total number iterations 156107 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 15.017508417508418
	Total time: 1.181605718
	Avarage time pr. topology: 0.00011367

Data for the geometric median iteration:
	Total time: 1.123549443
	Total steps: 5191087
	Average number of steps pr. geometric median: 83.23051146384479
	Average time pr. step: 0.00000021643818394875676
	Average time pr. geometric median: 0.000018014260750360747

