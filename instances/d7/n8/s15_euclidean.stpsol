All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.8836266946364795
MST len: 5.405187086132495
Steiner Ratio: 0.9035074303285215
Solution Steiner tree:
	Terminals:
	  #0: (0.3549731771,0.6731490375,0.5153574038,0.2768868838,0.9831143995,0.7832298362,0.3857721921) [ #5 ]
	  #1: (0.3530357351,0.0108154076,0.4717412081,0.6776405306,0.3551183903,0.1416954548,0.0826469702) [ #3 ]
	  #2: (0.7808070447,0.5882075376,0.619858953,0.1357802214,0.2613565746,0.1352163563,0.4126671052) [ #11 ]
	  #4: (0.3246047955,0.3443029827,0.6699542467,0.4050064401,0.8388915681,0.8634974965,0.5150421176) [ #7 ]
	  #6: (0.1940099584,0.0051929508,0.5976890882,0.1869087318,0.9714523661,0.5805552269,0.5467372041) [ #13 ]
	  #8: (0.2444709736,0.9184461929,0.7984392977,0.6373761276,0.9660496954,0.0332565117,0.740849513) [ #9 ]
	  #10: (0.9928987739,0.9662594152,0.9828661391,0.3598284723,0.0994729628,0.0463795699,0.3319424024) [ #11 ]
	  #12: (0.3929051535,0.0476035024,0.2348172144,0.1034733854,0.3869860398,0.9775588964,0.7308916956) [ #13 ]
	Steiner points:
	  #3: (0.5093735587558694,0.45535768932772425,0.6043850211104131,0.4072799312026291,0.49196952151816264,0.23849026740715135,0.36100066723960084) [ #1 #11 #9 ]
	  #5: (0.3509603872074024,0.5152376268869124,0.5899877830357759,0.3479830421151123,0.86312845548067,0.6909366383944949,0.46067322860407434) [ #9 #7 #0 ]
	  #7: (0.3235956658452801,0.34821751968657194,0.62411991369215,0.3588105461519433,0.8368380980955094,0.7993899585688943,0.5126803589675978) [ #4 #5 #13 ]
	  #9: (0.39300367408652925,0.5764746522833377,0.6401621522714446,0.43368818580059115,0.733472829251688,0.36646480372075496,0.47952001977708164) [ #8 #5 #3 ]
	  #11: (0.7670542349373275,0.6054852630747747,0.6497958835473374,0.18772278228092676,0.27464423248197417,0.13973626708395503,0.3994479894684527) [ #10 #3 #2 ]
	  #13: (0.29195432675775246,0.17997669856144857,0.5472567904154387,0.2562284895878929,0.8037632488404696,0.7566239908160532,0.5622580260875168) [ #6 #7 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 1.007167935
	Number of best updates: 20

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 10518

	Init times:
		Bottleneck Steiner distances: 0.000020075
		Smallest sphere distances: 0.000001484
		Sorting: 0.000002749
		Total init time: 0.000026101

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000801, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003691, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032195, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000369833, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004477504, bsd pruned: 0, ss pruned: 945

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 9450
	Total number iterations 129199 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 13.671851851851851
	Total time: 0.997377752
	Avarage time pr. topology: 0.000105542

Data for the geometric median iteration:
	Total time: 0.949254363
	Total steps: 4398882
	Average number of steps pr. geometric median: 77.58169312169312
	Average time pr. step: 0.00000021579445936490227
	Average time pr. geometric median: 0.000016741699523809525

