All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.225737012262516
MST len: 3.4287684523736566
Steiner Ratio: 0.9407858993888646
Solution Steiner tree:
	Terminals:
	  #0: (0.9451450268,0.2765807278,0.0144947227,0.0034960154,0.9854360809,0.0812585769,0.4243695994) [ #5 ]
	  #1: (0.1816147301,0.3554427341,0.7549710999,0.6513106882,0.8409329284,0.1592172925,0.3574281155) [ #7 ]
	  #2: (0.4696959581,0.4854901943,0.4042461926,0.7061174278,0.9462171723,0.1370705227,0.0844243104) [ #3 ]
	  #4: (0.0796413781,0.1055795798,0.3157934068,0.1158501856,0.8452832088,0.8998082606,0.2806415555) [ #5 ]
	  #6: (0.5482809849,0.0280388566,0.8680997448,0.9082862325,0.9183224421,0.4284378902,0.8492767708) [ #7 ]
	Steiner points:
	  #3: (0.40468515212381817,0.36627459037923515,0.4794908007109389,0.578494330569363,0.9103213537291437,0.22994499172540775,0.25454872439104886) [ #2 #5 #7 ]
	  #5: (0.45340541147731683,0.28188487711305693,0.33005351713055253,0.3305980179489055,0.9122756926001341,0.3573061496392536,0.30095408553347736) [ #4 #3 #0 ]
	  #7: (0.2970476134829939,0.317800849764802,0.6833177282932628,0.6608181632022688,0.8722461148644051,0.2149799722228764,0.3869939166391865) [ #1 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.001464671
	Number of best updates: 2

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000004635
		Smallest sphere distances: 0.000001
		Sorting: 0.000001459
		Total init time: 0.000008323

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000905, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003539, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 292 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 19.466666666666665
	Total time: 0.001440562
	Avarage time pr. topology: 0.000096037

Data for the geometric median iteration:
	Total time: 0.001376856
	Total steps: 5793
	Average number of steps pr. geometric median: 128.73333333333332
	Average time pr. step: 0.00000023767581563956502
	Average time pr. geometric median: 0.0000305968

