All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.414034901678959
MST len: 3.8689265384122793
Steiner Ratio: 0.8824243282427384
Solution Steiner tree:
	Terminals:
	  #0: (0.035923094,0.0724071777,0.1815535189,0.5228272334,0.9403845905,0.7741447132,0.7375370742) [ #5 ]
	  #1: (0.0319323023,0.6015652793,0.0553448466,0.5267799439,0.0893740547,0.7644367087,0.815491903) [ #3 ]
	  #2: (0.0533801331,0.3409319503,0.5098388086,0.016024685,0.2613700387,0.3636968142,0.7594234276) [ #7 ]
	  #4: (0.8889724807,0.163898673,0.2155044015,0.7876827134,0.788697773,0.0666457862,0.4325436914) [ #5 ]
	  #6: (0.4869041394,0.8679774124,0.5925911942,0.2147098403,0.0102265389,0.514818562,0.9959482527) [ #7 ]
	Steiner points:
	  #3: (0.16749833998249325,0.4700885864233186,0.26373691188437215,0.387708727721492,0.29427179168561896,0.5869880449715901,0.7875371134839657) [ #1 #5 #7 ]
	  #5: (0.26600792040975096,0.24944279645357,0.22127495071748113,0.5243667163181921,0.6521210724615828,0.5523228503379644,0.6938206038383061) [ #4 #3 #0 ]
	  #7: (0.19593062736705127,0.5097593617572814,0.4119202658037753,0.23053976353185412,0.22503763589103085,0.49910620283271684,0.8212466895577104) [ #2 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.001641467
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000016912
		Smallest sphere distances: 0.000001515
		Sorting: 0.000005126
		Total init time: 0.000028559

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002325, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003348, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 223 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 14.866666666666667
	Total time: 0.001581986
	Avarage time pr. topology: 0.000105465

Data for the geometric median iteration:
	Total time: 0.001533152
	Total steps: 6687
	Average number of steps pr. geometric median: 148.6
	Average time pr. step: 0.00000022927351577688052
	Average time pr. geometric median: 0.000034070044444444446

