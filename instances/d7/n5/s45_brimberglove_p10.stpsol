All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 6.198634371102682
MST len: 7.813685362899999
Steiner Ratio: 0.7933048341739344
Solution Steiner tree:
	Terminals:
	  #0: (0.5752755346,0.0176147758,0.6174002712,0.1841995149,0.0860658279,0.2139786585,0.0672587161) [ #3 ]
	  #1: (0.7481080414,0.2152077799,0.2788681999,0.3929495487,0.2476127913,0.5214306472,0.2908011355) [ #5 ]
	  #2: (0.6089239803,0.0684510521,0.5965783874,0.8830592017,0.0076694484,0.9101628568,0.7197443544) [ #7 ]
	  #4: (0.1297003627,0.0635728929,0.1258999995,0.2599066302,0.4942982874,0.8509792596,0.7114720651) [ #5 ]
	  #6: (0.0775070079,0.6388154154,0.6265101371,0.8390606664,0.1431079023,0.7291413614,0.1934557334) [ #7 ]
	Steiner points:
	  #3: (0.6089239802997839,0.0684510521,0.6065556372969579,0.3929495486999999,0.0860658279,0.6580028588864197,0.4398823594567901) [ #5 #0 #7 ]
	  #5: (0.6089239802997839,0.0684510521,0.27886819990000006,0.3929495486999999,0.24761279130000002,0.6580028588864197,0.4398823594567901) [ #4 #3 #1 ]
	  #7: (0.6089219531127534,0.06845322785907723,0.6065556372969579,0.8390606664000001,0.0860658279,0.7291413614,0.4398823594567901) [ #2 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000431439
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000008145
		Smallest sphere distances: 0.000001828
		Sorting: 0.000002887
		Total init time: 0.000013951

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001865, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000012412, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000371982
	Avarage time pr. topology: 0.000024798

Data for the geometric median iteration:
	Total time: 0.000322435
	Total steps: 376
	Average number of steps pr. geometric median: 8.355555555555556
	Average time pr. step: 0.0000008575398936170213
	Average time pr. geometric median: 0.0000071652222222222224

