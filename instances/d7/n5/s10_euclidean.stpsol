All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.715712142388925
MST len: 3.9362969360914137
Steiner Ratio: 0.9439613430379262
Solution Steiner tree:
	Terminals:
	  #0: (0.3317870611,0.5361120061,0.5565968144,0.8975977934,0.1470419239,0.0623648931,0.0772446674) [ #5 ]
	  #1: (0.8738890034,0.0530768098,0.2689884604,0.0923382296,0.8809632025,0.5625731631,0.6635139085) [ #7 ]
	  #2: (0.5658107323,0.6109299178,0.5057680786,0.1796468744,0.8166862739,0.1834716542,0.584652962) [ #3 ]
	  #4: (0.9814409283,0.9619104471,0.1394470134,0.2234422426,0.2144171056,0.0119858943,0.8683905582) [ #5 ]
	  #6: (0.4221560766,0.0253341845,0.3162595943,0.0612761933,0.083659018,0.9767909204,0.9780582851) [ #7 ]
	Steiner points:
	  #3: (0.6270041894259811,0.54032583591651,0.4266064309239351,0.2287667649459276,0.6599844586196351,0.24495036099026773,0.6046071233280617) [ #2 #5 #7 ]
	  #5: (0.6586406407997161,0.6393657798905968,0.38164743559971903,0.34589538667054126,0.46372682086750966,0.15749025511353779,0.5736948991007433) [ #4 #3 #0 ]
	  #7: (0.7070526367013824,0.21042715907724832,0.3304075900059431,0.13203965195374026,0.6581426979970288,0.5340829445073934,0.7026871297362821) [ #1 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.001102978
	Number of best updates: 2

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000004573
		Smallest sphere distances: 0.000000614
		Sorting: 0.000000998
		Total init time: 0.000007172

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000077, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003856, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 122 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 8.133333333333333
	Total time: 0.001079427
	Avarage time pr. topology: 0.000071961

Data for the geometric median iteration:
	Total time: 0.001040237
	Total steps: 4488
	Average number of steps pr. geometric median: 99.73333333333333
	Average time pr. step: 0.00000023178186274509804
	Average time pr. geometric median: 0.00002311637777777778

