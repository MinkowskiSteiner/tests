All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.825568491954988
MST len: 5.9636305185
Steiner Ratio: 0.8091662414338737
Solution Steiner tree:
	Terminals:
	  #0: (0.4625301303,0.7613690005,0.7293978635,0.7704790662,0.3740666413,0.1966241674,0.3118738645) [ #3 ]
	  #1: (0.3079489355,0.6126976412,0.4672263043,0.5413947988,0.3004468834,0.263256482,0.8872430967) [ #5 ]
	  #2: (0.6832347255,0.1237438932,0.9474799428,0.1736593936,0.3251002693,0.2867596859,0.331827752) [ #7 ]
	  #4: (0.4798882434,0.6288429693,0.6801386246,0.0063632978,0.7409414774,0.3425149626,0.8681538691) [ #5 ]
	  #6: (0.7088636578,0.0528921765,0.2627286242,0.2802385941,0.3090906377,0.2058983465,0.1701670499) [ #7 ]
	Steiner points:
	  #3: (0.4798882434,0.6126976412,0.5689979981222223,0.30794736612716045,0.3251002693,0.28675968589999995,0.3118738645000033) [ #5 #0 #7 ]
	  #5: (0.4798882434,0.6126976412,0.5689979981222223,0.30794736612716045,0.3251002693,0.28675968589999995,0.8681538691) [ #4 #3 #1 ]
	  #7: (0.6832347255,0.1237438932,0.5689979981222224,0.2802385941,0.3250997807397266,0.2867572182830633,0.3118738645000033) [ #2 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.00033379
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 15

	Init times:
		Bottleneck Steiner distances: 0.000010745
		Smallest sphere distances: 0.000001933
		Sorting: 0.000003281
		Total init time: 0.000016957

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002007, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000010005, bsd pruned: 0, ss pruned: 3

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 12
	Total number iterations 24 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000280795
	Avarage time pr. topology: 0.000023399

Data for the geometric median iteration:
	Total time: 0.000241458
	Total steps: 286
	Average number of steps pr. geometric median: 7.944444444444445
	Average time pr. step: 0.0000008442587412587413
	Average time pr. geometric median: 0.000006707166666666667

