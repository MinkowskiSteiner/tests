All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.939421008091452
MST len: 3.1382435658801446
Steiner Ratio: 0.9366452750989926
Solution Steiner tree:
	Terminals:
	  #0: (0.0517316135,0.6330953071,0.5835098287,0.7793133598,0.1173111168,0.526778261,0.1237135954) [ #3 ]
	  #1: (0.2621717841,0.5012496633,0.921708364,0.6899727819,0.193846746,0.6994581058,0.1578644305) [ #3 ]
	  #2: (0.7275817463,0.4842158102,0.9432684327,0.3444002356,0.4630026335,0.5578971945,0.2818813474) [ #5 ]
	  #4: (0.9471684238,0.6104576749,0.9758801474,0.721258364,0.4597480886,0.253197706,0.9255737224) [ #7 ]
	  #6: (0.7714722398,0.0351164541,0.1380414735,0.5359635696,0.9014610839,0.1763991002,0.6140612409) [ #7 ]
	Steiner points:
	  #3: (0.2723113810835085,0.5115942663730042,0.8888155480103339,0.6804085792798037,0.2046247672691247,0.6715431767429225,0.1674227963181076) [ #0 #5 #1 ]
	  #5: (0.6711404882591067,0.4769418119797463,0.8962013437404291,0.4430590861814544,0.44096617761311974,0.5321074348016158,0.3337994637475634) [ #2 #3 #7 ]
	  #7: (0.7880273716869621,0.432053600932778,0.7671313093247792,0.5591659159158314,0.5427577358726644,0.36139396684258474,0.597870562745739) [ #4 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.001355063
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000004706
		Smallest sphere distances: 0.000000555
		Sorting: 0.00000119
		Total init time: 0.000007609

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000095, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003451, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 147 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 9.8
	Total time: 0.001330612
	Avarage time pr. topology: 0.000088707

Data for the geometric median iteration:
	Total time: 0.001297109
	Total steps: 5646
	Average number of steps pr. geometric median: 125.46666666666667
	Average time pr. step: 0.00000022973946156571025
	Average time pr. geometric median: 0.000028824644444444447

