All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.4292641232502237
MST len: 3.821652727439747
Steiner Ratio: 0.8973248926119989
Solution Steiner tree:
	Terminals:
	  #0: (0.7133313109,0.2344475841,0.283649136,0.6526875024,0.0401867987,0.325981314,0.631247768) [ #3 ]
	  #1: (0.8456901428,0.1036064635,0.5821549858,0.7011187615,0.5075186931,0.9913254278,0.7521004219) [ #3 ]
	  #2: (0.5370173396,0.9999918654,0.1990076612,0.6581573387,0.8989526019,0.6503086554,0.7898902324) [ #5 ]
	  #4: (0.7428126711,0.9414201057,0.9901046399,0.0505868262,0.7723662428,0.4866999432,0.7424799096) [ #5 ]
	  #6: (0.939356192,0.805739215,0.042332178,0.9785602656,0.3255712615,0.9922046186,0.9965749998) [ #7 ]
	Steiner points:
	  #3: (0.7875346531786048,0.3775926550365096,0.38044510467970905,0.7041076362082412,0.3836540530107337,0.7384346220812361,0.7594146703959346) [ #0 #1 #7 ]
	  #5: (0.6635435860300486,0.8587082515800188,0.3725046824219495,0.5781430682014277,0.7240154301947092,0.6684369891698649,0.7963431850465968) [ #2 #4 #7 ]
	  #7: (0.7862100392862695,0.6382846933020329,0.2929775681534556,0.7329620652847492,0.47739582873887293,0.779906846332602,0.8307476444441715) [ #3 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000554277
	Number of best updates: 7

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.00000498
		Smallest sphere distances: 0.000000606
		Sorting: 0.000001087
		Total init time: 0.000007949

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000879, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000004096, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 70 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 4.666666666666667
	Total time: 0.000526239
	Avarage time pr. topology: 0.000035082

Data for the geometric median iteration:
	Total time: 0.000507778
	Total steps: 2055
	Average number of steps pr. geometric median: 45.666666666666664
	Average time pr. step: 0.00000024709391727493916
	Average time pr. geometric median: 0.000011283955555555555

