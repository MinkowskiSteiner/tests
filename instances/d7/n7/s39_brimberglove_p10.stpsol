All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 8.21865519257329
MST len: 10.8545467997
Steiner Ratio: 0.7571624448475767
Solution Steiner tree:
	Terminals:
	  #0: (0.6286001725,0.3799427857,0.0309723057,0.7538297781,0.4828030474,0.7509467996,0.2591224784) [ #5 ]
	  #1: (0.303527225,0.3381346959,0.3982325915,0.3818814775,0.10839433,0.5903588718,0.0385252712) [ #7 ]
	  #2: (0.9502985026,0.8603497673,0.402739647,0.5788986746,0.2402925525,0.4337119527,0.3327284527) [ #11 ]
	  #4: (0.5084510411,0.1311056945,0.9219905035,0.4655994868,0.719216585,0.2036320922,0.4264919485) [ #9 ]
	  #6: (0.1202703342,0.789870093,0.4786224181,0.0951295472,0.1731447364,0.2480481846,0.4273886245) [ #7 ]
	  #8: (0.9748592134,0.3832746434,0.7694257664,0.3322590768,0.7771537661,0.6123015828,0.975351023) [ #9 ]
	  #10: (0.9571484462,0.5881108905,0.2816415891,0.9608924622,0.4010537497,0.5862380008,0.0521304696) [ #11 ]
	Steiner points:
	  #3: (0.6286001725,0.4632749430401746,0.402739647,0.5788986746,0.29387961728845213,0.5184486472535997,0.3327284526976933) [ #7 #5 #11 ]
	  #5: (0.6286001725,0.3832746434,0.402739647,0.5788986746,0.4828030474,0.5184486472535997,0.3327284526976933) [ #3 #9 #0 ]
	  #7: (0.303527225,0.4632749430401746,0.402739647,0.3818814775,0.1731447364,0.5184486472535997,0.3327284526976933) [ #3 #6 #1 ]
	  #9: (0.6286001725,0.3832746434,0.7694257664,0.4655994868,0.719216585,0.5184486472535997,0.4264919485) [ #4 #5 #8 ]
	  #11: (0.9502985026,0.5881108905,0.4027391850493318,0.5789001317850982,0.29387961728845213,0.5184486472535997,0.332727382306571) [ #2 #3 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.031077047
	Number of best updates: 7

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000022779
		Smallest sphere distances: 0.000004873
		Sorting: 0.000022235
		Total init time: 0.000051893

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002108, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000010839, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000100158, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.0011575, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 1916 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0275132275132277
	Total time: 0.028388816
	Avarage time pr. topology: 0.000030041

Data for the geometric median iteration:
	Total time: 0.023813382
	Total steps: 30961
	Average number of steps pr. geometric median: 6.5525925925925925
	Average time pr. step: 0.000000769141242207939
	Average time pr. geometric median: 0.000005039869206349206

