All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.554148535423225
MST len: 5.166561962013418
Steiner Ratio: 0.8814659669054788
Solution Steiner tree:
	Terminals:
	  #0: (0.5463564538,0.9780446412,0.8501515877,0.8986921589,0.4174514107,0.3731905494,0.4855085041) [ #9 ]
	  #1: (0.3490163122,0.8709676535,0.6129136903,0.1817772687,0.4070928839,0.1923376211,0.4982240295) [ #5 ]
	  #2: (0.7376186767,0.6916461264,0.8085904055,0.6644335085,0.1308357772,0.4260037464,0.5934402573) [ #3 ]
	  #4: (0.0418115836,0.220037731,0.3087208249,0.1629512739,0.7427612197,0.2952385462,0.6634417692) [ #7 ]
	  #6: (0.0282778386,0.0528809102,0.589052372,0.5746342924,0.030925551,0.4392039592,0.4733264509) [ #7 ]
	  #8: (0.9403857789,0.7748197265,0.0876867548,0.9821973629,0.9948574575,0.3964075797,0.1451486364) [ #11 ]
	  #10: (0.9268148322,0.4391896513,0.6174133409,0.9290067493,0.8974420614,0.6268771643,0.9956121147) [ #11 ]
	Steiner points:
	  #3: (0.612458978530382,0.7429849360782866,0.7350579759234338,0.6615744083665085,0.32169363831882725,0.38632608989279743,0.5561143926155814) [ #9 #2 #5 ]
	  #5: (0.3721919153520434,0.677741296726149,0.6182342394408887,0.3780444677781357,0.37844372368875745,0.29228890797861645,0.5330593905017196) [ #3 #1 #7 ]
	  #7: (0.16915513464845225,0.35415012600277546,0.5115514132772224,0.3657304805672855,0.3951554378468079,0.3351081782102682,0.5574666981267519) [ #4 #6 #5 ]
	  #9: (0.6278100581638034,0.8073044403387454,0.7339705820585017,0.7812071307225426,0.43080067229178154,0.39879188183713643,0.5398181371893525) [ #0 #3 #11 ]
	  #11: (0.809171385160844,0.6744203428097637,0.5287231864235504,0.8827805291123392,0.7334010945218089,0.4753516843515238,0.5927771981677264) [ #8 #9 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.109577438
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000008339
		Smallest sphere distances: 0.000000997
		Sorting: 0.000002407
		Total init time: 0.000013064

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000799, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003549, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031397, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000351684, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 14049 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 14.866666666666667
	Total time: 0.108715171
	Avarage time pr. topology: 0.000115042

Data for the geometric median iteration:
	Total time: 0.104205276
	Total steps: 479038
	Average number of steps pr. geometric median: 101.3837037037037
	Average time pr. step: 0.00000021753029196013677
	Average time pr. geometric median: 0.000022054026666666667

