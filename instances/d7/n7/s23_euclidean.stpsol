All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.824729919304477
MST len: 4.249848213953563
Steiner Ratio: 0.8999685934068676
Solution Steiner tree:
	Terminals:
	  #0: (0.7797718648,0.0267614122,0.6903227464,0.5512441045,0.0618778663,0.8283642204,0.0872076736) [ #5 ]
	  #1: (0.7275817463,0.4842158102,0.9432684327,0.3444002356,0.4630026335,0.5578971945,0.2818813474) [ #3 ]
	  #2: (0.5803137503,0.0846754555,0.4055949428,0.8424855344,0.5859251188,0.3273033064,0.5324583159) [ #11 ]
	  #4: (0.2621717841,0.5012496633,0.921708364,0.6899727819,0.193846746,0.6994581058,0.1578644305) [ #7 ]
	  #6: (0.0517316135,0.6330953071,0.5835098287,0.7793133598,0.1173111168,0.526778261,0.1237135954) [ #7 ]
	  #8: (0.9471684238,0.6104576749,0.9758801474,0.721258364,0.4597480886,0.253197706,0.9255737224) [ #9 ]
	  #10: (0.7714722398,0.0351164541,0.1380414735,0.5359635696,0.9014610839,0.1763991002,0.6140612409) [ #11 ]
	Steiner points:
	  #3: (0.6757357203516009,0.4072512629400538,0.8524964041142175,0.49792738775116624,0.39510334488113813,0.5522951494441971,0.3295078456004446) [ #5 #9 #1 ]
	  #5: (0.5775251814932547,0.3479336104671357,0.8257138101255794,0.5671741479316468,0.2578992970626603,0.654557724907608,0.22309570124985817) [ #7 #3 #0 ]
	  #7: (0.27526546027479626,0.4972046501227304,0.8861150137914166,0.6854551826884386,0.19385099325231445,0.681671054666849,0.16130983491376383) [ #6 #4 #5 ]
	  #9: (0.7310742486968532,0.37106476000070204,0.7508688744558997,0.6378650978256605,0.47916651468373106,0.40372023956973285,0.5483322310235814) [ #8 #3 #11 ]
	  #11: (0.6311304952357706,0.12239480119708748,0.42145487691947725,0.7668059935301293,0.6141995178193825,0.31774663677137194,0.5465699845303481) [ #2 #9 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.082476746
	Number of best updates: 9

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 858

	Init times:
		Bottleneck Steiner distances: 0.000020473
		Smallest sphere distances: 0.000000961
		Sorting: 0.00000197
		Total init time: 0.000024861

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000815, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003662, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031036, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000287516, bsd pruned: 0, ss pruned: 210

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 735
	Total number iterations 9924 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 13.50204081632653
	Total time: 0.081710529
	Avarage time pr. topology: 0.00011117

Data for the geometric median iteration:
	Total time: 0.078551957
	Total steps: 363698
	Average number of steps pr. geometric median: 98.96544217687075
	Average time pr. step: 0.00000021598127292423935
	Average time pr. geometric median: 0.00002137468217687075

