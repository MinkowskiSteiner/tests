All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.937976740528211
MST len: 9.3785638807
Steiner Ratio: 0.8463957639467221
Solution Steiner tree:
	Terminals:
	  #0: (0.7797718648,0.0267614122,0.6903227464,0.5512441045,0.0618778663,0.8283642204,0.0872076736) [ #3 ]
	  #1: (0.7275817463,0.4842158102,0.9432684327,0.3444002356,0.4630026335,0.5578971945,0.2818813474) [ #9 ]
	  #2: (0.5803137503,0.0846754555,0.4055949428,0.8424855344,0.5859251188,0.3273033064,0.5324583159) [ #11 ]
	  #4: (0.2621717841,0.5012496633,0.921708364,0.6899727819,0.193846746,0.6994581058,0.1578644305) [ #7 ]
	  #6: (0.0517316135,0.6330953071,0.5835098287,0.7793133598,0.1173111168,0.526778261,0.1237135954) [ #7 ]
	  #8: (0.9471684238,0.6104576749,0.9758801474,0.721258364,0.4597480886,0.253197706,0.9255737224) [ #9 ]
	  #10: (0.7714722398,0.0351164541,0.1380414735,0.5359635696,0.9014610839,0.1763991002,0.6140612409) [ #11 ]
	Steiner points:
	  #3: (0.7275817463,0.06815578836666668,0.7855085459118633,0.5638388102383475,0.4619177852,0.5578971945,0.2818813474) [ #5 #0 #11 ]
	  #5: (0.7275817463,0.5012496632999576,0.7855085459118633,0.5638388102383475,0.4619177852,0.5578971945,0.2818813474) [ #3 #7 #9 ]
	  #7: (0.2621717841,0.5012496633,0.7855085459118633,0.6899727819,0.193846746,0.526778261,0.1578644305) [ #4 #6 #5 ]
	  #9: (0.7275818658892634,0.5012496632999576,0.7855085459118633,0.5638388102383475,0.4619177852,0.5578955293529034,0.28188236769185154) [ #8 #5 #1 ]
	  #11: (0.7275817462999999,0.06815578836666668,0.4055949428,0.5638388102383475,0.5859251188,0.32730330639999994,0.5324583159) [ #2 #3 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.025719707
	Number of best updates: 8

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 858

	Init times:
		Bottleneck Steiner distances: 0.000022964
		Smallest sphere distances: 0.000004275
		Sorting: 0.000022168
		Total init time: 0.00005095

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002158, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000010919, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000112958, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000961683, bsd pruned: 0, ss pruned: 210

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 735
	Total number iterations 1483 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.017687074829932
	Total time: 0.023394864
	Avarage time pr. topology: 0.000031829

Data for the geometric median iteration:
	Total time: 0.019740018
	Total steps: 25239
	Average number of steps pr. geometric median: 6.8677551020408165
	Average time pr. step: 0.0000007821236182099133
	Average time pr. geometric median: 0.000005371433469387756

