All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 9.046847015306927
MST len: 11.990051096199998
Steiner Ratio: 0.7545294797095685
Solution Steiner tree:
	Terminals:
	  #0: (0.215467854,0.2348691641,0.1913313205,0.4307993098,0.5479775013,0.0151233026,0.0380932228) [ #11 ]
	  #1: (0.861473049,0.2765600068,0.3932821464,0.0769409025,0.5114291709,0.5846134669,0.5077402124) [ #5 ]
	  #2: (0.5074669032,0.561359823,0.6648593674,0.4017881259,0.1900027982,0.7689555445,0.3776398615) [ #3 ]
	  #4: (0.0594066722,0.5997367695,0.5458334356,0.0842270214,0.5216057671,0.066293479,0.7319970623) [ #7 ]
	  #6: (0.1304018107,0.0569058615,0.9637256646,0.384026217,0.8656925191,0.3785816228,0.6610816175) [ #7 ]
	  #8: (0.0248203492,0.9218689981,0.5204600438,0.6477700405,0.0916402913,0.5212438672,0.540312229) [ #9 ]
	  #10: (0.8943212232,0.6286429756,0.1040961771,0.975851736,0.6714702508,0.5076044628,0.0156422849) [ #11 ]
	Steiner points:
	  #3: (0.5074669032,0.561359823,0.3932821464,0.4307993098,0.5114291709,0.509514361181346,0.3776398615) [ #5 #2 #11 ]
	  #5: (0.5074669032,0.561359823,0.3932821464,0.4307993098,0.5114291709,0.509514361181346,0.5077402124) [ #3 #1 #9 ]
	  #7: (0.1304018107,0.5997367695,0.5458334356,0.384026217,0.5216057671,0.3785816228,0.6610816175) [ #6 #4 #9 ]
	  #9: (0.1304018107,0.5997367695,0.5204600438,0.4307993098,0.5114291709,0.509514361181346,0.540312229) [ #5 #7 #8 ]
	  #11: (0.5074669032,0.561359823,0.1913313205,0.4308034681882737,0.5479775012999999,0.5076044628,0.03809322280000001) [ #0 #3 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.031269552
	Number of best updates: 12

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000025266
		Smallest sphere distances: 0.000004209
		Sorting: 0.00002205
		Total init time: 0.000053233

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002249, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000010752, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000112306, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.00112513, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 1903 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.013756613756614
	Total time: 0.028621432
	Avarage time pr. topology: 0.000030287

Data for the geometric median iteration:
	Total time: 0.024076741
	Total steps: 31348
	Average number of steps pr. geometric median: 6.634497354497355
	Average time pr. step: 0.0000007680471162434605
	Average time pr. geometric median: 0.00000509560656084656

