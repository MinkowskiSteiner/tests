All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.418050300987629
MST len: 3.694310547999134
Steiner Ratio: 0.9252200800603703
Solution Steiner tree:
	Terminals:
	  #0: (0.9164614044,0.3820900793,0.0400117864,0.5762943759,0.5174863937,0.1922520013,0.6995779) [ #11 ]
	  #1: (0.2152737436,0.6315492818,0.4236526631,0.6559597434,0.3525630521,0.6817573186,0.3819194806) [ #5 ]
	  #2: (0.4406859998,0.7210137708,0.2581046555,0.7259597377,0.3388851464,0.3019310251,0.3627231598) [ #3 ]
	  #4: (0.6596048789,0.9180047749,0.9557145545,0.6343384281,0.3725255692,0.5868318391,0.7012481791) [ #7 ]
	  #6: (0.691448199,0.9836883438,0.7446426403,0.9263836541,0.3540221231,0.6064687002,0.515986182) [ #7 ]
	  #8: (0.2349354551,0.3703337798,0.8618260598,0.5896025284,0.1705039685,0.3206381841,0.3995182511) [ #9 ]
	  #10: (0.6598329719,0.1353963139,0.1522402149,0.1232835241,0.6977873504,0.43929728,0.7240747631) [ #11 ]
	Steiner points:
	  #3: (0.45722192937079265,0.6521061967465765,0.3064397701433545,0.6775622857048661,0.362743925923789,0.3616350050198756,0.4172223116725668) [ #5 #2 #11 ]
	  #5: (0.3576402278119734,0.6410722909518562,0.4797069191248762,0.670154531744747,0.3347477701522189,0.5081913089735611,0.4210155025293104) [ #3 #1 #9 ]
	  #7: (0.6125314181990837,0.8833954385569123,0.7936646927598806,0.7652421693551625,0.34984393173225736,0.5722787663223639,0.5676277218187192) [ #6 #4 #9 ]
	  #9: (0.3894039385796349,0.6407317913766183,0.6245182302183552,0.6751116126175984,0.30551771652374365,0.48512091667489005,0.4490134298914381) [ #5 #7 #8 ]
	  #11: (0.7492182734847758,0.38243636955928967,0.1280041869507923,0.4875792432191649,0.5268810560572111,0.29149489568970915,0.6416754850674665) [ #0 #3 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.084141231
	Number of best updates: 7

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 858

	Init times:
		Bottleneck Steiner distances: 0.000008009
		Smallest sphere distances: 0.000001363
		Sorting: 0.000001944
		Total init time: 0.0000126

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000758, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003496, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031189, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000288191, bsd pruned: 0, ss pruned: 210

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 735
	Total number iterations 10801 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 14.695238095238095
	Total time: 0.083383528
	Avarage time pr. topology: 0.000113446

Data for the geometric median iteration:
	Total time: 0.07997856
	Total steps: 370455
	Average number of steps pr. geometric median: 100.80408163265307
	Average time pr. step: 0.00000021589278049965585
	Average time pr. geometric median: 0.000021762873469387757

