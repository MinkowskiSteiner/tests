All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.554906856064732
MST len: 5.1600489776908125
Steiner Ratio: 0.8827255081797907
Solution Steiner tree:
	Terminals:
	  #0: (0.3226665842,0.8743855478,0.8492661807,0.7391616813,0.1305225478,0.2126854878,0.0110087921) [ #5 ]
	  #1: (0.4484554052,0.4982478328,0.8940018629,0.0156746367,0.2746502162,0.9714187407,0.2212781451) [ #9 ]
	  #2: (0.4164950971,0.2561370005,0.3634193076,0.2718471113,0.4159667661,0.2562429226,0.3028504617) [ #11 ]
	  #4: (0.1638579933,0.5497685357,0.0089649442,0.6123133984,0.048016368,0.9029668076,0.6279880356) [ #7 ]
	  #6: (0.3736224269,0.9697014317,0.4773686391,0.6483680232,0.016169196,0.4701238631,0.7283984682) [ #7 ]
	  #8: (0.4612696383,0.4511669648,0.2853909383,0.0763532385,0.8217415124,0.9652387276,0.3917416899) [ #9 ]
	  #10: (0.2747455962,0.0464677648,0.9927552245,0.080030445,0.1476887968,0.0796446726,0.2805664764) [ #11 ]
	Steiner points:
	  #3: (0.3829112446426566,0.4631822859231365,0.5703830392953292,0.29857087735508187,0.3181453770134168,0.48993460771802844,0.3170609989974011) [ #9 #5 #11 ]
	  #5: (0.34492744811683024,0.6756601855132744,0.5713452203895368,0.5096182135352458,0.18568575025599188,0.45348051860762895,0.3473380885051552) [ #3 #7 #0 ]
	  #7: (0.3201973474649463,0.7728428546001154,0.41812978380633203,0.588335073991491,0.08704049394678784,0.5515405523424304,0.5632430060317261) [ #4 #6 #5 ]
	  #9: (0.4252100416578261,0.47213556903896553,0.6088667259459137,0.14541403111388007,0.4309534507801834,0.7752239897778966,0.3032189646566706) [ #8 #3 #1 ]
	  #11: (0.3813250517806404,0.3111668977797718,0.5436934034982015,0.25434923556333466,0.33511695838838285,0.32732235918092656,0.3054488181780113) [ #2 #3 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.097056983
	Number of best updates: 7

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000020844
		Smallest sphere distances: 0.000000923
		Sorting: 0.000001907
		Total init time: 0.000025048

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000782, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003502, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031693, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000349178, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 11518 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 12.188359788359788
	Total time: 0.096186373
	Avarage time pr. topology: 0.000101784

Data for the geometric median iteration:
	Total time: 0.092480714
	Total steps: 425972
	Average number of steps pr. geometric median: 90.15280423280423
	Average time pr. step: 0.00000021710514775619056
	Average time pr. geometric median: 0.000019572637883597883

