All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 8.786247015297642
MST len: 10.4369060786
Steiner Ratio: 0.8418440243812392
Solution Steiner tree:
	Terminals:
	  #0: (0.1197466637,0.0576706543,0.2857596349,0.6257388632,0.607333408,0.320081121,0.7510317898) [ #11 ]
	  #1: (0.2771394566,0.1947866791,0.8602793272,0.35034057,0.4464217548,0.0208598026,0.0373252058) [ #7 ]
	  #2: (0.8194229132,0.5062902763,0.0883140588,0.7855343049,0.3411775247,0.8119810777,0.9184383005) [ #3 ]
	  #4: (0.6239873639,0.9691653433,0.0583464685,0.4434102766,0.4754556192,0.1466605273,0.2289445811) [ #9 ]
	  #6: (0.5998500383,0.0285220109,0.4814383762,0.729074478,0.3766270487,0.206070881,0.2581979913) [ #7 ]
	  #8: (0.8166331438,0.958641605,0.1473828816,0.0937726004,0.1534282836,0.0076622083,0.4441131705) [ #9 ]
	  #10: (0.1292244392,0.3481050377,0.7246325052,0.5291235137,0.6861238385,0.9269996448,0.8449453608) [ #11 ]
	Steiner points:
	  #3: (0.5998500383,0.4184094093968676,0.2857596349,0.5552304988963054,0.4754556192,0.5223872953486584,0.782336313466663) [ #5 #2 #11 ]
	  #5: (0.5998500383,0.4184094093968676,0.2857596349,0.4434102766,0.4754556192,0.20607088070441712,0.3006674442333333) [ #7 #3 #9 ]
	  #7: (0.5998500383,0.1947866791,0.48143837619999996,0.4434102766,0.4464217548,0.20607088070441715,0.2581979913) [ #1 #6 #5 ]
	  #9: (0.6239873639,0.9586416050000001,0.1473828816,0.44340894284320054,0.47545439076788953,0.1466605273,0.3006674442333333) [ #4 #5 #8 ]
	  #11: (0.1292244392,0.3481050377,0.285762983208731,0.5552304988963054,0.607333408,0.5223872953486584,0.7823363134666629) [ #0 #3 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.025357851
	Number of best updates: 12

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 858

	Init times:
		Bottleneck Steiner distances: 0.0000375
		Smallest sphere distances: 0.000004112
		Sorting: 0.000009596
		Total init time: 0.000052987

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002108, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000010822, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000100154, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000927374, bsd pruned: 105, ss pruned: 105

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 735
	Total number iterations 1491 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0285714285714285
	Total time: 0.023143169
	Avarage time pr. topology: 0.000031487

Data for the geometric median iteration:
	Total time: 0.019521956
	Total steps: 25454
	Average number of steps pr. geometric median: 6.92625850340136
	Average time pr. step: 0.0000007669504203661507
	Average time pr. geometric median: 0.000005312096870748299

