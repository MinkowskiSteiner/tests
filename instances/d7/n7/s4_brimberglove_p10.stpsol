All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.9707007247429935
MST len: 8.8412480985
Steiner Ratio: 0.9015356922395716
Solution Steiner tree:
	Terminals:
	  #0: (0.654078258,0.1943656375,0.3097259371,0.5068725648,0.1146662767,0.5706952487,0.6701787769) [ #11 ]
	  #1: (0.2817635985,0.6604524426,0.8649749825,0.1982214736,0.79443441,0.0561858788,0.458302302) [ #9 ]
	  #2: (0.158582164,0.7489994023,0.9178017638,0.3488313283,0.3430800132,0.2595131855,0.6990013098) [ #3 ]
	  #4: (0.9271462466,0.29734262,0.6824838476,0.0102189095,0.7238869563,0.0950620273,0.2335532183) [ #5 ]
	  #6: (0.7253791097,0.7585264629,0.8398452051,0.6525253559,0.0558690829,0.5223290522,0.6627442654) [ #7 ]
	  #8: (0.0087156049,0.4163471956,0.8781052776,0.6627938634,0.6107128331,0.1878312143,0.1696664277) [ #9 ]
	  #10: (0.9164578756,0.1339819669,0.1912108968,0.2600808285,0.2142811954,0.3601613163,0.4198029756) [ #11 ]
	Steiner points:
	  #3: (0.1907476006333352,0.6604524426,0.8693517475333334,0.3488313283,0.3430800132,0.2595131855,0.458302302) [ #7 #2 #9 ]
	  #5: (0.6688834059836084,0.29734262,0.6824838476,0.4099880217654125,0.3430800132,0.35392198944195385,0.4583023019990059) [ #4 #7 #11 ]
	  #7: (0.6688834059836084,0.6604524426000001,0.8398452050999999,0.4099880217654125,0.3430800132,0.35392198944195385,0.458302302) [ #3 #6 #5 ]
	  #9: (0.1907476006333352,0.6604505802389721,0.8693517475333334,0.34883132829999997,0.6107128331,0.18783121429999997,0.4583000998998398) [ #1 #3 #8 ]
	  #11: (0.6688834059836085,0.1943656375,0.3097259371,0.4099880217654125,0.21428119540000004,0.3601613163000006,0.4583023019990059) [ #0 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.02140049
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 753

	Init times:
		Bottleneck Steiner distances: 0.000015794
		Smallest sphere distances: 0.000004743
		Sorting: 0.000021742
		Total init time: 0.000043414

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001778, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000010993, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000100903, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000821432, bsd pruned: 0, ss pruned: 315

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 630
	Total number iterations 1271 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0174603174603174
	Total time: 0.019392761
	Avarage time pr. topology: 0.000030782

Data for the geometric median iteration:
	Total time: 0.016411083
	Total steps: 21590
	Average number of steps pr. geometric median: 6.853968253968254
	Average time pr. step: 0.0000007601242704955998
	Average time pr. geometric median: 0.000005209867619047619

Data for the geometric median step function:
	Total number of fixed points encountered: 93420

