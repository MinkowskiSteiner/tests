All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.502000581702588
MST len: 4.892163747136338
Steiner Ratio: 0.9202473208992372
Solution Steiner tree:
	Terminals:
	  #0: (0.8646787037,0.8954018363,0.2308046744,0.0040116059,0.4605808875,0.1763232952,0.6261662299) [ #11 ]
	  #1: (0.3787998862,0.3083389789,0.7757516945,0.2434785898,0.2037408148,0.0065563684,0.2474901957) [ #7 ]
	  #2: (0.5613801752,0.2249833128,0.3930917794,0.4439383934,0.2850412509,0.1447810513,0.5635544996) [ #5 ]
	  #4: (0.6643217023,0.1828796636,0.873656426,0.6091953011,0.0289642629,0.8836533655,0.8662761761) [ #9 ]
	  #6: (0.2068786268,0.8687753225,0.7704661483,0.768258802,0.0937586353,0.1635579272,0.2121971949) [ #7 ]
	  #8: (0.3665206686,0.0588192782,0.5624328081,0.1339392146,0.878686387,0.568443856,0.5212840333) [ #9 ]
	  #10: (0.9448735988,0.8460845993,0.0099969395,0.257080875,0.0407533725,0.4439474528,0.1744425461) [ #11 ]
	Steiner points:
	  #3: (0.5496624969667957,0.4108436586314139,0.4897685746408458,0.3568602089916461,0.2728398977281261,0.20315846322930411,0.4533361601867902) [ #5 #7 #11 ]
	  #5: (0.5493110261871416,0.2977296391225458,0.4705173110328139,0.39360491028760186,0.3027587454122706,0.23249223391990909,0.5253705617468194) [ #3 #2 #9 ]
	  #7: (0.41840959896275176,0.4474361365289456,0.6585219467021749,0.3808021723680722,0.21272813795232084,0.11380364446871324,0.3250748287862039) [ #1 #6 #3 ]
	  #9: (0.526254183396815,0.21325616045738863,0.5765718182534428,0.371549847801854,0.393555836137292,0.4516167164085137,0.5941851879165272) [ #4 #5 #8 ]
	  #11: (0.7966175290864367,0.7389242439405483,0.24030514733927186,0.1830833472036323,0.2797754251937942,0.26443233543448785,0.4401549093135952) [ #0 #3 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.141187643
	Number of best updates: 8

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.00002111
		Smallest sphere distances: 0.000000984
		Sorting: 0.000002445
		Total init time: 0.000025911

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.0000008, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003569, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031128, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000364166, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 18208 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 19.26772486772487
	Total time: 0.14030426
	Avarage time pr. topology: 0.00014847

Data for the geometric median iteration:
	Total time: 0.134544414
	Total steps: 623036
	Average number of steps pr. geometric median: 131.8594708994709
	Average time pr. step: 0.0000002159496626198165
	Average time pr. geometric median: 0.000028475008253968253

