All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 8.075304288946976
MST len: 10.646382258500001
Steiner Ratio: 0.7585021928458098
Solution Steiner tree:
	Terminals:
	  #0: (0.7871501002,0.2962878152,0.4418524259,0.3354310847,0.3243266718,0.3099521703,0.2437173171) [ #7 ]
	  #1: (0.1816147301,0.3554427341,0.7549710999,0.6513106882,0.8409329284,0.1592172925,0.3574281155) [ #11 ]
	  #2: (0.4696959581,0.4854901943,0.4042461926,0.7061174278,0.9462171723,0.1370705227,0.0844243104) [ #3 ]
	  #4: (0.2426491134,0.7383900605,0.0929940874,0.3222904919,0.8439696402,0.4087874943,0.4381406775) [ #9 ]
	  #6: (0.9451450268,0.2765807278,0.0144947227,0.0034960154,0.9854360809,0.0812585769,0.4243695994) [ #7 ]
	  #8: (0.0796413781,0.1055795798,0.3157934068,0.1158501856,0.8452832088,0.8998082606,0.2806415555) [ #9 ]
	  #10: (0.5482809849,0.0280388566,0.8680997448,0.9082862325,0.9183224421,0.4284378902,0.8492767708) [ #11 ]
	Steiner points:
	  #3: (0.46969595809999976,0.3554419949288677,0.4042461926,0.7061174277981603,0.8439696402001446,0.29795721535308456,0.3856409742341752) [ #5 #2 #11 ]
	  #5: (0.46969595809999976,0.3554419949288677,0.4042461926,0.3354310847,0.8439696402001446,0.29795721535308456,0.3856409742341752) [ #3 #7 #9 ]
	  #7: (0.7871501002,0.2765807278,0.40424619259999994,0.3354310847,0.8439696402001446,0.29795721535308456,0.42436959939999996) [ #5 #6 #0 ]
	  #9: (0.24264911339999998,0.3554419949288677,0.3157934068,0.3222904919,0.8439699608172555,0.4087874943,0.3856409742341752) [ #4 #5 #8 ]
	  #11: (0.46969595809999976,0.3554373596988584,0.7549710999000001,0.7061174277981603,0.8439696402001445,0.29795721535308456,0.3856409742341752) [ #1 #3 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.030991587
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000023061
		Smallest sphere distances: 0.000004301
		Sorting: 0.000009951
		Total init time: 0.000039204

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002019, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000010844, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00009925, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001140091, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 1911 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.022222222222222
	Total time: 0.028356832
	Avarage time pr. topology: 0.000030007

Data for the geometric median iteration:
	Total time: 0.02382677
	Total steps: 31210
	Average number of steps pr. geometric median: 6.605291005291005
	Average time pr. step: 0.0000007634338353091958
	Average time pr. geometric median: 0.000005042702645502646

