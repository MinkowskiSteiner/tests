All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 9.2436269659
MST len: 11.627234139299999
Steiner Ratio: 0.7949979208431508
Solution Steiner tree:
	Terminals:
	  #0: (0.9253595797,0.9732846664,0.648336815,0.8774610296,0.3807683784,0.8408639463,0.2598100068) [ #9 ]
	  #1: (0.9521014504,0.4074837125,0.1925271313,0.3823489777,0.5831195775,0.1180090462,0.7377023845) [ #3 ]
	  #2: (0.5829005281,0.1765204129,0.443329524,0.5713405118,0.4465203399,0.0654066005,0.1171411286) [ #3 ]
	  #4: (0.7497371406,0.6546126439,0.7923043043,0.0619196091,0.0727788904,0.1874291036,0.8574096602) [ #11 ]
	  #6: (0.963887956,0.958872993,0.9975123913,0.5467884836,0.1353934054,0.4408419153,0.118128995) [ #7 ]
	  #8: (0.9792704061,0.1979680449,0.9935512585,0.7290075462,0.8525806893,0.7858555623,0.7909271553) [ #9 ]
	  #10: (0.3212535252,0.8584552788,0.7502715582,0.0250844127,0.5121703011,0.1360900286,0.5095511137) [ #11 ]
	Steiner points:
	  #3: (0.8317390799000001,0.4074837125,0.443329524,0.5165880483555556,0.44652033990000006,0.1180090462,0.49625694676666665) [ #1 #5 #2 ]
	  #5: (0.8317390799000001,0.6846200354677969,0.7562503587,0.5165880483555556,0.27468541319999995,0.1703160786,0.49625694676666665) [ #7 #3 #11 ]
	  #7: (0.9372085081111112,0.6846200354766733,0.7562503587,0.5467884835999999,0.27468541319999995,0.4408419153,0.447378603874074) [ #6 #5 #9 ]
	  #9: (0.9372085081111112,0.6846200354766733,0.7562503587,0.7290075462,0.3807683784,0.7858555623,0.447378603874074) [ #8 #7 #0 ]
	  #11: (0.7497371406,0.6846200354677969,0.7562503586999999,0.0619196091,0.27468541319999995,0.1703160786,0.5095511137000001) [ #4 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.031308631
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000020357
		Smallest sphere distances: 0.000004794
		Sorting: 0.000009266
		Total init time: 0.000036372

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002276, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000010992, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00009898, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001127795, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 1909 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.02010582010582
	Total time: 0.028644369
	Avarage time pr. topology: 0.000030311

Data for the geometric median iteration:
	Total time: 0.024128897
	Total steps: 31463
	Average number of steps pr. geometric median: 6.658835978835979
	Average time pr. step: 0.0000007668975304325716
	Average time pr. geometric median: 0.0000051066448677248685

