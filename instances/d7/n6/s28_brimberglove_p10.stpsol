All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.318113301152261
MST len: 9.1588123652
Steiner Ratio: 0.7990242631193435
Solution Steiner tree:
	Terminals:
	  #0: (0.3509209456,0.257528213,0.0420970586,0.0819942584,0.2799878476,0.3833070041,0.3121590714) [ #9 ]
	  #1: (0.9989762641,0.1782195168,0.6481475037,0.5161313692,0.2231632118,0.1005770402,0.9583443422) [ #5 ]
	  #2: (0.7310733133,0.0224596341,0.3412099454,0.230164813,0.4956663146,0.7013661609,0.408563846) [ #3 ]
	  #4: (0.9602391431,0.1126118736,0.8147720088,0.3593030024,0.8427590313,0.2819298144,0.890719803) [ #5 ]
	  #6: (0.7201248397,0.8276713341,0.0367873907,0.8458900362,0.860883003,0.4750771162,0.9620311726) [ #7 ]
	  #8: (0.517155105,0.0449436945,0.4524295369,0.442212973,0.1277577337,0.1569511728,0.0837527169) [ #9 ]
	Steiner points:
	  #3: (0.7310733133,0.13836126036666666,0.3412099454,0.230164813,0.4956663146,0.37197929492963033,0.408563846) [ #2 #7 #9 ]
	  #5: (0.9602391431,0.1126118736,0.6481475037,0.48766574614240205,0.6174052107304238,0.2819298144,0.8907198016923897) [ #4 #1 #7 ]
	  #7: (0.7310733133,0.13836126036666666,0.3412099454,0.48766574614240205,0.6174052107304238,0.37197929492963033,0.8907198016923896) [ #3 #5 #6 ]
	  #9: (0.517155105,0.13836126036666666,0.3412099454,0.230164813,0.2799878476,0.3719792949296258,0.31215907139999993) [ #0 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.003133962
	Number of best updates: 8

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000011946
		Smallest sphere distances: 0.000003646
		Sorting: 0.000017891
		Total init time: 0.000034976

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002056, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000010631, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000099959, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 212 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.019047619047619
	Total time: 0.002844652
	Avarage time pr. topology: 0.000027091

Data for the geometric median iteration:
	Total time: 0.002440541
	Total steps: 3101
	Average number of steps pr. geometric median: 7.383333333333334
	Average time pr. step: 0.0000007870174137375041
	Average time pr. geometric median: 0.000005810811904761906

