All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.326040369970239
MST len: 3.599881021373313
Steiner Ratio: 0.9239306383246504
Solution Steiner tree:
	Terminals:
	  #0: (0.1488122675,0.7401193766,0.5311698176,0.3798184727,0.6551358368,0.3903584468,0.6938135543) [ #7 ]
	  #1: (0.3702549456,0.5785871486,0.6617850073,0.153481192,0.2006002349,0.4885317048,0.9929940151) [ #5 ]
	  #2: (0.0627908218,0.1360618449,0.4750343694,0.4330457674,0.714648994,0.1368193762,0.5865269595) [ #9 ]
	  #4: (0.2525900748,0.6675356089,0.214533581,0.4014023423,0.4076549855,0.7457033986,0.781220815) [ #5 ]
	  #6: (0.0224110372,0.9863731647,0.8460247199,0.0179714379,0.5034501541,0.1039613784,0.3216621426) [ #7 ]
	  #8: (0.7874475535,0.1478928184,0.0819520862,0.2319531549,0.3117739127,0.1775910422,0.383365222) [ #9 ]
	Steiner points:
	  #3: (0.2270534773720726,0.5895754426396617,0.46560342591143417,0.33564341195436953,0.5255598205222487,0.407784758765148,0.7021436055491044) [ #5 #7 #9 ]
	  #5: (0.265208678862564,0.6084515579490785,0.43983285589285637,0.3139103133620426,0.4226188842567212,0.5175626621251174,0.7870627608199755) [ #4 #3 #1 ]
	  #7: (0.1581582311006225,0.7223148151339746,0.5355512828816642,0.34921742995237537,0.6185537912804175,0.3773438721528487,0.6738404509396138) [ #6 #3 #0 ]
	  #9: (0.273257805862215,0.33193114295827225,0.3948758414953699,0.35252247375366585,0.5559084151764707,0.2603750471314514,0.5965279934802586) [ #2 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.01060026
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000020745
		Smallest sphere distances: 0.000001004
		Sorting: 0.00000175
		Total init time: 0.000024983

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001445, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003273, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00002999, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 1229 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 11.704761904761904
	Total time: 0.010482609
	Avarage time pr. topology: 0.000099834

Data for the geometric median iteration:
	Total time: 0.010154309
	Total steps: 45901
	Average number of steps pr. geometric median: 109.28809523809524
	Average time pr. step: 0.00000022122195594867215
	Average time pr. geometric median: 0.000024176926190476192

