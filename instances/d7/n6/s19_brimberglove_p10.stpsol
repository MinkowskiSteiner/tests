All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.313605325144011
MST len: 8.9745469962
Steiner Ratio: 0.8149275198225309
Solution Steiner tree:
	Terminals:
	  #0: (0.2902333854,0.3277869785,0.2285638117,0.0594177293,0.2219586108,0.6696780285,0.3323929949) [ #9 ]
	  #1: (0.1610254427,0.8566360394,0.0970141232,0.4512588286,0.1844230174,0.3255779349,0.5106765579) [ #5 ]
	  #2: (0.4063816287,0.9952559634,0.8430695528,0.9825714156,0.2686294314,0.9196610394,0.6276440698) [ #7 ]
	  #4: (0.2170182561,0.784748353,0.4766765621,0.2870572444,0.5252522843,0.8965011402,0.6247255405) [ #5 ]
	  #6: (0.7965240673,0.6874572605,0.9015435013,0.9686559904,0.617568895,0.5169671907,0.3123343211) [ #7 ]
	  #8: (0.5761897869,0.273373468,0.0765914861,0.6450726547,0.0391567499,0.586565987,0.9423629041) [ #9 ]
	Steiner points:
	  #3: (0.38555218589999996,0.784748353,0.47667656209999987,0.4512599561666679,0.2686294314,0.6696780285,0.6247255405) [ #7 #5 #9 ]
	  #5: (0.2170182561,0.784748353,0.47667656209999987,0.4512599561666679,0.2686294314,0.6696780285,0.6247255405) [ #4 #3 #1 ]
	  #7: (0.4063816287,0.7847483530000001,0.8430695528,0.9686559904,0.26863209357652457,0.6696780285,0.6247255405) [ #2 #3 #6 ]
	  #9: (0.38555218589999996,0.32778697849999994,0.22856381169999998,0.4512599561666679,0.22195861079999998,0.669675492199182,0.6247255405) [ #0 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.003272792
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000009861
		Smallest sphere distances: 0.000002549
		Sorting: 0.000005292
		Total init time: 0.000018672

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001749, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000011614, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000104749, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 210 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.002986011
	Avarage time pr. topology: 0.000028438

Data for the geometric median iteration:
	Total time: 0.002575711
	Total steps: 3280
	Average number of steps pr. geometric median: 7.809523809523809
	Average time pr. step: 0.000000785277743902439
	Average time pr. geometric median: 0.000006132645238095238

Data for the geometric median step function:
	Total number of fixed points encountered: 13598

