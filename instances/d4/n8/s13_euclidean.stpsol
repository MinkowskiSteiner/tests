All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.5249016151050774
MST len: 3.653825412077965
Steiner Ratio: 0.9647153921074824
Solution Steiner tree:
	Terminals:
	  #0: (0.5670465732,0.4317533082,0.8557877559,0.1996897558) [ #5 ]
	  #1: (0.6083626135,0.3345068657,0.1558290944,0.7948272283) [ #9 ]
	  #2: (0.3779684163,0.1168798023,0.6027370345,0.4139833741) [ #11 ]
	  #4: (0.6326431831,0.3414155828,0.904109925,0.1300345134) [ #5 ]
	  #6: (0.792852993,0.8622477059,0.4365024196,0.0231408114) [ #13 ]
	  #8: (0.3533674392,0.8654058324,0.0406219666,0.7204131906) [ #9 ]
	  #10: (0.1637372478,0.1157799243,0.8294556531,0.9116950463) [ #11 ]
	  #12: (0.928200935,0.5560647936,0.0749040051,0.0531665883) [ #13 ]
	Steiner points:
	  #3: (0.515974161559767,0.3432185742046369,0.6191758997212141,0.33201570218792126) [ #11 #7 #5 ]
	  #5: (0.570547418141264,0.42062720899330075,0.8504697011105802,0.19907148448510145) [ #0 #4 #3 ]
	  #7: (0.6118255649208266,0.49907496050483524,0.37811876219417373,0.36942337664735475) [ #3 #9 #13 ]
	  #9: (0.545992358057313,0.5210152419261181,0.20044704818542106,0.635991109656357) [ #8 #7 #1 ]
	  #11: (0.380852515627207,0.13358878690500045,0.6117072297118628,0.42494066651480844) [ #10 #3 #2 ]
	  #13: (0.7860721210260236,0.6483621699098049,0.2938236087310573,0.1362776689566216) [ #6 #7 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.45367044
	Number of best updates: 8

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 8628

	Init times:
		Bottleneck Steiner distances: 0.000009757
		Smallest sphere distances: 0.000001534
		Sorting: 0.000002447
		Total init time: 0.000015238

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000097, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003402, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000030694, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000348928, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.003608508, bsd pruned: 945, ss pruned: 1890

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 7560
	Total number iterations 76706 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 10.146296296296295
	Total time: 0.445662725
	Avarage time pr. topology: 0.00005895

Data for the geometric median iteration:
	Total time: 0.416054929
	Total steps: 2677636
	Average number of steps pr. geometric median: 59.03077601410935
	Average time pr. step: 0.00000015538143683458095
	Average time pr. geometric median: 0.000009172286794532629

