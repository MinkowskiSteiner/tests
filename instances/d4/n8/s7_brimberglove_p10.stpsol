All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.899807300558463
MST len: 5.2045366281
Steiner Ratio: 0.9414492875511218
Solution Steiner tree:
	Terminals:
	  #0: (0.4869041394,0.8679774124,0.5925911942,0.2147098403) [ #11 ]
	  #1: (0.2155044015,0.7876827134,0.788697773,0.0666457862) [ #3 ]
	  #2: (0.6015652793,0.0553448466,0.5267799439,0.0893740547) [ #5 ]
	  #4: (0.4325436914,0.0533801331,0.3409319503,0.5098388086) [ #7 ]
	  #6: (0.016024685,0.2613700387,0.3636968142,0.7594234276) [ #9 ]
	  #8: (0.035923094,0.0724071777,0.1815535189,0.5228272334) [ #9 ]
	  #10: (0.7644367087,0.815491903,0.8889724807,0.163898673) [ #11 ]
	  #12: (0.0102265389,0.514818562,0.9959482527,0.0319323023) [ #13 ]
	Steiner points:
	  #3: (0.2155044015,0.7876827133999998,0.6913849563666492,0.11639636779175212) [ #1 #11 #13 ]
	  #5: (0.24341936312632115,0.2613700387,0.5267799439,0.1163963677917517) [ #7 #2 #13 ]
	  #7: (0.24341936312632115,0.2613700387,0.3409319503002804,0.5098388086) [ #5 #9 #4 ]
	  #9: (0.035923094,0.2613693178666406,0.3409319503002804,0.5228272334) [ #6 #7 #8 ]
	  #11: (0.4869041394,0.815491903,0.6913849563666492,0.163898673) [ #10 #3 #0 ]
	  #13: (0.21550440150000003,0.514818562,0.6913849563666491,0.11639636779175169) [ #3 #5 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.179746964
	Number of best updates: 13

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 5268

	Init times:
		Bottleneck Steiner distances: 0.000043372
		Smallest sphere distances: 0.000021106
		Sorting: 0.000026324
		Total init time: 0.000092568

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001764, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000013855, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000125013, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001143724, bsd pruned: 0, ss pruned: 210
		Level 5 - Time: 0.009192069, bsd pruned: 0, ss pruned: 3675

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 4410
	Total number iterations 8875 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.012471655328798
	Total time: 0.157755885
	Avarage time pr. topology: 0.000035772

Data for the geometric median iteration:
	Total time: 0.125457789
	Total steps: 151061
	Average number of steps pr. geometric median: 5.709032501889645
	Average time pr. step: 0.0000008305107804132105
	Average time pr. geometric median: 0.000004741413038548753

Data for the geometric median step function:
	Total number of fixed points encountered: 341498

