All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.095167769558211
MST len: 4.6221756033
Steiner Ratio: 0.8859827321650151
Solution Steiner tree:
	Terminals:
	  #0: (0.1354387687,0.9625167521,0.799727213,0.4030091993) [ #13 ]
	  #1: (0.0851281491,0.8056346727,0.6451566176,0.2473998085) [ #3 ]
	  #2: (0.3870529828,0.7761541152,0.6623876005,0.1703382573) [ #5 ]
	  #4: (0.87826215,0.7782872272,0.4166978609,0.2226562315) [ #9 ]
	  #6: (0.3191112491,0.3110007436,0.691104038,0.1034285073) [ #11 ]
	  #8: (0.9779993249,0.8022396713,0.6132100036,0.1134380932) [ #9 ]
	  #10: (0.5511224272,0.0462225136,0.6177704891,0.4252223039) [ #11 ]
	  #12: (0.3098868776,0.9831179767,0.5296626759,0.9438327159) [ #13 ]
	Steiner points:
	  #3: (0.19358813833333333,0.8056346727,0.6451566176,0.24739980850000004) [ #7 #1 #13 ]
	  #5: (0.3870529828,0.7761541152000856,0.6604662419582112,0.22265623149999997) [ #2 #7 #9 ]
	  #7: (0.3870529827933273,0.7761541152000856,0.6604662419582112,0.22265623149999997) [ #5 #3 #11 ]
	  #9: (0.87826215,0.8022396713,0.6132100036,0.2226562315) [ #4 #8 #5 ]
	  #11: (0.3870529827933273,0.3110007436,0.6604662419582112,0.22265623149999997) [ #6 #7 #10 ]
	  #13: (0.19358813833333333,0.9625167520999998,0.6451566176,0.40300919929999995) [ #0 #3 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.255792666
	Number of best updates: 20

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 8628

	Init times:
		Bottleneck Steiner distances: 0.000058564
		Smallest sphere distances: 0.000012111
		Sorting: 0.000036998
		Total init time: 0.000111873

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002727, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000013051, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000106868, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001196176, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.012425103, bsd pruned: 0, ss pruned: 2835

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 7560
	Total number iterations 15352 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.030687830687831
	Total time: 0.226642702
	Avarage time pr. topology: 0.000029979

Data for the geometric median iteration:
	Total time: 0.178882746
	Total steps: 252234
	Average number of steps pr. geometric median: 5.560714285714286
	Average time pr. step: 0.0000007091936297247794
	Average time pr. geometric median: 0.000003943623148148148

Data for the geometric median step function:
	Total number of fixed points encountered: 541464

