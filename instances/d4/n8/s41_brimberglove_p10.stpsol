All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.134954102600731
MST len: 6.5459463692
Steiner Ratio: 0.7844479335733221
Solution Steiner tree:
	Terminals:
	  #0: (0.2660512897,0.022327458,0.7685739201,0.5612500564) [ #11 ]
	  #1: (0.3927549945,0.8972157686,0.7339704534,0.1277633953) [ #5 ]
	  #2: (0.4878506383,0.4318964698,0.8467889367,0.5935844293) [ #7 ]
	  #4: (0.387681659,0.2726833854,0.3569481244,0.0666660061) [ #5 ]
	  #6: (0.6702460561,0.9116797088,0.38213495,0.5965716008) [ #9 ]
	  #8: (0.1785172881,0.7420079246,0.1287793886,0.8487633443) [ #9 ]
	  #10: (0.3233618859,0.0549102435,0.9953047172,0.408344438) [ #11 ]
	  #12: (0.9065222973,0.5735588286,0.6033617564,0.7889988608) [ #13 ]
	Steiner points:
	  #3: (0.38768165900037943,0.4318964698007298,0.7685670557715742,0.5102815169334847) [ #11 #5 #7 ]
	  #5: (0.38768165900037943,0.4318964698007298,0.7339704534,0.1277633953) [ #4 #1 #3 ]
	  #7: (0.4878506383,0.4318964698007298,0.7685670557715742,0.5935844292999999) [ #3 #2 #13 ]
	  #9: (0.6274078579666666,0.7420079246,0.38213495,0.6607141290698815) [ #6 #8 #13 ]
	  #11: (0.3233618859,0.0549102435,0.7685739201,0.5102815169334847) [ #10 #3 #0 ]
	  #13: (0.6274078579666666,0.5735588286,0.6033617564,0.6607141290698815) [ #7 #9 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.183863851
	Number of best updates: 17

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 9573

	Init times:
		Bottleneck Steiner distances: 0.000025926
		Smallest sphere distances: 0.000003235
		Sorting: 0.000009922
		Total init time: 0.000040048

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000149, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000007738, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000071404, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000815694, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.009280883, bsd pruned: 0, ss pruned: 1890

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 8505
	Total number iterations 17174 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0192827748383304
	Total time: 0.163911737
	Avarage time pr. topology: 0.000019272

Data for the geometric median iteration:
	Total time: 0.131314598
	Total steps: 273319
	Average number of steps pr. geometric median: 5.356045463452871
	Average time pr. step: 0.0000004804444550141044
	Average time pr. geometric median: 0.0000025732823437193804

