All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.507683503524162
MST len: 6.668784730900001
Steiner Ratio: 0.8258901322761487
Solution Steiner tree:
	Terminals:
	  #0: (0.4284378902,0.8492767708,0.0796413781,0.1055795798) [ #9 ]
	  #1: (0.1816147301,0.3554427341,0.7549710999,0.6513106882) [ #7 ]
	  #2: (0.4696959581,0.4854901943,0.4042461926,0.7061174278) [ #3 ]
	  #4: (0.0034960154,0.9854360809,0.0812585769,0.4243695994) [ #5 ]
	  #6: (0.3157934068,0.1158501856,0.8452832088,0.8998082606) [ #11 ]
	  #8: (0.2806415555,0.9451450268,0.2765807278,0.0144947227) [ #9 ]
	  #10: (0.0280388566,0.8680997448,0.9082862325,0.9183224421) [ #11 ]
	  #12: (0.9462171723,0.1370705227,0.0844243104,0.5482809849) [ #13 ]
	Steiner points:
	  #3: (0.4696959580999999,0.4854901943,0.4042461926,0.6513106882000002) [ #2 #7 #13 ]
	  #5: (0.42843788990230086,0.8492767708,0.11662224622416223,0.42436959939999996) [ #9 #4 #13 ]
	  #7: (0.21987522340060559,0.35544273410000016,0.7549710999,0.6513106882000002) [ #11 #3 #1 ]
	  #9: (0.42843788990230086,0.8492767708,0.11662224622416223,0.10557957979999999) [ #5 #8 #0 ]
	  #11: (0.21987522340060559,0.35544273410000016,0.8452832088,0.8998082606) [ #6 #7 #10 ]
	  #13: (0.4696959580999999,0.4854901943000001,0.11662224622416222,0.5482809849) [ #3 #5 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.189298825
	Number of best updates: 16

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 9573

	Init times:
		Bottleneck Steiner distances: 0.000016274
		Smallest sphere distances: 0.000003188
		Sorting: 0.000019069
		Total init time: 0.00003951

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001367, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000007816, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000071563, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000809017, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.009209015, bsd pruned: 945, ss pruned: 945

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 8505
	Total number iterations 17264 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.029864785420341
	Total time: 0.169454175
	Avarage time pr. topology: 0.000019924

Data for the geometric median iteration:
	Total time: 0.136917128
	Total steps: 288638
	Average number of steps pr. geometric median: 5.656241426611797
	Average time pr. step: 0.00000047435586443919373
	Average time pr. geometric median: 0.0000026830712913972173

