All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.134726472373592
MST len: 5.1350911983
Steiner Ratio: 0.8051904655057376
Solution Steiner tree:
	Terminals:
	  #0: (0.0816722829,0.5509562085,0.5646518891,0.743270507) [ #13 ]
	  #1: (0.2062651386,0.2501284509,0.6365585242,0.8636223808) [ #3 ]
	  #2: (0.3016556312,0.0249239351,0.3649927091,0.7653809822) [ #11 ]
	  #4: (0.9817601498,0.2188497932,0.4543959207,0.5186540231) [ #7 ]
	  #6: (0.5738417281,0.5552310401,0.7285462551,0.6288739409) [ #9 ]
	  #8: (0.7961281304,0.5837158191,0.2747664383,0.8295980784) [ #9 ]
	  #10: (0.3178602631,0.1357283928,0.106745258,0.7606715722) [ #11 ]
	  #12: (0.033469948,0.3299642076,0.6906357043,0.4224866822) [ #13 ]
	Steiner points:
	  #3: (0.2062651386,0.30900679987556234,0.5876252565995518,0.743270507) [ #5 #1 #13 ]
	  #5: (0.30705717516666664,0.30900679987556234,0.45439592070000007,0.743270507) [ #7 #3 #11 ]
	  #7: (0.5738417281,0.3090067998755623,0.4543959207,0.6288739409) [ #4 #5 #9 ]
	  #9: (0.5738417281,0.5552310401,0.4543959207,0.6288739409) [ #8 #6 #7 ]
	  #11: (0.30705717516666664,0.1357283928,0.3649927091,0.7606715722) [ #2 #5 #10 ]
	  #13: (0.0816722829,0.3299642076,0.5876252565995518,0.7432656123019699) [ #0 #3 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.169720152
	Number of best updates: 11

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 8628

	Init times:
		Bottleneck Steiner distances: 0.000017277
		Smallest sphere distances: 0.000012244
		Sorting: 0.000018809
		Total init time: 0.000049605

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001684, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000007745, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000071609, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000811219, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.008494305, bsd pruned: 945, ss pruned: 1890

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 7560
	Total number iterations 15268 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0195767195767194
	Total time: 0.15131519
	Avarage time pr. topology: 0.000020015

Data for the geometric median iteration:
	Total time: 0.122235309
	Total steps: 253370
	Average number of steps pr. geometric median: 5.585758377425044
	Average time pr. step: 0.0000004824379721356119
	Average time pr. geometric median: 0.0000026947819444444443

