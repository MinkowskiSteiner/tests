All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.6645096735831952
MST len: 4.074189577176411
Steiner Ratio: 0.8994450562908902
Solution Steiner tree:
	Terminals:
	  #0: (0.9520467748,0.6981957097,0.2743996956,0.1229939042) [ #13 ]
	  #1: (0.7037021051,0.7905814442,0.5198903161,0.7032006042) [ #7 ]
	  #2: (0.4961270823,0.6485379798,0.1537148399,0.2108905191) [ #11 ]
	  #4: (0.581988618,0.1856662101,0.839495378,0.391475477) [ #9 ]
	  #6: (0.746623855,0.6549854328,0.0995092923,0.7305007729) [ #7 ]
	  #8: (0.1415459719,0.6331634999,0.8501201872,0.1499063168) [ #9 ]
	  #10: (0.0529571777,0.5661688692,0.0288339364,0.6024998616) [ #11 ]
	  #12: (0.9994984996,0.2633149048,0.2158478607,0.460222048) [ #13 ]
	Steiner points:
	  #3: (0.7080767743799601,0.5922835058626865,0.2932904790902469,0.4354343140687286) [ #7 #5 #13 ]
	  #5: (0.5625271660512229,0.5761660691037899,0.3195262123176921,0.36235049714424966) [ #9 #3 #11 ]
	  #7: (0.7192914870139044,0.6655386544270772,0.29170970630290904,0.6019597144337887) [ #3 #6 #1 ]
	  #9: (0.44062172181592685,0.44990406081190887,0.6735938807088981,0.3079375460440314) [ #8 #4 #5 ]
	  #11: (0.47122192403630203,0.6112103534314665,0.203476465246962,0.3140084826981452) [ #2 #5 #10 ]
	  #13: (0.844037651035893,0.5419918330629643,0.26959233991658715,0.35683492172339804) [ #0 #3 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.774705179
	Number of best updates: 19

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 11463

	Init times:
		Bottleneck Steiner distances: 0.00001063
		Smallest sphere distances: 0.000001001
		Sorting: 0.000002138
		Total init time: 0.000014997

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000813, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003415, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000030406, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000347422, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004587616, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 10395
	Total number iterations 129604 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 12.467917267917269
	Total time: 0.765067328
	Avarage time pr. topology: 0.000073599

Data for the geometric median iteration:
	Total time: 0.71516116
	Total steps: 4567731
	Average number of steps pr. geometric median: 73.23602693602693
	Average time pr. step: 0.00000015656814291384496
	Average time pr. geometric median: 0.000011466428731762064

