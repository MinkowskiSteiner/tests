All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.781376107523325
MST len: 6.945226799199999
Steiner Ratio: 0.8324243793146201
Solution Steiner tree:
	Terminals:
	  #0: (0.3593030024,0.8427590313,0.2819298144,0.890719803) [ #9 ]
	  #1: (0.1277577337,0.1569511728,0.0837527169,0.7310733133) [ #7 ]
	  #2: (0.7013661609,0.408563846,0.7201248397,0.8276713341) [ #3 ]
	  #4: (0.9989762641,0.1782195168,0.6481475037,0.5161313692) [ #13 ]
	  #6: (0.0224596341,0.3412099454,0.230164813,0.4956663146) [ #7 ]
	  #8: (0.9620311726,0.9602391431,0.1126118736,0.8147720088) [ #9 ]
	  #10: (0.0367873907,0.8458900362,0.860883003,0.4750771162) [ #11 ]
	  #12: (0.517155105,0.0449436945,0.4524295369,0.442212973) [ #13 ]
	Steiner points:
	  #3: (0.5126681345795236,0.408563846,0.5163297970667738,0.8147720088) [ #5 #9 #2 ]
	  #5: (0.5126681345795236,0.34120994539715177,0.5163297970667738,0.5024466182) [ #3 #11 #13 ]
	  #7: (0.1277577337,0.3412099453971518,0.230164813,0.5024466182) [ #6 #11 #1 ]
	  #9: (0.5126681345795236,0.8427590313,0.2819298144,0.8147720088) [ #8 #3 #0 ]
	  #11: (0.1277577337,0.3412099453971518,0.5163297970667738,0.5024466182) [ #5 #7 #10 ]
	  #13: (0.517155105,0.17821951679999998,0.5163297970667738,0.5024466182) [ #4 #5 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.198314142
	Number of best updates: 13

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 10518

	Init times:
		Bottleneck Steiner distances: 0.000016684
		Smallest sphere distances: 0.000003195
		Sorting: 0.000010059
		Total init time: 0.000030909

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001547, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000007727, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00007164, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000806816, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.010059988, bsd pruned: 0, ss pruned: 945

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 9450
	Total number iterations 19022 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.012910052910053
	Total time: 0.176867084
	Avarage time pr. topology: 0.000018716

Data for the geometric median iteration:
	Total time: 0.140893459
	Total steps: 290948
	Average number of steps pr. geometric median: 5.131358024691358
	Average time pr. step: 0.0000004842564960061592
	Average time pr. geometric median: 0.0000024848934567901235

