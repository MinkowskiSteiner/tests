All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.0868963502092663
MST len: 3.405157702257123
Steiner Ratio: 0.9065355029410544
Solution Steiner tree:
	Terminals:
	  #0: (0.6638084062,0.3077898539,0.4603446049,0.1393605741) [ #3 ]
	  #1: (0.8887816281,0.2970957897,0.7287201051,0.3965607492) [ #11 ]
	  #2: (0.3061093089,0.3350736878,0.6921740634,0.5593727015) [ #13 ]
	  #4: (0.7955457819,0.2153102822,0.3426214272,0.296161194) [ #5 ]
	  #6: (0.4054190886,0.6635035848,0.7047985609,0.1648330605) [ #7 ]
	  #8: (0.2916780218,0.0321567464,0.8889940641,0.0872238032) [ #9 ]
	  #10: (0.8804080379,0.3659706252,0.346876925,0.9765705247) [ #11 ]
	  #12: (0.2554381654,0.71008044,0.8547150078,0.6357949691) [ #13 ]
	Steiner points:
	  #3: (0.6611499978689398,0.30424364681001304,0.4698620908309667,0.15919142593534305) [ #9 #5 #0 ]
	  #5: (0.7719307482135258,0.25463085154914844,0.42075707884599833,0.2950925521227999) [ #3 #4 #11 ]
	  #7: (0.4021186348813398,0.46517151991384287,0.6973207019031882,0.2766102437269185) [ #6 #9 #13 ]
	  #9: (0.45348482301533244,0.33518487924592244,0.6707292928177183,0.2065107033839547) [ #3 #8 #7 ]
	  #11: (0.8386409529411275,0.28949620891001293,0.5434354017574692,0.43976086210416737) [ #10 #5 #1 ]
	  #13: (0.3245203367745582,0.4541506695339337,0.7282220589477196,0.4897014798368939) [ #2 #7 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.673808253
	Number of best updates: 12

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 9573

	Init times:
		Bottleneck Steiner distances: 0.000019381
		Smallest sphere distances: 0.000001181
		Sorting: 0.000002307
		Total init time: 0.000024136

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003484, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000030665, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000345987, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.003923516, bsd pruned: 0, ss pruned: 1890

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 8505
	Total number iterations 125644 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 14.772957084068196
	Total time: 0.665273293
	Avarage time pr. topology: 0.000078221

Data for the geometric median iteration:
	Total time: 0.617603948
	Total steps: 3979154
	Average number of steps pr. geometric median: 77.97675876935136
	Average time pr. step: 0.0000001552098632020776
	Average time pr. geometric median: 0.00001210276206153243

