All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.8632663014318456
MST len: 4.190513928202642
Steiner Ratio: 0.9219075196079454
Solution Steiner tree:
	Terminals:
	  #0: (0.3317870611,0.5361120061,0.5565968144,0.8975977934) [ #9 ]
	  #1: (0.8166862739,0.1834716542,0.584652962,0.4221560766) [ #3 ]
	  #2: (0.5658107323,0.6109299178,0.5057680786,0.1796468744) [ #7 ]
	  #4: (0.2689884604,0.0923382296,0.8809632025,0.5625731631) [ #5 ]
	  #6: (0.6635139085,0.9814409283,0.9619104471,0.1394470134) [ #11 ]
	  #8: (0.2234422426,0.2144171056,0.0119858943,0.8683905582) [ #13 ]
	  #10: (0.9767909204,0.9780582851,0.8738890034,0.0530768098) [ #11 ]
	  #12: (0.0253341845,0.3162595943,0.0612761933,0.083659018) [ #13 ]
	Steiner points:
	  #3: (0.5906063299384129,0.3369096062165495,0.5721917914348811,0.4143593171700653) [ #5 #1 #7 ]
	  #5: (0.4121498915217824,0.293738205345372,0.6033650077871426,0.5573776904073384) [ #4 #9 #3 ]
	  #7: (0.5658107383635916,0.6109299158289978,0.5057680915328272,0.1796468820292615) [ #11 #2 #3 ]
	  #9: (0.3359219667839212,0.36651526389822675,0.47814165182741136,0.677928599593707) [ #5 #0 #13 ]
	  #11: (0.744861456614388,0.92645689625374,0.8686355615147235,0.11895088664775282) [ #10 #7 #6 ]
	  #13: (0.2301860285615799,0.299073382404802,0.21774419282324087,0.6281127054772455) [ #8 #9 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.640319856
	Number of best updates: 27

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 9573

	Init times:
		Bottleneck Steiner distances: 0.000022282
		Smallest sphere distances: 0.000001024
		Sorting: 0.000002495
		Total init time: 0.000026996

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000924, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003411, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000029997, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000347201, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.003897349, bsd pruned: 0, ss pruned: 1890

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 8505
	Total number iterations 119270 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 14.023515579071134
	Total time: 0.631814023
	Avarage time pr. topology: 0.000074287

Data for the geometric median iteration:
	Total time: 0.586547157
	Total steps: 3761889
	Average number of steps pr. geometric median: 73.71916519694298
	Average time pr. step: 0.00000015591825197394182
	Average time pr. geometric median: 0.000011494163374485598

