All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.746796680234143
MST len: 5.7415536036
Steiner Ratio: 0.8267442939586706
Solution Steiner tree:
	Terminals:
	  #0: (0.0163005716,0.2428867706,0.1372315768,0.8041767542) [ #11 ]
	  #1: (0.635711728,0.7172969294,0.1416025554,0.6069688763) [ #3 ]
	  #2: (0.2777747108,0.5539699558,0.4773970519,0.6288709248) [ #5 ]
	  #4: (0.9116473579,0.1975513693,0.3352227557,0.7682295948) [ #9 ]
	  #6: (0.8401877172,0.3943829268,0.7830992238,0.7984400335) [ #7 ]
	  #8: (0.998924518,0.2182569053,0.5129323944,0.8391122347) [ #9 ]
	  #10: (0.1566790893,0.4009443942,0.1297904468,0.108808802) [ #11 ]
	  #12: (0.3647844728,0.5134009102,0.9522297252,0.916195068) [ #13 ]
	Steiner points:
	  #3: (0.35919344694211675,0.4691986172887343,0.1416025554,0.8041760638852109) [ #1 #11 #5 ]
	  #5: (0.35919344694211675,0.4691986172887343,0.4773970519,0.8041760638852109) [ #2 #3 #13 ]
	  #7: (0.8401877172000001,0.39438292680000003,0.7569826117686151,0.7984400334737726) [ #6 #9 #13 ]
	  #9: (0.9116473578999998,0.2182569053,0.5129323944,0.7984400334737726) [ #8 #7 #4 ]
	  #11: (0.15667908929999996,0.4009443942,0.1416025554,0.8041760638852109) [ #10 #3 #0 ]
	  #13: (0.36478447280000004,0.4691986172887343,0.7569826117686151,0.804176063885211) [ #5 #7 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.256460202
	Number of best updates: 20

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 8628

	Init times:
		Bottleneck Steiner distances: 0.000044304
		Smallest sphere distances: 0.000008386
		Sorting: 0.000026273
		Total init time: 0.000082933

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002483, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000011902, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000107212, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001201507, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.012453152, bsd pruned: 0, ss pruned: 2835

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 7560
	Total number iterations 15291 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0226190476190475
	Total time: 0.227257685
	Avarage time pr. topology: 0.00003006

Data for the geometric median iteration:
	Total time: 0.179525499
	Total steps: 251806
	Average number of steps pr. geometric median: 5.5512786596119925
	Average time pr. step: 0.0000007129516334003161
	Average time pr. geometric median: 0.000003957793187830687

Data for the geometric median step function:
	Total number of fixed points encountered: 585932

