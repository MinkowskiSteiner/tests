All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.1681570130786807
MST len: 3.324314708297178
Steiner Ratio: 0.9530255980792847
Solution Steiner tree:
	Terminals:
	  #0: (0.0819520862,0.2319531549,0.3117739127,0.1775910422) [ #9 ]
	  #1: (0.1488122675,0.7401193766,0.5311698176,0.3798184727) [ #3 ]
	  #2: (0.6551358368,0.3903584468,0.6938135543,0.3702549456) [ #11 ]
	  #4: (0.2525900748,0.6675356089,0.214533581,0.4014023423) [ #5 ]
	  #6: (0.5785871486,0.6617850073,0.153481192,0.2006002349) [ #7 ]
	  #8: (0.0179714379,0.5034501541,0.1039613784,0.3216621426) [ #9 ]
	  #10: (0.383365222,0.0224110372,0.9863731647,0.8460247199) [ #11 ]
	  #12: (0.4885317048,0.9929940151,0.7874475535,0.1478928184) [ #13 ]
	Steiner points:
	  #3: (0.26679389416743715,0.713202544675081,0.49059571068025504,0.3527314337527568) [ #7 #1 #13 ]
	  #5: (0.2522033122317521,0.6630431314363334,0.2179574965910532,0.3946138209995682) [ #7 #4 #9 ]
	  #7: (0.30816207809276513,0.6747505368420192,0.27232206841303574,0.35346489591226593) [ #3 #6 #5 ]
	  #9: (0.07231992437839513,0.4920816909995988,0.1575493553135229,0.31339853289312797) [ #0 #8 #5 ]
	  #11: (0.6089499930128764,0.40379937108006336,0.7042359934655437,0.3938826511822919) [ #2 #10 #13 ]
	  #13: (0.41167415795133844,0.7061356756878288,0.6219340363540888,0.3104146545629289) [ #3 #11 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.688816604
	Number of best updates: 7

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 9573

	Init times:
		Bottleneck Steiner distances: 0.000010227
		Smallest sphere distances: 0.000001082
		Sorting: 0.000002486
		Total init time: 0.0000156

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000085, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003368, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000030788, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000357356, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.00391435, bsd pruned: 0, ss pruned: 1890

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 8505
	Total number iterations 118989 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 13.99047619047619
	Total time: 0.680275244
	Avarage time pr. topology: 0.000079985

Data for the geometric median iteration:
	Total time: 0.634924406
	Total steps: 4087925
	Average number of steps pr. geometric median: 80.10826964530668
	Average time pr. step: 0.00000015531703883999828
	Average time pr. geometric median: 0.000012442179227905151

