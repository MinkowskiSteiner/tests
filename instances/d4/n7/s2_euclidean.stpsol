All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.2818058366262868
MST len: 2.361500130409598
Steiner Ratio: 0.9662526828785359
Solution Steiner tree:
	Terminals:
	  #0: (0.3974697401,0.8897629529,0.8504806137,0.3177298374) [ #7 ]
	  #1: (0.5966266629,0.5564203358,0.8479080023,0.1846236713) [ #3 ]
	  #2: (0.3483067552,0.4219620016,0.6998054984,0.0663843365) [ #9 ]
	  #4: (0.5874823525,0.6429663057,0.9906032789,0.295718284) [ #5 ]
	  #6: (0.3227100961,0.9883655114,0.8425220697,0.2753141943) [ #7 ]
	  #8: (0.2713367107,0.069655987,0.949639379,0.3821752264) [ #9 ]
	  #10: (0.7009763693,0.8096763491,0.0887954552,0.1214791905) [ #11 ]
	Steiner points:
	  #3: (0.5965886428247956,0.5564423738967627,0.8478940770115074,0.1846400442278714) [ #5 #1 #11 ]
	  #5: (0.5649674633179508,0.6537000521075639,0.9369728646135923,0.27106293074099036) [ #3 #4 #7 ]
	  #7: (0.3974697401,0.8897629529,0.8504806137,0.3177298374) [ #0 #6 #5 ]
	  #9: (0.38014735049213777,0.4176053179618506,0.721747098721263,0.10912873638967455) [ #2 #8 #11 ]
	  #11: (0.5117679300576953,0.5220304181230975,0.7104379074826456,0.14435820237813501) [ #3 #9 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.048450863
	Number of best updates: 9

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000012074
		Smallest sphere distances: 0.000001151
		Sorting: 0.000002513
		Total init time: 0.000017509

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001061, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00000335, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000030278, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.00033629, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 9242 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 9.77989417989418
	Total time: 0.047607788
	Avarage time pr. topology: 0.000050378

Data for the geometric median iteration:
	Total time: 0.04457244
	Total steps: 288375
	Average number of steps pr. geometric median: 61.03174603174603
	Average time pr. step: 0.0000001545641612483745
	Average time pr. geometric median: 0.000009433320634920633

