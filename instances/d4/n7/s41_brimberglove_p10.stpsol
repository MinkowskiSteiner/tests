All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.355556229431187
MST len: 5.3789985121
Steiner Ratio: 0.8097336743323891
Solution Steiner tree:
	Terminals:
	  #0: (0.387681659,0.2726833854,0.3569481244,0.0666660061) [ #9 ]
	  #1: (0.2660512897,0.022327458,0.7685739201,0.5612500564) [ #7 ]
	  #2: (0.4878506383,0.4318964698,0.8467889367,0.5935844293) [ #3 ]
	  #4: (0.6702460561,0.9116797088,0.38213495,0.5965716008) [ #11 ]
	  #6: (0.3233618859,0.0549102435,0.9953047172,0.408344438) [ #7 ]
	  #8: (0.3927549945,0.8972157686,0.7339704534,0.1277633953) [ #9 ]
	  #10: (0.9065222973,0.5735588286,0.6033617564,0.7889988608) [ #11 ]
	Steiner points:
	  #3: (0.4878506383,0.4318979266979178,0.8441508524654774,0.5965715978367144) [ #5 #7 #2 ]
	  #5: (0.4878506383,0.4318979266979178,0.5793844779978242,0.5965715978367144) [ #3 #9 #11 ]
	  #7: (0.3233618859,0.0549102435,0.8441508524654774,0.5612500564) [ #6 #3 #1 ]
	  #9: (0.487850637176799,0.4318979266979178,0.5793844779978242,0.1277633953) [ #8 #5 #0 ]
	  #11: (0.6702460561,0.5735588286,0.5793844779978242,0.5965745354808649) [ #4 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.014675774
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 858

	Init times:
		Bottleneck Steiner distances: 0.000014865
		Smallest sphere distances: 0.000003392
		Sorting: 0.00000646
		Total init time: 0.000026522

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001701, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000007883, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000083583, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000690038, bsd pruned: 0, ss pruned: 210

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 735
	Total number iterations 1478 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0108843537414964
	Total time: 0.013058957
	Avarage time pr. topology: 0.000017767

Data for the geometric median iteration:
	Total time: 0.010639844
	Total steps: 21956
	Average number of steps pr. geometric median: 5.974421768707483
	Average time pr. step: 0.000000484598469666606
	Average time pr. geometric median: 0.0000028951956462585038

