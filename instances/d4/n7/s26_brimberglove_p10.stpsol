All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.6850852551170363
MST len: 4.3825332122
Steiner Ratio: 0.8408573481790341
Solution Steiner tree:
	Terminals:
	  #0: (0.7955457819,0.2153102822,0.3426214272,0.296161194) [ #9 ]
	  #1: (0.8887816281,0.2970957897,0.7287201051,0.3965607492) [ #3 ]
	  #2: (0.3061093089,0.3350736878,0.6921740634,0.5593727015) [ #5 ]
	  #4: (0.4054190886,0.6635035848,0.7047985609,0.1648330605) [ #11 ]
	  #6: (0.8804080379,0.3659706252,0.346876925,0.9765705247) [ #7 ]
	  #8: (0.6638084062,0.3077898539,0.4603446049,0.1393605741) [ #9 ]
	  #10: (0.2554381654,0.71008044,0.8547150078,0.6357949691) [ #11 ]
	Steiner points:
	  #3: (0.7587878300496365,0.30545153173333334,0.6963822292333389,0.4212704221069403) [ #7 #5 #1 ]
	  #5: (0.35542544753333327,0.3350736878000001,0.6963822292333389,0.4212704221069403) [ #2 #3 #11 ]
	  #7: (0.7587878300496365,0.30545153173333334,0.42117450132222223,0.4212704221069404) [ #6 #3 #9 ]
	  #9: (0.7587878300496365,0.30545153173333334,0.42117450132222223,0.29616119399999996) [ #8 #7 #0 ]
	  #11: (0.35542544753333327,0.6635035847999999,0.7047985609000051,0.4212704221069403) [ #4 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.011278997
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 648

	Init times:
		Bottleneck Steiner distances: 0.000013151
		Smallest sphere distances: 0.000002424
		Sorting: 0.00001902
		Total init time: 0.000035619

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000143, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00000764, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000072611, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000536248, bsd pruned: 210, ss pruned: 210

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 525
	Total number iterations 1055 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0095238095238095
	Total time: 0.00997123
	Avarage time pr. topology: 0.000018992

Data for the geometric median iteration:
	Total time: 0.008200016
	Total steps: 16698
	Average number of steps pr. geometric median: 6.361142857142857
	Average time pr. step: 0.0000004910777338603425
	Average time pr. geometric median: 0.000003123815619047619

