All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.42267712565267
MST len: 2.5636085629113223
Steiner Ratio: 0.9450261481812942
Solution Steiner tree:
	Terminals:
	  #0: (0.2062651386,0.2501284509,0.6365585242,0.8636223808) [ #3 ]
	  #1: (0.5738417281,0.5552310401,0.7285462551,0.6288739409) [ #7 ]
	  #2: (0.3016556312,0.0249239351,0.3649927091,0.7653809822) [ #9 ]
	  #4: (0.0816722829,0.5509562085,0.5646518891,0.743270507) [ #11 ]
	  #6: (0.9817601498,0.2188497932,0.4543959207,0.5186540231) [ #7 ]
	  #8: (0.3178602631,0.1357283928,0.106745258,0.7606715722) [ #9 ]
	  #10: (0.033469948,0.3299642076,0.6906357043,0.4224866822) [ #11 ]
	Steiner points:
	  #3: (0.2294006994895663,0.2587753627134042,0.5931337041635313,0.80709199922638) [ #9 #5 #0 ]
	  #5: (0.24340208805555086,0.37272252486082874,0.6159088557525739,0.7132897212313082) [ #3 #7 #11 ]
	  #7: (0.5611844820530928,0.48623510645790086,0.6786181640570305,0.6314438591196546) [ #6 #5 #1 ]
	  #9: (0.2979179334412182,0.051030993304129406,0.3590225686983856,0.7679373076512115) [ #8 #3 #2 ]
	  #11: (0.15206674745893425,0.42253955793594655,0.6131610305506308,0.6690693037540517) [ #4 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.049688535
	Number of best updates: 9

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 963

	Init times:
		Bottleneck Steiner distances: 0.000020572
		Smallest sphere distances: 0.000001002
		Sorting: 0.000001578
		Total init time: 0.000024556

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000092, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003228, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000030002, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000309355, bsd pruned: 0, ss pruned: 105

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 840
	Total number iterations 9612 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 11.442857142857143
	Total time: 0.048914216
	Avarage time pr. topology: 0.000058231

Data for the geometric median iteration:
	Total time: 0.045749589
	Total steps: 294627
	Average number of steps pr. geometric median: 70.14928571428571
	Average time pr. step: 0.00000015527968923418424
	Average time pr. geometric median: 0.000010892759285714286

