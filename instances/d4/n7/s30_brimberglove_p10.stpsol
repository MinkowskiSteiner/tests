All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.3635442186
MST len: 4.9519704875
Steiner Ratio: 0.8811733086080918
Solution Steiner tree:
	Terminals:
	  #0: (0.3946801244,0.8697485588,0.2358092783,0.2918461092) [ #5 ]
	  #1: (0.6298717496,0.5035535896,0.7460086135,0.8155644125) [ #9 ]
	  #2: (0.995645286,0.5080417518,0.6990755753,0.2370779334) [ #3 ]
	  #4: (0.3029992102,0.2899153434,0.1948941383,0.2638538039) [ #5 ]
	  #6: (0.6015800124,0.5553159199,0.0093155527,0.05122513) [ #7 ]
	  #8: (0.9940509931,0.5953307052,0.7999637326,0.8187845688) [ #9 ]
	  #10: (0.229967075,0.8697586934,0.5547460311,0.5776126504) [ #11 ]
	Steiner points:
	  #3: (0.6015800124023177,0.5080417518,0.6990755753,0.23707793381580808) [ #2 #7 #11 ]
	  #5: (0.3946801244,0.5553159199,0.2358092783,0.2918461092) [ #0 #4 #7 ]
	  #7: (0.6015800124,0.5553159199,0.2358092783,0.23707793381580805) [ #6 #3 #5 ]
	  #9: (0.6298717496,0.5953307051999801,0.7460086135,0.8155644125) [ #1 #8 #11 ]
	  #11: (0.6015800124023177,0.5953307051999801,0.6990755752999999,0.5776126504) [ #3 #9 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.013304306
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 753

	Init times:
		Bottleneck Steiner distances: 0.000014254
		Smallest sphere distances: 0.00000345
		Sorting: 0.000006366
		Total init time: 0.000025815

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001665, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000007852, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000071205, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000590183, bsd pruned: 0, ss pruned: 315

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 630
	Total number iterations 1273 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0206349206349206
	Total time: 0.011893076
	Avarage time pr. topology: 0.000018877

Data for the geometric median iteration:
	Total time: 0.009856677
	Total steps: 20363
	Average number of steps pr. geometric median: 6.464444444444444
	Average time pr. step: 0.0000004840483720473407
	Average time pr. geometric median: 0.0000031291038095238094

