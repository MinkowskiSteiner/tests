All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.83010254488831
MST len: 6.0360985915
Steiner Ratio: 0.800202725596635
Solution Steiner tree:
	Terminals:
	  #0: (0.1036064635,0.5821549858,0.7011187615,0.5075186931) [ #5 ]
	  #1: (0.6581573387,0.8989526019,0.6503086554,0.7898902324) [ #9 ]
	  #2: (0.9913254278,0.7521004219,0.7428126711,0.9414201057) [ #11 ]
	  #4: (0.7424799096,0.5370173396,0.9999918654,0.1990076612) [ #7 ]
	  #6: (0.9901046399,0.0505868262,0.7723662428,0.4866999432) [ #7 ]
	  #8: (0.3255712615,0.9922046186,0.9965749998,0.8456901428) [ #9 ]
	  #10: (0.939356192,0.805739215,0.042332178,0.9785602656) [ #11 ]
	Steiner points:
	  #3: (0.7424808541940531,0.7699800195911742,0.6503086554026006,0.808490202533385) [ #9 #5 #11 ]
	  #5: (0.7424808541940531,0.5821549858,0.6503086554026006,0.5075186931) [ #3 #7 #0 ]
	  #7: (0.7424808578935113,0.5370173396,0.7723662428,0.4866999432) [ #4 #6 #5 ]
	  #9: (0.6581573387,0.8989526019,0.6503086554026006,0.808490202533385) [ #8 #3 #1 ]
	  #11: (0.9393561919999999,0.7699800195911742,0.6503086554026007,0.9414201057) [ #2 #3 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.012882621
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 753

	Init times:
		Bottleneck Steiner distances: 0.000013917
		Smallest sphere distances: 0.000003305
		Sorting: 0.000006522
		Total init time: 0.000025215

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001627, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000007793, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000071574, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000605821, bsd pruned: 105, ss pruned: 210

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 630
	Total number iterations 1264 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0063492063492063
	Total time: 0.011439523
	Avarage time pr. topology: 0.000018157

Data for the geometric median iteration:
	Total time: 0.009387189
	Total steps: 19191
	Average number of steps pr. geometric median: 6.092380952380952
	Average time pr. step: 0.0000004891453806471784
	Average time pr. geometric median: 0.00000298006

