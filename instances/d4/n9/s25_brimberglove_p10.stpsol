All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 6.117474732140377
MST len: 7.682662307399999
Steiner Ratio: 0.7962701583600751
Solution Steiner tree:
	Terminals:
	  #0: (0.9913254278,0.7521004219,0.7428126711,0.9414201057) [ #15 ]
	  #1: (0.7133313109,0.2344475841,0.283649136,0.6526875024) [ #5 ]
	  #2: (0.6581573387,0.8989526019,0.6503086554,0.7898902324) [ #3 ]
	  #4: (0.1036064635,0.5821549858,0.7011187615,0.5075186931) [ #11 ]
	  #6: (0.9901046399,0.0505868262,0.7723662428,0.4866999432) [ #9 ]
	  #8: (0.7424799096,0.5370173396,0.9999918654,0.1990076612) [ #9 ]
	  #10: (0.0401867987,0.325981314,0.631247768,0.3657580602) [ #11 ]
	  #12: (0.3255712615,0.9922046186,0.9965749998,0.8456901428) [ #13 ]
	  #14: (0.939356192,0.805739215,0.042332178,0.9785602656) [ #15 ]
	Steiner points:
	  #3: (0.7133313109,0.8321351541333333,0.6503087046650109,0.7898902323999999) [ #5 #2 #13 ]
	  #5: (0.7133313109,0.5821549858,0.6503087046650109,0.6526875024) [ #1 #3 #7 ]
	  #7: (0.7133313109,0.5821549858,0.6778284314262989,0.486699943210481) [ #9 #5 #11 ]
	  #9: (0.7424799096,0.5370173396,0.7723662428,0.4866999432) [ #6 #8 #7 ]
	  #11: (0.1036064635,0.5821510769596229,0.677828431426299,0.486699943210481) [ #4 #7 #10 ]
	  #13: (0.7133313109,0.8321351541333333,0.6503087049404168,0.8456901428) [ #12 #3 #15 ]
	  #15: (0.939356192,0.805739215,0.6503087049404168,0.9414201057000001) [ #0 #13 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 1.7158345289999999
	Number of best updates: 15

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 77613

	Init times:
		Bottleneck Steiner distances: 0.000030009
		Smallest sphere distances: 0.000004238
		Sorting: 0.000014543
		Total init time: 0.000050148

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001703, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000007775, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000070944, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000799939, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.010863085, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.112692891, bsd pruned: 20790, ss pruned: 48195

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 66150
	Total number iterations 134402 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0317762660619803
	Total time: 1.494810905
	Avarage time pr. topology: 0.000022597

Data for the geometric median iteration:
	Total time: 1.199704489
	Total steps: 2495288
	Average number of steps pr. geometric median: 5.388808983911025
	Average time pr. step: 0.0000004807879847937392
	Average time pr. geometric median: 0.000002590874611812979

