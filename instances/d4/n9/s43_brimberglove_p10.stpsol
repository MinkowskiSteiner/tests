All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 6.153739261945177
MST len: 7.272049382799999
Steiner Ratio: 0.8462180243852756
Solution Steiner tree:
	Terminals:
	  #0: (0.1386943274,0.9690520419,0.497944257,0.1569527281) [ #13 ]
	  #1: (0.7615132685,0.3106321545,0.2578007133,0.9034257293) [ #5 ]
	  #2: (0.3921490034,0.2415533509,0.4918181642,0.7429387396) [ #3 ]
	  #4: (0.7194525505,0.1857139022,0.0945956191,0.7204462847) [ #5 ]
	  #6: (0.6047278813,0.6099463206,0.8727000411,0.9242446869) [ #7 ]
	  #8: (0.0485690669,0.5226562896,0.1556680115,0.8831310002) [ #11 ]
	  #10: (0.1298833849,0.3797864329,0.0226750942,0.2685777122) [ #11 ]
	  #12: (0.8875873521,0.8883300344,0.8879682691,0.2797363551) [ #13 ]
	  #14: (0.3024737403,0.0332014114,0.4304443828,0.8226312044) [ #15 ]
	Steiner points:
	  #3: (0.3921490034,0.2415533509,0.4918181642,0.7429387396567589) [ #2 #7 #15 ]
	  #5: (0.7194525505,0.3106321545,0.2578007133,0.7429387396567589) [ #9 #1 #4 ]
	  #7: (0.4866058014547203,0.6099463206,0.6279522610333251,0.7429387396567589) [ #3 #6 #13 ]
	  #9: (0.3142828284448562,0.3106321545,0.2578007133,0.7429387396567589) [ #11 #5 #15 ]
	  #11: (0.1298833849,0.3797864329,0.1556680115,0.7429387396567589) [ #8 #9 #10 ]
	  #13: (0.4866058014547203,0.8883300344,0.6279522610333251,0.2797363551) [ #12 #7 #0 ]
	  #15: (0.3142828284448562,0.2415533509,0.4304443828,0.742943603401937) [ #3 #9 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 2.003840849
	Number of best updates: 11

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 94623

	Init times:
		Bottleneck Steiner distances: 0.000022496
		Smallest sphere distances: 0.000004012
		Sorting: 0.000023771
		Total init time: 0.000051337

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001514, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.0000077, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000071518, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000808883, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.01086723, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.115901329, bsd pruned: 0, ss pruned: 51975

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 83160
	Total number iterations 168297 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.023773448773449
	Total time: 1.764365512
	Avarage time pr. topology: 0.000021216

Data for the geometric median iteration:
	Total time: 1.396868012
	Total steps: 2889835
	Average number of steps pr. geometric median: 4.964328660757232
	Average time pr. step: 0.0000004833729302884075
	Average time pr. geometric median: 0.000002399622091664949

