All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.41324949007751
MST len: 6.4060611831999985
Steiner Ratio: 0.8450199483379657
Solution Steiner tree:
	Terminals:
	  #0: (0.5214306472,0.2908011355,0.0775070079,0.6388154154) [ #5 ]
	  #1: (0.2599066302,0.4942982874,0.8509792596,0.7114720651) [ #13 ]
	  #2: (0.2152077799,0.2788681999,0.3929495487,0.2476127913) [ #9 ]
	  #4: (0.6265101371,0.8390606664,0.1431079023,0.7291413614) [ #5 ]
	  #6: (0.5752755346,0.0176147758,0.6174002712,0.1841995149) [ #15 ]
	  #8: (0.1934557334,0.1297003627,0.0635728929,0.1258999995) [ #11 ]
	  #10: (0.0860658279,0.2139786585,0.0672587161,0.0937352763) [ #11 ]
	  #12: (0.0076694484,0.9101628568,0.7197443544,0.7481080414) [ #13 ]
	  #14: (0.6089239803,0.0684510521,0.5965783874,0.8830592017) [ #15 ]
	Steiner points:
	  #3: (0.21520777990000067,0.27886818831176613,0.6027406548509692,0.7481080410560393) [ #9 #13 #7 ]
	  #5: (0.4809936359868865,0.2908011355,0.1431079023,0.7114720651) [ #7 #0 #4 ]
	  #7: (0.4809936359868865,0.2788681883117662,0.6027406548509692,0.7114720651) [ #3 #5 #15 ]
	  #9: (0.2152077799,0.27886818831176613,0.3929495487,0.2476127913) [ #3 #11 #2 ]
	  #11: (0.1934557334,0.2139786585,0.0672587161,0.1258999995) [ #8 #10 #9 ]
	  #13: (0.21520777990000067,0.4942982873999999,0.7197443544,0.7481080410560393) [ #12 #3 #1 ]
	  #15: (0.5752755345999999,0.0684510521,0.6027406548509693,0.7114720650999998) [ #6 #7 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 1.871322916
	Number of best updates: 19

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 87063

	Init times:
		Bottleneck Steiner distances: 0.000031752
		Smallest sphere distances: 0.000013096
		Sorting: 0.000015027
		Total init time: 0.000061227

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001524, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00000767, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000070945, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000804599, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.010879792, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.122931968, bsd pruned: 10395, ss pruned: 49140

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 75600
	Total number iterations 152818 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0214021164021165
	Total time: 1.6311893309999999
	Avarage time pr. topology: 0.000021576

Data for the geometric median iteration:
	Total time: 1.295872829
	Total steps: 2672152
	Average number of steps pr. geometric median: 5.049417989417989
	Average time pr. step: 0.0000004849547589358689
	Average time pr. geometric median: 0.000002448739283824641

