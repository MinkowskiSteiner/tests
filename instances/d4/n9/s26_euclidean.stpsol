All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.400970193946431
MST len: 3.7408387746346308
Steiner Ratio: 0.9091464237933122
Solution Steiner tree:
	Terminals:
	  #0: (0.8887816281,0.2970957897,0.7287201051,0.3965607492) [ #13 ]
	  #1: (0.2474670286,0.2316154909,0.3833849972,0.502905194) [ #3 ]
	  #2: (0.3061093089,0.3350736878,0.6921740634,0.5593727015) [ #15 ]
	  #4: (0.6638084062,0.3077898539,0.4603446049,0.1393605741) [ #5 ]
	  #6: (0.7955457819,0.2153102822,0.3426214272,0.296161194) [ #7 ]
	  #8: (0.4054190886,0.6635035848,0.7047985609,0.1648330605) [ #9 ]
	  #10: (0.2916780218,0.0321567464,0.8889940641,0.0872238032) [ #11 ]
	  #12: (0.8804080379,0.3659706252,0.346876925,0.9765705247) [ #13 ]
	  #14: (0.2554381654,0.71008044,0.8547150078,0.6357949691) [ #15 ]
	Steiner points:
	  #3: (0.31522858618611993,0.2980823875150279,0.6053955301502387,0.4599716515799198) [ #1 #11 #15 ]
	  #5: (0.6600987794618364,0.30789318740619387,0.46816191982291866,0.1630151767606245) [ #7 #9 #4 ]
	  #7: (0.7713107840868345,0.25595360217064306,0.4206973439275132,0.2959630556319025) [ #6 #5 #13 ]
	  #9: (0.4585990333738801,0.38405517839643316,0.6212156855107267,0.22503168189180567) [ #8 #5 #11 ]
	  #11: (0.3832460430073808,0.2980729178235317,0.6592697424467018,0.28186788253775086) [ #3 #9 #10 ]
	  #13: (0.8382110607281923,0.2899941055986519,0.5429435012591054,0.4400118956571332) [ #12 #7 #0 ]
	  #15: (0.3061067785928458,0.3351027129439026,0.6921627544483872,0.5593475960603105) [ #2 #3 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 10.089617275
	Number of best updates: 15

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 125808

	Init times:
		Bottleneck Steiner distances: 0.000012732
		Smallest sphere distances: 0.000001387
		Sorting: 0.000003069
		Total init time: 0.000019649

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001079, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003508, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000030539, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000344837, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004591658, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.061584624, bsd pruned: 0, ss pruned: 20790

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 114345
	Total number iterations 1670694 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 14.610993047356684
	Total time: 9.966221296
	Avarage time pr. topology: 0.000087159

Data for the geometric median iteration:
	Total time: 9.23567027
	Total steps: 58733635
	Average number of steps pr. geometric median: 73.37897840495243
	Average time pr. step: 0.00000015724669978284164
	Average time pr. geometric median: 0.000011538602187615176

