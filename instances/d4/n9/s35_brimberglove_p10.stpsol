All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.800650163547367
MST len: 6.514928349
Steiner Ratio: 0.8903628486470968
Solution Steiner tree:
	Terminals:
	  #0: (0.461066215,0.6987889817,0.7545507828,0.7532587111) [ #5 ]
	  #1: (0.470729368,0.5849627301,0.033694007,0.4157471649) [ #9 ]
	  #2: (0.5244897048,0.4379004908,0.0703035561,0.1812310276) [ #3 ]
	  #4: (0.809808453,0.6971266361,0.7090024225,0.6096137295) [ #5 ]
	  #6: (0.6606961906,0.4311483691,0.7982122655,0.1291394788) [ #13 ]
	  #8: (0.1675375119,0.7022328906,0.1086744881,0.7979400231) [ #15 ]
	  #10: (0.818542927,0.0240870542,0.352482224,0.9860804393) [ #11 ]
	  #12: (0.6467614917,0.3806424362,0.9537834986,0.171251196) [ #13 ]
	  #14: (0.0071734451,0.7727813925,0.162069249,0.0411185827) [ #15 ]
	Steiner points:
	  #3: (0.4610662150031615,0.4379004908,0.1086744881,0.3579631234528576) [ #2 #9 #11 ]
	  #5: (0.461066215,0.6971266361,0.7090024225,0.6096137295) [ #0 #4 #7 ]
	  #7: (0.4610662150000004,0.43114836910000004,0.7090024225,0.3579631234528602) [ #13 #11 #5 ]
	  #9: (0.4610662150031615,0.5849627301,0.1086744881,0.3579631234528576) [ #3 #1 #15 ]
	  #11: (0.4610662150000004,0.43114836910000004,0.352482224,0.3579631234528602) [ #7 #10 #3 ]
	  #13: (0.6467614917,0.43114835705845006,0.7982122655,0.171251196) [ #12 #7 #6 ]
	  #15: (0.16753751190000002,0.7022328906,0.10867774685867561,0.3579631234528575) [ #8 #9 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 1.89875997
	Number of best updates: 19

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 85173

	Init times:
		Bottleneck Steiner distances: 0.000031149
		Smallest sphere distances: 0.000012933
		Sorting: 0.000015013
		Total init time: 0.000060354

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001449, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000007791, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000071717, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000806692, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.010129216, bsd pruned: 0, ss pruned: 945
		Level 6 - Time: 0.104935825, bsd pruned: 0, ss pruned: 48195

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 74655
	Total number iterations 151478 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.029040251825062
	Total time: 1.68100468
	Avarage time pr. topology: 0.000022516

Data for the geometric median iteration:
	Total time: 1.3485334359999999
	Total steps: 2800597
	Average number of steps pr. geometric median: 5.35912243941177
	Average time pr. step: 0.000000481516418106568
	Average time pr. geometric median: 0.0000025805054412200886

