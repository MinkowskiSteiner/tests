All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.502760396526567
MST len: 6.722941334700001
Steiner Ratio: 0.8185048957848919
Solution Steiner tree:
	Terminals:
	  #0: (0.8540148287,0.22754956,0.1078885724,0.4860870128) [ #3 ]
	  #1: (0.9706272744,0.2301676097,0.3574414162,0.5424212746) [ #3 ]
	  #2: (0.5371329414,0.440277009,0.726414976,0.6913943327) [ #11 ]
	  #4: (0.1680129702,0.6031280205,0.3763264294,0.7351060159) [ #5 ]
	  #6: (0.8217135835,0.0627080598,0.4779169375,0.2268656857) [ #9 ]
	  #8: (0.2320200839,0.4801497965,0.4595757073,0.0860349122) [ #13 ]
	  #10: (0.2804932088,0.3381360212,0.8132569412,0.7641613385) [ #11 ]
	  #12: (0.6620165676,0.9043156765,0.6258278161,0.215128048) [ #13 ]
	  #14: (0.5717940003,0.0018524737,0.1227083798,0.9171544332) [ #15 ]
	Steiner points:
	  #3: (0.8540148287,0.22754956,0.3574414162,0.5424212746) [ #0 #1 #7 ]
	  #5: (0.5371329414,0.3381360212,0.37632642940000005,0.7641613090871273) [ #4 #11 #15 ]
	  #7: (0.571951054974851,0.22754956,0.3574414162,0.5424212746) [ #3 #9 #15 ]
	  #9: (0.571951054974851,0.22754956,0.4779169374999346,0.22686568569999999) [ #13 #7 #6 ]
	  #11: (0.5371329414,0.3381360212,0.726414976,0.7641613385) [ #5 #10 #2 ]
	  #13: (0.571951054974851,0.4801497965,0.4779169374999346,0.21512804799999996) [ #8 #9 #12 ]
	  #15: (0.5717940002985817,0.22754956,0.3574414162,0.7641613090871273) [ #5 #7 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 2.957815891
	Number of best updates: 29

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 125808

	Init times:
		Bottleneck Steiner distances: 0.000061347
		Smallest sphere distances: 0.000010574
		Sorting: 0.000038602
		Total init time: 0.000113827

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002248, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000008514, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000073771, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000832944, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.011135184, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.150057746, bsd pruned: 0, ss pruned: 20790

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 114345
	Total number iterations 233954 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0460361187633915
	Total time: 2.626884656
	Avarage time pr. topology: 0.000022972

Data for the geometric median iteration:
	Total time: 2.044494846
	Total steps: 4114237
	Average number of steps pr. geometric median: 5.140129807662275
	Average time pr. step: 0.0000004969317144345355
	Average time pr. geometric median: 0.0000025542935177376737

Data for the geometric median step function:
	Total number of fixed points encountered: 9746976

