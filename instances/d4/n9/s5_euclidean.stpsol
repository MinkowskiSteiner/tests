All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.6936223759675446
MST len: 3.898151715430649
Steiner Ratio: 0.9475317139008509
Solution Steiner tree:
	Terminals:
	  #0: (0.1476887968,0.0796446726,0.2805664764,0.4484554052) [ #3 ]
	  #1: (0.3634193076,0.2718471113,0.4159667661,0.2562429226) [ #9 ]
	  #2: (0.3028504617,0.4612696383,0.4511669648,0.2853909383) [ #7 ]
	  #4: (0.016169196,0.4701238631,0.7283984682,0.1638579933) [ #11 ]
	  #6: (0.3736224269,0.9697014317,0.4773686391,0.6483680232) [ #13 ]
	  #8: (0.9714187407,0.2212781451,0.4164950971,0.2561370005) [ #9 ]
	  #10: (0.0763532385,0.8217415124,0.9652387276,0.3917416899) [ #11 ]
	  #12: (0.4982478328,0.8940018629,0.0156746367,0.2746502162) [ #13 ]
	  #14: (0.2747455962,0.0464677648,0.9927552245,0.080030445) [ #15 ]
	Steiner points:
	  #3: (0.33371217933811526,0.28065850028434347,0.41862373286867266,0.2720181814314699) [ #0 #5 #9 ]
	  #5: (0.29025054468520967,0.39002811909178975,0.48021960526630575,0.2660705517955794) [ #7 #3 #15 ]
	  #7: (0.3028504617,0.4612696383,0.4511669648,0.2853909383) [ #13 #2 #5 ]
	  #9: (0.3634193076,0.2718471113,0.4159667661,0.2562429226) [ #1 #8 #3 ]
	  #11: (0.04230715931651153,0.473402636068299,0.7375357987433535,0.1801026698337639) [ #10 #4 #15 ]
	  #13: (0.39282176941542024,0.7838301792765942,0.3151319501402368,0.40907137757228124) [ #12 #7 #6 ]
	  #15: (0.15781368482889618,0.37282589330388155,0.705537826127523,0.18817242340263354) [ #5 #11 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 7.706653567
	Number of best updates: 23

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 125808

	Init times:
		Bottleneck Steiner distances: 0.000020372
		Smallest sphere distances: 0.000001275
		Sorting: 0.000003052
		Total init time: 0.000027006

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000101, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003454, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00003113, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000348191, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004651451, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.061996117, bsd pruned: 10395, ss pruned: 10395

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 114345
	Total number iterations 1365678 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 11.943486816214088
	Total time: 7.58248222
	Avarage time pr. topology: 0.000066312

Data for the geometric median iteration:
	Total time: 6.981666614
	Total steps: 44315447
	Average number of steps pr. geometric median: 55.36558785130214
	Average time pr. step: 0.00000015754476343203758
	Average time pr. geometric median: 0.00000872255844030909

