All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.106699481405316
MST len: 3.38600059752942
Steiner Ratio: 0.9175129749451625
Solution Steiner tree:
	Terminals:
	  #0: (0.3297338986,0.9487606892,0.4132981074,0.5756729662) [ #7 ]
	  #1: (0.7615917403,0.6558818466,0.357595246,0.5872719691) [ #3 ]
	  #2: (0.845191426,0.7414528996,0.4705074068,0.3762617672) [ #5 ]
	  #4: (0.8249852354,0.3957488953,0.5658004533,0.0595591958) [ #9 ]
	  #6: (0.7236746008,0.4541832616,0.7798275243,0.608648137) [ #13 ]
	  #8: (0.9910994321,0.8735950118,0.5318192572,0.054165287) [ #15 ]
	  #10: (0.5518146714,0.5618452218,0.610652922,0.9289213582) [ #11 ]
	  #12: (0.3771066863,0.354725022,0.9420667789,0.3395639064) [ #13 ]
	  #14: (0.8256802293,0.8959328252,0.2042499758,0.0233809524) [ #15 ]
	Steiner points:
	  #3: (0.7405591642193584,0.6790070187868432,0.40855778220925637,0.5591658960786902) [ #5 #1 #7 ]
	  #5: (0.8451914161313483,0.7414528860674255,0.47050739930309865,0.3762617618750316) [ #9 #2 #3 ]
	  #7: (0.6262613646235077,0.6901330428074707,0.4819076005242511,0.6149541945317958) [ #11 #0 #3 ]
	  #9: (0.8708135488564764,0.7089070452812996,0.47534389837506746,0.19021461663877928) [ #4 #5 #15 ]
	  #11: (0.6189910875310294,0.5782522757600889,0.6132761597539504,0.7090318525288739) [ #7 #10 #13 ]
	  #13: (0.6759335645294381,0.46708159119506026,0.764972165941347,0.6029351525557964) [ #12 #11 #6 ]
	  #15: (0.9096569456833975,0.8139445215074891,0.4414998446454414,0.10087292183245779) [ #8 #9 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 7.296752049
	Number of best updates: 22

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 115413

	Init times:
		Bottleneck Steiner distances: 0.000020421
		Smallest sphere distances: 0.000001154
		Sorting: 0.000002916
		Total init time: 0.000026915

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000868, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003515, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000030774, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000366502, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004635233, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.057651624, bsd pruned: 20790, ss pruned: 10395

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 103950
	Total number iterations 1250772 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 12.032438672438673
	Total time: 7.179257579
	Avarage time pr. topology: 0.000069064

Data for the geometric median iteration:
	Total time: 6.627703151
	Total steps: 42132317
	Average number of steps pr. geometric median: 57.90189926475641
	Average time pr. step: 0.00000015730687564607474
	Average time pr. geometric median: 0.000009108366867312582

