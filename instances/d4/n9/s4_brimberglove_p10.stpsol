All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.919633423800001
MST len: 5.4434349256
Steiner Ratio: 0.9037737184407943
Solution Steiner tree:
	Terminals:
	  #0: (0.6824838476,0.0102189095,0.7238869563,0.0950620273) [ #7 ]
	  #1: (0.2142811954,0.3601613163,0.4198029756,0.654078258) [ #11 ]
	  #2: (0.1943656375,0.3097259371,0.5068725648,0.1146662767) [ #3 ]
	  #4: (0.3488313283,0.3430800132,0.2595131855,0.6990013098) [ #5 ]
	  #6: (0.79443441,0.0561858788,0.458302302,0.0087156049) [ #15 ]
	  #8: (0.2817635985,0.6604524426,0.8649749825,0.1982214736) [ #13 ]
	  #10: (0.2335532183,0.158582164,0.7489994023,0.9178017638) [ #11 ]
	  #12: (0.5706952487,0.6701787769,0.9271462466,0.29734262) [ #13 ]
	  #14: (0.9164578756,0.1339819669,0.1912108968,0.2600808285) [ #15 ]
	Steiner points:
	  #3: (0.3488313283,0.3097259371,0.5068725648,0.1146662767) [ #7 #2 #9 ]
	  #5: (0.3488313283,0.3430800132,0.4198029756,0.654078258) [ #9 #11 #4 ]
	  #7: (0.6824838476,0.08211790816659069,0.5068725648,0.0950620273) [ #0 #3 #15 ]
	  #9: (0.3488313283,0.3430800132,0.4198029756000001,0.19822449831901368) [ #5 #13 #3 ]
	  #11: (0.2335532183,0.3430800132,0.4198029756,0.654078258) [ #5 #10 #1 ]
	  #13: (0.3488313283,0.6604524426,0.8649749825,0.19822449831901368) [ #8 #9 #12 ]
	  #15: (0.79443441,0.08211790816659069,0.4583023019999999,0.0950620273) [ #6 #7 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 2.222594088
	Number of best updates: 14

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 94623

	Init times:
		Bottleneck Steiner distances: 0.000075019
		Smallest sphere distances: 0.000010855
		Sorting: 0.000054608
		Total init time: 0.000144684

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002236, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000008923, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000075275, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000848306, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.011331159, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.120228293, bsd pruned: 0, ss pruned: 51975

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 83160
	Total number iterations 168918 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0312409812409813
	Total time: 1.955754914
	Avarage time pr. topology: 0.000023517

Data for the geometric median iteration:
	Total time: 1.526958923
	Total steps: 3039308
	Average number of steps pr. geometric median: 5.221102178245036
	Average time pr. step: 0.0000005024034823058407
	Average time pr. geometric median: 0.0000026230999158249162

Data for the geometric median step function:
	Total number of fixed points encountered: 7415547

