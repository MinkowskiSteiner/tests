All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.884660686823699
MST len: 4.11418066733736
Steiner Ratio: 0.9442124692442826
Solution Steiner tree:
	Terminals:
	  #0: (0.2155044015,0.7876827134,0.788697773,0.0666457862) [ #15 ]
	  #1: (0.4325436914,0.0533801331,0.3409319503,0.5098388086) [ #7 ]
	  #2: (0.4869041394,0.8679774124,0.5925911942,0.2147098403) [ #3 ]
	  #4: (0.6015652793,0.0553448466,0.5267799439,0.0893740547) [ #5 ]
	  #6: (0.016024685,0.2613700387,0.3636968142,0.7594234276) [ #13 ]
	  #8: (0.7644367087,0.815491903,0.8889724807,0.163898673) [ #9 ]
	  #10: (0.9403845905,0.7741447132,0.7375370742,0.9506111294) [ #11 ]
	  #12: (0.035923094,0.0724071777,0.1815535189,0.5228272334) [ #13 ]
	  #14: (0.0102265389,0.514818562,0.9959482527,0.0319323023) [ #15 ]
	Steiner points:
	  #3: (0.4845223101947017,0.8319518428544017,0.6485367498516244,0.20589304311553416) [ #9 #2 #15 ]
	  #5: (0.5568520687978756,0.17835641706650807,0.4936085874679111,0.30400699711278434) [ #4 #7 #11 ]
	  #7: (0.43244983094681455,0.05348505385802309,0.34097891157309407,0.50975985990477) [ #1 #5 #13 ]
	  #9: (0.629218897706468,0.7557225040970307,0.7271925092424464,0.25943396442792993) [ #8 #11 #3 ]
	  #11: (0.6696589762733316,0.6244138465686149,0.6746018936850652,0.39716325347450265) [ #5 #10 #9 ]
	  #13: (0.11298716184315424,0.11924547110634345,0.26362559892327014,0.5836805353597825) [ #6 #7 #12 ]
	  #15: (0.21550627001200856,0.7876791385590174,0.7886984281674316,0.06664791507301754) [ #0 #3 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 7.106190908
	Number of best updates: 21

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 94623

	Init times:
		Bottleneck Steiner distances: 0.000012404
		Smallest sphere distances: 0.000001129
		Sorting: 0.000003279
		Total init time: 0.000019562

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000979, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003471, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000030583, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000345094, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004619822, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.04892769, bsd pruned: 10395, ss pruned: 41580

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 83160
	Total number iterations 1253372 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 15.071813371813372
	Total time: 7.002203402
	Avarage time pr. topology: 0.000084201

Data for the geometric median iteration:
	Total time: 6.457127392
	Total steps: 41149024
	Average number of steps pr. geometric median: 70.68821548821549
	Average time pr. step: 0.00000015692054790898566
	Average time pr. geometric median: 0.00001109243350511922

