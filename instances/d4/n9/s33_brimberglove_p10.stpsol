All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.756408341003048
MST len: 5.7402680414
Steiner Ratio: 0.8286038747143597
Solution Steiner tree:
	Terminals:
	  #0: (0.331827752,0.4798882434,0.6288429693,0.6801386246) [ #3 ]
	  #1: (0.3079489355,0.6126976412,0.4672263043,0.5413947988) [ #9 ]
	  #2: (0.3740666413,0.1966241674,0.3118738645,0.6745135252) [ #5 ]
	  #4: (0.0528921765,0.2627286242,0.2802385941,0.3090906377) [ #13 ]
	  #6: (0.2058983465,0.1701670499,0.6832347255,0.1237438932) [ #7 ]
	  #8: (0.4625301303,0.7613690005,0.7293978635,0.7704790662) [ #11 ]
	  #10: (0.0063632978,0.7409414774,0.3425149626,0.8681538691) [ #11 ]
	  #12: (0.9474799428,0.1736593936,0.3251002693,0.2867596859) [ #13 ]
	  #14: (0.3004468834,0.263256482,0.8872430967,0.7088636578) [ #15 ]
	Steiner points:
	  #3: (0.33181549795974713,0.4798882434,0.6288446290515242,0.6801386245999999) [ #9 #0 #5 ]
	  #5: (0.33181549795974713,0.2629045768,0.6288446290515242,0.6745135252) [ #3 #2 #15 ]
	  #7: (0.2058983465,0.2627286241999967,0.6832347255,0.3016469871184717) [ #6 #13 #15 ]
	  #9: (0.3318145206269664,0.6126976411999999,0.6288446290515242,0.6801386245999999) [ #3 #11 #1 ]
	  #11: (0.3318145206269664,0.7409414774,0.6288446290515242,0.7704790662) [ #8 #10 #9 ]
	  #13: (0.2058983465,0.26272862419999676,0.3251002693,0.3016469871184717) [ #12 #7 #4 ]
	  #15: (0.3004468834,0.2629045768,0.6832347255,0.6745135252) [ #5 #7 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 2.636838551
	Number of best updates: 30

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 125808

	Init times:
		Bottleneck Steiner distances: 0.000031312
		Smallest sphere distances: 0.000004055
		Sorting: 0.000014534
		Total init time: 0.0000511

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001495, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000007556, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000071659, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000808845, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.010904041, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.146390192, bsd pruned: 0, ss pruned: 20790

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 114345
	Total number iterations 231486 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0244523153614065
	Total time: 2.337411817
	Avarage time pr. topology: 0.00002044

Data for the geometric median iteration:
	Total time: 1.833548889
	Total steps: 3791800
	Average number of steps pr. geometric median: 4.737292529500322
	Average time pr. step: 0.0000004835563291840287
	Average time pr. geometric median: 0.0000022907477858360977

