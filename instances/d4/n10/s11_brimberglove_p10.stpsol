All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.954432161592377
MST len: 6.022615698399999
Steiner Ratio: 0.822637938347718
Solution Steiner tree:
	Terminals:
	  #0: (0.9263453185,0.5264996991,0.3097808907,0.5022105041) [ #11 ]
	  #1: (0.8329159067,0.4041060449,0.0752406661,0.2481696737) [ #3 ]
	  #2: (0.7537675108,0.4059770589,0.4484664097,0.2208949445) [ #5 ]
	  #4: (0.3866200426,0.490760721,0.8656676732,0.1403875533) [ #9 ]
	  #6: (0.4371118506,0.8601203439,0.1809798303,0.3634571686) [ #13 ]
	  #8: (0.4592865964,0.7046087811,0.729890453,0.0711031715) [ #9 ]
	  #10: (0.8967377799,0.3141340824,0.3612824978,0.7296536866) [ #15 ]
	  #12: (0.2410501816,0.7910995301,0.1739375839,0.0170635157) [ #13 ]
	  #14: (0.9570931466,0.3607239967,0.5789050197,0.7321704266) [ #15 ]
	  #16: (0.3817098766,0.0352495974,0.3687719635,0.5168204054) [ #17 ]
	Steiner points:
	  #3: (0.8329159067,0.4041060449,0.36128258263180935,0.24817535492723358) [ #1 #5 #11 ]
	  #5: (0.7537675108,0.4059770589,0.38277066225345097,0.24817535492723358) [ #2 #3 #7 ]
	  #7: (0.45928659639999997,0.4059770589,0.38277066225345097,0.2481753549272336) [ #5 #9 #17 ]
	  #9: (0.4592865964,0.490760721,0.729890453,0.1403875533) [ #8 #4 #7 ]
	  #11: (0.8967377798999999,0.3296640538333333,0.3612825826318094,0.5022105041) [ #0 #15 #3 ]
	  #13: (0.4371118506,0.7910995301,0.18097983029999998,0.33772358845250816) [ #12 #6 #17 ]
	  #15: (0.8967377798999999,0.3296640538333333,0.3612825826318094,0.7296536866) [ #14 #11 #10 ]
	  #17: (0.4371118506,0.40597705889999997,0.36877196349999997,0.33772358845250816) [ #7 #13 #16 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 40.824335229
	Number of best updates: 22

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1833423

	Init times:
		Bottleneck Steiner distances: 0.000034966
		Smallest sphere distances: 0.000004925
		Sorting: 0.000020329
		Total init time: 0.000061643

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000142, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000007669, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000071504, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000810775, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.010825304, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.155965944, bsd pruned: 0, ss pruned: 10395
		Level 7 - Time: 2.495276588, bsd pruned: 0, ss pruned: 173880

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 1697220
	Total number iterations 3454869 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0356046947360977
	Total time: 35.909403514
	Avarage time pr. topology: 0.000021156

Data for the geometric median iteration:
	Total time: 27.446032292
	Total steps: 61214359
	Average number of steps pr. geometric median: 4.508428415290887
	Average time pr. step: 0.00000044835938398048083
	Average time pr. geometric median: 0.0000020213961869999175

