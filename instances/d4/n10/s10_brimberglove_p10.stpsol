All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 6.29128314099126
MST len: 7.5932864822
Steiner Ratio: 0.8285323035999149
Solution Steiner tree:
	Terminals:
	  #0: (0.2689884604,0.0923382296,0.8809632025,0.5625731631) [ #3 ]
	  #1: (0.8166862739,0.1834716542,0.584652962,0.4221560766) [ #11 ]
	  #2: (0.2458365472,0.6618976293,0.3858842744,0.2711707318) [ #5 ]
	  #4: (0.5658107323,0.6109299178,0.5057680786,0.1796468744) [ #9 ]
	  #6: (0.3317870611,0.5361120061,0.5565968144,0.8975977934) [ #7 ]
	  #8: (0.6635139085,0.9814409283,0.9619104471,0.1394470134) [ #15 ]
	  #10: (0.2234422426,0.2144171056,0.0119858943,0.8683905582) [ #13 ]
	  #12: (0.1470419239,0.0623648931,0.0772446674,0.9637281978) [ #13 ]
	  #14: (0.9767909204,0.9780582851,0.8738890034,0.0530768098) [ #15 ]
	  #16: (0.0253341845,0.3162595943,0.0612761933,0.083659018) [ #17 ]
	Steiner points:
	  #3: (0.2689884604,0.2144171056,0.5565968144,0.42216437866207074) [ #0 #7 #11 ]
	  #5: (0.3317870611,0.6109299185043958,0.5057680786,0.2711707318) [ #7 #2 #9 ]
	  #7: (0.3317870611,0.5361120061,0.5565968144,0.42216437866207074) [ #6 #3 #5 ]
	  #9: (0.5658107323,0.6109299185043958,0.5057680786,0.1796468744) [ #15 #4 #5 ]
	  #11: (0.2689884604,0.2144171056,0.5565968144,0.4221560766) [ #3 #1 #17 ]
	  #13: (0.14704192557266077,0.2144171056,0.07724466740000001,0.8683905582000001) [ #12 #10 #17 ]
	  #15: (0.6635139085,0.9780582851000001,0.8738890034,0.1394470134) [ #14 #9 #8 ]
	  #17: (0.14704192557266077,0.21442021349126003,0.07724466740000001,0.42215607659999993) [ #11 #13 #16 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 22.420666385
	Number of best updates: 28

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 957618

	Init times:
		Bottleneck Steiner distances: 0.000036544
		Smallest sphere distances: 0.00000494
		Sorting: 0.000029809
		Total init time: 0.000072557

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001467, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000007946, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000072454, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000824618, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.010161527, bsd pruned: 0, ss pruned: 945
		Level 6 - Time: 0.128493146, bsd pruned: 18900, ss pruned: 33180
		Level 7 - Time: 1.32115283, bsd pruned: 141540, ss pruned: 43680

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 876330
	Total number iterations 1787090 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0392888523729646
	Total time: 19.755814646
	Avarage time pr. topology: 0.000022543

Data for the geometric median iteration:
	Total time: 15.361443782
	Total steps: 34425070
	Average number of steps pr. geometric median: 4.910403329795853
	Average time pr. step: 0.00000044622839639832254
	Average time pr. geometric median: 0.000002191161403523787

