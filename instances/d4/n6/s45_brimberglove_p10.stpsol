All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.314469048307508
MST len: 4.8059050034999995
Steiner Ratio: 0.897743306446009
Solution Steiner tree:
	Terminals:
	  #0: (0.5214306472,0.2908011355,0.0775070079,0.6388154154) [ #3 ]
	  #1: (0.6265101371,0.8390606664,0.1431079023,0.7291413614) [ #7 ]
	  #2: (0.2152077799,0.2788681999,0.3929495487,0.2476127913) [ #5 ]
	  #4: (0.1934557334,0.1297003627,0.0635728929,0.1258999995) [ #5 ]
	  #6: (0.0076694484,0.9101628568,0.7197443544,0.7481080414) [ #7 ]
	  #8: (0.6089239803,0.0684510521,0.5965783874,0.8830592017) [ #9 ]
	Steiner points:
	  #3: (0.44529348159249227,0.2908011355,0.2831487829418938,0.6388154154) [ #5 #0 #9 ]
	  #5: (0.21520777989999998,0.27886819989999995,0.2831487829418938,0.2476127913) [ #4 #2 #3 ]
	  #7: (0.44529348159249227,0.8390606664,0.3876253177612625,0.7481080412468627) [ #1 #6 #9 ]
	  #9: (0.44529348159249227,0.2908011354999999,0.3876253177612625,0.7481080412468628) [ #3 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.001645337
	Number of best updates: 7

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 108

	Init times:
		Bottleneck Steiner distances: 0.000021703
		Smallest sphere distances: 0.000001811
		Sorting: 0.000003808
		Total init time: 0.000028373

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001794, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000007731, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000065812, bsd pruned: 0, ss pruned: 15

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 90
	Total number iterations 181 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.011111111111111
	Total time: 0.00141659
	Avarage time pr. topology: 0.000015739

Data for the geometric median iteration:
	Total time: 0.001156405
	Total steps: 2379
	Average number of steps pr. geometric median: 6.608333333333333
	Average time pr. step: 0.000000486088692728037
	Average time pr. geometric median: 0.0000032122361111111114

