All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.330750512569186
MST len: 2.4730864487775706
Steiner Ratio: 0.942446032859571
Solution Steiner tree:
	Terminals:
	  #0: (0.9910994321,0.8735950118,0.5318192572,0.054165287) [ #9 ]
	  #1: (0.8249852354,0.3957488953,0.5658004533,0.0595591958) [ #7 ]
	  #2: (0.845191426,0.7414528996,0.4705074068,0.3762617672) [ #5 ]
	  #4: (0.3297338986,0.9487606892,0.4132981074,0.5756729662) [ #5 ]
	  #6: (0.3771066863,0.354725022,0.9420667789,0.3395639064) [ #7 ]
	  #8: (0.8256802293,0.8959328252,0.2042499758,0.0233809524) [ #9 ]
	Steiner points:
	  #3: (0.8392801880992568,0.7126630625492064,0.4889311145354203,0.22929584610120138) [ #7 #5 #9 ]
	  #5: (0.8295133821870034,0.7439933168516571,0.47107218471914347,0.3642972348400494) [ #2 #4 #3 ]
	  #7: (0.7663068436081113,0.48090619240141697,0.5964913782497107,0.14749669775041246) [ #1 #3 #6 ]
	  #9: (0.9008703047794232,0.8233362917505579,0.44185279435648406,0.1073714603580067) [ #0 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.004924567
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 108

	Init times:
		Bottleneck Steiner distances: 0.000005863
		Smallest sphere distances: 0.000000666
		Sorting: 0.000001174
		Total init time: 0.000021971

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000869, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003395, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000027449, bsd pruned: 0, ss pruned: 15

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 90
	Total number iterations 924 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 10.266666666666667
	Total time: 0.004818703
	Avarage time pr. topology: 0.000053541

Data for the geometric median iteration:
	Total time: 0.004560033
	Total steps: 27634
	Average number of steps pr. geometric median: 76.7611111111111
	Average time pr. step: 0.00000016501530723022364
	Average time pr. geometric median: 0.000012666758333333332

