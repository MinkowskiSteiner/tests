All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.3454889392137765
MST len: 2.5863257602975587
Steiner Ratio: 0.9068807090039294
Solution Steiner tree:
	Terminals:
	  #0: (0.7084588719,0.4244470016,0.6334648415,0.41691941) [ #3 ]
	  #1: (0.5748512617,0.1829102199,0.3235021184,0.6120159149) [ #9 ]
	  #2: (0.4673976025,0.8627957766,0.6898134,0.18037494) [ #5 ]
	  #4: (0.1827713406,0.9935369142,0.1363650975,0.5229908035) [ #5 ]
	  #6: (0.3503611406,0.4857762337,0.7851141266,0.5901703474) [ #7 ]
	  #8: (0.5982263575,0.1752538104,0.4766825421,0.1399146664) [ #9 ]
	Steiner points:
	  #3: (0.6387340573242535,0.41305719206159386,0.6045159275845512,0.41715372595820216) [ #7 #0 #9 ]
	  #5: (0.4214520834470282,0.764046624664473,0.5836613547971314,0.34266121701693897) [ #2 #4 #7 ]
	  #7: (0.4753520762397436,0.5421986028456716,0.6602435616630774,0.4543567967826335) [ #3 #5 #6 ]
	  #9: (0.6104160126314754,0.2844904811220878,0.4944988838473099,0.3877149614334365) [ #1 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.006534458
	Number of best updates: 13

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000006934
		Smallest sphere distances: 0.000000644
		Sorting: 0.000001526
		Total init time: 0.000010347

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000857, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003743, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032565, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 1080 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 10.285714285714286
	Total time: 0.006419889
	Avarage time pr. topology: 0.000061141

Data for the geometric median iteration:
	Total time: 0.006110599
	Total steps: 38205
	Average number of steps pr. geometric median: 90.96428571428571
	Average time pr. step: 0.0000001599423897395629
	Average time pr. geometric median: 0.000014549045238095238

