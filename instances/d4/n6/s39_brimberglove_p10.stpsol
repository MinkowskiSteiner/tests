All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.456301341783809
MST len: 4.1282474186
Steiner Ratio: 0.837232120878049
Solution Steiner tree:
	Terminals:
	  #0: (0.4010537497,0.5862380008,0.0521304696,0.9748592134) [ #9 ]
	  #1: (0.0309723057,0.7538297781,0.4828030474,0.7509467996) [ #5 ]
	  #2: (0.3832746434,0.7694257664,0.3322590768,0.7771537661) [ #3 ]
	  #4: (0.2591224784,0.303527225,0.3381346959,0.3982325915) [ #7 ]
	  #6: (0.6123015828,0.975351023,0.6286001725,0.3799427857) [ #7 ]
	  #8: (0.9571484462,0.5881108905,0.2816415891,0.9608924622) [ #9 ]
	Steiner points:
	  #3: (0.3832746434,0.7538297781,0.3381346959,0.7771537661) [ #5 #2 #9 ]
	  #5: (0.37684884641425503,0.7538297781,0.3381346959,0.7509467996) [ #1 #3 #7 ]
	  #7: (0.37684884641425503,0.7538297781,0.33813912798380913,0.39823259149999995) [ #4 #5 #6 ]
	  #9: (0.4010537497000001,0.5881108905,0.2816415891,0.9608924622) [ #0 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.001518899
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 108

	Init times:
		Bottleneck Steiner distances: 0.000009824
		Smallest sphere distances: 0.000001845
		Sorting: 0.000004555
		Total init time: 0.000030909

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001443, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000007358, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000063696, bsd pruned: 0, ss pruned: 15

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 90
	Total number iterations 183 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.033333333333333
	Total time: 0.001321648
	Avarage time pr. topology: 0.000014684

Data for the geometric median iteration:
	Total time: 0.001080617
	Total steps: 2362
	Average number of steps pr. geometric median: 6.561111111111111
	Average time pr. step: 0.00000045750084674005085
	Average time pr. geometric median: 0.000003001713888888889

