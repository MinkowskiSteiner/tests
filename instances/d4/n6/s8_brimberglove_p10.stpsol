All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.336886971514416
MST len: 5.5216955858
Steiner Ratio: 0.7854266690593149
Solution Steiner tree:
	Terminals:
	  #0: (0.5345170878,0.886807702,0.5707898632,0.9158954317) [ #9 ]
	  #1: (0.6820132009,0.3835517202,0.9017543196,0.6433020787) [ #7 ]
	  #2: (0.9220907362,0.6533997663,0.55006754,0.2691321919) [ #5 ]
	  #4: (0.9163497486,0.5770127282,0.0059619849,0.751268397) [ #5 ]
	  #6: (0.9542649202,0.248271084,0.8679820145,0.8387081427) [ #7 ]
	  #8: (0.3527607286,0.7895895954,0.9058258696,0.043887435) [ #9 ]
	Steiner points:
	  #3: (0.7727637739999982,0.653399766299865,0.7417090186368815,0.6433020786999998) [ #7 #5 #9 ]
	  #5: (0.9163497486000001,0.653399766299865,0.55006754,0.6433020786999998) [ #2 #4 #3 ]
	  #7: (0.7727637739999982,0.3835517202,0.8679820145,0.6433050603144161) [ #1 #3 #6 ]
	  #9: (0.5345170878,0.7895895954,0.7417090186368815,0.6433020786999999) [ #0 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.003131284
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 108

	Init times:
		Bottleneck Steiner distances: 0.000017681
		Smallest sphere distances: 0.000004949
		Sorting: 0.000006644
		Total init time: 0.000031806

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002755, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000014539, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000115821, bsd pruned: 0, ss pruned: 15

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 90
	Total number iterations 181 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.011111111111111
	Total time: 0.002769745
	Avarage time pr. topology: 0.000030774

Data for the geometric median iteration:
	Total time: 0.002283951
	Total steps: 2560
	Average number of steps pr. geometric median: 7.111111111111111
	Average time pr. step: 0.0000008921683593750001
	Average time pr. geometric median: 0.000006344308333333334

Data for the geometric median step function:
	Total number of fixed points encountered: 5155

