All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.251020025620747
MST len: 2.3982086916101304
Steiner Ratio: 0.9386255806242773
Solution Steiner tree:
	Terminals:
	  #0: (0.0367873907,0.8458900362,0.860883003,0.4750771162) [ #5 ]
	  #1: (0.517155105,0.0449436945,0.4524295369,0.442212973) [ #3 ]
	  #2: (0.0224596341,0.3412099454,0.230164813,0.4956663146) [ #7 ]
	  #4: (0.7013661609,0.408563846,0.7201248397,0.8276713341) [ #5 ]
	  #6: (0.1277577337,0.1569511728,0.0837527169,0.7310733133) [ #7 ]
	Steiner points:
	  #3: (0.3729617395290791,0.23752147089851078,0.4486222153837893,0.5508618225320309) [ #1 #5 #7 ]
	  #5: (0.4226581196069067,0.4110700066959244,0.6197935225682663,0.6318738319197412) [ #4 #3 #0 ]
	  #7: (0.13798099538417627,0.25805299959601,0.23432964701324002,0.583864554257153) [ #2 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000675549
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000004767
		Smallest sphere distances: 0.000000495
		Sorting: 0.00000093
		Total init time: 0.000007302

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000085, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003382, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 111 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 7.4
	Total time: 0.000653045
	Avarage time pr. topology: 0.000043536

Data for the geometric median iteration:
	Total time: 0.000625764
	Total steps: 3694
	Average number of steps pr. geometric median: 82.08888888888889
	Average time pr. step: 0.0000001694001082837033
	Average time pr. geometric median: 0.000013905866666666667

