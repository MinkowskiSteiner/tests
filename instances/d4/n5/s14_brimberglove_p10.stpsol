All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.1477446942999996
MST len: 4.0306346657
Steiner Ratio: 0.7809550989790167
Solution Steiner tree:
	Terminals:
	  #0: (0.581988618,0.1856662101,0.839495378,0.391475477) [ #7 ]
	  #1: (0.746623855,0.6549854328,0.0995092923,0.7305007729) [ #5 ]
	  #2: (0.4961270823,0.6485379798,0.1537148399,0.2108905191) [ #3 ]
	  #4: (0.0529571777,0.5661688692,0.0288339364,0.6024998616) [ #5 ]
	  #6: (0.9994984996,0.2633149048,0.2158478607,0.460222048) [ #7 ]
	Steiner points:
	  #3: (0.4961270823,0.6485379798,0.1537148399,0.4415730518342766) [ #5 #2 #7 ]
	  #5: (0.4961270823,0.6485379798,0.0995092923,0.6024998616) [ #1 #3 #4 ]
	  #7: (0.581988618,0.2633149048,0.2158478607,0.4415730518342766) [ #0 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000584435
	Number of best updates: 2

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000011304
		Smallest sphere distances: 0.00000263
		Sorting: 0.000004011
		Total init time: 0.000019418

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002515, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000017185, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000489607
	Avarage time pr. topology: 0.00003264

Data for the geometric median iteration:
	Total time: 0.000371639
	Total steps: 319
	Average number of steps pr. geometric median: 7.088888888888889
	Average time pr. step: 0.000001165012539184953
	Average time pr. geometric median: 0.000008258644444444443

Data for the geometric median step function:
	Total number of fixed points encountered: 606

