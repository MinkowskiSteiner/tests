All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.447889102957614
MST len: 5.2001228907999995
Steiner Ratio: 0.8553430748390141
Solution Steiner tree:
	Terminals:
	  #0: (0.0740991244,0.9287021542,0.8842824203,0.0917933952) [ #3 ]
	  #1: (0.206965448,0.6995153961,0.2034307295,0.3588790108) [ #5 ]
	  #2: (0.0898954501,0.3307895457,0.6858023101,0.9032684844) [ #3 ]
	  #4: (0.7207948913,0.7304651908,0.05407485,0.9231773037) [ #5 ]
	  #6: (0.884531313,0.4632373589,0.7227528005,0.6292115332) [ #7 ]
	Steiner points:
	  #3: (0.0898954501,0.6207560503666661,0.6858023101,0.6136559453423867) [ #2 #0 #7 ]
	  #5: (0.5470050563445157,0.6995153961,0.2034307295,0.6136559453423867) [ #1 #4 #7 ]
	  #7: (0.5470050563445158,0.6207560503666661,0.6858023101,0.6136559453423867) [ #3 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000673208
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000031434
		Smallest sphere distances: 0.000004487
		Sorting: 0.00000591
		Total init time: 0.000044722

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000003226, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000020821, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.00055353
	Avarage time pr. topology: 0.000036902

Data for the geometric median iteration:
	Total time: 0.000466072
	Total steps: 303
	Average number of steps pr. geometric median: 6.733333333333333
	Average time pr. step: 0.000001538191419141914
	Average time pr. geometric median: 0.000010357155555555554

Data for the geometric median step function:
	Total number of fixed points encountered: 580

