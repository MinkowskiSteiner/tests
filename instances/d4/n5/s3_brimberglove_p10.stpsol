All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.2322245655806587
MST len: 3.6191545087000003
Steiner Ratio: 0.8930883049648171
Solution Steiner tree:
	Terminals:
	  #0: (0.0099969395,0.257080875,0.0407533725,0.4439474528) [ #7 ]
	  #1: (0.5613801752,0.2249833128,0.3930917794,0.4439383934) [ #3 ]
	  #2: (0.2850412509,0.1447810513,0.5635544996,0.8646787037) [ #5 ]
	  #4: (0.1763232952,0.6261662299,0.9448735988,0.8460845993) [ #5 ]
	  #6: (0.8954018363,0.2308046744,0.0040116059,0.4605808875) [ #7 ]
	Steiner points:
	  #3: (0.396813342098219,0.24107858119259257,0.39309177940000006,0.490466089144033) [ #1 #5 #7 ]
	  #5: (0.2850412509,0.24107858119259262,0.5635544996,0.8460845993) [ #2 #3 #4 ]
	  #7: (0.396813342098219,0.24107858119259257,0.04075337250000001,0.490466089144033) [ #0 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000528366
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 15

	Init times:
		Bottleneck Steiner distances: 0.000012522
		Smallest sphere distances: 0.000002769
		Sorting: 0.00001772
		Total init time: 0.000035023

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002534, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000014624, bsd pruned: 3, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 12
	Total number iterations 24 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000438747
	Avarage time pr. topology: 0.000036562

Data for the geometric median iteration:
	Total time: 0.000361819
	Total steps: 267
	Average number of steps pr. geometric median: 7.416666666666667
	Average time pr. step: 0.00000135512734082397
	Average time pr. geometric median: 0.000010050527777777777

Data for the geometric median step function:
	Total number of fixed points encountered: 361

