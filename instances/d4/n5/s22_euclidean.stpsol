All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.9186851158794114
MST len: 2.0430692547497875
Steiner Ratio: 0.9391189806311243
Solution Steiner tree:
	Terminals:
	  #0: (0.4461416623,0.7561149335,0.1936676377,0.5023851071) [ #3 ]
	  #1: (0.1862228323,0.3292264125,0.6730409189,0.520684405) [ #5 ]
	  #2: (0.5234706907,0.8290278501,0.414524392,0.4607495118) [ #3 ]
	  #4: (0.2885274982,0.0973462291,0.2666000273,0.563644016) [ #5 ]
	  #6: (0.8650269224,0.5663939652,0.6338536677,0.0191505631) [ #7 ]
	Steiner points:
	  #3: (0.5051553040517623,0.7624330193864055,0.37724730979628796,0.45163861374748177) [ #2 #0 #7 ]
	  #5: (0.2991746225385424,0.33763039457287575,0.507990154261161,0.49665441634034396) [ #1 #4 #7 ]
	  #7: (0.5170325984016682,0.6083036007552153,0.4620516286845216,0.3816762817303653) [ #3 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000804407
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.00000494
		Smallest sphere distances: 0.000000554
		Sorting: 0.000000974
		Total init time: 0.000007565

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000914, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003685, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 143 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 9.533333333333333
	Total time: 0.000778835
	Avarage time pr. topology: 0.000051922

Data for the geometric median iteration:
	Total time: 0.000745075
	Total steps: 4154
	Average number of steps pr. geometric median: 92.31111111111112
	Average time pr. step: 0.00000017936326432354358
	Average time pr. geometric median: 0.000016557222222222222

