All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.082541997598492
MST len: 2.221675485732643
Steiner Ratio: 0.9373745225044562
Solution Steiner tree:
	Terminals:
	  #0: (0.387681659,0.2726833854,0.3569481244,0.0666660061) [ #5 ]
	  #1: (0.9065222973,0.5735588286,0.6033617564,0.7889988608) [ #7 ]
	  #2: (0.4878506383,0.4318964698,0.8467889367,0.5935844293) [ #3 ]
	  #4: (0.2660512897,0.022327458,0.7685739201,0.5612500564) [ #5 ]
	  #6: (0.6702460561,0.9116797088,0.38213495,0.5965716008) [ #7 ]
	Steiner points:
	  #3: (0.4947767353179856,0.4250258500237226,0.8157403198855985,0.5883940359096493) [ #2 #5 #7 ]
	  #5: (0.377788468527738,0.22630675323202798,0.7041481635178956,0.4725598966999975) [ #4 #3 #0 ]
	  #7: (0.7452986579069582,0.6244223906083879,0.5981655881611019,0.6905401865273428) [ #1 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000507693
	Number of best updates: 2

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 15

	Init times:
		Bottleneck Steiner distances: 0.000004773
		Smallest sphere distances: 0.00000051
		Sorting: 0.000000883
		Total init time: 0.000007217

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000921, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003461, bsd pruned: 0, ss pruned: 3

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 12
	Total number iterations 59 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 4.916666666666667
	Total time: 0.000484358
	Avarage time pr. topology: 0.000040363

Data for the geometric median iteration:
	Total time: 0.000467689
	Total steps: 2702
	Average number of steps pr. geometric median: 75.05555555555556
	Average time pr. step: 0.0000001730899333826795
	Average time pr. geometric median: 0.000012991361111111111

