All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.871382049490239
MST len: 1.9587193139706764
Steiner Ratio: 0.9554110362533828
Solution Steiner tree:
	Terminals:
	  #0: (0.0309723057,0.7538297781,0.4828030474,0.7509467996) [ #5 ]
	  #1: (0.4010537497,0.5862380008,0.0521304696,0.9748592134) [ #7 ]
	  #2: (0.3832746434,0.7694257664,0.3322590768,0.7771537661) [ #3 ]
	  #4: (0.6123015828,0.975351023,0.6286001725,0.3799427857) [ #5 ]
	  #6: (0.9571484462,0.5881108905,0.2816415891,0.9608924622) [ #7 ]
	Steiner points:
	  #3: (0.3832749701505945,0.7694240769449253,0.33225998905021936,0.7771534223763827) [ #2 #5 #7 ]
	  #5: (0.337968602630775,0.7930631115579148,0.403252996692028,0.7195251686321653) [ #4 #3 #0 ]
	  #7: (0.5004813224084315,0.6542069958209469,0.19926723636070945,0.899226102350053) [ #1 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000516633
	Number of best updates: 2

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000005161
		Smallest sphere distances: 0.000000674
		Sorting: 0.000000959
		Total init time: 0.000008244

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001246, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000004493, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 88 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 5.866666666666666
	Total time: 0.000489396
	Avarage time pr. topology: 0.000032626

Data for the geometric median iteration:
	Total time: 0.000465162
	Total steps: 2685
	Average number of steps pr. geometric median: 59.666666666666664
	Average time pr. step: 0.00000017324469273743015
	Average time pr. geometric median: 0.000010336933333333331

