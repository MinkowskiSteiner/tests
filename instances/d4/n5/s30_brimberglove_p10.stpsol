All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.014979406875714
MST len: 3.2493602744
Steiner Ratio: 0.9278686117477186
Solution Steiner tree:
	Terminals:
	  #0: (0.3946801244,0.8697485588,0.2358092783,0.2918461092) [ #7 ]
	  #1: (0.6298717496,0.5035535896,0.7460086135,0.8155644125) [ #5 ]
	  #2: (0.995645286,0.5080417518,0.6990755753,0.2370779334) [ #3 ]
	  #4: (0.9940509931,0.5953307052,0.7999637326,0.8187845688) [ #5 ]
	  #6: (0.229967075,0.8697586934,0.5547460311,0.5776126504) [ #7 ]
	Steiner points:
	  #3: (0.7512648307666667,0.5080417518,0.6990755753,0.4694649548271604) [ #5 #2 #7 ]
	  #5: (0.7512648307666667,0.5080417518,0.7460086135,0.8155644124999849) [ #1 #3 #4 ]
	  #7: (0.3946801243999999,0.8697482945243015,0.5547460311,0.46946495482716044) [ #0 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000208293
	Number of best updates: 2

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 12

	Init times:
		Bottleneck Steiner distances: 0.000008628
		Smallest sphere distances: 0.000001638
		Sorting: 0.000002203
		Total init time: 0.000013989

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002304, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000007946, bsd pruned: 0, ss pruned: 6

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 9
	Total number iterations 18 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000163785
	Avarage time pr. topology: 0.000018198

Data for the geometric median iteration:
	Total time: 0.000138106
	Total steps: 225
	Average number of steps pr. geometric median: 8.333333333333334
	Average time pr. step: 0.0000006138044444444445
	Average time pr. geometric median: 0.000005115037037037038

