All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.006091429
MST len: 3.4867211821
Steiner Ratio: 0.8621542337347078
Solution Steiner tree:
	Terminals:
	  #0: (0.8095185197,0.5094773627,0.2020136715,0.8590005528) [ #3 ]
	  #1: (0.1044992358,0.7127510368,0.2668925339,0.4992394254) [ #7 ]
	  #2: (0.8234275756,0.3046993158,0.100103484,0.3428378414) [ #5 ]
	  #4: (0.8988103708,0.2810884273,0.6454682083,0.0593314241) [ #5 ]
	  #6: (0.0877142293,0.8987790253,0.2479340957,0.6666313995) [ #7 ]
	Steiner points:
	  #3: (0.8095185197,0.5094773627,0.2020136715,0.54105008045502) [ #0 #5 #7 ]
	  #5: (0.8234275756,0.3046993157999999,0.2020136715,0.3428378414) [ #2 #3 #4 ]
	  #7: (0.1044992358,0.7127510368000001,0.2479340957,0.54105008045502) [ #1 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000111174
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 9

	Init times:
		Bottleneck Steiner distances: 0.000007132
		Smallest sphere distances: 0.000001306
		Sorting: 0.000002047
		Total init time: 0.000011457

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001479, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000004264, bsd pruned: 0, ss pruned: 9

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 6
	Total number iterations 12 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000080629
	Avarage time pr. topology: 0.000013438

Data for the geometric median iteration:
	Total time: 0.000067171
	Total steps: 143
	Average number of steps pr. geometric median: 7.944444444444445
	Average time pr. step: 0.0000004697272727272727
	Average time pr. geometric median: 0.000003731722222222222

