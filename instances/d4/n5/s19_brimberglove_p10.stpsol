All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.282360261444445
MST len: 3.6389989381000003
Steiner Ratio: 0.9019953886434042
Solution Steiner tree:
	Terminals:
	  #0: (0.9015435013,0.9686559904,0.617568895,0.5169671907) [ #5 ]
	  #1: (0.273373468,0.0765914861,0.6450726547,0.0391567499) [ #7 ]
	  #2: (0.2219586108,0.6696780285,0.3323929949,0.5761897869) [ #3 ]
	  #4: (0.586565987,0.9423629041,0.7965240673,0.6874572605) [ #5 ]
	  #6: (0.2902333854,0.3277869785,0.2285638117,0.0594177293) [ #7 ]
	Steiner points:
	  #3: (0.34307734892222225,0.6696780285,0.46843821597777774,0.5272874533830705) [ #2 #5 #7 ]
	  #5: (0.586565987,0.9423629041000001,0.617568895,0.5272874533830705) [ #4 #3 #0 ]
	  #7: (0.34307734892222225,0.32778697849999994,0.46843821597777774,0.0594177293) [ #1 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000345424
	Number of best updates: 1

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 12

	Init times:
		Bottleneck Steiner distances: 0.000015975
		Smallest sphere distances: 0.000003662
		Sorting: 0.000005982
		Total init time: 0.000028243

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002906, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000012757, bsd pruned: 0, ss pruned: 6

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 9
	Total number iterations 18 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000265461
	Avarage time pr. topology: 0.000029495

Data for the geometric median iteration:
	Total time: 0.000218312
	Total steps: 191
	Average number of steps pr. geometric median: 7.074074074074074
	Average time pr. step: 0.0000011429947643979058
	Average time pr. geometric median: 0.00000808562962962963

Data for the geometric median step function:
	Total number of fixed points encountered: 302

