All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 10.871967612778366
MST len: 13.3536043814
Steiner Ratio: 0.8141597805549592
Solution Steiner tree:
	Terminals:
	  #0: (0.3548297497,0.954948144,0.4252187523,0.2287187526,0.0080249533,0.6942072132) [ #5 ]
	  #1: (0.2458365472,0.6618976293,0.3858842744,0.2711707318,0.9781572241,0.4471604677) [ #13 ]
	  #2: (0.5658107323,0.6109299178,0.5057680786,0.1796468744,0.8166862739,0.1834716542) [ #3 ]
	  #4: (0.5565968144,0.8975977934,0.1470419239,0.0623648931,0.0772446674,0.9637281978) [ #5 ]
	  #6: (0.8809632025,0.5625731631,0.6635139085,0.9814409283,0.9619104471,0.1394470134) [ #11 ]
	  #8: (0.584652962,0.4221560766,0.0253341845,0.3162595943,0.0612761933,0.083659018) [ #15 ]
	  #10: (0.3210569827,0.8889881558,0.2567803758,0.9845708911,0.8704290836,0.2186908225) [ #11 ]
	  #12: (0.1240179041,0.0938713258,0.4331079281,0.1360037984,0.962261884,0.7648949897) [ #13 ]
	  #14: (0.2234422426,0.2144171056,0.0119858943,0.8683905582,0.3317870611,0.5361120061) [ #15 ]
	  #16: (0.9767909204,0.9780582851,0.8738890034,0.0530768098,0.2689884604,0.0923382296) [ #17 ]
	Steiner points:
	  #3: (0.5658107323,0.6618977509605607,0.5057680786,0.1796468744,0.8166862739,0.18347165420000003) [ #13 #2 #7 ]
	  #5: (0.5565968144,0.8975977934,0.4252187523,0.2287187526,0.0772446674,0.6942072132) [ #4 #0 #17 ]
	  #7: (0.5658291225823544,0.6618977509605607,0.5057680786,0.31625959429999606,0.8166862739,0.18347165420000003) [ #11 #9 #3 ]
	  #9: (0.5565968143964792,0.6618977509605607,0.5057680785982601,0.31625959429999606,0.19692916520388498,0.18347165420000003) [ #15 #7 #17 ]
	  #11: (0.5658291225823544,0.6618977509605607,0.5057680786,0.9814409283000001,0.8704290836,0.18347165420000003) [ #10 #7 #6 ]
	  #13: (0.2458365472,0.6618977509605607,0.4331079281,0.1796468744,0.962261884,0.4471604677) [ #3 #12 #1 ]
	  #15: (0.5565968143964792,0.42215607660000004,0.0253341845,0.3162595953284221,0.19692916520388498,0.18347165420000003) [ #8 #9 #14 ]
	  #17: (0.5566000202003957,0.8975977934000001,0.5057680785982601,0.2287187526,0.19692916520388498,0.18347165420000006) [ #5 #9 #16 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 69.49913491
	Number of best updates: 32

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 2038488

	Init times:
		Bottleneck Steiner distances: 0.000033147
		Smallest sphere distances: 0.000006664
		Sorting: 0.000036392
		Total init time: 0.000077474

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001693, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000009576, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000086852, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000977635, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.013032998, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.200262059, bsd pruned: 0, ss pruned: 0
		Level 7 - Time: 3.326896266, bsd pruned: 0, ss pruned: 135135

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 1891890
	Total number iterations 3870660 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0459223316366173
	Total time: 62.718015858
	Avarage time pr. topology: 0.00003315

Data for the geometric median iteration:
	Total time: 50.311967409
	Total steps: 75597895
	Average number of steps pr. geometric median: 4.994865914508772
	Average time pr. step: 0.0000006655207451080483
	Average time pr. geometric median: 0.0000033241868851386708

