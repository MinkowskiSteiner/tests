All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 10.093875746413596
MST len: 12.457977145800001
Steiner Ratio: 0.8102339270879605
Solution Steiner tree:
	Terminals:
	  #0: (0.0059619849,0.751268397,0.5345170878,0.886807702,0.5707898632,0.9158954317) [ #3 ]
	  #1: (0.1945958953,0.7668709838,0.4508856048,0.0963502145,0.410173062,0.3672353534) [ #11 ]
	  #2: (0.8492760145,0.5195885676,0.7242932826,0.7551018837,0.5634760026,0.6785582023) [ #13 ]
	  #4: (0.6820132009,0.3835517202,0.9017543196,0.6433020787,0.9163497486,0.5770127282) [ #5 ]
	  #6: (0.8679820145,0.8387081427,0.9220907362,0.6533997663,0.55006754,0.2691321919) [ #7 ]
	  #8: (0.1237754617,0.5839534754,0.96214796,0.738404279,0.5816928249,0.128975798) [ #9 ]
	  #10: (0.2810107266,0.2728543464,0.6146288172,0.9977393495,0.1668278385,0.9347036872) [ #15 ]
	  #12: (0.6733629432,0.4161350468,0.1185037499,0.2078800305,0.3029427488,0.689293613) [ #13 ]
	  #14: (0.0033729677,0.4314580166,0.517266345,0.9254637039,0.0848577824,0.0673338846) [ #15 ]
	  #16: (0.3527607286,0.7895895954,0.9058258696,0.043887435,0.9542649202,0.248271084) [ #17 ]
	Steiner points:
	  #3: (0.19459591344549318,0.748966868956219,0.6146288172,0.738405033801694,0.5707898632,0.3672353534) [ #11 #0 #9 ]
	  #5: (0.6733629432,0.5195885676,0.9017543196,0.6433020787,0.5816928249,0.5770127282) [ #13 #4 #7 ]
	  #7: (0.6733629431999998,0.7184785199693053,0.9017543196000001,0.6533997663,0.5816928249,0.2691321919) [ #5 #6 #17 ]
	  #9: (0.19459591344549318,0.7184785199693052,0.9031115029333334,0.7384009639312434,0.5816928249,0.2621784892666666) [ #3 #8 #17 ]
	  #11: (0.19459591344549318,0.7489668689562189,0.6146288172,0.7384050338016939,0.410173062,0.3672353534) [ #15 #3 #1 ]
	  #13: (0.6733629432,0.5195885676,0.7242932826,0.6433020787,0.5634760026,0.6785582023) [ #5 #12 #2 ]
	  #15: (0.19459591344549318,0.43145801660000005,0.6146288171546621,0.9254637039,0.1668278385,0.3672353534000001) [ #10 #11 #14 ]
	  #17: (0.3527607286,0.7184785199693052,0.9031115029333334,0.6533997663,0.58169566737782,0.2621784892666666) [ #7 #9 #16 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 62.761926346
	Number of best updates: 29

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1768218

	Init times:
		Bottleneck Steiner distances: 0.000057869
		Smallest sphere distances: 0.000006693
		Sorting: 0.000028533
		Total init time: 0.000094728

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001622, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00000952, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000088352, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000999661, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.013213386, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.203463181, bsd pruned: 0, ss pruned: 0
		Level 7 - Time: 3.000787465, bsd pruned: 135135, ss pruned: 270270

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 1621620
	Total number iterations 3310581 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0415269915269914
	Total time: 56.68656808
	Avarage time pr. topology: 0.000034956

Data for the geometric median iteration:
	Total time: 46.011340963
	Total steps: 65420366
	Average number of steps pr. geometric median: 5.042824921991588
	Average time pr. step: 0.0000007033183055411216
	Average time pr. geometric median: 0.0000035467110792756626

