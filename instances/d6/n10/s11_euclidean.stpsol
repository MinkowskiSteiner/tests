All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.821301551455109
MST len: 5.305000550321598
Steiner Ratio: 0.9088220643375492
Solution Steiner tree:
	Terminals:
	  #0: (0.9778233603,0.0999500035,0.4717727618,0.3465953233,0.6167704093,0.4288659084) [ #11 ]
	  #1: (0.8967377799,0.3141340824,0.3612824978,0.7296536866,0.7182401273,0.4365231644) [ #5 ]
	  #2: (0.9263453185,0.5264996991,0.3097808907,0.5022105041,0.7537675108,0.4059770589) [ #7 ]
	  #4: (0.1809798303,0.3634571686,0.3866200426,0.490760721,0.8656676732,0.1403875533) [ #9 ]
	  #6: (0.9654330178,0.4460615546,0.0395826991,0.6953234704,0.5171647261,0.4766945497) [ #17 ]
	  #8: (0.3817098766,0.0352495974,0.3687719635,0.5168204054,0.9570931466,0.3607239967) [ #9 ]
	  #10: (0.4484664097,0.2208949445,0.8329159067,0.4041060449,0.0752406661,0.2481696737) [ #15 ]
	  #12: (0.70731932,0.1956754286,0.1610363345,0.9483695021,0.9867749587,0.3349739184) [ #13 ]
	  #14: (0.4592865964,0.7046087811,0.729890453,0.0711031715,0.4371118506,0.8601203439) [ #15 ]
	  #16: (0.5789050197,0.7321704266,0.2410501816,0.7910995301,0.1739375839,0.0170635157) [ #17 ]
	Steiner points:
	  #3: (0.8866744784316208,0.36224491680516835,0.34452440016862235,0.6028478375117796,0.6684921610563063,0.42154353012326884) [ #11 #7 #5 ]
	  #5: (0.8800825933916657,0.3152156288822786,0.3525149714707756,0.7122605264634747,0.7219274095776421,0.4287651500115383) [ #1 #13 #3 ]
	  #7: (0.8983950887948701,0.43319930485475616,0.29629281581948047,0.588166655382685,0.662539182280292,0.4107737224745428) [ #2 #3 #17 ]
	  #9: (0.403410150540062,0.14322350941330933,0.352799416520343,0.5643829371178063,0.9220812310977806,0.314565848016892) [ #13 #8 #4 ]
	  #11: (0.8675337980198068,0.24879540408478923,0.4683816053871324,0.4314756482740495,0.5819718867310237,0.43943542668703345) [ #15 #3 #0 ]
	  #13: (0.6844451896781821,0.22149147795756488,0.27515176456275053,0.7687314005894789,0.8818474613584502,0.3607028564605659) [ #9 #12 #5 ]
	  #15: (0.6138675397734122,0.36424095856345934,0.6633895221555036,0.32347637435820914,0.37080503317032215,0.48997367121452107) [ #10 #11 #14 ]
	  #17: (0.8900437404087447,0.475780891286353,0.1714548522748177,0.6623368004135576,0.5357577326118609,0.39280617306720433) [ #6 #7 #16 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 226.32177648
	Number of best updates: 36

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 2173623

	Init times:
		Bottleneck Steiner distances: 0.000016315
		Smallest sphere distances: 0.000010728
		Sorting: 0.000014016
		Total init time: 0.000042613

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000945, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003487, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032656, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000366111, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004767297, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.072196042, bsd pruned: 0, ss pruned: 0
		Level 7 - Time: 1.25575627, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 2027025
	Total number iterations 30547385 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 15.070058336725003
	Total time: 224.051740506
	Avarage time pr. topology: 0.000110531

Data for the geometric median iteration:
	Total time: 209.545151952
	Total steps: 1088008123
	Average number of steps pr. geometric median: 67.09390134556801
	Average time pr. step: 0.00000019259520910029087
	Average time pr. geometric median: 0.000012921963959003959

