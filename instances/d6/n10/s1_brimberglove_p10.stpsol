All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 9.929397422652583
MST len: 12.402324931299999
Steiner Ratio: 0.8006077471485658
Solution Steiner tree:
	Terminals:
	  #0: (0.493582987,0.9727750239,0.2925167844,0.7713576978,0.5267449792,0.7699138363) [ #5 ]
	  #1: (0.3352227557,0.7682295948,0.2777747108,0.5539699558,0.4773970519,0.6288709248) [ #3 ]
	  #2: (0.5129323944,0.8391122347,0.6126398326,0.2960316177,0.6375522677,0.5242871901) [ #7 ]
	  #4: (0.4002286221,0.891529452,0.283314746,0.3524583473,0.80772452,0.919026474) [ #5 ]
	  #6: (0.1566790893,0.4009443942,0.1297904468,0.108808802,0.998924518,0.2182569053) [ #13 ]
	  #8: (0.1416025554,0.6069688763,0.0163005716,0.2428867706,0.1372315768,0.8041767542) [ #15 ]
	  #10: (0.8401877172,0.3943829268,0.7830992238,0.7984400335,0.9116473579,0.1975513693) [ #17 ]
	  #12: (0.8902326025,0.3488929352,0.0641713208,0.0200230489,0.4577017373,0.0630958383) [ #13 ]
	  #14: (0.0697552762,0.9493270754,0.5259953502,0.0860558479,0.192213846,0.663226927) [ #15 ]
	  #16: (0.3647844728,0.5134009102,0.9522297252,0.916195068,0.635711728,0.7172969294) [ #17 ]
	Steiner points:
	  #3: (0.36512566834342,0.7682295948,0.2925167844,0.5539699558,0.5267449792,0.6288709472820733) [ #1 #5 #11 ]
	  #5: (0.4002286221,0.891529452,0.2925167844,0.5539699558,0.5267449792,0.7699138363) [ #4 #0 #3 ]
	  #7: (0.36512566834342,0.6069688763,0.6126398326,0.2960316177,0.635782701934048,0.6288709472820726) [ #2 #11 #9 ]
	  #9: (0.36512566833975313,0.6069688763,0.1297904468,0.19060979636947037,0.6357827019152118,0.6288709472820726) [ #13 #15 #7 ]
	  #11: (0.36512566834342,0.6069688763,0.6126398326,0.5539699558,0.635782701934048,0.6288709472820726) [ #3 #7 #17 ]
	  #13: (0.36512566833975313,0.4009443942,0.1297904468,0.19060979613979057,0.6357827019152118,0.2182569053) [ #9 #12 #6 ]
	  #15: (0.1416025554,0.6069688763,0.1297904468,0.19060979636947037,0.192213846,0.663226927) [ #14 #9 #8 ]
	  #17: (0.3651256683434406,0.5134009102,0.7830992238,0.7984400335,0.6357827174307007,0.6288709472820726) [ #10 #11 #16 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 70.939598455
	Number of best updates: 29

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 2038488

	Init times:
		Bottleneck Steiner distances: 0.000041981
		Smallest sphere distances: 0.000006547
		Sorting: 0.000036528
		Total init time: 0.000086611

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001939, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000009559, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000088019, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000995425, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.013177617, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.202298516, bsd pruned: 0, ss pruned: 0
		Level 7 - Time: 3.358536138, bsd pruned: 0, ss pruned: 135135

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 1891890
	Total number iterations 3838851 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.029108986251843
	Total time: 64.146786269
	Avarage time pr. topology: 0.000033905

Data for the geometric median iteration:
	Total time: 51.787361023
	Total steps: 73475498
	Average number of steps pr. geometric median: 4.854635972493115
	Average time pr. step: 0.0000007048249067056341
	Average time pr. geometric median: 0.0000034216683464022754

