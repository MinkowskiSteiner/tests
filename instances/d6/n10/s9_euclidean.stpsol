All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.567632358979603
MST len: 6.117842822205872
Steiner Ratio: 0.9100646291158093
Solution Steiner tree:
	Terminals:
	  #0: (0.7227528005,0.6292115332,0.7207948913,0.7304651908,0.05407485,0.9231773037) [ #7 ]
	  #1: (0.2488169443,0.8343745516,0.425641888,0.1330993646,0.9261679467,0.5155373381) [ #5 ]
	  #2: (0.4638889103,0.6119702568,0.4188058225,0.5586368472,0.2869822789,0.6803660592) [ #3 ]
	  #4: (0.206965448,0.6995153961,0.2034307295,0.3588790108,0.884531313,0.4632373589) [ #5 ]
	  #6: (0.9381766622,0.9481784259,0.9291220382,0.1416073912,0.3070574362,0.8136533512) [ #17 ]
	  #8: (0.6048447502,0.0298102363,0.4428648839,0.3256396411,0.7602754271,0.4969397343) [ #13 ]
	  #10: (0.9914577939,0.2373887199,0.3081078475,0.6704191042,0.7412129779,0.229606642) [ #11 ]
	  #12: (0.8316096942,0.2784400723,0.9177547795,0.1397175417,0.9488591766,0.658967757) [ #13 ]
	  #14: (0.6858023101,0.9032684844,0.0947479369,0.6750120226,0.2615602367,0.272972847) [ #15 ]
	  #16: (0.0740991244,0.9287021542,0.8842824203,0.0917933952,0.0898954501,0.3307895457) [ #17 ]
	Steiner points:
	  #3: (0.4649224158506195,0.6126263761360998,0.4193325477246979,0.5583346478726422,0.28729191152929867,0.6797218942143677) [ #7 #2 #15 ]
	  #5: (0.26634153104248187,0.7058721713430888,0.2754653404097725,0.3240562285468232,0.8511719727242081,0.4782375725285639) [ #9 #1 #4 ]
	  #7: (0.580730293315192,0.6890430598731135,0.630680225612312,0.5243085118516663,0.19080326465263284,0.7510171292492155) [ #0 #3 #17 ]
	  #9: (0.5061848563399256,0.5557830705425862,0.3353067473323998,0.4546178739030118,0.6284318215126178,0.48264337763562926) [ #11 #5 #15 ]
	  #11: (0.698585831433351,0.29143379266371583,0.4055863623763016,0.4532586627859971,0.7242857321776458,0.429172580583291) [ #9 #10 #13 ]
	  #13: (0.6707677614683522,0.16128816326174883,0.4938650391703769,0.34779511688240167,0.7726324398342532,0.4938249839148854) [ #12 #8 #11 ]
	  #15: (0.5241814623290433,0.6477758470617834,0.3232377316994259,0.5412724305201445,0.41369532789192076,0.52393444986642) [ #14 #9 #3 ]
	  #17: (0.5822369039312486,0.8154783316730597,0.7716657963855678,0.32135673017515465,0.204482576021798,0.6828323206100558) [ #6 #7 #16 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 233.197315602
	Number of best updates: 51

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 2038488

	Init times:
		Bottleneck Steiner distances: 0.00002474
		Smallest sphere distances: 0.000001741
		Sorting: 0.000013806
		Total init time: 0.000042134

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000818, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003582, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032974, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000368388, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.00480808, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.072346231, bsd pruned: 0, ss pruned: 0
		Level 7 - Time: 1.194546034, bsd pruned: 0, ss pruned: 135135

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 1891890
	Total number iterations 30298977 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 16.015189572332428
	Total time: 231.006472344
	Avarage time pr. topology: 0.000122103

Data for the geometric median iteration:
	Total time: 216.552380032
	Total steps: 1126302396
	Average number of steps pr. geometric median: 74.41648272362558
	Average time pr. step: 0.00000019226841814513906
	Average time pr. geometric median: 0.00001430793941719656

