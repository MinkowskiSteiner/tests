All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.274667866098966
MST len: 5.63612738563755
Steiner Ratio: 0.9358673970961542
Solution Steiner tree:
	Terminals:
	  #0: (0.3191112491,0.3110007436,0.691104038,0.1034285073,0.9779993249,0.8022396713) [ #3 ]
	  #1: (0.6177704891,0.4252223039,0.3870529828,0.7761541152,0.6623876005,0.1703382573) [ #9 ]
	  #2: (0.6132100036,0.1134380932,0.7647564228,0.4129372162,0.5164472929,0.8498845724) [ #3 ]
	  #4: (0.2185718884,0.16160391,0.0972843804,0.5284587664,0.1447218867,0.6269470568) [ #7 ]
	  #6: (0.6451566176,0.2473998085,0.3098868776,0.9831179767,0.5296626759,0.9438327159) [ #15 ]
	  #8: (0.4926030964,0.9586761212,0.3549673587,0.183707134,0.062104628,0.3329666836) [ #17 ]
	  #10: (0.87826215,0.7782872272,0.4166978609,0.2226562315,0.5511224272,0.0462225136) [ #13 ]
	  #12: (0.9352118568,0.8634107573,0.3815790147,0.3222648391,0.6395648721,0.0439666151) [ #13 ]
	  #14: (0.4722914819,0.0229840363,0.4052342835,0.8889893428,0.2456402682,0.9563567112) [ #15 ]
	  #16: (0.1354387687,0.9625167521,0.799727213,0.4030091993,0.0851281491,0.8056346727) [ #17 ]
	Steiner points:
	  #3: (0.5317265692416016,0.19443275608945038,0.6595725649882784,0.40359606630240125,0.5835393205882334,0.7781985287214743) [ #0 #2 #5 ]
	  #5: (0.5141713402060406,0.2648602679296748,0.4650035149506251,0.5567535351571653,0.47550134780523556,0.6443269714287555) [ #3 #9 #7 ]
	  #7: (0.44901187149225863,0.19541286731551943,0.34580958125827516,0.6567029615941062,0.3601635046130971,0.7288085789595713) [ #4 #15 #5 ]
	  #9: (0.6028877002136551,0.47053129580742853,0.42082743279179496,0.582503661003385,0.5348232176412616,0.33245594600160794) [ #11 #5 #1 ]
	  #11: (0.6647690100104104,0.7103119117242892,0.422161526995623,0.36998109379642974,0.43307203288918816,0.2494555622267807) [ #13 #9 #17 ]
	  #13: (0.8711787633376269,0.7885297644329768,0.41059925603172226,0.2534589324914253,0.557830722847671,0.06265858190953039) [ #10 #12 #11 ]
	  #15: (0.5125538924410316,0.1387853651788891,0.36037119955557145,0.8420640372947958,0.35968754938136166,0.8815733414166493) [ #14 #7 #6 ]
	  #17: (0.4804861452451473,0.8992626564733044,0.43807106719054223,0.2616744206605763,0.1551714329913511,0.38386462308252617) [ #8 #11 #16 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 160.570331356
	Number of best updates: 32

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1383603

	Init times:
		Bottleneck Steiner distances: 0.000026105
		Smallest sphere distances: 0.000001624
		Sorting: 0.000013984
		Total init time: 0.000043151

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000987, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003527, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032839, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000369933, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004900189, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.064989535, bsd pruned: 0, ss pruned: 20790
		Level 7 - Time: 0.85183157, bsd pruned: 0, ss pruned: 457380

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 1257795
	Total number iterations 21679979 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 17.236496408397233
	Total time: 158.92565403
	Avarage time pr. topology: 0.000126351

Data for the geometric median iteration:
	Total time: 148.671435414
	Total steps: 765245877
	Average number of steps pr. geometric median: 76.05033779351962
	Average time pr. step: 0.0000001942793027475534
	Average time pr. geometric median: 0.0000147750066002409

