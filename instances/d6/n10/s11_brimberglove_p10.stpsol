All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 8.931277517705464
MST len: 10.913423094000002
Steiner Ratio: 0.8183754483609927
Solution Steiner tree:
	Terminals:
	  #0: (0.1809798303,0.3634571686,0.3866200426,0.490760721,0.8656676732,0.1403875533) [ #11 ]
	  #1: (0.4484664097,0.2208949445,0.8329159067,0.4041060449,0.0752406661,0.2481696737) [ #15 ]
	  #2: (0.9263453185,0.5264996991,0.3097808907,0.5022105041,0.7537675108,0.4059770589) [ #9 ]
	  #4: (0.9654330178,0.4460615546,0.0395826991,0.6953234704,0.5171647261,0.4766945497) [ #5 ]
	  #6: (0.8967377799,0.3141340824,0.3612824978,0.7296536866,0.7182401273,0.4365231644) [ #7 ]
	  #8: (0.9778233603,0.0999500035,0.4717727618,0.3465953233,0.6167704093,0.4288659084) [ #9 ]
	  #10: (0.3817098766,0.0352495974,0.3687719635,0.5168204054,0.9570931466,0.3607239967) [ #13 ]
	  #12: (0.70731932,0.1956754286,0.1610363345,0.9483695021,0.9867749587,0.3349739184) [ #13 ]
	  #14: (0.4592865964,0.7046087811,0.729890453,0.0711031715,0.4371118506,0.8601203439) [ #15 ]
	  #16: (0.5789050197,0.7321704266,0.2410501816,0.7910995301,0.1739375839,0.0170635157) [ #17 ]
	Steiner points:
	  #3: (0.448466615278007,0.3634571686,0.3612824978,0.5168204054,0.4371118511255333,0.2481696737) [ #15 #11 #17 ]
	  #5: (0.9263453184999999,0.4460615546,0.3097808907,0.6953234704,0.5171647261,0.4059770588573086) [ #7 #9 #4 ]
	  #7: (0.8967377799,0.4460615529422976,0.3612824978,0.6953234704,0.5171647261,0.4059770588573086) [ #6 #5 #17 ]
	  #9: (0.9263453184999999,0.4460615546,0.3097808907,0.5022105041,0.6167704093,0.4059770588573086) [ #8 #5 #2 ]
	  #11: (0.4484664096534713,0.3634571686,0.3612824978,0.5168204054,0.8656676732,0.2481696737) [ #3 #13 #0 ]
	  #13: (0.4484664096534713,0.1956754286,0.36128249779999994,0.5168204054000061,0.9570931466,0.3349739184) [ #10 #12 #11 ]
	  #15: (0.448466615278007,0.3634571686,0.729890453,0.5168204053902501,0.4371118506,0.2481696737) [ #14 #3 #1 ]
	  #17: (0.5789050197,0.4460615529422976,0.3612788287128688,0.6953234704,0.4371118511255333,0.24816967369999998) [ #3 #7 #16 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 75.194748614
	Number of best updates: 23

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 2173623

	Init times:
		Bottleneck Steiner distances: 0.000035379
		Smallest sphere distances: 0.000006637
		Sorting: 0.000027991
		Total init time: 0.000071198

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001979, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000009431, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000086602, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000990331, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.013074307, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.200271108, bsd pruned: 0, ss pruned: 0
		Level 7 - Time: 3.5087614929999997, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 2027025
	Total number iterations 4190108 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0671220137886803
	Total time: 68.04489731
	Avarage time pr. topology: 0.000033568

Data for the geometric median iteration:
	Total time: 54.69516748
	Total steps: 81654137
	Average number of steps pr. geometric median: 5.035343483676817
	Average time pr. step: 0.0000006698395144388091
	Average time pr. geometric median: 0.0000033728720341387007

