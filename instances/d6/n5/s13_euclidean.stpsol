All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.8749876284331295
MST len: 3.096702007272501
Steiner Ratio: 0.9284030628976625
Solution Steiner tree:
	Terminals:
	  #0: (0.4365024196,0.0231408114,0.3533674392,0.8654058324,0.0406219666,0.7204131906) [ #5 ]
	  #1: (0.6326431831,0.3414155828,0.904109925,0.1300345134,0.6083626135,0.3345068657) [ #7 ]
	  #2: (0.3779684163,0.1168798023,0.6027370345,0.4139833741,0.5670465732,0.4317533082) [ #3 ]
	  #4: (0.1637372478,0.1157799243,0.8294556531,0.9116950463,0.792852993,0.8622477059) [ #5 ]
	  #6: (0.1558290944,0.7948272283,0.928200935,0.5560647936,0.0749040051,0.0531665883) [ #7 ]
	Steiner points:
	  #3: (0.3795620111082408,0.12697129863704168,0.6126976414885904,0.42227764482591595,0.5596510634458686,0.4364751363451717) [ #2 #5 #7 ]
	  #5: (0.3328055192304731,0.0961346301968895,0.6058712420398854,0.6805660698090125,0.48844238923398514,0.6341514857359029) [ #4 #3 #0 ]
	  #7: (0.4508282268748847,0.3275006569228818,0.7884316708613636,0.31962389251648776,0.5011768575416778,0.3303780454261489) [ #1 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.001029602
	Number of best updates: 2

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000004813
		Smallest sphere distances: 0.000000793
		Sorting: 0.00000113
		Total init time: 0.000007728

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000939, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00000366, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 135 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 9
	Total time: 0.001005229
	Avarage time pr. topology: 0.000067015

Data for the geometric median iteration:
	Total time: 0.000976056
	Total steps: 4885
	Average number of steps pr. geometric median: 108.55555555555556
	Average time pr. step: 0.00000019980675537359264
	Average time pr. geometric median: 0.000021690133333333334

