All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.9219246440314985
MST len: 3.275732625687125
Steiner Ratio: 0.8919911903428288
Solution Steiner tree:
	Terminals:
	  #0: (0.3117739127,0.1775910422,0.383365222,0.0224110372,0.9863731647,0.8460247199) [ #3 ]
	  #1: (0.0179714379,0.5034501541,0.1039613784,0.3216621426,0.2525900748,0.6675356089) [ #3 ]
	  #2: (0.1488122675,0.7401193766,0.5311698176,0.3798184727,0.6551358368,0.3903584468) [ #5 ]
	  #4: (0.4885317048,0.9929940151,0.7874475535,0.1478928184,0.0819520862,0.2319531549) [ #7 ]
	  #6: (0.6938135543,0.3702549456,0.5785871486,0.6617850073,0.153481192,0.2006002349) [ #7 ]
	Steiner points:
	  #3: (0.17316306411971238,0.5174604506356916,0.3474753870076298,0.27737460437324796,0.5312453935774158,0.5854628362880854) [ #0 #5 #1 ]
	  #5: (0.21959688680952935,0.6582895419743232,0.491342050583978,0.3472858169034132,0.5363235808717431,0.431553686773736) [ #2 #3 #7 ]
	  #7: (0.4425352684305559,0.6801795427617533,0.6102503285776644,0.37583277731776116,0.28140907997016873,0.30130454116543565) [ #4 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000966557
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000005427
		Smallest sphere distances: 0.000000792
		Sorting: 0.000001133
		Total init time: 0.000008336

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000109, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003553, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 139 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 9.266666666666667
	Total time: 0.000940523
	Avarage time pr. topology: 0.000062701

Data for the geometric median iteration:
	Total time: 0.000909774
	Total steps: 4502
	Average number of steps pr. geometric median: 100.04444444444445
	Average time pr. step: 0.00000020208218569524655
	Average time pr. geometric median: 0.0000202172

