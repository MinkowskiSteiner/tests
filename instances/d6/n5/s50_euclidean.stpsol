All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.8898234821273863
MST len: 3.124762687656266
Steiner Ratio: 0.9248137445902823
Solution Steiner tree:
	Terminals:
	  #0: (0.0916402913,0.5212438672,0.540312229,0.1304018107,0.0569058615,0.9637256646) [ #3 ]
	  #1: (0.0156422849,0.215467854,0.2348691641,0.1913313205,0.4307993098,0.5479775013) [ #3 ]
	  #2: (0.384026217,0.8656925191,0.3785816228,0.6610816175,0.5074669032,0.561359823) [ #7 ]
	  #4: (0.0151233026,0.0380932228,0.0248203492,0.9218689981,0.5204600438,0.6477700405) [ #5 ]
	  #6: (0.8943212232,0.6286429756,0.1040961771,0.975851736,0.6714702508,0.5076044628) [ #7 ]
	Steiner points:
	  #3: (0.07938180549823208,0.3158503786112787,0.28250116448542356,0.31127743664865704,0.3738074728167016,0.6458426450864191) [ #0 #5 #1 ]
	  #5: (0.16917110812236902,0.35679910294162237,0.21516927120052048,0.5910197671493662,0.4589247024707007,0.6221487524034192) [ #4 #3 #7 ]
	  #7: (0.42862858815412863,0.688032406553009,0.28266055390631406,0.7047005123827285,0.5270896824151391,0.5664989838876626) [ #2 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.00059734
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 15

	Init times:
		Bottleneck Steiner distances: 0.00000444
		Smallest sphere distances: 0.000000831
		Sorting: 0.000001279
		Total init time: 0.00000753

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000984, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003459, bsd pruned: 0, ss pruned: 3

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 12
	Total number iterations 71 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 5.916666666666667
	Total time: 0.000572676
	Avarage time pr. topology: 0.000047723

Data for the geometric median iteration:
	Total time: 0.000554852
	Total steps: 2531
	Average number of steps pr. geometric median: 70.30555555555556
	Average time pr. step: 0.00000021922244172263926
	Average time pr. geometric median: 0.000015412555555555556

