All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.0825801015
MST len: 6.2360853273
Steiner Ratio: 0.8150273504516934
Solution Steiner tree:
	Terminals:
	  #0: (0.0504484792,0.7854848689,0.4252954514,0.4809464605,0.2813506742,0.1364504407) [ #3 ]
	  #1: (0.1665347033,0.6197118892,0.4428926201,0.5949672445,0.2624237846,0.4864085733) [ #3 ]
	  #2: (0.5921190123,0.7871168474,0.8626960571,0.4022940902,0.433104444,0.3224785879) [ #5 ]
	  #4: (0.9411502084,0.4651792224,0.1887814688,0.3742742359,0.7257872944,0.9398560226) [ #5 ]
	  #6: (0.4051467089,0.068010787,0.7012902786,0.2066657628,0.0859796964,0.6465744323) [ #7 ]
	Steiner points:
	  #3: (0.16653470329999998,0.6197118892,0.4428926201,0.4809464605,0.2813506742,0.4864085733) [ #0 #1 #7 ]
	  #5: (0.5921190123,0.4651792224061906,0.5849862481675535,0.3742742359,0.433104444,0.5397971929666666) [ #2 #4 #7 ]
	  #7: (0.40514670890000004,0.4651792224061906,0.5849862481675535,0.3742742359,0.2813506742,0.5397971929666666) [ #3 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000386098
	Number of best updates: 9

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.0000207
		Smallest sphere distances: 0.000001873
		Sorting: 0.000003033
		Total init time: 0.000027091

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002187, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000010757, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000319505
	Avarage time pr. topology: 0.0000213

Data for the geometric median iteration:
	Total time: 0.000273362
	Total steps: 349
	Average number of steps pr. geometric median: 7.7555555555555555
	Average time pr. step: 0.0000007832722063037249
	Average time pr. geometric median: 0.000006074711111111111

