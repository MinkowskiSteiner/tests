All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.8052981514555553
MST len: 4.7841176655
Steiner Ratio: 0.7954022909797839
Solution Steiner tree:
	Terminals:
	  #0: (0.8804080379,0.3659706252,0.346876925,0.9765705247,0.2916780218,0.0321567464) [ #3 ]
	  #1: (0.8547150078,0.6357949691,0.4054190886,0.6635035848,0.7047985609,0.1648330605) [ #3 ]
	  #2: (0.7955457819,0.2153102822,0.3426214272,0.296161194,0.2554381654,0.71008044) [ #5 ]
	  #4: (0.4603446049,0.1393605741,0.3061093089,0.3350736878,0.6921740634,0.5593727015) [ #5 ]
	  #6: (0.8887816281,0.2970957897,0.7287201051,0.3965607492,0.6638084062,0.3077898539) [ #7 ]
	Steiner points:
	  #3: (0.8547150078,0.3659706252,0.34687692500001327,0.6635035848,0.49841712424444456,0.1648330605) [ #0 #1 #7 ]
	  #5: (0.7955457819,0.21531028219999998,0.3426214272,0.3350736878,0.49841712424444456,0.5593727015) [ #2 #4 #7 ]
	  #7: (0.8547150078,0.2970957897,0.3468769250000132,0.3965607492,0.49841712424444456,0.30778985389999997) [ #3 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000431222
	Number of best updates: 7

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000014829
		Smallest sphere distances: 0.000003328
		Sorting: 0.000005163
		Total init time: 0.000025474

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002797, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000012784, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000336152
	Avarage time pr. topology: 0.00002241

Data for the geometric median iteration:
	Total time: 0.000284624
	Total steps: 321
	Average number of steps pr. geometric median: 7.133333333333334
	Average time pr. step: 0.0000008866791277258566
	Average time pr. geometric median: 0.000006324977777777778

