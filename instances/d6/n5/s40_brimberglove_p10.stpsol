All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.933046503911111
MST len: 6.7834180848
Steiner Ratio: 0.8746396624447539
Solution Steiner tree:
	Terminals:
	  #0: (0.6861238385,0.9269996448,0.8449453608,0.1197466637,0.0576706543,0.2857596349) [ #5 ]
	  #1: (0.6257388632,0.607333408,0.320081121,0.7510317898,0.6239873639,0.9691653433) [ #3 ]
	  #2: (0.9184383005,0.2771394566,0.1947866791,0.8602793272,0.35034057,0.4464217548) [ #7 ]
	  #4: (0.0208598026,0.0373252058,0.1292244392,0.3481050377,0.7246325052,0.5291235137) [ #5 ]
	  #6: (0.8194229132,0.5062902763,0.0883140588,0.7855343049,0.3411775247,0.8119810777) [ #7 ]
	Steiner points:
	  #3: (0.6257388632,0.5227163401333339,0.320081121,0.7510317898,0.37543422963333334,0.46066624208888896) [ #1 #5 #7 ]
	  #5: (0.6257388632,0.5227163401333339,0.320081121,0.3481050377,0.37543422963333334,0.46066624208888896) [ #4 #3 #0 ]
	  #7: (0.8194229132,0.5062902763,0.19478667910000003,0.7855343049,0.35034057,0.46066624208888896) [ #2 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.00034476
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000007908
		Smallest sphere distances: 0.000001645
		Sorting: 0.000002674
		Total init time: 0.000013621

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001835, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000011425, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000291547
	Avarage time pr. topology: 0.000019436

Data for the geometric median iteration:
	Total time: 0.000246009
	Total steps: 331
	Average number of steps pr. geometric median: 7.355555555555555
	Average time pr. step: 0.0000007432296072507553
	Average time pr. geometric median: 0.000005466866666666667

