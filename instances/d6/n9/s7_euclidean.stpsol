All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.333906391125478
MST len: 5.81597583192404
Steiner Ratio: 0.9171128878919217
Solution Steiner tree:
	Terminals:
	  #0: (0.788697773,0.0666457862,0.4325436914,0.0533801331,0.3409319503,0.5098388086) [ #15 ]
	  #1: (0.9799026092,0.5532668813,0.3248152781,0.86887509,0.7171655543,0.54031968) [ #9 ]
	  #2: (0.4869041394,0.8679774124,0.5925911942,0.2147098403,0.0102265389,0.514818562) [ #7 ]
	  #4: (0.6565578029,0.5058633268,0.6069654662,0.0891014939,0.5592434604,0.9478974165) [ #5 ]
	  #6: (0.7644367087,0.815491903,0.8889724807,0.163898673,0.2155044015,0.7876827134) [ #7 ]
	  #8: (0.2889632747,0.7334853265,0.9825434321,0.8905285545,0.7888301731,0.5093233755) [ #11 ]
	  #10: (0.1815535189,0.5228272334,0.9403845905,0.7741447132,0.7375370742,0.9506111294) [ #11 ]
	  #12: (0.016024685,0.2613700387,0.3636968142,0.7594234276,0.035923094,0.0724071777) [ #13 ]
	  #14: (0.9959482527,0.0319323023,0.6015652793,0.0553448466,0.5267799439,0.0893740547) [ #15 ]
	Steiner points:
	  #3: (0.6902230303102933,0.428033157218537,0.5703547348217527,0.286642340490541,0.4445430223184592,0.6310480799286949) [ #5 #9 #15 ]
	  #5: (0.6745659977436658,0.5361826970415604,0.6263126621836059,0.19957255039090915,0.42183241748210043,0.749055038230928) [ #4 #3 #7 ]
	  #7: (0.670241498073429,0.7406878146786424,0.7362781174227965,0.18715186756131033,0.23110365143306724,0.7106167770700287) [ #6 #2 #5 ]
	  #9: (0.6283346012200611,0.47974861432638055,0.5427052887336619,0.5886952661082949,0.5292638387839017,0.5571557446331281) [ #3 #1 #13 ]
	  #11: (0.2927936442750039,0.6009743770674719,0.8732144131767495,0.7978074426321655,0.698569223995626,0.6506289596604953) [ #10 #8 #13 ]
	  #13: (0.4335757910673178,0.484379821951508,0.6175919897945166,0.6785398549020009,0.5075681042218295,0.5134213491197706) [ #9 #11 #12 ]
	  #15: (0.8038432127847119,0.12272740211685305,0.48196756739789814,0.09332784786026055,0.387131649711925,0.46575760969460506) [ #0 #3 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 13.333903476
	Number of best updates: 19

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 105018

	Init times:
		Bottleneck Steiner distances: 0.000013334
		Smallest sphere distances: 0.000001484
		Sorting: 0.000003888
		Total init time: 0.000020213

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000836, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003496, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032373, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000357328, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004763276, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.055559875, bsd pruned: 0, ss pruned: 41580

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 93555
	Total number iterations 1808511 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 19.3309924643258
	Total time: 13.218914959
	Avarage time pr. topology: 0.000141294

Data for the geometric median iteration:
	Total time: 12.465892133
	Total steps: 64296499
	Average number of steps pr. geometric median: 98.17983157348236
	Average time pr. step: 0.00000019388135165804284
	Average time pr. geometric median: 0.00001903523845102575

