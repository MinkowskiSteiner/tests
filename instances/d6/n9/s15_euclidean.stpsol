All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.398373693593469
MST len: 6.092751764112351
Steiner Ratio: 0.8860321087412553
Solution Steiner tree:
	Terminals:
	  #0: (0.9184461929,0.7984392977,0.6373761276,0.9660496954,0.0332565117,0.740849513) [ #15 ]
	  #1: (0.3598284723,0.0994729628,0.0463795699,0.3319424024,0.7808070447,0.5882075376) [ #7 ]
	  #2: (0.3549731771,0.6731490375,0.5153574038,0.2768868838,0.9831143995,0.7832298362) [ #3 ]
	  #4: (0.9775588964,0.7308916956,0.3246047955,0.3443029827,0.6699542467,0.4050064401) [ #5 ]
	  #6: (0.3530357351,0.0108154076,0.4717412081,0.6776405306,0.3551183903,0.1416954548) [ #13 ]
	  #8: (0.3857721921,0.3929051535,0.0476035024,0.2348172144,0.1034733854,0.3869860398) [ #11 ]
	  #10: (0.0826469702,0.1940099584,0.0051929508,0.5976890882,0.1869087318,0.9714523661) [ #11 ]
	  #12: (0.619858953,0.1357802214,0.2613565746,0.1352163563,0.4126671052,0.2444709736) [ #13 ]
	  #14: (0.8388915681,0.8634974965,0.5150421176,0.9928987739,0.9662594152,0.9828661391) [ #15 ]
	Steiner points:
	  #3: (0.5514312857997029,0.5511938515947568,0.3649979939707992,0.3818899248548746,0.7544927249848713,0.6310277027124934) [ #2 #5 #7 ]
	  #5: (0.7759306720839246,0.6740624522127,0.38798338877294314,0.4812281635934929,0.6859434456742579,0.591304471234534) [ #4 #3 #15 ]
	  #7: (0.4341839858684399,0.26054724110952204,0.1846720470187492,0.3469811646465714,0.624166483676712,0.5356101401245378) [ #1 #3 #9 ]
	  #9: (0.42831685125366614,0.22751568132595332,0.19342873069309324,0.3392327911378067,0.4341846342051591,0.44375568656041325) [ #7 #11 #13 ]
	  #11: (0.3563198584207722,0.28471856570307885,0.10778167601345967,0.3414864705660955,0.2689898586426432,0.5075805325739141) [ #8 #10 #9 ]
	  #13: (0.47534693696168845,0.16355769875130738,0.2601072715732939,0.3314701982805795,0.4145537655654101,0.3324685187367476) [ #6 #9 #12 ]
	  #15: (0.8315445956947114,0.7663542838314137,0.4907965003509108,0.7672085458432897,0.616261931689998,0.7555630217673278) [ #0 #5 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 16.272479161
	Number of best updates: 16

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 125808

	Init times:
		Bottleneck Steiner distances: 0.000022068
		Smallest sphere distances: 0.000001294
		Sorting: 0.000003706
		Total init time: 0.000028411

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000967, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00000359, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000041819, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000358331, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004816133, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.064514677, bsd pruned: 0, ss pruned: 20790

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 114345
	Total number iterations 2163956 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 18.924797761161397
	Total time: 16.144500305
	Avarage time pr. topology: 0.00014119

Data for the geometric median iteration:
	Total time: 15.242255782
	Total steps: 79106989
	Average number of steps pr. geometric median: 98.83246690779158
	Average time pr. step: 0.0000001926790031409235
	Average time pr. geometric median: 0.000019042941201751592

