All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.7320428000562105
MST len: 6.462207109266442
Steiner Ratio: 0.8870100730502405
Solution Steiner tree:
	Terminals:
	  #0: (0.7427683257,0.4821770696,0.3179936159,0.5119722684,0.8435701522,0.9867132548) [ #3 ]
	  #1: (0.052807556,0.6665685525,0.8844101391,0.2439990897,0.1642170884,0.3632087118) [ #15 ]
	  #2: (0.5058995711,0.1543681692,0.3336426291,0.698289798,0.0904165954,0.6112220658) [ #9 ]
	  #4: (0.3869168281,0.0459364807,0.2802418788,0.1041107807,0.2645267724,0.9979975852) [ #5 ]
	  #6: (0.2957911837,0.8367009162,0.7128135188,0.2540005475,0.6589878549,0.9021343397) [ #7 ]
	  #8: (0.7699328697,0.1005670764,0.8980665714,0.6543430084,0.3445661666,0.0622836598) [ #13 ]
	  #10: (0.0175517202,0.7314829946,0.1082201405,0.297793599,0.8355937753,0.3727469129) [ #11 ]
	  #12: (0.8250681748,0.3494697229,0.1410814236,0.1587108039,0.0477595204,0.2314980189) [ #13 ]
	  #14: (0.0011071404,0.3400666059,0.9582093637,0.8222869392,0.1893208209,0.7204574974) [ #15 ]
	Steiner points:
	  #3: (0.460838995871141,0.4457338987945163,0.3886323458954337,0.3930151994455432,0.5616581821232111,0.7865910187743302) [ #5 #11 #0 ]
	  #5: (0.47616242224133803,0.2605683238524829,0.35382644787239315,0.36535373812078337,0.3572600676172635,0.7626042715270362) [ #3 #4 #9 ]
	  #7: (0.25123083424347264,0.6630284746368842,0.6095505734195764,0.33993946725139784,0.5491037831220439,0.73068108091364) [ #11 #6 #15 ]
	  #9: (0.5560161663181257,0.20826745731324547,0.36739439678466296,0.5191654246898822,0.19786415882253838,0.5702638357265736) [ #2 #5 #13 ]
	  #11: (0.28935335761797987,0.5981588361720722,0.46032513365370265,0.352075848278766,0.5952153260231844,0.6977336461676082) [ #7 #10 #3 ]
	  #13: (0.6664141884770692,0.2237603064493574,0.41165459302610946,0.4526483836154143,0.18761016667031538,0.3852126993743697) [ #8 #9 #12 ]
	  #15: (0.10757216594967556,0.5817224032190892,0.8091083319031043,0.42500896326618415,0.3025633749410338,0.5806647086073607) [ #1 #7 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 18.622373548
	Number of best updates: 15

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 146598

	Init times:
		Bottleneck Steiner distances: 0.000022528
		Smallest sphere distances: 0.000001749
		Sorting: 0.000003931
		Total init time: 0.000029746

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001041, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003661, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032811, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000359653, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004801774, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.073431788, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 135135
	Total number iterations 2458660 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 18.194102194102193
	Total time: 18.481445655999998
	Avarage time pr. topology: 0.000136762

Data for the geometric median iteration:
	Total time: 17.454748277
	Total steps: 90200888
	Average number of steps pr. geometric median: 95.35531981246267
	Average time pr. step: 0.0000001935097166338318
	Average time pr. geometric median: 0.00001845218091643806

