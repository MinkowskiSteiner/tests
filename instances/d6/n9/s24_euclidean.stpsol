All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.062213594822166
MST len: 5.5910450003118095
Steiner Ratio: 0.9054145682139652
Solution Steiner tree:
	Terminals:
	  #0: (0.100103484,0.3428378414,0.1916555726,0.7579178944,0.3188702512,0.3531317987) [ #3 ]
	  #1: (0.239600985,0.0523553547,0.3122986417,0.0491195042,0.5618327174,0.5143123132) [ #7 ]
	  #2: (0.7105784624,0.7402557478,0.3541568473,0.9585125581,0.4068871468,0.2529672176) [ #11 ]
	  #4: (0.4586557506,0.9458669317,0.9701537522,0.1303727818,0.652541518,0.4553778225) [ #9 ]
	  #6: (0.4797119067,0.2656748631,0.393328767,0.6713674793,0.0235927571,0.7121990182) [ #13 ]
	  #8: (0.1044992358,0.7127510368,0.2668925339,0.4992394254,0.8234275756,0.3046993158) [ #15 ]
	  #10: (0.908120057,0.6663319537,0.22706335,0.1750125909,0.1655713786,0.0504909251) [ #11 ]
	  #12: (0.6454682083,0.0593314241,0.8095185197,0.5094773627,0.2020136715,0.8590005528) [ #13 ]
	  #14: (0.0877142293,0.8987790253,0.2479340957,0.6666313995,0.8988103708,0.2810884273) [ #15 ]
	Steiner points:
	  #3: (0.2999638865767635,0.3922912378132354,0.3176273860578209,0.5735472962527391,0.37189176713964583,0.41902110784337393) [ #0 #5 #7 ]
	  #5: (0.4027710457919314,0.5782342983777136,0.36003836365132685,0.547773660607081,0.4599820179529248,0.33992892110693596) [ #9 #11 #3 ]
	  #7: (0.3389257706124059,0.28901323883003444,0.35787258100428543,0.4893755521164966,0.3388659998585348,0.5111934038480681) [ #1 #13 #3 ]
	  #9: (0.30968919805991746,0.6913181376521381,0.42921044416982024,0.46705416452975274,0.6185530416115418,0.34636728996568855) [ #5 #4 #15 ]
	  #11: (0.5952821494493331,0.6452585832795259,0.3325782147016591,0.6027307747714561,0.38680921745654384,0.2572564404367912) [ #5 #10 #2 ]
	  #13: (0.4739276026884543,0.23270111935493118,0.4629079871107146,0.5925287749583853,0.1408128564118428,0.6868941736738835) [ #12 #7 #6 ]
	  #15: (0.11686406538373902,0.7275437267348736,0.27617806255050886,0.5116807746075452,0.8161986584760613,0.30544690140834935) [ #8 #9 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 12.485020111
	Number of best updates: 21

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 105018

	Init times:
		Bottleneck Steiner distances: 0.000024485
		Smallest sphere distances: 0.00000142
		Sorting: 0.00000366
		Total init time: 0.000031149

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000917, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003628, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032696, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000358799, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.00480088, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.055873348, bsd pruned: 10395, ss pruned: 31185

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 93555
	Total number iterations 1602470 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 17.128640906418685
	Total time: 12.369413466
	Avarage time pr. topology: 0.000132214

Data for the geometric median iteration:
	Total time: 11.696322512
	Total steps: 60720230
	Average number of steps pr. geometric median: 92.71892011574552
	Average time pr. step: 0.00000019262645269953687
	Average time pr. geometric median: 0.00001786011668002779

