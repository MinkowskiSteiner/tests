All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.906049256850995
MST len: 5.650410399411153
Steiner Ratio: 0.8682642339328609
Solution Steiner tree:
	Terminals:
	  #0: (0.1241415148,0.7870030705,0.8418433177,0.3393492947,0.0658712699,0.2347928664) [ #11 ]
	  #1: (0.5214306472,0.2908011355,0.0775070079,0.6388154154,0.6265101371,0.8390606664) [ #9 ]
	  #2: (0.5869620864,0.5873019176,0.5255940019,0.6644690943,0.2261173326,0.1521041385) [ #3 ]
	  #4: (0.7197443544,0.7481080414,0.2152077799,0.2788681999,0.3929495487,0.2476127913) [ #7 ]
	  #6: (0.6174002712,0.1841995149,0.0860658279,0.2139786585,0.0672587161,0.0937352763) [ #13 ]
	  #8: (0.5035297603,0.3692252349,0.8812454999,0.6969854937,0.4989255976,0.9448183928) [ #15 ]
	  #10: (0.2599066302,0.4942982874,0.8509792596,0.7114720651,0.5752755346,0.0176147758) [ #11 ]
	  #12: (0.1431079023,0.7291413614,0.1934557334,0.1297003627,0.0635728929,0.1258999995) [ #13 ]
	  #14: (0.6089239803,0.0684510521,0.5965783874,0.8830592017,0.0076694484,0.9101628568) [ #15 ]
	Steiner points:
	  #3: (0.5275862832815282,0.5784987175542964,0.5301456637034165,0.605524137574773,0.261101837765824,0.19379017705317575) [ #2 #11 #5 ]
	  #5: (0.5455758730208385,0.5447675925457217,0.4172886926987712,0.5223070697444362,0.2929836497297216,0.2986846770469658) [ #3 #9 #7 ]
	  #7: (0.5724936342883263,0.5969636432116772,0.2790164761178592,0.3526405483219036,0.2769328791439717,0.23636297213280422) [ #4 #13 #5 ]
	  #9: (0.5386185926661969,0.354880809939183,0.4017643208381229,0.6461225409275091,0.40782231668537466,0.6927090157332618) [ #5 #1 #15 ]
	  #11: (0.3444276907141337,0.6053162568045177,0.7064130231994739,0.5708191451872411,0.3077323381845963,0.15040501935829786) [ #3 #10 #0 ]
	  #13: (0.4824637688741952,0.5255859181900085,0.21092780652080156,0.2656632410360442,0.17457461296783855,0.17490569238339132) [ #12 #7 #6 ]
	  #15: (0.5459975781747359,0.28270414422604806,0.6110582990782322,0.7263684256518631,0.3302590051957935,0.8336251449428238) [ #8 #9 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 15.028864597
	Number of best updates: 15

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 125808

	Init times:
		Bottleneck Steiner distances: 0.000023101
		Smallest sphere distances: 0.000001319
		Sorting: 0.00000363
		Total init time: 0.000038377

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000083, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003583, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032547, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.00036052, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004801916, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.064501648, bsd pruned: 0, ss pruned: 20790

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 114345
	Total number iterations 2092125 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 18.296602387511477
	Total time: 14.901126294
	Avarage time pr. topology: 0.000130316

Data for the geometric median iteration:
	Total time: 14.02963131
	Total steps: 72397585
	Average number of steps pr. geometric median: 90.45006028122911
	Average time pr. step: 0.00000019378590197449264
	Average time pr. geometric median: 0.000017527946515245213

