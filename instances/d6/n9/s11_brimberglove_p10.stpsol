All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 8.266035499658837
MST len: 10.2222592255
Steiner Ratio: 0.8086309804234613
Solution Steiner tree:
	Terminals:
	  #0: (0.1809798303,0.3634571686,0.3866200426,0.490760721,0.8656676732,0.1403875533) [ #9 ]
	  #1: (0.4484664097,0.2208949445,0.8329159067,0.4041060449,0.0752406661,0.2481696737) [ #13 ]
	  #2: (0.9263453185,0.5264996991,0.3097808907,0.5022105041,0.7537675108,0.4059770589) [ #5 ]
	  #4: (0.8967377799,0.3141340824,0.3612824978,0.7296536866,0.7182401273,0.4365231644) [ #5 ]
	  #6: (0.9778233603,0.0999500035,0.4717727618,0.3465953233,0.6167704093,0.4288659084) [ #7 ]
	  #8: (0.3817098766,0.0352495974,0.3687719635,0.5168204054,0.9570931466,0.3607239967) [ #9 ]
	  #10: (0.70731932,0.1956754286,0.1610363345,0.9483695021,0.9867749587,0.3349739184) [ #15 ]
	  #12: (0.4592865964,0.7046087811,0.729890453,0.0711031715,0.4371118506,0.8601203439) [ #13 ]
	  #14: (0.5789050197,0.7321704266,0.2410501816,0.7910995301,0.1739375839,0.0170635157) [ #15 ]
	Steiner points:
	  #3: (0.6163972621178231,0.3634571732287143,0.3612824978,0.5168204053496154,0.7182401273,0.3349739184267876) [ #5 #11 #9 ]
	  #5: (0.8967377798999999,0.3634571732287143,0.3612824978,0.5168204053496154,0.7182401273,0.4059770589) [ #2 #4 #3 ]
	  #7: (0.6163972621178231,0.36345717322541304,0.4717727618,0.3465953233,0.6167704090357652,0.42886590840000005) [ #13 #11 #6 ]
	  #9: (0.38170987660000005,0.3634571686,0.3687719635,0.5168204054,0.8656676732,0.3607239967) [ #8 #0 #3 ]
	  #11: (0.6163972621178231,0.36345717322541304,0.3612824978,0.5168204053496154,0.6167704090357652,0.3349739184267876) [ #7 #3 #15 ]
	  #13: (0.4592865964,0.36345717322541304,0.729890453,0.3465953233,0.4371118506,0.42886590840000005) [ #12 #7 #1 ]
	  #15: (0.6163972621178232,0.36345717322541304,0.24105018159999997,0.7910995301,0.6167704090357653,0.33496906755963546) [ #10 #11 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 4.658038717
	Number of best updates: 19

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 146598

	Init times:
		Bottleneck Steiner distances: 0.000035662
		Smallest sphere distances: 0.000007791
		Sorting: 0.000024292
		Total init time: 0.000070243

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001905, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000010017, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00009679, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000998967, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.013417594, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.206964473, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 135135
	Total number iterations 276300 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0446220446220447
	Total time: 4.213750361
	Avarage time pr. topology: 0.000031181

Data for the geometric median iteration:
	Total time: 3.419228568
	Total steps: 5120116
	Average number of steps pr. geometric median: 5.412699469842327
	Average time pr. step: 0.000000667802949776919
	Average time pr. geometric median: 0.0000036146166722166715

Data for the geometric median step function:
	Total number of fixed points encountered: 20813510

