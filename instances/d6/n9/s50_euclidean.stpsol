All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.621679837908508
MST len: 5.0436561516696505
Steiner Ratio: 0.916335233594096
Solution Steiner tree:
	Terminals:
	  #0: (0.7319970623,0.6132460584,0.5875373467,0.2723092908,0.7436478691,0.6444432082) [ #3 ]
	  #1: (0.384026217,0.8656925191,0.3785816228,0.6610816175,0.5074669032,0.561359823) [ #3 ]
	  #2: (0.2765600068,0.3932821464,0.0769409025,0.5114291709,0.5846134669,0.5077402124) [ #5 ]
	  #4: (0.0151233026,0.0380932228,0.0248203492,0.9218689981,0.5204600438,0.6477700405) [ #7 ]
	  #6: (0.0156422849,0.215467854,0.2348691641,0.1913313205,0.4307993098,0.5479775013) [ #11 ]
	  #8: (0.6648593674,0.4017881259,0.1900027982,0.7689555445,0.3776398615,0.861473049) [ #15 ]
	  #10: (0.0594066722,0.5997367695,0.5458334356,0.0842270214,0.5216057671,0.066293479) [ #13 ]
	  #12: (0.0916402913,0.5212438672,0.540312229,0.1304018107,0.0569058615,0.9637256646) [ #13 ]
	  #14: (0.8943212232,0.6286429756,0.1040961771,0.975851736,0.6714702508,0.5076044628) [ #15 ]
	Steiner points:
	  #3: (0.5081897108839921,0.6599296401509974,0.33700287452993666,0.5758634705540221,0.5728209277246132,0.5993634782597805) [ #1 #0 #9 ]
	  #5: (0.27654121686484945,0.39320079005413344,0.07742577985259638,0.5116289731213199,0.5843301111348875,0.5080768539934766) [ #2 #9 #7 ]
	  #7: (0.17956425757217928,0.3080342344201008,0.11262731914760701,0.5096597440069978,0.5363542257663281,0.5385340938965932) [ #11 #4 #5 ]
	  #9: (0.508747424384649,0.5633970727092901,0.24035432348954483,0.6198890918953899,0.5575412958091934,0.6062358472703077) [ #3 #5 #15 ]
	  #11: (0.05171067348016989,0.27796201781308116,0.2555987558083733,0.2343404119944253,0.43027158908828006,0.540868768955543) [ #13 #7 #6 ]
	  #13: (0.05981063076177727,0.3754386634403446,0.3545601061142811,0.1904820221031321,0.38312128957704217,0.5292926361472375) [ #10 #11 #12 ]
	  #15: (0.647192684032567,0.52081969358015,0.1931200250241332,0.7494374672152765,0.5191034531752756,0.6744381701406962) [ #8 #9 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 12.818988246
	Number of best updates: 21

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 115413

	Init times:
		Bottleneck Steiner distances: 0.000021299
		Smallest sphere distances: 0.000001414
		Sorting: 0.000003944
		Total init time: 0.000028132

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000891, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000012649, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032663, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000362173, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004842567, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.060189805, bsd pruned: 20790, ss pruned: 10395

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 103950
	Total number iterations 1695675 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 16.31240981240981
	Total time: 12.696713636
	Avarage time pr. topology: 0.000122142

Data for the geometric median iteration:
	Total time: 11.985636827
	Total steps: 62046653
	Average number of steps pr. geometric median: 85.26991410705696
	Average time pr. step: 0.0000001931713677931991
	Average time pr. geometric median: 0.000016471705939668798

