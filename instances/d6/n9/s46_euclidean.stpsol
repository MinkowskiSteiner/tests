All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.864483894259019
MST len: 5.413348799948631
Steiner Ratio: 0.8986089893755191
Solution Steiner tree:
	Terminals:
	  #0: (0.2426491134,0.7383900605,0.0929940874,0.3222904919,0.8439696402,0.4087874943) [ #3 ]
	  #1: (0.2962878152,0.4418524259,0.3354310847,0.3243266718,0.3099521703,0.2437173171) [ #7 ]
	  #2: (0.4381406775,0.6892528486,0.3085957548,0.7187822329,0.6343978749,0.5851764826) [ #15 ]
	  #4: (0.4696959581,0.4854901943,0.4042461926,0.7061174278,0.9462171723,0.1370705227) [ #13 ]
	  #6: (0.0034960154,0.9854360809,0.0812585769,0.4243695994,0.1816147301,0.3554427341) [ #9 ]
	  #8: (0.4284378902,0.8492767708,0.0796413781,0.1055795798,0.3157934068,0.1158501856) [ #9 ]
	  #10: (0.7549710999,0.6513106882,0.8409329284,0.1592172925,0.3574281155,0.7871501002) [ #11 ]
	  #12: (0.8452832088,0.8998082606,0.2806415555,0.9451450268,0.2765807278,0.0144947227) [ #13 ]
	  #14: (0.0844243104,0.5482809849,0.0280388566,0.8680997448,0.9082862325,0.9183224421) [ #15 ]
	Steiner points:
	  #3: (0.3886095486334272,0.6768754166315085,0.275930401393666,0.46433550768332493,0.6193735211098629,0.41194582300969834) [ #11 #5 #0 ]
	  #5: (0.4457944774089244,0.6731067922295765,0.3038584520523817,0.621548825009376,0.6422432914789149,0.42204976306882364) [ #13 #3 #15 ]
	  #7: (0.33808866328716525,0.6239404959003,0.2891591054645318,0.3259264210199824,0.37761551605864596,0.30951143328951625) [ #9 #11 #1 ]
	  #9: (0.29410980397598585,0.7856992233921101,0.16785649822730606,0.2703215080990175,0.3112611058868953,0.25142834210690007) [ #6 #8 #7 ]
	  #11: (0.402693086904645,0.649217294574218,0.3407112092693321,0.3673340323351354,0.47801597053529576,0.40241821807167877) [ #3 #10 #7 ]
	  #13: (0.5111570234061976,0.6547148281770808,0.3281290135431881,0.6924779468998393,0.6722823976236323,0.2834974498785947) [ #4 #5 #12 ]
	  #15: (0.43795391828115227,0.6891406973452862,0.3084263862671526,0.7186763647428926,0.6345695461428105,0.5850460156077169) [ #2 #5 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 17.468891753
	Number of best updates: 21

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 136203

	Init times:
		Bottleneck Steiner distances: 0.000022205
		Smallest sphere distances: 0.000001608
		Sorting: 0.000003813
		Total init time: 0.000029091

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000082, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003461, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032782, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000358823, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004785182, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.068720882, bsd pruned: 0, ss pruned: 10395

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 124740
	Total number iterations 2241533 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 17.969640852974187
	Total time: 17.33493649
	Avarage time pr. topology: 0.000138968

Data for the geometric median iteration:
	Total time: 16.399529978
	Total steps: 84996415
	Average number of steps pr. geometric median: 97.34122975789643
	Average time pr. step: 0.00000019294378448785164
	Average time pr. geometric median: 0.00001878138525619002

