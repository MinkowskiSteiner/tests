All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.061772691970647
MST len: 8.522231381300001
Steiner Ratio: 0.8286295426648493
Solution Steiner tree:
	Terminals:
	  #0: (0.5706952487,0.6701787769,0.9271462466,0.29734262,0.6824838476,0.0102189095) [ #9 ]
	  #1: (0.9164578756,0.1339819669,0.1912108968,0.2600808285,0.2142811954,0.3601613163) [ #11 ]
	  #2: (0.4198029756,0.654078258,0.1943656375,0.3097259371,0.5068725648,0.1146662767) [ #3 ]
	  #4: (0.3488313283,0.3430800132,0.2595131855,0.6990013098,0.2817635985,0.6604524426) [ #5 ]
	  #6: (0.4163471956,0.8781052776,0.6627938634,0.6107128331,0.1878312143,0.1696664277) [ #7 ]
	  #8: (0.8649749825,0.1982214736,0.79443441,0.0561858788,0.458302302,0.0087156049) [ #9 ]
	  #10: (0.7238869563,0.0950620273,0.2335532183,0.158582164,0.7489994023,0.9178017638) [ #11 ]
	Steiner points:
	  #3: (0.5192458976666666,0.654078258,0.39393956802561864,0.3097259371,0.5068725648,0.1146662767) [ #2 #7 #9 ]
	  #5: (0.5192458976666666,0.3430800132,0.2595131855,0.30972593709999996,0.2817635985834844,0.5460414654665583) [ #7 #4 #11 ]
	  #7: (0.5192458976666666,0.654078258,0.39393956802561864,0.30972593709999996,0.2817635985834844,0.1696664277) [ #3 #5 #6 ]
	  #9: (0.5706952487,0.6540782579999999,0.79443441,0.29734262,0.5068725648,0.1146657275204965) [ #8 #3 #0 ]
	  #11: (0.7238869563,0.1339819669,0.2335532183,0.26008082849999997,0.2817635985834844,0.5460414654665583) [ #1 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.025041858
	Number of best updates: 8

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 963

	Init times:
		Bottleneck Steiner distances: 0.000013904
		Smallest sphere distances: 0.000003276
		Sorting: 0.000017469
		Total init time: 0.000035718

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001466, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000009467, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000087894, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000923223, bsd pruned: 105, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 840
	Total number iterations 1704 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0285714285714285
	Total time: 0.022811036
	Avarage time pr. topology: 0.000027155

Data for the geometric median iteration:
	Total time: 0.019139627
	Total steps: 28358
	Average number of steps pr. geometric median: 6.751904761904762
	Average time pr. step: 0.0000006749286621059313
	Average time pr. geometric median: 0.000004557054047619048

Data for the geometric median step function:
	Total number of fixed points encountered: 107532

