All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.6362441630058373
MST len: 4.15049788691217
Steiner Ratio: 0.8760983048496574
Solution Steiner tree:
	Terminals:
	  #0: (0.8967377799,0.3141340824,0.3612824978,0.7296536866,0.7182401273,0.4365231644) [ #3 ]
	  #1: (0.4484664097,0.2208949445,0.8329159067,0.4041060449,0.0752406661,0.2481696737) [ #7 ]
	  #2: (0.9263453185,0.5264996991,0.3097808907,0.5022105041,0.7537675108,0.4059770589) [ #3 ]
	  #4: (0.1809798303,0.3634571686,0.3866200426,0.490760721,0.8656676732,0.1403875533) [ #11 ]
	  #6: (0.4592865964,0.7046087811,0.729890453,0.0711031715,0.4371118506,0.8601203439) [ #7 ]
	  #8: (0.5789050197,0.7321704266,0.2410501816,0.7910995301,0.1739375839,0.0170635157) [ #9 ]
	  #10: (0.3817098766,0.0352495974,0.3687719635,0.5168204054,0.9570931466,0.3607239967) [ #11 ]
	Steiner points:
	  #3: (0.8305550079792183,0.4107438625500973,0.35424371633824964,0.6042067897894404,0.7079169682550254,0.3955308426981453) [ #0 #5 #2 ]
	  #5: (0.6056777903257237,0.3965925804560141,0.40358860333596003,0.5577686295260159,0.6311865219885145,0.32180575115665294) [ #3 #9 #11 ]
	  #7: (0.5053597058258826,0.43922276496412294,0.6344862301972926,0.40099382191593197,0.32103414307555705,0.39585546293844986) [ #6 #9 #1 ]
	  #9: (0.5648309559465516,0.4720869223459238,0.45740698699714505,0.5432784288076242,0.4377488630572489,0.2937658600241203) [ #5 #7 #8 ]
	  #11: (0.36684135603455365,0.2575201768645899,0.38498586299370946,0.5181203005216692,0.8338884757082515,0.26692161773849143) [ #4 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.092906633
	Number of best updates: 10

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000021177
		Smallest sphere distances: 0.000001138
		Sorting: 0.00000216
		Total init time: 0.000025824

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000906, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003391, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031494, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000352087, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 14420 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 15.25925925925926
	Total time: 0.092052098
	Avarage time pr. topology: 0.000097409

Data for the geometric median iteration:
	Total time: 0.087543233
	Total steps: 450987
	Average number of steps pr. geometric median: 95.44698412698413
	Average time pr. step: 0.00000019411475940548175
	Average time pr. geometric median: 0.000018527668359788362

