All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.404482797083214
MST len: 9.216729774200001
Steiner Ratio: 0.8033741878611075
Solution Steiner tree:
	Terminals:
	  #0: (0.0527966507,0.6832414389,0.2217341593,0.6872992696,0.7162293935,0.6441561727) [ #3 ]
	  #1: (0.3027468763,0.1342774458,0.6345026189,0.0329879546,0.4224220134,0.736733945) [ #5 ]
	  #2: (0.6838657743,0.3087545369,0.0063704299,0.343231192,0.8108547613,0.5749283333) [ #3 ]
	  #4: (0.2241443257,0.7155450055,0.4162855211,0.0445806133,0.6728030828,0.7459352001) [ #11 ]
	  #6: (0.471497888,0.0372633706,0.2766019526,0.4778683178,0.3804945626,0.0874567135) [ #7 ]
	  #8: (0.9519015979,0.4328879981,0.4879451569,0.8024439862,0.3533975968,0.9678474162) [ #9 ]
	  #10: (0.2257205198,0.9471432748,0.0564415637,0.0184599767,0.1722892593,0.6367555897) [ #11 ]
	Steiner points:
	  #3: (0.47149067175396214,0.4679154161333008,0.2217341593,0.343231192,0.7162293935,0.6416893117000066) [ #7 #0 #2 ]
	  #5: (0.3027468763,0.45623961032819127,0.41628552110000006,0.035873734490802896,0.39941387950804885,0.7367339449999999) [ #1 #9 #11 ]
	  #7: (0.47149067175396214,0.4679154161333008,0.2766019526,0.343231192,0.39941387950804885,0.6416893117000066) [ #3 #6 #9 ]
	  #9: (0.47149067175396214,0.45623961032819127,0.41628552110000006,0.343231192,0.39941387950804885,0.7367339449999999) [ #5 #7 #8 ]
	  #11: (0.2257205198,0.7155450055,0.4162827757294249,0.035873734490802896,0.39941387950804885,0.7367339449999999) [ #4 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.027769636
	Number of best updates: 13

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000016472
		Smallest sphere distances: 0.000004422
		Sorting: 0.000008695
		Total init time: 0.000031651

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001843, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000009737, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000087516, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001009389, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 1906 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.016931216931217
	Total time: 0.025386649
	Avarage time pr. topology: 0.000026864

Data for the geometric median iteration:
	Total time: 0.021395341
	Total steps: 30587
	Average number of steps pr. geometric median: 6.473439153439154
	Average time pr. step: 0.0000006994913198417628
	Average time pr. geometric median: 0.000004528114497354497

