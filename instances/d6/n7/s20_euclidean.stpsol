All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.9074669996408358
MST len: 4.288060810677122
Steiner Ratio: 0.9112433736740345
Solution Steiner tree:
	Terminals:
	  #0: (0.1360618449,0.4750343694,0.4330457674,0.714648994,0.1368193762,0.5865269595) [ #3 ]
	  #1: (0.0179714379,0.5034501541,0.1039613784,0.3216621426,0.2525900748,0.6675356089) [ #3 ]
	  #2: (0.1488122675,0.7401193766,0.5311698176,0.3798184727,0.6551358368,0.3903584468) [ #9 ]
	  #4: (0.6938135543,0.3702549456,0.5785871486,0.6617850073,0.153481192,0.2006002349) [ #11 ]
	  #6: (0.214533581,0.4014023423,0.4076549855,0.7457033986,0.781220815,0.0627908218) [ #7 ]
	  #8: (0.3117739127,0.1775910422,0.383365222,0.0224110372,0.9863731647,0.8460247199) [ #9 ]
	  #10: (0.4885317048,0.9929940151,0.7874475535,0.1478928184,0.0819520862,0.2319531549) [ #11 ]
	Steiner points:
	  #3: (0.16137730699199027,0.5075861608995019,0.37525459854604815,0.5547359772792908,0.24364828649433826,0.534157248732079) [ #0 #5 #1 ]
	  #5: (0.2733324784510741,0.5470368048406157,0.4685623269013583,0.5104735837776719,0.35870660378268693,0.397475150702672) [ #7 #3 #11 ]
	  #7: (0.2258231850694278,0.5535744368725996,0.46987018265999914,0.491648576328263,0.5722295908351704,0.3518417233880047) [ #6 #5 #9 ]
	  #9: (0.19957937287243913,0.6032124803342537,0.4892577479115478,0.40420890102152435,0.6425568804934623,0.4095744149468882) [ #8 #7 #2 ]
	  #11: (0.451691666087161,0.5835767690662881,0.5698232548296347,0.4831741443070886,0.23595253607413602,0.30050045758324767) [ #4 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.089484833
	Number of best updates: 12

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000008006
		Smallest sphere distances: 0.000001031
		Sorting: 0.000001934
		Total init time: 0.000012008

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000939, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003245, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00003168, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000360355, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 12279 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 12.993650793650794
	Total time: 0.088628834
	Avarage time pr. topology: 0.000093787

Data for the geometric median iteration:
	Total time: 0.084789042
	Total steps: 438227
	Average number of steps pr. geometric median: 92.74645502645502
	Average time pr. step: 0.00000019348201274681842
	Average time pr. geometric median: 0.000017944770793650792

