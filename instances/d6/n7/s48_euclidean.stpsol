All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.117472877262784
MST len: 4.611680043187224
Steiner Ratio: 0.8928357645594851
Solution Steiner tree:
	Terminals:
	  #0: (0.0527966507,0.6832414389,0.2217341593,0.6872992696,0.7162293935,0.6441561727) [ #5 ]
	  #1: (0.3027468763,0.1342774458,0.6345026189,0.0329879546,0.4224220134,0.736733945) [ #3 ]
	  #2: (0.6838657743,0.3087545369,0.0063704299,0.343231192,0.8108547613,0.5749283333) [ #7 ]
	  #4: (0.2241443257,0.7155450055,0.4162855211,0.0445806133,0.6728030828,0.7459352001) [ #11 ]
	  #6: (0.471497888,0.0372633706,0.2766019526,0.4778683178,0.3804945626,0.0874567135) [ #7 ]
	  #8: (0.9519015979,0.4328879981,0.4879451569,0.8024439862,0.3533975968,0.9678474162) [ #9 ]
	  #10: (0.2257205198,0.9471432748,0.0564415637,0.0184599767,0.1722892593,0.6367555897) [ #11 ]
	Steiner points:
	  #3: (0.39050173176228475,0.3924319087279475,0.38133123381829676,0.289701637036195,0.5306972217383266,0.6695154729877909) [ #9 #5 #1 ]
	  #5: (0.2608568812740816,0.5832945758174564,0.3281478488719393,0.29331285158608256,0.5776395305771505,0.680393992465242) [ #0 #3 #11 ]
	  #7: (0.5692011800905873,0.2851481405849033,0.2295685039671576,0.400066296269488,0.5829102255524782,0.5306356419522986) [ #6 #9 #2 ]
	  #9: (0.545955662855307,0.3405247234617109,0.3133225411832479,0.4039266779598209,0.5386656624700432,0.6293092517820693) [ #3 #7 #8 ]
	  #11: (0.23998734702979205,0.6921066477877431,0.32777980289719505,0.1467028573936126,0.5613685631562682,0.7025743284130975) [ #4 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.133969883
	Number of best updates: 8

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000008456
		Smallest sphere distances: 0.000001075
		Sorting: 0.000002005
		Total init time: 0.000013003

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000083, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00000336, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031287, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000349212, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 21416 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 22.66243386243386
	Total time: 0.133139096
	Avarage time pr. topology: 0.000140887

Data for the geometric median iteration:
	Total time: 0.126615333
	Total steps: 655524
	Average number of steps pr. geometric median: 138.7352380952381
	Average time pr. step: 0.00000019315133084372196
	Average time pr. geometric median: 0.00002679689587301587

