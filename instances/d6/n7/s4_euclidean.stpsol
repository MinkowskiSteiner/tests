All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.857273364049413
MST len: 4.40735800379812
Steiner Ratio: 0.8751894810281665
Solution Steiner tree:
	Terminals:
	  #0: (0.3488313283,0.3430800132,0.2595131855,0.6990013098,0.2817635985,0.6604524426) [ #3 ]
	  #1: (0.9164578756,0.1339819669,0.1912108968,0.2600808285,0.2142811954,0.3601613163) [ #11 ]
	  #2: (0.4198029756,0.654078258,0.1943656375,0.3097259371,0.5068725648,0.1146662767) [ #5 ]
	  #4: (0.5706952487,0.6701787769,0.9271462466,0.29734262,0.6824838476,0.0102189095) [ #9 ]
	  #6: (0.4163471956,0.8781052776,0.6627938634,0.6107128331,0.1878312143,0.1696664277) [ #7 ]
	  #8: (0.8649749825,0.1982214736,0.79443441,0.0561858788,0.458302302,0.0087156049) [ #9 ]
	  #10: (0.7238869563,0.0950620273,0.2335532183,0.158582164,0.7489994023,0.9178017638) [ #11 ]
	Steiner points:
	  #3: (0.5353110168336543,0.3706665657824102,0.2811669835201012,0.44738176771668536,0.378169694069979,0.47146199906030917) [ #0 #5 #11 ]
	  #5: (0.4806003969157434,0.5826665297491088,0.3557031300435988,0.38232192448952473,0.4361240693095095,0.22029010756881096) [ #7 #3 #2 ]
	  #7: (0.5040408382155422,0.65365785479111,0.5652956931127403,0.41130667732941323,0.4044195796745504,0.15755763690459723) [ #5 #6 #9 ]
	  #9: (0.6120716957106078,0.5636501313061709,0.7809285486147777,0.28274992571539787,0.5439326675781503,0.05785225707739848) [ #4 #7 #8 ]
	  #11: (0.7017392857781539,0.23425767207620007,0.24123945526095628,0.32468789923341657,0.40086737586561866,0.5274451376420449) [ #1 #3 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.075305168
	Number of best updates: 11

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000020714
		Smallest sphere distances: 0.000001207
		Sorting: 0.000002128
		Total init time: 0.0000254

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000911, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003386, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031361, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000347738, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 11146 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 11.794708994708994
	Total time: 0.074462136
	Avarage time pr. topology: 0.000078795

Data for the geometric median iteration:
	Total time: 0.070929991
	Total steps: 368223
	Average number of steps pr. geometric median: 77.93079365079365
	Average time pr. step: 0.0000001926278124940593
	Average time pr. geometric median: 0.000015011638306878306

