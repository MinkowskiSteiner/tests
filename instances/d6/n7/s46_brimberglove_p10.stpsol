All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.802333881428808
MST len: 9.1972698101
Steiner Ratio: 0.8483315203888724
Solution Steiner tree:
	Terminals:
	  #0: (0.0034960154,0.9854360809,0.0812585769,0.4243695994,0.1816147301,0.3554427341) [ #5 ]
	  #1: (0.4696959581,0.4854901943,0.4042461926,0.7061174278,0.9462171723,0.1370705227) [ #11 ]
	  #2: (0.2962878152,0.4418524259,0.3354310847,0.3243266718,0.3099521703,0.2437173171) [ #3 ]
	  #4: (0.4284378902,0.8492767708,0.0796413781,0.1055795798,0.3157934068,0.1158501856) [ #5 ]
	  #6: (0.7549710999,0.6513106882,0.8409329284,0.1592172925,0.3574281155,0.7871501002) [ #7 ]
	  #8: (0.8452832088,0.8998082606,0.2806415555,0.9451450268,0.2765807278,0.0144947227) [ #9 ]
	  #10: (0.0844243104,0.5482809849,0.0280388566,0.8680997448,0.9082862325,0.9183224421) [ #11 ]
	Steiner points:
	  #3: (0.42843789019999995,0.6235962163812162,0.3354310847,0.33598549710000003,0.3574281155,0.2437173171) [ #7 #2 #9 ]
	  #5: (0.4284378902,0.8492767708000001,0.07964147477119113,0.33598549710000003,0.3157934068,0.2437173171) [ #4 #7 #0 ]
	  #7: (0.42843789019999995,0.6513106882,0.3354310847,0.33598549710000003,0.3574281155,0.2437173171) [ #3 #6 #5 ]
	  #9: (0.4284378902,0.6235962163812162,0.3354310847,0.7601115334666666,0.3574281155,0.2437173170973849) [ #8 #3 #11 ]
	  #11: (0.4284378902,0.5482809849,0.3354310847,0.7601115334666666,0.9082862325,0.2437173170973849) [ #1 #9 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.02831507
	Number of best updates: 7

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000018937
		Smallest sphere distances: 0.000004381
		Sorting: 0.000021294
		Total init time: 0.00004692

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001755, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000009505, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000087041, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.001003677, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 1924 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.035978835978836
	Total time: 0.025935025
	Avarage time pr. topology: 0.000027444

Data for the geometric median iteration:
	Total time: 0.021884757
	Total steps: 31350
	Average number of steps pr. geometric median: 6.634920634920635
	Average time pr. step: 0.0000006980783732057417
	Average time pr. geometric median: 0.000004631694603174604

