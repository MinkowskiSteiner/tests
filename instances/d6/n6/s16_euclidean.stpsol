All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.842021579555669
MST len: 4.427360994530279
Steiner Ratio: 0.8677904476960973
Solution Steiner tree:
	Terminals:
	  #0: (0.0997784725,0.5895236719,0.2716148031,0.0401816108,0.8858513329,0.9203709387) [ #5 ]
	  #1: (0.3833639991,0.2886057418,0.8238008194,0.3094491373,0.8584360545,0.6783925484) [ #3 ]
	  #2: (0.0389199243,0.0333043621,0.0983172874,0.3055314134,0.1783176238,0.3670024752) [ #7 ]
	  #4: (0.7515285484,0.101288803,0.0099073653,0.073051563,0.7007883027,0.9308896982) [ #5 ]
	  #6: (0.2498201687,0.6923724262,0.3582976681,0.8231695224,0.1180784167,0.5551794663) [ #7 ]
	  #8: (0.2154374701,0.0895364271,0.3215230151,0.5994994997,0.9209823329,0.0083380486) [ #9 ]
	Steiner points:
	  #3: (0.32114235663417484,0.2978144292669353,0.46352577168910386,0.2915870755473179,0.7567794048005455,0.6404953904510688) [ #5 #1 #9 ]
	  #5: (0.3431088258551336,0.3448969889222245,0.31297492810351923,0.17143552699889503,0.7839639967249478,0.7839175299911931) [ #0 #4 #3 ]
	  #7: (0.17941789274145745,0.271856537310841,0.27605695327065377,0.4785772000943029,0.38781153476375047,0.42746737985297695) [ #2 #6 #9 ]
	  #9: (0.23598132992478651,0.24191470470088253,0.35041238800081526,0.43988258500354965,0.6288065164780166,0.41152552927066266) [ #3 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.010389164
	Number of best updates: 8

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000016362
		Smallest sphere distances: 0.000001052
		Sorting: 0.000001671
		Total init time: 0.000020204

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000099, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003426, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00003167, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 1561 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 14.866666666666667
	Total time: 0.010271114
	Avarage time pr. topology: 0.00009782

Data for the geometric median iteration:
	Total time: 0.009854608
	Total steps: 49689
	Average number of steps pr. geometric median: 118.30714285714286
	Average time pr. step: 0.00000019832574614099703
	Average time pr. geometric median: 0.000023463352380952385

