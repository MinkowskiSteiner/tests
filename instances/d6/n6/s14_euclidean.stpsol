All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.601699711718844
MST len: 4.032941250883435
Steiner Ratio: 0.8930702154240096
Solution Steiner tree:
	Terminals:
	  #0: (0.5198903161,0.7032006042,0.0538963489,0.7357381767,0.1634226517,0.1068535266) [ #9 ]
	  #1: (0.581988618,0.1856662101,0.839495378,0.391475477,0.4961270823,0.6485379798) [ #5 ]
	  #2: (0.0288339364,0.6024998616,0.746623855,0.6549854328,0.0995092923,0.7305007729) [ #7 ]
	  #4: (0.1415459719,0.6331634999,0.8501201872,0.1499063168,0.7037021051,0.7905814442) [ #5 ]
	  #6: (0.1537148399,0.2108905191,0.9520467748,0.6981957097,0.2743996956,0.1229939042) [ #7 ]
	  #8: (0.9994984996,0.2633149048,0.2158478607,0.460222048,0.0529571777,0.5661688692) [ #9 ]
	Steiner points:
	  #3: (0.3636047967583309,0.398004234842943,0.6943669672424845,0.5213655843990626,0.3095878774290214,0.5203790323013205) [ #7 #5 #9 ]
	  #5: (0.4091479959445783,0.3572105713914375,0.7796106086474037,0.40301143685064467,0.4543026877914322,0.6197002418171657) [ #4 #3 #1 ]
	  #7: (0.2502220151184528,0.4076918422030716,0.7545578554022876,0.5842013111764071,0.25667252805700963,0.4915804174829094) [ #2 #3 #6 ]
	  #9: (0.6318190134171429,0.4505492057543987,0.3235156161409819,0.5697296499058886,0.1744836663662599,0.4023822734598401) [ #0 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.007350905
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000006086
		Smallest sphere distances: 0.000001015
		Sorting: 0.000001427
		Total init time: 0.00000955

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000743, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003454, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000030374, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 905 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 8.619047619047619
	Total time: 0.007251934
	Avarage time pr. topology: 0.000069066

Data for the geometric median iteration:
	Total time: 0.007012603
	Total steps: 36002
	Average number of steps pr. geometric median: 85.71904761904761
	Average time pr. step: 0.00000019478370646075217
	Average time pr. geometric median: 0.00001669667380952381

