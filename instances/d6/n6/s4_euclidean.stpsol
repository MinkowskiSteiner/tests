All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.3285786679167333
MST len: 3.790441178611997
Steiner Ratio: 0.8781507246962764
Solution Steiner tree:
	Terminals:
	  #0: (0.8649749825,0.1982214736,0.79443441,0.0561858788,0.458302302,0.0087156049) [ #9 ]
	  #1: (0.9164578756,0.1339819669,0.1912108968,0.2600808285,0.2142811954,0.3601613163) [ #7 ]
	  #2: (0.4198029756,0.654078258,0.1943656375,0.3097259371,0.5068725648,0.1146662767) [ #3 ]
	  #4: (0.3488313283,0.3430800132,0.2595131855,0.6990013098,0.2817635985,0.6604524426) [ #5 ]
	  #6: (0.7238869563,0.0950620273,0.2335532183,0.158582164,0.7489994023,0.9178017638) [ #7 ]
	  #8: (0.5706952487,0.6701787769,0.9271462466,0.29734262,0.6824838476,0.0102189095) [ #9 ]
	Steiner points:
	  #3: (0.5411000633277432,0.4883844828752365,0.3653068682433626,0.3160749005941775,0.48228499593254287,0.2241180332514123) [ #2 #5 #9 ]
	  #5: (0.5825573721993644,0.33150300447091574,0.2861589643875831,0.3931874775829482,0.4059990684232199,0.4556095798095952) [ #3 #4 #7 ]
	  #7: (0.7027297972770544,0.23352857567554355,0.24980236451389431,0.3139228731232776,0.411611288781625,0.5096688646006274) [ #1 #5 #6 ]
	  #9: (0.6554406924040429,0.46093416539262216,0.7049449469326357,0.22612950784243702,0.5466762169777529,0.07815708858793813) [ #0 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.006967509
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000005599
		Smallest sphere distances: 0.000001002
		Sorting: 0.00000157
		Total init time: 0.000009222

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000968, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003412, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000030156, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 948 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 9.028571428571428
	Total time: 0.006870037
	Avarage time pr. topology: 0.000065428

Data for the geometric median iteration:
	Total time: 0.006615737
	Total steps: 32553
	Average number of steps pr. geometric median: 77.50714285714285
	Average time pr. step: 0.00000020322971769114983
	Average time pr. geometric median: 0.000015751754761904762

