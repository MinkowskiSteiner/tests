All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.136316161366179
MST len: 8.7484879404
Steiner Ratio: 0.8157199518343156
Solution Steiner tree:
	Terminals:
	  #0: (0.4465203399,0.0654066005,0.1171411286,0.3212535252,0.8584552788,0.7502715582) [ #7 ]
	  #1: (0.8574096602,0.9521014504,0.4074837125,0.1925271313,0.3823489777,0.5831195775) [ #9 ]
	  #2: (0.1180090462,0.7377023845,0.5829005281,0.1765204129,0.443329524,0.5713405118) [ #3 ]
	  #4: (0.9935512585,0.7290075462,0.8525806893,0.7858555623,0.7909271553,0.9253595797) [ #5 ]
	  #6: (0.0250844127,0.5121703011,0.1360900286,0.5095511137,0.9792704061,0.1979680449) [ #7 ]
	  #8: (0.7497371406,0.6546126439,0.7923043043,0.0619196091,0.0727788904,0.1874291036) [ #9 ]
	Steiner points:
	  #3: (0.31556291590741725,0.7377023845,0.5829005281,0.34111059006617844,0.5185417035666666,0.5713405118) [ #5 #2 #7 ]
	  #5: (0.8215188203930375,0.7377023845,0.5829005281000001,0.3411105900661784,0.5185417035666666,0.5713405118) [ #4 #3 #9 ]
	  #7: (0.31556291590741725,0.5121703011,0.1360900286,0.34111059006617844,0.8584552788,0.5713405118) [ #6 #3 #0 ]
	  #9: (0.8215188203930375,0.7377023845,0.5829005281000001,0.19252713129999996,0.3823489777,0.5713405117999999) [ #1 #5 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.002863646
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000010873
		Smallest sphere distances: 0.000002382
		Sorting: 0.000005287
		Total init time: 0.000019691

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000176, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000009342, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000087981, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 211 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0095238095238095
	Total time: 0.002604193
	Avarage time pr. topology: 0.000024801

Data for the geometric median iteration:
	Total time: 0.002224356
	Total steps: 3132
	Average number of steps pr. geometric median: 7.457142857142857
	Average time pr. step: 0.0000007102030651340996
	Average time pr. geometric median: 0.000005296085714285714

