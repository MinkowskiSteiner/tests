All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.947988530022631
MST len: 3.239097971681869
Steiner Ratio: 0.9101263857393971
Solution Steiner tree:
	Terminals:
	  #0: (0.567347118,0.6107281813,0.0388049632,0.4401840444,0.6103033869,0.6381843079) [ #7 ]
	  #1: (0.3812597861,0.3492478469,0.8728369269,0.9995752056,0.5993793447,0.9228681605) [ #3 ]
	  #2: (0.3998939984,0.2654632643,0.541314984,0.3868324786,0.2293901449,0.7843163962) [ #5 ]
	  #4: (0.1882775664,0.1697030753,0.9607357951,0.4463474226,0.2294683951,0.6895571163) [ #5 ]
	  #6: (0.4063076491,0.8204223732,0.1528823809,0.9984735842,0.4864156537,0.460190471) [ #7 ]
	  #8: (0.4677818946,0.0902298559,0.4658759178,0.5137684199,0.8042997703,0.7905323886) [ #9 ]
	Steiner points:
	  #3: (0.4017078286984047,0.2815897200443824,0.6043628656383121,0.6188367218517131,0.5151079711671466,0.7886023663791636) [ #1 #5 #9 ]
	  #5: (0.35518171119969705,0.2501409991809544,0.6513165427254111,0.4740098137993896,0.32107974157544705,0.7654093335333261) [ #2 #4 #3 ]
	  #7: (0.4951576414383561,0.5697428753607768,0.18541830720329933,0.6100101083784374,0.5830827618171166,0.6291755899288487) [ #0 #6 #9 ]
	  #9: (0.44009089599241574,0.27904556591720375,0.4805523234063654,0.584970614518507,0.6168121720902676,0.7582036589233505) [ #3 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.007060203
	Number of best updates: 7

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000005543
		Smallest sphere distances: 0.000000999
		Sorting: 0.000001588
		Total init time: 0.000009008

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000968, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003462, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000030597, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 1295 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 12.333333333333334
	Total time: 0.006962216
	Avarage time pr. topology: 0.000066306

Data for the geometric median iteration:
	Total time: 0.006625855
	Total steps: 33098
	Average number of steps pr. geometric median: 78.8047619047619
	Average time pr. step: 0.0000002001889842286543
	Average time pr. geometric median: 0.000015775845238095238

