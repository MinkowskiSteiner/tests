All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 6.324690148788852
MST len: 7.929291809199999
Steiner Ratio: 0.7976361951329121
Solution Steiner tree:
	Terminals:
	  #0: (0.8425220697,0.2753141943,0.5966266629,0.5564203358,0.8479080023,0.1846236713) [ #3 ]
	  #1: (0.6998054984,0.0663843365,0.5874823525,0.6429663057,0.9906032789,0.295718284) [ #3 ]
	  #2: (0.3974697401,0.8897629529,0.8504806137,0.3177298374,0.1029680931,0.0449004877) [ #7 ]
	  #4: (0.6449911807,0.8039444624,0.8545768367,0.7337866364,0.9254236528,0.202883592) [ #5 ]
	  #6: (0.2713367107,0.069655987,0.949639379,0.3821752264,0.3227100961,0.9883655114) [ #7 ]
	  #8: (0.7009763693,0.8096763491,0.0887954552,0.1214791905,0.3483067552,0.4219620016) [ #9 ]
	Steiner points:
	  #3: (0.6998054984,0.2753141943,0.5966266629,0.5564203358,0.8479080023,0.202883592) [ #1 #0 #5 ]
	  #5: (0.6449911807,0.8039444624,0.8545751412888511,0.5564203358,0.8479080023,0.202883592) [ #3 #4 #9 ]
	  #7: (0.3974697401,0.8058550913,0.8504806137,0.3177298374,0.3227100961,0.2759097285333273) [ #2 #6 #9 ]
	  #9: (0.6449911807,0.8058550913,0.8504806137000002,0.3177298374,0.3483067552,0.2759097285333273) [ #5 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.006535975
	Number of best updates: 10

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000015308
		Smallest sphere distances: 0.000004616
		Sorting: 0.000007366
		Total init time: 0.00003081

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000003289, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000021597, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000200904, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 212 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.019047619047619
	Total time: 0.00595322
	Avarage time pr. topology: 0.000056697

Data for the geometric median iteration:
	Total time: 0.005113091
	Total steps: 3299
	Average number of steps pr. geometric median: 7.854761904761904
	Average time pr. step: 0.0000015498911791451955
	Average time pr. geometric median: 0.00001217402619047619

Data for the geometric median step function:
	Total number of fixed points encountered: 11208

