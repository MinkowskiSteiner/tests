All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 6.558968331106352
MST len: 7.9925407506
Steiner Ratio: 0.8206362076557409
Solution Steiner tree:
	Terminals:
	  #0: (0.052807556,0.6665685525,0.8844101391,0.2439990897,0.1642170884,0.3632087118) [ #9 ]
	  #1: (0.3869168281,0.0459364807,0.2802418788,0.1041107807,0.2645267724,0.9979975852) [ #7 ]
	  #2: (0.5058995711,0.1543681692,0.3336426291,0.698289798,0.0904165954,0.6112220658) [ #5 ]
	  #4: (0.8250681748,0.3494697229,0.1410814236,0.1587108039,0.0477595204,0.2314980189) [ #5 ]
	  #6: (0.7427683257,0.4821770696,0.3179936159,0.5119722684,0.8435701522,0.9867132548) [ #7 ]
	  #8: (0.0011071404,0.3400666059,0.9582093637,0.8222869392,0.1893208209,0.7204574974) [ #9 ]
	Steiner points:
	  #3: (0.505533914312993,0.2930761132269806,0.3336426291,0.42264787549999994,0.20602156056666665,0.6112220658) [ #7 #5 #9 ]
	  #5: (0.5058995711,0.2930761132269806,0.3336426291,0.42264787549999994,0.0904165954,0.6112220658) [ #2 #4 #3 ]
	  #7: (0.505533914312993,0.2930761132269806,0.3179936159,0.42264787549999994,0.2645267724,0.9867132548000002) [ #1 #3 #6 ]
	  #9: (0.05280755599999999,0.3400666059,0.8844101391000002,0.42264787549999994,0.20602156056666665,0.6112220657999999) [ #0 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.002700639
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000009971
		Smallest sphere distances: 0.000002585
		Sorting: 0.000017642
		Total init time: 0.000032193

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001839, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000009347, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00008836, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 210 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.002441395
	Avarage time pr. topology: 0.000023251

Data for the geometric median iteration:
	Total time: 0.002079242
	Total steps: 2986
	Average number of steps pr. geometric median: 7.109523809523809
	Average time pr. step: 0.0000006963302076356329
	Average time pr. geometric median: 0.00000495057619047619

