All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 6.0983722461
MST len: 7.4744897035
Steiner Ratio: 0.8158914505219508
Solution Steiner tree:
	Terminals:
	  #0: (0.0504484792,0.7854848689,0.4252954514,0.4809464605,0.2813506742,0.1364504407) [ #5 ]
	  #1: (0.1665347033,0.6197118892,0.4428926201,0.5949672445,0.2624237846,0.4864085733) [ #3 ]
	  #2: (0.5921190123,0.7871168474,0.8626960571,0.4022940902,0.433104444,0.3224785879) [ #7 ]
	  #4: (0.0017597149,0.873469687,0.9235672881,0.864455772,0.2757637767,0.3566717321) [ #5 ]
	  #6: (0.9411502084,0.4651792224,0.1887814688,0.3742742359,0.7257872944,0.9398560226) [ #9 ]
	  #8: (0.4051467089,0.068010787,0.7012902786,0.2066657628,0.0859796964,0.6465744323) [ #9 ]
	Steiner points:
	  #3: (0.1665347033,0.7854848689,0.6749129867333332,0.4809464605,0.2624237846,0.3566717321) [ #7 #1 #5 ]
	  #5: (0.0504484792,0.7854848689,0.6749129867333333,0.4809464605,0.2757637767,0.3566717321) [ #0 #4 #3 ]
	  #7: (0.5921190123,0.7854848689,0.6749129867333332,0.4022940902,0.433104444,0.3566717321) [ #2 #3 #9 ]
	  #9: (0.5921190123,0.4651792223999999,0.6749129867333331,0.3742742359,0.4331044439999999,0.6465744323) [ #6 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.00290981
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000011439
		Smallest sphere distances: 0.000002325
		Sorting: 0.000005335
		Total init time: 0.000032537

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001753, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000009282, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00008913, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 210 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.002646414
	Avarage time pr. topology: 0.000025203

Data for the geometric median iteration:
	Total time: 0.002243605
	Total steps: 3117
	Average number of steps pr. geometric median: 7.421428571428572
	Average time pr. step: 0.0000007197962784728906
	Average time pr. geometric median: 0.000005341916666666666

