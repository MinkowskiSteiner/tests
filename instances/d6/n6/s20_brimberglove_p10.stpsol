All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 6.400133983557094
MST len: 8.0821765653
Steiner Ratio: 0.7918824752029566
Solution Steiner tree:
	Terminals:
	  #0: (0.6938135543,0.3702549456,0.5785871486,0.6617850073,0.153481192,0.2006002349) [ #9 ]
	  #1: (0.0179714379,0.5034501541,0.1039613784,0.3216621426,0.2525900748,0.6675356089) [ #3 ]
	  #2: (0.1488122675,0.7401193766,0.5311698176,0.3798184727,0.6551358368,0.3903584468) [ #5 ]
	  #4: (0.214533581,0.4014023423,0.4076549855,0.7457033986,0.781220815,0.0627908218) [ #7 ]
	  #6: (0.3117739127,0.1775910422,0.383365222,0.0224110372,0.9863731647,0.8460247199) [ #7 ]
	  #8: (0.4885317048,0.9929940151,0.7874475535,0.1478928184,0.0819520862,0.2319531549) [ #9 ]
	Steiner points:
	  #3: (0.214533581,0.5034501541,0.5785871485999999,0.32166214260038356,0.2525900748,0.39035844679999954) [ #1 #5 #9 ]
	  #5: (0.214533581,0.5034501541,0.5785871485999999,0.32166214260038356,0.6551358368000001,0.39035844679999954) [ #3 #2 #7 ]
	  #7: (0.2145365484488602,0.40140234230000005,0.4076549855,0.32166214260038356,0.781220815,0.3903584467999995) [ #4 #5 #6 ]
	  #9: (0.4885317048,0.5034501541,0.5785903355082336,0.32166214260038356,0.153481192,0.2319531549) [ #0 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.003584796
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000010857
		Smallest sphere distances: 0.000002703
		Sorting: 0.000005632
		Total init time: 0.000020176

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002145, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000012676, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000116263, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 213 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0285714285714285
	Total time: 0.003227418
	Avarage time pr. topology: 0.000030737

Data for the geometric median iteration:
	Total time: 0.00273677
	Total steps: 3113
	Average number of steps pr. geometric median: 7.411904761904762
	Average time pr. step: 0.0000008791423064567941
	Average time pr. geometric median: 0.000006516119047619048

Data for the geometric median step function:
	Total number of fixed points encountered: 10707

