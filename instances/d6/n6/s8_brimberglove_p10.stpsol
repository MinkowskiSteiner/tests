All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.654629253263838
MST len: 6.730718272900001
Steiner Ratio: 0.8401227066702767
Solution Steiner tree:
	Terminals:
	  #0: (0.0059619849,0.751268397,0.5345170878,0.886807702,0.5707898632,0.9158954317) [ #5 ]
	  #1: (0.8492760145,0.5195885676,0.7242932826,0.7551018837,0.5634760026,0.6785582023) [ #3 ]
	  #2: (0.6820132009,0.3835517202,0.9017543196,0.6433020787,0.9163497486,0.5770127282) [ #7 ]
	  #4: (0.2810107266,0.2728543464,0.6146288172,0.9977393495,0.1668278385,0.9347036872) [ #5 ]
	  #6: (0.8679820145,0.8387081427,0.9220907362,0.6533997663,0.55006754,0.2691321919) [ #9 ]
	  #8: (0.3527607286,0.7895895954,0.9058258696,0.043887435,0.9542649202,0.248271084) [ #9 ]
	Steiner points:
	  #3: (0.7440028054333333,0.5195885676000286,0.7242932826,0.7551018837,0.5707898632099053,0.5770133467794288) [ #7 #1 #5 ]
	  #5: (0.2810107266,0.5195885676000286,0.6146288172,0.8868077019999999,0.5707898632,0.9158954317) [ #0 #4 #3 ]
	  #7: (0.7440028054333333,0.5195885676000286,0.9017543196,0.6433020787,0.5707898632099053,0.5770127282) [ #2 #3 #9 ]
	  #9: (0.7440028054333333,0.7895895954,0.9058258696,0.6433020787,0.5707898632099053,0.2691321919) [ #6 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.004512597
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 108

	Init times:
		Bottleneck Steiner distances: 0.000031773
		Smallest sphere distances: 0.000004436
		Sorting: 0.000009091
		Total init time: 0.000047479

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002853, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000030021, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000138061, bsd pruned: 15, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 90
	Total number iterations 180 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.004069184
	Avarage time pr. topology: 0.000045213

Data for the geometric median iteration:
	Total time: 0.003505106
	Total steps: 2883
	Average number of steps pr. geometric median: 8.008333333333333
	Average time pr. step: 0.0000012157842525147416
	Average time pr. geometric median: 0.000009736405555555555

Data for the geometric median step function:
	Total number of fixed points encountered: 9292

