All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 6.155467918713532
MST len: 8.320882122299999
Steiner Ratio: 0.7397614613740113
Solution Steiner tree:
	Terminals:
	  #0: (0.5259626366,0.9567871433,0.9126800242,0.2932653806,0.3414342214,0.5614015812) [ #3 ]
	  #1: (0.6125465755,0.2276748625,0.680792569,0.0897512576,0.6359386517,0.9450358706) [ #7 ]
	  #2: (0.2520150534,0.5079065824,0.3625599515,0.8518746006,0.2945063893,0.4759735886) [ #5 ]
	  #4: (0.8609460652,0.737937074,0.6182698098,0.6337565941,0.1025872855,0.8561092638) [ #5 ]
	  #6: (0.7468854965,0.2319139071,0.0643970538,0.576686004,0.5689670525,0.5896822263) [ #7 ]
	  #8: (0.1689700215,0.0285875835,0.7728105293,0.3646502115,0.237839454,0.4875804249) [ #9 ]
	Steiner points:
	  #3: (0.5259626366003217,0.5079065824,0.6556399699062998,0.36226524378049085,0.3414342214,0.5614015812) [ #5 #0 #9 ]
	  #5: (0.5259626366003217,0.5079065824,0.6182698098,0.6337565941,0.29450638930000006,0.5614015812) [ #4 #2 #3 ]
	  #7: (0.6125465755,0.2276748625,0.6556399699062998,0.36226524378049085,0.5689670525000001,0.5896822263) [ #1 #6 #9 ]
	  #9: (0.5259626366003217,0.2276748625,0.6556399699062998,0.36226524378049085,0.3414342214,0.5614015812) [ #3 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.002743449
	Number of best updates: 8

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000010016
		Smallest sphere distances: 0.000002308
		Sorting: 0.000017662
		Total init time: 0.000030983

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001814, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000009299, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000088287, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 210 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.0024721
	Avarage time pr. topology: 0.000023543

Data for the geometric median iteration:
	Total time: 0.002086472
	Total steps: 2972
	Average number of steps pr. geometric median: 7.076190476190476
	Average time pr. step: 0.0000007020430686406459
	Average time pr. geometric median: 0.0000049677904761904756

