All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 6.331684040934384
MST len: 7.357097574400001
Steiner Ratio: 0.8606225453589633
Solution Steiner tree:
	Terminals:
	  #0: (0.6177704891,0.4252223039,0.3870529828,0.7761541152,0.6623876005,0.1703382573) [ #5 ]
	  #1: (0.3191112491,0.3110007436,0.691104038,0.1034285073,0.9779993249,0.8022396713) [ #9 ]
	  #2: (0.6132100036,0.1134380932,0.7647564228,0.4129372162,0.5164472929,0.8498845724) [ #7 ]
	  #4: (0.87826215,0.7782872272,0.4166978609,0.2226562315,0.5511224272,0.0462225136) [ #5 ]
	  #6: (0.6451566176,0.2473998085,0.3098868776,0.9831179767,0.5296626759,0.9438327159) [ #7 ]
	  #8: (0.1354387687,0.9625167521,0.799727213,0.4030091993,0.0851281491,0.8056346727) [ #9 ]
	Steiner points:
	  #3: (0.6177704891,0.42522230390298466,0.5724133796222223,0.41293721619995766,0.5511224271999999,0.8033713384333235) [ #7 #5 #9 ]
	  #5: (0.6177704891,0.42522230390298466,0.4166978609,0.41293721619995766,0.5511224272,0.17033825730000002) [ #4 #3 #0 ]
	  #7: (0.6177704891,0.2473998085,0.5724133796222223,0.41293830373139856,0.5296626759,0.8498845724) [ #2 #3 #6 ]
	  #9: (0.3191112491,0.42522230390298466,0.691104038,0.40300919929999995,0.5511224271999999,0.8033713384333235) [ #1 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.004886822
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000026913
		Smallest sphere distances: 0.000016524
		Sorting: 0.000006659
		Total init time: 0.000051769

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002491, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000017884, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000178069, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 210 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.004372056
	Avarage time pr. topology: 0.000041638

Data for the geometric median iteration:
	Total time: 0.00367873
	Total steps: 2885
	Average number of steps pr. geometric median: 6.869047619047619
	Average time pr. step: 0.0000012751230502599653
	Average time pr. geometric median: 0.000008758880952380951

Data for the geometric median step function:
	Total number of fixed points encountered: 9099

