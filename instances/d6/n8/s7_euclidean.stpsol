All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.902109844757134
MST len: 5.6069605352688345
Steiner Ratio: 0.8742900567824479
Solution Steiner tree:
	Terminals:
	  #0: (0.788697773,0.0666457862,0.4325436914,0.0533801331,0.3409319503,0.5098388086) [ #13 ]
	  #1: (0.9799026092,0.5532668813,0.3248152781,0.86887509,0.7171655543,0.54031968) [ #7 ]
	  #2: (0.4869041394,0.8679774124,0.5925911942,0.2147098403,0.0102265389,0.514818562) [ #5 ]
	  #4: (0.7644367087,0.815491903,0.8889724807,0.163898673,0.2155044015,0.7876827134) [ #5 ]
	  #6: (0.2889632747,0.7334853265,0.9825434321,0.8905285545,0.7888301731,0.5093233755) [ #9 ]
	  #8: (0.1815535189,0.5228272334,0.9403845905,0.7741447132,0.7375370742,0.9506111294) [ #9 ]
	  #10: (0.016024685,0.2613700387,0.3636968142,0.7594234276,0.035923094,0.0724071777) [ #11 ]
	  #12: (0.9959482527,0.0319323023,0.6015652793,0.0553448466,0.5267799439,0.0893740547) [ #13 ]
	Steiner points:
	  #3: (0.5978020663590363,0.4762020496372441,0.5762861393063862,0.3572922340317237,0.30084387717016464,0.48044041165072726) [ #11 #5 #13 ]
	  #5: (0.5996503797498262,0.7356792385647499,0.6726704129917593,0.24281752002022344,0.15604816344782751,0.5827474550434435) [ #2 #4 #3 ]
	  #7: (0.5658224614602758,0.5323815803298768,0.5927480249714402,0.6953068981338424,0.5464112903947238,0.5240556188925246) [ #1 #9 #11 ]
	  #9: (0.3228881010614677,0.6215689562782556,0.8763156931685802,0.8085241984635044,0.7151852917165087,0.6462683073386767) [ #8 #7 #6 ]
	  #11: (0.5074364749020607,0.46902193867867376,0.5541077458121186,0.5414417684584465,0.35989598820885965,0.44250204840614277) [ #3 #7 #10 ]
	  #13: (0.7944243579883177,0.13392766134669304,0.49132973251077205,0.10868802941248637,0.369760011960158,0.42291303707871297) [ #0 #3 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.837365944
	Number of best updates: 11

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 7683

	Init times:
		Bottleneck Steiner distances: 0.000010423
		Smallest sphere distances: 0.000001361
		Sorting: 0.00000329
		Total init time: 0.000016312

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000908, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003582, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031716, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000360036, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.00339047, bsd pruned: 0, ss pruned: 3780

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 6615
	Total number iterations 125880 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 19.029478458049887
	Total time: 0.829495077
	Avarage time pr. topology: 0.000125396

Data for the geometric median iteration:
	Total time: 0.783726823
	Total steps: 4055336
	Average number of steps pr. geometric median: 102.17525825144872
	Average time pr. step: 0.0000001932581721958427
	Average time pr. geometric median: 0.000019746203653313177

