All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.647508933172344
MST len: 5.044982356437157
Steiner Ratio: 0.9212141103412075
Solution Steiner tree:
	Terminals:
	  #0: (0.0583464685,0.4434102766,0.4754556192,0.1466605273,0.2289445811,0.8166331438) [ #11 ]
	  #1: (0.958641605,0.1473828816,0.0937726004,0.1534282836,0.0076622083,0.4441131705) [ #3 ]
	  #2: (0.5998500383,0.0285220109,0.4814383762,0.729074478,0.3766270487,0.206070881) [ #5 ]
	  #4: (0.9184383005,0.2771394566,0.1947866791,0.8602793272,0.35034057,0.4464217548) [ #7 ]
	  #6: (0.6257388632,0.607333408,0.320081121,0.7510317898,0.6239873639,0.9691653433) [ #13 ]
	  #8: (0.6861238385,0.9269996448,0.8449453608,0.1197466637,0.0576706543,0.2857596349) [ #9 ]
	  #10: (0.0208598026,0.0373252058,0.1292244392,0.3481050377,0.7246325052,0.5291235137) [ #11 ]
	  #12: (0.8194229132,0.5062902763,0.0883140588,0.7855343049,0.3411775247,0.8119810777) [ #13 ]
	Steiner points:
	  #3: (0.7039361318382074,0.28153768660742406,0.3154739852279599,0.3993235896512127,0.21035769514023625,0.440402360265064) [ #9 #1 #5 ]
	  #5: (0.7294941598998963,0.22327998506337615,0.32242625726047686,0.6271448356966308,0.30226198722394426,0.4003150844454335) [ #2 #7 #3 ]
	  #7: (0.8483210383992151,0.30297059142723964,0.21588454306751037,0.7861183091636621,0.3425328567968426,0.4979734511458031) [ #4 #5 #13 ]
	  #9: (0.5365913788109977,0.4157634071274757,0.4312007746636345,0.29796349079927287,0.22654690595143614,0.4790521240804286) [ #8 #3 #11 ]
	  #11: (0.2161195467896612,0.3438050234128594,0.3833488126825976,0.24385190820880012,0.3379756488156778,0.6356770327569384) [ #10 #9 #0 ]
	  #13: (0.8007411295992917,0.4904149397017803,0.13273910635779726,0.7815851908525602,0.3743835933073339,0.7875954333773951) [ #6 #7 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 1.152160369
	Number of best updates: 10

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 11463

	Init times:
		Bottleneck Steiner distances: 0.000010585
		Smallest sphere distances: 0.000001168
		Sorting: 0.000002928
		Total init time: 0.000015885

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000987, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003498, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032276, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000359678, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004830152, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 10395
	Total number iterations 165533 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 15.924290524290525
	Total time: 1.142113783
	Avarage time pr. topology: 0.000109871

Data for the geometric median iteration:
	Total time: 1.081353282
	Total steps: 5574527
	Average number of steps pr. geometric median: 89.37833894500561
	Average time pr. step: 0.00000019398117221425244
	Average time pr. geometric median: 0.00001733771495911496

