All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.932492369242765
MST len: 9.4892086926
Steiner Ratio: 0.8359487736241681
Solution Steiner tree:
	Terminals:
	  #0: (0.6916461264,0.8085904055,0.6644335085,0.1308357772,0.4260037464,0.5934402573) [ #5 ]
	  #1: (0.1817772687,0.4070928839,0.1923376211,0.4982240295,0.9403857789,0.7748197265) [ #7 ]
	  #2: (0.4174514107,0.3731905494,0.4855085041,0.3490163122,0.8709676535,0.6129136903) [ #3 ]
	  #4: (0.6634417692,0.9268148322,0.4391896513,0.6174133409,0.9290067493,0.8974420614) [ #11 ]
	  #6: (0.0282778386,0.0528809102,0.589052372,0.5746342924,0.030925551,0.4392039592) [ #9 ]
	  #8: (0.0876867548,0.9821973629,0.9948574575,0.3964075797,0.1451486364,0.7376186767) [ #9 ]
	  #10: (0.6268771643,0.9956121147,0.5463564538,0.9780446412,0.8501515877,0.8986921589) [ #11 ]
	  #12: (0.0418115836,0.220037731,0.3087208249,0.1629512739,0.7427612197,0.2952385462) [ #13 ]
	Steiner points:
	  #3: (0.4174514107000001,0.3731905494,0.43918985570406444,0.4982240294999998,0.7598950027531706,0.6129136903) [ #2 #5 #7 ]
	  #5: (0.6268771643,0.8085904055000002,0.43918985570406444,0.4982240294999998,0.7598950027531706,0.5934402573) [ #11 #0 #3 ]
	  #7: (0.1817772687,0.3731905494,0.43918985570406444,0.4982240294999998,0.7598950027531706,0.6129136903) [ #1 #3 #13 ]
	  #9: (0.0876867548,0.32213960993333335,0.589052372,0.3964075797212817,0.1451486364,0.5070219756000001) [ #8 #6 #13 ]
	  #11: (0.6268771643,0.9268148322000002,0.43918985570406444,0.6174133409,0.8501515877,0.8974420613999999) [ #10 #5 #4 ]
	  #13: (0.0876867548,0.32213960993333335,0.43918985570406444,0.3964075797212817,0.7427612197000001,0.5070219756000001) [ #7 #9 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.275174487
	Number of best updates: 10

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 9573

	Init times:
		Bottleneck Steiner distances: 0.000025569
		Smallest sphere distances: 0.000004203
		Sorting: 0.000013467
		Total init time: 0.000044947

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001806, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000009561, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000086809, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.00099016, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.011254397, bsd pruned: 945, ss pruned: 945

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 8505
	Total number iterations 17246 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0277483833039387
	Total time: 0.250481342
	Avarage time pr. topology: 0.000029451

Data for the geometric median iteration:
	Total time: 0.208240554
	Total steps: 301545
	Average number of steps pr. geometric median: 5.909171075837743
	Average time pr. step: 0.0000006905786996965627
	Average time pr. geometric median: 0.000004080747677836567

