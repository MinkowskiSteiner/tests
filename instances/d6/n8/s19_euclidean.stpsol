All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.9826126513976807
MST len: 4.366473161642143
Steiner Ratio: 0.9120891172270253
Solution Steiner tree:
	Terminals:
	  #0: (0.586565987,0.9423629041,0.7965240673,0.6874572605,0.9015435013,0.9686559904) [ #9 ]
	  #1: (0.3077861812,0.506227026,0.5700069734,0.1043102486,0.1936842861,0.4715504742) [ #11 ]
	  #2: (0.617568895,0.5169671907,0.3123343211,0.2170182561,0.784748353,0.4766765621) [ #3 ]
	  #4: (0.0970141232,0.4512588286,0.1844230174,0.3255779349,0.5106765579,0.4063816287) [ #5 ]
	  #6: (0.2902333854,0.3277869785,0.2285638117,0.0594177293,0.2219586108,0.6696780285) [ #7 ]
	  #8: (0.9952559634,0.8430695528,0.9825714156,0.2686294314,0.9196610394,0.6276440698) [ #9 ]
	  #10: (0.2870572444,0.5252522843,0.8965011402,0.6247255405,0.1610254427,0.8566360394) [ #11 ]
	  #12: (0.3323929949,0.5761897869,0.273373468,0.0765914861,0.6450726547,0.0391567499) [ #13 ]
	Steiner points:
	  #3: (0.6173511676775745,0.5171655346100361,0.3126178802921587,0.21709426109454039,0.7845915872333391,0.47663041562348324) [ #2 #9 #13 ]
	  #5: (0.2431689408698604,0.4659573039049189,0.26678540142022733,0.22292970156635067,0.4858824245316015,0.4134461058680253) [ #7 #4 #13 ]
	  #7: (0.27887217376274726,0.42903069508470915,0.35495814567016354,0.1455102080684903,0.302060248308228,0.5356449295482735) [ #5 #6 #11 ]
	  #9: (0.7685897494653218,0.7966388004100825,0.7583977369148521,0.39429165014266526,0.8808632182413867,0.7034602533791754) [ #3 #8 #0 ]
	  #11: (0.2986492168270152,0.4878582121710656,0.5409957218424761,0.15710225360194227,0.2189902413909859,0.5192825048836585) [ #10 #7 #1 ]
	  #13: (0.3710695606581077,0.5130063783040234,0.2811432055437436,0.17724572765214908,0.6144987503100277,0.3176851407126439) [ #3 #5 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.890126803
	Number of best updates: 13

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 10518

	Init times:
		Bottleneck Steiner distances: 0.000010573
		Smallest sphere distances: 0.00000125
		Sorting: 0.000003522
		Total init time: 0.000016619

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000904, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00000362, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00003238, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000356402, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004421132, bsd pruned: 0, ss pruned: 945

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 9450
	Total number iterations 130115 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 13.768783068783069
	Total time: 0.880731389
	Avarage time pr. topology: 0.000093199

Data for the geometric median iteration:
	Total time: 0.83281208
	Total steps: 4301476
	Average number of steps pr. geometric median: 75.86377425044091
	Average time pr. step: 0.00000019361076988456986
	Average time pr. geometric median: 0.000014688043738977071

