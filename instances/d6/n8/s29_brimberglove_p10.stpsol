All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 8.0150785266795
MST len: 9.9253388858
Steiner Ratio: 0.8075370139901747
Solution Steiner tree:
	Terminals:
	  #0: (0.4031608391,0.1850156338,0.0691547445,0.2341284045,0.1110238685,0.8146039973) [ #3 ]
	  #1: (0.5542016586,0.1448612135,0.5434454384,0.7647884776,0.324058877,0.6973411928) [ #3 ]
	  #2: (0.589627737,0.3313394461,0.5059928463,0.5876262708,0.6013480707,0.5097069594) [ #5 ]
	  #4: (0.4450082292,0.5312397017,0.8309675659,0.9260082347,0.7454492528,0.1616497483) [ #9 ]
	  #6: (0.8759893737,0.4619202984,0.7565934233,0.2626699606,0.5662877856,0.3799464867) [ #11 ]
	  #8: (0.3957781528,0.2137418809,0.7814581803,0.6129272085,0.001057459,0.0070835003) [ #9 ]
	  #10: (0.2666209295,0.477337444,0.9716272578,0.0232143523,0.740007405,0.5379150429) [ #11 ]
	  #12: (0.1027180125,0.9668541835,0.2171490557,0.7873155781,0.22562532,0.6172168532) [ #13 ]
	Steiner points:
	  #3: (0.4031608391,0.1850156338,0.409711582758438,0.6823967491492835,0.324058877,0.6973411928) [ #0 #1 #13 ]
	  #5: (0.4031580306615238,0.4619202984,0.5059928463,0.6299373346739252,0.5662877856,0.5097069594) [ #2 #7 #13 ]
	  #7: (0.4031580306615238,0.4619202984,0.7814581802999883,0.6299373346739252,0.5662877856,0.4326026720469894) [ #9 #5 #11 ]
	  #9: (0.3957781528,0.4619202984,0.7814581803,0.6299373346739252,0.5662877856,0.16164974830000003) [ #7 #8 #4 ]
	  #11: (0.4031580306615237,0.46192029931893347,0.7814581802999883,0.2626699606,0.5662877959544956,0.4326026720469894) [ #6 #7 #10 ]
	  #13: (0.4031547080293761,0.4619202984,0.409711582758438,0.6823967491492835,0.32405887699999997,0.6172168532) [ #3 #5 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.333891336
	Number of best updates: 21

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 11463

	Init times:
		Bottleneck Steiner distances: 0.000019369
		Smallest sphere distances: 0.000004258
		Sorting: 0.000013545
		Total init time: 0.000038382

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001945, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000009607, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000086927, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000988005, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.013141792, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 10395
	Total number iterations 21085 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.028379028379028
	Total time: 0.305173767
	Avarage time pr. topology: 0.000029357

Data for the geometric median iteration:
	Total time: 0.253358552
	Total steps: 365189
	Average number of steps pr. geometric median: 5.855202821869488
	Average time pr. step: 0.0000006937737774138871
	Average time pr. geometric median: 0.000004062186179252846

