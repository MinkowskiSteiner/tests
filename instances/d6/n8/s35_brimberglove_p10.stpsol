All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 8.653643541919976
MST len: 10.2970879048
Steiner Ratio: 0.8403971707268878
Solution Steiner tree:
	Terminals:
	  #0: (0.7090024225,0.6096137295,0.470729368,0.5849627301,0.033694007,0.4157471649) [ #3 ]
	  #1: (0.461066215,0.6987889817,0.7545507828,0.7532587111,0.6467614917,0.3806424362) [ #3 ]
	  #2: (0.7263199448,0.4611567121,0.784020462,0.387016135,0.8923050812,0.582232727) [ #5 ]
	  #4: (0.5244897048,0.4379004908,0.0703035561,0.1812310276,0.1675375119,0.7022328906) [ #11 ]
	  #6: (0.1086744881,0.7979400231,0.6606961906,0.4311483691,0.7982122655,0.1291394788) [ #9 ]
	  #8: (0.5161556138,0.8994785263,0.355014119,0.6782248629,0.940597109,0.1648225715) [ #9 ]
	  #10: (0.9537834986,0.171251196,0.818542927,0.0240870542,0.352482224,0.9860804393) [ #11 ]
	  #12: (0.0071734451,0.7727813925,0.162069249,0.0411185827,0.809808453,0.6971266361) [ #13 ]
	Steiner points:
	  #3: (0.461066215,0.6987889817,0.470729368,0.5849627301,0.6467614917,0.4157471649000001) [ #1 #0 #7 ]
	  #5: (0.6675876360666665,0.4611567121000176,0.47073003140044195,0.387016135,0.7951291683984064,0.582232727) [ #2 #11 #13 ]
	  #7: (0.461066215,0.6987889817,0.47073003140044206,0.4311483691,0.7951291683984065,0.4157471649000001) [ #9 #3 #13 ]
	  #9: (0.461066215,0.7979400231,0.47073003140044206,0.4311483691,0.7982122655,0.1648225715) [ #7 #8 #6 ]
	  #11: (0.6675876360666665,0.4611567121000176,0.470730031400442,0.18123102760000004,0.352482224,0.7022328906) [ #10 #5 #4 ]
	  #13: (0.461066215,0.6987889816999999,0.4707253216825361,0.387016135,0.7951291683984065,0.582232727) [ #5 #7 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.331578105
	Number of best updates: 17

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 11463

	Init times:
		Bottleneck Steiner distances: 0.000021045
		Smallest sphere distances: 0.000004171
		Sorting: 0.000022283
		Total init time: 0.000048661

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001742, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000009561, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000087321, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000978134, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.013203093, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 10395
	Total number iterations 20984 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.018662818662819
	Total time: 0.302923328
	Avarage time pr. topology: 0.000029141

Data for the geometric median iteration:
	Total time: 0.251430751
	Total steps: 364300
	Average number of steps pr. geometric median: 5.840949174282508
	Average time pr. step: 0.0000006901749958825144
	Average time pr. geometric median: 0.000004031277072310406

