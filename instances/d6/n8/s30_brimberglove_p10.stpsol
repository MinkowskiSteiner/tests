All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 8.609167606200653
MST len: 10.868262262099998
Steiner Ratio: 0.7921383748920653
Solution Steiner tree:
	Terminals:
	  #0: (0.7999637326,0.8187845688,0.6298717496,0.5035535896,0.7460086135,0.8155644125) [ #7 ]
	  #1: (0.229967075,0.8697586934,0.5547460311,0.5776126504,0.9940509931,0.5953307052) [ #3 ]
	  #2: (0.3029992102,0.2899153434,0.1948941383,0.2638538039,0.6694181327,0.2983180547) [ #5 ]
	  #4: (0.2358092783,0.2918461092,0.6015800124,0.5553159199,0.0093155527,0.05122513) [ #5 ]
	  #6: (0.995645286,0.5080417518,0.6990755753,0.2370779334,0.3946801244,0.8697485588) [ #7 ]
	  #8: (0.4551926155,0.776160465,0.234962674,0.1542681903,0.0132383984,0.6296427984) [ #9 ]
	  #10: (0.0097564538,0.2769615898,0.9809123091,0.6396282034,0.7805151794,0.7269209226) [ #11 ]
	  #12: (0.859679718,0.8993852078,0.1680767476,0.4144257491,0.4769978577,0.1621277403) [ #13 ]
	Steiner points:
	  #3: (0.3029992101999597,0.7761604650016136,0.5547460311,0.5070986981614061,0.6051497085387195,0.5953307052) [ #1 #11 #9 ]
	  #5: (0.3029992101999597,0.28991534340747505,0.6015800123999407,0.5070986981614061,0.6051497085387195,0.2983180547) [ #2 #11 #4 ]
	  #7: (0.7999637326,0.8172353791510555,0.6298717496,0.4108305893616951,0.4686316385374859,0.8155644125) [ #6 #0 #13 ]
	  #9: (0.4551926155,0.7761604650016136,0.5547460311,0.4108305893616951,0.4686316385374859,0.5953307052) [ #3 #8 #13 ]
	  #11: (0.3029992101999597,0.28991534340747505,0.6015800123999407,0.5070986981614061,0.6051497085387195,0.5953307052000001) [ #10 #5 #3 ]
	  #13: (0.7999637326000001,0.8172353791510555,0.5547460311,0.4108305893616951,0.4686316385374859,0.5953307052) [ #7 #9 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.304626812
	Number of best updates: 18

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 10518

	Init times:
		Bottleneck Steiner distances: 0.000020272
		Smallest sphere distances: 0.000004265
		Sorting: 0.000013541
		Total init time: 0.000039565

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000184, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000009639, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000087553, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000995015, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.012244701, bsd pruned: 945, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 9450
	Total number iterations 19106 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.021798941798942
	Total time: 0.277899747
	Avarage time pr. topology: 0.000029407

Data for the geometric median iteration:
	Total time: 0.230998678
	Total steps: 332818
	Average number of steps pr. geometric median: 5.869805996472663
	Average time pr. step: 0.0000006940690647741409
	Average time pr. geometric median: 0.000004074050758377425

