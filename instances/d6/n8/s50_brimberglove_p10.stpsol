All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.155051965063582
MST len: 9.2241802624
Steiner Ratio: 0.7756843168199251
Solution Steiner tree:
	Terminals:
	  #0: (0.0156422849,0.215467854,0.2348691641,0.1913313205,0.4307993098,0.5479775013) [ #7 ]
	  #1: (0.384026217,0.8656925191,0.3785816228,0.6610816175,0.5074669032,0.561359823) [ #3 ]
	  #2: (0.2765600068,0.3932821464,0.0769409025,0.5114291709,0.5846134669,0.5077402124) [ #3 ]
	  #4: (0.6648593674,0.4017881259,0.1900027982,0.7689555445,0.3776398615,0.861473049) [ #13 ]
	  #6: (0.0594066722,0.5997367695,0.5458334356,0.0842270214,0.5216057671,0.066293479) [ #9 ]
	  #8: (0.0916402913,0.5212438672,0.540312229,0.1304018107,0.0569058615,0.9637256646) [ #9 ]
	  #10: (0.0151233026,0.0380932228,0.0248203492,0.9218689981,0.5204600438,0.6477700405) [ #11 ]
	  #12: (0.8943212232,0.6286429756,0.1040961771,0.975851736,0.6714702508,0.5076044628) [ #13 ]
	Steiner points:
	  #3: (0.38402102927735543,0.47173575596579514,0.1900027982,0.6610743930869112,0.5216064224410024,0.547980545574025) [ #5 #1 #2 ]
	  #5: (0.38402102927735543,0.47173575596579514,0.1900027982,0.6610743930869112,0.5216064224410024,0.547980545574025) [ #3 #11 #13 ]
	  #7: (0.09164029130000757,0.4386155285220073,0.2348691641,0.19133132050000004,0.5216064574256664,0.5479775013) [ #11 #9 #0 ]
	  #9: (0.0916402913,0.5212438672,0.540312229,0.1304018107,0.5216064574256664,0.5479775013) [ #8 #7 #6 ]
	  #11: (0.09164029130000757,0.4386155285220073,0.1900027982,0.6610743930869112,0.5216064224410024,0.547980545574025) [ #5 #7 #10 ]
	  #13: (0.6648593674,0.4717357559657951,0.19000017661798346,0.7689555445,0.5216064224410024,0.547980545574025) [ #4 #5 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.314138244
	Number of best updates: 11

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 10518

	Init times:
		Bottleneck Steiner distances: 0.000019958
		Smallest sphere distances: 0.000004242
		Sorting: 0.000013664
		Total init time: 0.000039044

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001776, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000009544, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000087215, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000988938, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.012250049, bsd pruned: 945, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 9450
	Total number iterations 19238 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0357671957671957
	Total time: 0.287328985
	Avarage time pr. topology: 0.000030405

Data for the geometric median iteration:
	Total time: 0.24005506
	Total steps: 345838
	Average number of steps pr. geometric median: 6.099435626102292
	Average time pr. step: 0.0000006941257467369114
	Average time pr. geometric median: 0.0000042337753086419746

