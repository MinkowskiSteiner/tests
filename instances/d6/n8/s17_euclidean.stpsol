All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.685542662589112
MST len: 4.082027414928415
Steiner Ratio: 0.902870629704907
Solution Steiner tree:
	Terminals:
	  #0: (0.2804932088,0.3381360212,0.8132569412,0.7641613385,0.5371329414,0.440277009) [ #13 ]
	  #1: (0.7076993565,0.5674642797,0.572121925,0.3697159241,0.4717799562,0.1979497407) [ #5 ]
	  #2: (0.3574414162,0.5424212746,0.2320200839,0.4801497965,0.4595757073,0.0860349122) [ #3 ]
	  #4: (0.584843972,0.752273165,0.5360857619,0.3981009128,0.516434503,0.0732187028) [ #5 ]
	  #6: (0.5717940003,0.0018524737,0.1227083798,0.9171544332,0.8540148287,0.22754956) [ #9 ]
	  #8: (0.8217135835,0.0627080598,0.4779169375,0.2268656857,0.9706272744,0.2301676097) [ #9 ]
	  #10: (0.726414976,0.6913943327,0.1680129702,0.6031280205,0.3763264294,0.7351060159) [ #11 ]
	  #12: (0.1078885724,0.4860870128,0.6620165676,0.9043156765,0.6258278161,0.215128048) [ #13 ]
	Steiner points:
	  #3: (0.4870223202452477,0.510724087397432,0.3762223177633445,0.5258569221584605,0.5211742221585992,0.21832738070472182) [ #2 #7 #11 ]
	  #5: (0.6298542087009552,0.6213549283944064,0.5128481682454253,0.4140158177642118,0.49178329944473287,0.1788977657835937) [ #1 #4 #11 ]
	  #7: (0.46008229804121625,0.39086487543597814,0.4442371406224894,0.6110459634913021,0.6109648623458347,0.2482771901045899) [ #9 #3 #13 ]
	  #9: (0.5879968081682053,0.1870923401639174,0.3549477883055198,0.6046677263320924,0.778411879859201,0.23725756194548359) [ #8 #6 #7 ]
	  #11: (0.5602189744444255,0.5669217029292886,0.4005075406408286,0.49623185496055533,0.49611393545505633,0.2590884553826818) [ #3 #5 #10 ]
	  #13: (0.2642107801643299,0.40216230049424584,0.6714627996750652,0.775616525604162,0.5857450443411517,0.31576722108606203) [ #0 #7 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.918507529
	Number of best updates: 17

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 8628

	Init times:
		Bottleneck Steiner distances: 0.000011195
		Smallest sphere distances: 0.000001169
		Sorting: 0.000003166
		Total init time: 0.000017093

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000922, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000004021, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000031975, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000353913, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.003725545, bsd pruned: 0, ss pruned: 2835

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 7560
	Total number iterations 131911 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 17.448544973544973
	Total time: 0.910101434
	Avarage time pr. topology: 0.000120383

Data for the geometric median iteration:
	Total time: 0.861884669
	Total steps: 4468142
	Average number of steps pr. geometric median: 98.50401234567902
	Average time pr. step: 0.00000019289554114439515
	Average time pr. geometric median: 0.000019000984766313936

