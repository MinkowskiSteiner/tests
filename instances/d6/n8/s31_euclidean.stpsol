All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.172411237860688
MST len: 4.708212342630807
Steiner Ratio: 0.8861986108997943
Solution Steiner tree:
	Terminals:
	  #0: (0.1162355529,0.442412433,0.306818193,0.8175258314,0.6490781958,0.3927978894) [ #3 ]
	  #1: (0.1665347033,0.6197118892,0.4428926201,0.5949672445,0.2624237846,0.4864085733) [ #3 ]
	  #2: (0.5921190123,0.7871168474,0.8626960571,0.4022940902,0.433104444,0.3224785879) [ #11 ]
	  #4: (0.0504484792,0.7854848689,0.4252954514,0.4809464605,0.2813506742,0.1364504407) [ #5 ]
	  #6: (0.1869343599,0.44229848,0.9763836213,0.62982698,0.0372657241,0.2388074055) [ #9 ]
	  #8: (0.0017597149,0.873469687,0.9235672881,0.864455772,0.2757637767,0.3566717321) [ #9 ]
	  #10: (0.9411502084,0.4651792224,0.1887814688,0.3742742359,0.7257872944,0.9398560226) [ #13 ]
	  #12: (0.4051467089,0.068010787,0.7012902786,0.2066657628,0.0859796964,0.6465744323) [ #13 ]
	Steiner points:
	  #3: (0.16366962500813764,0.6164605524576121,0.46149246419230167,0.6051007431977373,0.3072779362808854,0.4312351783318633) [ #0 #1 #5 ]
	  #5: (0.17009852058895072,0.6514853508275524,0.5233118515977515,0.5714430235729037,0.29398786541230826,0.36055720624977877) [ #4 #3 #7 ]
	  #7: (0.2408833273530065,0.6363840080359368,0.667619611844935,0.5666513084917133,0.28025771151159284,0.36742428235812724) [ #5 #11 #9 ]
	  #9: (0.16218225583278542,0.6381930545848696,0.8291396679972325,0.6634303045917731,0.20428039883255197,0.3250347159209589) [ #6 #8 #7 ]
	  #11: (0.43140950295010433,0.6123593106115555,0.7068466718743925,0.4544210962069108,0.34245584083805003,0.42320328657300893) [ #2 #7 #13 ]
	  #13: (0.521191531718051,0.41023355002627604,0.6053033206458018,0.3598911697334447,0.3343989147657692,0.5940113483533592) [ #10 #11 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 1.058092606
	Number of best updates: 16

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 11463

	Init times:
		Bottleneck Steiner distances: 0.000011496
		Smallest sphere distances: 0.000001296
		Sorting: 0.000002918
		Total init time: 0.000016965

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000923, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00000352, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032171, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000356791, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004778896, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 10395
	Total number iterations 148961 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 14.33006253006253
	Total time: 1.048146825
	Avarage time pr. topology: 0.000100831

Data for the geometric median iteration:
	Total time: 0.99331353
	Total steps: 5162546
	Average number of steps pr. geometric median: 82.7729036395703
	Average time pr. step: 0.00000019240768605257947
	Average time pr. geometric median: 0.000015926142857142856

