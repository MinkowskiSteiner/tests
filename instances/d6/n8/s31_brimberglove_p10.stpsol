All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 7.656940883740998
MST len: 9.5032963034
Steiner Ratio: 0.8057142110786938
Solution Steiner tree:
	Terminals:
	  #0: (0.1162355529,0.442412433,0.306818193,0.8175258314,0.6490781958,0.3927978894) [ #11 ]
	  #1: (0.1665347033,0.6197118892,0.4428926201,0.5949672445,0.2624237846,0.4864085733) [ #5 ]
	  #2: (0.5921190123,0.7871168474,0.8626960571,0.4022940902,0.433104444,0.3224785879) [ #9 ]
	  #4: (0.0504484792,0.7854848689,0.4252954514,0.4809464605,0.2813506742,0.1364504407) [ #5 ]
	  #6: (0.1869343599,0.44229848,0.9763836213,0.62982698,0.0372657241,0.2388074055) [ #7 ]
	  #8: (0.0017597149,0.873469687,0.9235672881,0.864455772,0.2757637767,0.3566717321) [ #9 ]
	  #10: (0.9411502084,0.4651792224,0.1887814688,0.3742742359,0.7257872944,0.9398560226) [ #11 ]
	  #12: (0.4051467089,0.068010787,0.7012902786,0.2066657628,0.0859796964,0.6465744323) [ #13 ]
	Steiner points:
	  #3: (0.16653649200371914,0.442412433,0.4428926201,0.4809476110020521,0.2624237846047164,0.3927980323445002) [ #5 #11 #13 ]
	  #5: (0.1665347033,0.6197118892,0.4428926201,0.48094646050011447,0.2624237846,0.3927980323445002) [ #1 #4 #3 ]
	  #7: (0.1869343599,0.44229847999999994,0.8829864674333332,0.5563479844402124,0.2757637766590436,0.33387630263333334) [ #9 #6 #13 ]
	  #9: (0.1869343599,0.7871168474,0.8829864674333332,0.5563479844402124,0.2757637767,0.33387630263333334) [ #8 #7 #2 ]
	  #11: (0.16653649200371914,0.442412433,0.306818193,0.4809476110020521,0.6490781958,0.3927980323445002) [ #10 #3 #0 ]
	  #13: (0.1869343599,0.44229847471998324,0.7012902786,0.4809476110020521,0.2624237846047164,0.3927980323445002) [ #3 #7 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.337814126
	Number of best updates: 17

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 11463

	Init times:
		Bottleneck Steiner distances: 0.000020082
		Smallest sphere distances: 0.000004537
		Sorting: 0.000013552
		Total init time: 0.000039465

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001624, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000009545, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000087257, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000990713, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.013257461, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 10395
	Total number iterations 21164 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.035978835978836
	Total time: 0.308997854
	Avarage time pr. topology: 0.000029725

Data for the geometric median iteration:
	Total time: 0.256907001
	Total steps: 369255
	Average number of steps pr. geometric median: 5.9203944203944205
	Average time pr. step: 0.0000006957441361660642
	Average time pr. geometric median: 0.000004119079701779702

