All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 8.408311862488734
MST len: 10.451812576400002
Steiner Ratio: 0.80448360521452
Solution Steiner tree:
	Terminals:
	  #0: (0.2488169443,0.8343745516,0.425641888,0.1330993646,0.9261679467,0.5155373381) [ #3 ]
	  #1: (0.206965448,0.6995153961,0.2034307295,0.3588790108,0.884531313,0.4632373589) [ #3 ]
	  #2: (0.6858023101,0.9032684844,0.0947479369,0.6750120226,0.2615602367,0.272972847) [ #13 ]
	  #4: (0.9381766622,0.9481784259,0.9291220382,0.1416073912,0.3070574362,0.8136533512) [ #11 ]
	  #6: (0.6048447502,0.0298102363,0.4428648839,0.3256396411,0.7602754271,0.4969397343) [ #9 ]
	  #8: (0.9914577939,0.2373887199,0.3081078475,0.6704191042,0.7412129779,0.229606642) [ #9 ]
	  #10: (0.0740991244,0.9287021542,0.8842824203,0.0917933952,0.0898954501,0.3307895457) [ #11 ]
	  #12: (0.7227528005,0.6292115332,0.7207948913,0.7304651908,0.05407485,0.9231773037) [ #13 ]
	Steiner points:
	  #3: (0.2488169443,0.6995153961,0.425641888,0.3588790108,0.884531313,0.5155373381) [ #7 #0 #1 ]
	  #5: (0.6981191402333329,0.7833493705270539,0.4428648839,0.35887901080000145,0.23467010854460169,0.49693973430000027) [ #7 #11 #13 ]
	  #7: (0.6981191402333329,0.6995153961,0.4428648839,0.35887901080000145,0.7412129779,0.49693973430000027) [ #3 #5 #9 ]
	  #9: (0.6981191402333329,0.2373887199,0.4428648839,0.35887901080000145,0.7412129779,0.4969397343) [ #6 #8 #7 ]
	  #11: (0.6981191402333329,0.9287021542,0.8842824203000001,0.14160739119999996,0.23467010854460169,0.4969397343000002) [ #4 #5 #10 ]
	  #13: (0.6981191402333329,0.7833493705270539,0.44286488389999995,0.6750120226000001,0.23467010854460166,0.49693973430000027) [ #2 #5 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.265441674
	Number of best updates: 15

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 9573

	Init times:
		Bottleneck Steiner distances: 0.000027309
		Smallest sphere distances: 0.00000591
		Sorting: 0.000018607
		Total init time: 0.000054352

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002033, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000009975, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000087146, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000997114, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.011333317, bsd pruned: 0, ss pruned: 1890

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 8505
	Total number iterations 17331 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.037742504409171
	Total time: 0.239872793
	Avarage time pr. topology: 0.000028203

Data for the geometric median iteration:
	Total time: 0.196847363
	Total steps: 297680
	Average number of steps pr. geometric median: 5.833431314912796
	Average time pr. step: 0.0000006612717112335394
	Average time pr. geometric median: 0.0000038574831079757

Data for the geometric median step function:
	Total number of fixed points encountered: 1165076

