All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.277146312431468
MST len: 4.964420783735532
Steiner Ratio: 0.861559988316116
Solution Steiner tree:
	Terminals:
	  #0: (0.1339392146,0.878686387,0.568443856,0.5212840333,0.2068786268,0.8687753225) [ #3 ]
	  #1: (0.5613801752,0.2249833128,0.3930917794,0.4439383934,0.2850412509,0.1447810513) [ #11 ]
	  #2: (0.5635544996,0.8646787037,0.8954018363,0.2308046744,0.0040116059,0.4605808875) [ #9 ]
	  #4: (0.0407533725,0.4439474528,0.1744425461,0.3665206686,0.0588192782,0.5624328081) [ #5 ]
	  #6: (0.3083389789,0.7757516945,0.2434785898,0.2037408148,0.0065563684,0.2474901957) [ #7 ]
	  #8: (0.6643217023,0.1828796636,0.873656426,0.6091953011,0.0289642629,0.8836533655) [ #13 ]
	  #10: (0.7704661483,0.768258802,0.0937586353,0.1635579272,0.2121971949,0.3787998862) [ #11 ]
	  #12: (0.1763232952,0.6261662299,0.9448735988,0.8460845993,0.0099969395,0.257080875) [ #13 ]
	Steiner points:
	  #3: (0.2745388234530477,0.7142672656269522,0.5697035894468225,0.44234129245813997,0.10393860534474841,0.6148305416652861) [ #9 #0 #5 ]
	  #5: (0.24645914454871432,0.6194963750603885,0.3346856701231336,0.35053015366971163,0.08277160979734577,0.48464824431449566) [ #3 #4 #7 ]
	  #7: (0.36053118596125006,0.6675653796427118,0.2737662866608067,0.27542847950381505,0.08334875192714597,0.34238573149641843) [ #6 #11 #5 ]
	  #9: (0.3836006373330903,0.6821727271645741,0.7464509880422231,0.46131026654704876,0.05480568018434836,0.552245769606793) [ #3 #2 #13 ]
	  #11: (0.5055504940737493,0.6058612970479114,0.25114436053243205,0.2800369586721403,0.15634497941530723,0.3126417984708208) [ #10 #7 #1 ]
	  #13: (0.38665618982951533,0.5931643045898729,0.805368513656098,0.5601311206041204,0.041951219531770466,0.5459223205194443) [ #8 #9 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 1.121233946
	Number of best updates: 22

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 11463

	Init times:
		Bottleneck Steiner distances: 0.000011252
		Smallest sphere distances: 0.000001373
		Sorting: 0.000003059
		Total init time: 0.000016997

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001035, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003606, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000032209, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000357561, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004759186, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 10395
	Total number iterations 154701 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 14.882251082251083
	Total time: 1.111257951
	Avarage time pr. topology: 0.000106903

Data for the geometric median iteration:
	Total time: 1.054362174
	Total steps: 5478808
	Average number of steps pr. geometric median: 87.84364277697611
	Average time pr. step: 0.00000019244371658944793
	Average time pr. geometric median: 0.000016904957094757095

