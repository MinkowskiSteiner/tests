All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.2639713072541774
MST len: 3.6555518094000004
Steiner Ratio: 0.892880603924447
Solution Steiner tree:
	Terminals:
	  #0: (0.9984735842,0.4864156537,0.460190471) [ #13 ]
	  #1: (0.3812597861,0.3492478469,0.8728369269) [ #5 ]
	  #2: (0.5137684199,0.8042997703,0.7905323886) [ #9 ]
	  #4: (0.3868324786,0.2293901449,0.7843163962) [ #5 ]
	  #6: (0.3998939984,0.2654632643,0.541314984) [ #11 ]
	  #8: (0.4063076491,0.8204223732,0.1528823809) [ #9 ]
	  #10: (0.4677818946,0.0902298559,0.4658759178) [ #11 ]
	  #12: (0.9995752056,0.5993793447,0.9228681605) [ #13 ]
	Steiner points:
	  #3: (0.5137684199000001,0.5240702166484528,0.5413149839999999) [ #7 #9 #13 ]
	  #5: (0.3868324786,0.3492478469,0.7843163962) [ #4 #1 #7 ]
	  #7: (0.4225232971275437,0.3492478469,0.5413149839999999) [ #5 #3 #11 ]
	  #9: (0.5137684199,0.8042997703,0.5413149839999999) [ #2 #8 #3 ]
	  #11: (0.4225232971275437,0.2654632643,0.5413103798458223) [ #6 #7 #10 ]
	  #13: (0.9984735842,0.5240702166484528,0.5413149839999999) [ #0 #3 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.099981981
	Number of best updates: 10

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 6738

	Init times:
		Bottleneck Steiner distances: 0.000015588
		Smallest sphere distances: 0.000002891
		Sorting: 0.000017735
		Total init time: 0.000037156

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001348, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000006782, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00006196, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000707774, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.005907048, bsd pruned: 0, ss pruned: 4725

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 5670
	Total number iterations 11459 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0209876543209875
	Total time: 0.087284353
	Avarage time pr. topology: 0.000015394

Data for the geometric median iteration:
	Total time: 0.069002217
	Total steps: 196111
	Average number of steps pr. geometric median: 5.764579659024103
	Average time pr. step: 0.0000003518528639392997
	Average time pr. geometric median: 0.0000020282838624338624

