All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.955177015667923
MST len: 3.4008265387000005
Steiner Ratio: 0.8689584670194819
Solution Steiner tree:
	Terminals:
	  #0: (0.8071258347,0.3703712553,0.6420550997) [ #5 ]
	  #1: (0.6899730524,0.429892563,0.1510021729) [ #3 ]
	  #2: (0.5980566296,0.3121092172,0.450471332) [ #3 ]
	  #4: (0.8194240331,0.0320747178,0.6424573714) [ #11 ]
	  #6: (0.3284930686,0.8837608927,0.2698044499) [ #13 ]
	  #8: (0.59228968,0.9251535474,0.6100173786) [ #9 ]
	  #10: (0.9574638638,0.1660335614,0.7214625924) [ #11 ]
	  #12: (0.2394123931,0.7611420498,0.1745217923) [ #13 ]
	Steiner points:
	  #3: (0.5980566296,0.429892563,0.45046618353205703) [ #2 #1 #7 ]
	  #5: (0.8071258347,0.3703712553,0.6420550997) [ #11 #7 #0 ]
	  #7: (0.59228968000002,0.42989256299999995,0.4504693062357658) [ #5 #9 #3 ]
	  #9: (0.59228968,0.8428879452795937,0.4504693062357658) [ #7 #8 #13 ]
	  #11: (0.8194240331,0.1660335614,0.6424573714) [ #10 #5 #4 ]
	  #13: (0.3284930686,0.8428879452795937,0.2698044499000001) [ #6 #9 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.052400135
	Number of best updates: 11

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 3738

	Init times:
		Bottleneck Steiner distances: 0.00001633
		Smallest sphere distances: 0.000003017
		Sorting: 0.000009055
		Total init time: 0.00002946

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001282, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000006846, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000062839, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000649935, bsd pruned: 0, ss pruned: 105
		Level 5 - Time: 0.004853521, bsd pruned: 1680, ss pruned: 4785

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 2775
	Total number iterations 5580 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0108108108108107
	Total time: 0.042763278
	Avarage time pr. topology: 0.00001541

Data for the geometric median iteration:
	Total time: 0.033586127
	Total steps: 93504
	Average number of steps pr. geometric median: 5.615855855855856
	Average time pr. step: 0.00000035919454782683095
	Average time pr. geometric median: 0.000002017184804804805

