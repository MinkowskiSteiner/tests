All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.3958829749835164
MST len: 2.5127309709525134
Steiner Ratio: 0.953497609843722
Solution Steiner tree:
	Terminals:
	  #0: (0.3350736878,0.6921740634,0.5593727015) [ #7 ]
	  #1: (0.7955457819,0.2153102822,0.3426214272) [ #3 ]
	  #2: (0.4603446049,0.1393605741,0.3061093089) [ #13 ]
	  #4: (0.8547150078,0.6357949691,0.4054190886) [ #5 ]
	  #6: (0.3965607492,0.6638084062,0.3077898539) [ #7 ]
	  #8: (0.8887816281,0.2970957897,0.7287201051) [ #9 ]
	  #10: (0.6635035848,0.7047985609,0.1648330605) [ #11 ]
	  #12: (0.296161194,0.2554381654,0.71008044) [ #13 ]
	Steiner points:
	  #3: (0.7504668930440079,0.24988750770389448,0.38692880344392233) [ #1 #9 #13 ]
	  #5: (0.806891334561807,0.5961757287437524,0.38684140907996145) [ #4 #9 #11 ]
	  #7: (0.40011254917545036,0.664516337520742,0.311235578850264) [ #6 #11 #0 ]
	  #9: (0.8003300214089589,0.3557000291423673,0.4722647636389977) [ #8 #3 #5 ]
	  #11: (0.6418022389801027,0.6732054968306008,0.24223755180287412) [ #5 #7 #10 ]
	  #13: (0.5064825156870543,0.1862841306499799,0.3928446644658033) [ #2 #3 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.480697081
	Number of best updates: 12

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 10518

	Init times:
		Bottleneck Steiner distances: 0.000018433
		Smallest sphere distances: 0.000001205
		Sorting: 0.000002074
		Total init time: 0.000023125

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000788, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003318, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000029439, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000328777, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004167031, bsd pruned: 0, ss pruned: 945

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 9450
	Total number iterations 93845 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 9.93068783068783
	Total time: 0.47174184
	Avarage time pr. topology: 0.000049919

Data for the geometric median iteration:
	Total time: 0.436748708
	Total steps: 3124397
	Average number of steps pr. geometric median: 55.10400352733686
	Average time pr. step: 0.0000001397865597745741
	Average time pr. geometric median: 0.000007702799082892415

