All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.407931577646608
MST len: 3.9165470403000002
Steiner Ratio: 0.8701367665395299
Solution Steiner tree:
	Terminals:
	  #0: (0.0156422849,0.215467854,0.2348691641) [ #11 ]
	  #1: (0.0916402913,0.5212438672,0.540312229) [ #5 ]
	  #2: (0.1913313205,0.4307993098,0.5479775013) [ #3 ]
	  #4: (0.9218689981,0.5204600438,0.6477700405) [ #7 ]
	  #6: (0.8943212232,0.6286429756,0.1040961771) [ #13 ]
	  #8: (0.1304018107,0.0569058615,0.9637256646) [ #9 ]
	  #10: (0.0151233026,0.0380932228,0.0248203492) [ #11 ]
	  #12: (0.975851736,0.6714702508,0.5076044628) [ #13 ]
	Steiner points:
	  #3: (0.09340476555339204,0.4307993098,0.5479775013) [ #2 #9 #5 ]
	  #5: (0.09340476555339211,0.5212438672,0.540312229) [ #1 #7 #3 ]
	  #7: (0.9214980602512677,0.5212438672,0.540312229) [ #4 #5 #13 ]
	  #9: (0.09340476555339204,0.14789341289750177,0.5479775013) [ #8 #3 #11 ]
	  #11: (0.0156422849,0.14789341289750177,0.2348691641) [ #10 #9 #0 ]
	  #13: (0.9214980602512678,0.6286429756,0.5076044628) [ #6 #7 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.051846159
	Number of best updates: 7

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 3738

	Init times:
		Bottleneck Steiner distances: 0.0000159
		Smallest sphere distances: 0.000003058
		Sorting: 0.000008582
		Total init time: 0.000028475

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001716, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000006684, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000061163, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000643502, bsd pruned: 0, ss pruned: 105
		Level 5 - Time: 0.004679128, bsd pruned: 1680, ss pruned: 4785

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 2775
	Total number iterations 5596 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0165765765765764
	Total time: 0.042511751
	Avarage time pr. topology: 0.000015319

Data for the geometric median iteration:
	Total time: 0.033538887
	Total steps: 95191
	Average number of steps pr. geometric median: 5.717177177177177
	Average time pr. step: 0.00000035233254194199036
	Average time pr. geometric median: 0.0000020143475675675678

