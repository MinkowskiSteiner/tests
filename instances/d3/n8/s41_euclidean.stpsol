All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.218720486041173
MST len: 2.368681557952223
Steiner Ratio: 0.9366900665023563
Solution Steiner tree:
	Terminals:
	  #0: (0.9065222973,0.5735588286,0.6033617564) [ #3 ]
	  #1: (0.7889988608,0.387681659,0.2726833854) [ #5 ]
	  #2: (0.7685739201,0.5612500564,0.4878506383) [ #3 ]
	  #4: (0.5965716008,0.2660512897,0.022327458) [ #11 ]
	  #6: (0.6702460561,0.9116797088,0.38213495) [ #13 ]
	  #8: (0.8972157686,0.7339704534,0.1277633953) [ #9 ]
	  #10: (0.3569481244,0.0666660061,0.3927549945) [ #11 ]
	  #12: (0.4318964698,0.8467889367,0.5935844293) [ #13 ]
	Steiner points:
	  #3: (0.7685743592555095,0.5612501120312441,0.48785045396340077) [ #2 #0 #7 ]
	  #5: (0.7889988607986429,0.38768165900056817,0.27268338540017245) [ #7 #11 #1 ]
	  #7: (0.7773973960623054,0.5650607262919037,0.3833115372351174) [ #9 #5 #3 ]
	  #9: (0.7826749409622566,0.7124602903909071,0.30452036542581534) [ #7 #8 #13 ]
	  #11: (0.6111497594027574,0.26618781358528576,0.1705221522602712) [ #10 #5 #4 ]
	  #13: (0.6654321509974721,0.8926434708185254,0.38811200866574147) [ #6 #9 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.396636588
	Number of best updates: 13

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 8628

	Init times:
		Bottleneck Steiner distances: 0.000018293
		Smallest sphere distances: 0.000001073
		Sorting: 0.000002214
		Total init time: 0.000023032

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000717, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003355, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000029255, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000324882, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.00348394, bsd pruned: 945, ss pruned: 1890

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 7560
	Total number iterations 84339 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 11.155952380952382
	Total time: 0.388728149
	Avarage time pr. topology: 0.000051419

Data for the geometric median iteration:
	Total time: 0.357553517
	Total steps: 2548768
	Average number of steps pr. geometric median: 56.189770723104054
	Average time pr. step: 0.00000014028484232382075
	Average time pr. geometric median: 0.000007882573126102292

