All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.5964750227066205
MST len: 2.7991606834000002
Steiner Ratio: 0.927590558878818
Solution Steiner tree:
	Terminals:
	  #0: (0.6898134,0.18037494,0.5748512617) [ #5 ]
	  #1: (0.41691941,0.3503611406,0.4857762337) [ #7 ]
	  #2: (0.7084588719,0.4244470016,0.6334648415) [ #11 ]
	  #4: (0.5982263575,0.1752538104,0.4766825421) [ #9 ]
	  #6: (0.1829102199,0.3235021184,0.6120159149) [ #13 ]
	  #8: (0.9935369142,0.1363650975,0.5229908035) [ #9 ]
	  #10: (0.7851141266,0.5901703474,0.1827713406) [ #11 ]
	  #12: (0.1399146664,0.4673976025,0.8627957766) [ #13 ]
	Steiner points:
	  #3: (0.6592843858334286,0.350363901347696,0.5248415369923526) [ #11 #7 #5 ]
	  #5: (0.6592843858334286,0.18037494,0.5248415357666667) [ #0 #9 #3 ]
	  #7: (0.41691941,0.350363901347696,0.5248415369923526) [ #3 #1 #13 ]
	  #9: (0.6592843858334286,0.1752538104,0.5248415357666667) [ #8 #5 #4 ]
	  #11: (0.7084588719,0.4244470016,0.5248415369923526) [ #10 #3 #2 ]
	  #13: (0.18291021989999998,0.350363901347696,0.6120159148999998) [ #6 #7 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.041519052
	Number of best updates: 7

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 3228

	Init times:
		Bottleneck Steiner distances: 0.000024323
		Smallest sphere distances: 0.00000325
		Sorting: 0.000008706
		Total init time: 0.000037369

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001444, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00000677, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00006187, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000698704, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.00467209, bsd pruned: 4758, ss pruned: 3477

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 2160
	Total number iterations 4370 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0231481481481484
	Total time: 0.032265728
	Avarage time pr. topology: 0.000014937

Data for the geometric median iteration:
	Total time: 0.025258817
	Total steps: 72136
	Average number of steps pr. geometric median: 5.5660493827160495
	Average time pr. step: 0.00000035015549794831985
	Average time pr. geometric median: 0.0000019489827932098766

