All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.6927164719591237
MST len: 2.911088162361824
Steiner Ratio: 0.9249862325620771
Solution Steiner tree:
	Terminals:
	  #0: (0.1812310276,0.1675375119,0.7022328906) [ #9 ]
	  #1: (0.5244897048,0.4379004908,0.0703035561) [ #5 ]
	  #2: (0.7090024225,0.6096137295,0.470729368) [ #3 ]
	  #4: (0.1086744881,0.7979400231,0.6606961906) [ #11 ]
	  #6: (0.0071734451,0.7727813925,0.162069249) [ #7 ]
	  #8: (0.5849627301,0.033694007,0.4157471649) [ #9 ]
	  #10: (0.0411185827,0.809808453,0.6971266361) [ #11 ]
	  #12: (0.4311483691,0.7982122655,0.1291394788) [ #13 ]
	Steiner points:
	  #3: (0.5668787106900908,0.4724198000616659,0.3499742970166104) [ #9 #2 #5 ]
	  #5: (0.5054651643987755,0.5252463824939536,0.19081412222985333) [ #1 #3 #13 ]
	  #7: (0.15362503527888766,0.7656869904819554,0.2722142516259988) [ #6 #11 #13 ]
	  #9: (0.48340222862472426,0.19489173785843364,0.46464841293388) [ #3 #8 #0 ]
	  #11: (0.10867448802992484,0.7979400231065523,0.6606961905516674) [ #10 #7 #4 ]
	  #13: (0.393667840710929,0.7340657674621727,0.16967340649603402) [ #5 #7 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.416426096
	Number of best updates: 12

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 9573

	Init times:
		Bottleneck Steiner distances: 0.000018618
		Smallest sphere distances: 0.00000104
		Sorting: 0.000002169
		Total init time: 0.000023235

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000069, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003321, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000029103, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000326049, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.003833717, bsd pruned: 0, ss pruned: 1890

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 8505
	Total number iterations 85141 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 10.010699588477367
	Total time: 0.407991301
	Avarage time pr. topology: 0.00004797

Data for the geometric median iteration:
	Total time: 0.37624875
	Total steps: 2685890
	Average number of steps pr. geometric median: 52.63354889280815
	Average time pr. step: 0.00000014008345464631836
	Average time pr. geometric median: 0.0000073730893592004705

