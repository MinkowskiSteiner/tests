All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.418969246800001
MST len: 4.197877428200001
Steiner Ratio: 0.8144518998655027
Solution Steiner tree:
	Terminals:
	  #0: (0.4224866822,0.2062651386,0.2501284509) [ #11 ]
	  #1: (0.7606715722,0.0816722829,0.5509562085) [ #7 ]
	  #2: (0.2188497932,0.4543959207,0.5186540231) [ #5 ]
	  #4: (0.033469948,0.3299642076,0.6906357043) [ #13 ]
	  #6: (0.6365585242,0.8636223808,0.3016556312) [ #9 ]
	  #8: (0.5646518891,0.743270507,0.9817601498) [ #9 ]
	  #10: (0.3178602631,0.1357283928,0.106745258) [ #11 ]
	  #12: (0.0249239351,0.3649927091,0.7653809822) [ #13 ]
	Steiner points:
	  #3: (0.3876112091666667,0.328887827200668,0.5509562085) [ #5 #7 #11 ]
	  #5: (0.21884979320000003,0.3416403747503463,0.5509562085) [ #2 #3 #13 ]
	  #7: (0.6125896458461548,0.328887827200668,0.5509562085) [ #9 #3 #1 ]
	  #9: (0.6125896458461548,0.7432705070000001,0.5509562085) [ #6 #7 #8 ]
	  #11: (0.3876112091666667,0.2062651386,0.2501284509) [ #10 #3 #0 ]
	  #13: (0.033469948,0.3416403747503463,0.6906357042999999) [ #4 #5 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.074324306
	Number of best updates: 10

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 5268

	Init times:
		Bottleneck Steiner distances: 0.000016131
		Smallest sphere distances: 0.000003024
		Sorting: 0.000017778
		Total init time: 0.000037993

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.0000013, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000006794, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000062021, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000708772, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.005794091, bsd pruned: 0, ss pruned: 6195

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 4200
	Total number iterations 8488 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.020952380952381
	Total time: 0.062663184
	Avarage time pr. topology: 0.000014919

Data for the geometric median iteration:
	Total time: 0.049087894
	Total steps: 138778
	Average number of steps pr. geometric median: 5.507063492063492
	Average time pr. step: 0.00000035371524305005115
	Average time pr. geometric median: 0.0000019479323015873015

