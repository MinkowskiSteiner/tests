All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.245273494374364
MST len: 2.334988433245285
Steiner Ratio: 0.9615779943088495
Solution Steiner tree:
	Terminals:
	  #0: (0.726414976,0.6913943327,0.1680129702) [ #11 ]
	  #1: (0.6031280205,0.3763264294,0.7351060159) [ #7 ]
	  #2: (0.7641613385,0.5371329414,0.440277009) [ #3 ]
	  #4: (0.9043156765,0.6258278161,0.215128048) [ #5 ]
	  #6: (0.2804932088,0.3381360212,0.8132569412) [ #9 ]
	  #8: (0.1078885724,0.4860870128,0.6620165676) [ #9 ]
	  #10: (0.9171544332,0.8540148287,0.22754956) [ #11 ]
	  #12: (0.5717940003,0.0018524737,0.1227083798) [ #13 ]
	Steiner points:
	  #3: (0.7641613384999961,0.5371329413999881,0.4402770089999832) [ #5 #2 #13 ]
	  #5: (0.858753301702639,0.6471697640367743,0.23999080771961634) [ #11 #4 #3 ]
	  #7: (0.6010471727699676,0.376349323231509,0.7328090573918535) [ #1 #9 #13 ]
	  #9: (0.28563504700595094,0.34850360765517396,0.80070518141296) [ #8 #7 #6 ]
	  #11: (0.8325080836671291,0.6975945709617205,0.21758174493115556) [ #10 #5 #0 ]
	  #13: (0.68008148267167,0.4004812602811687,0.4879951158610588) [ #3 #7 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.282366163
	Number of best updates: 20

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 7668

	Init times:
		Bottleneck Steiner distances: 0.000019921
		Smallest sphere distances: 0.000000944
		Sorting: 0.0000022
		Total init time: 0.000024538

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000762, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003272, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000029087, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000271132, bsd pruned: 0, ss pruned: 210
		Level 5 - Time: 0.003039858, bsd pruned: 0, ss pruned: 1275

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 6810
	Total number iterations 62193 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 9.132599118942732
	Total time: 0.275250225
	Avarage time pr. topology: 0.000040418

Data for the geometric median iteration:
	Total time: 0.251818275
	Total steps: 1806356
	Average number of steps pr. geometric median: 44.208418991678904
	Average time pr. step: 0.00000013940678083389983
	Average time pr. geometric median: 0.000006162953377386196

Data for the geometric median step function:
	Total number of precision errors encountered: 0

