All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.0669495054137244
MST len: 3.335913379603145
Steiner Ratio: 0.9193732439715153
Solution Steiner tree:
	Terminals:
	  #0: (0.1796468744,0.8166862739,0.1834716542) [ #5 ]
	  #1: (0.8809632025,0.5625731631,0.6635139085) [ #9 ]
	  #2: (0.5658107323,0.6109299178,0.5057680786) [ #3 ]
	  #4: (0.584652962,0.4221560766,0.0253341845) [ #7 ]
	  #6: (0.3162595943,0.0612761933,0.083659018) [ #11 ]
	  #8: (0.9814409283,0.9619104471,0.1394470134) [ #13 ]
	  #10: (0.0530768098,0.2689884604,0.0923382296) [ #11 ]
	  #12: (0.9767909204,0.9780582851,0.8738890034) [ #13 ]
	Steiner points:
	  #3: (0.565812436888605,0.6109302769054428,0.5057659645305067) [ #9 #2 #5 ]
	  #5: (0.40934147845787655,0.5898952760985973,0.2545818591992282) [ #3 #0 #7 ]
	  #7: (0.4454923305546861,0.4251359595419592,0.11717595046429652) [ #4 #11 #5 ]
	  #9: (0.8217013729035518,0.6534226086737989,0.6055677134788306) [ #3 #1 #13 ]
	  #11: (0.2633404037223362,0.22387170269813722,0.09520296557585863) [ #10 #7 #6 ]
	  #13: (0.8882291056393073,0.7880869792824298,0.5889190894181051) [ #8 #9 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.259856251
	Number of best updates: 8

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 4848

	Init times:
		Bottleneck Steiner distances: 0.000018691
		Smallest sphere distances: 0.000000977
		Sorting: 0.000002479
		Total init time: 0.000023383

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000753, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003287, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00002966, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000326059, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.002932242, bsd pruned: 2835, ss pruned: 3780

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 3780
	Total number iterations 56880 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 15.047619047619047
	Total time: 0.253039371
	Avarage time pr. topology: 0.000066941

Data for the geometric median iteration:
	Total time: 0.232133784
	Total steps: 1651643
	Average number of steps pr. geometric median: 72.82376543209877
	Average time pr. step: 0.00000014054719088810356
	Average time pr. geometric median: 0.000010235175661375663

Data for the geometric median step function:
	Total number of precision errors encountered: 0

