All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.347313363600004
MST len: 3.9143785399000004
Steiner Ratio: 0.8551327699863992
Solution Steiner tree:
	Terminals:
	  #0: (0.2520150534,0.5079065824,0.3625599515) [ #7 ]
	  #1: (0.2932653806,0.3414342214,0.5614015812) [ #3 ]
	  #2: (0.6125465755,0.2276748625,0.680792569) [ #11 ]
	  #4: (0.3646502115,0.237839454,0.4875804249) [ #5 ]
	  #6: (0.0897512576,0.6359386517,0.9450358706) [ #9 ]
	  #8: (0.5259626366,0.9567871433,0.9126800242) [ #9 ]
	  #10: (0.8518746006,0.2945063893,0.4759735886) [ #11 ]
	  #12: (0.1689700215,0.0285875835,0.7728105293) [ #13 ]
	Steiner points:
	  #3: (0.2520150534,0.3414342214,0.680792569) [ #7 #1 #13 ]
	  #5: (0.3646502115,0.237839454,0.6125195755333533) [ #4 #11 #13 ]
	  #7: (0.2520150534,0.5079065824,0.680792569) [ #9 #3 #0 ]
	  #9: (0.2520150534,0.6359386517,0.9126800242) [ #8 #6 #7 ]
	  #11: (0.6125465755,0.23783945400000386,0.6125195755333533) [ #10 #5 #2 ]
	  #13: (0.2520150534,0.23783945399999998,0.6807925690000001) [ #3 #5 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.151333588
	Number of best updates: 17

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 10518

	Init times:
		Bottleneck Steiner distances: 0.000017056
		Smallest sphere distances: 0.00000308
		Sorting: 0.00001367
		Total init time: 0.000034796

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001439, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000006607, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000061398, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000690254, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.008585808, bsd pruned: 0, ss pruned: 945

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 9450
	Total number iterations 19255 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0375661375661376
	Total time: 0.133669306
	Avarage time pr. topology: 0.000014144

Data for the geometric median iteration:
	Total time: 0.103358491
	Total steps: 292162
	Average number of steps pr. geometric median: 5.152768959435626
	Average time pr. step: 0.00000035377116462784344
	Average time pr. geometric median: 0.0000018229010758377424

