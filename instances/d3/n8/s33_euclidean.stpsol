All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.319453335511132
MST len: 2.3673872732510812
Steiner Ratio: 0.9797523885164244
Solution Steiner tree:
	Terminals:
	  #0: (0.3251002693,0.2867596859,0.331827752) [ #5 ]
	  #1: (0.4798882434,0.6288429693,0.6801386246) [ #3 ]
	  #2: (0.3079489355,0.6126976412,0.4672263043) [ #9 ]
	  #4: (0.5413947988,0.3004468834,0.263256482) [ #13 ]
	  #6: (0.2627286242,0.2802385941,0.3090906377) [ #7 ]
	  #8: (0.1237438932,0.9474799428,0.1736593936) [ #9 ]
	  #10: (0.2058983465,0.1701670499,0.6832347255) [ #11 ]
	  #12: (0.8872430967,0.7088636578,0.0528921765) [ #13 ]
	Steiner points:
	  #3: (0.3359242071248192,0.566371606795746,0.5051058884594444) [ #9 #11 #1 ]
	  #5: (0.3251002693,0.2867596859,0.331827752) [ #7 #0 #13 ]
	  #7: (0.3064808856501564,0.290538076260802,0.33924244356014266) [ #6 #5 #11 ]
	  #9: (0.30794893558080805,0.6126976411871925,0.46722630422982536) [ #8 #3 #2 ]
	  #11: (0.29189749430893513,0.3419720690616842,0.46543744516284746) [ #3 #7 #10 ]
	  #13: (0.5413915505183136,0.3004529147157642,0.26325588661325255) [ #4 #5 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.43914578
	Number of best updates: 11

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 8628

	Init times:
		Bottleneck Steiner distances: 0.00001042
		Smallest sphere distances: 0.000001178
		Sorting: 0.000002218
		Total init time: 0.000014969

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000734, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003389, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000028924, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000362462, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.003527144, bsd pruned: 0, ss pruned: 2835

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 7560
	Total number iterations 91850 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 12.149470899470899
	Total time: 0.431157247
	Avarage time pr. topology: 0.000057031

Data for the geometric median iteration:
	Total time: 0.397227917
	Total steps: 2830217
	Average number of steps pr. geometric median: 62.39455467372134
	Average time pr. step: 0.00000014035245954638814
	Average time pr. geometric median: 0.000008757229210758378

