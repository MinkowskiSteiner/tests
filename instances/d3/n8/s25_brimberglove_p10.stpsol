All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.9551627063259747
MST len: 3.309691747
Steiner Ratio: 0.892881552792528
Solution Steiner tree:
	Terminals:
	  #0: (0.7723662428,0.4866999432,0.7424799096) [ #11 ]
	  #1: (0.5821549858,0.7011187615,0.5075186931) [ #5 ]
	  #2: (0.9913254278,0.7521004219,0.7428126711) [ #3 ]
	  #4: (0.9965749998,0.8456901428,0.1036064635) [ #7 ]
	  #6: (0.9414201057,0.9901046399,0.0505868262) [ #9 ]
	  #8: (0.5370173396,0.9999918654,0.1990076612) [ #9 ]
	  #10: (0.9785602656,0.3255712615,0.9922046186) [ #11 ]
	  #12: (0.939356192,0.805739215,0.042332178) [ #13 ]
	Steiner points:
	  #3: (0.9193480440917368,0.7521004219,0.7428126711) [ #11 #2 #5 ]
	  #5: (0.9193480440917368,0.7521004219,0.5075186931) [ #1 #3 #13 ]
	  #7: (0.9193480440917369,0.8456901428,0.11773365029999999) [ #4 #9 #13 ]
	  #9: (0.9193480440917369,0.9901046399000001,0.11773365029999999) [ #8 #7 #6 ]
	  #11: (0.9193480440917368,0.4866999432,0.7428126710999999) [ #10 #3 #0 ]
	  #13: (0.9193529277011847,0.805739215,0.11773365029999999) [ #5 #7 #12 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.053116084
	Number of best updates: 15

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 4023

	Init times:
		Bottleneck Steiner distances: 0.000016458
		Smallest sphere distances: 0.000003063
		Sorting: 0.000008509
		Total init time: 0.000028937

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001392, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000006578, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000061211, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.00056573, bsd pruned: 0, ss pruned: 210
		Level 5 - Time: 0.004444977, bsd pruned: 735, ss pruned: 4185

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 3165
	Total number iterations 6378 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0151658767772513
	Total time: 0.044099553
	Avarage time pr. topology: 0.000013933

Data for the geometric median iteration:
	Total time: 0.034013287
	Total steps: 97130
	Average number of steps pr. geometric median: 5.114797261716693
	Average time pr. step: 0.0000003501831257078143
	Average time pr. geometric median: 0.000001791115692469721

