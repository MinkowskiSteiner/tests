All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.080202697954118
MST len: 4.606702649100001
Steiner Ratio: 0.8857100205395841
Solution Steiner tree:
	Terminals:
	  #0: (0.3443029827,0.6699542467,0.4050064401) [ #11 ]
	  #1: (0.3549731771,0.6731490375,0.5153574038) [ #3 ]
	  #2: (0.3319424024,0.7808070447,0.5882075376) [ #9 ]
	  #4: (0.9775588964,0.7308916956,0.3246047955) [ #7 ]
	  #6: (0.8388915681,0.8634974965,0.5150421176) [ #17 ]
	  #8: (0.2768868838,0.9831143995,0.7832298362) [ #9 ]
	  #10: (0.619858953,0.1357802214,0.2613565746) [ #13 ]
	  #12: (0.3857721921,0.3929051535,0.0476035024) [ #15 ]
	  #14: (0.3598284723,0.0994729628,0.0463795699) [ #15 ]
	  #16: (0.9928987739,0.9662594152,0.9828661391) [ #17 ]
	  #18: (0.2348172144,0.1034733854,0.3869860398) [ #19 ]
	Steiner points:
	  #3: (0.35497057286267514,0.6731490375,0.5150421124910002) [ #5 #1 #11 ]
	  #5: (0.35497057286267514,0.7308916956,0.5150421124910002) [ #7 #3 #9 ]
	  #7: (0.8902273033665193,0.7308916956,0.51504211134714) [ #17 #4 #5 ]
	  #9: (0.3319424024,0.7808070447,0.5882075375999999) [ #8 #2 #5 ]
	  #11: (0.3443029827,0.6699542467,0.4050064401) [ #3 #0 #19 ]
	  #13: (0.3598284722999999,0.1357802214,0.2613565746) [ #15 #10 #19 ]
	  #15: (0.3598284723,0.1357802214,0.047603502401257665) [ #13 #14 #12 ]
	  #17: (0.8902273033665193,0.8634974964999999,0.5150421176) [ #16 #7 #6 ]
	  #19: (0.3443029827,0.1357802214,0.3869860398) [ #11 #13 #18 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 335.154470138
	Number of best updates: 36

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 16154898

	Init times:
		Bottleneck Steiner distances: 0.000108498
		Smallest sphere distances: 0.000019775
		Sorting: 0.000069028
		Total init time: 0.000202824

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000003409, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000009238, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000067264, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000723959, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.009661634, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.147131975, bsd pruned: 0, ss pruned: 0
		Level 7 - Time: 2.295104125, bsd pruned: 135135, ss pruned: 135135
		Level 8 - Time: 28.892985995, bsd pruned: 7972965, ss pruned: 7640325

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 14251545
	Total number iterations 29081142 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0405606550026683
	Total time: 282.897305752
	Avarage time pr. topology: 0.000019849

Data for the geometric median iteration:
	Total time: 215.415870634
	Total steps: 584345331
	Average number of steps pr. geometric median: 4.555804932026668
	Average time pr. step: 0.00000036864480505963
	Average time pr. geometric median: 0.0000016794738210566718

Data for the geometric median step function:
	Total number of fixed points encountered: 983849103

