All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.564451540604318
MST len: 3.8550151740417653
Steiner Ratio: 0.9246271103174912
Solution Steiner tree:
	Terminals:
	  #0: (0.3527607286,0.7895895954,0.9058258696) [ #3 ]
	  #1: (0.2810107266,0.2728543464,0.6146288172) [ #13 ]
	  #2: (0.6533997663,0.55006754,0.2691321919) [ #7 ]
	  #4: (0.6433020787,0.9163497486,0.5770127282) [ #5 ]
	  #6: (0.8492760145,0.5195885676,0.7242932826) [ #9 ]
	  #8: (0.6820132009,0.3835517202,0.9017543196) [ #15 ]
	  #10: (0.886807702,0.5707898632,0.9158954317) [ #17 ]
	  #12: (0.0059619849,0.751268397,0.5345170878) [ #19 ]
	  #14: (0.9977393495,0.1668278385,0.9347036872) [ #15 ]
	  #16: (0.8679820145,0.8387081427,0.9220907362) [ #17 ]
	  #18: (0.043887435,0.9542649202,0.248271084) [ #19 ]
	Steiner points:
	  #3: (0.38171308248373464,0.7163320355388799,0.7190276335109574) [ #13 #0 #5 ]
	  #5: (0.5922487190887398,0.7590073666365645,0.6026408369837765) [ #4 #3 #7 ]
	  #7: (0.6764818335046385,0.6434165539783578,0.5539616165736572) [ #2 #5 #9 ]
	  #9: (0.8492760143966384,0.5195885676417015,0.7242932826571441) [ #6 #7 #11 ]
	  #11: (0.848227678969176,0.495734352517683,0.8441479051031849) [ #17 #15 #9 ]
	  #13: (0.25074575362651075,0.6188971754419142,0.638621389207354) [ #1 #3 #19 ]
	  #15: (0.8082253244049308,0.39790059015695417,0.8814754737893945) [ #11 #14 #8 ]
	  #17: (0.8868077019997954,0.5707898632001563,0.9158954316996945) [ #16 #11 #10 ]
	  #19: (0.01522200594883804,0.7525406275555456,0.5299595550033207) [ #12 #13 #18 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 1352.004152312
	Number of best updates: 30

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 19876308

	Init times:
		Bottleneck Steiner distances: 0.000027975
		Smallest sphere distances: 0.000001663
		Sorting: 0.000005398
		Total init time: 0.000036389

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000831, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003326, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000029872, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000343538, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004590142, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.067817672, bsd pruned: 0, ss pruned: 0
		Level 7 - Time: 1.176707483, bsd pruned: 0, ss pruned: 0
		Level 8 - Time: 16.837444157, bsd pruned: 10135125, ss pruned: 6621615

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 17702685
	Total number iterations 237107885 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 13.393893920611477
	Total time: 1320.284768812
	Avarage time pr. topology: 0.00007458

Data for the geometric median iteration:
	Total time: 1190.926072949
	Total steps: 8411598258
	Average number of steps pr. geometric median: 52.7954956362081
	Average time pr. step: 0.00000014158142560081832
	Average time pr. geometric median: 0.000007474861537476126

Data for the geometric median step function:
	Total number of precision errors encountered: 54768

