All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 5.496387393088423
MST len: 6.0898935094999995
Steiner Ratio: 0.9025424475016304
Solution Steiner tree:
	Terminals:
	  #0: (0.9914577939,0.2373887199,0.3081078475) [ #7 ]
	  #1: (0.7227528005,0.6292115332,0.7207948913) [ #5 ]
	  #2: (0.6704191042,0.7412129779,0.229606642) [ #17 ]
	  #4: (0.0740991244,0.9287021542,0.8842824203) [ #15 ]
	  #6: (0.6750120226,0.2615602367,0.272972847) [ #13 ]
	  #8: (0.3588790108,0.884531313,0.4632373589) [ #11 ]
	  #10: (0.206965448,0.6995153961,0.2034307295) [ #11 ]
	  #12: (0.7304651908,0.05407485,0.9231773037) [ #19 ]
	  #14: (0.9381766622,0.9481784259,0.9291220382) [ #15 ]
	  #16: (0.6858023101,0.9032684844,0.0947479369) [ #17 ]
	  #18: (0.0917933952,0.0898954501,0.3307895457) [ #19 ]
	Steiner points:
	  #3: (0.6704191042000548,0.7205638494936414,0.3278035108867133) [ #17 #5 #9 ]
	  #5: (0.6704191042000548,0.7205638494936414,0.7207948913) [ #15 #1 #3 ]
	  #7: (0.6750120225999999,0.2615602367,0.32631213612492105) [ #0 #13 #9 ]
	  #9: (0.6704191042000548,0.6995153961,0.3278035108867133) [ #7 #3 #11 ]
	  #11: (0.3588790108,0.6995153961,0.3278035108867133) [ #8 #10 #9 ]
	  #13: (0.6750120226,0.2615602367,0.32631213612492105) [ #7 #6 #19 ]
	  #15: (0.6704191042000548,0.9287021542,0.8842824202999999) [ #14 #5 #4 ]
	  #17: (0.6704191042018182,0.7412129779,0.229606642) [ #16 #3 #2 ]
	  #19: (0.6750120225999999,0.0898954501,0.3307895457000001) [ #12 #13 #18 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 566.673136101
	Number of best updates: 47

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 27984408

	Init times:
		Bottleneck Steiner distances: 0.000045323
		Smallest sphere distances: 0.000007064
		Sorting: 0.000026075
		Total init time: 0.000081903

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001595, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000007189, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00006458, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000732077, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.009785208, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.148567663, bsd pruned: 0, ss pruned: 0
		Level 7 - Time: 2.592432703, bsd pruned: 0, ss pruned: 0
		Level 8 - Time: 40.382044365, bsd pruned: 4054050, ss pruned: 4594590

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 25810785
	Total number iterations 52388474 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.029712540707305
	Total time: 490.654118536
	Avarage time pr. topology: 0.000019009

Data for the geometric median iteration:
	Total time: 370.022376275
	Total steps: 1008796919
	Average number of steps pr. geometric median: 4.342701957943377
	Average time pr. step: 0.00000036679570417581737
	Average time pr. geometric median: 0.0000015928844226895418

Data for the geometric median step function:
	Total number of fixed points encountered: 1809119119

