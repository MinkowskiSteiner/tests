All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.034305967986663
MST len: 4.6356764989
Steiner Ratio: 0.8702734043119627
Solution Steiner tree:
	Terminals:
	  #0: (0.581988618,0.1856662101,0.839495378) [ #5 ]
	  #1: (0.391475477,0.4961270823,0.6485379798) [ #9 ]
	  #2: (0.460222048,0.0529571777,0.5661688692) [ #3 ]
	  #4: (0.6549854328,0.0995092923,0.7305007729) [ #5 ]
	  #6: (0.6981957097,0.2743996956,0.1229939042) [ #19 ]
	  #8: (0.0288339364,0.6024998616,0.746623855) [ #11 ]
	  #10: (0.1415459719,0.6331634999,0.8501201872) [ #13 ]
	  #12: (0.1499063168,0.7037021051,0.7905814442) [ #13 ]
	  #14: (0.5198903161,0.7032006042,0.0538963489) [ #15 ]
	  #16: (0.1537148399,0.2108905191,0.9520467748) [ #17 ]
	  #18: (0.9994984996,0.2633149048,0.2158478607) [ #19 ]
	Steiner points:
	  #3: (0.460222048,0.18566621010000003,0.5661688692) [ #2 #7 #5 ]
	  #5: (0.581988618,0.1856662101,0.7305007729) [ #4 #0 #3 ]
	  #7: (0.460222048,0.2743996956,0.5661688692) [ #3 #9 #15 ]
	  #9: (0.391475477,0.49612708229999997,0.6485379798) [ #7 #17 #1 ]
	  #11: (0.1499063168,0.6024998616,0.7905814442000001) [ #8 #13 #17 ]
	  #13: (0.1499063168,0.602499861600548,0.7905814442000001) [ #11 #12 #10 ]
	  #15: (0.5198903161,0.2743996956,0.15394522303333294) [ #14 #7 #19 ]
	  #17: (0.1537148399,0.49612708229999997,0.7905814442000001) [ #9 #11 #16 ]
	  #19: (0.6981957097,0.27439699001278983,0.15394522303333294) [ #6 #15 #18 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 238.418879239
	Number of best updates: 30

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 12277563

	Init times:
		Bottleneck Steiner distances: 0.00006479
		Smallest sphere distances: 0.000005763
		Sorting: 0.000026869
		Total init time: 0.000099776

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001509, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000007179, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000064544, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000732263, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.009727027, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.147074791, bsd pruned: 0, ss pruned: 0
		Level 7 - Time: 2.538645894, bsd pruned: 0, ss pruned: 0
		Level 8 - Time: 27.677477578, bsd pruned: 6081075, ss pruned: 18274410

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 10103940
	Total number iterations 20493569 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.02827500955073
	Total time: 190.553481442
	Avarage time pr. topology: 0.000018858

Data for the geometric median iteration:
	Total time: 143.476837141
	Total steps: 400134094
	Average number of steps pr. geometric median: 4.400198712361492
	Average time pr. step: 0.00000035857188700595954
	Average time pr. geometric median: 0.0000015777875554926537

Data for the geometric median step function:
	Total number of fixed points encountered: 694990104

