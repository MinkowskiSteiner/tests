All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.6847743801218396
MST len: 1.749874014939409
Steiner Ratio: 0.9627975304154548
Solution Steiner tree:
	Terminals:
	  #0: (0.7013661609,0.408563846,0.7201248397) [ #3 ]
	  #1: (0.3412099454,0.230164813,0.4956663146) [ #3 ]
	  #2: (0.442212973,0.1277577337,0.1569511728) [ #5 ]
	  #4: (0.0837527169,0.7310733133,0.0224596341) [ #5 ]
	  #6: (0.517155105,0.0449436945,0.4524295369) [ #7 ]
	Steiner points:
	  #3: (0.42465694648187396,0.21427106649480515,0.4854716344782304) [ #0 #1 #7 ]
	  #5: (0.4162893625104457,0.1823942015240903,0.21367877815776207) [ #2 #4 #7 ]
	  #7: (0.45530949850340546,0.14825920499479267,0.41688555306046055) [ #3 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.014213254
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000130139
		Smallest sphere distances: 0.000010159
		Sorting: 0.000015476
		Total init time: 0.000166564

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000011647, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000081973, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 66 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 4.4
	Total time: 0.013684016
	Avarage time pr. topology: 0.000912267

Data for the geometric median iteration:
	Total time: 0.013244122
	Total steps: 2271
	Average number of steps pr. geometric median: 50.46666666666667
	Average time pr. step: 0.000005831845882870982
	Average time pr. geometric median: 0.00029431382222222226

Data for the geometric median step function:
	Total number of precision errors encountered: 0

