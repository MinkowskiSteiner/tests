All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.9354886368531652
MST len: 2.0348562375313217
Steiner Ratio: 0.9511672624112705
Solution Steiner tree:
	Terminals:
	  #0: (0.8574096602,0.9521014504,0.4074837125) [ #7 ]
	  #1: (0.1925271313,0.3823489777,0.5831195775) [ #5 ]
	  #2: (0.1180090462,0.7377023845,0.5829005281) [ #3 ]
	  #4: (0.0619196091,0.0727788904,0.1874291036) [ #5 ]
	  #6: (0.7497371406,0.6546126439,0.7923043043) [ #7 ]
	Steiner points:
	  #3: (0.2514227903528168,0.6014154759123446,0.5985669562287808) [ #5 #2 #7 ]
	  #5: (0.1925273465642552,0.38235961216214115,0.583099466067671) [ #1 #3 #4 ]
	  #7: (0.6714007120839474,0.7112689121139208,0.6648447433427168) [ #0 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.006694474
	Number of best updates: 2

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 15

	Init times:
		Bottleneck Steiner distances: 0.000104136
		Smallest sphere distances: 0.000013463
		Sorting: 0.000027044
		Total init time: 0.00015578

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000011307, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00006584, bsd pruned: 0, ss pruned: 3

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 12
	Total number iterations 31 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.5833333333333335
	Total time: 0.006243579
	Avarage time pr. topology: 0.000520298

Data for the geometric median iteration:
	Total time: 0.00602236
	Total steps: 1054
	Average number of steps pr. geometric median: 29.27777777777778
	Average time pr. step: 0.00000571381404174573
	Average time pr. geometric median: 0.00016728777777777777

Data for the geometric median step function:
	Total number of precision errors encountered: 0

