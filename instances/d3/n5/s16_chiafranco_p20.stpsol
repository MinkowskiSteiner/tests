All distances are in Lp space with p=20 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Rodríguez-chía and Valero-Franco's iteration with the hyberbolic approximation where epsilon=0.00001 stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.4506621039523735
MST len: 1.7710621013374355
Steiner Ratio: 0.8190916077177031
Solution Steiner tree:
	Terminals:
	  #0: (0.8231695224,0.1180784167,0.5551794663) [ #3 ]
	  #1: (0.3833639991,0.2886057418,0.8238008194) [ #3 ]
	  #2: (0.2498201687,0.6923724262,0.3582976681) [ #5 ]
	  #4: (0.5994994997,0.9209823329,0.0083380486) [ #5 ]
	  #6: (0.2154374701,0.0895364271,0.3215230151) [ #7 ]
	Steiner points:
	  #3: (0.5682908466824279,0.33244286036818077,0.6382565166444746) [ #0 #1 #7 ]
	  #5: (0.2941304171512443,0.6477556190738087,0.317602156840964) [ #2 #4 #7 ]
	  #7: (0.5089534975294528,0.3841854691376308,0.5788158824349735) [ #3 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 10.790556527
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000015165
		Smallest sphere distances: 0.000006208
		Sorting: 0.000010136
		Total init time: 0.000032517

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000004899, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000034782, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 1398 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 93.2
	Total time: 10.790431856
	Avarage time pr. topology: 0.719362123

Data for the geometric median iteration:
	Total time: 10.787554368
	Total steps: 1014433
	Average number of steps pr. geometric median: 22542.955555555556
	Average time pr. step: 0.000010634072795344788
	Average time pr. geometric median: 0.2397234304

Data for the geometric median step function:
	Total number of precision errors encountered: 0

