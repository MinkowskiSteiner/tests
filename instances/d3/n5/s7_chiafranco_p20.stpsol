All distances are in Lp space with p=20 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Rodríguez-chía and Valero-Franco's iteration with the hyberbolic approximation where epsilon=0.00001 stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.7439537110307015
MST len: 2.1026163922690375
Steiner Ratio: 0.8294207718739959
Solution Steiner tree:
	Terminals:
	  #0: (0.7644367087,0.815491903,0.8889724807) [ #3 ]
	  #1: (0.4869041394,0.8679774124,0.5925911942) [ #3 ]
	  #2: (0.2147098403,0.0102265389,0.514818562) [ #5 ]
	  #4: (0.0553448466,0.5267799439,0.0893740547) [ #5 ]
	  #6: (0.9959482527,0.0319323023,0.6015652793) [ #7 ]
	Steiner points:
	  #3: (0.649939805346144,0.6924683682100689,0.7665307070662309) [ #0 #1 #7 ]
	  #5: (0.3773642316052724,0.2053947433559517,0.36431553157052765) [ #2 #4 #7 ]
	  #7: (0.5997978320030286,0.42668871441921885,0.5507744509757124) [ #3 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 50.897639024
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000016112
		Smallest sphere distances: 0.000006187
		Sorting: 0.00001032
		Total init time: 0.000033949

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000514, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000037215, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 1205 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 80.33333333333333
	Total time: 50.897497869
	Avarage time pr. topology: 3.393166524

Data for the geometric median iteration:
	Total time: 50.894869784
	Total steps: 4775065
	Average number of steps pr. geometric median: 106112.55555555556
	Average time pr. step: 0.00001065846638401781
	Average time pr. geometric median: 1.1309971063111113

Data for the geometric median step function:
	Total number of precision errors encountered: 0

