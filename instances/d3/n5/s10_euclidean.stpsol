All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.048807180024394
MST len: 2.1762117334198483
Steiner Ratio: 0.9414558099109033
Solution Steiner tree:
	Terminals:
	  #0: (0.9767909204,0.9780582851,0.8738890034) [ #3 ]
	  #1: (0.584652962,0.4221560766,0.0253341845) [ #5 ]
	  #2: (0.5658107323,0.6109299178,0.5057680786) [ #3 ]
	  #4: (0.3162595943,0.0612761933,0.083659018) [ #5 ]
	  #6: (0.1796468744,0.8166862739,0.1834716542) [ #7 ]
	Steiner points:
	  #3: (0.5658109676446723,0.6109306686268259,0.5057675752042945) [ #2 #0 #7 ]
	  #5: (0.5075559064919505,0.4051780493395088,0.09049292347504025) [ #1 #4 #7 ]
	  #7: (0.43715499762810966,0.5921930822063707,0.2632314282010879) [ #3 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000248816
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000003817
		Smallest sphere distances: 0.000000603
		Sorting: 0.00000074
		Total init time: 0.000006036

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000612, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00000367, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 50 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 3.3333333333333335
	Total time: 0.000222549
	Avarage time pr. topology: 0.000014836

Data for the geometric median iteration:
	Total time: 0.000208287
	Total steps: 1320
	Average number of steps pr. geometric median: 29.333333333333332
	Average time pr. step: 0.00000015779318181818182
	Average time pr. geometric median: 0.0000046286

Data for the geometric median step function:
	Total number of precision errors encountered: 0

