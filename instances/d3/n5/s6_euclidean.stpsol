All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.3944572142855978
MST len: 2.6126690510825403
Steiner Ratio: 0.9164793425686548
Solution Steiner tree:
	Terminals:
	  #0: (0.9831179767,0.5296626759,0.9438327159) [ #3 ]
	  #1: (0.4030091993,0.0851281491,0.8056346727) [ #5 ]
	  #2: (0.87826215,0.7782872272,0.4166978609) [ #3 ]
	  #4: (0.6451566176,0.2473998085,0.3098868776) [ #5 ]
	  #6: (0.1354387687,0.9625167521,0.799727213) [ #7 ]
	Steiner points:
	  #3: (0.7655000052486628,0.6137904041728681,0.6437077304193412) [ #2 #0 #7 ]
	  #5: (0.574680375795876,0.33019275274840204,0.5646220527296835) [ #1 #4 #7 ]
	  #7: (0.6233065835198798,0.5648640164958986,0.6374908505924828) [ #3 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000582089
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000003651
		Smallest sphere distances: 0.000000504
		Sorting: 0.000000639
		Total init time: 0.000005534

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000742, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003045, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 116 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 7.733333333333333
	Total time: 0.000560116
	Avarage time pr. topology: 0.000037341

Data for the geometric median iteration:
	Total time: 0.00053405
	Total steps: 3607
	Average number of steps pr. geometric median: 80.15555555555555
	Average time pr. step: 0.0000001480593290823399
	Average time pr. geometric median: 0.000011867777777777777

Data for the geometric median step function:
	Total number of precision errors encountered: 0

