All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.6376539349680856
MST len: 1.709091492287273
Steiner Ratio: 0.9582014434911365
Solution Steiner tree:
	Terminals:
	  #0: (0.3647844728,0.5134009102,0.9522297252) [ #7 ]
	  #1: (0.3352227557,0.7682295948,0.2777747108) [ #5 ]
	  #2: (0.5539699558,0.4773970519,0.6288709248) [ #3 ]
	  #4: (0.7984400335,0.9116473579,0.1975513693) [ #5 ]
	  #6: (0.8401877172,0.3943829268,0.7830992238) [ #7 ]
	Steiner points:
	  #3: (0.5539732802421425,0.477410137963181,0.6288761316384874) [ #5 #2 #7 ]
	  #5: (0.5040616199899897,0.734089610231355,0.3422847157121914) [ #1 #3 #4 ]
	  #7: (0.5793051842049877,0.46664803785451964,0.7193794361638945) [ #0 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000701871
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000027957
		Smallest sphere distances: 0.000001315
		Sorting: 0.00000292
		Total init time: 0.000034227

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000002473, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000004196, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 143 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 9.533333333333333
	Total time: 0.000642825
	Avarage time pr. topology: 0.000042855

Data for the geometric median iteration:
	Total time: 0.000609709
	Total steps: 3377
	Average number of steps pr. geometric median: 75.04444444444445
	Average time pr. step: 0.0000001805475273911756
	Average time pr. geometric median: 0.00001354908888888889

Data for the geometric median step function:
	Total number of precision errors encountered: 0

