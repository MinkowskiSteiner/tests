All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.7244420866456107
MST len: 1.7554305012971656
Steiner Ratio: 0.9823471139252417
Solution Steiner tree:
	Terminals:
	  #0: (0.581988618,0.1856662101,0.839495378) [ #5 ]
	  #1: (0.6549854328,0.0995092923,0.7305007729) [ #3 ]
	  #2: (0.460222048,0.0529571777,0.5661688692) [ #7 ]
	  #4: (0.0288339364,0.6024998616,0.746623855) [ #5 ]
	  #6: (0.9994984996,0.2633149048,0.2158478607) [ #7 ]
	Steiner points:
	  #3: (0.6548426301571659,0.09961897537429552,0.7304479998087089) [ #1 #5 #7 ]
	  #5: (0.5819672001044824,0.18566934192141984,0.8394411337062353) [ #4 #3 #0 ]
	  #7: (0.6227623345353344,0.10098109732357867,0.6134019853351077) [ #2 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000593857
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000003857
		Smallest sphere distances: 0.000000542
		Sorting: 0.000000711
		Total init time: 0.000005863

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000576, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000002976, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 95 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 6.333333333333333
	Total time: 0.000572821
	Avarage time pr. topology: 0.000038188

Data for the geometric median iteration:
	Total time: 0.000551122
	Total steps: 3832
	Average number of steps pr. geometric median: 85.15555555555555
	Average time pr. step: 0.00000014382098121085595
	Average time pr. geometric median: 0.000012247155555555555

Data for the geometric median step function:
	Total number of precision errors encountered: 0

