All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.0588147080504213
MST len: 2.2585354547239986
Steiner Ratio: 0.9115706834462849
Solution Steiner tree:
	Terminals:
	  #0: (0.8231695224,0.1180784167,0.5551794663) [ #3 ]
	  #1: (0.3833639991,0.2886057418,0.8238008194) [ #3 ]
	  #2: (0.2498201687,0.6923724262,0.3582976681) [ #5 ]
	  #4: (0.5994994997,0.9209823329,0.0083380486) [ #5 ]
	  #6: (0.2154374701,0.0895364271,0.3215230151) [ #7 ]
	Steiner points:
	  #3: (0.4561056820390087,0.2558281805463722,0.616949351445847) [ #0 #1 #7 ]
	  #5: (0.26876192109106656,0.6804173391477184,0.3494364757919629) [ #2 #4 #7 ]
	  #7: (0.33535891629274767,0.2961702121839871,0.4603422844716791) [ #3 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000562138
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000003793
		Smallest sphere distances: 0.000000568
		Sorting: 0.000000637
		Total init time: 0.000005689

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000562, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003092, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 101 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 6.733333333333333
	Total time: 0.000540219
	Avarage time pr. topology: 0.000036014

Data for the geometric median iteration:
	Total time: 0.000516856
	Total steps: 3604
	Average number of steps pr. geometric median: 80.08888888888889
	Average time pr. step: 0.00000014341176470588235
	Average time pr. geometric median: 0.000011485688888888889

Data for the geometric median step function:
	Total number of precision errors encountered: 0

