All distances are in Lp space with p=20 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Rodríguez-chía and Valero-Franco's iteration with the hyberbolic approximation where epsilon=0.00001 stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.2615288041419335
MST len: 1.36854970792842
Steiner Ratio: 0.9217997686408597
Solution Steiner tree:
	Terminals:
	  #0: (0.581988618,0.1856662101,0.839495378) [ #3 ]
	  #1: (0.6549854328,0.0995092923,0.7305007729) [ #3 ]
	  #2: (0.460222048,0.0529571777,0.5661688692) [ #5 ]
	  #4: (0.0288339364,0.6024998616,0.746623855) [ #5 ]
	  #6: (0.9994984996,0.2633149048,0.2158478607) [ #7 ]
	Steiner points:
	  #3: (0.652830756577007,0.10056193836556,0.7326082762096714) [ #0 #1 #7 ]
	  #5: (0.518300287508133,0.11126275661767362,0.609173604028812) [ #2 #4 #7 ]
	  #7: (0.5687560083325985,0.12604520602317587,0.648104168645359) [ #3 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 1.319653245
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.00001555
		Smallest sphere distances: 0.000006341
		Sorting: 0.000010221
		Total init time: 0.000033616

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000004761, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00003433, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 536 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 35.733333333333334
	Total time: 1.3195309499999999
	Avarage time pr. topology: 0.087968729

Data for the geometric median iteration:
	Total time: 1.318423645
	Total steps: 123479
	Average number of steps pr. geometric median: 2743.9777777777776
	Average time pr. step: 0.000010677310676309331
	Average time pr. geometric median: 0.029298303222222218

Data for the geometric median step function:
	Total number of precision errors encountered: 0

