All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.2739951495
MST len: 2.7254345798
Steiner Ratio: 0.8343605700001328
Solution Steiner tree:
	Terminals:
	  #0: (0.2885274982,0.0973462291,0.2666000273) [ #3 ]
	  #1: (0.414524392,0.4607495118,0.1862228323) [ #3 ]
	  #2: (0.3292264125,0.6730409189,0.520684405) [ #5 ]
	  #4: (0.0191505631,0.5234706907,0.8290278501) [ #7 ]
	  #6: (0.8650269224,0.5663939652,0.6338536677) [ #7 ]
	Steiner points:
	  #3: (0.28852749820000007,0.4607495118,0.2666000273) [ #0 #5 #1 ]
	  #5: (0.28852749820000007,0.5377784488666666,0.520684405) [ #2 #3 #7 ]
	  #7: (0.28852749820000007,0.5377784488666666,0.6338536676999998) [ #4 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.002672779
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 15

	Init times:
		Bottleneck Steiner distances: 0.000139419
		Smallest sphere distances: 0.000013082
		Sorting: 0.000018072
		Total init time: 0.000184182

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000014811, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000080077, bsd pruned: 0, ss pruned: 3

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 12
	Total number iterations 24 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.002126408
	Avarage time pr. topology: 0.0001772

Data for the geometric median iteration:
	Total time: 0.001837855
	Total steps: 245
	Average number of steps pr. geometric median: 6.805555555555555
	Average time pr. step: 0.000007501448979591837
	Average time pr. geometric median: 0.000051051527777777776

Data for the geometric median step function:
	Total number of fixed points encountered: 234

