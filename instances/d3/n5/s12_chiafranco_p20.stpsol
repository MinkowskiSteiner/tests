All distances are in Lp space with p=20 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Rodríguez-chía and Valero-Franco's iteration with the hyberbolic approximation where epsilon=0.00001 stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.0339014312248485
MST len: 1.2379101439655418
Steiner Ratio: 0.8351990944292867
Solution Steiner tree:
	Terminals:
	  #0: (0.4910349215,0.0853253426,0.4141814399) [ #3 ]
	  #1: (0.2860070939,0.0876902482,0.3650377357) [ #3 ]
	  #2: (0.7856002826,0.4401777473,0.1151188249) [ #7 ]
	  #4: (0.3111650372,0.5165184259,0.6420080073) [ #5 ]
	  #6: (0.8235438768,0.6883942167,0.1299254853) [ #7 ]
	Steiner points:
	  #3: (0.3877654107778925,0.1883145212014148,0.3982168887982584) [ #0 #5 #1 ]
	  #5: (0.49225739598828155,0.31199921063702507,0.43771236145642367) [ #4 #3 #7 ]
	  #7: (0.698549726405431,0.5374356693177309,0.21217038309293804) [ #2 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 7.121422473
	Number of best updates: 2

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 12

	Init times:
		Bottleneck Steiner distances: 0.000016802
		Smallest sphere distances: 0.000007457
		Sorting: 0.000010593
		Total init time: 0.000036424

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000005953, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000026584, bsd pruned: 0, ss pruned: 6

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 9
	Total number iterations 401 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 44.55555555555556
	Total time: 7.121302293
	Avarage time pr. topology: 0.791255809

Data for the geometric median iteration:
	Total time: 7.120372984
	Total steps: 659461
	Average number of steps pr. geometric median: 24424.48148148148
	Average time pr. step: 0.000010797261678855915
	Average time pr. geometric median: 0.26371751792592596

Data for the geometric median step function:
	Total number of precision errors encountered: 0

