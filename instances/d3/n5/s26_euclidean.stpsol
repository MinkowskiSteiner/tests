All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.5943904364621297
MST len: 1.7441182830757551
Steiner Ratio: 0.9141526993515714
Solution Steiner tree:
	Terminals:
	  #0: (0.8887816281,0.2970957897,0.7287201051) [ #7 ]
	  #1: (0.8547150078,0.6357949691,0.4054190886) [ #5 ]
	  #2: (0.7955457819,0.2153102822,0.3426214272) [ #3 ]
	  #4: (0.6635035848,0.7047985609,0.1648330605) [ #5 ]
	  #6: (0.296161194,0.2554381654,0.71008044) [ #7 ]
	Steiner points:
	  #3: (0.7769867759701299,0.34196848598023327,0.4549789010298515) [ #5 #2 #7 ]
	  #5: (0.8134609574971224,0.6003972247420293,0.37653364910037185) [ #1 #3 #4 ]
	  #7: (0.7432794700882147,0.3132301271335064,0.5890482914860227) [ #0 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.030673261
	Number of best updates: 2

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000101415
		Smallest sphere distances: 0.000015455
		Sorting: 0.000014693
		Total init time: 0.000143446

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000012341, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000083619, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 164 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 10.933333333333334
	Total time: 0.030192738
	Avarage time pr. topology: 0.002012849

Data for the geometric median iteration:
	Total time: 0.029242958
	Total steps: 5087
	Average number of steps pr. geometric median: 113.04444444444445
	Average time pr. step: 0.000005748566542166306
	Average time pr. geometric median: 0.0006498435111111111

Data for the geometric median step function:
	Total number of precision errors encountered: 0

