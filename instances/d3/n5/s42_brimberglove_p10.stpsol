All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.1378192219
MST len: 2.3130454712
Steiner Ratio: 0.9242443559879118
Solution Steiner tree:
	Terminals:
	  #0: (0.3178602631,0.1357283928,0.106745258) [ #3 ]
	  #1: (0.033469948,0.3299642076,0.6906357043) [ #5 ]
	  #2: (0.4224866822,0.2062651386,0.2501284509) [ #3 ]
	  #4: (0.0249239351,0.3649927091,0.7653809822) [ #5 ]
	  #6: (0.6365585242,0.8636223808,0.3016556312) [ #7 ]
	Steiner points:
	  #3: (0.3178602631,0.20626513859999998,0.2501284509) [ #2 #0 #7 ]
	  #5: (0.033469948,0.36499270909024817,0.6906357043) [ #1 #4 #7 ]
	  #7: (0.3178602631,0.36499270909024817,0.3016556312) [ #3 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.002032849
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 12

	Init times:
		Bottleneck Steiner distances: 0.000112617
		Smallest sphere distances: 0.000011081
		Sorting: 0.000033781
		Total init time: 0.000168008

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000012747, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00005959, bsd pruned: 0, ss pruned: 6

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 9
	Total number iterations 18 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.001554217
	Avarage time pr. topology: 0.00017269

Data for the geometric median iteration:
	Total time: 0.001354202
	Total steps: 186
	Average number of steps pr. geometric median: 6.888888888888889
	Average time pr. step: 0.000007280655913978495
	Average time pr. geometric median: 0.00005015562962962963

Data for the geometric median step function:
	Total number of fixed points encountered: 193

