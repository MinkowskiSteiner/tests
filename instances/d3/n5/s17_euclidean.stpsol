All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.872366986736816
MST len: 2.036457832550161
Steiner Ratio: 0.9194234011671816
Solution Steiner tree:
	Terminals:
	  #0: (0.1078885724,0.4860870128,0.6620165676) [ #3 ]
	  #1: (0.2804932088,0.3381360212,0.8132569412) [ #3 ]
	  #2: (0.9043156765,0.6258278161,0.215128048) [ #5 ]
	  #4: (0.9171544332,0.8540148287,0.22754956) [ #5 ]
	  #6: (0.5717940003,0.0018524737,0.1227083798) [ #7 ]
	Steiner points:
	  #3: (0.2567505392848682,0.3891049165493408,0.6915554293167738) [ #0 #1 #7 ]
	  #5: (0.9042998750651626,0.6258354538080617,0.21513474094858526) [ #2 #4 #7 ]
	  #7: (0.5867546075579019,0.3088260241713373,0.3139704758580204) [ #3 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.00021331
	Number of best updates: 10

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000005112
		Smallest sphere distances: 0.000000707
		Sorting: 0.000000784
		Total init time: 0.000007703

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000759, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003509, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 34 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.2666666666666666
	Total time: 0.000183751
	Avarage time pr. topology: 0.00001225

Data for the geometric median iteration:
	Total time: 0.000173779
	Total steps: 1193
	Average number of steps pr. geometric median: 26.511111111111113
	Average time pr. step: 0.0000001456655490360436
	Average time pr. geometric median: 0.000003861755555555556

Data for the geometric median step function:
	Total number of precision errors encountered: 0

