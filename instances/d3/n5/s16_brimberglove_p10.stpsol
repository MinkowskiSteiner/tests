All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.8116220380000003
MST len: 3.341446349
Steiner Ratio: 0.8414386299637697
Solution Steiner tree:
	Terminals:
	  #0: (0.3833639991,0.2886057418,0.8238008194) [ #5 ]
	  #1: (0.2154374701,0.0895364271,0.3215230151) [ #3 ]
	  #2: (0.2498201687,0.6923724262,0.3582976681) [ #7 ]
	  #4: (0.8231695224,0.1180784167,0.5551794663) [ #5 ]
	  #6: (0.5994994997,0.9209823329,0.0083380486) [ #7 ]
	Steiner points:
	  #3: (0.36637994126506596,0.2545074522888888,0.3215230151) [ #1 #5 #7 ]
	  #5: (0.3833639991,0.2545074522888888,0.5551794663) [ #4 #3 #0 ]
	  #7: (0.366379941265066,0.6923724262000001,0.32152301510000003) [ #2 #3 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000231391
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 18

	Init times:
		Bottleneck Steiner distances: 0.000006986
		Smallest sphere distances: 0.000001693
		Sorting: 0.000002599
		Total init time: 0.000024151

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001719, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000008064, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 15
	Total number iterations 30 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.000178597
	Avarage time pr. topology: 0.000011906

Data for the geometric median iteration:
	Total time: 0.000147211
	Total steps: 288
	Average number of steps pr. geometric median: 6.4
	Average time pr. step: 0.0000005111493055555556
	Average time pr. geometric median: 0.0000032713555555555557

Data for the geometric median step function:
	Total number of fixed points encountered: 324

