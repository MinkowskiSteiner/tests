All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.966572478231973
MST len: 2.5233577808
Steiner Ratio: 0.7793474604336508
Solution Steiner tree:
	Terminals:
	  #0: (0.3817098766,0.0352495974,0.3687719635) [ #3 ]
	  #1: (0.4041060449,0.0752406661,0.2481696737) [ #3 ]
	  #2: (0.5022105041,0.7537675108,0.4059770589) [ #7 ]
	  #4: (0.4484664097,0.2208949445,0.8329159067) [ #5 ]
	  #6: (0.9263453185,0.5264996991,0.3097808907) [ #7 ]
	Steiner points:
	  #3: (0.40410604490000007,0.0752406661,0.3687719635000001) [ #0 #5 #1 ]
	  #5: (0.4484664097,0.2208949445,0.4172154197659865) [ #4 #3 #7 ]
	  #7: (0.5022105040999999,0.5264996991,0.4172154197659864) [ #2 #5 #6 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.000140305
	Number of best updates: 2

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 12

	Init times:
		Bottleneck Steiner distances: 0.000007996
		Smallest sphere distances: 0.000001691
		Sorting: 0.000002669
		Total init time: 0.000013657

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001514, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000006135, bsd pruned: 0, ss pruned: 6

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 9
	Total number iterations 18 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.00010285
	Avarage time pr. topology: 0.000011427

Data for the geometric median iteration:
	Total time: 0.000082894
	Total steps: 174
	Average number of steps pr. geometric median: 6.444444444444445
	Average time pr. step: 0.0000004764022988505747
	Average time pr. geometric median: 0.000003070148148148148

Data for the geometric median step function:
	Total number of fixed points encountered: 156

