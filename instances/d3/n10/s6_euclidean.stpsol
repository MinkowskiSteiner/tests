All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.2889690974819032
MST len: 3.402936510157868
Steiner Ratio: 0.9665090981463308
Solution Steiner tree:
	Terminals:
	  #0: (0.87826215,0.7782872272,0.4166978609) [ #17 ]
	  #1: (0.6177704891,0.4252223039,0.3870529828) [ #3 ]
	  #2: (0.3191112491,0.3110007436,0.691104038) [ #5 ]
	  #4: (0.4030091993,0.0851281491,0.8056346727) [ #5 ]
	  #6: (0.7761541152,0.6623876005,0.1703382573) [ #7 ]
	  #8: (0.1354387687,0.9625167521,0.799727213) [ #13 ]
	  #10: (0.6451566176,0.2473998085,0.3098868776) [ #11 ]
	  #12: (0.1034285073,0.9779993249,0.8022396713) [ #13 ]
	  #14: (0.2226562315,0.5511224272,0.0462225136) [ #15 ]
	  #16: (0.9831179767,0.5296626759,0.9438327159) [ #17 ]
	Steiner points:
	  #3: (0.6177703832193594,0.42522240093457886,0.38705255706702907) [ #11 #1 #7 ]
	  #5: (0.31911124910001437,0.3110007435999997,0.6911040379999962) [ #2 #4 #9 ]
	  #7: (0.7690930792346852,0.6331433455481358,0.3023727281542012) [ #3 #6 #17 ]
	  #9: (0.33672718603498175,0.461157176503767,0.5943540297228036) [ #5 #13 #15 ]
	  #11: (0.6000856915505007,0.4097109883967094,0.3793420421257148) [ #3 #10 #15 ]
	  #13: (0.1354387687,0.9625167521,0.799727213) [ #12 #8 #9 ]
	  #15: (0.438231224251662,0.4540410840213962,0.3910385447224719) [ #9 #11 #14 ]
	  #17: (0.8652287601764016,0.7382781172556921,0.4274418742913605) [ #0 #7 #16 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 96.101423321
	Number of best updates: 44

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1747428

	Init times:
		Bottleneck Steiner distances: 0.000023891
		Smallest sphere distances: 0.000001396
		Sorting: 0.000004089
		Total init time: 0.000030459

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000789, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00000336, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000030123, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000340218, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004411854, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.067201393, bsd pruned: 0, ss pruned: 0
		Level 7 - Time: 0.977922669, bsd pruned: 0, ss pruned: 426195

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 1600830
	Total number iterations 16698922 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 10.431414953492876
	Total time: 94.109001198
	Avarage time pr. topology: 0.000058787

Data for the geometric median iteration:
	Total time: 85.942207282
	Total steps: 613985503
	Average number of steps pr. geometric median: 47.94274712180556
	Average time pr. step: 0.00000013997432653063798
	Average time pr. geometric median: 0.000006710753740403415

Data for the geometric median step function:
	Total number of precision errors encountered: 2622

