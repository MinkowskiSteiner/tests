All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.697810777578307
MST len: 4.1384433802
Steiner Ratio: 0.8935269708582075
Solution Steiner tree:
	Terminals:
	  #0: (0.3488313283,0.3430800132,0.2595131855) [ #5 ]
	  #1: (0.3097259371,0.5068725648,0.1146662767) [ #3 ]
	  #2: (0.4198029756,0.654078258,0.1943656375) [ #9 ]
	  #4: (0.2600808285,0.2142811954,0.3601613163) [ #7 ]
	  #6: (0.6990013098,0.2817635985,0.6604524426) [ #11 ]
	  #8: (0.5706952487,0.6701787769,0.9271462466) [ #13 ]
	  #10: (0.7238869563,0.0950620273,0.2335532183) [ #17 ]
	  #12: (0.158582164,0.7489994023,0.9178017638) [ #13 ]
	  #14: (0.29734262,0.6824838476,0.0102189095) [ #15 ]
	  #16: (0.9164578756,0.1339819669,0.1912108968) [ #17 ]
	Steiner points:
	  #3: (0.3789828569829129,0.5068725648,0.13924042104230638) [ #1 #5 #15 ]
	  #5: (0.3789828569829129,0.3430800132,0.23355321830000003) [ #0 #3 #7 ]
	  #7: (0.3789828569829129,0.2142811954,0.2335532183) [ #11 #4 #5 ]
	  #9: (0.4198029756,0.654078258,0.19436563750000002) [ #13 #15 #2 ]
	  #11: (0.6990013097999999,0.2142811954,0.2335532183) [ #6 #7 #17 ]
	  #13: (0.4198029756,0.6701787769,0.9178017638) [ #12 #9 #8 ]
	  #15: (0.3789828569829129,0.654078258,0.13924042104230638) [ #14 #9 #3 ]
	  #17: (0.7238869563,0.1339819669,0.23355063408751908) [ #10 #11 #16 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 11.364851942
	Number of best updates: 19

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 649338

	Init times:
		Bottleneck Steiner distances: 0.000035119
		Smallest sphere distances: 0.000005454
		Sorting: 0.000026439
		Total init time: 0.000068756

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001647, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000007017, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00006189, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000700042, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.009489338, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.134748876, bsd pruned: 0, ss pruned: 10395
		Level 7 - Time: 1.345239782, bsd pruned: 374220, ss pruned: 983745

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 513135
	Total number iterations 1036698 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.020322137449209
	Total time: 9.001800229
	Avarage time pr. topology: 0.000017542

Data for the geometric median iteration:
	Total time: 6.879890229
	Total steps: 19433655
	Average number of steps pr. geometric median: 4.734050249934228
	Average time pr. step: 0.00000035401936635182627
	Average time pr. geometric median: 0.00000167594546975942

Data for the geometric median step function:
	Total number of fixed points encountered: 32576279

