All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.660717349130868
MST len: 5.0220249930000005
Steiner Ratio: 0.9280553871450771
Solution Steiner tree:
	Terminals:
	  #0: (0.3055314134,0.1783176238,0.3670024752) [ #7 ]
	  #1: (0.0997784725,0.5895236719,0.2716148031) [ #3 ]
	  #2: (0.2498201687,0.6923724262,0.3582976681) [ #9 ]
	  #4: (0.2154374701,0.0895364271,0.3215230151) [ #11 ]
	  #6: (0.3833639991,0.2886057418,0.8238008194) [ #15 ]
	  #8: (0.3094491373,0.8584360545,0.6783925484) [ #13 ]
	  #10: (0.0389199243,0.0333043621,0.0983172874) [ #11 ]
	  #12: (0.0401816108,0.8858513329,0.9203709387) [ #13 ]
	  #14: (0.8231695224,0.1180784167,0.5551794663) [ #15 ]
	  #16: (0.5994994997,0.9209823329,0.0083380486) [ #17 ]
	Steiner points:
	  #3: (0.3094468650193927,0.5895236719,0.2716148031) [ #1 #5 #9 ]
	  #5: (0.3094468650193927,0.28860566048636516,0.2716148031) [ #7 #3 #11 ]
	  #7: (0.3833642085366764,0.28860566048636516,0.3670024752) [ #15 #0 #5 ]
	  #9: (0.3094468650193927,0.6923724262,0.35829766809999986) [ #3 #2 #17 ]
	  #11: (0.2154374701,0.08953642710000001,0.2716148031000001) [ #4 #10 #5 ]
	  #13: (0.30944913729999934,0.8584360545,0.6783925484) [ #12 #8 #17 ]
	  #15: (0.3833642085366764,0.28860566048636516,0.5551794663) [ #14 #7 #6 ]
	  #17: (0.3094525236517563,0.8584360545,0.35829766809999986) [ #9 #13 #16 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 27.668088671
	Number of best updates: 26

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1487553

	Init times:
		Bottleneck Steiner distances: 0.000030049
		Smallest sphere distances: 0.000004639
		Sorting: 0.000020335
		Total init time: 0.000056894

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001522, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000007538, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000065585, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000729967, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.009675868, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.147698716, bsd pruned: 0, ss pruned: 0
		Level 7 - Time: 1.8746222989999999, bsd pruned: 0, ss pruned: 686070

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 1340955
	Total number iterations 2743288 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0457718566245697
	Total time: 23.994665889
	Avarage time pr. topology: 0.000017892

Data for the geometric median iteration:
	Total time: 18.363221342
	Total steps: 50944413
	Average number of steps pr. geometric median: 4.748892859939372
	Average time pr. step: 0.0000003604560394483297
	Average time pr. geometric median: 0.0000017117671120581973

Data for the geometric median step function:
	Total number of fixed points encountered: 88263460

