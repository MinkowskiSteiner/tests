All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.328686471320906
MST len: 5.066439418500001
Steiner Ratio: 0.8543843345910336
Solution Steiner tree:
	Terminals:
	  #0: (0.0533801331,0.3409319503,0.5098388086) [ #11 ]
	  #1: (0.788697773,0.0666457862,0.4325436914) [ #7 ]
	  #2: (0.163898673,0.2155044015,0.7876827134) [ #3 ]
	  #4: (0.4869041394,0.8679774124,0.5925911942) [ #13 ]
	  #6: (0.7594234276,0.035923094,0.0724071777) [ #7 ]
	  #8: (0.2147098403,0.0102265389,0.514818562) [ #9 ]
	  #10: (0.016024685,0.2613700387,0.3636968142) [ #15 ]
	  #12: (0.7644367087,0.815491903,0.8889724807) [ #13 ]
	  #14: (0.0553448466,0.5267799439,0.0893740547) [ #15 ]
	  #16: (0.9959482527,0.0319323023,0.6015652793) [ #17 ]
	Steiner points:
	  #3: (0.1836063039581888,0.2155044015,0.5901422517790943) [ #9 #2 #5 ]
	  #5: (0.7644367087,0.2155044015,0.5901422517790943) [ #3 #13 #17 ]
	  #7: (0.788697773,0.0666457862,0.4325436914) [ #1 #6 #17 ]
	  #9: (0.1836063039581888,0.2155044015,0.514818562) [ #11 #8 #3 ]
	  #11: (0.0533801331,0.34093195026213435,0.5098388086) [ #9 #15 #0 ]
	  #13: (0.7644367087,0.815491903,0.5925911942) [ #5 #12 #4 ]
	  #15: (0.0533801331,0.34093195026213435,0.36369681419999994) [ #10 #11 #14 ]
	  #17: (0.7886977729999999,0.0666457862,0.5901422517790943) [ #5 #7 #16 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 16.830568561
	Number of best updates: 20

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 947013

	Init times:
		Bottleneck Steiner distances: 0.00004078
		Smallest sphere distances: 0.000006395
		Sorting: 0.000017167
		Total init time: 0.000066247

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001431, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000006792, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000062316, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000701478, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.009346622, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.143785155, bsd pruned: 0, ss pruned: 0
		Level 7 - Time: 1.6354761199999999, bsd pruned: 270270, ss pruned: 956340

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 800415
	Total number iterations 1621597 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.025945290880356
	Total time: 13.887822168
	Avarage time pr. topology: 0.00001735

Data for the geometric median iteration:
	Total time: 10.571030607
	Total steps: 29737404
	Average number of steps pr. geometric median: 4.644060268735593
	Average time pr. step: 0.0000003554792680289107
	Average time pr. geometric median: 0.000001650867145012275

Data for the geometric median step function:
	Total number of fixed points encountered: 52074242

