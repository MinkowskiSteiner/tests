All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.597554333826411
MST len: 2.6729007226266446
Steiner Ratio: 0.9718110036177509
Solution Steiner tree:
	Terminals:
	  #0: (0.5921190123,0.7871168474,0.8626960571) [ #9 ]
	  #1: (0.5949672445,0.2624237846,0.4864085733) [ #7 ]
	  #2: (0.4022940902,0.433104444,0.3224785879) [ #3 ]
	  #4: (0.1665347033,0.6197118892,0.4428926201) [ #11 ]
	  #6: (0.4051467089,0.068010787,0.7012902786) [ #15 ]
	  #8: (0.3742742359,0.7257872944,0.9398560226) [ #9 ]
	  #10: (0.0504484792,0.7854848689,0.4252954514) [ #11 ]
	  #12: (0.9411502084,0.4651792224,0.1887814688) [ #13 ]
	  #14: (0.2066657628,0.0859796964,0.6465744323) [ #15 ]
	Steiner points:
	  #3: (0.40239404841079407,0.433169677497501,0.32273376160925515) [ #13 #2 #5 ]
	  #5: (0.26493201076475514,0.5886486493201712,0.4732505019964138) [ #9 #11 #3 ]
	  #7: (0.5949670351006219,0.2624238225542169,0.48640851743288865) [ #13 #1 #15 ]
	  #9: (0.4312972767635283,0.7283538270774816,0.8555766027104577) [ #5 #8 #0 ]
	  #11: (0.1665347033,0.6197118892,0.4428926201) [ #10 #5 #4 ]
	  #13: (0.5899321991379622,0.3513769015104608,0.3840212963348122) [ #3 #7 #12 ]
	  #15: (0.3936397950370334,0.08469745157185386,0.6783614109412703) [ #6 #7 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 4.327684742
	Number of best updates: 9

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 83283

	Init times:
		Bottleneck Steiner distances: 0.000021676
		Smallest sphere distances: 0.000001476
		Sorting: 0.000002868
		Total init time: 0.000026994

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000767, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00000349, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000029888, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.00033571, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004571127, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.043671464, bsd pruned: 10395, ss pruned: 52920

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 71820
	Total number iterations 806461 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 11.228919521024784
	Total time: 4.231428128
	Avarage time pr. topology: 0.000058916

Data for the geometric median iteration:
	Total time: 3.886421478
	Total steps: 27534447
	Average number of steps pr. geometric median: 54.768761188686
	Average time pr. step: 0.0000001411476133150595
	Average time pr. geometric median: 0.00000773047992600549

