All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.502058913078967
MST len: 2.6325497917138216
Steiner Ratio: 0.9504317528786783
Solution Steiner tree:
	Terminals:
	  #0: (0.3488313283,0.3430800132,0.2595131855) [ #3 ]
	  #1: (0.2600808285,0.2142811954,0.3601613163) [ #9 ]
	  #2: (0.4198029756,0.654078258,0.1943656375) [ #7 ]
	  #4: (0.3097259371,0.5068725648,0.1146662767) [ #5 ]
	  #6: (0.5706952487,0.6701787769,0.9271462466) [ #11 ]
	  #8: (0.7238869563,0.0950620273,0.2335532183) [ #15 ]
	  #10: (0.158582164,0.7489994023,0.9178017638) [ #11 ]
	  #12: (0.29734262,0.6824838476,0.0102189095) [ #13 ]
	  #14: (0.9164578756,0.1339819669,0.1912108968) [ #15 ]
	Steiner points:
	  #3: (0.3488313283,0.3430800132,0.2595131855) [ #0 #9 #5 ]
	  #5: (0.30974291519383873,0.5068803276480129,0.11468747837659492) [ #3 #4 #13 ]
	  #7: (0.419802975337307,0.6540782578827574,0.1943656376207869) [ #11 #13 #2 ]
	  #9: (0.36063907870640477,0.2784491562531125,0.2873381781630703) [ #3 #1 #15 ]
	  #11: (0.386899225966695,0.6979351766267068,0.802992169684477) [ #10 #7 #6 ]
	  #13: (0.3407130284770329,0.6066852422273376,0.10809919568985776) [ #12 #7 #5 ]
	  #15: (0.723887476385917,0.09506657790799614,0.233552652375739) [ #8 #9 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 3.816561445
	Number of best updates: 25

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 73833

	Init times:
		Bottleneck Steiner distances: 0.000013466
		Smallest sphere distances: 0.000001321
		Sorting: 0.00000312
		Total init time: 0.000018884

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000847, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00000356, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000030105, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000334856, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004423063, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.038317718, bsd pruned: 0, ss pruned: 72765

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 62370
	Total number iterations 738388 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 11.838832772166105
	Total time: 3.7248003450000002
	Avarage time pr. topology: 0.00005972

Data for the geometric median iteration:
	Total time: 3.409192764
	Total steps: 24406742
	Average number of steps pr. geometric median: 55.90311734121258
	Average time pr. step: 0.00000013968241906273275
	Average time pr. geometric median: 0.000007808682663368377

Data for the geometric median step function:
	Total number of precision errors encountered: 0

