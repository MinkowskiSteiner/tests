All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.2958231454746487
MST len: 3.578986748958346
Steiner Ratio: 0.9208816284200797
Solution Steiner tree:
	Terminals:
	  #0: (0.5214306472,0.2908011355,0.0775070079) [ #11 ]
	  #1: (0.6089239803,0.0684510521,0.5965783874) [ #15 ]
	  #2: (0.2599066302,0.4942982874,0.8509792596) [ #7 ]
	  #4: (0.2788681999,0.3929495487,0.2476127913) [ #5 ]
	  #6: (0.6388154154,0.6265101371,0.8390606664) [ #7 ]
	  #8: (0.1431079023,0.7291413614,0.1934557334) [ #9 ]
	  #10: (0.1297003627,0.0635728929,0.1258999995) [ #11 ]
	  #12: (0.7197443544,0.7481080414,0.2152077799) [ #13 ]
	  #14: (0.8830592017,0.0076694484,0.9101628568) [ #15 ]
	Steiner points:
	  #3: (0.5099836453066541,0.4174409309813009,0.6100590195129709) [ #13 #7 #15 ]
	  #5: (0.2788710879209873,0.39295040119367175,0.24761096773590696) [ #11 #9 #4 ]
	  #7: (0.47953327962327336,0.5034688777732431,0.7482693225547579) [ #6 #2 #3 ]
	  #9: (0.31193553197790497,0.503900778818545,0.27334001296901966) [ #8 #5 #13 ]
	  #11: (0.316147171471284,0.29168730642358504,0.17209340829019407) [ #10 #5 #0 ]
	  #13: (0.46644952576745347,0.5344130769095911,0.3616858184876211) [ #3 #9 #12 ]
	  #15: (0.6251054475598952,0.1042164729960917,0.6303288958303902) [ #1 #3 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 6.187768177
	Number of best updates: 18

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 94623

	Init times:
		Bottleneck Steiner distances: 0.000021247
		Smallest sphere distances: 0.00000135
		Sorting: 0.000002843
		Total init time: 0.000026415

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000698, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003349, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000029581, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000331039, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004522851, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.048164161, bsd pruned: 10395, ss pruned: 41580

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 83160
	Total number iterations 1190862 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 14.32012987012987
	Total time: 6.084451203
	Avarage time pr. topology: 0.000073165

Data for the geometric median iteration:
	Total time: 5.581565208
	Total steps: 39486522
	Average number of steps pr. geometric median: 67.83227169655741
	Average time pr. step: 0.00000014135368032666944
	Average time pr. geometric median: 0.000009588341249226964

