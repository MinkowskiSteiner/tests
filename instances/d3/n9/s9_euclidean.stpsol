All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.724580295836471
MST len: 3.9913431056354
Steiner Ratio: 0.9331646509110466
Solution Steiner tree:
	Terminals:
	  #0: (0.0917933952,0.0898954501,0.3307895457) [ #3 ]
	  #1: (0.3588790108,0.884531313,0.4632373589) [ #13 ]
	  #2: (0.7227528005,0.6292115332,0.7207948913) [ #15 ]
	  #4: (0.6750120226,0.2615602367,0.272972847) [ #5 ]
	  #6: (0.206965448,0.6995153961,0.2034307295) [ #7 ]
	  #8: (0.9914577939,0.2373887199,0.3081078475) [ #9 ]
	  #10: (0.6858023101,0.9032684844,0.0947479369) [ #11 ]
	  #12: (0.0740991244,0.9287021542,0.8842824203) [ #13 ]
	  #14: (0.7304651908,0.05407485,0.9231773037) [ #15 ]
	Steiner points:
	  #3: (0.36651827987341273,0.35164938688205616,0.28012082544336925) [ #5 #7 #0 ]
	  #5: (0.6750089510468519,0.26156594273732164,0.2729834720492642) [ #4 #3 #9 ]
	  #7: (0.28379244244998664,0.6773280998406651,0.24259600885044702) [ #6 #3 #11 ]
	  #9: (0.7987096857073609,0.2731530796985294,0.37482671503827575) [ #8 #5 #15 ]
	  #11: (0.39114481996692646,0.8019950356127756,0.2999061020899627) [ #7 #10 #13 ]
	  #13: (0.35887901079970025,0.8845313129997212,0.4632373588999581) [ #12 #11 #1 ]
	  #15: (0.7536248625291121,0.34182983736778205,0.6430460915270907) [ #2 #9 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 7.670409072
	Number of best updates: 22

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 115413

	Init times:
		Bottleneck Steiner distances: 0.000022441
		Smallest sphere distances: 0.000001325
		Sorting: 0.00000331
		Total init time: 0.000028248

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000768, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003316, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000037898, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000328795, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004357196, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.055361489, bsd pruned: 0, ss pruned: 31185

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 103950
	Total number iterations 1388272 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 13.355189995189995
	Total time: 7.549770914
	Avarage time pr. topology: 0.000072628

Data for the geometric median iteration:
	Total time: 6.959123234
	Total steps: 50015976
	Average number of steps pr. geometric median: 68.73631003916718
	Average time pr. step: 0.00000013913800730390625
	Average time pr. geometric median: 0.000009563833208273208

Data for the geometric median step function:
	Total number of precision errors encountered: 112

