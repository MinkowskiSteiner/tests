All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.466723023045067
MST len: 2.527630302080213
Steiner Ratio: 0.9759034068451309
Solution Steiner tree:
	Terminals:
	  #0: (0.3251002693,0.2867596859,0.331827752) [ #5 ]
	  #1: (0.4798882434,0.6288429693,0.6801386246) [ #3 ]
	  #2: (0.3079489355,0.6126976412,0.4672263043) [ #9 ]
	  #4: (0.5413947988,0.3004468834,0.263256482) [ #15 ]
	  #6: (0.2627286242,0.2802385941,0.3090906377) [ #7 ]
	  #8: (0.0063632978,0.7409414774,0.3425149626) [ #11 ]
	  #10: (0.1237438932,0.9474799428,0.1736593936) [ #11 ]
	  #12: (0.2058983465,0.1701670499,0.6832347255) [ #13 ]
	  #14: (0.8872430967,0.7088636578,0.0528921765) [ #15 ]
	Steiner points:
	  #3: (0.3358348305769887,0.5665335337015538,0.5048683904104534) [ #9 #13 #1 ]
	  #5: (0.3251002693,0.2867596859,0.331827752) [ #7 #0 #15 ]
	  #7: (0.30647607368377555,0.29056474043317165,0.3392333782599746) [ #5 #6 #13 ]
	  #9: (0.3079487863029985,0.6126975413493458,0.46722636054245614) [ #11 #3 #2 ]
	  #11: (0.07615229489569324,0.7610097288626704,0.329205406257114) [ #10 #8 #9 ]
	  #13: (0.2918796294474818,0.34198809736689684,0.46526955343339516) [ #3 #7 #12 ]
	  #15: (0.5413904154466582,0.3004550222109637,0.2632556785818079) [ #4 #5 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 5.67439011
	Number of best updates: 24

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 105018

	Init times:
		Bottleneck Steiner distances: 0.00001352
		Smallest sphere distances: 0.000001251
		Sorting: 0.000002886
		Total init time: 0.000018517

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000665, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003251, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000029723, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000332111, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004530638, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.052250938, bsd pruned: 0, ss pruned: 41580

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 93555
	Total number iterations 1086362 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 11.612014323125434
	Total time: 5.564604676
	Avarage time pr. topology: 0.000059479

Data for the geometric median iteration:
	Total time: 5.100407548
	Total steps: 36005650
	Average number of steps pr. geometric median: 54.98011101185704
	Average time pr. step: 0.00000014165575536061702
	Average time pr. geometric median: 0.000007788249155195186

