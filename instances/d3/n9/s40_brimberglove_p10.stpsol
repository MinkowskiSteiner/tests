All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.9332268968689825
MST len: 4.4815117537
Steiner Ratio: 0.8776562715966669
Solution Steiner tree:
	Terminals:
	  #0: (0.3481050377,0.7246325052,0.5291235137) [ #11 ]
	  #1: (0.6257388632,0.607333408,0.320081121) [ #3 ]
	  #2: (0.8194229132,0.5062902763,0.0883140588) [ #5 ]
	  #4: (0.8602793272,0.35034057,0.4464217548) [ #7 ]
	  #6: (0.7855343049,0.3411775247,0.8119810777) [ #7 ]
	  #8: (0.1197466637,0.0576706543,0.2857596349) [ #13 ]
	  #10: (0.6861238385,0.9269996448,0.8449453608) [ #11 ]
	  #12: (0.0208598026,0.0373252058,0.1292244392) [ #13 ]
	  #14: (0.9184383005,0.2771394566,0.1947866791) [ #15 ]
	Steiner points:
	  #3: (0.6861238384999999,0.607333408,0.320081121) [ #5 #1 #11 ]
	  #5: (0.8194229132,0.5062902763,0.2857592753668168) [ #2 #3 #9 ]
	  #7: (0.8194229132,0.3411775247,0.4464217548) [ #4 #6 #9 ]
	  #9: (0.8194229132,0.3411775247,0.2857592753668168) [ #5 #7 #15 ]
	  #11: (0.6861238385,0.7246325052,0.5291235137) [ #10 #0 #3 ]
	  #13: (0.1197466637,0.05767065429999999,0.28575828163423755) [ #8 #12 #15 ]
	  #15: (0.8194229131999999,0.2771394566,0.28575613636525543) [ #9 #13 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 1.539146811
	Number of best updates: 16

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 95568

	Init times:
		Bottleneck Steiner distances: 0.000028183
		Smallest sphere distances: 0.000003489
		Sorting: 0.000012821
		Total init time: 0.000045433

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001253, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00000662, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000061511, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000711748, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.008668101, bsd pruned: 0, ss pruned: 945
		Level 6 - Time: 0.098796634, bsd pruned: 18900, ss pruned: 18900

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 85050
	Total number iterations 172499 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.028206937095826
	Total time: 1.342596248
	Avarage time pr. topology: 0.000015785

Data for the geometric median iteration:
	Total time: 1.028093848
	Total steps: 2887109
	Average number of steps pr. geometric median: 4.849431426891744
	Average time pr. step: 0.0000003560980371714403
	Average time pr. geometric median: 0.0000017268730125136472

