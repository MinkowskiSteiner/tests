All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.9632654096510267
MST len: 4.7086107011
Steiner Ratio: 0.8417059003678411
Solution Steiner tree:
	Terminals:
	  #0: (0.384026217,0.8656925191,0.3785816228) [ #7 ]
	  #1: (0.0916402913,0.5212438672,0.540312229) [ #3 ]
	  #2: (0.1913313205,0.4307993098,0.5479775013) [ #5 ]
	  #4: (0.0156422849,0.215467854,0.2348691641) [ #13 ]
	  #6: (0.9218689981,0.5204600438,0.6477700405) [ #9 ]
	  #8: (0.8943212232,0.6286429756,0.1040961771) [ #9 ]
	  #10: (0.1304018107,0.0569058615,0.9637256646) [ #11 ]
	  #12: (0.0151233026,0.0380932228,0.0248203492) [ #13 ]
	  #14: (0.975851736,0.6714702508,0.5076044628) [ #15 ]
	Steiner points:
	  #3: (0.1913313205,0.5212438672,0.4972727742159071) [ #1 #5 #7 ]
	  #5: (0.1913313205,0.4307993098,0.4972727742159071) [ #11 #2 #3 ]
	  #7: (0.384026217,0.642918734,0.4644922985330662) [ #0 #3 #15 ]
	  #9: (0.8943212232000853,0.6286429756,0.4644922985330662) [ #8 #6 #15 ]
	  #11: (0.1304018107,0.1563429769615391,0.4972727742159071) [ #5 #10 #13 ]
	  #13: (0.0156422849,0.1563429769615391,0.2348691641) [ #12 #11 #4 ]
	  #15: (0.8943212232000853,0.642918734,0.4644922985330662) [ #7 #9 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.943815556
	Number of best updates: 19

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 58293

	Init times:
		Bottleneck Steiner distances: 0.00002902
		Smallest sphere distances: 0.00000373
		Sorting: 0.000012568
		Total init time: 0.000046463

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001242, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000006677, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00006128, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000698644, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.008638352, bsd pruned: 0, ss pruned: 945
		Level 6 - Time: 0.08374263, bsd pruned: 18900, ss pruned: 56175

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 47775
	Total number iterations 96733 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0247619047619048
	Total time: 0.788259326
	Avarage time pr. topology: 0.000016499

Data for the geometric median iteration:
	Total time: 0.611446113
	Total steps: 1725441
	Average number of steps pr. geometric median: 5.159425880242207
	Average time pr. step: 0.00000035437091908677256
	Average time pr. geometric median: 0.0000018283504911415113

