All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.52497434971859
MST len: 2.6321612146993454
Steiner Ratio: 0.9592780015212713
Solution Steiner tree:
	Terminals:
	  #0: (0.8071258347,0.3703712553,0.6420550997) [ #7 ]
	  #1: (0.6899730524,0.429892563,0.1510021729) [ #11 ]
	  #2: (0.5980566296,0.3121092172,0.450471332) [ #3 ]
	  #4: (0.59228968,0.9251535474,0.6100173786) [ #9 ]
	  #6: (0.8194240331,0.0320747178,0.6424573714) [ #13 ]
	  #8: (0.2394123931,0.7611420498,0.1745217923) [ #15 ]
	  #10: (0.9164035199,0.1465295028,0.1651677811) [ #11 ]
	  #12: (0.9574638638,0.1660335614,0.7214625924) [ #13 ]
	  #14: (0.3284930686,0.8837608927,0.2698044499) [ #15 ]
	Steiner points:
	  #3: (0.6411728216012714,0.3521230443470719,0.43444487326856146) [ #11 #2 #5 ]
	  #5: (0.674367064224936,0.4221997441056094,0.5029310398793667) [ #3 #9 #7 ]
	  #7: (0.8071258249764751,0.3703712321164709,0.6420550816212727) [ #13 #0 #5 ]
	  #9: (0.5344906519068894,0.8017704669911482,0.4883259904263437) [ #5 #4 #15 ]
	  #11: (0.7249075029096488,0.3467804186091447,0.23447699117761597) [ #3 #10 #1 ]
	  #13: (0.897269379288119,0.1698977193956229,0.6881912041554291) [ #12 #7 #6 ]
	  #15: (0.3285553709935396,0.8834003392252362,0.2698688701966504) [ #8 #9 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 2.583973364
	Number of best updates: 10

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 50628

	Init times:
		Bottleneck Steiner distances: 0.000021314
		Smallest sphere distances: 0.000001104
		Sorting: 0.000002872
		Total init time: 0.000026231

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000766, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00000341, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000029641, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000331164, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004236485, bsd pruned: 0, ss pruned: 945
		Level 6 - Time: 0.037372104, bsd pruned: 0, ss pruned: 82740

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 40110
	Total number iterations 502185 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 12.520194465220642
	Total time: 2.50391119
	Avarage time pr. topology: 0.000062425

Data for the geometric median iteration:
	Total time: 2.2903825859999998
	Total steps: 16214910
	Average number of steps pr. geometric median: 57.75157602307939
	Average time pr. step: 0.00000014125163728938365
	Average time pr. geometric median: 0.000008157504669302276

