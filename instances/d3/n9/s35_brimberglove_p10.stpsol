All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.7140561268636763
MST len: 4.382476570000001
Steiner Ratio: 0.8474788324683903
Solution Steiner tree:
	Terminals:
	  #0: (0.4311483691,0.7982122655,0.1291394788) [ #9 ]
	  #1: (0.7090024225,0.6096137295,0.470729368) [ #3 ]
	  #2: (0.461066215,0.6987889817,0.7545507828) [ #5 ]
	  #4: (0.1812310276,0.1675375119,0.7022328906) [ #7 ]
	  #6: (0.1086744881,0.7979400231,0.6606961906) [ #13 ]
	  #8: (0.0071734451,0.7727813925,0.162069249) [ #9 ]
	  #10: (0.5849627301,0.033694007,0.4157471649) [ #11 ]
	  #12: (0.0411185827,0.809808453,0.6971266361) [ #13 ]
	  #14: (0.5244897048,0.4379004908,0.0703035561) [ #15 ]
	Steiner points:
	  #3: (0.5189216616416175,0.6096137295,0.470729368) [ #5 #1 #11 ]
	  #5: (0.461066215,0.6096137295,0.6607030645947065) [ #3 #2 #7 ]
	  #7: (0.1812310276,0.6096137295,0.6607030645947065) [ #13 #4 #5 ]
	  #9: (0.4311483691,0.7727813925,0.162069249) [ #0 #8 #15 ]
	  #11: (0.5189216616416175,0.6096137295,0.4157471649000001) [ #10 #3 #15 ]
	  #13: (0.1086744881,0.7979400230999999,0.6607030645947065) [ #12 #7 #6 ]
	  #15: (0.5189216616416173,0.6096137295,0.162069249) [ #9 #11 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 1.415130996
	Number of best updates: 16

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 88953

	Init times:
		Bottleneck Steiner distances: 0.000022096
		Smallest sphere distances: 0.000003855
		Sorting: 0.000021687
		Total init time: 0.000048558

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001275, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000006486, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000062253, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000691935, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.009288335, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.117311056, bsd pruned: 20790, ss pruned: 36855

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 77490
	Total number iterations 156193 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0156536327268033
	Total time: 1.20193892
	Avarage time pr. topology: 0.000015509

Data for the geometric median iteration:
	Total time: 0.916720148
	Total steps: 2576081
	Average number of steps pr. geometric median: 4.749149198974983
	Average time pr. step: 0.0000003558584330228747
	Average time pr. geometric median: 0.000001690024792139078

