All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.8462970045655482
MST len: 2.9329627173737607
Steiner Ratio: 0.9704511372426122
Solution Steiner tree:
	Terminals:
	  #0: (0.9775588964,0.7308916956,0.3246047955) [ #5 ]
	  #1: (0.3443029827,0.6699542467,0.4050064401) [ #7 ]
	  #2: (0.3549731771,0.6731490375,0.5153574038) [ #3 ]
	  #4: (0.8388915681,0.8634974965,0.5150421176) [ #5 ]
	  #6: (0.3857721921,0.3929051535,0.0476035024) [ #9 ]
	  #8: (0.2348172144,0.1034733854,0.3869860398) [ #11 ]
	  #10: (0.3598284723,0.0994729628,0.0463795699) [ #11 ]
	  #12: (0.9928987739,0.9662594152,0.9828661391) [ #13 ]
	  #14: (0.2768868838,0.9831143995,0.7832298362) [ #15 ]
	Steiner points:
	  #3: (0.3550000142486293,0.6732092265319379,0.5153188364274213) [ #7 #2 #15 ]
	  #5: (0.8388916297916204,0.8634957597269854,0.5150427802674085) [ #0 #13 #4 ]
	  #7: (0.34430299211649174,0.6699542099841158,0.4050064549780683) [ #3 #9 #1 ]
	  #9: (0.3742132998748162,0.3794326157257866,0.09091672197071698) [ #6 #7 #11 ]
	  #11: (0.33804322603078973,0.18234126120907856,0.13027114386871821) [ #10 #8 #9 ]
	  #13: (0.7798733103188243,0.8718858925455757,0.6177511276725626) [ #12 #5 #15 ]
	  #15: (0.43771118322832164,0.8209830960570355,0.6255596267480326) [ #3 #13 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 4.060097398
	Number of best updates: 22

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 95568

	Init times:
		Bottleneck Steiner distances: 0.000021292
		Smallest sphere distances: 0.000001216
		Sorting: 0.000003387
		Total init time: 0.000026931

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000092, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003467, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000029636, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000334707, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004089082, bsd pruned: 0, ss pruned: 945
		Level 6 - Time: 0.04674716, bsd pruned: 28350, ss pruned: 9450

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 85050
	Total number iterations 837638 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 9.848771310993532
	Total time: 3.9561083889999997
	Avarage time pr. topology: 0.000046514

Data for the geometric median iteration:
	Total time: 3.593709768
	Total steps: 25655060
	Average number of steps pr. geometric median: 43.09239942890736
	Average time pr. step: 0.00000014007801065364884
	Average time pr. geometric median: 0.000006036297586293777

Data for the geometric median step function:
	Total number of precision errors encountered: 110

