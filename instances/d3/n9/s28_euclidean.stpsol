All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.7245983824566142
MST len: 2.9358318240787966
Steiner Ratio: 0.9280498835492857
Solution Steiner tree:
	Terminals:
	  #0: (0.442212973,0.1277577337,0.1569511728) [ #3 ]
	  #1: (0.517155105,0.0449436945,0.4524295369) [ #3 ]
	  #2: (0.3412099454,0.230164813,0.4956663146) [ #5 ]
	  #4: (0.7013661609,0.408563846,0.7201248397) [ #9 ]
	  #6: (0.3593030024,0.8427590313,0.2819298144) [ #15 ]
	  #8: (0.9602391431,0.1126118736,0.8147720088) [ #13 ]
	  #10: (0.860883003,0.4750771162,0.9620311726) [ #11 ]
	  #12: (0.8276713341,0.0367873907,0.8458900362) [ #13 ]
	  #14: (0.0837527169,0.7310733133,0.0224596341) [ #15 ]
	Steiner points:
	  #3: (0.45604232204634304,0.12854277386179455,0.396818381696273) [ #0 #1 #5 ]
	  #5: (0.3848331839498985,0.23819196096694584,0.4777300862405246) [ #2 #7 #3 ]
	  #7: (0.4591579500385234,0.38425868260232837,0.5030328900215886) [ #5 #9 #15 ]
	  #9: (0.7013661609,0.408563846,0.7201248397) [ #7 #11 #4 ]
	  #11: (0.806658070145606,0.3599404920451038,0.8321037563984007) [ #9 #10 #13 ]
	  #13: (0.8965148983656799,0.12334156743161005,0.8270953813316412) [ #8 #11 #12 ]
	  #15: (0.3218982886087862,0.7604098829643081,0.2638189836420822) [ #6 #7 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 2.800348616
	Number of best updates: 13

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 46428

	Init times:
		Bottleneck Steiner distances: 0.000012252
		Smallest sphere distances: 0.000001266
		Sorting: 0.000002965
		Total init time: 0.000017439

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000697, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003233, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000029451, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000333761, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.004564667, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.037602953, bsd pruned: 20790, ss pruned: 79380

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 34965
	Total number iterations 543926 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 15.556299156299156
	Total time: 2.71829522
	Avarage time pr. topology: 0.000077743

Data for the geometric median iteration:
	Total time: 2.489185587
	Total steps: 17601201
	Average number of steps pr. geometric median: 71.91355028497885
	Average time pr. step: 0.00000014142134886136464
	Average time pr. geometric median: 0.000010170111282711283

