All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 4.358322073483386
MST len: 5.077848577899999
Steiner Ratio: 0.8583009135900265
Solution Steiner tree:
	Terminals:
	  #0: (0.3779684163,0.1168798023,0.6027370345) [ #9 ]
	  #1: (0.1637372478,0.1157799243,0.8294556531) [ #5 ]
	  #2: (0.6326431831,0.3414155828,0.904109925) [ #11 ]
	  #4: (0.1300345134,0.6083626135,0.3345068657) [ #15 ]
	  #6: (0.8654058324,0.0406219666,0.7204131906) [ #7 ]
	  #8: (0.4365024196,0.0231408114,0.3533674392) [ #13 ]
	  #10: (0.9116950463,0.792852993,0.8622477059) [ #11 ]
	  #12: (0.5560647936,0.0749040051,0.0531665883) [ #13 ]
	  #14: (0.1558290944,0.7948272283,0.928200935) [ #15 ]
	Steiner points:
	  #3: (0.4365024196000001,0.34142237793669056,0.8294556531) [ #5 #7 #11 ]
	  #5: (0.1637372478,0.34142237793669056,0.8294556531) [ #3 #1 #15 ]
	  #7: (0.4365024196000001,0.11687980230000002,0.7204131906) [ #6 #9 #3 ]
	  #9: (0.4365024196000001,0.1168798023,0.6027370345) [ #13 #7 #0 ]
	  #11: (0.6326431830999999,0.34142237793669056,0.8622477059) [ #3 #10 #2 ]
	  #13: (0.43650264764669566,0.0749040051,0.35336743919999997) [ #8 #9 #12 ]
	  #15: (0.1558290944,0.6083626134999999,0.8294556531000001) [ #4 #5 #14 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 1.724523891
	Number of best updates: 18

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 105018

	Init times:
		Bottleneck Steiner distances: 0.000026222
		Smallest sphere distances: 0.000004152
		Sorting: 0.000015126
		Total init time: 0.000046978

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001646, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00000691, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000063412, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000708693, bsd pruned: 0, ss pruned: 0
		Level 5 - Time: 0.00947472, bsd pruned: 0, ss pruned: 0
		Level 6 - Time: 0.109593611, bsd pruned: 31185, ss pruned: 10395

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 93555
	Total number iterations 190650 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.037838704505371
	Total time: 1.500211514
	Avarage time pr. topology: 0.000016034

Data for the geometric median iteration:
	Total time: 1.157576462
	Total steps: 3271414
	Average number of steps pr. geometric median: 4.995402246195897
	Average time pr. step: 0.00000035384590944466217
	Average time pr. geometric median: 0.0000017676026508470953

Data for the geometric median step function:
	Total number of fixed points encountered: 5502562

