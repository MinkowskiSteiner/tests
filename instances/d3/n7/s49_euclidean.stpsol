All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.1945949477545965
MST len: 2.341375825409903
Steiner Ratio: 0.9373099883998292
Solution Steiner tree:
	Terminals:
	  #0: (0.6268771643,0.9956121147,0.5463564538) [ #5 ]
	  #1: (0.6634417692,0.9268148322,0.4391896513) [ #3 ]
	  #2: (0.4174514107,0.3731905494,0.4855085041) [ #11 ]
	  #4: (0.6174133409,0.9290067493,0.8974420614) [ #7 ]
	  #6: (0.9780446412,0.8501515877,0.8986921589) [ #7 ]
	  #8: (0.1629512739,0.7427612197,0.2952385462) [ #9 ]
	  #10: (0.0418115836,0.220037731,0.3087208249) [ #11 ]
	Steiner points:
	  #3: (0.631730561849888,0.9282613711289923,0.46063383762816956) [ #5 #9 #1 ]
	  #5: (0.6269004963373191,0.995537943405146,0.5463704466806711) [ #7 #3 #0 ]
	  #7: (0.6923256331866794,0.9270699316637229,0.8237844304988389) [ #6 #4 #5 ]
	  #9: (0.2946367993621011,0.6781209000635269,0.36334188601932416) [ #8 #3 #11 ]
	  #11: (0.3034042355285218,0.42928117746706584,0.4132337939651836) [ #2 #9 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.034344177
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 648

	Init times:
		Bottleneck Steiner distances: 0.000020483
		Smallest sphere distances: 0.000000968
		Sorting: 0.000001558
		Total init time: 0.000023844

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000694, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003094, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000027756, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.00022561, bsd pruned: 105, ss pruned: 315

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 525
	Total number iterations 7485 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 14.257142857142858
	Total time: 0.033726376
	Avarage time pr. topology: 0.00006424

Data for the geometric median iteration:
	Total time: 0.031396028
	Total steps: 223000
	Average number of steps pr. geometric median: 84.95238095238095
	Average time pr. step: 0.00000014078936322869956
	Average time pr. geometric median: 0.00001196039161904762

