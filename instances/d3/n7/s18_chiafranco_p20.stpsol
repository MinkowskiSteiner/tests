All distances are in Lp space with p=20 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Rodríguez-chía and Valero-Franco's iteration with the hyberbolic approximation where epsilon=0.00001 stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.859483142396918
MST len: 2.1969074281193732
Steiner Ratio: 0.8464094201678303
Solution Steiner tree:
	Terminals:
	  #0: (0.4872422654,0.1321992586,0.3741906497) [ #7 ]
	  #1: (0.6797411119,0.8863195306,0.801372399) [ #5 ]
	  #2: (0.9167352235,0.3484674186,0.7351341866) [ #9 ]
	  #4: (0.9290727782,0.9129959489,0.9238626137) [ #5 ]
	  #6: (0.2213446718,0.2145956132,0.2002614784) [ #7 ]
	  #8: (0.9674219824,0.2784534135,0.4652112594) [ #9 ]
	  #10: (0.2359872904,0.785772445,0.946517847) [ #11 ]
	Steiner points:
	  #3: (0.6557111208744928,0.6008997845344614,0.6850427107214649) [ #9 #5 #11 ]
	  #5: (0.7908925439086656,0.7755404952393848,0.8164914630805338) [ #1 #4 #3 ]
	  #7: (0.37200480917597717,0.24743439525775865,0.35089690643720445) [ #0 #6 #11 ]
	  #9: (0.814429057823983,0.44270535040337233,0.6316607515082796) [ #8 #3 #2 ]
	  #11: (0.56728327818003,0.5125598813655057,0.6156934205948934) [ #3 #7 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 1413.397368008
	Number of best updates: 7

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 858

	Init times:
		Bottleneck Steiner distances: 0.000045305
		Smallest sphere distances: 0.000012799
		Sorting: 0.000042975
		Total init time: 0.000102213

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000505, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000034465, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000321686, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.00298451, bsd pruned: 0, ss pruned: 210

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 735
	Total number iterations 80431 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 109.42993197278912
	Total time: 1413.390865045
	Avarage time pr. topology: 1.922980768

Data for the geometric median iteration:
	Total time: 1413.12285215
	Total steps: 132398868
	Average number of steps pr. geometric median: 36026.90285714286
	Average time pr. step: 0.00001067322457885365
	Average time pr. geometric median: 0.38452322507482994

Data for the geometric median step function:
	Total number of precision errors encountered: 0

