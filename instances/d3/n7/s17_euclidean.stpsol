All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.1164155836653054
MST len: 2.2707670878776587
Steiner Ratio: 0.9320267124548578
Solution Steiner tree:
	Terminals:
	  #0: (0.9043156765,0.6258278161,0.215128048) [ #3 ]
	  #1: (0.726414976,0.6913943327,0.1680129702) [ #9 ]
	  #2: (0.7641613385,0.5371329414,0.440277009) [ #5 ]
	  #4: (0.2804932088,0.3381360212,0.8132569412) [ #7 ]
	  #6: (0.1078885724,0.4860870128,0.6620165676) [ #7 ]
	  #8: (0.9171544332,0.8540148287,0.22754956) [ #9 ]
	  #10: (0.5717940003,0.0018524737,0.1227083798) [ #11 ]
	Steiner points:
	  #3: (0.8587769961540531,0.6471630744016426,0.239980785710747) [ #9 #0 #5 ]
	  #5: (0.7641609410771748,0.5371326850286955,0.44027610454052) [ #3 #2 #11 ]
	  #7: (0.2700167041239957,0.38845376782998925,0.707661513917646) [ #6 #4 #11 ]
	  #9: (0.8325222532590008,0.6975965526157941,0.21757597945522628) [ #8 #3 #1 ]
	  #11: (0.5692338276115769,0.3590939871758846,0.44030389964210104) [ #5 #7 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.034161988
	Number of best updates: 14

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 963

	Init times:
		Bottleneck Steiner distances: 0.000009302
		Smallest sphere distances: 0.000001088
		Sorting: 0.000001562
		Total init time: 0.000013366

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000996, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003319, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000029187, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000292099, bsd pruned: 0, ss pruned: 105

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 840
	Total number iterations 7065 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 8.410714285714286
	Total time: 0.033368638
	Avarage time pr. topology: 0.000039724

Data for the geometric median iteration:
	Total time: 0.031014616
	Total steps: 220713
	Average number of steps pr. geometric median: 52.550714285714285
	Average time pr. step: 0.0000001405201143566532
	Average time pr. geometric median: 0.000007384432380952381

Data for the geometric median step function:
	Total number of precision errors encountered: 0

