All distances are in Lp space with p=20 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Rodríguez-chía and Valero-Franco's iteration with the hyberbolic approximation where epsilon=0.00001 stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.1208958684038626
MST len: 2.692139351396294
Steiner Ratio: 0.7878105816862145
Solution Steiner tree:
	Terminals:
	  #0: (0.1300345134,0.6083626135,0.3345068657) [ #5 ]
	  #1: (0.1637372478,0.1157799243,0.8294556531) [ #3 ]
	  #2: (0.4365024196,0.0231408114,0.3533674392) [ #9 ]
	  #4: (0.6326431831,0.3414155828,0.904109925) [ #7 ]
	  #6: (0.9116950463,0.792852993,0.8622477059) [ #7 ]
	  #8: (0.5560647936,0.0749040051,0.0531665883) [ #9 ]
	  #10: (0.1558290944,0.7948272283,0.928200935) [ #11 ]
	Steiner points:
	  #3: (0.32167260989359864,0.31340565038438084,0.6283330023862375) [ #1 #9 #5 ]
	  #5: (0.3512838183307167,0.3525635691331646,0.5941901114414958) [ #3 #0 #11 ]
	  #7: (0.5524426710731277,0.43336141057239136,0.8125572275742483) [ #4 #6 #11 ]
	  #9: (0.43803063909255097,0.031194093282429513,0.345349562603273) [ #8 #3 #2 ]
	  #11: (0.4655083121794047,0.4849499464425795,0.7261434300448977) [ #5 #7 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 1453.394885547
	Number of best updates: 7

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 963

	Init times:
		Bottleneck Steiner distances: 0.000045705
		Smallest sphere distances: 0.000012906
		Sorting: 0.000043414
		Total init time: 0.000103068

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000004933, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000034987, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000323346, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.003351175, bsd pruned: 0, ss pruned: 105

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 840
	Total number iterations 71919 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 85.61785714285715
	Total time: 1453.387493211
	Avarage time pr. topology: 1.7302232050000002

Data for the geometric median iteration:
	Total time: 1453.147300249
	Total steps: 136234905
	Average number of steps pr. geometric median: 32436.882142857143
	Average time pr. step: 0.000010666483015120098
	Average time pr. geometric median: 0.3459874524402381

Data for the geometric median step function:
	Total number of precision errors encountered: 0

