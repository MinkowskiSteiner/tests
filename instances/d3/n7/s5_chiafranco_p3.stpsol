All distances are in Lp space with p=3 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Rodríguez-chía and Valero-Franco's iteration with the hyberbolic approximation where epsilon=0.00001 stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.9245643262126566
MST len: 1.9852683266682885
Steiner Ratio: 0.9694227729117573
Solution Steiner tree:
	Terminals:
	  #0: (0.2561370005,0.3634193076,0.2718471113) [ #9 ]
	  #1: (0.2805664764,0.4484554052,0.4982478328) [ #11 ]
	  #2: (0.4159667661,0.2562429226,0.3028504617) [ #5 ]
	  #4: (0.8940018629,0.0156746367,0.2746502162) [ #7 ]
	  #6: (0.9714187407,0.2212781451,0.4164950971) [ #7 ]
	  #8: (0.080030445,0.1476887968,0.0796446726) [ #9 ]
	  #10: (0.2747455962,0.0464677648,0.9927552245) [ #11 ]
	Steiner points:
	  #3: (0.3314007947145889,0.33160729468367073,0.3473260433092326) [ #5 #9 #11 ]
	  #5: (0.41803171683217405,0.2587404014509941,0.3043269629242989) [ #3 #2 #7 ]
	  #7: (0.8388637187798601,0.09006622665828946,0.3219262939133538) [ #4 #5 #6 ]
	  #9: (0.25867243714481886,0.3582544223417577,0.2738211367542378) [ #8 #3 #0 ]
	  #11: (0.2828227460307436,0.43906361589105347,0.4971912419871622) [ #1 #3 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 128.839662118
	Number of best updates: 7

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 753

	Init times:
		Bottleneck Steiner distances: 0.000035129
		Smallest sphere distances: 0.000030731
		Sorting: 0.000057178
		Total init time: 0.000124043

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000004772, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00003345, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000311744, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.002580152, bsd pruned: 0, ss pruned: 315

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 630
	Total number iterations 45774 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 72.65714285714286
	Total time: 128.834226047
	Avarage time pr. topology: 0.204498771

Data for the geometric median iteration:
	Total time: 128.688099451
	Total steps: 13986404
	Average number of steps pr. geometric median: 4440.128253968254
	Average time pr. step: 0.000009200942533263018
	Average time pr. geometric median: 0.04085336490507937

Data for the geometric median step function:
	Total number of precision errors encountered: 0

