All distances are in Lp space with p=20 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Rodríguez-chía and Valero-Franco's iteration with the hyberbolic approximation where epsilon=0.00001 stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.1991277581443627
MST len: 2.599641267391891
Steiner Ratio: 0.8459350856322779
Solution Steiner tree:
	Terminals:
	  #0: (0.3588790108,0.884531313,0.4632373589) [ #3 ]
	  #1: (0.206965448,0.6995153961,0.2034307295) [ #7 ]
	  #2: (0.7227528005,0.6292115332,0.7207948913) [ #5 ]
	  #4: (0.0917933952,0.0898954501,0.3307895457) [ #11 ]
	  #6: (0.6858023101,0.9032684844,0.0947479369) [ #7 ]
	  #8: (0.0740991244,0.9287021542,0.8842824203) [ #9 ]
	  #10: (0.7304651908,0.05407485,0.9231773037) [ #11 ]
	Steiner points:
	  #3: (0.36245729365050006,0.8748508616737943,0.45360061452409384) [ #9 #7 #0 ]
	  #5: (0.5508760927341617,0.48689574118753426,0.6720268945187022) [ #9 #2 #11 ]
	  #7: (0.39184748923023305,0.8298732559212048,0.38762185766403656) [ #6 #3 #1 ]
	  #9: (0.33398680604472336,0.7049254748591063,0.6236094848157316) [ #3 #5 #8 ]
	  #11: (0.42733591185789854,0.36259646918844757,0.6355947252947383) [ #4 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 1828.699621753
	Number of best updates: 15

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000043072
		Smallest sphere distances: 0.000012968
		Sorting: 0.000043381
		Total init time: 0.000100599

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000004962, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000034439, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000322074, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.003695324, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 98370 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 104.0952380952381
	Total time: 1828.691344208
	Avarage time pr. topology: 1.9351231150000001

Data for the geometric median iteration:
	Total time: 1828.361354733
	Total steps: 171406173
	Average number of steps pr. geometric median: 36276.43873015873
	Average time pr. step: 0.000010666834937928402
	Average time pr. geometric median: 0.3869547840704762

Data for the geometric median step function:
	Total number of precision errors encountered: 0

