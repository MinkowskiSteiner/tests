All distances are in Lp space with p=3 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Rodríguez-chía and Valero-Franco's iteration with the hyberbolic approximation where epsilon=0.00001 stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.1781295453106266
MST len: 2.3112641174468833
Steiner Ratio: 0.9423975083023732
Solution Steiner tree:
	Terminals:
	  #0: (0.9674219824,0.2784534135,0.4652112594) [ #5 ]
	  #1: (0.6797411119,0.8863195306,0.801372399) [ #9 ]
	  #2: (0.9167352235,0.3484674186,0.7351341866) [ #3 ]
	  #4: (0.4872422654,0.1321992586,0.3741906497) [ #7 ]
	  #6: (0.2213446718,0.2145956132,0.2002614784) [ #7 ]
	  #8: (0.2359872904,0.785772445,0.946517847) [ #9 ]
	  #10: (0.9290727782,0.9129959489,0.9238626137) [ #11 ]
	Steiner points:
	  #3: (0.8837003790576458,0.4046260669548756,0.6753452947408901) [ #5 #2 #11 ]
	  #5: (0.8427254296310442,0.3340448291131884,0.5750892996140172) [ #0 #3 #7 ]
	  #7: (0.4870673522266908,0.13434119151667093,0.3734785720610901) [ #6 #4 #5 ]
	  #9: (0.6774560194969916,0.8822773789649462,0.8032862595660254) [ #1 #8 #11 ]
	  #11: (0.78560773001052,0.7951233996420368,0.8383450903533274) [ #3 #9 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 509.084704123
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 648

	Init times:
		Bottleneck Steiner distances: 0.00003622
		Smallest sphere distances: 0.000030919
		Sorting: 0.000057757
		Total init time: 0.00012583

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000005037, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000033679, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000327496, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.002328423, bsd pruned: 0, ss pruned: 420

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 525
	Total number iterations 53189 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 101.31238095238095
	Total time: 509.079695598
	Avarage time pr. topology: 0.96967561

Data for the geometric median iteration:
	Total time: 508.90816584
	Total steps: 55337608
	Average number of steps pr. geometric median: 21080.993523809524
	Average time pr. step: 0.000009196425075691742
	Average time pr. geometric median: 0.19386977746285713

Data for the geometric median step function:
	Total number of precision errors encountered: 0

