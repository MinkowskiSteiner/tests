All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.348150472339868
MST len: 2.4594712758387067
Steiner Ratio: 0.9547379127406653
Solution Steiner tree:
	Terminals:
	  #0: (0.442212973,0.1277577337,0.1569511728) [ #11 ]
	  #1: (0.517155105,0.0449436945,0.4524295369) [ #3 ]
	  #2: (0.3412099454,0.230164813,0.4956663146) [ #5 ]
	  #4: (0.7013661609,0.408563846,0.7201248397) [ #7 ]
	  #6: (0.860883003,0.4750771162,0.9620311726) [ #9 ]
	  #8: (0.8276713341,0.0367873907,0.8458900362) [ #9 ]
	  #10: (0.0837527169,0.7310733133,0.0224596341) [ #11 ]
	Steiner points:
	  #3: (0.45518049274698086,0.14840226972172632,0.41686061207703723) [ #1 #5 #11 ]
	  #5: (0.4248979080424308,0.21416889552200663,0.4855437801628655) [ #2 #7 #3 ]
	  #7: (0.7013661609,0.408563846,0.7201248397) [ #4 #5 #9 ]
	  #9: (0.7804372654618509,0.3542803625794357,0.8265139880151214) [ #6 #7 #8 ]
	  #11: (0.4164412180929102,0.18215316066736623,0.21345794651632025) [ #0 #3 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.03294054
	Number of best updates: 10

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 858

	Init times:
		Bottleneck Steiner distances: 0.000010384
		Smallest sphere distances: 0.000001109
		Sorting: 0.000002363
		Total init time: 0.000014986

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001071, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003107, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000028505, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000263264, bsd pruned: 0, ss pruned: 210

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 735
	Total number iterations 6477 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 8.812244897959184
	Total time: 0.032246659
	Avarage time pr. topology: 0.000043873

Data for the geometric median iteration:
	Total time: 0.030102715
	Total steps: 211580
	Average number of steps pr. geometric median: 57.57278911564626
	Average time pr. step: 0.00000014227580584176197
	Average time pr. geometric median: 0.000008191214965986394

