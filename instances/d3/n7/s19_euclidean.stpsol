All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.1925010216380847
MST len: 2.261260242830238
Steiner Ratio: 0.9695925219531154
Solution Steiner tree:
	Terminals:
	  #0: (0.586565987,0.9423629041,0.7965240673) [ #7 ]
	  #1: (0.3323929949,0.5761897869,0.273373468) [ #5 ]
	  #2: (0.617568895,0.5169671907,0.3123343211) [ #3 ]
	  #4: (0.2902333854,0.3277869785,0.2285638117) [ #11 ]
	  #6: (0.6874572605,0.9015435013,0.9686559904) [ #7 ]
	  #8: (0.0765914861,0.6450726547,0.0391567499) [ #9 ]
	  #10: (0.0594177293,0.2219586108,0.6696780285) [ #11 ]
	Steiner points:
	  #3: (0.5119641460917765,0.5916874056013175,0.35944943861897716) [ #7 #5 #2 ]
	  #5: (0.33239330735698275,0.5761890371442294,0.2733734626488588) [ #9 #3 #1 ]
	  #7: (0.5865661810180506,0.9423624659465988,0.7965241057676805) [ #3 #6 #0 ]
	  #9: (0.27181372234128554,0.5252806711157387,0.23491882063329686) [ #8 #5 #11 ]
	  #11: (0.2653981251645156,0.36973869601457887,0.2685553076819069) [ #4 #9 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.027428432
	Number of best updates: 8

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 858

	Init times:
		Bottleneck Steiner distances: 0.000020082
		Smallest sphere distances: 0.000000752
		Sorting: 0.000001596
		Total init time: 0.000023383

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000787, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003142, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000029295, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000264942, bsd pruned: 0, ss pruned: 210

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 735
	Total number iterations 5621 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 7.647619047619048
	Total time: 0.026670197
	Avarage time pr. topology: 0.000036285

Data for the geometric median iteration:
	Total time: 0.024812219
	Total steps: 176422
	Average number of steps pr. geometric median: 48.00598639455782
	Average time pr. step: 0.00000014064129757059778
	Average time pr. geometric median: 0.000006751624217687075

Data for the geometric median step function:
	Total number of precision errors encountered: 0

