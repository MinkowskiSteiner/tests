All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.1669958865999996
MST len: 4.0396020513
Steiner Ratio: 0.7839870973381688
Solution Steiner tree:
	Terminals:
	  #0: (0.1214791905,0.3483067552,0.4219620016) [ #5 ]
	  #1: (0.6998054984,0.0663843365,0.5874823525) [ #3 ]
	  #2: (0.8425220697,0.2753141943,0.5966266629) [ #3 ]
	  #4: (0.3821752264,0.3227100961,0.9883655114) [ #7 ]
	  #6: (0.2713367107,0.069655987,0.949639379) [ #7 ]
	  #8: (0.6429663057,0.9906032789,0.295718284) [ #11 ]
	  #10: (0.7009763693,0.8096763491,0.0887954552) [ #11 ]
	Steiner points:
	  #3: (0.6998054984,0.2753141943,0.5874823525) [ #2 #1 #9 ]
	  #5: (0.3821752264,0.3483067552,0.4219620016) [ #9 #0 #7 ]
	  #7: (0.3821752264,0.3227100961,0.949639379) [ #4 #6 #5 ]
	  #9: (0.6623029935666648,0.3483067552,0.42196200159999997) [ #3 #5 #11 ]
	  #11: (0.6623029935666648,0.8096763491,0.295718284) [ #8 #9 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.006673462
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 543

	Init times:
		Bottleneck Steiner distances: 0.000023428
		Smallest sphere distances: 0.000002136
		Sorting: 0.000005905
		Total init time: 0.000032452

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001434, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000006961, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000064163, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000394435, bsd pruned: 0, ss pruned: 525

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 420
	Total number iterations 852 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0285714285714285
	Total time: 0.00564083
	Avarage time pr. topology: 0.00001343

Data for the geometric median iteration:
	Total time: 0.004503242
	Total steps: 12491
	Average number of steps pr. geometric median: 5.948095238095238
	Average time pr. step: 0.0000003605189336322152
	Average time pr. geometric median: 0.0000021444009523809525

Data for the geometric median step function:
	Total number of fixed points encountered: 17612

