All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.488057874950442
MST len: 2.5983376635238207
Steiner Ratio: 0.9575575606967806
Solution Steiner tree:
	Terminals:
	  #0: (0.8759893737,0.4619202984,0.7565934233) [ #11 ]
	  #1: (0.4450082292,0.5312397017,0.8309675659) [ #3 ]
	  #2: (0.2626699606,0.5662877856,0.3799464867) [ #9 ]
	  #4: (0.7873155781,0.22562532,0.6172168532) [ #5 ]
	  #6: (0.5542016586,0.1448612135,0.5434454384) [ #7 ]
	  #8: (0.1027180125,0.9668541835,0.2171490557) [ #9 ]
	  #10: (0.9260082347,0.7454492528,0.1616497483) [ #11 ]
	Steiner points:
	  #3: (0.45300292386484436,0.4406913961198316,0.6285632704098636) [ #1 #7 #9 ]
	  #5: (0.7637310581611938,0.26072957343092606,0.6139215761274164) [ #7 #4 #11 ]
	  #7: (0.6005256612490466,0.24832374015908154,0.5853940170022321) [ #6 #3 #5 ]
	  #9: (0.2626710526851177,0.566290281986236,0.3799484162846014) [ #2 #3 #8 ]
	  #11: (0.8374867127626688,0.41570999588838853,0.6302392224667168) [ #0 #5 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.042052606
	Number of best updates: 14

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 1068

	Init times:
		Bottleneck Steiner distances: 0.000007873
		Smallest sphere distances: 0.000000819
		Sorting: 0.000001584
		Total init time: 0.000011183

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000736, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003231, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00002875, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000320947, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 945
	Total number iterations 8110 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 8.582010582010582
	Total time: 0.041258203
	Avarage time pr. topology: 0.000043659

Data for the geometric median iteration:
	Total time: 0.038658551
	Total steps: 272794
	Average number of steps pr. geometric median: 57.734179894179896
	Average time pr. step: 0.0000001417133478009047
	Average time pr. geometric median: 0.000008181703915343915

