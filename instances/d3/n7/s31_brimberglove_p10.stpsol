All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.110474566013282
MST len: 3.5488023982000003
Steiner Ratio: 0.8764857033434592
Solution Steiner tree:
	Terminals:
	  #0: (0.1665347033,0.6197118892,0.4428926201) [ #3 ]
	  #1: (0.4022940902,0.433104444,0.3224785879) [ #3 ]
	  #2: (0.5949672445,0.2624237846,0.4864085733) [ #5 ]
	  #4: (0.4051467089,0.068010787,0.7012902786) [ #9 ]
	  #6: (0.9411502084,0.4651792224,0.1887814688) [ #7 ]
	  #8: (0.2066657628,0.0859796964,0.6465744323) [ #9 ]
	  #10: (0.5921190123,0.7871168474,0.8626960571) [ #11 ]
	Steiner points:
	  #3: (0.4022940902,0.43310444403989334,0.4428926201) [ #1 #0 #7 ]
	  #5: (0.5926779817531749,0.2624237846,0.4864085733) [ #2 #9 #11 ]
	  #7: (0.592677981753175,0.43310444403989334,0.4428926201000001) [ #6 #3 #11 ]
	  #9: (0.4051467089,0.0859796964,0.6465744323) [ #8 #5 #4 ]
	  #11: (0.592677981753175,0.43310444403989334,0.48640857330000004) [ #5 #7 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.012579853
	Number of best updates: 20

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 963

	Init times:
		Bottleneck Steiner distances: 0.000013755
		Smallest sphere distances: 0.000002817
		Sorting: 0.000005582
		Total init time: 0.000023538

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001751, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000006641, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000060794, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000625128, bsd pruned: 0, ss pruned: 105

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 840
	Total number iterations 1687 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0083333333333333
	Total time: 0.011139239
	Avarage time pr. topology: 0.00001326

Data for the geometric median iteration:
	Total time: 0.008872829
	Total steps: 25123
	Average number of steps pr. geometric median: 5.9816666666666665
	Average time pr. step: 0.0000003531755363611034
	Average time pr. geometric median: 0.0000021125783333333333

