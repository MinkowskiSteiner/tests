All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.972743991698035
MST len: 2.1147119197369677
Steiner Ratio: 0.9328665400171429
Solution Steiner tree:
	Terminals:
	  #0: (0.5789050197,0.7321704266,0.2410501816) [ #3 ]
	  #1: (0.9263453185,0.5264996991,0.3097808907) [ #5 ]
	  #2: (0.5022105041,0.7537675108,0.4059770589) [ #7 ]
	  #4: (0.3817098766,0.0352495974,0.3687719635) [ #9 ]
	  #6: (0.5168204054,0.9570931466,0.3607239967) [ #7 ]
	  #8: (0.4041060449,0.0752406661,0.2481696737) [ #9 ]
	  #10: (0.4484664097,0.2208949445,0.8329159067) [ #11 ]
	Steiner points:
	  #3: (0.5736810030913924,0.698612405935511,0.317482287174496) [ #0 #5 #7 ]
	  #5: (0.668546241129643,0.5404518511886404,0.35735498835893775) [ #3 #1 #11 ]
	  #7: (0.5022105041000512,0.7537675108000433,0.4059770588999262) [ #6 #2 #3 ]
	  #9: (0.3970210315944294,0.06449157337477585,0.3546577854349368) [ #4 #8 #11 ]
	  #11: (0.4876985672347842,0.24417457648807603,0.5051292392296012) [ #5 #9 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.030203634
	Number of best updates: 12

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 858

	Init times:
		Bottleneck Steiner distances: 0.000007969
		Smallest sphere distances: 0.000000804
		Sorting: 0.00000178
		Total init time: 0.00001147

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000786, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003208, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000028838, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000261554, bsd pruned: 0, ss pruned: 210

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 735
	Total number iterations 6239 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 8.48843537414966
	Total time: 0.029461532
	Avarage time pr. topology: 0.000040083

Data for the geometric median iteration:
	Total time: 0.027426827
	Total steps: 196332
	Average number of steps pr. geometric median: 53.42367346938776
	Average time pr. step: 0.0000001396961626224966
	Average time pr. geometric median: 0.000007463082176870749

Data for the geometric median step function:
	Total number of precision errors encountered: 0

