All distances are in Lp space with p=3 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Rodríguez-chía and Valero-Franco's iteration with the hyberbolic approximation where epsilon=0.00001 stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.2163996069941008
MST len: 2.434961946620251
Steiner Ratio: 0.9102399362217891
Solution Steiner tree:
	Terminals:
	  #0: (0.3821752264,0.3227100961,0.9883655114) [ #7 ]
	  #1: (0.1214791905,0.3483067552,0.4219620016) [ #5 ]
	  #2: (0.8425220697,0.2753141943,0.5966266629) [ #11 ]
	  #4: (0.7009763693,0.8096763491,0.0887954552) [ #9 ]
	  #6: (0.2713367107,0.069655987,0.949639379) [ #7 ]
	  #8: (0.6429663057,0.9906032789,0.295718284) [ #9 ]
	  #10: (0.6998054984,0.0663843365,0.5874823525) [ #11 ]
	Steiner points:
	  #3: (0.46993418618804655,0.25641815639424914,0.726698004501033) [ #7 #5 #11 ]
	  #5: (0.28704810278545523,0.4364701907684528,0.5104584830282726) [ #9 #3 #1 ]
	  #7: (0.36823132078926163,0.20524170110452375,0.876568520781903) [ #3 #6 #0 ]
	  #9: (0.6229549247973541,0.8580201818462332,0.1988209890424966) [ #4 #5 #8 ]
	  #11: (0.6789263518563754,0.12389770225828316,0.6155944270501388) [ #2 #3 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 713.802870735
	Number of best updates: 9

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 753

	Init times:
		Bottleneck Steiner distances: 0.00006321
		Smallest sphere distances: 0.000013088
		Sorting: 0.000057845
		Total init time: 0.000135716

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000005098, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000035274, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000322614, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.002656273, bsd pruned: 0, ss pruned: 315

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 630
	Total number iterations 68028 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 107.98095238095237
	Total time: 713.797141112
	Avarage time pr. topology: 1.133011334

Data for the geometric median iteration:
	Total time: 713.578583597
	Total steps: 77587135
	Average number of steps pr. geometric median: 24630.836507936507
	Average time pr. step: 0.000009197125059418679
	Average time pr. geometric median: 0.2265328836815873

Data for the geometric median step function:
	Total number of precision errors encountered: 0

