All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.9545420335211205
MST len: 3.4270432598999996
Steiner Ratio: 0.8621256895389565
Solution Steiner tree:
	Terminals:
	  #0: (0.4803538809,0.6394705305,0.1302257493) [ #3 ]
	  #1: (0.3111650372,0.5165184259,0.6420080073) [ #7 ]
	  #2: (0.7856002826,0.4401777473,0.1151188249) [ #11 ]
	  #4: (0.4910349215,0.0853253426,0.4141814399) [ #9 ]
	  #6: (0.7771808131,0.8949633971,0.8192397947) [ #7 ]
	  #8: (0.2860070939,0.0876902482,0.3650377357) [ #9 ]
	  #10: (0.8235438768,0.6883942167,0.1299254853) [ #11 ]
	Steiner points:
	  #3: (0.49103492165566,0.5165184587457163,0.1331937918816713) [ #5 #0 #11 ]
	  #5: (0.49103492165566,0.5165184587457163,0.39780020516666714) [ #7 #3 #9 ]
	  #7: (0.49103492165566,0.5165184590151324,0.6420080073) [ #5 #6 #1 ]
	  #9: (0.49103413946171637,0.0876902482,0.39780020516666714) [ #4 #5 #8 ]
	  #11: (0.7856002826000001,0.5165184587457163,0.1331937918760892) [ #2 #3 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.007137305
	Number of best updates: 11

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 573

	Init times:
		Bottleneck Steiner distances: 0.000012925
		Smallest sphere distances: 0.000003007
		Sorting: 0.000005542
		Total init time: 0.000022719

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001458, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000006537, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000061213, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000532144, bsd pruned: 0, ss pruned: 495

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 450
	Total number iterations 902 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.0044444444444443
	Total time: 0.00595794
	Avarage time pr. topology: 0.000013239

Data for the geometric median iteration:
	Total time: 0.004679553
	Total steps: 13021
	Average number of steps pr. geometric median: 5.787111111111111
	Average time pr. step: 0.00000035938507027110057
	Average time pr. geometric median: 0.0000020798013333333333

Data for the geometric median step function:
	Total number of fixed points encountered: 18234

