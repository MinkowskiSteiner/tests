All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.6991857293929002
MST len: 2.9018936065331133
Steiner Ratio: 0.9301463442064688
Solution Steiner tree:
	Terminals:
	  #0: (0.3094491373,0.8584360545,0.6783925484) [ #3 ]
	  #1: (0.2154374701,0.0895364271,0.3215230151) [ #7 ]
	  #2: (0.2498201687,0.6923724262,0.3582976681) [ #11 ]
	  #4: (0.3833639991,0.2886057418,0.8238008194) [ #9 ]
	  #6: (0.0389199243,0.0333043621,0.0983172874) [ #7 ]
	  #8: (0.8231695224,0.1180784167,0.5551794663) [ #9 ]
	  #10: (0.5994994997,0.9209823329,0.0083380486) [ #11 ]
	Steiner points:
	  #3: (0.28052382093376105,0.6598626780164208,0.4541814108102647) [ #5 #0 #11 ]
	  #5: (0.34819685975067177,0.3143421986339566,0.5120813526745571) [ #7 #3 #9 ]
	  #7: (0.21543747009997105,0.08953642710007341,0.32152301509997366) [ #5 #6 #1 ]
	  #9: (0.44571765492881943,0.26990914398835203,0.6348258774566748) [ #4 #5 #8 ]
	  #11: (0.25014485071668613,0.6924113645665815,0.35839007722007665) [ #2 #3 #10 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.040134526
	Number of best updates: 12

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 858

	Init times:
		Bottleneck Steiner distances: 0.000008436
		Smallest sphere distances: 0.000001102
		Sorting: 0.000001834
		Total init time: 0.000012577

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000859, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003246, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000030437, bsd pruned: 0, ss pruned: 0
		Level 4 - Time: 0.000279229, bsd pruned: 0, ss pruned: 210

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 735
	Total number iterations 7671 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 10.43673469387755
	Total time: 0.039346057
	Avarage time pr. topology: 0.000053532

Data for the geometric median iteration:
	Total time: 0.036769789
	Total steps: 249761
	Average number of steps pr. geometric median: 67.9621768707483
	Average time pr. step: 0.0000001472198982227009
	Average time pr. geometric median: 0.00001000538476190476

Data for the geometric median step function:
	Total number of precision errors encountered: 0

