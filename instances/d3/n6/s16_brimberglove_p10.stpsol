All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.2424072719608334
MST len: 3.8071136237000003
Steiner Ratio: 0.8516707386341812
Solution Steiner tree:
	Terminals:
	  #0: (0.2154374701,0.0895364271,0.3215230151) [ #5 ]
	  #1: (0.3094491373,0.8584360545,0.6783925484) [ #9 ]
	  #2: (0.2498201687,0.6923724262,0.3582976681) [ #3 ]
	  #4: (0.3833639991,0.2886057418,0.8238008194) [ #7 ]
	  #6: (0.8231695224,0.1180784167,0.5551794663) [ #7 ]
	  #8: (0.5994994997,0.9209823329,0.0083380486) [ #9 ]
	Steiner points:
	  #3: (0.3833639990999999,0.6923724262,0.3215230151000302) [ #5 #2 #9 ]
	  #5: (0.3833639990999999,0.2886057417999999,0.3215230151000302) [ #7 #3 #0 ]
	  #7: (0.3833648379608635,0.2886057418,0.5551794663) [ #4 #5 #6 ]
	  #9: (0.38336399909999985,0.8584360545,0.3215230151000302) [ #1 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.001392989
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000019014
		Smallest sphere distances: 0.000001709
		Sorting: 0.000003486
		Total init time: 0.000025391

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001257, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000007146, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00006575, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 210 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.001195835
	Avarage time pr. topology: 0.000011388

Data for the geometric median iteration:
	Total time: 0.000952621
	Total steps: 2568
	Average number of steps pr. geometric median: 6.114285714285714
	Average time pr. step: 0.0000003709583333333333
	Average time pr. geometric median: 0.0000022681452380952378

Data for the geometric median step function:
	Total number of fixed points encountered: 3362

