All distances are in Lp space with p=20 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Rodríguez-chía and Valero-Franco's iteration with the hyberbolic approximation where epsilon=0.00001 stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.744461322906966
MST len: 2.0470318851025486
Steiner Ratio: 0.8521905963470496
Solution Steiner tree:
	Terminals:
	  #0: (0.9290727782,0.9129959489,0.9238626137) [ #3 ]
	  #1: (0.4872422654,0.1321992586,0.3741906497) [ #5 ]
	  #2: (0.6797411119,0.8863195306,0.801372399) [ #3 ]
	  #4: (0.2213446718,0.2145956132,0.2002614784) [ #5 ]
	  #6: (0.9674219824,0.2784534135,0.4652112594) [ #7 ]
	  #8: (0.2359872904,0.785772445,0.946517847) [ #9 ]
	Steiner points:
	  #3: (0.7908014086281373,0.7756177127259295,0.8129901555664096) [ #0 #2 #7 ]
	  #5: (0.3665861524532243,0.25285935821047306,0.3454300134003648) [ #1 #4 #9 ]
	  #7: (0.6485603234507789,0.5962981240770695,0.6737519388236515) [ #3 #6 #9 ]
	  #9: (0.5710702488082117,0.5189402998737328,0.6116643748444123) [ #5 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 152.19338783
	Number of best updates: 11

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 108

	Init times:
		Bottleneck Steiner distances: 0.000023876
		Smallest sphere distances: 0.000018151
		Sorting: 0.000028872
		Total init time: 0.000072268

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000005088, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000034643, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000283797, bsd pruned: 0, ss pruned: 15

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 90
	Total number iterations 8093 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 89.92222222222222
	Total time: 152.192663528
	Avarage time pr. topology: 1.691029593

Data for the geometric median iteration:
	Total time: 152.170197952
	Total steps: 14246480
	Average number of steps pr. geometric median: 39573.555555555555
	Average time pr. step: 0.000010681248838449918
	Average time pr. geometric median: 0.42269499431111107

Data for the geometric median step function:
	Total number of precision errors encountered: 0

