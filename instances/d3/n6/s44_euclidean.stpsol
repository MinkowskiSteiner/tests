All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.9458976934066028
MST len: 2.031414421842169
Steiner Ratio: 0.9579028643707195
Solution Steiner tree:
	Terminals:
	  #0: (0.1925271313,0.3823489777,0.5831195775) [ #7 ]
	  #1: (0.1765204129,0.443329524,0.5713405118) [ #3 ]
	  #2: (0.1180090462,0.7377023845,0.5829005281) [ #5 ]
	  #4: (0.8574096602,0.9521014504,0.4074837125) [ #9 ]
	  #6: (0.0619196091,0.0727788904,0.1874291036) [ #7 ]
	  #8: (0.7497371406,0.6546126439,0.7923043043) [ #9 ]
	Steiner points:
	  #3: (0.17652145237759662,0.4433294703330697,0.5713408694016013) [ #5 #7 #1 ]
	  #5: (0.2288041058423481,0.618110243451147,0.5903773677468007) [ #2 #3 #9 ]
	  #7: (0.1881326148623716,0.38600274256250444,0.5750277510711587) [ #6 #3 #0 ]
	  #9: (0.6684059905710432,0.7182936820657626,0.6587084398011218) [ #4 #5 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.07653545
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 78

	Init times:
		Bottleneck Steiner distances: 0.000215441
		Smallest sphere distances: 0.000021488
		Sorting: 0.000050599
		Total init time: 0.000303503

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000016093, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00010582, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000637937, bsd pruned: 0, ss pruned: 45

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 60
	Total number iterations 342 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 5.7
	Total time: 0.073838327
	Avarage time pr. topology: 0.001230638

Data for the geometric median iteration:
	Total time: 0.07033282
	Total steps: 9359
	Average number of steps pr. geometric median: 38.99583333333333
	Average time pr. step: 0.0000075149930548135485
	Average time pr. geometric median: 0.00029305341666666665

Data for the geometric median step function:
	Total number of precision errors encountered: 0

