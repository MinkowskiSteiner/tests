All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.463219388610754
MST len: 3.1372075924
Steiner Ratio: 0.7851630203171741
Solution Steiner tree:
	Terminals:
	  #0: (0.563644016,0.4461416623,0.7561149335) [ #9 ]
	  #1: (0.414524392,0.4607495118,0.1862228323) [ #5 ]
	  #2: (0.3292264125,0.6730409189,0.520684405) [ #3 ]
	  #4: (0.2885274982,0.0973462291,0.2666000273) [ #5 ]
	  #6: (0.0191505631,0.5234706907,0.8290278501) [ #7 ]
	  #8: (0.8650269224,0.5663939652,0.6338536677) [ #9 ]
	Steiner points:
	  #3: (0.33243965694493344,0.49635107593418,0.520684405) [ #5 #2 #7 ]
	  #5: (0.33243965694493344,0.46074951179999996,0.2666000273) [ #1 #3 #4 ]
	  #7: (0.3324396569449334,0.49635107593418,0.697303665812515) [ #6 #3 #9 ]
	  #9: (0.5636440160000001,0.49635107593418,0.6973036658125149) [ #0 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.035860624
	Number of best updates: 2

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 108

	Init times:
		Bottleneck Steiner distances: 0.000234974
		Smallest sphere distances: 0.000031887
		Sorting: 0.000061857
		Total init time: 0.000348757

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000024103, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000151648, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00127579, bsd pruned: 15, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 90
	Total number iterations 181 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.011111111111111
	Total time: 0.031332493
	Avarage time pr. topology: 0.000348138

Data for the geometric median iteration:
	Total time: 0.026827505
	Total steps: 2209
	Average number of steps pr. geometric median: 6.136111111111111
	Average time pr. step: 0.000012144637845178814
	Average time pr. geometric median: 0.00007452084722222223

Data for the geometric median step function:
	Total number of fixed points encountered: 3015

