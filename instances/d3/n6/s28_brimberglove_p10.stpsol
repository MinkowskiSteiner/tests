All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.8719019780000004
MST len: 3.2898848227000004
Steiner Ratio: 0.872949094808443
Solution Steiner tree:
	Terminals:
	  #0: (0.7013661609,0.408563846,0.7201248397) [ #5 ]
	  #1: (0.3412099454,0.230164813,0.4956663146) [ #3 ]
	  #2: (0.442212973,0.1277577337,0.1569511728) [ #7 ]
	  #4: (0.8276713341,0.0367873907,0.8458900362) [ #5 ]
	  #6: (0.0837527169,0.7310733133,0.0224596341) [ #7 ]
	  #8: (0.517155105,0.0449436945,0.4524295369) [ #9 ]
	Steiner points:
	  #3: (0.3412099502425329,0.15317989020913866,0.4956663146) [ #7 #1 #9 ]
	  #5: (0.7013661609,0.11710115830610046,0.7201248397) [ #0 #4 #9 ]
	  #7: (0.3412099502425329,0.15317989020913866,0.1569511728) [ #6 #3 #2 ]
	  #9: (0.517155105,0.11710115830610046,0.4956663146) [ #3 #5 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.038659104
	Number of best updates: 9

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 108

	Init times:
		Bottleneck Steiner distances: 0.000210795
		Smallest sphere distances: 0.00002321
		Sorting: 0.000057954
		Total init time: 0.000308562

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000017268, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000168643, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.001459752, bsd pruned: 0, ss pruned: 15

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 90
	Total number iterations 181 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.011111111111111
	Total time: 0.033540975
	Avarage time pr. topology: 0.000372677

Data for the geometric median iteration:
	Total time: 0.028578627
	Total steps: 2178
	Average number of steps pr. geometric median: 6.05
	Average time pr. step: 0.0000131215
	Average time pr. geometric median: 0.000079385075

Data for the geometric median step function:
	Total number of fixed points encountered: 2724

