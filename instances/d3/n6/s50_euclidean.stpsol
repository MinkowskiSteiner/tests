All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.05592811605777
MST len: 2.063204425362969
Steiner Ratio: 0.9964732969667224
Solution Steiner tree:
	Terminals:
	  #0: (0.975851736,0.6714702508,0.5076044628) [ #9 ]
	  #1: (0.0156422849,0.215467854,0.2348691641) [ #7 ]
	  #2: (0.1913313205,0.4307993098,0.5479775013) [ #3 ]
	  #4: (0.9218689981,0.5204600438,0.6477700405) [ #5 ]
	  #6: (0.0151233026,0.0380932228,0.0248203492) [ #7 ]
	  #8: (0.8943212232,0.6286429756,0.1040961771) [ #9 ]
	Steiner points:
	  #3: (0.19142378394280984,0.430742671236195,0.5478670221317647) [ #2 #5 #7 ]
	  #5: (0.887297129852899,0.5505805599004475,0.6020432882710823) [ #4 #3 #9 ]
	  #7: (0.015642327121335422,0.21546784078160616,0.23486916245692505) [ #1 #3 #6 ]
	  #9: (0.9612200080396147,0.6548553137346691,0.49914556200673743) [ #0 #5 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.106301654
	Number of best updates: 1

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 63

	Init times:
		Bottleneck Steiner distances: 0.000333251
		Smallest sphere distances: 0.0000305
		Sorting: 0.000041698
		Total init time: 0.000429377

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000024848, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000129451, bsd pruned: 0, ss pruned: 3
		Level 3 - Time: 0.000733683, bsd pruned: 0, ss pruned: 36

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 48
	Total number iterations 242 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 5.041666666666667
	Total time: 0.103032966
	Avarage time pr. topology: 0.00214652

Data for the geometric median iteration:
	Total time: 0.099465136
	Total steps: 9089
	Average number of steps pr. geometric median: 47.338541666666664
	Average time pr. step: 0.000010943463087248322
	Average time pr. geometric median: 0.0005180475833333333

Data for the geometric median step function:
	Total number of precision errors encountered: 0

