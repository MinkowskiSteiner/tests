All distances are in Lp space with p=20 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Rodríguez-chía and Valero-Franco's iteration with the hyberbolic approximation where epsilon=0.00001 stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.1935602658881848
MST len: 1.54315956840061
Steiner Ratio: 0.773452266589149
Solution Steiner tree:
	Terminals:
	  #0: (0.2860070939,0.0876902482,0.3650377357) [ #5 ]
	  #1: (0.4803538809,0.6394705305,0.1302257493) [ #3 ]
	  #2: (0.7856002826,0.4401777473,0.1151188249) [ #9 ]
	  #4: (0.4910349215,0.0853253426,0.4141814399) [ #5 ]
	  #6: (0.3111650372,0.5165184259,0.6420080073) [ #7 ]
	  #8: (0.8235438768,0.6883942167,0.1299254853) [ #9 ]
	Steiner points:
	  #3: (0.6300069481131858,0.49312279892470356,0.2713976536410077) [ #1 #7 #9 ]
	  #5: (0.3873508016258363,0.1886456905369148,0.4004728228811697) [ #4 #7 #0 ]
	  #7: (0.48043455605988206,0.3197528973365455,0.4450026724596161) [ #3 #5 #6 ]
	  #9: (0.6854374859546892,0.5451746613861389,0.21754433706076987) [ #2 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 52.415152034
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 93

	Init times:
		Bottleneck Steiner distances: 0.000033544
		Smallest sphere distances: 0.000017857
		Sorting: 0.000020284
		Total init time: 0.000072841

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.00000479, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00003346, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000241042, bsd pruned: 0, ss pruned: 30

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 75
	Total number iterations 3789 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 50.52
	Total time: 52.414537959
	Avarage time pr. topology: 0.698860505

Data for the geometric median iteration:
	Total time: 52.403111749
	Total steps: 4901169
	Average number of steps pr. geometric median: 16337.23
	Average time pr. step: 0.000010691961805234628
	Average time pr. geometric median: 0.17467703916333333

Data for the geometric median step function:
	Total number of precision errors encountered: 0

