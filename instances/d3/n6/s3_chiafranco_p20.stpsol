All distances are in Lp space with p=20 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Rodríguez-chía and Valero-Franco's iteration with the hyberbolic approximation where epsilon=0.00001 stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.5360941321078674
MST len: 1.7894193632122397
Steiner Ratio: 0.8584316028358938
Solution Steiner tree:
	Terminals:
	  #0: (0.4439383934,0.2850412509,0.1447810513) [ #7 ]
	  #1: (0.1763232952,0.6261662299,0.9448735988) [ #9 ]
	  #2: (0.5613801752,0.2249833128,0.3930917794) [ #3 ]
	  #4: (0.8460845993,0.0099969395,0.257080875) [ #5 ]
	  #6: (0.2308046744,0.0040116059,0.4605808875) [ #7 ]
	  #8: (0.5635544996,0.8646787037,0.8954018363) [ #9 ]
	Steiner points:
	  #3: (0.561131148667261,0.22498471260547354,0.3933211928233834) [ #5 #2 #7 ]
	  #5: (0.6030690229917057,0.25286597314694764,0.43527024494554584) [ #4 #3 #9 ]
	  #7: (0.4303216682782173,0.182470381839893,0.2629606114833433) [ #6 #3 #0 ]
	  #9: (0.31897189464942854,0.6200740448607526,0.8023144126393829) [ #1 #5 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 53.269243389
	Number of best updates: 2

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 63

	Init times:
		Bottleneck Steiner distances: 0.000032304
		Smallest sphere distances: 0.000009269
		Sorting: 0.000029003
		Total init time: 0.000071637

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000004932, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000036453, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000169769, bsd pruned: 0, ss pruned: 60

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 45
	Total number iterations 2299 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 51.08888888888889
	Total time: 53.268778904
	Avarage time pr. topology: 1.183750641

Data for the geometric median iteration:
	Total time: 53.261984267
	Total steps: 4992796
	Average number of steps pr. geometric median: 27737.755555555555
	Average time pr. step: 0.000010667766972053335
	Average time pr. geometric median: 0.2958999125944445

Data for the geometric median step function:
	Total number of precision errors encountered: 0

