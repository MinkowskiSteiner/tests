All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.328371201382892
MST len: 2.461204717
Steiner Ratio: 0.9460290667007086
Solution Steiner tree:
	Terminals:
	  #0: (0.4311483691,0.7982122655,0.1291394788) [ #9 ]
	  #1: (0.1086744881,0.7979400231,0.6606961906) [ #5 ]
	  #2: (0.0071734451,0.7727813925,0.162069249) [ #3 ]
	  #4: (0.0411185827,0.809808453,0.6971266361) [ #5 ]
	  #6: (0.1812310276,0.1675375119,0.7022328906) [ #7 ]
	  #8: (0.5244897048,0.4379004908,0.0703035561) [ #9 ]
	Steiner points:
	  #3: (0.11784757784444445,0.7727813915171184,0.16206924900000003) [ #2 #7 #9 ]
	  #5: (0.10867448809999025,0.7979400230999999,0.6606961906) [ #4 #1 #7 ]
	  #7: (0.11784757784444445,0.7727813915171184,0.6606961906) [ #3 #5 #6 ]
	  #9: (0.4311483691,0.7727813915171184,0.1291394788) [ #0 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.008658181
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 48

	Init times:
		Bottleneck Steiner distances: 0.000165983
		Smallest sphere distances: 0.000033739
		Sorting: 0.000056694
		Total init time: 0.000273006

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000015055, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000088764, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00049475, bsd pruned: 30, ss pruned: 45

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 30
	Total number iterations 60 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.00668526
	Avarage time pr. topology: 0.000222842

Data for the geometric median iteration:
	Total time: 0.005767974
	Total steps: 832
	Average number of steps pr. geometric median: 6.933333333333334
	Average time pr. step: 0.000006932661057692308
	Average time pr. geometric median: 0.00004806645000000001

Data for the geometric median step function:
	Total number of fixed points encountered: 1096

