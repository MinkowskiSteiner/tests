All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.9985626252268642
MST len: 2.0942023109655583
Steiner Ratio: 0.9543312099132398
Solution Steiner tree:
	Terminals:
	  #0: (0.3857721921,0.3929051535,0.0476035024) [ #7 ]
	  #1: (0.3549731771,0.6731490375,0.5153574038) [ #9 ]
	  #2: (0.3443029827,0.6699542467,0.4050064401) [ #3 ]
	  #4: (0.9775588964,0.7308916956,0.3246047955) [ #5 ]
	  #6: (0.2348172144,0.1034733854,0.3869860398) [ #7 ]
	  #8: (0.2768868838,0.9831143995,0.7832298362) [ #9 ]
	Steiner points:
	  #3: (0.34433112899845864,0.6699351724707443,0.4050243352742261) [ #2 #5 #9 ]
	  #5: (0.4458113665180311,0.5866953262153993,0.32660045407832594) [ #4 #3 #7 ]
	  #7: (0.374150876830441,0.39574918622713867,0.20594177080787582) [ #6 #5 #0 ]
	  #9: (0.354963697228337,0.6731729453182326,0.5153455961243759) [ #1 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.002226262
	Number of best updates: 1

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 93

	Init times:
		Bottleneck Steiner distances: 0.000005988
		Smallest sphere distances: 0.000000728
		Sorting: 0.000001123
		Total init time: 0.000008694

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000621, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003663, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000023837, bsd pruned: 30, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 75
	Total number iterations 387 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 5.16
	Total time: 0.002131278
	Avarage time pr. topology: 0.000028417

Data for the geometric median iteration:
	Total time: 0.00199755
	Total steps: 13425
	Average number of steps pr. geometric median: 44.75
	Average time pr. step: 0.00000014879329608938547
	Average time pr. geometric median: 0.0000066585

Data for the geometric median step function:
	Total number of precision errors encountered: 0

