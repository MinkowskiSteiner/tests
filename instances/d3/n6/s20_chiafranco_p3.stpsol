All distances are in Lp space with p=3 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Rodríguez-chía and Valero-Franco's iteration with the hyberbolic approximation where epsilon=0.00001 stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.8133044725674838
MST len: 1.9749565085464338
Steiner Ratio: 0.9181490654202175
Solution Steiner tree:
	Terminals:
	  #0: (0.1488122675,0.7401193766,0.5311698176) [ #7 ]
	  #1: (0.6938135543,0.3702549456,0.5785871486) [ #5 ]
	  #2: (0.3798184727,0.6551358368,0.3903584468) [ #3 ]
	  #4: (0.1478928184,0.0819520862,0.2319531549) [ #9 ]
	  #6: (0.4885317048,0.9929940151,0.7874475535) [ #7 ]
	  #8: (0.6617850073,0.153481192,0.2006002349) [ #9 ]
	Steiner points:
	  #3: (0.37892366540149724,0.6509809629812539,0.39568493850218867) [ #5 #7 #2 ]
	  #5: (0.5595981599651314,0.41997663404095564,0.45725602235636853) [ #1 #3 #9 ]
	  #7: (0.24727026845092817,0.7629235657789544,0.5297705236223279) [ #6 #3 #0 ]
	  #9: (0.48439010167672225,0.2852259683793787,0.34117426624275937) [ #4 #5 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 28.85570758
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 93

	Init times:
		Bottleneck Steiner distances: 0.000032433
		Smallest sphere distances: 0.000009202
		Sorting: 0.000040961
		Total init time: 0.000083452

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000004833, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000033721, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000240607, bsd pruned: 15, ss pruned: 15

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 75
	Total number iterations 4149 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 55.32
	Total time: 28.855078377
	Avarage time pr. topology: 0.384734378

Data for the geometric median iteration:
	Total time: 28.844138797
	Total steps: 3133498
	Average number of steps pr. geometric median: 10444.993333333334
	Average time pr. step: 0.000009205092454822055
	Average time pr. geometric median: 0.09614712932333333

Data for the geometric median step function:
	Total number of precision errors encountered: 0

