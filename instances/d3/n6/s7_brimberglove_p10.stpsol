All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.5245736147379128
MST len: 4.3169763421
Steiner Ratio: 0.8164449687540744
Solution Steiner tree:
	Terminals:
	  #0: (0.2147098403,0.0102265389,0.514818562) [ #9 ]
	  #1: (0.4869041394,0.8679774124,0.5925911942) [ #5 ]
	  #2: (0.163898673,0.2155044015,0.7876827134) [ #3 ]
	  #4: (0.7644367087,0.815491903,0.8889724807) [ #5 ]
	  #6: (0.0553448466,0.5267799439,0.0893740547) [ #7 ]
	  #8: (0.9959482527,0.0319323023,0.6015652793) [ #9 ]
	Steiner points:
	  #3: (0.25231960874163994,0.2155044015,0.5859563257037274) [ #2 #7 #9 ]
	  #5: (0.4869041394,0.815491903,0.5925911942) [ #4 #1 #7 ]
	  #7: (0.25231960874163994,0.5267799439,0.5859563257037274) [ #3 #5 #6 ]
	  #9: (0.25231960874163994,0.0319323023,0.5859563257037274) [ #0 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.0013508
	Number of best updates: 7

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000018628
		Smallest sphere distances: 0.000001605
		Sorting: 0.000003585
		Total init time: 0.000024813

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001262, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000006905, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000084181, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 210 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.00113942
	Avarage time pr. topology: 0.000010851

Data for the geometric median iteration:
	Total time: 0.000891865
	Total steps: 2326
	Average number of steps pr. geometric median: 5.538095238095238
	Average time pr. step: 0.000000383432932072227
	Average time pr. geometric median: 0.0000021234880952380952

Data for the geometric median step function:
	Total number of fixed points encountered: 3210

