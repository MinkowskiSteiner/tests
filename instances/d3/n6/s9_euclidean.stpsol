All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.5880347362031193
MST len: 2.6203421114809844
Steiner Ratio: 0.9876705506749249
Solution Steiner tree:
	Terminals:
	  #0: (0.206965448,0.6995153961,0.2034307295) [ #5 ]
	  #1: (0.3588790108,0.884531313,0.4632373589) [ #3 ]
	  #2: (0.7227528005,0.6292115332,0.7207948913) [ #9 ]
	  #4: (0.0917933952,0.0898954501,0.3307895457) [ #5 ]
	  #6: (0.0740991244,0.9287021542,0.8842824203) [ #7 ]
	  #8: (0.7304651908,0.05407485,0.9231773037) [ #9 ]
	Steiner points:
	  #3: (0.3588202860428823,0.8843754749440434,0.4632816399956454) [ #7 #5 #1 ]
	  #5: (0.21163243156821468,0.6915729736777317,0.22083290776959305) [ #4 #3 #0 ]
	  #7: (0.3744065644529319,0.8415916771633131,0.6019075615680853) [ #6 #3 #9 ]
	  #9: (0.7226612660779518,0.6291612433875755,0.7208008035261612) [ #2 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.005143759
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 93

	Init times:
		Bottleneck Steiner distances: 0.000005374
		Smallest sphere distances: 0.000000801
		Sorting: 0.000001104
		Total init time: 0.000019073

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000000806, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000003203, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000021561, bsd pruned: 0, ss pruned: 30

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 75
	Total number iterations 830 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 11.066666666666666
	Total time: 0.005044751
	Avarage time pr. topology: 0.000067263

Data for the geometric median iteration:
	Total time: 0.004827558
	Total steps: 34693
	Average number of steps pr. geometric median: 115.64333333333333
	Average time pr. step: 0.0000001391507796961923
	Average time pr. geometric median: 0.000016091859999999998

Data for the geometric median step function:
	Total number of precision errors encountered: 0

