All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 3.705504090158013
MST len: 4.3272346665
Steiner Ratio: 0.8563215022390127
Solution Steiner tree:
	Terminals:
	  #0: (0.1300345134,0.6083626135,0.3345068657) [ #9 ]
	  #1: (0.1637372478,0.1157799243,0.8294556531) [ #7 ]
	  #2: (0.6326431831,0.3414155828,0.904109925) [ #3 ]
	  #4: (0.9116950463,0.792852993,0.8622477059) [ #5 ]
	  #6: (0.5560647936,0.0749040051,0.0531665883) [ #7 ]
	  #8: (0.1558290944,0.7948272283,0.928200935) [ #9 ]
	Steiner points:
	  #3: (0.4501781803777777,0.3414155828,0.8622477058642094) [ #5 #2 #7 ]
	  #5: (0.4501781803777777,0.6500243676412455,0.8622477058642094) [ #3 #4 #9 ]
	  #7: (0.4501781803777777,0.1157799243,0.8294556531000001) [ #1 #3 #6 ]
	  #9: (0.1558290944,0.6500243676412455,0.8622477058642094) [ #0 #5 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.00122105
	Number of best updates: 4

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 108

	Init times:
		Bottleneck Steiner distances: 0.000009063
		Smallest sphere distances: 0.000002168
		Sorting: 0.00000344
		Total init time: 0.000015804

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000001356, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.00000711, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.00005592, bsd pruned: 0, ss pruned: 15

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 90
	Total number iterations 181 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2.011111111111111
	Total time: 0.001055557
	Avarage time pr. topology: 0.000011728

Data for the geometric median iteration:
	Total time: 0.000853469
	Total steps: 2335
	Average number of steps pr. geometric median: 6.486111111111111
	Average time pr. step: 0.0000003655113490364026
	Average time pr. geometric median: 0.000002370747222222222

Data for the geometric median step function:
	Total number of fixed points encountered: 3104

