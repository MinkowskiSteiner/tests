All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.247136883845442
MST len: 2.412678715982513
Steiner Ratio: 0.9313867068000152
Solution Steiner tree:
	Terminals:
	  #0: (0.8602793272,0.35034057,0.4464217548) [ #3 ]
	  #1: (0.9184383005,0.2771394566,0.1947866791) [ #9 ]
	  #2: (0.3481050377,0.7246325052,0.5291235137) [ #5 ]
	  #4: (0.0208598026,0.0373252058,0.1292244392) [ #5 ]
	  #6: (0.7855343049,0.3411775247,0.8119810777) [ #7 ]
	  #8: (0.8194229132,0.5062902763,0.0883140588) [ #9 ]
	Steiner points:
	  #3: (0.8602723239242019,0.3503415916260801,0.4464181174232629) [ #7 #0 #9 ]
	  #5: (0.4336907462798949,0.47398649371221724,0.4719293923773244) [ #2 #4 #7 ]
	  #7: (0.7351817763900831,0.3769990203118289,0.5663508822326611) [ #3 #5 #6 ]
	  #9: (0.8880039928058823,0.33558249870571394,0.2234056146406481) [ #1 #3 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.162935474
	Number of best updates: 8

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 108

	Init times:
		Bottleneck Steiner distances: 0.000208625
		Smallest sphere distances: 0.000014761
		Sorting: 0.000035382
		Total init time: 0.00027009

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000012112, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000078544, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000667297, bsd pruned: 0, ss pruned: 15

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 90
	Total number iterations 707 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 7.855555555555555
	Total time: 0.160452583
	Avarage time pr. topology: 0.001782806

Data for the geometric median iteration:
	Total time: 0.1552841
	Total steps: 27368
	Average number of steps pr. geometric median: 76.02222222222223
	Average time pr. step: 0.0000056739294066062555
	Average time pr. geometric median: 0.00043134472222222225

Data for the geometric median step function:
	Total number of precision errors encountered: 0

