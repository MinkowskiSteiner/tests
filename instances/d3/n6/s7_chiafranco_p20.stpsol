All distances are in Lp space with p=20 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Rodríguez-chía and Valero-Franco's iteration with the hyberbolic approximation where epsilon=0.00001 stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 1.9789586330348519
MST len: 2.3755264776936493
Steiner Ratio: 0.8330610715634637
Solution Steiner tree:
	Terminals:
	  #0: (0.4869041394,0.8679774124,0.5925911942) [ #5 ]
	  #1: (0.2147098403,0.0102265389,0.514818562) [ #3 ]
	  #2: (0.163898673,0.2155044015,0.7876827134) [ #3 ]
	  #4: (0.7644367087,0.815491903,0.8889724807) [ #5 ]
	  #6: (0.0553448466,0.5267799439,0.0893740547) [ #7 ]
	  #8: (0.9959482527,0.0319323023,0.6015652793) [ #9 ]
	Steiner points:
	  #3: (0.28725989267353697,0.09805097165679223,0.602422644369142) [ #1 #2 #7 ]
	  #5: (0.6500290972182821,0.6923139579263905,0.7667826863112519) [ #0 #4 #9 ]
	  #7: (0.41454538115657086,0.25168061113218454,0.44865210734634275) [ #3 #6 #9 ]
	  #9: (0.5949910943323888,0.43145996281364785,0.5812681234035254) [ #5 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 236.823550765
	Number of best updates: 6

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 123

	Init times:
		Bottleneck Steiner distances: 0.000033705
		Smallest sphere distances: 0.000009186
		Sorting: 0.000028873
		Total init time: 0.000072795

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000005123, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000036739, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000326913, bsd pruned: 0, ss pruned: 0

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 105
	Total number iterations 9698 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 92.36190476190477
	Total time: 236.82272911
	Avarage time pr. topology: 2.255454562

Data for the geometric median iteration:
	Total time: 236.796208007
	Total steps: 22204436
	Average number of steps pr. geometric median: 52867.70476190476
	Average time pr. step: 0.000010664364904697421
	Average time pr. geometric median: 0.5638004952547618

Data for the geometric median step function:
	Total number of precision errors encountered: 0

