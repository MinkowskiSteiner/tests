All distances are in Euclidean space and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Weiszfeld's iteration with Ostresh's modification stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.4007174977403225
MST len: 2.598167891895748
Steiner Ratio: 0.924003989591544
Solution Steiner tree:
	Terminals:
	  #0: (0.9487606892,0.4132981074,0.5756729662) [ #3 ]
	  #1: (0.8256802293,0.8959328252,0.2042499758) [ #5 ]
	  #2: (0.8249852354,0.3957488953,0.5658004533) [ #3 ]
	  #4: (0.0595591958,0.9910994321,0.8735950118) [ #9 ]
	  #6: (0.9420667789,0.3395639064,0.3297338986) [ #7 ]
	  #8: (0.0233809524,0.3771066863,0.354725022) [ #9 ]
	Steiner points:
	  #3: (0.8670146767758152,0.41016583649728766,0.5372736376822693) [ #0 #7 #2 ]
	  #5: (0.6993817072647933,0.6313545333515718,0.38374939884612347) [ #1 #7 #9 ]
	  #7: (0.8571688221914789,0.4309044137340266,0.4529663687609117) [ #3 #5 #6 ]
	  #9: (0.25169712078609446,0.6254610422927633,0.504530031742275) [ #4 #5 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.224770053
	Number of best updates: 5

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 108

	Init times:
		Bottleneck Steiner distances: 0.000305318
		Smallest sphere distances: 0.000029285
		Sorting: 0.000107267
		Total init time: 0.000464568

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000019073, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000109705, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.000895832, bsd pruned: 0, ss pruned: 15

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 90
	Total number iterations 1125 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 12.5
	Total time: 0.221320051
	Avarage time pr. topology: 0.002459111

Data for the geometric median iteration:
	Total time: 0.209779013
	Total steps: 27546
	Average number of steps pr. geometric median: 76.51666666666667
	Average time pr. step: 0.000007615588942133159
	Average time pr. geometric median: 0.0005827194805555555

Data for the geometric median step function:
	Total number of precision errors encountered: 0

