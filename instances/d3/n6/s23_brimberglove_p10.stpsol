All distances are in Lp space with p=1 and the solution was found by the Steiner branch and bound algorithm using the Gilbert-Pollak enumerator (depth-first) with:
      - bottleneck Steiner distance pruning;
      - smallest spheres pruning;
      - and furthest site ordering.
Relatively minimal trees were found using the Geometric median iterator that continuously finds geometric medians until the change in tree length is less than 0.00001. Geometric medians were found using Brimberg and Love's iteration stopping when the change in position (measured in the current space) got below 0.00001.

########## Results:

Steiner tree len: 2.7633056845
MST len: 3.1909487938
Steiner Ratio: 0.865982459470704
Solution Steiner tree:
	Terminals:
	  #0: (0.2818813474,0.2621717841,0.5012496633) [ #5 ]
	  #1: (0.6994581058,0.1578644305,0.7714722398) [ #7 ]
	  #2: (0.3444002356,0.4630026335,0.5578971945) [ #3 ]
	  #4: (0.0351164541,0.1380414735,0.5359635696) [ #5 ]
	  #6: (0.921708364,0.6899727819,0.193846746) [ #9 ]
	  #8: (0.7275817463,0.4842158102,0.9432684327) [ #9 ]
	Steiner points:
	  #3: (0.3444002356,0.31214217437822395,0.5578971945) [ #2 #7 #5 ]
	  #5: (0.2818813474,0.2621717841,0.5359635696) [ #4 #0 #3 ]
	  #7: (0.6994581058,0.31214217437822395,0.5578971945) [ #1 #3 #9 ]
	  #9: (0.7275817463,0.4842158102,0.5578971945000001) [ #6 #7 #8 ]

########## Statistics: (All times are given in seconds)

Data for the Steiner branch and bound algorithm:
	Total time: 0.040262457
	Number of best updates: 3

Data for the Gilbert-Pollak enumeration:
	Number of nodes enumerated (i.e. not pruned): 93

	Init times:
		Bottleneck Steiner distances: 0.000426405
		Smallest sphere distances: 0.000063852
		Sorting: 0.00010807
		Total init time: 0.000640032

	Number of pruned nodes & time used on pruning at each level:
		Level 1 - Time: 0.000031228, bsd pruned: 0, ss pruned: 0
		Level 2 - Time: 0.000212904, bsd pruned: 0, ss pruned: 0
		Level 3 - Time: 0.001429724, bsd pruned: 0, ss pruned: 30

Data for the geometric median iteration RMT algorithm:
	Number of topologies optimized: 75
	Total number iterations 150 (one iteration optimizes all Steiner points of a topology)
	Avarage number of iterations pr. topology: 2
	Total time: 0.03462179
	Avarage time pr. topology: 0.000461623

Data for the geometric median iteration:
	Total time: 0.029788608
	Total steps: 1922
	Average number of steps pr. geometric median: 6.406666666666666
	Average time pr. step: 0.000015498755463059314
	Average time pr. geometric median: 0.00009929536

Data for the geometric median step function:
	Total number of fixed points encountered: 2711

