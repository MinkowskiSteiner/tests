#![allow(dead_code)]

extern crate minkowski_steiner;
extern crate rand;
extern crate conv;

use minkowski_steiner::traits::*;
use minkowski_steiner::geo::points::*;
use minkowski_steiner::geo::spaces::{EuclideanSpace, LpSpace};
use minkowski_steiner::steinertree::{SteinerTree, Node};
use minkowski_steiner::algorithms::rmt::*;
use minkowski_steiner::algorithms::geomedians::*;
use minkowski_steiner::algorithms::steinerbnb::*;
use minkowski_steiner::enumerator::*;
use minkowski_steiner::algorithms::mst::Kruskal;

use rand::distributions::{IndependentSample, Range};

use conv::ValueFrom;

use std::time::Duration;
use std::io::*;
use std::fs::{File, OpenOptions};
use std::path::*;

fn main() {
    do_experiments();
    //cherry(&Path::new("./instances/d3/n7/s1.stp"));
}

fn brimberg<P: Point>(i: &[P], path: &mut PathBuf, name: &str, data: &mut Data<P::R>) {
    let geo = LpSpace::new(P::R::one());
    let mut smtalg = SteinerBnB::default_rmt(
        GeoMedianIter::default_with_geomedian(
            GeoMedianStepper::default_with_step(
                BrimbergLove::default()
    )));

    path.push(&(name.to_string() + "_brimberglove_p10"));
    path.set_extension("stpsol");
    let mut file = BufWriter::new(File::create(&path).unwrap());
    let sr = run(i, &mut smtalg, &geo, &mut file);
    data.push(smtalg.data().clone(),
              smtalg.enumerator().data().clone(),
              smtalg.rmt_alg().data().clone(),
              smtalg.rmt_alg().geo_median_alg().data().clone(),
              <BrimbergLove as GeoMedianStep<P, LpSpace<P::R>>>::data(smtalg.rmt_alg().geo_median_alg().step_alg()).clone(),
              GeoMedianStepPrecisionErrorData::new(),
              sr);
    path.pop();
}

fn euclidean<P: Point>(i: &[P], path: &mut PathBuf, name: &str, data: &mut Data<P::R>) {
    let mut smtalg = SteinerBnB::default_rmt(
        GeoMedianIter::default_with_geomedian(
            GeoMedianStepper::default_with_step(
                Ostresh::default()
    )));

    path.push(&(name.to_string() + "_euclidean"));
    path.set_extension("stpsol");
    let mut file = BufWriter::new(File::create(&path).unwrap());
    let sr = run(i, &mut smtalg, &EuclideanSpace, &mut file);
    data.push(smtalg.data().clone(),
              smtalg.enumerator().data().clone(),
              smtalg.rmt_alg().data().clone(),
              smtalg.rmt_alg().geo_median_alg().data().clone(),
              GeoMedianStepFixedPointData::new(),
              <Ostresh as GeoMedianStep<P, EuclideanSpace>>::data(smtalg.rmt_alg().geo_median_alg().step_alg()).clone(),
              sr);
    path.pop();
}

fn chia3<P: Point>(i: &[P], path: &mut PathBuf, name: &str, data: &mut Data<P::R>) {
    let geo = LpSpace::new(P::R::from(3.0));
    //let cf = ChiaFranco::new(|n| P::R::from(n as f64).pow(P::R::from(-3.0)));
    let cf = ChiaFrancoApprox::new(P::R::from(0.00001));
    let mut smtalg = SteinerBnB::default_rmt(
        GeoMedianIter::default_with_geomedian(
            GeoMedianStepper::default_with_step(
                cf
    )));

    path.push(&(name.to_string() + "_chiafranco_p3"));
    path.set_extension("stpsol");
    let mut file = BufWriter::new(File::create(&path).unwrap());
    let sr = run(i, &mut smtalg, &geo, &mut file);
    data.push(smtalg.data().clone(),
              smtalg.enumerator().data().clone(),
              smtalg.rmt_alg().data().clone(),
              smtalg.rmt_alg().geo_median_alg().data().clone(),
              GeoMedianStepFixedPointData::new(),
              smtalg.rmt_alg().geo_median_alg().step_alg().data().clone(),
              sr);
    path.pop();
}

fn chia20<P: Point>(i: &[P], path: &mut PathBuf, name: &str, data: &mut Data<P::R>) {
    let geo = LpSpace::new(P::R::from(20.0));
    //let cf = ChiaFranco::new(|n| P::R::from(n as f64).pow(P::R::from(-3.0)));
    let cf = ChiaFrancoApprox::new(P::R::from(0.00001));
    let mut smtalg = SteinerBnB::default_rmt(
        GeoMedianIter::default_with_geomedian(
            GeoMedianStepper::default_with_step(
                cf
    )));

    path.push(&(name.to_string() + "_chiafranco_p20"));
    path.set_extension("stpsol");
    let mut file = BufWriter::new(File::create(&path).unwrap());
    let sr = run(i, &mut smtalg, &geo, &mut file);
    data.push(smtalg.data().clone(),
              smtalg.enumerator().data().clone(),
              smtalg.rmt_alg().data().clone(),
              smtalg.rmt_alg().geo_median_alg().data().clone(),
              GeoMedianStepFixedPointData::new(),
              smtalg.rmt_alg().geo_median_alg().step_alg().data().clone(),
              sr);
    path.pop();
}



struct Data<R: Real> {
    sbnbdata: Vec<SteinerBnBData>,
    gpenumdata: Vec<GPEnumerationData>,
    rmtdata: Vec<GeoMedianIterData>,
    geomediandata: Vec<GeoMedianStepperData>,
    geomedianstepfixeddata: Vec<GeoMedianStepFixedPointData>,
    geomedianstepprecisiondata: Vec<GeoMedianStepPrecisionErrorData>,
    srs: Vec<R>,
    path: PathBuf,
    alg: String,
    d: usize,
    n: usize,
}

impl<R: Real> Data<R> {
    fn new(path: PathBuf) -> Self {
        Data {
            sbnbdata: Vec::new(),
            gpenumdata: Vec::new(),
            rmtdata: Vec::new(),
            geomediandata: Vec::new(),
            geomedianstepfixeddata: Vec::new(),
            geomedianstepprecisiondata: Vec::new(),
            srs: Vec::new(),
            path: path,
            alg: String::new(),
            d: 0,
            n: 0
        }
    }

    fn num(&self) -> usize {
        self.sbnbdata.len()
    }

    fn push(&mut self, sbnb: SteinerBnBData, gpenum: GPEnumerationData,
            rmt: GeoMedianIterData, geomedian: GeoMedianStepperData,
            geomedianstepfixed: GeoMedianStepFixedPointData,
            geomedianstepprecision: GeoMedianStepPrecisionErrorData,
            sr: R)
    {
        self.sbnbdata.push(sbnb);
        self.gpenumdata.push(gpenum);
        self.rmtdata.push(rmt);
        self.geomediandata.push(geomedian);
        self.geomedianstepfixeddata.push(geomedianstepfixed);
        self.geomedianstepprecisiondata.push(geomedianstepprecision);
        self.srs.push(sr);
    }


    fn print(&mut self) {
        fn sec(dur: &Duration) -> f64 {
            (dur.as_secs() as f64) + (dur.subsec_nanos() as f64) / 1000000000.0
        }

        fn writeline<R: Real>(data: &mut Data<R>, file: &mut BufWriter<File>, s: usize) -> Vec<f64> {
            let totalprunetimes = data.gpenumdata[s].prune_times().iter()
                .fold(Duration::new(0,0), |acc, &t| acc + t);
            let totalprunetime = &totalprunetimes;
            let totaltime = sec(&(*data.gpenumdata[s].time()
                        + *data.gpenumdata[s].init_time()
                        + *totalprunetime
                        + *data.rmtdata[s].selftime()
                        + *data.geomediandata[s].time()));

            let mut dat = vec![
                sec(data.sbnbdata[s].time()),
                f64::value_from(data.sbnbdata[s].best_updates()).unwrap(),
                f64::value_from(data.gpenumdata[s].nodes()).unwrap(),
                sec(data.gpenumdata[s].time()),
                (sec(data.gpenumdata[s].time()) / totaltime) * 100.0,
                sec(data.gpenumdata[s].sort_time()),
                sec(data.gpenumdata[s].bsd_time()),
                sec(data.gpenumdata[s].ss_time()),
                sec(data.gpenumdata[s].init_time()),
                (sec(data.gpenumdata[s].init_time()) / totaltime) * 100.0,
                sec(totalprunetime),
                (sec(totalprunetime) / totaltime) * 100.0,
                f64::value_from(data.rmtdata[s].nodes()).unwrap(),
                sec(data.rmtdata[s].time()),
                (sec(data.rmtdata[s].time()) / totaltime) * 100.0,
                sec(data.rmtdata[s].selftime()),
                (sec(data.rmtdata[s].selftime()) / totaltime) * 100.0,
                f64::value_from(data.rmtdata[s].iterations()).unwrap(),
                sec(data.geomediandata[s].time()),
                (sec(data.geomediandata[s].time()) / totaltime) * 100.0,
                f64::value_from(data.geomediandata[s].average_steps()).unwrap(),
                data.geomediandata[s].average_time_step(),
                data.geomediandata[s].average_time_problem(),
                f64::value_from(data.geomedianstepfixeddata[s].fixed_points()).unwrap(),
                f64::value_from(data.geomedianstepprecisiondata[s].precision_errors()).unwrap(),
                data.srs[s].into()
            ];

            let terminals = data.n;
            for (i, &data) in data.gpenumdata[s].bsd_pruned().iter().enumerate() {
                let mut leafspr = (i+4..terminals).fold(1, |acc, t| acc*(2*t-5)) as f64;
                dat.push(f64::value_from(data).unwrap());
                dat.push(f64::value_from(data).unwrap()*leafspr);
            }
            for (i, &data) in data.gpenumdata[s].ss_pruned().iter().enumerate() {
                let mut leafspr = (i+4..terminals).fold(1, |acc, t| acc*(2*t-5)) as f64;
                dat.push(f64::value_from(data).unwrap());
                dat.push(f64::value_from(data).unwrap()*leafspr);
            }
            for data in data.gpenumdata[s].prune_times() {
                dat.push(sec(data));
            }

            for i in 0..data.gpenumdata[s].prune_times().len() {
                let mut leafspr = (i+4..terminals).fold(1, |acc, t| acc*(2*t-5)) as f64;
                let usedtime = sec(data.rmtdata[s].time())
                    + sec(&data.gpenumdata[s].prune_times()[i]);
                let timewopruning = sec(data.rmtdata[s].time())
                    + f64::value_from(data.gpenumdata[s].bsd_pruned()[i] + data.gpenumdata[s].ss_pruned()[i]).unwrap() * leafspr
                        * (sec(data.rmtdata[s].time()) / f64::value_from(data.rmtdata[s].nodes()).unwrap());
                dat.push(((timewopruning-usedtime)/timewopruning) * 100.0);
            }

            write!(file, "{}", s+1);
            for &d in dat.iter() {
                write!(file, " {}", d);
            }
            writeln!(file, "");
            dat
        };

        fn writeavg(file: &mut BufWriter<File>, avg: &[f64]) {
            write!(file, "{}", avg[0]);
            for d in avg.iter().skip(1) {
                write!(file, " {}", d);
            }
            writeln!(file, "");
        };

        self.path.set_extension("stpdata");
        let mut file = BufWriter::new(File::create(&self.path).unwrap());
        writeheader(&mut file, true, false, self.n);

        let mut avg = vec![0.0; 26 + self.gpenumdata[0].bsd_pruned().len()*6];
        for s in 0..self.num() {
            let dat = writeline(self, &mut file, s);
            for (avg, n) in avg.iter_mut().zip(dat.iter()) {
                *avg += n / (self.num() as f64);
            }
        }

        self.path.set_extension("stpdataavg");
        file = BufWriter::new(File::create(&self.path).unwrap());
        writeheader(&mut file, false, false, self.n);
        writeavg(&mut file, &avg);

        file = BufWriter::new(
            OpenOptions::new()
                .write(true)
                .append(true)
                .create(true)
                .open("./instances/d".to_string() + &self.d.to_string() + "/" + &self.alg + ".stpdataavgs")
                .unwrap());
        write!(file, "{} ", self.n);
        writeavg(&mut file, &avg[0..26]);
    }
}

fn writeheader(file: &mut BufWriter<File>, s: bool, t: bool, n: usize) {
    if s {
        write!(file, "s ");
    }
    if t {
        write!(file, "t ");
    }

    write!(file, "sbnbtime sbnbbestupdates gpenumnodes gpenumtime \
              gpenumtimepercentage gpenumsorttime gpenumbsdtime \
              gpenumsstime gpenuminittime gpenuminittimepercentage \
              totalprunetime totalprunetimepercentage rmtnodes rmttime \
              rmttimepercentage rmtselftime rmtselftimepercentage \
              rmtiterations geomediantime \
              geomediantimepercentage geomedianaveragesteps \
              geomedianaveragetimestep geomedianaverageproblemtime \
              geomedianstepfixedpoints geomedianstepprecisionerrors sr");

    for level in 1..n-2 {
        write!(file, " gpenumbsdpruned-level{} gpenumbsdleafspruned-level{}", level, level);
    }
    for level in 1..n-2 {
        write!(file, " gpenumsspruned-level{} gpenumssleafspruned-level{}", level, level);
    }
    for level in 1..n-2 {
        write!(file, " gpenumprunetime-level{}", level);
    }
    for level in 1..n-2 {
        write!(file, " gpenumpruneperformance-level{}", level);
    }
    writeln!(file, "");
}


fn cherry(path: &Path) {
    println!("Reading {}", path.display());
    let i = slurp_file_2d(&path);

    //let geo = EuclideanSpace;
    //let mut smtalg = SteinerBnB::default();


    let geo = LpSpace::new(3.0);
    //let cf = ChiaFranco::new(|n| P::R::from(n as f64).pow(P::R::from(-3.0)));
    let cf = ChiaFrancoApprox::new(0.00001);
    let mut smtalg = SteinerBnB::default_rmt(
        GeoMedianIter::default_with_geomedian(
            GeoMedianStepper::default_with_step(
                cf
    )));

    let mst = Kruskal::new().find(&i[..], &geo);
    println!("JAA");
    let smt = smtalg.find(i.clone(), &geo);
    println!("JAA");
    let sr = smt.len(&geo) / mst.len(&geo);

    println!("All distances are in {} and the solution was found by the {}.", &geo, smtalg);

    println!("\n########## Results:\n");

    println!("Steiner tree len: {}", smt.len(&geo));
    println!("MST len: {}", mst.len(&geo));
    println!("Steiner Ratio: {}", sr);
    println!("Solution {}", smt);

    println!("########## Statistics: (All times are given in seconds)\n");
    smtalg.print_data(&mut stdout());
}

fn do_experiments() {
    let mut path = PathBuf::from("./instances");

    let mut datas = Vec::new();
    for alg in 0..4 {
        datas.push(Vec::new());
        for d in 0..10 {
            datas[alg].push(Vec::new());
            for _ in 0..16 {
                datas[alg][d].push(Data::new(path.clone()));
            }
        }
    }
    let filenames = ["brimberg_1", "euclidean", "chia_3", "chia_20"];
    for (alg, algs) in datas.iter_mut().enumerate() {
        for (d, dim) in algs.iter_mut().enumerate() {
            for (n, data) in dim.iter_mut().enumerate() {
                data.path.push(&("d".to_string() + &d.to_string()));
                data.path.push(&("n".to_string() + &n.to_string()));
                data.path.push(&("d".to_string() + &(d.to_string())
                               + "_n" + &(n.to_string())
                               + "_" + filenames[alg]));
                data.d = d;
                data.n = n;
                data.alg = filenames[alg].to_string();
            }
        }
    }

    fn doit<P, F>(d: usize, n: usize, s: usize, path: &mut PathBuf, datas: &mut Vec<Vec<Vec<Data<P::R>>>>, slurp: &F)
        where P: Point, F: Fn(&Path) -> Vec<P> {

        path.push("d".to_string() + &(d.to_string()));
        path.push("n".to_string() + &(n.to_string()));
        let name = "s".to_string() + &(s.to_string());
        path.push(&name);
        path.set_extension("stp");
        println!("Reading {}", path.display());
        let i = slurp(&path);
        path.pop();

        println!("Brimberg");
        brimberg(&i[..], path, &name, &mut datas[0][d][n]);
        println!("Euclidean");
        euclidean(&i[..], path, &name, &mut datas[1][d][n]);
        //println!("Chia-Franco");
        //chia3(&i[..], path, &name, &mut datas[2][d][n]);
        //println!("Chia-Franco");
        //chia20(&i[..], path, &name, &mut datas[3][d][n]);

        path.pop();
        path.pop();
    };

    for d in 3..10 {
        for &name in filenames.iter() {
        let mut file = BufWriter::new(
            OpenOptions::new()
                .write(true).truncate(true).create(true)
                .open("./instances/d".to_string() + &d.to_string() + "/" + name + ".stpdataavgs")
                .unwrap());
            writeheader(&mut file, false, true, 3);
        }

        for n in 5..16 {
            for s in 1..21 {
                match d {
                    2 => doit(d, n, s, &mut path, &mut datas, &slurp_file_2d),
                    3 => doit(d, n, s, &mut path, &mut datas, &slurp_file_3d),
                    4 => doit(d, n, s, &mut path, &mut datas, &slurp_file_4d),
                    5 => doit(d, n, s, &mut path, &mut datas, &slurp_file_5d),
                    6 => doit(d, n, s, &mut path, &mut datas, &slurp_file_6d),
                    7 => doit(d, n, s, &mut path, &mut datas, &slurp_file_7d),
                    8 => doit(d, n, s, &mut path, &mut datas, &slurp_file_8d),
                    9 => doit(d, n, s, &mut path, &mut datas, &slurp_file_9d),
                    _ => panic!("ARGH!")
                };
            }

            datas[0][d][n].print();
            datas[1][d][n].print();
            //datas[2][d][n].print();
            //datas[3][d][n].print();
        }
    }
}

fn slurp_file_2d(file: &Path) -> Vec<Point2d<f64>> {
    let mut lines = BufReader::new(File::open(file).unwrap()).lines().skip(9);
    let mut n = lines.next().unwrap().unwrap();
    let mut n2 = n.as_str().trim().split(' ').skip(1).next().unwrap();
    let mut n3 = n2.parse::<usize>().unwrap();
    lines.skip(3).take(n3).map(|line| {
            let line = line.unwrap();
            let mut iter = line.as_str().trim()
                .split(' ').skip(2)
                .map(|x| x.parse::<f64>().unwrap());

            Point2d::new([
                iter.next().unwrap(),
                iter.next().unwrap()
            ])
        })
        .collect()
}

fn slurp_file_3d(file: &Path) -> Vec<Point3d<f64>> {
    let mut lines = BufReader::new(File::open(file).unwrap()).lines().skip(9);
    let mut n = lines.next().unwrap().unwrap();
    let mut n2 = n.as_str().trim().split(' ').skip(1).next().unwrap();
    let mut n3 = n2.parse::<usize>().unwrap();
    lines.skip(3).take(n3).map(|line| {
            let line = line.unwrap();
            let mut iter = line.as_str().trim()
                .split(' ').skip(2)
                .map(|x| x.parse::<f64>().unwrap());

            Point3d::new([
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap()
            ])
        })
        .collect()
}

fn slurp_file_4d(file: &Path) -> Vec<Point4d<f64>> {
    let mut lines = BufReader::new(File::open(file).unwrap()).lines().skip(9);
    let mut n = lines.next().unwrap().unwrap();
    let mut n2 = n.as_str().trim().split(' ').skip(1).next().unwrap();
    let mut n3 = n2.parse::<usize>().unwrap();
    lines.skip(3).take(n3).map(|line| {
            let line = line.unwrap();
            let mut iter = line.as_str().trim()
                .split(' ').skip(2)
                .map(|x| x.parse::<f64>().unwrap());

            Point4d::new([
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap()
            ])
        })
        .collect()
}

fn slurp_file_5d(file: &Path) -> Vec<Point5d<f64>> {
    let mut lines = BufReader::new(File::open(file).unwrap()).lines().skip(9);
    let mut n = lines.next().unwrap().unwrap();
    let mut n2 = n.as_str().trim().split(' ').skip(1).next().unwrap();
    let mut n3 = n2.parse::<usize>().unwrap();
    lines.skip(3).take(n3).map(|line| {
            let line = line.unwrap();
            let mut iter = line.as_str().trim()
                .split(' ').skip(2)
                .map(|x| x.parse::<f64>().unwrap());

            Point5d::new([
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap()
            ])
        })
        .collect()
}

fn slurp_file_6d(file: &Path) -> Vec<Point6d<f64>> {
    let mut lines = BufReader::new(File::open(file).unwrap()).lines().skip(9);
    let mut n = lines.next().unwrap().unwrap();
    let mut n2 = n.as_str().trim().split(' ').skip(1).next().unwrap();
    let mut n3 = n2.parse::<usize>().unwrap();
    lines.skip(3).take(n3).map(|line| {
            let line = line.unwrap();
            let mut iter = line.as_str().trim()
                .split(' ').skip(2)
                .map(|x| x.parse::<f64>().unwrap());

            Point6d::new([
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap()
            ])
        })
        .collect()
}

fn slurp_file_7d(file: &Path) -> Vec<Point7d<f64>> {
    let mut lines = BufReader::new(File::open(file).unwrap()).lines().skip(9);
    let mut n = lines.next().unwrap().unwrap();
    let mut n2 = n.as_str().trim().split(' ').skip(1).next().unwrap();
    let mut n3 = n2.parse::<usize>().unwrap();
    lines.skip(3).take(n3).map(|line| {
            let line = line.unwrap();
            let mut iter = line.as_str().trim()
                .split(' ').skip(2)
                .map(|x| x.parse::<f64>().unwrap());

            Point7d::new([
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap()
            ])
        })
        .collect()
}

fn slurp_file_8d(file: &Path) -> Vec<Point8d<f64>> {
    let mut lines = BufReader::new(File::open(file).unwrap()).lines().skip(9);
    let mut n = lines.next().unwrap().unwrap();
    let mut n2 = n.as_str().trim().split(' ').skip(1).next().unwrap();
    let mut n3 = n2.parse::<usize>().unwrap();
    lines.skip(3).take(n3).map(|line| {
            let line = line.unwrap();
            let mut iter = line.as_str().trim()
                .split(' ').skip(2)
                .map(|x| x.parse::<f64>().unwrap());

            Point8d::new([
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap()
            ])
        })
        .collect()
}

fn slurp_file_9d(file: &Path) -> Vec<Point9d<f64>> {
    let mut lines = BufReader::new(File::open(file).unwrap()).lines().skip(9);
    let mut n = lines.next().unwrap().unwrap();
    let mut n2 = n.as_str().trim().split(' ').skip(1).next().unwrap();
    let mut n3 = n2.parse::<usize>().unwrap();
    lines.skip(3).take(n3).map(|line| {
            let line = line.unwrap();
            let mut iter = line.as_str().trim()
                .split(' ').skip(2)
                .map(|x| x.parse::<f64>().unwrap());

            Point9d::new([
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap(),
                iter.next().unwrap()
            ])
        })
        .collect()
}

fn steiner_ratio<P, M, A, B>(i: &[P], smtalg: &mut A, mstalg: &mut B, geo: &M)
        -> (SteinerTree<P>, SteinerTree<P>, P::R)
    where P: Point, M: MinkowskiSpace<P>, A: SMT<P, M>, B: MST<P, M> {

    let (smt, mst) = (smtalg.find(i.to_vec(), geo), mstalg.find(&i[..], geo));
    let sr = smt.len(geo) / mst.len(geo);
    if sr >= P::R::one() { println!("######################### {}", smt); }
    (smt, mst, sr)
}

fn run<P, M, A>(i: &[P], smtalg: &mut A, geo: &M, file: &mut BufWriter<File>) -> P::R
    where P: Point, M: MinkowskiSpace<P>, A: SMT<P, M>  {

    let (smt, mst, sr) = steiner_ratio(i, smtalg, &mut Kruskal::new(), geo);

    writeln!(file, "All distances are in {} and the solution was found by the {}.", geo, smtalg);

    writeln!(file, "\n########## Results:\n");

    writeln!(file, "Steiner tree len: {}", smt.len(geo));
    writeln!(file, "MST len: {}", mst.len(geo));
    writeln!(file, "Steiner Ratio: {}", sr);
    writeln!(file, "Solution {}", smt);

    writeln!(file, "########## Statistics: (All times are given in seconds)\n");
    smtalg.print_data(file);
    sr

}
